#!/bin/bash

#
# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.
#


pushd apps/blockchain && 
ignite generate ts-client && 
cp -r ts-client/org.borderblockchain.businesslogic/* ../../libs/api-interfaces/src/lib/client/org.borderblockchain.businesslogic/ &&

# Bandaid Fix for Angular and one Microservice having hickups with some definitions of Proto generated Code.
#find ../../libs/api-interfaces/src/lib/client/org.borderblockchain.businesslogic  -type f -name '*.ts' -exec sed -i 's/util.Long !== Long/true/g' {} + &&
#find ../../libs/api-interfaces/src/lib/client/org.borderblockchain.businesslogic  -type f -name '*.ts' -exec sed -i 's/import _m0 from/import * as _m0 from/g' {} + &&
#find ../../libs/api-interfaces/src/lib/client/org.borderblockchain.businesslogic  -type f -name '*.ts' -exec sed -i 's/static plugins = currentPlugins.concat(plugin);/static override plugins = currentPlugins.concat(plugin);/g' {} +
popd

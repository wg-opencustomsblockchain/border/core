# Border

This is the modular Blockchain Europe "Border" repository which houses the components of the BORDER blockchain project.
We use the following technologies:

- Angular Frontend (Typescript)
- NestJS Backend Microservices (Typescript)
- Tendermint/Cosmos Blockchain (Golang)
- Rabbit MQ (MQP)

As package manager we recommend using yarn.
This project was generated based on the NX build system https://nx.dev/

# License

Border is released under the Open Logistics License, Version 1.0

# Tools & Libs

The `tools` folder contains scripts to facilitate the maintainability of the project, like additional feature for yarn, or the license header.

The `libs` folder contains shared code wich is used by multiple projects at once, e.g. the AMQP-Patterns required for the communication between different microservices, or the models representing the user.

## Prerequisites

This project is tested with the following frameworks:

- Golang ver. 1.19 (Breaking Changes below)
- Node ver. 18.12.x.
- Yarn ver. 3.2.1
- Ignite ver. v0.25.2 via ( `curl https://get.ignite.com/cli@v0.25.2! | bash`)

If required we recommend referring to the individual docker Files included in each project for detailed installation instructions.

## Quick Start

As this project depends on the Tokenmanager base component we recommend checking out the repositories and submodules recursively via:  
`git clone --recursive`  
`git pull --recursive`

After checking out the repository a setup for the environment variables is required.
For this we have provided an example file. Please substitute the placeholder fields with your own setup.

<details>
  <summary>.env example</summary>

```
# The log level setting.
LOG_LEVEL=dev
# LOG_LEVEL=prod
# All available Log levels"verbose,debug,log,warn,error"
LOG_SETTINGS_DEV="debug,log,error,warn"
LOG_SETTINGS_PROD="error,warn"

# Rabbit MQ and Authentication secrets
# Please subsitute your own JWT secrets.
MSG_QUEUE_URL=amqp://127.0.0.1:5672
JWT_SECRET="<PLACEHOLDER>"
JWT_EXPIRATION_TIME="1h"

# Environments for the Microservice queues.
AMQP_QUEUE_AUTH="border_auth"
AMQP_QUEUE_BLOCKCHAIN="border_blockchain"
AMQP_QUEUE_COMPANY="border_company"
AMQP_QUEUE_IMPORT="border_import"

# Setup variables for the keycloak instance.
# Border requires a user management to run. Please substitute your own.
KEYCLOAK_URL=<PLACEHOLDER>
KEYCLOAK_REALM=BORDER
KEYCLOAK_AUTH_CLIENT_ID=auth-service
KEYCLOAK_AUTH_SECRET=<PLACEHOLDER>
KEYCLOAK_CONTROLLER_CLIENT_ID=nest
KEYCLOAK_CONTROLLER_SECRET=<PLACEHOLDER>

COMPANY_PORT=8083
AUTH_PORT=8082
DATA_PORT=8081

# The basic local running ports of the tendermint instance.
# These can be substituted to a remote running instance as well.
TENDERMINT_CHAIN_ENDPOINT_RPC=http://localhost:26657
TENDERMINT_CHAIN_ENDPOINT_NODE=http://localhost:1317

# Basic Wallet Address to use for the backend microservices.
WALLET_ADDRESS=cosmos1N2qdDpTMMSN87Qa8w4eP2STGb8EcBBwgs
WALLET_ADDRESS_PREFIX=cosmos

# Type URLs for the different modules.
BORDERBLOCKCHAIN_URL=org/borderblockchain
BLOCKCHAIN_EXPORT_TYPE_URL=/org.borderblockchain.exporttoken
BLOCKCHAIN_IMPORT_TYPE_URL=/org.borderblockchain.importtoken
BLOCKCHAIN_BUSINESS_TYPE_URL=/org.borderblockchain.businesslogic

# Tendermint configuration.
TENDERMINT_MAX_RETRIES=50
TENDERMINT_MAX_GAS='1800000000'

TOKENWALLET_ID_CONNECTOR='DE537400371045831'

# Mnemonic for Blockchain identities.
# These are used to sign transactions/Generate private/public key pairs.
# We have included some examples, but for security reasons you should set up your own cosmos accounts.
ERIK_MNEMONIC="identify border wish sleep imitate clay flush ask sting canvas celery case coil idle assume violin plastic result immense green cream hockey educate dress"

FREDDY_MNEMONIC="opera desert element marriage cabbage minor cheap inform ugly again quote nerve accident action torch safe shock possible fury gesture time key gym pottery"

INA_MNEMONIC="roast copper notice peasant fix vintage useful obtain review ocean city exist chef require invite candy door grape hood refuse ugly foot spoil parent"

BROKER_MNEMONIC="human route comic gun anger elephant canal clever cinnamon error exit uncover head topic margin enforce monkey relief debate model note salad include just"

FABIAN_MNEMONIC="correct youth income public force dance right kick idle boy arch demand define gate famous vault large step series field echo accuse trophy sphere"

INGRID_MNEMONIC="robust whale science treat ahead flock habit volcano lucky torch patient wave round three defense nasty desk relief water dynamic such garage broken town"

# The default identity used by the connector
WALLET_ADDRESS_MNEMONIC="identify border wish sleep imitate clay flush ask sting canvas celery case coil idle assume violin plastic result immense green cream hockey educate dress"

# Wallet IDs used by the Tokenmanager module to identify which wallet of our module belongs to whom.
ERIK_WALLET="DE537400371045831"
FREDDY_WALLET="FREDDY_WALLET"
INA_WALLET="INA_WALLET"

FABIAN_WALLET="FABIAN_WALLET"
INGRID_WALLET="INGRID_WALLET"
```

</details>

Before starting the application we need a RabbitMQ instance.
We Recommend using a docker container with following configuration:
`docker run -it --rm --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq`

Then, in another terminal:
Build and Initialize the Blockchain.
`yarn blockchain:init`

The Typescript needs to be generated by running the "ts-client.sh" script.
`yarn blockchain:ts-client`

Start the Blockchain via the startup "init.sh" script.
`yarn blockchain:start`

After pulling the software run:
`yarn install`

To test all services run:
`yarn test`

To start the frontend and all microservice run:
`yarn all`

Refer to the "package.json" file in the project root for an overview of the nx based commands.

## Project Overview & Documentation

The extensive Border arc42 documentation can be found [here](documentation/).

### frontend

The border.frontend serves the main component to orchestrate both import and export processes from the perspective of export, import and logistic workers. It can write/read and sign blockchain transaction on its own with Light Client capabilities.

### user and company management (auth-service, company-service)

The user and company management serves as a placeholder endpoint to serve basic user-data for our use-case demonstration. Currently border supports three process views: (Erik: Export, Freddy: Driver and Ina: Import). Exposes a number of AMQP endpoints and implements basic authentication.

This microservice is separated into a User controller for business logic and a service to serve as a data-source.

The users different companies are being handled by the company services, and offer AMQP endpoints to map a given user to his company.

### api-controller

The api-controller serves as a controller between the blockchain connector and the application. Exposes a number of REST endpoints and communicates with the Tendermint Blockchain Connector via a MQ (Message Queue) Broker to serve Blockchain responses to the frontend border application.

### blockchain-connector

The blockchain-connector allows to read/write transactions to a Cosmos/Tendermint Blockchain instance.

### blockchain (tokenmanager)

The blockchain tokenmanager serves as the backbone of the border application.
It provides export/import token functionalities for saving, sharing and updating customs processes, as well as all tokenmanager functionalities developed in the base component repository as outlined in the [arc42](https://git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/documentation) documentation and [repository](https://git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/tokenmanager).

Standard RPC port: http://localhost:26657
Standard Node Address: http://localhost:1317

### Third Party Licenses

All third party licenses can be found here: https://silicon-economy.pages.fraunhofer.de/services/border/border/npm-licenses-list.txt

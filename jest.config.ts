const { getJestProjects } = require('@nrwl/jest');

export default {
  projects: getJestProjects(),
  reporters: ['default', 'jest-junit'],
};

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export enum UserMsgPatterns {
  // Message Queue API TAG
  API_VERSION = '/v1',
  API_TAG = 'auth',

  // ENDPOINTS
  GET_ONE_BY_USERNAME = '/users',
  GET_ONE_BY_ID = '/users/id',
  GET_ONE_BY_COSMOS = '/users/cosmos',
}

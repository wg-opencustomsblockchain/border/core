/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ExportToken } from '../client/org.borderblockchain.businesslogic/types/exporttoken/exporttoken';
import { EventDto } from './event.dto';

/**
 * @Interface for accepting incoming token writing requests.
 * Extends the Blockchain Data Model generated Protofile "ExportToken".
 */
export interface ExportSaveDTO extends ExportToken {
  /** Status as defined in the Export Process Enum. */
  status: string;

  /** The Tokenwallet the token belongs to */
  tokenWalletId: string;
}
/** DTO Object to consume Blockchain response.*/
export interface ExportTokenDTO extends ExportToken {
  /**Status as defined in the Export Process Enum. */
  status: string;

  /** List of process updates. */
  events: EventDto[];

  /** The timestamp of the last update. */
  timestamp: string;

  /** The Tokenwallet the token belongs to */
  tokenWalletId: string;

  /** The Last Event of the Token */
  eventType: string;
}

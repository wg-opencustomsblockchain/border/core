/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { UserRole } from '../entities/user';
import { EventDto } from './event.dto';
import { ExportSaveDTO } from './export.dto';
import { ImportSaveDTO } from './import.dto';

/** Ead DTO Definitions with Swagger API Properties. */
export class TokenQueryDto {
  wallet: string;
  mnemonic: string;
  query: any;
  tokenWalletId: string;
  role?: UserRole;
}

/** Ead DTO Definitions with Swagger API Properties. */
export class TokenTransactionDto {
  dto: EventDto[] | ImportSaveDTO[] | ExportSaveDTO[];
  mnemonic: string;
  tokenWalletId: string;
  role?: UserRole;
}

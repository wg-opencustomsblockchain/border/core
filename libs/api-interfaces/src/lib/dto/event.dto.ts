/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/** Event and Event History definitions. Every Token has an event history associated with it. */
export class EventHistory {
  creator: string;
  index: string;
  events: EventDto[];
}

export class EventDto {
  creator: string;
  status: string;
  message: string;
  index: string;
  eventType: string;
  timestamp?: string;
  creatorName?: string;
}

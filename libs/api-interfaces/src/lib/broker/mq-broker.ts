/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { DynamicModule } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { AmqpClientEnum } from '../patterns';

/**
 * The interface used to communicate to other microservices via mqtt.
 */
export class MQBroker {
  getMsgBroker(): DynamicModule {
    return ClientsModule.register([
      {
        name: AmqpClientEnum.AMQP_CLIENT_BLOCKCHAIN,
        transport: Transport.RMQ,
        options: {
          urls: [process.env.MSG_QUEUE_URL],
          queue: AmqpClientEnum.AMQP_QUEUE_BLOCKCHAIN,
          queueOptions: {
            durable: false,
          },
        },
      },
      {
        name: AmqpClientEnum.AMQP_CLIENT_DATA,
        transport: Transport.RMQ,
        options: {
          urls: [process.env.MSG_QUEUE_URL],
          queue: AmqpClientEnum.AMQP_QUEUE_DATA,
          queueOptions: {
            durable: false,
          },
        },
      },
      {
        name: AmqpClientEnum.AMQP_CLIENT_AUTH,
        transport: Transport.RMQ,
        options: {
          urls: [process.env.MSG_QUEUE_URL],
          queue: AmqpClientEnum.AMQP_QUEUE_AUTH,
          queueOptions: {
            durable: false,
          },
        },
      },

      {
        name: AmqpClientEnum.AMQP_CLIENT_COMPANY,
        transport: Transport.RMQ,
        options: {
          urls: [process.env.MSG_QUEUE_URL],
          queue: AmqpClientEnum.AMQP_QUEUE_COMPANY,
          queueOptions: {
            durable: false,
          },
        },
      },
      {
        name: AmqpClientEnum.AMQP_CLIENT_HISTORY,
        transport: Transport.RMQ,
        options: {
          urls: [process.env.MSG_QUEUE_URL],
          queue: AmqpClientEnum.AMQP_QUEUE_HISTORY,
          queueOptions: {
            durable: false,
          },
        },
      },
    ]);
  }
}

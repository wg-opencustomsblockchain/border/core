/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Address } from './address.entity';

export class Company {
  name: string;
  address: Address;
  customsId?: string;
  subsidiaryNumber?: number;
  users?: string[];
  identificationNumber?: string;
}

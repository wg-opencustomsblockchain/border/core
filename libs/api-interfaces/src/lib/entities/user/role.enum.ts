/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export enum Role {
  DRIVER,
  EXPORTER,
  IMPORTER,
}

export enum UserRole {
  DRIVER = 'DRIVER',
  EXPORTER = 'EXPORTER',
  IMPORTER = 'IMPORTER',
}

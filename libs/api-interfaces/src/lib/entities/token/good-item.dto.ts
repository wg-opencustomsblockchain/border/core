/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Document } from './document.dto';

/** Ead DTO Definitions with Swagger API Properties. */
export class GoodItemDto {
  documents: Document[];
  countryOfOrigin: string;
  sequenceNumber: number;
  descriptionOfGoods: string;
  harmonizedSystemSubheadingCode: number;
  localClassificationCode: number;
  grossMass: string;
  netMass: number;
  numberOfPackages: number;
  typeOfPackages: string;
  marksNumbers: string;
  containerId: string;
  consigneeOrderNumber: string;
  valueAtBorderExport: number;
  valueAtBorderExportCurrency: string;
  amountInvoiced: number;
  amountInvoicedCurrency: string;
  dangerousGoodsCode: number;

  valueAtBorder?: number | undefined;
  valueAtBorderCurrency?: string | undefined;
}

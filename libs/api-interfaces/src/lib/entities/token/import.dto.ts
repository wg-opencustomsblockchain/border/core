/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */

import { ApiProperty } from '@nestjs/swagger';
import { GoodItemDto } from './good-item.dto';
import { CustomsOfficeDto } from './customs-office.dto';
import { TransportDto } from './transport.dto';
import { CompanyDto } from './company.dto';
import { EventDto } from '../../dto/event.dto';

/** Import DTO corresponding to import process definition with Swagger documentation tags. */
export class ImportDTO {
  @ApiProperty() creator: string;
  @ApiProperty() id: string;
  @ApiProperty() consignor: CompanyDto | undefined;
  @ApiProperty() exporter: CompanyDto | undefined;
  @ApiProperty() consignee: CompanyDto | undefined;
  @ApiProperty() declarant: CompanyDto | undefined;
  @ApiProperty() customOfficeOfExit: CustomsOfficeDto | undefined;
  @ApiProperty() customOfficeOfEntry: CustomsOfficeDto | undefined;
  @ApiProperty() customOfficeOfImport: CustomsOfficeDto | undefined;
  @ApiProperty() goodsItems: GoodItemDto[];
  @ApiProperty() transportAtBorder: TransportDto | undefined;
  @ApiProperty() transportFromBorder: TransportDto | undefined;
  @ApiProperty() importId: string;
  @ApiProperty() uniqueConsignmentReference: string;
  @ApiProperty() localReferenceNumber: string;
  @ApiProperty() destinationCountry: string;
  @ApiProperty() exportCountry: string;
  @ApiProperty() itinerary: string;
  @ApiProperty() incotermCode: string;
  @ApiProperty() incotermLocation: string;
  @ApiProperty() totalGrossMass: string;
  @ApiProperty() goodsItemQuantity: number;
  @ApiProperty() totalPackagesQuantity: number;
  @ApiProperty() releaseDateAndTime: string;
  @ApiProperty() natureOfTransaction: number;
  @ApiProperty() totalAmountInvoiced: number;
  @ApiProperty() invoiceCurrency: string;
  @ApiProperty() relatedExportToken: string;

  @ApiProperty() timestamp: string;
  @ApiProperty() eventType: string;

  @ApiProperty() status: string;
  @ApiProperty() user: string;
  // @ApiProperty() timestamp: string;
  @ApiProperty() events: EventDto[];
}

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/** Ead DTO Definitions with Swagger API Properties. */
export class TransportDto {
  modeOfTransport: number;
  typeOfIdentification: number;
  identity: string;
  nationality: string;
  transportCost: number;
  transportCostCurrency: string;
  transportOrderNumber: string;
}

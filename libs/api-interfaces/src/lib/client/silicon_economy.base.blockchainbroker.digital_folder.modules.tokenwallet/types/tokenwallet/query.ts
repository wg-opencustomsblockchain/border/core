/* eslint-disable */
import _m0 from "protobufjs/minimal";
import { PageRequest, PageResponse } from "../cosmos/base/query/v1beta1/pagination";
import { Segment } from "./segment";
import { SegmentHistory } from "./segmentHistory";
import { TokenHistoryGlobal } from "./tokenHistoryGlobal";
import { Wallet } from "./wallet";
import { WalletHistory } from "./walletHistory";

export const protobufPackage = "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet";

/** this line is used by starport scaffolding # 3 */
export interface QueryGetTokenHistoryGlobalRequest {
  id: string;
}

export interface QueryGetTokenHistoryGlobalResponse {
  TokenHistoryGlobal: TokenHistoryGlobal | undefined;
}

export interface QueryAllTokenHistoryGlobalRequest {
  pagination: PageRequest | undefined;
}

export interface QueryAllTokenHistoryGlobalResponse {
  TokenHistoryGlobal: TokenHistoryGlobal[];
  pagination: PageResponse | undefined;
}

export interface QueryGetSegmentHistoryRequest {
  id: string;
}

export interface QueryGetSegmentHistoryResponse {
  SegmentHistory: SegmentHistory | undefined;
}

export interface QueryAllSegmentHistoryRequest {
  pagination: PageRequest | undefined;
}

export interface QueryAllSegmentHistoryResponse {
  SegmentHistory: SegmentHistory[];
  pagination: PageResponse | undefined;
}

export interface QueryGetSegmentRequest {
  id: string;
}

export interface QueryGetSegmentResponse {
  Segment: Segment | undefined;
}

export interface QueryAllSegmentRequest {
  pagination: PageRequest | undefined;
}

export interface QueryAllSegmentResponse {
  Segment: Segment[];
  pagination: PageResponse | undefined;
}

export interface QueryGetWalletHistoryRequest {
  id: string;
}

export interface QueryGetWalletHistoryResponse {
  WalletHistory: WalletHistory | undefined;
}

export interface QueryAllWalletHistoryRequest {
  pagination: PageRequest | undefined;
}

export interface QueryAllWalletHistoryResponse {
  WalletHistory: WalletHistory[];
  pagination: PageResponse | undefined;
}

export interface QueryGetWalletRequest {
  id: string;
}

export interface QueryGetWalletResponse {
  Wallet: Wallet | undefined;
}

export interface QueryAllWalletRequest {
  pagination: PageRequest | undefined;
}

export interface QueryAllWalletResponse {
  Wallet: Wallet[];
  pagination: PageResponse | undefined;
}

function createBaseQueryGetTokenHistoryGlobalRequest(): QueryGetTokenHistoryGlobalRequest {
  return { id: "" };
}

export const QueryGetTokenHistoryGlobalRequest = {
  encode(message: QueryGetTokenHistoryGlobalRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.id !== "") {
      writer.uint32(10).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryGetTokenHistoryGlobalRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryGetTokenHistoryGlobalRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetTokenHistoryGlobalRequest {
    return { id: isSet(object.id) ? String(object.id) : "" };
  },

  toJSON(message: QueryGetTokenHistoryGlobalRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryGetTokenHistoryGlobalRequest>, I>>(
    object: I,
  ): QueryGetTokenHistoryGlobalRequest {
    const message = createBaseQueryGetTokenHistoryGlobalRequest();
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseQueryGetTokenHistoryGlobalResponse(): QueryGetTokenHistoryGlobalResponse {
  return { TokenHistoryGlobal: undefined };
}

export const QueryGetTokenHistoryGlobalResponse = {
  encode(message: QueryGetTokenHistoryGlobalResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.TokenHistoryGlobal !== undefined) {
      TokenHistoryGlobal.encode(message.TokenHistoryGlobal, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryGetTokenHistoryGlobalResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryGetTokenHistoryGlobalResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.TokenHistoryGlobal = TokenHistoryGlobal.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetTokenHistoryGlobalResponse {
    return {
      TokenHistoryGlobal: isSet(object.TokenHistoryGlobal)
        ? TokenHistoryGlobal.fromJSON(object.TokenHistoryGlobal)
        : undefined,
    };
  },

  toJSON(message: QueryGetTokenHistoryGlobalResponse): unknown {
    const obj: any = {};
    message.TokenHistoryGlobal !== undefined && (obj.TokenHistoryGlobal = message.TokenHistoryGlobal
      ? TokenHistoryGlobal.toJSON(message.TokenHistoryGlobal)
      : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryGetTokenHistoryGlobalResponse>, I>>(
    object: I,
  ): QueryGetTokenHistoryGlobalResponse {
    const message = createBaseQueryGetTokenHistoryGlobalResponse();
    message.TokenHistoryGlobal = (object.TokenHistoryGlobal !== undefined && object.TokenHistoryGlobal !== null)
      ? TokenHistoryGlobal.fromPartial(object.TokenHistoryGlobal)
      : undefined;
    return message;
  },
};

function createBaseQueryAllTokenHistoryGlobalRequest(): QueryAllTokenHistoryGlobalRequest {
  return { pagination: undefined };
}

export const QueryAllTokenHistoryGlobalRequest = {
  encode(message: QueryAllTokenHistoryGlobalRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.pagination !== undefined) {
      PageRequest.encode(message.pagination, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryAllTokenHistoryGlobalRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryAllTokenHistoryGlobalRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.pagination = PageRequest.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllTokenHistoryGlobalRequest {
    return { pagination: isSet(object.pagination) ? PageRequest.fromJSON(object.pagination) : undefined };
  },

  toJSON(message: QueryAllTokenHistoryGlobalRequest): unknown {
    const obj: any = {};
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageRequest.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryAllTokenHistoryGlobalRequest>, I>>(
    object: I,
  ): QueryAllTokenHistoryGlobalRequest {
    const message = createBaseQueryAllTokenHistoryGlobalRequest();
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageRequest.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

function createBaseQueryAllTokenHistoryGlobalResponse(): QueryAllTokenHistoryGlobalResponse {
  return { TokenHistoryGlobal: [], pagination: undefined };
}

export const QueryAllTokenHistoryGlobalResponse = {
  encode(message: QueryAllTokenHistoryGlobalResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.TokenHistoryGlobal) {
      TokenHistoryGlobal.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.pagination !== undefined) {
      PageResponse.encode(message.pagination, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryAllTokenHistoryGlobalResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryAllTokenHistoryGlobalResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.TokenHistoryGlobal.push(TokenHistoryGlobal.decode(reader, reader.uint32()));
          break;
        case 2:
          message.pagination = PageResponse.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllTokenHistoryGlobalResponse {
    return {
      TokenHistoryGlobal: Array.isArray(object?.TokenHistoryGlobal)
        ? object.TokenHistoryGlobal.map((e: any) => TokenHistoryGlobal.fromJSON(e))
        : [],
      pagination: isSet(object.pagination) ? PageResponse.fromJSON(object.pagination) : undefined,
    };
  },

  toJSON(message: QueryAllTokenHistoryGlobalResponse): unknown {
    const obj: any = {};
    if (message.TokenHistoryGlobal) {
      obj.TokenHistoryGlobal = message.TokenHistoryGlobal.map((e) => e ? TokenHistoryGlobal.toJSON(e) : undefined);
    } else {
      obj.TokenHistoryGlobal = [];
    }
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageResponse.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryAllTokenHistoryGlobalResponse>, I>>(
    object: I,
  ): QueryAllTokenHistoryGlobalResponse {
    const message = createBaseQueryAllTokenHistoryGlobalResponse();
    message.TokenHistoryGlobal = object.TokenHistoryGlobal?.map((e) => TokenHistoryGlobal.fromPartial(e)) || [];
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageResponse.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

function createBaseQueryGetSegmentHistoryRequest(): QueryGetSegmentHistoryRequest {
  return { id: "" };
}

export const QueryGetSegmentHistoryRequest = {
  encode(message: QueryGetSegmentHistoryRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.id !== "") {
      writer.uint32(10).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryGetSegmentHistoryRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryGetSegmentHistoryRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetSegmentHistoryRequest {
    return { id: isSet(object.id) ? String(object.id) : "" };
  },

  toJSON(message: QueryGetSegmentHistoryRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryGetSegmentHistoryRequest>, I>>(
    object: I,
  ): QueryGetSegmentHistoryRequest {
    const message = createBaseQueryGetSegmentHistoryRequest();
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseQueryGetSegmentHistoryResponse(): QueryGetSegmentHistoryResponse {
  return { SegmentHistory: undefined };
}

export const QueryGetSegmentHistoryResponse = {
  encode(message: QueryGetSegmentHistoryResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.SegmentHistory !== undefined) {
      SegmentHistory.encode(message.SegmentHistory, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryGetSegmentHistoryResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryGetSegmentHistoryResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.SegmentHistory = SegmentHistory.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetSegmentHistoryResponse {
    return {
      SegmentHistory: isSet(object.SegmentHistory) ? SegmentHistory.fromJSON(object.SegmentHistory) : undefined,
    };
  },

  toJSON(message: QueryGetSegmentHistoryResponse): unknown {
    const obj: any = {};
    message.SegmentHistory !== undefined
      && (obj.SegmentHistory = message.SegmentHistory ? SegmentHistory.toJSON(message.SegmentHistory) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryGetSegmentHistoryResponse>, I>>(
    object: I,
  ): QueryGetSegmentHistoryResponse {
    const message = createBaseQueryGetSegmentHistoryResponse();
    message.SegmentHistory = (object.SegmentHistory !== undefined && object.SegmentHistory !== null)
      ? SegmentHistory.fromPartial(object.SegmentHistory)
      : undefined;
    return message;
  },
};

function createBaseQueryAllSegmentHistoryRequest(): QueryAllSegmentHistoryRequest {
  return { pagination: undefined };
}

export const QueryAllSegmentHistoryRequest = {
  encode(message: QueryAllSegmentHistoryRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.pagination !== undefined) {
      PageRequest.encode(message.pagination, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryAllSegmentHistoryRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryAllSegmentHistoryRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.pagination = PageRequest.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllSegmentHistoryRequest {
    return { pagination: isSet(object.pagination) ? PageRequest.fromJSON(object.pagination) : undefined };
  },

  toJSON(message: QueryAllSegmentHistoryRequest): unknown {
    const obj: any = {};
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageRequest.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryAllSegmentHistoryRequest>, I>>(
    object: I,
  ): QueryAllSegmentHistoryRequest {
    const message = createBaseQueryAllSegmentHistoryRequest();
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageRequest.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

function createBaseQueryAllSegmentHistoryResponse(): QueryAllSegmentHistoryResponse {
  return { SegmentHistory: [], pagination: undefined };
}

export const QueryAllSegmentHistoryResponse = {
  encode(message: QueryAllSegmentHistoryResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.SegmentHistory) {
      SegmentHistory.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.pagination !== undefined) {
      PageResponse.encode(message.pagination, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryAllSegmentHistoryResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryAllSegmentHistoryResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.SegmentHistory.push(SegmentHistory.decode(reader, reader.uint32()));
          break;
        case 2:
          message.pagination = PageResponse.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllSegmentHistoryResponse {
    return {
      SegmentHistory: Array.isArray(object?.SegmentHistory)
        ? object.SegmentHistory.map((e: any) => SegmentHistory.fromJSON(e))
        : [],
      pagination: isSet(object.pagination) ? PageResponse.fromJSON(object.pagination) : undefined,
    };
  },

  toJSON(message: QueryAllSegmentHistoryResponse): unknown {
    const obj: any = {};
    if (message.SegmentHistory) {
      obj.SegmentHistory = message.SegmentHistory.map((e) => e ? SegmentHistory.toJSON(e) : undefined);
    } else {
      obj.SegmentHistory = [];
    }
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageResponse.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryAllSegmentHistoryResponse>, I>>(
    object: I,
  ): QueryAllSegmentHistoryResponse {
    const message = createBaseQueryAllSegmentHistoryResponse();
    message.SegmentHistory = object.SegmentHistory?.map((e) => SegmentHistory.fromPartial(e)) || [];
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageResponse.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

function createBaseQueryGetSegmentRequest(): QueryGetSegmentRequest {
  return { id: "" };
}

export const QueryGetSegmentRequest = {
  encode(message: QueryGetSegmentRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.id !== "") {
      writer.uint32(10).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryGetSegmentRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryGetSegmentRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetSegmentRequest {
    return { id: isSet(object.id) ? String(object.id) : "" };
  },

  toJSON(message: QueryGetSegmentRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryGetSegmentRequest>, I>>(object: I): QueryGetSegmentRequest {
    const message = createBaseQueryGetSegmentRequest();
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseQueryGetSegmentResponse(): QueryGetSegmentResponse {
  return { Segment: undefined };
}

export const QueryGetSegmentResponse = {
  encode(message: QueryGetSegmentResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.Segment !== undefined) {
      Segment.encode(message.Segment, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryGetSegmentResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryGetSegmentResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.Segment = Segment.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetSegmentResponse {
    return { Segment: isSet(object.Segment) ? Segment.fromJSON(object.Segment) : undefined };
  },

  toJSON(message: QueryGetSegmentResponse): unknown {
    const obj: any = {};
    message.Segment !== undefined && (obj.Segment = message.Segment ? Segment.toJSON(message.Segment) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryGetSegmentResponse>, I>>(object: I): QueryGetSegmentResponse {
    const message = createBaseQueryGetSegmentResponse();
    message.Segment = (object.Segment !== undefined && object.Segment !== null)
      ? Segment.fromPartial(object.Segment)
      : undefined;
    return message;
  },
};

function createBaseQueryAllSegmentRequest(): QueryAllSegmentRequest {
  return { pagination: undefined };
}

export const QueryAllSegmentRequest = {
  encode(message: QueryAllSegmentRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.pagination !== undefined) {
      PageRequest.encode(message.pagination, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryAllSegmentRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryAllSegmentRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.pagination = PageRequest.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllSegmentRequest {
    return { pagination: isSet(object.pagination) ? PageRequest.fromJSON(object.pagination) : undefined };
  },

  toJSON(message: QueryAllSegmentRequest): unknown {
    const obj: any = {};
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageRequest.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryAllSegmentRequest>, I>>(object: I): QueryAllSegmentRequest {
    const message = createBaseQueryAllSegmentRequest();
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageRequest.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

function createBaseQueryAllSegmentResponse(): QueryAllSegmentResponse {
  return { Segment: [], pagination: undefined };
}

export const QueryAllSegmentResponse = {
  encode(message: QueryAllSegmentResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.Segment) {
      Segment.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.pagination !== undefined) {
      PageResponse.encode(message.pagination, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryAllSegmentResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryAllSegmentResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.Segment.push(Segment.decode(reader, reader.uint32()));
          break;
        case 2:
          message.pagination = PageResponse.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllSegmentResponse {
    return {
      Segment: Array.isArray(object?.Segment) ? object.Segment.map((e: any) => Segment.fromJSON(e)) : [],
      pagination: isSet(object.pagination) ? PageResponse.fromJSON(object.pagination) : undefined,
    };
  },

  toJSON(message: QueryAllSegmentResponse): unknown {
    const obj: any = {};
    if (message.Segment) {
      obj.Segment = message.Segment.map((e) => e ? Segment.toJSON(e) : undefined);
    } else {
      obj.Segment = [];
    }
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageResponse.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryAllSegmentResponse>, I>>(object: I): QueryAllSegmentResponse {
    const message = createBaseQueryAllSegmentResponse();
    message.Segment = object.Segment?.map((e) => Segment.fromPartial(e)) || [];
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageResponse.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

function createBaseQueryGetWalletHistoryRequest(): QueryGetWalletHistoryRequest {
  return { id: "" };
}

export const QueryGetWalletHistoryRequest = {
  encode(message: QueryGetWalletHistoryRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.id !== "") {
      writer.uint32(10).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryGetWalletHistoryRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryGetWalletHistoryRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetWalletHistoryRequest {
    return { id: isSet(object.id) ? String(object.id) : "" };
  },

  toJSON(message: QueryGetWalletHistoryRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryGetWalletHistoryRequest>, I>>(object: I): QueryGetWalletHistoryRequest {
    const message = createBaseQueryGetWalletHistoryRequest();
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseQueryGetWalletHistoryResponse(): QueryGetWalletHistoryResponse {
  return { WalletHistory: undefined };
}

export const QueryGetWalletHistoryResponse = {
  encode(message: QueryGetWalletHistoryResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.WalletHistory !== undefined) {
      WalletHistory.encode(message.WalletHistory, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryGetWalletHistoryResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryGetWalletHistoryResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.WalletHistory = WalletHistory.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetWalletHistoryResponse {
    return { WalletHistory: isSet(object.WalletHistory) ? WalletHistory.fromJSON(object.WalletHistory) : undefined };
  },

  toJSON(message: QueryGetWalletHistoryResponse): unknown {
    const obj: any = {};
    message.WalletHistory !== undefined
      && (obj.WalletHistory = message.WalletHistory ? WalletHistory.toJSON(message.WalletHistory) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryGetWalletHistoryResponse>, I>>(
    object: I,
  ): QueryGetWalletHistoryResponse {
    const message = createBaseQueryGetWalletHistoryResponse();
    message.WalletHistory = (object.WalletHistory !== undefined && object.WalletHistory !== null)
      ? WalletHistory.fromPartial(object.WalletHistory)
      : undefined;
    return message;
  },
};

function createBaseQueryAllWalletHistoryRequest(): QueryAllWalletHistoryRequest {
  return { pagination: undefined };
}

export const QueryAllWalletHistoryRequest = {
  encode(message: QueryAllWalletHistoryRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.pagination !== undefined) {
      PageRequest.encode(message.pagination, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryAllWalletHistoryRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryAllWalletHistoryRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.pagination = PageRequest.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllWalletHistoryRequest {
    return { pagination: isSet(object.pagination) ? PageRequest.fromJSON(object.pagination) : undefined };
  },

  toJSON(message: QueryAllWalletHistoryRequest): unknown {
    const obj: any = {};
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageRequest.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryAllWalletHistoryRequest>, I>>(object: I): QueryAllWalletHistoryRequest {
    const message = createBaseQueryAllWalletHistoryRequest();
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageRequest.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

function createBaseQueryAllWalletHistoryResponse(): QueryAllWalletHistoryResponse {
  return { WalletHistory: [], pagination: undefined };
}

export const QueryAllWalletHistoryResponse = {
  encode(message: QueryAllWalletHistoryResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.WalletHistory) {
      WalletHistory.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.pagination !== undefined) {
      PageResponse.encode(message.pagination, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryAllWalletHistoryResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryAllWalletHistoryResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.WalletHistory.push(WalletHistory.decode(reader, reader.uint32()));
          break;
        case 2:
          message.pagination = PageResponse.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllWalletHistoryResponse {
    return {
      WalletHistory: Array.isArray(object?.WalletHistory)
        ? object.WalletHistory.map((e: any) => WalletHistory.fromJSON(e))
        : [],
      pagination: isSet(object.pagination) ? PageResponse.fromJSON(object.pagination) : undefined,
    };
  },

  toJSON(message: QueryAllWalletHistoryResponse): unknown {
    const obj: any = {};
    if (message.WalletHistory) {
      obj.WalletHistory = message.WalletHistory.map((e) => e ? WalletHistory.toJSON(e) : undefined);
    } else {
      obj.WalletHistory = [];
    }
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageResponse.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryAllWalletHistoryResponse>, I>>(
    object: I,
  ): QueryAllWalletHistoryResponse {
    const message = createBaseQueryAllWalletHistoryResponse();
    message.WalletHistory = object.WalletHistory?.map((e) => WalletHistory.fromPartial(e)) || [];
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageResponse.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

function createBaseQueryGetWalletRequest(): QueryGetWalletRequest {
  return { id: "" };
}

export const QueryGetWalletRequest = {
  encode(message: QueryGetWalletRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.id !== "") {
      writer.uint32(10).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryGetWalletRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryGetWalletRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetWalletRequest {
    return { id: isSet(object.id) ? String(object.id) : "" };
  },

  toJSON(message: QueryGetWalletRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryGetWalletRequest>, I>>(object: I): QueryGetWalletRequest {
    const message = createBaseQueryGetWalletRequest();
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseQueryGetWalletResponse(): QueryGetWalletResponse {
  return { Wallet: undefined };
}

export const QueryGetWalletResponse = {
  encode(message: QueryGetWalletResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.Wallet !== undefined) {
      Wallet.encode(message.Wallet, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryGetWalletResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryGetWalletResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.Wallet = Wallet.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetWalletResponse {
    return { Wallet: isSet(object.Wallet) ? Wallet.fromJSON(object.Wallet) : undefined };
  },

  toJSON(message: QueryGetWalletResponse): unknown {
    const obj: any = {};
    message.Wallet !== undefined && (obj.Wallet = message.Wallet ? Wallet.toJSON(message.Wallet) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryGetWalletResponse>, I>>(object: I): QueryGetWalletResponse {
    const message = createBaseQueryGetWalletResponse();
    message.Wallet = (object.Wallet !== undefined && object.Wallet !== null)
      ? Wallet.fromPartial(object.Wallet)
      : undefined;
    return message;
  },
};

function createBaseQueryAllWalletRequest(): QueryAllWalletRequest {
  return { pagination: undefined };
}

export const QueryAllWalletRequest = {
  encode(message: QueryAllWalletRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.pagination !== undefined) {
      PageRequest.encode(message.pagination, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryAllWalletRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryAllWalletRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.pagination = PageRequest.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllWalletRequest {
    return { pagination: isSet(object.pagination) ? PageRequest.fromJSON(object.pagination) : undefined };
  },

  toJSON(message: QueryAllWalletRequest): unknown {
    const obj: any = {};
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageRequest.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryAllWalletRequest>, I>>(object: I): QueryAllWalletRequest {
    const message = createBaseQueryAllWalletRequest();
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageRequest.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

function createBaseQueryAllWalletResponse(): QueryAllWalletResponse {
  return { Wallet: [], pagination: undefined };
}

export const QueryAllWalletResponse = {
  encode(message: QueryAllWalletResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.Wallet) {
      Wallet.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.pagination !== undefined) {
      PageResponse.encode(message.pagination, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryAllWalletResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryAllWalletResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.Wallet.push(Wallet.decode(reader, reader.uint32()));
          break;
        case 2:
          message.pagination = PageResponse.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllWalletResponse {
    return {
      Wallet: Array.isArray(object?.Wallet) ? object.Wallet.map((e: any) => Wallet.fromJSON(e)) : [],
      pagination: isSet(object.pagination) ? PageResponse.fromJSON(object.pagination) : undefined,
    };
  },

  toJSON(message: QueryAllWalletResponse): unknown {
    const obj: any = {};
    if (message.Wallet) {
      obj.Wallet = message.Wallet.map((e) => e ? Wallet.toJSON(e) : undefined);
    } else {
      obj.Wallet = [];
    }
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageResponse.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryAllWalletResponse>, I>>(object: I): QueryAllWalletResponse {
    const message = createBaseQueryAllWalletResponse();
    message.Wallet = object.Wallet?.map((e) => Wallet.fromPartial(e)) || [];
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageResponse.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

/** Query defines the gRPC querier service. */
export interface Query {
  /** this line is used by starport scaffolding # 2 */
  TokenHistoryGlobal(request: QueryGetTokenHistoryGlobalRequest): Promise<QueryGetTokenHistoryGlobalResponse>;
  /** option (google.api.http).get = "/silicon-economy/base/blockchainbroker/digital-folder/tokenmanager/Wallet/tokenHistoryGlobal"; */
  TokenHistoryGlobalAll(request: QueryAllTokenHistoryGlobalRequest): Promise<QueryAllTokenHistoryGlobalResponse>;
  /** option (google.api.http).get = "/silicon-economy/base/blockchainbroker/digital-folder/tokenmanager/Wallet/segmentHistory/{id}"; */
  SegmentHistory(request: QueryGetSegmentHistoryRequest): Promise<QueryGetSegmentHistoryResponse>;
  /** option (google.api.http).get = "/silicon-economy/base/blockchainbroker/digital-folder/tokenmanager/Wallet/segmentHistory"; */
  SegmentHistoryAll(request: QueryAllSegmentHistoryRequest): Promise<QueryAllSegmentHistoryResponse>;
  /** option (google.api.http).get = "/silicon-economy/base/blockchainbroker/digital-folder/tokenmanager/Wallet/segment/{id}"; */
  Segment(request: QueryGetSegmentRequest): Promise<QueryGetSegmentResponse>;
  /** option (google.api.http).get = "/silicon-economy/base/blockchainbroker/digital-folder/tokenmanager/Wallet/segment"; */
  SegmentAll(request: QueryAllSegmentRequest): Promise<QueryAllSegmentResponse>;
  /** option (google.api.http).get = "/silicon-economy/base/blockchainbroker/digital-folder/tokenmanager/Wallet/walletHistory/{id}"; */
  WalletHistory(request: QueryGetWalletHistoryRequest): Promise<QueryGetWalletHistoryResponse>;
  /** option (google.api.http).get = "/silicon-economy/base/blockchainbroker/digital-folder/tokenmanager/Wallet/walletHistory"; */
  WalletHistoryAll(request: QueryAllWalletHistoryRequest): Promise<QueryAllWalletHistoryResponse>;
  /** option (google.api.http).get = "/silicon-economy/base/blockchainbroker/digital-folder/tokenmanager/Wallet/wallet/{id}"; */
  Wallet(request: QueryGetWalletRequest): Promise<QueryGetWalletResponse>;
  /** option (google.api.http).get = "/silicon-economy/base/blockchainbroker/digital-folder/tokenmanager/Wallet/wallet"; */
  WalletAll(request: QueryAllWalletRequest): Promise<QueryAllWalletResponse>;
}

export class QueryClientImpl implements Query {
  private readonly rpc: Rpc;
  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.TokenHistoryGlobal = this.TokenHistoryGlobal.bind(this);
    this.TokenHistoryGlobalAll = this.TokenHistoryGlobalAll.bind(this);
    this.SegmentHistory = this.SegmentHistory.bind(this);
    this.SegmentHistoryAll = this.SegmentHistoryAll.bind(this);
    this.Segment = this.Segment.bind(this);
    this.SegmentAll = this.SegmentAll.bind(this);
    this.WalletHistory = this.WalletHistory.bind(this);
    this.WalletHistoryAll = this.WalletHistoryAll.bind(this);
    this.Wallet = this.Wallet.bind(this);
    this.WalletAll = this.WalletAll.bind(this);
  }
  TokenHistoryGlobal(request: QueryGetTokenHistoryGlobalRequest): Promise<QueryGetTokenHistoryGlobalResponse> {
    const data = QueryGetTokenHistoryGlobalRequest.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Query",
      "TokenHistoryGlobal",
      data,
    );
    return promise.then((data) => QueryGetTokenHistoryGlobalResponse.decode(new _m0.Reader(data)));
  }

  TokenHistoryGlobalAll(request: QueryAllTokenHistoryGlobalRequest): Promise<QueryAllTokenHistoryGlobalResponse> {
    const data = QueryAllTokenHistoryGlobalRequest.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Query",
      "TokenHistoryGlobalAll",
      data,
    );
    return promise.then((data) => QueryAllTokenHistoryGlobalResponse.decode(new _m0.Reader(data)));
  }

  SegmentHistory(request: QueryGetSegmentHistoryRequest): Promise<QueryGetSegmentHistoryResponse> {
    const data = QueryGetSegmentHistoryRequest.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Query",
      "SegmentHistory",
      data,
    );
    return promise.then((data) => QueryGetSegmentHistoryResponse.decode(new _m0.Reader(data)));
  }

  SegmentHistoryAll(request: QueryAllSegmentHistoryRequest): Promise<QueryAllSegmentHistoryResponse> {
    const data = QueryAllSegmentHistoryRequest.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Query",
      "SegmentHistoryAll",
      data,
    );
    return promise.then((data) => QueryAllSegmentHistoryResponse.decode(new _m0.Reader(data)));
  }

  Segment(request: QueryGetSegmentRequest): Promise<QueryGetSegmentResponse> {
    const data = QueryGetSegmentRequest.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Query",
      "Segment",
      data,
    );
    return promise.then((data) => QueryGetSegmentResponse.decode(new _m0.Reader(data)));
  }

  SegmentAll(request: QueryAllSegmentRequest): Promise<QueryAllSegmentResponse> {
    const data = QueryAllSegmentRequest.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Query",
      "SegmentAll",
      data,
    );
    return promise.then((data) => QueryAllSegmentResponse.decode(new _m0.Reader(data)));
  }

  WalletHistory(request: QueryGetWalletHistoryRequest): Promise<QueryGetWalletHistoryResponse> {
    const data = QueryGetWalletHistoryRequest.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Query",
      "WalletHistory",
      data,
    );
    return promise.then((data) => QueryGetWalletHistoryResponse.decode(new _m0.Reader(data)));
  }

  WalletHistoryAll(request: QueryAllWalletHistoryRequest): Promise<QueryAllWalletHistoryResponse> {
    const data = QueryAllWalletHistoryRequest.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Query",
      "WalletHistoryAll",
      data,
    );
    return promise.then((data) => QueryAllWalletHistoryResponse.decode(new _m0.Reader(data)));
  }

  Wallet(request: QueryGetWalletRequest): Promise<QueryGetWalletResponse> {
    const data = QueryGetWalletRequest.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Query",
      "Wallet",
      data,
    );
    return promise.then((data) => QueryGetWalletResponse.decode(new _m0.Reader(data)));
  }

  WalletAll(request: QueryAllWalletRequest): Promise<QueryAllWalletResponse> {
    const data = QueryAllWalletRequest.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Query",
      "WalletAll",
      data,
    );
    return promise.then((data) => QueryAllWalletResponse.decode(new _m0.Reader(data)));
  }
}

interface Rpc {
  request(service: string, method: string, data: Uint8Array): Promise<Uint8Array>;
}

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

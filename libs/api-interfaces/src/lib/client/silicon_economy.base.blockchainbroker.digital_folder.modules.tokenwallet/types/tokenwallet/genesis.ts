/* eslint-disable */
import _m0 from "protobufjs/minimal";
import { Segment } from "./segment";
import { SegmentHistory } from "./segmentHistory";
import { TokenHistoryGlobal } from "./tokenHistoryGlobal";
import { Wallet } from "./wallet";
import { WalletHistory } from "./walletHistory";

export const protobufPackage = "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet";

/** GenesisState defines the Wallet module's genesis state. */
export interface GenesisState {
  /** this line is used by starport scaffolding # genesis/proto/state */
  tokenHistoryGlobalList: TokenHistoryGlobal[];
  /** this line is used by starport scaffolding # genesis/proto/stateField */
  segmentHistoryList: SegmentHistory[];
  /** this line is used by starport scaffolding # genesis/proto/stateField */
  segmentList: Segment[];
  /** this line is used by starport scaffolding # genesis/proto/stateField */
  walletHistoryList: WalletHistory[];
  /** this line is used by starport scaffolding # genesis/proto/stateField */
  walletList: Wallet[];
}

function createBaseGenesisState(): GenesisState {
  return { tokenHistoryGlobalList: [], segmentHistoryList: [], segmentList: [], walletHistoryList: [], walletList: [] };
}

export const GenesisState = {
  encode(message: GenesisState, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.tokenHistoryGlobalList) {
      TokenHistoryGlobal.encode(v!, writer.uint32(42).fork()).ldelim();
    }
    for (const v of message.segmentHistoryList) {
      SegmentHistory.encode(v!, writer.uint32(34).fork()).ldelim();
    }
    for (const v of message.segmentList) {
      Segment.encode(v!, writer.uint32(26).fork()).ldelim();
    }
    for (const v of message.walletHistoryList) {
      WalletHistory.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    for (const v of message.walletList) {
      Wallet.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): GenesisState {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseGenesisState();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 5:
          message.tokenHistoryGlobalList.push(TokenHistoryGlobal.decode(reader, reader.uint32()));
          break;
        case 4:
          message.segmentHistoryList.push(SegmentHistory.decode(reader, reader.uint32()));
          break;
        case 3:
          message.segmentList.push(Segment.decode(reader, reader.uint32()));
          break;
        case 2:
          message.walletHistoryList.push(WalletHistory.decode(reader, reader.uint32()));
          break;
        case 1:
          message.walletList.push(Wallet.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GenesisState {
    return {
      tokenHistoryGlobalList: Array.isArray(object?.tokenHistoryGlobalList)
        ? object.tokenHistoryGlobalList.map((e: any) => TokenHistoryGlobal.fromJSON(e))
        : [],
      segmentHistoryList: Array.isArray(object?.segmentHistoryList)
        ? object.segmentHistoryList.map((e: any) => SegmentHistory.fromJSON(e))
        : [],
      segmentList: Array.isArray(object?.segmentList) ? object.segmentList.map((e: any) => Segment.fromJSON(e)) : [],
      walletHistoryList: Array.isArray(object?.walletHistoryList)
        ? object.walletHistoryList.map((e: any) => WalletHistory.fromJSON(e))
        : [],
      walletList: Array.isArray(object?.walletList) ? object.walletList.map((e: any) => Wallet.fromJSON(e)) : [],
    };
  },

  toJSON(message: GenesisState): unknown {
    const obj: any = {};
    if (message.tokenHistoryGlobalList) {
      obj.tokenHistoryGlobalList = message.tokenHistoryGlobalList.map((e) =>
        e ? TokenHistoryGlobal.toJSON(e) : undefined
      );
    } else {
      obj.tokenHistoryGlobalList = [];
    }
    if (message.segmentHistoryList) {
      obj.segmentHistoryList = message.segmentHistoryList.map((e) => e ? SegmentHistory.toJSON(e) : undefined);
    } else {
      obj.segmentHistoryList = [];
    }
    if (message.segmentList) {
      obj.segmentList = message.segmentList.map((e) => e ? Segment.toJSON(e) : undefined);
    } else {
      obj.segmentList = [];
    }
    if (message.walletHistoryList) {
      obj.walletHistoryList = message.walletHistoryList.map((e) => e ? WalletHistory.toJSON(e) : undefined);
    } else {
      obj.walletHistoryList = [];
    }
    if (message.walletList) {
      obj.walletList = message.walletList.map((e) => e ? Wallet.toJSON(e) : undefined);
    } else {
      obj.walletList = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<GenesisState>, I>>(object: I): GenesisState {
    const message = createBaseGenesisState();
    message.tokenHistoryGlobalList = object.tokenHistoryGlobalList?.map((e) => TokenHistoryGlobal.fromPartial(e)) || [];
    message.segmentHistoryList = object.segmentHistoryList?.map((e) => SegmentHistory.fromPartial(e)) || [];
    message.segmentList = object.segmentList?.map((e) => Segment.fromPartial(e)) || [];
    message.walletHistoryList = object.walletHistoryList?.map((e) => WalletHistory.fromPartial(e)) || [];
    message.walletList = object.walletList?.map((e) => Wallet.fromPartial(e)) || [];
    return message;
  },
};

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

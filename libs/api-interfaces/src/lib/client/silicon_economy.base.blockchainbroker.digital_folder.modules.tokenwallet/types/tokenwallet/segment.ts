/* eslint-disable */
import _m0 from "protobufjs/minimal";

export const protobufPackage = "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet";

export interface Segment {
  creator: string;
  id: string;
  name: string;
  timestamp: string;
  info: string;
  walletId: string;
  tokenRefs: TokenRef[];
}

export interface TokenRef {
  id: string;
  moduleRef: string;
  valid: boolean;
}

function createBaseSegment(): Segment {
  return { creator: "", id: "", name: "", timestamp: "", info: "", walletId: "", tokenRefs: [] };
}

export const Segment = {
  encode(message: Segment, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.name !== "") {
      writer.uint32(26).string(message.name);
    }
    if (message.timestamp !== "") {
      writer.uint32(34).string(message.timestamp);
    }
    if (message.info !== "") {
      writer.uint32(42).string(message.info);
    }
    if (message.walletId !== "") {
      writer.uint32(50).string(message.walletId);
    }
    for (const v of message.tokenRefs) {
      TokenRef.encode(v!, writer.uint32(58).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Segment {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseSegment();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.name = reader.string();
          break;
        case 4:
          message.timestamp = reader.string();
          break;
        case 5:
          message.info = reader.string();
          break;
        case 6:
          message.walletId = reader.string();
          break;
        case 7:
          message.tokenRefs.push(TokenRef.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Segment {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      name: isSet(object.name) ? String(object.name) : "",
      timestamp: isSet(object.timestamp) ? String(object.timestamp) : "",
      info: isSet(object.info) ? String(object.info) : "",
      walletId: isSet(object.walletId) ? String(object.walletId) : "",
      tokenRefs: Array.isArray(object?.tokenRefs) ? object.tokenRefs.map((e: any) => TokenRef.fromJSON(e)) : [],
    };
  },

  toJSON(message: Segment): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.name !== undefined && (obj.name = message.name);
    message.timestamp !== undefined && (obj.timestamp = message.timestamp);
    message.info !== undefined && (obj.info = message.info);
    message.walletId !== undefined && (obj.walletId = message.walletId);
    if (message.tokenRefs) {
      obj.tokenRefs = message.tokenRefs.map((e) => e ? TokenRef.toJSON(e) : undefined);
    } else {
      obj.tokenRefs = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<Segment>, I>>(object: I): Segment {
    const message = createBaseSegment();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.name = object.name ?? "";
    message.timestamp = object.timestamp ?? "";
    message.info = object.info ?? "";
    message.walletId = object.walletId ?? "";
    message.tokenRefs = object.tokenRefs?.map((e) => TokenRef.fromPartial(e)) || [];
    return message;
  },
};

function createBaseTokenRef(): TokenRef {
  return { id: "", moduleRef: "", valid: false };
}

export const TokenRef = {
  encode(message: TokenRef, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.id !== "") {
      writer.uint32(10).string(message.id);
    }
    if (message.moduleRef !== "") {
      writer.uint32(18).string(message.moduleRef);
    }
    if (message.valid === true) {
      writer.uint32(24).bool(message.valid);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): TokenRef {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseTokenRef();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        case 2:
          message.moduleRef = reader.string();
          break;
        case 3:
          message.valid = reader.bool();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): TokenRef {
    return {
      id: isSet(object.id) ? String(object.id) : "",
      moduleRef: isSet(object.moduleRef) ? String(object.moduleRef) : "",
      valid: isSet(object.valid) ? Boolean(object.valid) : false,
    };
  },

  toJSON(message: TokenRef): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.moduleRef !== undefined && (obj.moduleRef = message.moduleRef);
    message.valid !== undefined && (obj.valid = message.valid);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<TokenRef>, I>>(object: I): TokenRef {
    const message = createBaseTokenRef();
    message.id = object.id ?? "";
    message.moduleRef = object.moduleRef ?? "";
    message.valid = object.valid ?? false;
    return message;
  },
};

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

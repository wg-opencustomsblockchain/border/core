import { GeneratedType } from "@cosmjs/proto-signing";
import { MsgCreateSegmentHistory } from "./types/tokenwallet/tx";
import { MsgFetchAllTokenHistoryGlobal } from "./types/tokenwallet/tx";
import { MsgMoveTokenToSegment } from "./types/tokenwallet/tx";
import { MsgUpdateSegmentHistory } from "./types/tokenwallet/tx";
import { MsgFetchGetWalletHistory } from "./types/tokenwallet/tx";
import { MsgUpdateWallet } from "./types/tokenwallet/tx";
import { MsgRemoveTokenRefFromSegment } from "./types/tokenwallet/tx";
import { MsgCreateSegment } from "./types/tokenwallet/tx";
import { MsgFetchAllSegmentHistory } from "./types/tokenwallet/tx";
import { MsgFetchAllWalletHistory } from "./types/tokenwallet/tx";
import { MsgUpdateTokenHistoryGlobal } from "./types/tokenwallet/tx";
import { MsgCreateWalletHistory } from "./types/tokenwallet/tx";
import { MsgFetchGetTokenHistoryGlobal } from "./types/tokenwallet/tx";
import { MsgFetchAllWallet } from "./types/tokenwallet/tx";
import { MsgUpdateWalletHistory } from "./types/tokenwallet/tx";
import { MsgCreateWalletWithId } from "./types/tokenwallet/tx";
import { MsgFetchAllSegment } from "./types/tokenwallet/tx";
import { MsgMoveTokenToWallet } from "./types/tokenwallet/tx";
import { MsgCreateTokenRef } from "./types/tokenwallet/tx";
import { MsgFetchGetSegment } from "./types/tokenwallet/tx";
import { MsgCreateTokenHistoryGlobal } from "./types/tokenwallet/tx";
import { MsgCreateWallet } from "./types/tokenwallet/tx";
import { MsgAssignCosmosAddressToWallet } from "./types/tokenwallet/tx";
import { MsgCreateSegmentWithId } from "./types/tokenwallet/tx";
import { MsgUpdateSegment } from "./types/tokenwallet/tx";
import { MsgFetchGetWallet } from "./types/tokenwallet/tx";
import { MsgFetchGetSegmentHistory } from "./types/tokenwallet/tx";

const msgTypes: Array<[string, GeneratedType]>  = [
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.MsgCreateSegmentHistory", MsgCreateSegmentHistory],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.MsgFetchAllTokenHistoryGlobal", MsgFetchAllTokenHistoryGlobal],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.MsgMoveTokenToSegment", MsgMoveTokenToSegment],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.MsgUpdateSegmentHistory", MsgUpdateSegmentHistory],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.MsgFetchGetWalletHistory", MsgFetchGetWalletHistory],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.MsgUpdateWallet", MsgUpdateWallet],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.MsgRemoveTokenRefFromSegment", MsgRemoveTokenRefFromSegment],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.MsgCreateSegment", MsgCreateSegment],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.MsgFetchAllSegmentHistory", MsgFetchAllSegmentHistory],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.MsgFetchAllWalletHistory", MsgFetchAllWalletHistory],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.MsgUpdateTokenHistoryGlobal", MsgUpdateTokenHistoryGlobal],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.MsgCreateWalletHistory", MsgCreateWalletHistory],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.MsgFetchGetTokenHistoryGlobal", MsgFetchGetTokenHistoryGlobal],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.MsgFetchAllWallet", MsgFetchAllWallet],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.MsgUpdateWalletHistory", MsgUpdateWalletHistory],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.MsgCreateWalletWithId", MsgCreateWalletWithId],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.MsgFetchAllSegment", MsgFetchAllSegment],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.MsgMoveTokenToWallet", MsgMoveTokenToWallet],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.MsgCreateTokenRef", MsgCreateTokenRef],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.MsgFetchGetSegment", MsgFetchGetSegment],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.MsgCreateTokenHistoryGlobal", MsgCreateTokenHistoryGlobal],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.MsgCreateWallet", MsgCreateWallet],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.MsgAssignCosmosAddressToWallet", MsgAssignCosmosAddressToWallet],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.MsgCreateSegmentWithId", MsgCreateSegmentWithId],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.MsgUpdateSegment", MsgUpdateSegment],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.MsgFetchGetWallet", MsgFetchGetWallet],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.MsgFetchGetSegmentHistory", MsgFetchGetSegmentHistory],
    
];

export { msgTypes }
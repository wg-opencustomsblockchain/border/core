/* eslint-disable */
import Long from "long";
import _m0 from "protobufjs/minimal";
import { ApplicationRole } from "./applicationRole";
import { BlockchainAccount } from "./blockchainAccount";

export const protobufPackage = "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization";

/** this line is used by starport scaffolding # proto/tx/message */
export interface MsgCreateConfiguration {
  creator: string;
  PermissionCheck: boolean;
}

export interface MsgCreateConfigurationResponse {
  id: number;
}

export interface MsgUpdateConfiguration {
  creator: string;
  id: number;
  PermissionCheck: boolean;
}

export interface MsgUpdateConfigurationResponse {
}

export interface MsgDeleteConfiguration {
  creator: string;
  id: number;
}

export interface MsgDeleteConfigurationResponse {
}

export interface MsgCreateBlockchainAccountState {
  creator: string;
  id: string;
  accountID: string;
  timeStamp: string;
  applicationRoleIDs: string[];
  valid: boolean;
}

export interface MsgCreateBlockchainAccountStateResponse {
  id: string;
}

export interface MsgUpdateBlockchainAccountState {
  creator: string;
  id: string;
  accountID: string;
  timeStamp: string;
  applicationRoleIDs: string[];
  valid: boolean;
}

export interface MsgUpdateBlockchainAccountStateResponse {
}

export interface MsgDeleteBlockchainAccountState {
  creator: string;
  id: string;
}

export interface MsgDeleteBlockchainAccountStateResponse {
}

export interface MsgCreateApplicationRoleState {
  creator: string;
  id: string;
  applicationRoleID: string;
  description: string;
  valid: boolean;
  timeStamp: string;
}

export interface MsgCreateApplicationRoleStateResponse {
  id: string;
}

export interface MsgUpdateApplicationRoleState {
  creator: string;
  id: string;
  applicationRoleID: string;
  description: string;
  valid: boolean;
  timeStamp: string;
}

export interface MsgUpdateApplicationRoleStateResponse {
}

export interface MsgDeleteApplicationRoleState {
  creator: string;
  id: string;
}

export interface MsgDeleteApplicationRoleStateResponse {
}

export interface MsgCreateBlockchainAccount {
  creator: string;
  id: string;
}

export interface MsgCreateBlockchainAccountResponse {
  id: string;
}

export interface MsgDeactivateBlockchainAccount {
  creator: string;
  id: string;
}

export interface MsgDeleteBlockchainAccountResponse {
}

export interface MsgCreateApplicationRole {
  creator: string;
  id: string;
  description: string;
}

export interface MsgCreateApplicationRoleResponse {
  id: string;
}

export interface MsgUpdateApplicationRole {
  creator: string;
  id: string;
  description: string;
}

export interface MsgUpdateApplicationRoleResponse {
}

export interface MsgDeleteApplicationRole {
  creator: string;
  id: string;
}

export interface MsgDeleteApplicationRoleResponse {
}

export interface MsgDeactivateApplicationRole {
  creator: string;
  id: string;
}

export interface MsgDeactivateApplicationRoleResponse {
}

export interface MsgDeactivateBlockchainAccountResponse {
}

export interface MsgGrantAppRoleToBlockchainAccount {
  creator: string;
  user: string;
  roleID: string;
}

export interface MsgGrantAppRoleToBlockchainAccountResponse {
}

export interface MsgRevokeAppRoleFromBlockchainAccount {
  creator: string;
  account: string;
  roleID: string;
}

export interface MsgRevokeAppRoleFromBlockchainAccountResponse {
}

export interface MsgFetchAllApplicationRole {
  creator: string;
}

export interface MsgFetchAllApplicationRoleResponse {
  role: ApplicationRole[];
}

export interface MsgFetchApplicationRole {
  creator: string;
  id: string;
}

export interface MsgFetchApplicationRoleResponse {
  role: ApplicationRole | undefined;
}

export interface MsgFetchAllBlockchainAccount {
  creator: string;
}

export interface MsgFetchAllBlockchainAccountResponse {
  account: BlockchainAccount[];
}

export interface MsgFetchBlockchainAccount {
  creator: string;
  id: string;
}

export interface MsgFetchBlockchainAccountResponse {
  account: BlockchainAccount | undefined;
}

function createBaseMsgCreateConfiguration(): MsgCreateConfiguration {
  return { creator: "", PermissionCheck: false };
}

export const MsgCreateConfiguration = {
  encode(message: MsgCreateConfiguration, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.PermissionCheck === true) {
      writer.uint32(16).bool(message.PermissionCheck);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateConfiguration {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateConfiguration();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.PermissionCheck = reader.bool();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateConfiguration {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      PermissionCheck: isSet(object.PermissionCheck) ? Boolean(object.PermissionCheck) : false,
    };
  },

  toJSON(message: MsgCreateConfiguration): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.PermissionCheck !== undefined && (obj.PermissionCheck = message.PermissionCheck);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateConfiguration>, I>>(object: I): MsgCreateConfiguration {
    const message = createBaseMsgCreateConfiguration();
    message.creator = object.creator ?? "";
    message.PermissionCheck = object.PermissionCheck ?? false;
    return message;
  },
};

function createBaseMsgCreateConfigurationResponse(): MsgCreateConfigurationResponse {
  return { id: 0 };
}

export const MsgCreateConfigurationResponse = {
  encode(message: MsgCreateConfigurationResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).uint64(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateConfigurationResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateConfigurationResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = longToNumber(reader.uint64() as Long);
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateConfigurationResponse {
    return { id: isSet(object.id) ? Number(object.id) : 0 };
  },

  toJSON(message: MsgCreateConfigurationResponse): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateConfigurationResponse>, I>>(
    object: I,
  ): MsgCreateConfigurationResponse {
    const message = createBaseMsgCreateConfigurationResponse();
    message.id = object.id ?? 0;
    return message;
  },
};

function createBaseMsgUpdateConfiguration(): MsgUpdateConfiguration {
  return { creator: "", id: 0, PermissionCheck: false };
}

export const MsgUpdateConfiguration = {
  encode(message: MsgUpdateConfiguration, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== 0) {
      writer.uint32(16).uint64(message.id);
    }
    if (message.PermissionCheck === true) {
      writer.uint32(24).bool(message.PermissionCheck);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgUpdateConfiguration {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgUpdateConfiguration();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = longToNumber(reader.uint64() as Long);
          break;
        case 3:
          message.PermissionCheck = reader.bool();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateConfiguration {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? Number(object.id) : 0,
      PermissionCheck: isSet(object.PermissionCheck) ? Boolean(object.PermissionCheck) : false,
    };
  },

  toJSON(message: MsgUpdateConfiguration): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.PermissionCheck !== undefined && (obj.PermissionCheck = message.PermissionCheck);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgUpdateConfiguration>, I>>(object: I): MsgUpdateConfiguration {
    const message = createBaseMsgUpdateConfiguration();
    message.creator = object.creator ?? "";
    message.id = object.id ?? 0;
    message.PermissionCheck = object.PermissionCheck ?? false;
    return message;
  },
};

function createBaseMsgUpdateConfigurationResponse(): MsgUpdateConfigurationResponse {
  return {};
}

export const MsgUpdateConfigurationResponse = {
  encode(_: MsgUpdateConfigurationResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgUpdateConfigurationResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgUpdateConfigurationResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgUpdateConfigurationResponse {
    return {};
  },

  toJSON(_: MsgUpdateConfigurationResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgUpdateConfigurationResponse>, I>>(_: I): MsgUpdateConfigurationResponse {
    const message = createBaseMsgUpdateConfigurationResponse();
    return message;
  },
};

function createBaseMsgDeleteConfiguration(): MsgDeleteConfiguration {
  return { creator: "", id: 0 };
}

export const MsgDeleteConfiguration = {
  encode(message: MsgDeleteConfiguration, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== 0) {
      writer.uint32(16).uint64(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgDeleteConfiguration {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgDeleteConfiguration();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = longToNumber(reader.uint64() as Long);
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgDeleteConfiguration {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? Number(object.id) : 0,
    };
  },

  toJSON(message: MsgDeleteConfiguration): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = Math.round(message.id));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgDeleteConfiguration>, I>>(object: I): MsgDeleteConfiguration {
    const message = createBaseMsgDeleteConfiguration();
    message.creator = object.creator ?? "";
    message.id = object.id ?? 0;
    return message;
  },
};

function createBaseMsgDeleteConfigurationResponse(): MsgDeleteConfigurationResponse {
  return {};
}

export const MsgDeleteConfigurationResponse = {
  encode(_: MsgDeleteConfigurationResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgDeleteConfigurationResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgDeleteConfigurationResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgDeleteConfigurationResponse {
    return {};
  },

  toJSON(_: MsgDeleteConfigurationResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgDeleteConfigurationResponse>, I>>(_: I): MsgDeleteConfigurationResponse {
    const message = createBaseMsgDeleteConfigurationResponse();
    return message;
  },
};

function createBaseMsgCreateBlockchainAccountState(): MsgCreateBlockchainAccountState {
  return { creator: "", id: "", accountID: "", timeStamp: "", applicationRoleIDs: [], valid: false };
}

export const MsgCreateBlockchainAccountState = {
  encode(message: MsgCreateBlockchainAccountState, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.accountID !== "") {
      writer.uint32(26).string(message.accountID);
    }
    if (message.timeStamp !== "") {
      writer.uint32(34).string(message.timeStamp);
    }
    for (const v of message.applicationRoleIDs) {
      writer.uint32(42).string(v!);
    }
    if (message.valid === true) {
      writer.uint32(48).bool(message.valid);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateBlockchainAccountState {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateBlockchainAccountState();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.accountID = reader.string();
          break;
        case 4:
          message.timeStamp = reader.string();
          break;
        case 5:
          message.applicationRoleIDs.push(reader.string());
          break;
        case 6:
          message.valid = reader.bool();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateBlockchainAccountState {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      accountID: isSet(object.accountID) ? String(object.accountID) : "",
      timeStamp: isSet(object.timeStamp) ? String(object.timeStamp) : "",
      applicationRoleIDs: Array.isArray(object?.applicationRoleIDs)
        ? object.applicationRoleIDs.map((e: any) => String(e))
        : [],
      valid: isSet(object.valid) ? Boolean(object.valid) : false,
    };
  },

  toJSON(message: MsgCreateBlockchainAccountState): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.accountID !== undefined && (obj.accountID = message.accountID);
    message.timeStamp !== undefined && (obj.timeStamp = message.timeStamp);
    if (message.applicationRoleIDs) {
      obj.applicationRoleIDs = message.applicationRoleIDs.map((e) => e);
    } else {
      obj.applicationRoleIDs = [];
    }
    message.valid !== undefined && (obj.valid = message.valid);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateBlockchainAccountState>, I>>(
    object: I,
  ): MsgCreateBlockchainAccountState {
    const message = createBaseMsgCreateBlockchainAccountState();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.accountID = object.accountID ?? "";
    message.timeStamp = object.timeStamp ?? "";
    message.applicationRoleIDs = object.applicationRoleIDs?.map((e) => e) || [];
    message.valid = object.valid ?? false;
    return message;
  },
};

function createBaseMsgCreateBlockchainAccountStateResponse(): MsgCreateBlockchainAccountStateResponse {
  return { id: "" };
}

export const MsgCreateBlockchainAccountStateResponse = {
  encode(message: MsgCreateBlockchainAccountStateResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.id !== "") {
      writer.uint32(10).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateBlockchainAccountStateResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateBlockchainAccountStateResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateBlockchainAccountStateResponse {
    return { id: isSet(object.id) ? String(object.id) : "" };
  },

  toJSON(message: MsgCreateBlockchainAccountStateResponse): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateBlockchainAccountStateResponse>, I>>(
    object: I,
  ): MsgCreateBlockchainAccountStateResponse {
    const message = createBaseMsgCreateBlockchainAccountStateResponse();
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgUpdateBlockchainAccountState(): MsgUpdateBlockchainAccountState {
  return { creator: "", id: "", accountID: "", timeStamp: "", applicationRoleIDs: [], valid: false };
}

export const MsgUpdateBlockchainAccountState = {
  encode(message: MsgUpdateBlockchainAccountState, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.accountID !== "") {
      writer.uint32(26).string(message.accountID);
    }
    if (message.timeStamp !== "") {
      writer.uint32(34).string(message.timeStamp);
    }
    for (const v of message.applicationRoleIDs) {
      writer.uint32(42).string(v!);
    }
    if (message.valid === true) {
      writer.uint32(48).bool(message.valid);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgUpdateBlockchainAccountState {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgUpdateBlockchainAccountState();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.accountID = reader.string();
          break;
        case 4:
          message.timeStamp = reader.string();
          break;
        case 5:
          message.applicationRoleIDs.push(reader.string());
          break;
        case 6:
          message.valid = reader.bool();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateBlockchainAccountState {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      accountID: isSet(object.accountID) ? String(object.accountID) : "",
      timeStamp: isSet(object.timeStamp) ? String(object.timeStamp) : "",
      applicationRoleIDs: Array.isArray(object?.applicationRoleIDs)
        ? object.applicationRoleIDs.map((e: any) => String(e))
        : [],
      valid: isSet(object.valid) ? Boolean(object.valid) : false,
    };
  },

  toJSON(message: MsgUpdateBlockchainAccountState): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.accountID !== undefined && (obj.accountID = message.accountID);
    message.timeStamp !== undefined && (obj.timeStamp = message.timeStamp);
    if (message.applicationRoleIDs) {
      obj.applicationRoleIDs = message.applicationRoleIDs.map((e) => e);
    } else {
      obj.applicationRoleIDs = [];
    }
    message.valid !== undefined && (obj.valid = message.valid);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgUpdateBlockchainAccountState>, I>>(
    object: I,
  ): MsgUpdateBlockchainAccountState {
    const message = createBaseMsgUpdateBlockchainAccountState();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.accountID = object.accountID ?? "";
    message.timeStamp = object.timeStamp ?? "";
    message.applicationRoleIDs = object.applicationRoleIDs?.map((e) => e) || [];
    message.valid = object.valid ?? false;
    return message;
  },
};

function createBaseMsgUpdateBlockchainAccountStateResponse(): MsgUpdateBlockchainAccountStateResponse {
  return {};
}

export const MsgUpdateBlockchainAccountStateResponse = {
  encode(_: MsgUpdateBlockchainAccountStateResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgUpdateBlockchainAccountStateResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgUpdateBlockchainAccountStateResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgUpdateBlockchainAccountStateResponse {
    return {};
  },

  toJSON(_: MsgUpdateBlockchainAccountStateResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgUpdateBlockchainAccountStateResponse>, I>>(
    _: I,
  ): MsgUpdateBlockchainAccountStateResponse {
    const message = createBaseMsgUpdateBlockchainAccountStateResponse();
    return message;
  },
};

function createBaseMsgDeleteBlockchainAccountState(): MsgDeleteBlockchainAccountState {
  return { creator: "", id: "" };
}

export const MsgDeleteBlockchainAccountState = {
  encode(message: MsgDeleteBlockchainAccountState, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgDeleteBlockchainAccountState {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgDeleteBlockchainAccountState();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgDeleteBlockchainAccountState {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
    };
  },

  toJSON(message: MsgDeleteBlockchainAccountState): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgDeleteBlockchainAccountState>, I>>(
    object: I,
  ): MsgDeleteBlockchainAccountState {
    const message = createBaseMsgDeleteBlockchainAccountState();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgDeleteBlockchainAccountStateResponse(): MsgDeleteBlockchainAccountStateResponse {
  return {};
}

export const MsgDeleteBlockchainAccountStateResponse = {
  encode(_: MsgDeleteBlockchainAccountStateResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgDeleteBlockchainAccountStateResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgDeleteBlockchainAccountStateResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgDeleteBlockchainAccountStateResponse {
    return {};
  },

  toJSON(_: MsgDeleteBlockchainAccountStateResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgDeleteBlockchainAccountStateResponse>, I>>(
    _: I,
  ): MsgDeleteBlockchainAccountStateResponse {
    const message = createBaseMsgDeleteBlockchainAccountStateResponse();
    return message;
  },
};

function createBaseMsgCreateApplicationRoleState(): MsgCreateApplicationRoleState {
  return { creator: "", id: "", applicationRoleID: "", description: "", valid: false, timeStamp: "" };
}

export const MsgCreateApplicationRoleState = {
  encode(message: MsgCreateApplicationRoleState, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.applicationRoleID !== "") {
      writer.uint32(26).string(message.applicationRoleID);
    }
    if (message.description !== "") {
      writer.uint32(34).string(message.description);
    }
    if (message.valid === true) {
      writer.uint32(40).bool(message.valid);
    }
    if (message.timeStamp !== "") {
      writer.uint32(50).string(message.timeStamp);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateApplicationRoleState {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateApplicationRoleState();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.applicationRoleID = reader.string();
          break;
        case 4:
          message.description = reader.string();
          break;
        case 5:
          message.valid = reader.bool();
          break;
        case 6:
          message.timeStamp = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateApplicationRoleState {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      applicationRoleID: isSet(object.applicationRoleID) ? String(object.applicationRoleID) : "",
      description: isSet(object.description) ? String(object.description) : "",
      valid: isSet(object.valid) ? Boolean(object.valid) : false,
      timeStamp: isSet(object.timeStamp) ? String(object.timeStamp) : "",
    };
  },

  toJSON(message: MsgCreateApplicationRoleState): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.applicationRoleID !== undefined && (obj.applicationRoleID = message.applicationRoleID);
    message.description !== undefined && (obj.description = message.description);
    message.valid !== undefined && (obj.valid = message.valid);
    message.timeStamp !== undefined && (obj.timeStamp = message.timeStamp);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateApplicationRoleState>, I>>(
    object: I,
  ): MsgCreateApplicationRoleState {
    const message = createBaseMsgCreateApplicationRoleState();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.applicationRoleID = object.applicationRoleID ?? "";
    message.description = object.description ?? "";
    message.valid = object.valid ?? false;
    message.timeStamp = object.timeStamp ?? "";
    return message;
  },
};

function createBaseMsgCreateApplicationRoleStateResponse(): MsgCreateApplicationRoleStateResponse {
  return { id: "" };
}

export const MsgCreateApplicationRoleStateResponse = {
  encode(message: MsgCreateApplicationRoleStateResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.id !== "") {
      writer.uint32(10).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateApplicationRoleStateResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateApplicationRoleStateResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateApplicationRoleStateResponse {
    return { id: isSet(object.id) ? String(object.id) : "" };
  },

  toJSON(message: MsgCreateApplicationRoleStateResponse): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateApplicationRoleStateResponse>, I>>(
    object: I,
  ): MsgCreateApplicationRoleStateResponse {
    const message = createBaseMsgCreateApplicationRoleStateResponse();
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgUpdateApplicationRoleState(): MsgUpdateApplicationRoleState {
  return { creator: "", id: "", applicationRoleID: "", description: "", valid: false, timeStamp: "" };
}

export const MsgUpdateApplicationRoleState = {
  encode(message: MsgUpdateApplicationRoleState, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.applicationRoleID !== "") {
      writer.uint32(26).string(message.applicationRoleID);
    }
    if (message.description !== "") {
      writer.uint32(34).string(message.description);
    }
    if (message.valid === true) {
      writer.uint32(40).bool(message.valid);
    }
    if (message.timeStamp !== "") {
      writer.uint32(50).string(message.timeStamp);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgUpdateApplicationRoleState {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgUpdateApplicationRoleState();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.applicationRoleID = reader.string();
          break;
        case 4:
          message.description = reader.string();
          break;
        case 5:
          message.valid = reader.bool();
          break;
        case 6:
          message.timeStamp = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateApplicationRoleState {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      applicationRoleID: isSet(object.applicationRoleID) ? String(object.applicationRoleID) : "",
      description: isSet(object.description) ? String(object.description) : "",
      valid: isSet(object.valid) ? Boolean(object.valid) : false,
      timeStamp: isSet(object.timeStamp) ? String(object.timeStamp) : "",
    };
  },

  toJSON(message: MsgUpdateApplicationRoleState): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.applicationRoleID !== undefined && (obj.applicationRoleID = message.applicationRoleID);
    message.description !== undefined && (obj.description = message.description);
    message.valid !== undefined && (obj.valid = message.valid);
    message.timeStamp !== undefined && (obj.timeStamp = message.timeStamp);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgUpdateApplicationRoleState>, I>>(
    object: I,
  ): MsgUpdateApplicationRoleState {
    const message = createBaseMsgUpdateApplicationRoleState();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.applicationRoleID = object.applicationRoleID ?? "";
    message.description = object.description ?? "";
    message.valid = object.valid ?? false;
    message.timeStamp = object.timeStamp ?? "";
    return message;
  },
};

function createBaseMsgUpdateApplicationRoleStateResponse(): MsgUpdateApplicationRoleStateResponse {
  return {};
}

export const MsgUpdateApplicationRoleStateResponse = {
  encode(_: MsgUpdateApplicationRoleStateResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgUpdateApplicationRoleStateResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgUpdateApplicationRoleStateResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgUpdateApplicationRoleStateResponse {
    return {};
  },

  toJSON(_: MsgUpdateApplicationRoleStateResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgUpdateApplicationRoleStateResponse>, I>>(
    _: I,
  ): MsgUpdateApplicationRoleStateResponse {
    const message = createBaseMsgUpdateApplicationRoleStateResponse();
    return message;
  },
};

function createBaseMsgDeleteApplicationRoleState(): MsgDeleteApplicationRoleState {
  return { creator: "", id: "" };
}

export const MsgDeleteApplicationRoleState = {
  encode(message: MsgDeleteApplicationRoleState, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgDeleteApplicationRoleState {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgDeleteApplicationRoleState();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgDeleteApplicationRoleState {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
    };
  },

  toJSON(message: MsgDeleteApplicationRoleState): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgDeleteApplicationRoleState>, I>>(
    object: I,
  ): MsgDeleteApplicationRoleState {
    const message = createBaseMsgDeleteApplicationRoleState();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgDeleteApplicationRoleStateResponse(): MsgDeleteApplicationRoleStateResponse {
  return {};
}

export const MsgDeleteApplicationRoleStateResponse = {
  encode(_: MsgDeleteApplicationRoleStateResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgDeleteApplicationRoleStateResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgDeleteApplicationRoleStateResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgDeleteApplicationRoleStateResponse {
    return {};
  },

  toJSON(_: MsgDeleteApplicationRoleStateResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgDeleteApplicationRoleStateResponse>, I>>(
    _: I,
  ): MsgDeleteApplicationRoleStateResponse {
    const message = createBaseMsgDeleteApplicationRoleStateResponse();
    return message;
  },
};

function createBaseMsgCreateBlockchainAccount(): MsgCreateBlockchainAccount {
  return { creator: "", id: "" };
}

export const MsgCreateBlockchainAccount = {
  encode(message: MsgCreateBlockchainAccount, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateBlockchainAccount {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateBlockchainAccount();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateBlockchainAccount {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
    };
  },

  toJSON(message: MsgCreateBlockchainAccount): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateBlockchainAccount>, I>>(object: I): MsgCreateBlockchainAccount {
    const message = createBaseMsgCreateBlockchainAccount();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgCreateBlockchainAccountResponse(): MsgCreateBlockchainAccountResponse {
  return { id: "" };
}

export const MsgCreateBlockchainAccountResponse = {
  encode(message: MsgCreateBlockchainAccountResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.id !== "") {
      writer.uint32(10).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateBlockchainAccountResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateBlockchainAccountResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateBlockchainAccountResponse {
    return { id: isSet(object.id) ? String(object.id) : "" };
  },

  toJSON(message: MsgCreateBlockchainAccountResponse): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateBlockchainAccountResponse>, I>>(
    object: I,
  ): MsgCreateBlockchainAccountResponse {
    const message = createBaseMsgCreateBlockchainAccountResponse();
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgDeactivateBlockchainAccount(): MsgDeactivateBlockchainAccount {
  return { creator: "", id: "" };
}

export const MsgDeactivateBlockchainAccount = {
  encode(message: MsgDeactivateBlockchainAccount, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgDeactivateBlockchainAccount {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgDeactivateBlockchainAccount();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgDeactivateBlockchainAccount {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
    };
  },

  toJSON(message: MsgDeactivateBlockchainAccount): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgDeactivateBlockchainAccount>, I>>(
    object: I,
  ): MsgDeactivateBlockchainAccount {
    const message = createBaseMsgDeactivateBlockchainAccount();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgDeleteBlockchainAccountResponse(): MsgDeleteBlockchainAccountResponse {
  return {};
}

export const MsgDeleteBlockchainAccountResponse = {
  encode(_: MsgDeleteBlockchainAccountResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgDeleteBlockchainAccountResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgDeleteBlockchainAccountResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgDeleteBlockchainAccountResponse {
    return {};
  },

  toJSON(_: MsgDeleteBlockchainAccountResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgDeleteBlockchainAccountResponse>, I>>(
    _: I,
  ): MsgDeleteBlockchainAccountResponse {
    const message = createBaseMsgDeleteBlockchainAccountResponse();
    return message;
  },
};

function createBaseMsgCreateApplicationRole(): MsgCreateApplicationRole {
  return { creator: "", id: "", description: "" };
}

export const MsgCreateApplicationRole = {
  encode(message: MsgCreateApplicationRole, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.description !== "") {
      writer.uint32(26).string(message.description);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateApplicationRole {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateApplicationRole();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.description = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateApplicationRole {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      description: isSet(object.description) ? String(object.description) : "",
    };
  },

  toJSON(message: MsgCreateApplicationRole): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.description !== undefined && (obj.description = message.description);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateApplicationRole>, I>>(object: I): MsgCreateApplicationRole {
    const message = createBaseMsgCreateApplicationRole();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.description = object.description ?? "";
    return message;
  },
};

function createBaseMsgCreateApplicationRoleResponse(): MsgCreateApplicationRoleResponse {
  return { id: "" };
}

export const MsgCreateApplicationRoleResponse = {
  encode(message: MsgCreateApplicationRoleResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.id !== "") {
      writer.uint32(10).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateApplicationRoleResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateApplicationRoleResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateApplicationRoleResponse {
    return { id: isSet(object.id) ? String(object.id) : "" };
  },

  toJSON(message: MsgCreateApplicationRoleResponse): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateApplicationRoleResponse>, I>>(
    object: I,
  ): MsgCreateApplicationRoleResponse {
    const message = createBaseMsgCreateApplicationRoleResponse();
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgUpdateApplicationRole(): MsgUpdateApplicationRole {
  return { creator: "", id: "", description: "" };
}

export const MsgUpdateApplicationRole = {
  encode(message: MsgUpdateApplicationRole, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.description !== "") {
      writer.uint32(26).string(message.description);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgUpdateApplicationRole {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgUpdateApplicationRole();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.description = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateApplicationRole {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      description: isSet(object.description) ? String(object.description) : "",
    };
  },

  toJSON(message: MsgUpdateApplicationRole): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.description !== undefined && (obj.description = message.description);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgUpdateApplicationRole>, I>>(object: I): MsgUpdateApplicationRole {
    const message = createBaseMsgUpdateApplicationRole();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.description = object.description ?? "";
    return message;
  },
};

function createBaseMsgUpdateApplicationRoleResponse(): MsgUpdateApplicationRoleResponse {
  return {};
}

export const MsgUpdateApplicationRoleResponse = {
  encode(_: MsgUpdateApplicationRoleResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgUpdateApplicationRoleResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgUpdateApplicationRoleResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgUpdateApplicationRoleResponse {
    return {};
  },

  toJSON(_: MsgUpdateApplicationRoleResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgUpdateApplicationRoleResponse>, I>>(
    _: I,
  ): MsgUpdateApplicationRoleResponse {
    const message = createBaseMsgUpdateApplicationRoleResponse();
    return message;
  },
};

function createBaseMsgDeleteApplicationRole(): MsgDeleteApplicationRole {
  return { creator: "", id: "" };
}

export const MsgDeleteApplicationRole = {
  encode(message: MsgDeleteApplicationRole, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgDeleteApplicationRole {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgDeleteApplicationRole();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgDeleteApplicationRole {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
    };
  },

  toJSON(message: MsgDeleteApplicationRole): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgDeleteApplicationRole>, I>>(object: I): MsgDeleteApplicationRole {
    const message = createBaseMsgDeleteApplicationRole();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgDeleteApplicationRoleResponse(): MsgDeleteApplicationRoleResponse {
  return {};
}

export const MsgDeleteApplicationRoleResponse = {
  encode(_: MsgDeleteApplicationRoleResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgDeleteApplicationRoleResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgDeleteApplicationRoleResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgDeleteApplicationRoleResponse {
    return {};
  },

  toJSON(_: MsgDeleteApplicationRoleResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgDeleteApplicationRoleResponse>, I>>(
    _: I,
  ): MsgDeleteApplicationRoleResponse {
    const message = createBaseMsgDeleteApplicationRoleResponse();
    return message;
  },
};

function createBaseMsgDeactivateApplicationRole(): MsgDeactivateApplicationRole {
  return { creator: "", id: "" };
}

export const MsgDeactivateApplicationRole = {
  encode(message: MsgDeactivateApplicationRole, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgDeactivateApplicationRole {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgDeactivateApplicationRole();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgDeactivateApplicationRole {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
    };
  },

  toJSON(message: MsgDeactivateApplicationRole): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgDeactivateApplicationRole>, I>>(object: I): MsgDeactivateApplicationRole {
    const message = createBaseMsgDeactivateApplicationRole();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgDeactivateApplicationRoleResponse(): MsgDeactivateApplicationRoleResponse {
  return {};
}

export const MsgDeactivateApplicationRoleResponse = {
  encode(_: MsgDeactivateApplicationRoleResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgDeactivateApplicationRoleResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgDeactivateApplicationRoleResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgDeactivateApplicationRoleResponse {
    return {};
  },

  toJSON(_: MsgDeactivateApplicationRoleResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgDeactivateApplicationRoleResponse>, I>>(
    _: I,
  ): MsgDeactivateApplicationRoleResponse {
    const message = createBaseMsgDeactivateApplicationRoleResponse();
    return message;
  },
};

function createBaseMsgDeactivateBlockchainAccountResponse(): MsgDeactivateBlockchainAccountResponse {
  return {};
}

export const MsgDeactivateBlockchainAccountResponse = {
  encode(_: MsgDeactivateBlockchainAccountResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgDeactivateBlockchainAccountResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgDeactivateBlockchainAccountResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgDeactivateBlockchainAccountResponse {
    return {};
  },

  toJSON(_: MsgDeactivateBlockchainAccountResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgDeactivateBlockchainAccountResponse>, I>>(
    _: I,
  ): MsgDeactivateBlockchainAccountResponse {
    const message = createBaseMsgDeactivateBlockchainAccountResponse();
    return message;
  },
};

function createBaseMsgGrantAppRoleToBlockchainAccount(): MsgGrantAppRoleToBlockchainAccount {
  return { creator: "", user: "", roleID: "" };
}

export const MsgGrantAppRoleToBlockchainAccount = {
  encode(message: MsgGrantAppRoleToBlockchainAccount, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.user !== "") {
      writer.uint32(18).string(message.user);
    }
    if (message.roleID !== "") {
      writer.uint32(26).string(message.roleID);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgGrantAppRoleToBlockchainAccount {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgGrantAppRoleToBlockchainAccount();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.user = reader.string();
          break;
        case 3:
          message.roleID = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgGrantAppRoleToBlockchainAccount {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      user: isSet(object.user) ? String(object.user) : "",
      roleID: isSet(object.roleID) ? String(object.roleID) : "",
    };
  },

  toJSON(message: MsgGrantAppRoleToBlockchainAccount): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.user !== undefined && (obj.user = message.user);
    message.roleID !== undefined && (obj.roleID = message.roleID);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgGrantAppRoleToBlockchainAccount>, I>>(
    object: I,
  ): MsgGrantAppRoleToBlockchainAccount {
    const message = createBaseMsgGrantAppRoleToBlockchainAccount();
    message.creator = object.creator ?? "";
    message.user = object.user ?? "";
    message.roleID = object.roleID ?? "";
    return message;
  },
};

function createBaseMsgGrantAppRoleToBlockchainAccountResponse(): MsgGrantAppRoleToBlockchainAccountResponse {
  return {};
}

export const MsgGrantAppRoleToBlockchainAccountResponse = {
  encode(_: MsgGrantAppRoleToBlockchainAccountResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgGrantAppRoleToBlockchainAccountResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgGrantAppRoleToBlockchainAccountResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgGrantAppRoleToBlockchainAccountResponse {
    return {};
  },

  toJSON(_: MsgGrantAppRoleToBlockchainAccountResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgGrantAppRoleToBlockchainAccountResponse>, I>>(
    _: I,
  ): MsgGrantAppRoleToBlockchainAccountResponse {
    const message = createBaseMsgGrantAppRoleToBlockchainAccountResponse();
    return message;
  },
};

function createBaseMsgRevokeAppRoleFromBlockchainAccount(): MsgRevokeAppRoleFromBlockchainAccount {
  return { creator: "", account: "", roleID: "" };
}

export const MsgRevokeAppRoleFromBlockchainAccount = {
  encode(message: MsgRevokeAppRoleFromBlockchainAccount, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.account !== "") {
      writer.uint32(18).string(message.account);
    }
    if (message.roleID !== "") {
      writer.uint32(26).string(message.roleID);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgRevokeAppRoleFromBlockchainAccount {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgRevokeAppRoleFromBlockchainAccount();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.account = reader.string();
          break;
        case 3:
          message.roleID = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgRevokeAppRoleFromBlockchainAccount {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      account: isSet(object.account) ? String(object.account) : "",
      roleID: isSet(object.roleID) ? String(object.roleID) : "",
    };
  },

  toJSON(message: MsgRevokeAppRoleFromBlockchainAccount): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.account !== undefined && (obj.account = message.account);
    message.roleID !== undefined && (obj.roleID = message.roleID);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgRevokeAppRoleFromBlockchainAccount>, I>>(
    object: I,
  ): MsgRevokeAppRoleFromBlockchainAccount {
    const message = createBaseMsgRevokeAppRoleFromBlockchainAccount();
    message.creator = object.creator ?? "";
    message.account = object.account ?? "";
    message.roleID = object.roleID ?? "";
    return message;
  },
};

function createBaseMsgRevokeAppRoleFromBlockchainAccountResponse(): MsgRevokeAppRoleFromBlockchainAccountResponse {
  return {};
}

export const MsgRevokeAppRoleFromBlockchainAccountResponse = {
  encode(_: MsgRevokeAppRoleFromBlockchainAccountResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgRevokeAppRoleFromBlockchainAccountResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgRevokeAppRoleFromBlockchainAccountResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgRevokeAppRoleFromBlockchainAccountResponse {
    return {};
  },

  toJSON(_: MsgRevokeAppRoleFromBlockchainAccountResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgRevokeAppRoleFromBlockchainAccountResponse>, I>>(
    _: I,
  ): MsgRevokeAppRoleFromBlockchainAccountResponse {
    const message = createBaseMsgRevokeAppRoleFromBlockchainAccountResponse();
    return message;
  },
};

function createBaseMsgFetchAllApplicationRole(): MsgFetchAllApplicationRole {
  return { creator: "" };
}

export const MsgFetchAllApplicationRole = {
  encode(message: MsgFetchAllApplicationRole, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchAllApplicationRole {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchAllApplicationRole();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllApplicationRole {
    return { creator: isSet(object.creator) ? String(object.creator) : "" };
  },

  toJSON(message: MsgFetchAllApplicationRole): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchAllApplicationRole>, I>>(object: I): MsgFetchAllApplicationRole {
    const message = createBaseMsgFetchAllApplicationRole();
    message.creator = object.creator ?? "";
    return message;
  },
};

function createBaseMsgFetchAllApplicationRoleResponse(): MsgFetchAllApplicationRoleResponse {
  return { role: [] };
}

export const MsgFetchAllApplicationRoleResponse = {
  encode(message: MsgFetchAllApplicationRoleResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.role) {
      ApplicationRole.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchAllApplicationRoleResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchAllApplicationRoleResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.role.push(ApplicationRole.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllApplicationRoleResponse {
    return { role: Array.isArray(object?.role) ? object.role.map((e: any) => ApplicationRole.fromJSON(e)) : [] };
  },

  toJSON(message: MsgFetchAllApplicationRoleResponse): unknown {
    const obj: any = {};
    if (message.role) {
      obj.role = message.role.map((e) => e ? ApplicationRole.toJSON(e) : undefined);
    } else {
      obj.role = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchAllApplicationRoleResponse>, I>>(
    object: I,
  ): MsgFetchAllApplicationRoleResponse {
    const message = createBaseMsgFetchAllApplicationRoleResponse();
    message.role = object.role?.map((e) => ApplicationRole.fromPartial(e)) || [];
    return message;
  },
};

function createBaseMsgFetchApplicationRole(): MsgFetchApplicationRole {
  return { creator: "", id: "" };
}

export const MsgFetchApplicationRole = {
  encode(message: MsgFetchApplicationRole, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchApplicationRole {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchApplicationRole();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchApplicationRole {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
    };
  },

  toJSON(message: MsgFetchApplicationRole): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchApplicationRole>, I>>(object: I): MsgFetchApplicationRole {
    const message = createBaseMsgFetchApplicationRole();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgFetchApplicationRoleResponse(): MsgFetchApplicationRoleResponse {
  return { role: undefined };
}

export const MsgFetchApplicationRoleResponse = {
  encode(message: MsgFetchApplicationRoleResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.role !== undefined) {
      ApplicationRole.encode(message.role, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchApplicationRoleResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchApplicationRoleResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.role = ApplicationRole.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchApplicationRoleResponse {
    return { role: isSet(object.role) ? ApplicationRole.fromJSON(object.role) : undefined };
  },

  toJSON(message: MsgFetchApplicationRoleResponse): unknown {
    const obj: any = {};
    message.role !== undefined && (obj.role = message.role ? ApplicationRole.toJSON(message.role) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchApplicationRoleResponse>, I>>(
    object: I,
  ): MsgFetchApplicationRoleResponse {
    const message = createBaseMsgFetchApplicationRoleResponse();
    message.role = (object.role !== undefined && object.role !== null)
      ? ApplicationRole.fromPartial(object.role)
      : undefined;
    return message;
  },
};

function createBaseMsgFetchAllBlockchainAccount(): MsgFetchAllBlockchainAccount {
  return { creator: "" };
}

export const MsgFetchAllBlockchainAccount = {
  encode(message: MsgFetchAllBlockchainAccount, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchAllBlockchainAccount {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchAllBlockchainAccount();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllBlockchainAccount {
    return { creator: isSet(object.creator) ? String(object.creator) : "" };
  },

  toJSON(message: MsgFetchAllBlockchainAccount): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchAllBlockchainAccount>, I>>(object: I): MsgFetchAllBlockchainAccount {
    const message = createBaseMsgFetchAllBlockchainAccount();
    message.creator = object.creator ?? "";
    return message;
  },
};

function createBaseMsgFetchAllBlockchainAccountResponse(): MsgFetchAllBlockchainAccountResponse {
  return { account: [] };
}

export const MsgFetchAllBlockchainAccountResponse = {
  encode(message: MsgFetchAllBlockchainAccountResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.account) {
      BlockchainAccount.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchAllBlockchainAccountResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchAllBlockchainAccountResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.account.push(BlockchainAccount.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllBlockchainAccountResponse {
    return {
      account: Array.isArray(object?.account) ? object.account.map((e: any) => BlockchainAccount.fromJSON(e)) : [],
    };
  },

  toJSON(message: MsgFetchAllBlockchainAccountResponse): unknown {
    const obj: any = {};
    if (message.account) {
      obj.account = message.account.map((e) => e ? BlockchainAccount.toJSON(e) : undefined);
    } else {
      obj.account = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchAllBlockchainAccountResponse>, I>>(
    object: I,
  ): MsgFetchAllBlockchainAccountResponse {
    const message = createBaseMsgFetchAllBlockchainAccountResponse();
    message.account = object.account?.map((e) => BlockchainAccount.fromPartial(e)) || [];
    return message;
  },
};

function createBaseMsgFetchBlockchainAccount(): MsgFetchBlockchainAccount {
  return { creator: "", id: "" };
}

export const MsgFetchBlockchainAccount = {
  encode(message: MsgFetchBlockchainAccount, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchBlockchainAccount {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchBlockchainAccount();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchBlockchainAccount {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
    };
  },

  toJSON(message: MsgFetchBlockchainAccount): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchBlockchainAccount>, I>>(object: I): MsgFetchBlockchainAccount {
    const message = createBaseMsgFetchBlockchainAccount();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgFetchBlockchainAccountResponse(): MsgFetchBlockchainAccountResponse {
  return { account: undefined };
}

export const MsgFetchBlockchainAccountResponse = {
  encode(message: MsgFetchBlockchainAccountResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.account !== undefined) {
      BlockchainAccount.encode(message.account, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchBlockchainAccountResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchBlockchainAccountResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.account = BlockchainAccount.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchBlockchainAccountResponse {
    return { account: isSet(object.account) ? BlockchainAccount.fromJSON(object.account) : undefined };
  },

  toJSON(message: MsgFetchBlockchainAccountResponse): unknown {
    const obj: any = {};
    message.account !== undefined
      && (obj.account = message.account ? BlockchainAccount.toJSON(message.account) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchBlockchainAccountResponse>, I>>(
    object: I,
  ): MsgFetchBlockchainAccountResponse {
    const message = createBaseMsgFetchBlockchainAccountResponse();
    message.account = (object.account !== undefined && object.account !== null)
      ? BlockchainAccount.fromPartial(object.account)
      : undefined;
    return message;
  },
};

/** Msg defines the Msg service. */
export interface Msg {
  /** this line is used by starport scaffolding # proto/tx/rpc */
  CreateBlockchainAccount(request: MsgCreateBlockchainAccount): Promise<MsgCreateBlockchainAccountResponse>;
  CreateApplicationRole(request: MsgCreateApplicationRole): Promise<MsgCreateApplicationRoleResponse>;
  UpdateApplicationRole(request: MsgUpdateApplicationRole): Promise<MsgUpdateApplicationRoleResponse>;
  UpdateConfiguration(request: MsgUpdateConfiguration): Promise<MsgUpdateConfigurationResponse>;
  DeactivateApplicationRole(request: MsgDeactivateApplicationRole): Promise<MsgDeactivateApplicationRoleResponse>;
  DeactivateBlockchainAccount(request: MsgDeactivateBlockchainAccount): Promise<MsgDeactivateBlockchainAccountResponse>;
  GrantAppRoleToBlockchainAccount(
    request: MsgGrantAppRoleToBlockchainAccount,
  ): Promise<MsgGrantAppRoleToBlockchainAccountResponse>;
  RevokeAppRoleFromBlockchainAccount(
    request: MsgRevokeAppRoleFromBlockchainAccount,
  ): Promise<MsgRevokeAppRoleFromBlockchainAccountResponse>;
  FetchAllApplicationRole(request: MsgFetchAllApplicationRole): Promise<MsgFetchAllApplicationRoleResponse>;
  FetchApplicationRole(request: MsgFetchApplicationRole): Promise<MsgFetchApplicationRoleResponse>;
  FetchAllBlockchainAccount(request: MsgFetchAllBlockchainAccount): Promise<MsgFetchAllBlockchainAccountResponse>;
  FetchBlockchainAccount(request: MsgFetchBlockchainAccount): Promise<MsgFetchBlockchainAccountResponse>;
}

export class MsgClientImpl implements Msg {
  private readonly rpc: Rpc;
  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.CreateBlockchainAccount = this.CreateBlockchainAccount.bind(this);
    this.CreateApplicationRole = this.CreateApplicationRole.bind(this);
    this.UpdateApplicationRole = this.UpdateApplicationRole.bind(this);
    this.UpdateConfiguration = this.UpdateConfiguration.bind(this);
    this.DeactivateApplicationRole = this.DeactivateApplicationRole.bind(this);
    this.DeactivateBlockchainAccount = this.DeactivateBlockchainAccount.bind(this);
    this.GrantAppRoleToBlockchainAccount = this.GrantAppRoleToBlockchainAccount.bind(this);
    this.RevokeAppRoleFromBlockchainAccount = this.RevokeAppRoleFromBlockchainAccount.bind(this);
    this.FetchAllApplicationRole = this.FetchAllApplicationRole.bind(this);
    this.FetchApplicationRole = this.FetchApplicationRole.bind(this);
    this.FetchAllBlockchainAccount = this.FetchAllBlockchainAccount.bind(this);
    this.FetchBlockchainAccount = this.FetchBlockchainAccount.bind(this);
  }
  CreateBlockchainAccount(request: MsgCreateBlockchainAccount): Promise<MsgCreateBlockchainAccountResponse> {
    const data = MsgCreateBlockchainAccount.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.Msg",
      "CreateBlockchainAccount",
      data,
    );
    return promise.then((data) => MsgCreateBlockchainAccountResponse.decode(new _m0.Reader(data)));
  }

  CreateApplicationRole(request: MsgCreateApplicationRole): Promise<MsgCreateApplicationRoleResponse> {
    const data = MsgCreateApplicationRole.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.Msg",
      "CreateApplicationRole",
      data,
    );
    return promise.then((data) => MsgCreateApplicationRoleResponse.decode(new _m0.Reader(data)));
  }

  UpdateApplicationRole(request: MsgUpdateApplicationRole): Promise<MsgUpdateApplicationRoleResponse> {
    const data = MsgUpdateApplicationRole.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.Msg",
      "UpdateApplicationRole",
      data,
    );
    return promise.then((data) => MsgUpdateApplicationRoleResponse.decode(new _m0.Reader(data)));
  }

  UpdateConfiguration(request: MsgUpdateConfiguration): Promise<MsgUpdateConfigurationResponse> {
    const data = MsgUpdateConfiguration.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.Msg",
      "UpdateConfiguration",
      data,
    );
    return promise.then((data) => MsgUpdateConfigurationResponse.decode(new _m0.Reader(data)));
  }

  DeactivateApplicationRole(request: MsgDeactivateApplicationRole): Promise<MsgDeactivateApplicationRoleResponse> {
    const data = MsgDeactivateApplicationRole.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.Msg",
      "DeactivateApplicationRole",
      data,
    );
    return promise.then((data) => MsgDeactivateApplicationRoleResponse.decode(new _m0.Reader(data)));
  }

  DeactivateBlockchainAccount(
    request: MsgDeactivateBlockchainAccount,
  ): Promise<MsgDeactivateBlockchainAccountResponse> {
    const data = MsgDeactivateBlockchainAccount.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.Msg",
      "DeactivateBlockchainAccount",
      data,
    );
    return promise.then((data) => MsgDeactivateBlockchainAccountResponse.decode(new _m0.Reader(data)));
  }

  GrantAppRoleToBlockchainAccount(
    request: MsgGrantAppRoleToBlockchainAccount,
  ): Promise<MsgGrantAppRoleToBlockchainAccountResponse> {
    const data = MsgGrantAppRoleToBlockchainAccount.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.Msg",
      "GrantAppRoleToBlockchainAccount",
      data,
    );
    return promise.then((data) => MsgGrantAppRoleToBlockchainAccountResponse.decode(new _m0.Reader(data)));
  }

  RevokeAppRoleFromBlockchainAccount(
    request: MsgRevokeAppRoleFromBlockchainAccount,
  ): Promise<MsgRevokeAppRoleFromBlockchainAccountResponse> {
    const data = MsgRevokeAppRoleFromBlockchainAccount.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.Msg",
      "RevokeAppRoleFromBlockchainAccount",
      data,
    );
    return promise.then((data) => MsgRevokeAppRoleFromBlockchainAccountResponse.decode(new _m0.Reader(data)));
  }

  FetchAllApplicationRole(request: MsgFetchAllApplicationRole): Promise<MsgFetchAllApplicationRoleResponse> {
    const data = MsgFetchAllApplicationRole.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.Msg",
      "FetchAllApplicationRole",
      data,
    );
    return promise.then((data) => MsgFetchAllApplicationRoleResponse.decode(new _m0.Reader(data)));
  }

  FetchApplicationRole(request: MsgFetchApplicationRole): Promise<MsgFetchApplicationRoleResponse> {
    const data = MsgFetchApplicationRole.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.Msg",
      "FetchApplicationRole",
      data,
    );
    return promise.then((data) => MsgFetchApplicationRoleResponse.decode(new _m0.Reader(data)));
  }

  FetchAllBlockchainAccount(request: MsgFetchAllBlockchainAccount): Promise<MsgFetchAllBlockchainAccountResponse> {
    const data = MsgFetchAllBlockchainAccount.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.Msg",
      "FetchAllBlockchainAccount",
      data,
    );
    return promise.then((data) => MsgFetchAllBlockchainAccountResponse.decode(new _m0.Reader(data)));
  }

  FetchBlockchainAccount(request: MsgFetchBlockchainAccount): Promise<MsgFetchBlockchainAccountResponse> {
    const data = MsgFetchBlockchainAccount.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.Msg",
      "FetchBlockchainAccount",
      data,
    );
    return promise.then((data) => MsgFetchBlockchainAccountResponse.decode(new _m0.Reader(data)));
  }
}

interface Rpc {
  request(service: string, method: string, data: Uint8Array): Promise<Uint8Array>;
}

declare var self: any | undefined;
declare var window: any | undefined;
declare var global: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== "undefined") {
    return globalThis;
  }
  if (typeof self !== "undefined") {
    return self;
  }
  if (typeof window !== "undefined") {
    return window;
  }
  if (typeof global !== "undefined") {
    return global;
  }
  throw "Unable to locate global object";
})();

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
  }
  return long.toNumber();
}

if (true) {
  _m0.util.Long = Long as any;
  _m0.configure();
}

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

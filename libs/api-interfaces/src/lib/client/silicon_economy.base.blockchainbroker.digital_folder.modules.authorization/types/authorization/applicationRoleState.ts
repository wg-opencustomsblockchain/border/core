/* eslint-disable */
import _m0 from "protobufjs/minimal";

export const protobufPackage = "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization";

export interface ApplicationRoleState {
  creator: string;
  id: string;
  applicationRoleID: string;
  description: string;
  valid: boolean;
  timeStamp: string;
}

function createBaseApplicationRoleState(): ApplicationRoleState {
  return { creator: "", id: "", applicationRoleID: "", description: "", valid: false, timeStamp: "" };
}

export const ApplicationRoleState = {
  encode(message: ApplicationRoleState, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.applicationRoleID !== "") {
      writer.uint32(26).string(message.applicationRoleID);
    }
    if (message.description !== "") {
      writer.uint32(34).string(message.description);
    }
    if (message.valid === true) {
      writer.uint32(40).bool(message.valid);
    }
    if (message.timeStamp !== "") {
      writer.uint32(50).string(message.timeStamp);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ApplicationRoleState {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseApplicationRoleState();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.applicationRoleID = reader.string();
          break;
        case 4:
          message.description = reader.string();
          break;
        case 5:
          message.valid = reader.bool();
          break;
        case 6:
          message.timeStamp = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ApplicationRoleState {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      applicationRoleID: isSet(object.applicationRoleID) ? String(object.applicationRoleID) : "",
      description: isSet(object.description) ? String(object.description) : "",
      valid: isSet(object.valid) ? Boolean(object.valid) : false,
      timeStamp: isSet(object.timeStamp) ? String(object.timeStamp) : "",
    };
  },

  toJSON(message: ApplicationRoleState): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.applicationRoleID !== undefined && (obj.applicationRoleID = message.applicationRoleID);
    message.description !== undefined && (obj.description = message.description);
    message.valid !== undefined && (obj.valid = message.valid);
    message.timeStamp !== undefined && (obj.timeStamp = message.timeStamp);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<ApplicationRoleState>, I>>(object: I): ApplicationRoleState {
    const message = createBaseApplicationRoleState();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.applicationRoleID = object.applicationRoleID ?? "";
    message.description = object.description ?? "";
    message.valid = object.valid ?? false;
    message.timeStamp = object.timeStamp ?? "";
    return message;
  },
};

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

/* eslint-disable */
import _m0 from "protobufjs/minimal";
import { BlockchainAccountState } from "./blockchainAccountState";

export const protobufPackage = "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization";

export interface BlockchainAccount {
  creator: string;
  id: string;
  blockchainAccountStates: BlockchainAccountState[];
}

function createBaseBlockchainAccount(): BlockchainAccount {
  return { creator: "", id: "", blockchainAccountStates: [] };
}

export const BlockchainAccount = {
  encode(message: BlockchainAccount, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    for (const v of message.blockchainAccountStates) {
      BlockchainAccountState.encode(v!, writer.uint32(26).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): BlockchainAccount {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseBlockchainAccount();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.blockchainAccountStates.push(BlockchainAccountState.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): BlockchainAccount {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      blockchainAccountStates: Array.isArray(object?.blockchainAccountStates)
        ? object.blockchainAccountStates.map((e: any) => BlockchainAccountState.fromJSON(e))
        : [],
    };
  },

  toJSON(message: BlockchainAccount): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    if (message.blockchainAccountStates) {
      obj.blockchainAccountStates = message.blockchainAccountStates.map((e) =>
        e ? BlockchainAccountState.toJSON(e) : undefined
      );
    } else {
      obj.blockchainAccountStates = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<BlockchainAccount>, I>>(object: I): BlockchainAccount {
    const message = createBaseBlockchainAccount();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.blockchainAccountStates = object.blockchainAccountStates?.map((e) => BlockchainAccountState.fromPartial(e))
      || [];
    return message;
  },
};

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

/* eslint-disable */
import _m0 from "protobufjs/minimal";
import { ApplicationRoleState } from "./applicationRoleState";

export const protobufPackage = "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization";

export interface ApplicationRole {
  creator: string;
  id: string;
  applicationRoleStates: ApplicationRoleState[];
}

function createBaseApplicationRole(): ApplicationRole {
  return { creator: "", id: "", applicationRoleStates: [] };
}

export const ApplicationRole = {
  encode(message: ApplicationRole, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    for (const v of message.applicationRoleStates) {
      ApplicationRoleState.encode(v!, writer.uint32(26).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ApplicationRole {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseApplicationRole();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.applicationRoleStates.push(ApplicationRoleState.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ApplicationRole {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      applicationRoleStates: Array.isArray(object?.applicationRoleStates)
        ? object.applicationRoleStates.map((e: any) => ApplicationRoleState.fromJSON(e))
        : [],
    };
  },

  toJSON(message: ApplicationRole): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    if (message.applicationRoleStates) {
      obj.applicationRoleStates = message.applicationRoleStates.map((e) =>
        e ? ApplicationRoleState.toJSON(e) : undefined
      );
    } else {
      obj.applicationRoleStates = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<ApplicationRole>, I>>(object: I): ApplicationRole {
    const message = createBaseApplicationRole();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.applicationRoleStates = object.applicationRoleStates?.map((e) => ApplicationRoleState.fromPartial(e)) || [];
    return message;
  },
};

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

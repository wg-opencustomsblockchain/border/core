/* eslint-disable */
import _m0 from "protobufjs/minimal";
import { ApplicationRole } from "./applicationRole";
import { ApplicationRoleState } from "./applicationRoleState";
import { BlockchainAccount } from "./blockchainAccount";
import { BlockchainAccountState } from "./blockchainAccountState";
import { Configuration } from "./configuration";

export const protobufPackage = "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization";

/** GenesisState defines the capability module's genesis state. */
export interface GenesisState {
  /** this line is used by starport scaffolding # genesis/proto/state */
  configurationList: Configuration[];
  /** this line is used by starport scaffolding # genesis/proto/stateField */
  blockchainAccountStateList: BlockchainAccountState[];
  /** this line is used by starport scaffolding # genesis/proto/stateField */
  applicationRoleStateList: ApplicationRoleState[];
  /** this line is used by starport scaffolding # genesis/proto/stateField */
  blockchainAccountList: BlockchainAccount[];
  /** this line is used by starport scaffolding # genesis/proto/stateField */
  applicationRoleList: ApplicationRole[];
}

function createBaseGenesisState(): GenesisState {
  return {
    configurationList: [],
    blockchainAccountStateList: [],
    applicationRoleStateList: [],
    blockchainAccountList: [],
    applicationRoleList: [],
  };
}

export const GenesisState = {
  encode(message: GenesisState, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.configurationList) {
      Configuration.encode(v!, writer.uint32(42).fork()).ldelim();
    }
    for (const v of message.blockchainAccountStateList) {
      BlockchainAccountState.encode(v!, writer.uint32(34).fork()).ldelim();
    }
    for (const v of message.applicationRoleStateList) {
      ApplicationRoleState.encode(v!, writer.uint32(26).fork()).ldelim();
    }
    for (const v of message.blockchainAccountList) {
      BlockchainAccount.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    for (const v of message.applicationRoleList) {
      ApplicationRole.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): GenesisState {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseGenesisState();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 5:
          message.configurationList.push(Configuration.decode(reader, reader.uint32()));
          break;
        case 4:
          message.blockchainAccountStateList.push(BlockchainAccountState.decode(reader, reader.uint32()));
          break;
        case 3:
          message.applicationRoleStateList.push(ApplicationRoleState.decode(reader, reader.uint32()));
          break;
        case 2:
          message.blockchainAccountList.push(BlockchainAccount.decode(reader, reader.uint32()));
          break;
        case 1:
          message.applicationRoleList.push(ApplicationRole.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GenesisState {
    return {
      configurationList: Array.isArray(object?.configurationList)
        ? object.configurationList.map((e: any) => Configuration.fromJSON(e))
        : [],
      blockchainAccountStateList: Array.isArray(object?.blockchainAccountStateList)
        ? object.blockchainAccountStateList.map((e: any) => BlockchainAccountState.fromJSON(e))
        : [],
      applicationRoleStateList: Array.isArray(object?.applicationRoleStateList)
        ? object.applicationRoleStateList.map((e: any) => ApplicationRoleState.fromJSON(e))
        : [],
      blockchainAccountList: Array.isArray(object?.blockchainAccountList)
        ? object.blockchainAccountList.map((e: any) => BlockchainAccount.fromJSON(e))
        : [],
      applicationRoleList: Array.isArray(object?.applicationRoleList)
        ? object.applicationRoleList.map((e: any) => ApplicationRole.fromJSON(e))
        : [],
    };
  },

  toJSON(message: GenesisState): unknown {
    const obj: any = {};
    if (message.configurationList) {
      obj.configurationList = message.configurationList.map((e) => e ? Configuration.toJSON(e) : undefined);
    } else {
      obj.configurationList = [];
    }
    if (message.blockchainAccountStateList) {
      obj.blockchainAccountStateList = message.blockchainAccountStateList.map((e) =>
        e ? BlockchainAccountState.toJSON(e) : undefined
      );
    } else {
      obj.blockchainAccountStateList = [];
    }
    if (message.applicationRoleStateList) {
      obj.applicationRoleStateList = message.applicationRoleStateList.map((e) =>
        e ? ApplicationRoleState.toJSON(e) : undefined
      );
    } else {
      obj.applicationRoleStateList = [];
    }
    if (message.blockchainAccountList) {
      obj.blockchainAccountList = message.blockchainAccountList.map((e) => e ? BlockchainAccount.toJSON(e) : undefined);
    } else {
      obj.blockchainAccountList = [];
    }
    if (message.applicationRoleList) {
      obj.applicationRoleList = message.applicationRoleList.map((e) => e ? ApplicationRole.toJSON(e) : undefined);
    } else {
      obj.applicationRoleList = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<GenesisState>, I>>(object: I): GenesisState {
    const message = createBaseGenesisState();
    message.configurationList = object.configurationList?.map((e) => Configuration.fromPartial(e)) || [];
    message.blockchainAccountStateList =
      object.blockchainAccountStateList?.map((e) => BlockchainAccountState.fromPartial(e)) || [];
    message.applicationRoleStateList = object.applicationRoleStateList?.map((e) => ApplicationRoleState.fromPartial(e))
      || [];
    message.blockchainAccountList = object.blockchainAccountList?.map((e) => BlockchainAccount.fromPartial(e)) || [];
    message.applicationRoleList = object.applicationRoleList?.map((e) => ApplicationRole.fromPartial(e)) || [];
    return message;
  },
};

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

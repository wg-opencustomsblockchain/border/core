/* eslint-disable */
import _m0 from "protobufjs/minimal";

export const protobufPackage = "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization";

export interface BlockchainAccountState {
  creator: string;
  id: string;
  accountID: string;
  timeStamp: string;
  applicationRoleIDs: string[];
  valid: boolean;
}

function createBaseBlockchainAccountState(): BlockchainAccountState {
  return { creator: "", id: "", accountID: "", timeStamp: "", applicationRoleIDs: [], valid: false };
}

export const BlockchainAccountState = {
  encode(message: BlockchainAccountState, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.accountID !== "") {
      writer.uint32(26).string(message.accountID);
    }
    if (message.timeStamp !== "") {
      writer.uint32(34).string(message.timeStamp);
    }
    for (const v of message.applicationRoleIDs) {
      writer.uint32(42).string(v!);
    }
    if (message.valid === true) {
      writer.uint32(48).bool(message.valid);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): BlockchainAccountState {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseBlockchainAccountState();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.accountID = reader.string();
          break;
        case 4:
          message.timeStamp = reader.string();
          break;
        case 5:
          message.applicationRoleIDs.push(reader.string());
          break;
        case 6:
          message.valid = reader.bool();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): BlockchainAccountState {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      accountID: isSet(object.accountID) ? String(object.accountID) : "",
      timeStamp: isSet(object.timeStamp) ? String(object.timeStamp) : "",
      applicationRoleIDs: Array.isArray(object?.applicationRoleIDs)
        ? object.applicationRoleIDs.map((e: any) => String(e))
        : [],
      valid: isSet(object.valid) ? Boolean(object.valid) : false,
    };
  },

  toJSON(message: BlockchainAccountState): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.accountID !== undefined && (obj.accountID = message.accountID);
    message.timeStamp !== undefined && (obj.timeStamp = message.timeStamp);
    if (message.applicationRoleIDs) {
      obj.applicationRoleIDs = message.applicationRoleIDs.map((e) => e);
    } else {
      obj.applicationRoleIDs = [];
    }
    message.valid !== undefined && (obj.valid = message.valid);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<BlockchainAccountState>, I>>(object: I): BlockchainAccountState {
    const message = createBaseBlockchainAccountState();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.accountID = object.accountID ?? "";
    message.timeStamp = object.timeStamp ?? "";
    message.applicationRoleIDs = object.applicationRoleIDs?.map((e) => e) || [];
    message.valid = object.valid ?? false;
    return message;
  },
};

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

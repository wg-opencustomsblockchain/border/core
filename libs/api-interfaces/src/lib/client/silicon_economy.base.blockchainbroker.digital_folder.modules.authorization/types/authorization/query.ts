/* eslint-disable */
import Long from "long";
import _m0 from "protobufjs/minimal";
import { PageRequest, PageResponse } from "../cosmos/base/query/v1beta1/pagination";
import { ApplicationRole } from "./applicationRole";
import { ApplicationRoleState } from "./applicationRoleState";
import { BlockchainAccount } from "./blockchainAccount";
import { BlockchainAccountState } from "./blockchainAccountState";
import { Configuration } from "./configuration";

export const protobufPackage = "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization";

/** this line is used by starport scaffolding # 3 */
export interface QueryGetConfigurationRequest {
  id: number;
}

export interface QueryGetConfigurationResponse {
  Configuration: Configuration | undefined;
}

export interface QueryAllConfigurationRequest {
  pagination: PageRequest | undefined;
}

export interface QueryAllConfigurationResponse {
  Configuration: Configuration[];
  pagination: PageResponse | undefined;
}

export interface QueryGetBlockchainAccountStateRequest {
  id: string;
}

export interface QueryGetBlockchainAccountStateResponse {
  BlockchainAccountState: BlockchainAccountState | undefined;
}

export interface QueryAllBlockchainAccountStateRequest {
  pagination: PageRequest | undefined;
}

export interface QueryAllBlockchainAccountStateResponse {
  BlockchainAccountState: BlockchainAccountState[];
  pagination: PageResponse | undefined;
}

export interface QueryGetApplicationRoleStateRequest {
  id: string;
}

export interface QueryGetApplicationRoleStateResponse {
  ApplicationRoleState: ApplicationRoleState | undefined;
}

export interface QueryAllApplicationRoleStateRequest {
  pagination: PageRequest | undefined;
}

export interface QueryAllApplicationRoleStateResponse {
  ApplicationRoleState: ApplicationRoleState[];
  pagination: PageResponse | undefined;
}

export interface QueryGetBlockchainAccountRequest {
  id: string;
}

export interface QueryGetBlockchainAccountResponse {
  BlockchainAccount: BlockchainAccount | undefined;
}

export interface QueryAllBlockchainAccountRequest {
  pagination: PageRequest | undefined;
}

export interface QueryAllBlockchainAccountResponse {
  BlockchainAccount: BlockchainAccount[];
  pagination: PageResponse | undefined;
}

export interface QueryGetApplicationRoleRequest {
  id: string;
}

export interface QueryGetApplicationRoleResponse {
  ApplicationRole: ApplicationRole | undefined;
}

export interface QueryAllApplicationRoleRequest {
  pagination: PageRequest | undefined;
}

export interface QueryAllApplicationRoleResponse {
  ApplicationRole: ApplicationRole[];
  pagination: PageResponse | undefined;
}

function createBaseQueryGetConfigurationRequest(): QueryGetConfigurationRequest {
  return { id: 0 };
}

export const QueryGetConfigurationRequest = {
  encode(message: QueryGetConfigurationRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).uint64(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryGetConfigurationRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryGetConfigurationRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = longToNumber(reader.uint64() as Long);
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetConfigurationRequest {
    return { id: isSet(object.id) ? Number(object.id) : 0 };
  },

  toJSON(message: QueryGetConfigurationRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryGetConfigurationRequest>, I>>(object: I): QueryGetConfigurationRequest {
    const message = createBaseQueryGetConfigurationRequest();
    message.id = object.id ?? 0;
    return message;
  },
};

function createBaseQueryGetConfigurationResponse(): QueryGetConfigurationResponse {
  return { Configuration: undefined };
}

export const QueryGetConfigurationResponse = {
  encode(message: QueryGetConfigurationResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.Configuration !== undefined) {
      Configuration.encode(message.Configuration, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryGetConfigurationResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryGetConfigurationResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.Configuration = Configuration.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetConfigurationResponse {
    return { Configuration: isSet(object.Configuration) ? Configuration.fromJSON(object.Configuration) : undefined };
  },

  toJSON(message: QueryGetConfigurationResponse): unknown {
    const obj: any = {};
    message.Configuration !== undefined
      && (obj.Configuration = message.Configuration ? Configuration.toJSON(message.Configuration) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryGetConfigurationResponse>, I>>(
    object: I,
  ): QueryGetConfigurationResponse {
    const message = createBaseQueryGetConfigurationResponse();
    message.Configuration = (object.Configuration !== undefined && object.Configuration !== null)
      ? Configuration.fromPartial(object.Configuration)
      : undefined;
    return message;
  },
};

function createBaseQueryAllConfigurationRequest(): QueryAllConfigurationRequest {
  return { pagination: undefined };
}

export const QueryAllConfigurationRequest = {
  encode(message: QueryAllConfigurationRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.pagination !== undefined) {
      PageRequest.encode(message.pagination, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryAllConfigurationRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryAllConfigurationRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.pagination = PageRequest.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllConfigurationRequest {
    return { pagination: isSet(object.pagination) ? PageRequest.fromJSON(object.pagination) : undefined };
  },

  toJSON(message: QueryAllConfigurationRequest): unknown {
    const obj: any = {};
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageRequest.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryAllConfigurationRequest>, I>>(object: I): QueryAllConfigurationRequest {
    const message = createBaseQueryAllConfigurationRequest();
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageRequest.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

function createBaseQueryAllConfigurationResponse(): QueryAllConfigurationResponse {
  return { Configuration: [], pagination: undefined };
}

export const QueryAllConfigurationResponse = {
  encode(message: QueryAllConfigurationResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.Configuration) {
      Configuration.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.pagination !== undefined) {
      PageResponse.encode(message.pagination, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryAllConfigurationResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryAllConfigurationResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.Configuration.push(Configuration.decode(reader, reader.uint32()));
          break;
        case 2:
          message.pagination = PageResponse.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllConfigurationResponse {
    return {
      Configuration: Array.isArray(object?.Configuration)
        ? object.Configuration.map((e: any) => Configuration.fromJSON(e))
        : [],
      pagination: isSet(object.pagination) ? PageResponse.fromJSON(object.pagination) : undefined,
    };
  },

  toJSON(message: QueryAllConfigurationResponse): unknown {
    const obj: any = {};
    if (message.Configuration) {
      obj.Configuration = message.Configuration.map((e) => e ? Configuration.toJSON(e) : undefined);
    } else {
      obj.Configuration = [];
    }
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageResponse.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryAllConfigurationResponse>, I>>(
    object: I,
  ): QueryAllConfigurationResponse {
    const message = createBaseQueryAllConfigurationResponse();
    message.Configuration = object.Configuration?.map((e) => Configuration.fromPartial(e)) || [];
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageResponse.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

function createBaseQueryGetBlockchainAccountStateRequest(): QueryGetBlockchainAccountStateRequest {
  return { id: "" };
}

export const QueryGetBlockchainAccountStateRequest = {
  encode(message: QueryGetBlockchainAccountStateRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.id !== "") {
      writer.uint32(10).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryGetBlockchainAccountStateRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryGetBlockchainAccountStateRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetBlockchainAccountStateRequest {
    return { id: isSet(object.id) ? String(object.id) : "" };
  },

  toJSON(message: QueryGetBlockchainAccountStateRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryGetBlockchainAccountStateRequest>, I>>(
    object: I,
  ): QueryGetBlockchainAccountStateRequest {
    const message = createBaseQueryGetBlockchainAccountStateRequest();
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseQueryGetBlockchainAccountStateResponse(): QueryGetBlockchainAccountStateResponse {
  return { BlockchainAccountState: undefined };
}

export const QueryGetBlockchainAccountStateResponse = {
  encode(message: QueryGetBlockchainAccountStateResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.BlockchainAccountState !== undefined) {
      BlockchainAccountState.encode(message.BlockchainAccountState, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryGetBlockchainAccountStateResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryGetBlockchainAccountStateResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.BlockchainAccountState = BlockchainAccountState.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetBlockchainAccountStateResponse {
    return {
      BlockchainAccountState: isSet(object.BlockchainAccountState)
        ? BlockchainAccountState.fromJSON(object.BlockchainAccountState)
        : undefined,
    };
  },

  toJSON(message: QueryGetBlockchainAccountStateResponse): unknown {
    const obj: any = {};
    message.BlockchainAccountState !== undefined && (obj.BlockchainAccountState = message.BlockchainAccountState
      ? BlockchainAccountState.toJSON(message.BlockchainAccountState)
      : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryGetBlockchainAccountStateResponse>, I>>(
    object: I,
  ): QueryGetBlockchainAccountStateResponse {
    const message = createBaseQueryGetBlockchainAccountStateResponse();
    message.BlockchainAccountState =
      (object.BlockchainAccountState !== undefined && object.BlockchainAccountState !== null)
        ? BlockchainAccountState.fromPartial(object.BlockchainAccountState)
        : undefined;
    return message;
  },
};

function createBaseQueryAllBlockchainAccountStateRequest(): QueryAllBlockchainAccountStateRequest {
  return { pagination: undefined };
}

export const QueryAllBlockchainAccountStateRequest = {
  encode(message: QueryAllBlockchainAccountStateRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.pagination !== undefined) {
      PageRequest.encode(message.pagination, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryAllBlockchainAccountStateRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryAllBlockchainAccountStateRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.pagination = PageRequest.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllBlockchainAccountStateRequest {
    return { pagination: isSet(object.pagination) ? PageRequest.fromJSON(object.pagination) : undefined };
  },

  toJSON(message: QueryAllBlockchainAccountStateRequest): unknown {
    const obj: any = {};
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageRequest.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryAllBlockchainAccountStateRequest>, I>>(
    object: I,
  ): QueryAllBlockchainAccountStateRequest {
    const message = createBaseQueryAllBlockchainAccountStateRequest();
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageRequest.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

function createBaseQueryAllBlockchainAccountStateResponse(): QueryAllBlockchainAccountStateResponse {
  return { BlockchainAccountState: [], pagination: undefined };
}

export const QueryAllBlockchainAccountStateResponse = {
  encode(message: QueryAllBlockchainAccountStateResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.BlockchainAccountState) {
      BlockchainAccountState.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.pagination !== undefined) {
      PageResponse.encode(message.pagination, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryAllBlockchainAccountStateResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryAllBlockchainAccountStateResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.BlockchainAccountState.push(BlockchainAccountState.decode(reader, reader.uint32()));
          break;
        case 2:
          message.pagination = PageResponse.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllBlockchainAccountStateResponse {
    return {
      BlockchainAccountState: Array.isArray(object?.BlockchainAccountState)
        ? object.BlockchainAccountState.map((e: any) => BlockchainAccountState.fromJSON(e))
        : [],
      pagination: isSet(object.pagination) ? PageResponse.fromJSON(object.pagination) : undefined,
    };
  },

  toJSON(message: QueryAllBlockchainAccountStateResponse): unknown {
    const obj: any = {};
    if (message.BlockchainAccountState) {
      obj.BlockchainAccountState = message.BlockchainAccountState.map((e) =>
        e ? BlockchainAccountState.toJSON(e) : undefined
      );
    } else {
      obj.BlockchainAccountState = [];
    }
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageResponse.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryAllBlockchainAccountStateResponse>, I>>(
    object: I,
  ): QueryAllBlockchainAccountStateResponse {
    const message = createBaseQueryAllBlockchainAccountStateResponse();
    message.BlockchainAccountState = object.BlockchainAccountState?.map((e) => BlockchainAccountState.fromPartial(e))
      || [];
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageResponse.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

function createBaseQueryGetApplicationRoleStateRequest(): QueryGetApplicationRoleStateRequest {
  return { id: "" };
}

export const QueryGetApplicationRoleStateRequest = {
  encode(message: QueryGetApplicationRoleStateRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.id !== "") {
      writer.uint32(10).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryGetApplicationRoleStateRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryGetApplicationRoleStateRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetApplicationRoleStateRequest {
    return { id: isSet(object.id) ? String(object.id) : "" };
  },

  toJSON(message: QueryGetApplicationRoleStateRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryGetApplicationRoleStateRequest>, I>>(
    object: I,
  ): QueryGetApplicationRoleStateRequest {
    const message = createBaseQueryGetApplicationRoleStateRequest();
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseQueryGetApplicationRoleStateResponse(): QueryGetApplicationRoleStateResponse {
  return { ApplicationRoleState: undefined };
}

export const QueryGetApplicationRoleStateResponse = {
  encode(message: QueryGetApplicationRoleStateResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.ApplicationRoleState !== undefined) {
      ApplicationRoleState.encode(message.ApplicationRoleState, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryGetApplicationRoleStateResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryGetApplicationRoleStateResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.ApplicationRoleState = ApplicationRoleState.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetApplicationRoleStateResponse {
    return {
      ApplicationRoleState: isSet(object.ApplicationRoleState)
        ? ApplicationRoleState.fromJSON(object.ApplicationRoleState)
        : undefined,
    };
  },

  toJSON(message: QueryGetApplicationRoleStateResponse): unknown {
    const obj: any = {};
    message.ApplicationRoleState !== undefined && (obj.ApplicationRoleState = message.ApplicationRoleState
      ? ApplicationRoleState.toJSON(message.ApplicationRoleState)
      : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryGetApplicationRoleStateResponse>, I>>(
    object: I,
  ): QueryGetApplicationRoleStateResponse {
    const message = createBaseQueryGetApplicationRoleStateResponse();
    message.ApplicationRoleState = (object.ApplicationRoleState !== undefined && object.ApplicationRoleState !== null)
      ? ApplicationRoleState.fromPartial(object.ApplicationRoleState)
      : undefined;
    return message;
  },
};

function createBaseQueryAllApplicationRoleStateRequest(): QueryAllApplicationRoleStateRequest {
  return { pagination: undefined };
}

export const QueryAllApplicationRoleStateRequest = {
  encode(message: QueryAllApplicationRoleStateRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.pagination !== undefined) {
      PageRequest.encode(message.pagination, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryAllApplicationRoleStateRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryAllApplicationRoleStateRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.pagination = PageRequest.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllApplicationRoleStateRequest {
    return { pagination: isSet(object.pagination) ? PageRequest.fromJSON(object.pagination) : undefined };
  },

  toJSON(message: QueryAllApplicationRoleStateRequest): unknown {
    const obj: any = {};
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageRequest.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryAllApplicationRoleStateRequest>, I>>(
    object: I,
  ): QueryAllApplicationRoleStateRequest {
    const message = createBaseQueryAllApplicationRoleStateRequest();
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageRequest.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

function createBaseQueryAllApplicationRoleStateResponse(): QueryAllApplicationRoleStateResponse {
  return { ApplicationRoleState: [], pagination: undefined };
}

export const QueryAllApplicationRoleStateResponse = {
  encode(message: QueryAllApplicationRoleStateResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.ApplicationRoleState) {
      ApplicationRoleState.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.pagination !== undefined) {
      PageResponse.encode(message.pagination, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryAllApplicationRoleStateResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryAllApplicationRoleStateResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.ApplicationRoleState.push(ApplicationRoleState.decode(reader, reader.uint32()));
          break;
        case 2:
          message.pagination = PageResponse.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllApplicationRoleStateResponse {
    return {
      ApplicationRoleState: Array.isArray(object?.ApplicationRoleState)
        ? object.ApplicationRoleState.map((e: any) => ApplicationRoleState.fromJSON(e))
        : [],
      pagination: isSet(object.pagination) ? PageResponse.fromJSON(object.pagination) : undefined,
    };
  },

  toJSON(message: QueryAllApplicationRoleStateResponse): unknown {
    const obj: any = {};
    if (message.ApplicationRoleState) {
      obj.ApplicationRoleState = message.ApplicationRoleState.map((e) =>
        e ? ApplicationRoleState.toJSON(e) : undefined
      );
    } else {
      obj.ApplicationRoleState = [];
    }
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageResponse.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryAllApplicationRoleStateResponse>, I>>(
    object: I,
  ): QueryAllApplicationRoleStateResponse {
    const message = createBaseQueryAllApplicationRoleStateResponse();
    message.ApplicationRoleState = object.ApplicationRoleState?.map((e) => ApplicationRoleState.fromPartial(e)) || [];
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageResponse.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

function createBaseQueryGetBlockchainAccountRequest(): QueryGetBlockchainAccountRequest {
  return { id: "" };
}

export const QueryGetBlockchainAccountRequest = {
  encode(message: QueryGetBlockchainAccountRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.id !== "") {
      writer.uint32(10).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryGetBlockchainAccountRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryGetBlockchainAccountRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetBlockchainAccountRequest {
    return { id: isSet(object.id) ? String(object.id) : "" };
  },

  toJSON(message: QueryGetBlockchainAccountRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryGetBlockchainAccountRequest>, I>>(
    object: I,
  ): QueryGetBlockchainAccountRequest {
    const message = createBaseQueryGetBlockchainAccountRequest();
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseQueryGetBlockchainAccountResponse(): QueryGetBlockchainAccountResponse {
  return { BlockchainAccount: undefined };
}

export const QueryGetBlockchainAccountResponse = {
  encode(message: QueryGetBlockchainAccountResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.BlockchainAccount !== undefined) {
      BlockchainAccount.encode(message.BlockchainAccount, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryGetBlockchainAccountResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryGetBlockchainAccountResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.BlockchainAccount = BlockchainAccount.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetBlockchainAccountResponse {
    return {
      BlockchainAccount: isSet(object.BlockchainAccount)
        ? BlockchainAccount.fromJSON(object.BlockchainAccount)
        : undefined,
    };
  },

  toJSON(message: QueryGetBlockchainAccountResponse): unknown {
    const obj: any = {};
    message.BlockchainAccount !== undefined && (obj.BlockchainAccount = message.BlockchainAccount
      ? BlockchainAccount.toJSON(message.BlockchainAccount)
      : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryGetBlockchainAccountResponse>, I>>(
    object: I,
  ): QueryGetBlockchainAccountResponse {
    const message = createBaseQueryGetBlockchainAccountResponse();
    message.BlockchainAccount = (object.BlockchainAccount !== undefined && object.BlockchainAccount !== null)
      ? BlockchainAccount.fromPartial(object.BlockchainAccount)
      : undefined;
    return message;
  },
};

function createBaseQueryAllBlockchainAccountRequest(): QueryAllBlockchainAccountRequest {
  return { pagination: undefined };
}

export const QueryAllBlockchainAccountRequest = {
  encode(message: QueryAllBlockchainAccountRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.pagination !== undefined) {
      PageRequest.encode(message.pagination, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryAllBlockchainAccountRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryAllBlockchainAccountRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.pagination = PageRequest.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllBlockchainAccountRequest {
    return { pagination: isSet(object.pagination) ? PageRequest.fromJSON(object.pagination) : undefined };
  },

  toJSON(message: QueryAllBlockchainAccountRequest): unknown {
    const obj: any = {};
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageRequest.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryAllBlockchainAccountRequest>, I>>(
    object: I,
  ): QueryAllBlockchainAccountRequest {
    const message = createBaseQueryAllBlockchainAccountRequest();
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageRequest.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

function createBaseQueryAllBlockchainAccountResponse(): QueryAllBlockchainAccountResponse {
  return { BlockchainAccount: [], pagination: undefined };
}

export const QueryAllBlockchainAccountResponse = {
  encode(message: QueryAllBlockchainAccountResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.BlockchainAccount) {
      BlockchainAccount.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.pagination !== undefined) {
      PageResponse.encode(message.pagination, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryAllBlockchainAccountResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryAllBlockchainAccountResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.BlockchainAccount.push(BlockchainAccount.decode(reader, reader.uint32()));
          break;
        case 2:
          message.pagination = PageResponse.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllBlockchainAccountResponse {
    return {
      BlockchainAccount: Array.isArray(object?.BlockchainAccount)
        ? object.BlockchainAccount.map((e: any) => BlockchainAccount.fromJSON(e))
        : [],
      pagination: isSet(object.pagination) ? PageResponse.fromJSON(object.pagination) : undefined,
    };
  },

  toJSON(message: QueryAllBlockchainAccountResponse): unknown {
    const obj: any = {};
    if (message.BlockchainAccount) {
      obj.BlockchainAccount = message.BlockchainAccount.map((e) => e ? BlockchainAccount.toJSON(e) : undefined);
    } else {
      obj.BlockchainAccount = [];
    }
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageResponse.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryAllBlockchainAccountResponse>, I>>(
    object: I,
  ): QueryAllBlockchainAccountResponse {
    const message = createBaseQueryAllBlockchainAccountResponse();
    message.BlockchainAccount = object.BlockchainAccount?.map((e) => BlockchainAccount.fromPartial(e)) || [];
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageResponse.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

function createBaseQueryGetApplicationRoleRequest(): QueryGetApplicationRoleRequest {
  return { id: "" };
}

export const QueryGetApplicationRoleRequest = {
  encode(message: QueryGetApplicationRoleRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.id !== "") {
      writer.uint32(10).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryGetApplicationRoleRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryGetApplicationRoleRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetApplicationRoleRequest {
    return { id: isSet(object.id) ? String(object.id) : "" };
  },

  toJSON(message: QueryGetApplicationRoleRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryGetApplicationRoleRequest>, I>>(
    object: I,
  ): QueryGetApplicationRoleRequest {
    const message = createBaseQueryGetApplicationRoleRequest();
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseQueryGetApplicationRoleResponse(): QueryGetApplicationRoleResponse {
  return { ApplicationRole: undefined };
}

export const QueryGetApplicationRoleResponse = {
  encode(message: QueryGetApplicationRoleResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.ApplicationRole !== undefined) {
      ApplicationRole.encode(message.ApplicationRole, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryGetApplicationRoleResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryGetApplicationRoleResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.ApplicationRole = ApplicationRole.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetApplicationRoleResponse {
    return {
      ApplicationRole: isSet(object.ApplicationRole) ? ApplicationRole.fromJSON(object.ApplicationRole) : undefined,
    };
  },

  toJSON(message: QueryGetApplicationRoleResponse): unknown {
    const obj: any = {};
    message.ApplicationRole !== undefined
      && (obj.ApplicationRole = message.ApplicationRole ? ApplicationRole.toJSON(message.ApplicationRole) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryGetApplicationRoleResponse>, I>>(
    object: I,
  ): QueryGetApplicationRoleResponse {
    const message = createBaseQueryGetApplicationRoleResponse();
    message.ApplicationRole = (object.ApplicationRole !== undefined && object.ApplicationRole !== null)
      ? ApplicationRole.fromPartial(object.ApplicationRole)
      : undefined;
    return message;
  },
};

function createBaseQueryAllApplicationRoleRequest(): QueryAllApplicationRoleRequest {
  return { pagination: undefined };
}

export const QueryAllApplicationRoleRequest = {
  encode(message: QueryAllApplicationRoleRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.pagination !== undefined) {
      PageRequest.encode(message.pagination, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryAllApplicationRoleRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryAllApplicationRoleRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.pagination = PageRequest.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllApplicationRoleRequest {
    return { pagination: isSet(object.pagination) ? PageRequest.fromJSON(object.pagination) : undefined };
  },

  toJSON(message: QueryAllApplicationRoleRequest): unknown {
    const obj: any = {};
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageRequest.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryAllApplicationRoleRequest>, I>>(
    object: I,
  ): QueryAllApplicationRoleRequest {
    const message = createBaseQueryAllApplicationRoleRequest();
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageRequest.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

function createBaseQueryAllApplicationRoleResponse(): QueryAllApplicationRoleResponse {
  return { ApplicationRole: [], pagination: undefined };
}

export const QueryAllApplicationRoleResponse = {
  encode(message: QueryAllApplicationRoleResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.ApplicationRole) {
      ApplicationRole.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.pagination !== undefined) {
      PageResponse.encode(message.pagination, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryAllApplicationRoleResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryAllApplicationRoleResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.ApplicationRole.push(ApplicationRole.decode(reader, reader.uint32()));
          break;
        case 2:
          message.pagination = PageResponse.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllApplicationRoleResponse {
    return {
      ApplicationRole: Array.isArray(object?.ApplicationRole)
        ? object.ApplicationRole.map((e: any) => ApplicationRole.fromJSON(e))
        : [],
      pagination: isSet(object.pagination) ? PageResponse.fromJSON(object.pagination) : undefined,
    };
  },

  toJSON(message: QueryAllApplicationRoleResponse): unknown {
    const obj: any = {};
    if (message.ApplicationRole) {
      obj.ApplicationRole = message.ApplicationRole.map((e) => e ? ApplicationRole.toJSON(e) : undefined);
    } else {
      obj.ApplicationRole = [];
    }
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageResponse.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryAllApplicationRoleResponse>, I>>(
    object: I,
  ): QueryAllApplicationRoleResponse {
    const message = createBaseQueryAllApplicationRoleResponse();
    message.ApplicationRole = object.ApplicationRole?.map((e) => ApplicationRole.fromPartial(e)) || [];
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageResponse.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

/** Query defines the gRPC querier service. */
export interface Query {
  /** Queries a configuration by id. */
  Configuration(request: QueryGetConfigurationRequest): Promise<QueryGetConfigurationResponse>;
  /** option (google.api.http).get = "/silicon-economy/authorization/authorization/blockchainAccountState/{id}"; */
  BlockchainAccountState(
    request: QueryGetBlockchainAccountStateRequest,
  ): Promise<QueryGetBlockchainAccountStateResponse>;
  /** option (google.api.http).get = "/silicon-economy/authorization/authorization/blockchainAccountState"; */
  BlockchainAccountStateAll(
    request: QueryAllBlockchainAccountStateRequest,
  ): Promise<QueryAllBlockchainAccountStateResponse>;
  /** option (google.api.http).get = "/silicon-economy/authorization/authorization/applicationRoleState/{id}"; */
  ApplicationRoleState(request: QueryGetApplicationRoleStateRequest): Promise<QueryGetApplicationRoleStateResponse>;
  /** option (google.api.http).get = "/silicon-economy/authorization/authorization/applicationRoleState"; */
  ApplicationRoleStateAll(request: QueryAllApplicationRoleStateRequest): Promise<QueryAllApplicationRoleStateResponse>;
  /** option (google.api.http).get = "/silicon-economy/authorization/authorization/blockchainAccount/{id}"; */
  BlockchainAccount(request: QueryGetBlockchainAccountRequest): Promise<QueryGetBlockchainAccountResponse>;
  /** option (google.api.http).get = "/silicon-economy/authorization/authorization/blockchainAccount"; */
  BlockchainAccountAll(request: QueryAllBlockchainAccountRequest): Promise<QueryAllBlockchainAccountResponse>;
  /** option (google.api.http).get = "/silicon-economy/authorization/authorization/applicationRole/{id}"; */
  ApplicationRole(request: QueryGetApplicationRoleRequest): Promise<QueryGetApplicationRoleResponse>;
  /** option (google.api.http).get = "/silicon-economy/authorization/authorization/applicationRole"; */
  ApplicationRoleAll(request: QueryAllApplicationRoleRequest): Promise<QueryAllApplicationRoleResponse>;
}

export class QueryClientImpl implements Query {
  private readonly rpc: Rpc;
  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.Configuration = this.Configuration.bind(this);
    this.BlockchainAccountState = this.BlockchainAccountState.bind(this);
    this.BlockchainAccountStateAll = this.BlockchainAccountStateAll.bind(this);
    this.ApplicationRoleState = this.ApplicationRoleState.bind(this);
    this.ApplicationRoleStateAll = this.ApplicationRoleStateAll.bind(this);
    this.BlockchainAccount = this.BlockchainAccount.bind(this);
    this.BlockchainAccountAll = this.BlockchainAccountAll.bind(this);
    this.ApplicationRole = this.ApplicationRole.bind(this);
    this.ApplicationRoleAll = this.ApplicationRoleAll.bind(this);
  }
  Configuration(request: QueryGetConfigurationRequest): Promise<QueryGetConfigurationResponse> {
    const data = QueryGetConfigurationRequest.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.Query",
      "Configuration",
      data,
    );
    return promise.then((data) => QueryGetConfigurationResponse.decode(new _m0.Reader(data)));
  }

  BlockchainAccountState(
    request: QueryGetBlockchainAccountStateRequest,
  ): Promise<QueryGetBlockchainAccountStateResponse> {
    const data = QueryGetBlockchainAccountStateRequest.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.Query",
      "BlockchainAccountState",
      data,
    );
    return promise.then((data) => QueryGetBlockchainAccountStateResponse.decode(new _m0.Reader(data)));
  }

  BlockchainAccountStateAll(
    request: QueryAllBlockchainAccountStateRequest,
  ): Promise<QueryAllBlockchainAccountStateResponse> {
    const data = QueryAllBlockchainAccountStateRequest.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.Query",
      "BlockchainAccountStateAll",
      data,
    );
    return promise.then((data) => QueryAllBlockchainAccountStateResponse.decode(new _m0.Reader(data)));
  }

  ApplicationRoleState(request: QueryGetApplicationRoleStateRequest): Promise<QueryGetApplicationRoleStateResponse> {
    const data = QueryGetApplicationRoleStateRequest.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.Query",
      "ApplicationRoleState",
      data,
    );
    return promise.then((data) => QueryGetApplicationRoleStateResponse.decode(new _m0.Reader(data)));
  }

  ApplicationRoleStateAll(request: QueryAllApplicationRoleStateRequest): Promise<QueryAllApplicationRoleStateResponse> {
    const data = QueryAllApplicationRoleStateRequest.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.Query",
      "ApplicationRoleStateAll",
      data,
    );
    return promise.then((data) => QueryAllApplicationRoleStateResponse.decode(new _m0.Reader(data)));
  }

  BlockchainAccount(request: QueryGetBlockchainAccountRequest): Promise<QueryGetBlockchainAccountResponse> {
    const data = QueryGetBlockchainAccountRequest.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.Query",
      "BlockchainAccount",
      data,
    );
    return promise.then((data) => QueryGetBlockchainAccountResponse.decode(new _m0.Reader(data)));
  }

  BlockchainAccountAll(request: QueryAllBlockchainAccountRequest): Promise<QueryAllBlockchainAccountResponse> {
    const data = QueryAllBlockchainAccountRequest.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.Query",
      "BlockchainAccountAll",
      data,
    );
    return promise.then((data) => QueryAllBlockchainAccountResponse.decode(new _m0.Reader(data)));
  }

  ApplicationRole(request: QueryGetApplicationRoleRequest): Promise<QueryGetApplicationRoleResponse> {
    const data = QueryGetApplicationRoleRequest.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.Query",
      "ApplicationRole",
      data,
    );
    return promise.then((data) => QueryGetApplicationRoleResponse.decode(new _m0.Reader(data)));
  }

  ApplicationRoleAll(request: QueryAllApplicationRoleRequest): Promise<QueryAllApplicationRoleResponse> {
    const data = QueryAllApplicationRoleRequest.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.Query",
      "ApplicationRoleAll",
      data,
    );
    return promise.then((data) => QueryAllApplicationRoleResponse.decode(new _m0.Reader(data)));
  }
}

interface Rpc {
  request(service: string, method: string, data: Uint8Array): Promise<Uint8Array>;
}

declare var self: any | undefined;
declare var window: any | undefined;
declare var global: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== "undefined") {
    return globalThis;
  }
  if (typeof self !== "undefined") {
    return self;
  }
  if (typeof window !== "undefined") {
    return window;
  }
  if (typeof global !== "undefined") {
    return global;
  }
  throw "Unable to locate global object";
})();

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
  }
  return long.toNumber();
}

if (true) {
  _m0.util.Long = Long as any;
  _m0.configure();
}

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

import { GeneratedType } from "@cosmjs/proto-signing";
import { MsgCreateBlockchainAccountState } from "./types/authorization/tx";
import { MsgFetchApplicationRole } from "./types/authorization/tx";
import { MsgFetchBlockchainAccount } from "./types/authorization/tx";
import { MsgCreateApplicationRole } from "./types/authorization/tx";
import { MsgUpdateApplicationRoleState } from "./types/authorization/tx";
import { MsgCreateApplicationRoleState } from "./types/authorization/tx";
import { MsgGrantAppRoleToBlockchainAccount } from "./types/authorization/tx";
import { MsgDeactivateApplicationRole } from "./types/authorization/tx";
import { MsgUpdateConfiguration } from "./types/authorization/tx";
import { MsgDeleteBlockchainAccountState } from "./types/authorization/tx";
import { MsgDeleteApplicationRole } from "./types/authorization/tx";
import { MsgCreateBlockchainAccount } from "./types/authorization/tx";
import { MsgCreateConfiguration } from "./types/authorization/tx";
import { MsgUpdateBlockchainAccountState } from "./types/authorization/tx";
import { MsgDeactivateBlockchainAccount } from "./types/authorization/tx";
import { MsgUpdateApplicationRole } from "./types/authorization/tx";
import { MsgRevokeAppRoleFromBlockchainAccount } from "./types/authorization/tx";
import { MsgFetchAllApplicationRole } from "./types/authorization/tx";
import { MsgDeleteConfiguration } from "./types/authorization/tx";
import { MsgDeleteApplicationRoleState } from "./types/authorization/tx";
import { MsgFetchAllBlockchainAccount } from "./types/authorization/tx";

const msgTypes: Array<[string, GeneratedType]>  = [
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgCreateBlockchainAccountState", MsgCreateBlockchainAccountState],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgFetchApplicationRole", MsgFetchApplicationRole],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgFetchBlockchainAccount", MsgFetchBlockchainAccount],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgCreateApplicationRole", MsgCreateApplicationRole],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgUpdateApplicationRoleState", MsgUpdateApplicationRoleState],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgCreateApplicationRoleState", MsgCreateApplicationRoleState],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgGrantAppRoleToBlockchainAccount", MsgGrantAppRoleToBlockchainAccount],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgDeactivateApplicationRole", MsgDeactivateApplicationRole],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgUpdateConfiguration", MsgUpdateConfiguration],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgDeleteBlockchainAccountState", MsgDeleteBlockchainAccountState],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgDeleteApplicationRole", MsgDeleteApplicationRole],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgCreateBlockchainAccount", MsgCreateBlockchainAccount],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgCreateConfiguration", MsgCreateConfiguration],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgUpdateBlockchainAccountState", MsgUpdateBlockchainAccountState],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgDeactivateBlockchainAccount", MsgDeactivateBlockchainAccount],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgUpdateApplicationRole", MsgUpdateApplicationRole],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgRevokeAppRoleFromBlockchainAccount", MsgRevokeAppRoleFromBlockchainAccount],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgFetchAllApplicationRole", MsgFetchAllApplicationRole],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgDeleteConfiguration", MsgDeleteConfiguration],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgDeleteApplicationRoleState", MsgDeleteApplicationRoleState],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.authorization.MsgFetchAllBlockchainAccount", MsgFetchAllBlockchainAccount],
    
];

export { msgTypes }
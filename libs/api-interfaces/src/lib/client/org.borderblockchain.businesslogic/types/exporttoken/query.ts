/* eslint-disable */
import _m0 from "protobufjs/minimal";
import { PageRequest, PageResponse } from "../cosmos/base/query/v1beta1/pagination";
import { EventHistory, ExportEvent } from "./event-history";
import { ExportMapping } from "./export-mapping";
import { ExportToken } from "./exporttoken";

export const protobufPackage = "org.borderblockchain.exporttoken";

/** --- Token Message DTOs --- */
export interface QueryGetExportTokenRequest {
  id: string;
}

export interface QueryGetExportTokenResponse {
  exportToken: ExportToken | undefined;
  events: ExportEvent[];
}

export interface QueryAllExportTokenRequest {
  pagination: PageRequest | undefined;
}

export interface QueryAllExportTokenResponse {
  exportToken: QueryGetExportTokenResponse[];
  pagination: PageResponse | undefined;
}

/** --- Mapping Message DTOs--- */
export interface QueryGetExportMappingRequest {
  index: string;
}

export interface QueryGetExportMappingResponse {
  exportToken: ExportToken | undefined;
  events: ExportEvent[];
}

export interface QueryAllExportMappingRequest {
  pagination: PageRequest | undefined;
}

export interface QueryAllExportMappingResponse {
  exportMapping: ExportMapping[];
  pagination: PageResponse | undefined;
}

/** --- Token Message DTOs --- */
export interface QueryGetEventHistoryRequest {
  index: string;
}

export interface QueryGetEventHistoryResponse {
  eventHistory: EventHistory | undefined;
}

export interface QueryAllEventHistoryRequest {
  pagination: PageRequest | undefined;
}

export interface QueryAllEventHistoryResponse {
  eventHistory: EventHistory[];
  pagination: PageResponse | undefined;
}

function createBaseQueryGetExportTokenRequest(): QueryGetExportTokenRequest {
  return { id: "" };
}

export const QueryGetExportTokenRequest = {
  encode(message: QueryGetExportTokenRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.id !== "") {
      writer.uint32(10).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryGetExportTokenRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryGetExportTokenRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetExportTokenRequest {
    return { id: isSet(object.id) ? String(object.id) : "" };
  },

  toJSON(message: QueryGetExportTokenRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryGetExportTokenRequest>, I>>(object: I): QueryGetExportTokenRequest {
    const message = createBaseQueryGetExportTokenRequest();
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseQueryGetExportTokenResponse(): QueryGetExportTokenResponse {
  return { exportToken: undefined, events: [] };
}

export const QueryGetExportTokenResponse = {
  encode(message: QueryGetExportTokenResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.exportToken !== undefined) {
      ExportToken.encode(message.exportToken, writer.uint32(10).fork()).ldelim();
    }
    for (const v of message.events) {
      ExportEvent.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryGetExportTokenResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryGetExportTokenResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.exportToken = ExportToken.decode(reader, reader.uint32());
          break;
        case 2:
          message.events.push(ExportEvent.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetExportTokenResponse {
    return {
      exportToken: isSet(object.exportToken) ? ExportToken.fromJSON(object.exportToken) : undefined,
      events: Array.isArray(object?.events) ? object.events.map((e: any) => ExportEvent.fromJSON(e)) : [],
    };
  },

  toJSON(message: QueryGetExportTokenResponse): unknown {
    const obj: any = {};
    message.exportToken !== undefined
      && (obj.exportToken = message.exportToken ? ExportToken.toJSON(message.exportToken) : undefined);
    if (message.events) {
      obj.events = message.events.map((e) => e ? ExportEvent.toJSON(e) : undefined);
    } else {
      obj.events = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryGetExportTokenResponse>, I>>(object: I): QueryGetExportTokenResponse {
    const message = createBaseQueryGetExportTokenResponse();
    message.exportToken = (object.exportToken !== undefined && object.exportToken !== null)
      ? ExportToken.fromPartial(object.exportToken)
      : undefined;
    message.events = object.events?.map((e) => ExportEvent.fromPartial(e)) || [];
    return message;
  },
};

function createBaseQueryAllExportTokenRequest(): QueryAllExportTokenRequest {
  return { pagination: undefined };
}

export const QueryAllExportTokenRequest = {
  encode(message: QueryAllExportTokenRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.pagination !== undefined) {
      PageRequest.encode(message.pagination, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryAllExportTokenRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryAllExportTokenRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.pagination = PageRequest.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllExportTokenRequest {
    return { pagination: isSet(object.pagination) ? PageRequest.fromJSON(object.pagination) : undefined };
  },

  toJSON(message: QueryAllExportTokenRequest): unknown {
    const obj: any = {};
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageRequest.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryAllExportTokenRequest>, I>>(object: I): QueryAllExportTokenRequest {
    const message = createBaseQueryAllExportTokenRequest();
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageRequest.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

function createBaseQueryAllExportTokenResponse(): QueryAllExportTokenResponse {
  return { exportToken: [], pagination: undefined };
}

export const QueryAllExportTokenResponse = {
  encode(message: QueryAllExportTokenResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.exportToken) {
      QueryGetExportTokenResponse.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.pagination !== undefined) {
      PageResponse.encode(message.pagination, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryAllExportTokenResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryAllExportTokenResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.exportToken.push(QueryGetExportTokenResponse.decode(reader, reader.uint32()));
          break;
        case 2:
          message.pagination = PageResponse.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllExportTokenResponse {
    return {
      exportToken: Array.isArray(object?.exportToken)
        ? object.exportToken.map((e: any) => QueryGetExportTokenResponse.fromJSON(e))
        : [],
      pagination: isSet(object.pagination) ? PageResponse.fromJSON(object.pagination) : undefined,
    };
  },

  toJSON(message: QueryAllExportTokenResponse): unknown {
    const obj: any = {};
    if (message.exportToken) {
      obj.exportToken = message.exportToken.map((e) => e ? QueryGetExportTokenResponse.toJSON(e) : undefined);
    } else {
      obj.exportToken = [];
    }
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageResponse.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryAllExportTokenResponse>, I>>(object: I): QueryAllExportTokenResponse {
    const message = createBaseQueryAllExportTokenResponse();
    message.exportToken = object.exportToken?.map((e) => QueryGetExportTokenResponse.fromPartial(e)) || [];
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageResponse.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

function createBaseQueryGetExportMappingRequest(): QueryGetExportMappingRequest {
  return { index: "" };
}

export const QueryGetExportMappingRequest = {
  encode(message: QueryGetExportMappingRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.index !== "") {
      writer.uint32(10).string(message.index);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryGetExportMappingRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryGetExportMappingRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.index = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetExportMappingRequest {
    return { index: isSet(object.index) ? String(object.index) : "" };
  },

  toJSON(message: QueryGetExportMappingRequest): unknown {
    const obj: any = {};
    message.index !== undefined && (obj.index = message.index);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryGetExportMappingRequest>, I>>(object: I): QueryGetExportMappingRequest {
    const message = createBaseQueryGetExportMappingRequest();
    message.index = object.index ?? "";
    return message;
  },
};

function createBaseQueryGetExportMappingResponse(): QueryGetExportMappingResponse {
  return { exportToken: undefined, events: [] };
}

export const QueryGetExportMappingResponse = {
  encode(message: QueryGetExportMappingResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.exportToken !== undefined) {
      ExportToken.encode(message.exportToken, writer.uint32(10).fork()).ldelim();
    }
    for (const v of message.events) {
      ExportEvent.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryGetExportMappingResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryGetExportMappingResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.exportToken = ExportToken.decode(reader, reader.uint32());
          break;
        case 2:
          message.events.push(ExportEvent.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetExportMappingResponse {
    return {
      exportToken: isSet(object.exportToken) ? ExportToken.fromJSON(object.exportToken) : undefined,
      events: Array.isArray(object?.events) ? object.events.map((e: any) => ExportEvent.fromJSON(e)) : [],
    };
  },

  toJSON(message: QueryGetExportMappingResponse): unknown {
    const obj: any = {};
    message.exportToken !== undefined
      && (obj.exportToken = message.exportToken ? ExportToken.toJSON(message.exportToken) : undefined);
    if (message.events) {
      obj.events = message.events.map((e) => e ? ExportEvent.toJSON(e) : undefined);
    } else {
      obj.events = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryGetExportMappingResponse>, I>>(
    object: I,
  ): QueryGetExportMappingResponse {
    const message = createBaseQueryGetExportMappingResponse();
    message.exportToken = (object.exportToken !== undefined && object.exportToken !== null)
      ? ExportToken.fromPartial(object.exportToken)
      : undefined;
    message.events = object.events?.map((e) => ExportEvent.fromPartial(e)) || [];
    return message;
  },
};

function createBaseQueryAllExportMappingRequest(): QueryAllExportMappingRequest {
  return { pagination: undefined };
}

export const QueryAllExportMappingRequest = {
  encode(message: QueryAllExportMappingRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.pagination !== undefined) {
      PageRequest.encode(message.pagination, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryAllExportMappingRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryAllExportMappingRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.pagination = PageRequest.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllExportMappingRequest {
    return { pagination: isSet(object.pagination) ? PageRequest.fromJSON(object.pagination) : undefined };
  },

  toJSON(message: QueryAllExportMappingRequest): unknown {
    const obj: any = {};
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageRequest.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryAllExportMappingRequest>, I>>(object: I): QueryAllExportMappingRequest {
    const message = createBaseQueryAllExportMappingRequest();
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageRequest.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

function createBaseQueryAllExportMappingResponse(): QueryAllExportMappingResponse {
  return { exportMapping: [], pagination: undefined };
}

export const QueryAllExportMappingResponse = {
  encode(message: QueryAllExportMappingResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.exportMapping) {
      ExportMapping.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.pagination !== undefined) {
      PageResponse.encode(message.pagination, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryAllExportMappingResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryAllExportMappingResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.exportMapping.push(ExportMapping.decode(reader, reader.uint32()));
          break;
        case 2:
          message.pagination = PageResponse.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllExportMappingResponse {
    return {
      exportMapping: Array.isArray(object?.exportMapping)
        ? object.exportMapping.map((e: any) => ExportMapping.fromJSON(e))
        : [],
      pagination: isSet(object.pagination) ? PageResponse.fromJSON(object.pagination) : undefined,
    };
  },

  toJSON(message: QueryAllExportMappingResponse): unknown {
    const obj: any = {};
    if (message.exportMapping) {
      obj.exportMapping = message.exportMapping.map((e) => e ? ExportMapping.toJSON(e) : undefined);
    } else {
      obj.exportMapping = [];
    }
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageResponse.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryAllExportMappingResponse>, I>>(
    object: I,
  ): QueryAllExportMappingResponse {
    const message = createBaseQueryAllExportMappingResponse();
    message.exportMapping = object.exportMapping?.map((e) => ExportMapping.fromPartial(e)) || [];
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageResponse.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

function createBaseQueryGetEventHistoryRequest(): QueryGetEventHistoryRequest {
  return { index: "" };
}

export const QueryGetEventHistoryRequest = {
  encode(message: QueryGetEventHistoryRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.index !== "") {
      writer.uint32(10).string(message.index);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryGetEventHistoryRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryGetEventHistoryRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.index = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetEventHistoryRequest {
    return { index: isSet(object.index) ? String(object.index) : "" };
  },

  toJSON(message: QueryGetEventHistoryRequest): unknown {
    const obj: any = {};
    message.index !== undefined && (obj.index = message.index);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryGetEventHistoryRequest>, I>>(object: I): QueryGetEventHistoryRequest {
    const message = createBaseQueryGetEventHistoryRequest();
    message.index = object.index ?? "";
    return message;
  },
};

function createBaseQueryGetEventHistoryResponse(): QueryGetEventHistoryResponse {
  return { eventHistory: undefined };
}

export const QueryGetEventHistoryResponse = {
  encode(message: QueryGetEventHistoryResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.eventHistory !== undefined) {
      EventHistory.encode(message.eventHistory, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryGetEventHistoryResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryGetEventHistoryResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.eventHistory = EventHistory.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetEventHistoryResponse {
    return { eventHistory: isSet(object.eventHistory) ? EventHistory.fromJSON(object.eventHistory) : undefined };
  },

  toJSON(message: QueryGetEventHistoryResponse): unknown {
    const obj: any = {};
    message.eventHistory !== undefined
      && (obj.eventHistory = message.eventHistory ? EventHistory.toJSON(message.eventHistory) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryGetEventHistoryResponse>, I>>(object: I): QueryGetEventHistoryResponse {
    const message = createBaseQueryGetEventHistoryResponse();
    message.eventHistory = (object.eventHistory !== undefined && object.eventHistory !== null)
      ? EventHistory.fromPartial(object.eventHistory)
      : undefined;
    return message;
  },
};

function createBaseQueryAllEventHistoryRequest(): QueryAllEventHistoryRequest {
  return { pagination: undefined };
}

export const QueryAllEventHistoryRequest = {
  encode(message: QueryAllEventHistoryRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.pagination !== undefined) {
      PageRequest.encode(message.pagination, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryAllEventHistoryRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryAllEventHistoryRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.pagination = PageRequest.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllEventHistoryRequest {
    return { pagination: isSet(object.pagination) ? PageRequest.fromJSON(object.pagination) : undefined };
  },

  toJSON(message: QueryAllEventHistoryRequest): unknown {
    const obj: any = {};
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageRequest.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryAllEventHistoryRequest>, I>>(object: I): QueryAllEventHistoryRequest {
    const message = createBaseQueryAllEventHistoryRequest();
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageRequest.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

function createBaseQueryAllEventHistoryResponse(): QueryAllEventHistoryResponse {
  return { eventHistory: [], pagination: undefined };
}

export const QueryAllEventHistoryResponse = {
  encode(message: QueryAllEventHistoryResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.eventHistory) {
      EventHistory.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.pagination !== undefined) {
      PageResponse.encode(message.pagination, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryAllEventHistoryResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryAllEventHistoryResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.eventHistory.push(EventHistory.decode(reader, reader.uint32()));
          break;
        case 2:
          message.pagination = PageResponse.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllEventHistoryResponse {
    return {
      eventHistory: Array.isArray(object?.eventHistory)
        ? object.eventHistory.map((e: any) => EventHistory.fromJSON(e))
        : [],
      pagination: isSet(object.pagination) ? PageResponse.fromJSON(object.pagination) : undefined,
    };
  },

  toJSON(message: QueryAllEventHistoryResponse): unknown {
    const obj: any = {};
    if (message.eventHistory) {
      obj.eventHistory = message.eventHistory.map((e) => e ? EventHistory.toJSON(e) : undefined);
    } else {
      obj.eventHistory = [];
    }
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageResponse.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryAllEventHistoryResponse>, I>>(object: I): QueryAllEventHistoryResponse {
    const message = createBaseQueryAllEventHistoryResponse();
    message.eventHistory = object.eventHistory?.map((e) => EventHistory.fromPartial(e)) || [];
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageResponse.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

/** Query defines the gRPC querier service. */
export interface Query {
}

export class QueryClientImpl implements Query {
  private readonly rpc: Rpc;
  constructor(rpc: Rpc) {
    this.rpc = rpc;
  }
}

interface Rpc {
  request(service: string, method: string, data: Uint8Array): Promise<Uint8Array>;
}

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

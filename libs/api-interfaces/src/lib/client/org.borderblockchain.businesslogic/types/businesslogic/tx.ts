/* eslint-disable */
import Long from "long";
import _m0 from "protobufjs/minimal";
import { ExportEvent } from "../exporttoken/event-history";
import {
  Company as Company1,
  CustomsOffice as CustomsOffice2,
  ExportToken,
  GoodsItem as GoodsItem3,
  Transport as Transport4,
} from "../exporttoken/exporttoken";
import { ImportEvent } from "../importtoken/event-history";
import {
  Company as Company5,
  CustomsOffice as CustomsOffice6,
  GoodsItem as GoodsItem7,
  ImportToken,
  Transport as Transport8,
} from "../importtoken/importtoken";
import { TokenHistory } from "../token/tokenHistory";
import {
  MsgEmptyResponse as MsgEmptyResponse10,
  MsgFetchAllSegmentResponse,
  MsgFetchAllWalletResponse,
  MsgFetchGetSegmentHistoryResponse,
  MsgFetchGetSegmentResponse,
  MsgFetchGetWalletHistoryResponse,
  MsgFetchGetWalletResponse,
  MsgIdResponse as MsgIdResponse9,
} from "../tokenwallet/tx";

export const protobufPackage = "org.borderblockchain.businesslogic";

/**
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */

/** Views for redacted information */
export interface ExportTokenExporter {
  creator: string;
  id: string;
  exporter: Company | undefined;
  declarant: Company | undefined;
  representative: Company | undefined;
  consignee: Company | undefined;
  customsOfficeOfExport: CustomsOffice | undefined;
  customOfficeOfExit: CustomsOffice | undefined;
  goodsItems: GoodsItem[];
  transportToBorder: Transport | undefined;
  transportAtBorder: Transport | undefined;
  exportId: string;
  uniqueConsignmentReference: string;
  localReferenceNumber: string;
  destinationCountry: string;
  exportCountry: string;
  itinerary: string;
  releaseDateAndTime: string;
  incotermCode: string;
  incotermLocation: string;
  totalGrossMass: string;
  goodsItemQuantity: number;
  totalPackagesQuantity: number;
  natureOfTransaction: number;
  totalAmountInvoiced: number;
  invoiceCurrency: string;
}

export interface ExportTokenImporter {
  creator: string;
  id: string;
  exporter: Company | undefined;
  declarant: Company | undefined;
  representative: Company | undefined;
  consignee: Company | undefined;
  customsOfficeOfExport: CustomsOffice | undefined;
  customOfficeOfExit: CustomsOffice | undefined;
  goodsItems: GoodsItem[];
  transportToBorder: Transport | undefined;
  transportAtBorder: Transport | undefined;
  exportId: string;
  uniqueConsignmentReference: string;
  destinationCountry: string;
  exportCountry: string;
  itinerary: string;
  releaseDateAndTime: string;
  incotermCode: string;
  incotermLocation: string;
  totalGrossMass: string;
  goodsItemQuantity: number;
  totalPackagesQuantity: number;
  natureOfTransaction: number;
  totalAmountInvoiced: number;
  invoiceCurrency: string;
}

export interface ExportTokenDriver {
  creator: string;
  id: string;
  exporter: CompanyDriver | undefined;
  declarant: CompanyDriver | undefined;
  representative: CompanyDriver | undefined;
  consignee: CompanyDriver | undefined;
  customOfficeOfExit: CustomsOffice | undefined;
  goodsItems: GoodsItemDriver[];
  transportToBorder: TransportDriver | undefined;
  transportAtBorder: TransportDriver | undefined;
  exportId: string;
  uniqueConsignmentReference: string;
  destinationCountry: string;
  exportCountry: string;
  itinerary: string;
  totalGrossMass: string;
  goodsItemQuantity: number;
  totalPackagesQuantity: number;
}

export interface Company {
  address: Address | undefined;
  customsId: string;
  name: string;
  subsidiaryNumber: string;
  identificationNumber: string;
}

export interface CustomsOffice {
  address: Address | undefined;
  customsOfficeCode: string;
  customsOfficeName: string;
}

export interface Address {
  streetAndNumber: string;
  postcode: string;
  city: string;
  countryCode: string;
}

export interface GoodsItem {
  documents: Document[];
  countryOfOrigin: string;
  sequenceNumber: number;
  descriptionOfGoods: string;
  harmonizedSystemSubheadingCode: string;
  localClassificationCode: string;
  grossMass: string;
  netMass: string;
  numberOfPackages: number;
  typeOfPackages: string;
  marksNumbers: string;
  containerId: string;
  consigneeOrderNumber: string;
  valueAtBorderExport: number;
  valueAtBorderExportCurrency: string;
  amountInvoiced: number;
  amountInvoicedCurrency: string;
  dangerousGoodsCode: number;
}

export interface Transport {
  modeOfTransport: number;
  typeOfIdentification: number;
  identity: string;
  nationality: string;
  transportCost: number;
  transportCostCurrency: string;
  transportOrderNumber: string;
}

export interface Document {
  type: string;
  referenceNumber: string;
  complement: string;
  detail: string;
}

export interface CompanyDriver {
  address: Address | undefined;
  name: string;
}

export interface GoodsItemDriver {
  documents: Document[];
  countryOfOrigin: string;
  sequenceNumber: number;
  descriptionOfGoods: string;
  harmonizedSystemSubheadingCode: string;
  localClassificationCode: string;
  grossMass: string;
  netMass: string;
  numberOfPackages: number;
  typeOfPackages: string;
  marksNumbers: string;
  containerId: string;
  consigneeOrderNumber: string;
  dangerousGoodsCode: number;
}

export interface TransportDriver {
  modeOfTransport: number;
  typeOfIdentification: number;
  identity: string;
  nationality: string;
}

/** this line is used by starport scaffolding # proto/tx/message */
export interface MsgFetchAllSegmentHistory {
  creator: string;
}

export interface MsgFetchGetWallet {
  creator: string;
  id: string;
}

export interface MsgFetchGetSegment {
  creator: string;
  id: string;
}

export interface MsgFetchAllSegment {
  creator: string;
}

export interface MsgUpdateWallet {
  creator: string;
  id: string;
  name: string;
}

export interface MsgCreateWallet {
  creator: string;
  name: string;
}

export interface MsgUpdateSegment {
  creator: string;
  id: string;
  name: string;
  info: string;
}

export interface MsgFetchGetWalletHistory {
  creator: string;
  id: string;
}

export interface MsgCreateSegment {
  creator: string;
  name: string;
  info: string;
  walletId: string;
}

export interface MsgFetchAllTokenHistoryGlobal {
  creator: string;
}

export interface MsgFetchGetTokenHistoryGlobal {
  creator: string;
  id: string;
}

export interface MsgCreateWalletWithId {
  creator: string;
  id: string;
  name: string;
}

export interface MsgMoveTokenToSegment {
  creator: string;
  tokenRefId: string;
  sourceSegmentId: string;
  targetSegmentId: string;
}

export interface MsgFetchAllWalletHistory {
  creator: string;
}

export interface MsgFetchAllWallet {
  creator: string;
}

export interface MsgFetchSegmentHistory {
  creator: string;
  id: string;
}

export interface MsgCreateSegmentWithId {
  creator: string;
  id: string;
  name: string;
  info: string;
  walletId: string;
}

export interface MsgFetchTokensByWalletId {
  creator: string;
  id: string;
}

export interface MsgFetchTokensBySegmentId {
  creator: string;
  id: string;
}

export interface MsgFetchDocumentHash {
  creator: string;
  id: string;
}

export interface MsgCreateTokenCopies {
  creator: string;
  index: string;
  tokens: string[];
}

export interface MsgCreateTokenCopiesResponse {
}

export interface MsgUpdateTokenCopies {
  creator: string;
  index: string;
  tokens: string[];
}

export interface MsgUpdateTokenCopiesResponse {
}

export interface MsgDeleteTokenCopies {
  creator: string;
  index: string;
}

export interface MsgDeleteTokenCopiesResponse {
}

export interface MsgCreateDocumentTokenMapper {
  creator: string;
  documentId: string;
  tokenId: string;
}

export interface MsgCreateDocumentTokenMapperResponse {
}

export interface MsgUpdateDocumentTokenMapper {
  creator: string;
  documentId: string;
  tokenId: string;
}

export interface MsgUpdateDocumentTokenMapperResponse {
}

export interface MsgDeleteDocumentTokenMapper {
  creator: string;
  index: string;
}

export interface MsgDeleteDocumentTokenMapperResponse {
}

export interface MsgMoveTokenToWallet {
  creator: string;
  tokenRefId: string;
  sourceSegmentId: string;
  targetSegmentId: string;
}

export interface MsgCreateToken {
  creator: string;
  tokenType: string;
  changeMessage: string;
  segmentId: string;
  moduleRef: string;
}

export interface MsgUpdateToken {
  creator: string;
  tokenRefId: string;
  tokenType: string;
  changeMessage: string;
  segmentId: string;
  moduleRef: string;
}

export interface MsgIdResponse {
  id: string;
}

export interface MsgActivateToken {
  creator: string;
  id: string;
  segmentId: string;
  moduleRef: string;
}

export interface MsgDeactivateToken {
  creator: string;
  id: string;
}

export interface MsgCreateHashToken {
  creator: string;
  changeMessage: string;
  segmentId: string;
  document: string;
  hash: string;
  hashFunction: string;
  metadata: string;
}

export interface MsgCloneToken {
  creator: string;
  tokenId: string;
  walletId: string;
  moduleRef: string;
}

export interface MsgRevertToGenesis {
  creator: string;
}

export interface MsgEmptyResponse {
}

export interface MsgCreateExportToken {
  creator: string;
  id: string;
  exporter: Company1 | undefined;
  declarant: Company1 | undefined;
  representative: Company1 | undefined;
  consignee: Company1 | undefined;
  customsOfficeOfExport: CustomsOffice2 | undefined;
  customOfficeOfExit: CustomsOffice2 | undefined;
  goodsItems: GoodsItem3[];
  transportToBorder: Transport4 | undefined;
  transportAtBorder: Transport4 | undefined;
  exportId: string;
  uniqueConsignmentReference: string;
  localReferenceNumber: string;
  destinationCountry: string;
  exportCountry: string;
  itinerary: string;
  releaseDateAndTime: string;
  incotermCode: string;
  incotermLocation: string;
  totalGrossMass: string;
  goodsItemQuantity: number;
  totalPackagesQuantity: number;
  natureOfTransaction: number;
  totalAmountInvoiced: number;
  invoiceCurrency: string;
  user: string;
  timestamp: string;
}

/**
 * This Messages is used in place of an update on the Token.
 * If you query a token the Event History will be appended and sent alongside the
 */
export interface MsgCreateExportEvent {
  creator: string;
  index: string;
  status: string;
  message: string;
  eventType: string;
}

/** We might want to return a Response in the future. */
export interface MsgCreateExportEventResponse {
}

/** Accept a newthat has been written by a third party (Status 0 -> 1) */
export interface MsgForwardExportToken {
  creator: string;
  id: string;
}

export interface MsgForwardExportTokenResponse {
  exportToken: ExportToken | undefined;
  events: ExportEvent[];
}

export interface MsgEventPresentQrCode {
  creator: string;
  id: string;
  message: string;
}

/** Scan the QR code and accept the transport (Status 2 -> 3) */
export interface MsgEventAcceptGoodsForTransport {
  creator: string;
  id: string;
  message: string;
}

/** Scan the QR code and accept the transport (Status 3 -> 4) */
export interface MsgEventPresentGoods {
  creator: string;
  id: string;
  message: string;
}

/** Scan the QR code and accept the transport (Status 4 -> 5) */
export interface MsgEventCertifyExitOfGoods {
  creator: string;
  id: string;
  message: string;
}

/** Scan the QR code and accept the transport (Status 5 -> 6) */
export interface MsgEventMoveToArchive {
  creator: string;
  id: string;
  message: string;
}

/** Export checked by officials */
export interface MsgEventExportCheck {
  creator: string;
  id: string;
  message: string;
}

/** Accept Export by Import */
export interface MsgEventAcceptExport {
  creator: string;
  id: string;
  message: string;
}

/** Refuse Export by Import */
export interface MsgEventExportRefusal {
  creator: string;
  id: string;
  message: string;
}

/** Accept Export by Import (Status 0 -> 1) */
export interface MsgEventAcceptImport {
  creator: string;
  id: string;
  message: string;
}

/** Status 1 -> 2 */
export interface MsgEventCompleteImport {
  creator: string;
  id: string;
  message: string;
}

/** Status 1 -> 2 */
export interface MsgEventImportProposal {
  creator: string;
  id: string;
  message: string;
}

/** Event for when the Exportis refused */
export interface MsgEventImportRefusal {
  creator: string;
  id: string;
  message: string;
}

/** Event for when the Importis updated with additional info */
export interface MsgEventImportSupplement {
  creator: string;
  id: string;
  message: string;
}

/** SOON TO BE IMPLEMENTED */
export interface MsgEventImportExport {
  creator: string;
  id: string;
  message: string;
}

/** TO BE IMPLEMENTED */
export interface MsgEventImportSubmit {
  creator: string;
  id: string;
  message: string;
}

/** TO BE IMPLEMENTED */
export interface MsgEventImportRelease {
  creator: string;
  id: string;
  message: string;
}

/** Collective ResponseObject for status changes */
export interface MsgEventStatusResponseExport {
  exportToken: ExportToken | undefined;
  events: ExportEvent[];
}

/** Collective ResponseObject for status changes */
export interface MsgEventStatusResponseImport {
  importToken: ImportToken | undefined;
  events: ImportEvent[];
}

export interface MsgCreateExportTokenResponse {
  exportToken: ExportToken | undefined;
  events: ExportEvent[];
}

export interface MsgUpdateExportToken {
  creator: string;
  id: string;
  exporter: Company1 | undefined;
  declarant: Company1 | undefined;
  representative: Company1 | undefined;
  consignee: Company1 | undefined;
  customsOfficeOfExport: CustomsOffice2 | undefined;
  customOfficeOfExit: CustomsOffice2 | undefined;
  goodsItems: GoodsItem3[];
  transportToBorder: Transport4 | undefined;
  transportAtBorder: Transport4 | undefined;
  exportId: string;
  uniqueConsignmentReference: string;
  localReferenceNumber: string;
  destinationCountry: string;
  exportCountry: string;
  itinerary: string;
  releaseDateAndTime: string;
  incotermCode: string;
  incotermLocation: string;
  totalGrossMass: string;
  goodsItemQuantity: number;
  totalPackagesQuantity: number;
  natureOfTransaction: number;
  totalAmountInvoiced: number;
  invoiceCurrency: string;
  status: string;
  user: string;
  timestamp: string;
}

export interface MsgUpdateExportTokenResponse {
  exportToken: ExportToken | undefined;
  events: ExportEvent[];
}

export interface MsgDeleteExportTokenResponse {
}

/**
 * This Messages is used in place of an update on the Token.
 * If you query a token the Event History will be appended and sent alongside the Token.
 */
export interface MsgCreateImportEvent {
  creator: string;
  index: string;
  status: string;
  message: string;
  eventType: string;
}

/** We might want to return a Response in the future. */
export interface MsgCreateImportEventResponse {
}

export interface MsgCreateImportToken {
  creator: string;
  id: string;
  consignor: Company5 | undefined;
  exporter: Company5 | undefined;
  consignee: Company5 | undefined;
  declarant: Company5 | undefined;
  customOfficeOfExit: CustomsOffice6 | undefined;
  customOfficeOfEntry: CustomsOffice6 | undefined;
  customOfficeOfImport: CustomsOffice6 | undefined;
  goodsItems: GoodsItem7[];
  transportAtBorder: Transport8 | undefined;
  transportFromBorder: Transport8 | undefined;
  importId: string;
  uniqueConsignmentReference: string;
  localReferenceNumber: string;
  destinationCountry: string;
  exportCountry: string;
  itinerary: string;
  incotermCode: string;
  incotermLocation: string;
  totalGrossMass: string;
  goodsItemQuantity: number;
  totalPackagesQuantity: number;
  releaseDateAndTime: string;
  natureOfTransaction: number;
  totalAmountInvoiced: number;
  invoiceCurrency: string;
  status: string;
  relatedExportToken: string;
  tokenWalletId: string;
}

export interface MsgCreateImportTokenResponse {
  importToken: ImportToken | undefined;
  events: ImportEvent[];
}

export interface MsgUpdateImportToken {
  creator: string;
  id: string;
  consignor: Company5 | undefined;
  exporter: Company5 | undefined;
  consignee: Company5 | undefined;
  declarant: Company5 | undefined;
  customOfficeOfExit: CustomsOffice6 | undefined;
  customOfficeOfEntry: CustomsOffice6 | undefined;
  customOfficeOfImport: CustomsOffice6 | undefined;
  goodsItems: GoodsItem7[];
  transportAtBorder: Transport8 | undefined;
  transportFromBorder: Transport8 | undefined;
  importId: string;
  uniqueConsignmentReference: string;
  localReferenceNumber: string;
  destinationCountry: string;
  exportCountry: string;
  itinerary: string;
  incotermCode: string;
  incotermLocation: string;
  totalGrossMass: string;
  goodsItemQuantity: number;
  totalPackagesQuantity: number;
  releaseDateAndTime: string;
  natureOfTransaction: number;
  totalAmountInvoiced: number;
  invoiceCurrency: string;
  status: string;
  relatedExportToken: string;
  eventType: string;
}

export interface MsgUpdateImportTokenResponse {
  importToken: ImportToken | undefined;
  events: ImportEvent[];
}

export interface MsgDeleteImportToken {
  creator: string;
  id: string;
}

export interface MsgDeleteImportTokenResponse {
}

/** Fetching all ExportTokens */
export interface MsgFetchExportTokenAllExporter {
  creator: string;
  timestamp: string;
  tokenWalletId: string;
}

export interface MsgFetchExportTokenAllDriver {
  creator: string;
  timestamp: string;
}

export interface MsgFetchExportTokenAllImporter {
  creator: string;
  timestamp: string;
  companyName: string;
  companyCountry: string;
  identificationNumber: string;
}

export interface MsgFetchExportTokenAllResponse {
  tokens: MsgFetchExportTokenResponse[];
}

export interface MsgFetchExportTokenAllExporterResponse {
  tokens: MsgFetchExportTokenExporterResponse[];
}

export interface MsgFetchExportTokenAllDriverResponse {
  tokens: MsgFetchExportTokenDriverResponse[];
}

export interface MsgFetchExportTokenAllImporterResponse {
  tokens: MsgFetchExportTokenImporterResponse[];
}

/** Fetch specific ExportToken */
export interface MsgFetchExportTokenExporter {
  creator: string;
  id: string;
  timestamp: string;
  tokenWalletId: string;
}

export interface MsgFetchExportTokenDriver {
  creator: string;
  id: string;
  timestamp: string;
}

export interface MsgFetchExportTokenImporter {
  creator: string;
  id: string;
  timestamp: string;
  companyName: string;
  companyCountry: string;
}

export interface MsgFetchExportTokenExporterByExportId {
  creator: string;
  exportId: string;
  timestamp: string;
  tokenWalletId: string;
}

export interface MsgFetchExportTokenDriverByExportId {
  creator: string;
  exportId: string;
  timestamp: string;
}

export interface MsgFetchExportTokenImporterByExportId {
  creator: string;
  exportId: string;
  timestamp: string;
  companyName: string;
  companyCountry: string;
}

export interface MsgFetchExportTokenResponse {
  exportToken: ExportToken | undefined;
  events: ExportEvent[];
}

export interface MsgFetchExportTokenExporterResponse {
  exportToken: ExportTokenExporter | undefined;
  events: ExportEvent[];
}

export interface MsgFetchExportTokenDriverResponse {
  exportToken: ExportTokenDriver | undefined;
  events: ExportEvent[];
}

export interface MsgFetchExportTokenImporterResponse {
  exportToken: ExportTokenImporter | undefined;
  events: ExportEvent[];
}

/** Fetching Import Tokens */
export interface MsgFetchImportTokenAll {
  creator: string;
  timestamp: string;
  tokenWalletId: string;
}

export interface MsgFetchImportTokenAllResponse {
  tokens: MsgCreateImportTokenResponse[];
}

export interface MsgFetchImportToken {
  creator: string;
  id: string;
  timestamp: string;
  tokenWalletId: string;
}

export interface MsgFetchImportTokenByImportId {
  creator: string;
  importId: string;
  timestamp: string;
  tokenWalletId: string;
}

export interface MsgFetchImportTokenResponse {
  importToken: ImportToken | undefined;
  events: ImportEvent[];
}

export interface MsgAssignCosmosAddressToWallet {
  creator: string;
  cosmosAddress: string;
  walletId: string;
}

export interface MsgFetchTokenHistory {
  creator: string;
  id: string;
}

export interface MsgFetchTokenHistoryResponse {
  TokenHistory: TokenHistory | undefined;
}

function createBaseExportTokenExporter(): ExportTokenExporter {
  return {
    creator: "",
    id: "",
    exporter: undefined,
    declarant: undefined,
    representative: undefined,
    consignee: undefined,
    customsOfficeOfExport: undefined,
    customOfficeOfExit: undefined,
    goodsItems: [],
    transportToBorder: undefined,
    transportAtBorder: undefined,
    exportId: "",
    uniqueConsignmentReference: "",
    localReferenceNumber: "",
    destinationCountry: "",
    exportCountry: "",
    itinerary: "",
    releaseDateAndTime: "",
    incotermCode: "",
    incotermLocation: "",
    totalGrossMass: "",
    goodsItemQuantity: 0,
    totalPackagesQuantity: 0,
    natureOfTransaction: 0,
    totalAmountInvoiced: 0,
    invoiceCurrency: "",
  };
}

export const ExportTokenExporter = {
  encode(message: ExportTokenExporter, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.exporter !== undefined) {
      Company.encode(message.exporter, writer.uint32(26).fork()).ldelim();
    }
    if (message.declarant !== undefined) {
      Company.encode(message.declarant, writer.uint32(34).fork()).ldelim();
    }
    if (message.representative !== undefined) {
      Company.encode(message.representative, writer.uint32(42).fork()).ldelim();
    }
    if (message.consignee !== undefined) {
      Company.encode(message.consignee, writer.uint32(50).fork()).ldelim();
    }
    if (message.customsOfficeOfExport !== undefined) {
      CustomsOffice.encode(message.customsOfficeOfExport, writer.uint32(58).fork()).ldelim();
    }
    if (message.customOfficeOfExit !== undefined) {
      CustomsOffice.encode(message.customOfficeOfExit, writer.uint32(66).fork()).ldelim();
    }
    for (const v of message.goodsItems) {
      GoodsItem.encode(v!, writer.uint32(74).fork()).ldelim();
    }
    if (message.transportToBorder !== undefined) {
      Transport.encode(message.transportToBorder, writer.uint32(82).fork()).ldelim();
    }
    if (message.transportAtBorder !== undefined) {
      Transport.encode(message.transportAtBorder, writer.uint32(90).fork()).ldelim();
    }
    if (message.exportId !== "") {
      writer.uint32(98).string(message.exportId);
    }
    if (message.uniqueConsignmentReference !== "") {
      writer.uint32(106).string(message.uniqueConsignmentReference);
    }
    if (message.localReferenceNumber !== "") {
      writer.uint32(114).string(message.localReferenceNumber);
    }
    if (message.destinationCountry !== "") {
      writer.uint32(122).string(message.destinationCountry);
    }
    if (message.exportCountry !== "") {
      writer.uint32(130).string(message.exportCountry);
    }
    if (message.itinerary !== "") {
      writer.uint32(138).string(message.itinerary);
    }
    if (message.releaseDateAndTime !== "") {
      writer.uint32(146).string(message.releaseDateAndTime);
    }
    if (message.incotermCode !== "") {
      writer.uint32(154).string(message.incotermCode);
    }
    if (message.incotermLocation !== "") {
      writer.uint32(162).string(message.incotermLocation);
    }
    if (message.totalGrossMass !== "") {
      writer.uint32(170).string(message.totalGrossMass);
    }
    if (message.goodsItemQuantity !== 0) {
      writer.uint32(176).int64(message.goodsItemQuantity);
    }
    if (message.totalPackagesQuantity !== 0) {
      writer.uint32(184).int64(message.totalPackagesQuantity);
    }
    if (message.natureOfTransaction !== 0) {
      writer.uint32(192).int64(message.natureOfTransaction);
    }
    if (message.totalAmountInvoiced !== 0) {
      writer.uint32(200).int64(message.totalAmountInvoiced);
    }
    if (message.invoiceCurrency !== "") {
      writer.uint32(210).string(message.invoiceCurrency);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ExportTokenExporter {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseExportTokenExporter();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.exporter = Company.decode(reader, reader.uint32());
          break;
        case 4:
          message.declarant = Company.decode(reader, reader.uint32());
          break;
        case 5:
          message.representative = Company.decode(reader, reader.uint32());
          break;
        case 6:
          message.consignee = Company.decode(reader, reader.uint32());
          break;
        case 7:
          message.customsOfficeOfExport = CustomsOffice.decode(reader, reader.uint32());
          break;
        case 8:
          message.customOfficeOfExit = CustomsOffice.decode(reader, reader.uint32());
          break;
        case 9:
          message.goodsItems.push(GoodsItem.decode(reader, reader.uint32()));
          break;
        case 10:
          message.transportToBorder = Transport.decode(reader, reader.uint32());
          break;
        case 11:
          message.transportAtBorder = Transport.decode(reader, reader.uint32());
          break;
        case 12:
          message.exportId = reader.string();
          break;
        case 13:
          message.uniqueConsignmentReference = reader.string();
          break;
        case 14:
          message.localReferenceNumber = reader.string();
          break;
        case 15:
          message.destinationCountry = reader.string();
          break;
        case 16:
          message.exportCountry = reader.string();
          break;
        case 17:
          message.itinerary = reader.string();
          break;
        case 18:
          message.releaseDateAndTime = reader.string();
          break;
        case 19:
          message.incotermCode = reader.string();
          break;
        case 20:
          message.incotermLocation = reader.string();
          break;
        case 21:
          message.totalGrossMass = reader.string();
          break;
        case 22:
          message.goodsItemQuantity = longToNumber(reader.int64() as Long);
          break;
        case 23:
          message.totalPackagesQuantity = longToNumber(reader.int64() as Long);
          break;
        case 24:
          message.natureOfTransaction = longToNumber(reader.int64() as Long);
          break;
        case 25:
          message.totalAmountInvoiced = longToNumber(reader.int64() as Long);
          break;
        case 26:
          message.invoiceCurrency = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ExportTokenExporter {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      exporter: isSet(object.exporter) ? Company.fromJSON(object.exporter) : undefined,
      declarant: isSet(object.declarant) ? Company.fromJSON(object.declarant) : undefined,
      representative: isSet(object.representative) ? Company.fromJSON(object.representative) : undefined,
      consignee: isSet(object.consignee) ? Company.fromJSON(object.consignee) : undefined,
      customsOfficeOfExport: isSet(object.customsOfficeOfExport)
        ? CustomsOffice.fromJSON(object.customsOfficeOfExport)
        : undefined,
      customOfficeOfExit: isSet(object.customOfficeOfExit)
        ? CustomsOffice.fromJSON(object.customOfficeOfExit)
        : undefined,
      goodsItems: Array.isArray(object?.goodsItems) ? object.goodsItems.map((e: any) => GoodsItem.fromJSON(e)) : [],
      transportToBorder: isSet(object.transportToBorder) ? Transport.fromJSON(object.transportToBorder) : undefined,
      transportAtBorder: isSet(object.transportAtBorder) ? Transport.fromJSON(object.transportAtBorder) : undefined,
      exportId: isSet(object.exportId) ? String(object.exportId) : "",
      uniqueConsignmentReference: isSet(object.uniqueConsignmentReference)
        ? String(object.uniqueConsignmentReference)
        : "",
      localReferenceNumber: isSet(object.localReferenceNumber) ? String(object.localReferenceNumber) : "",
      destinationCountry: isSet(object.destinationCountry) ? String(object.destinationCountry) : "",
      exportCountry: isSet(object.exportCountry) ? String(object.exportCountry) : "",
      itinerary: isSet(object.itinerary) ? String(object.itinerary) : "",
      releaseDateAndTime: isSet(object.releaseDateAndTime) ? String(object.releaseDateAndTime) : "",
      incotermCode: isSet(object.incotermCode) ? String(object.incotermCode) : "",
      incotermLocation: isSet(object.incotermLocation) ? String(object.incotermLocation) : "",
      totalGrossMass: isSet(object.totalGrossMass) ? String(object.totalGrossMass) : "",
      goodsItemQuantity: isSet(object.goodsItemQuantity) ? Number(object.goodsItemQuantity) : 0,
      totalPackagesQuantity: isSet(object.totalPackagesQuantity) ? Number(object.totalPackagesQuantity) : 0,
      natureOfTransaction: isSet(object.natureOfTransaction) ? Number(object.natureOfTransaction) : 0,
      totalAmountInvoiced: isSet(object.totalAmountInvoiced) ? Number(object.totalAmountInvoiced) : 0,
      invoiceCurrency: isSet(object.invoiceCurrency) ? String(object.invoiceCurrency) : "",
    };
  },

  toJSON(message: ExportTokenExporter): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.exporter !== undefined && (obj.exporter = message.exporter ? Company.toJSON(message.exporter) : undefined);
    message.declarant !== undefined
      && (obj.declarant = message.declarant ? Company.toJSON(message.declarant) : undefined);
    message.representative !== undefined
      && (obj.representative = message.representative ? Company.toJSON(message.representative) : undefined);
    message.consignee !== undefined
      && (obj.consignee = message.consignee ? Company.toJSON(message.consignee) : undefined);
    message.customsOfficeOfExport !== undefined && (obj.customsOfficeOfExport = message.customsOfficeOfExport
      ? CustomsOffice.toJSON(message.customsOfficeOfExport)
      : undefined);
    message.customOfficeOfExit !== undefined && (obj.customOfficeOfExit = message.customOfficeOfExit
      ? CustomsOffice.toJSON(message.customOfficeOfExit)
      : undefined);
    if (message.goodsItems) {
      obj.goodsItems = message.goodsItems.map((e) => e ? GoodsItem.toJSON(e) : undefined);
    } else {
      obj.goodsItems = [];
    }
    message.transportToBorder !== undefined
      && (obj.transportToBorder = message.transportToBorder ? Transport.toJSON(message.transportToBorder) : undefined);
    message.transportAtBorder !== undefined
      && (obj.transportAtBorder = message.transportAtBorder ? Transport.toJSON(message.transportAtBorder) : undefined);
    message.exportId !== undefined && (obj.exportId = message.exportId);
    message.uniqueConsignmentReference !== undefined
      && (obj.uniqueConsignmentReference = message.uniqueConsignmentReference);
    message.localReferenceNumber !== undefined && (obj.localReferenceNumber = message.localReferenceNumber);
    message.destinationCountry !== undefined && (obj.destinationCountry = message.destinationCountry);
    message.exportCountry !== undefined && (obj.exportCountry = message.exportCountry);
    message.itinerary !== undefined && (obj.itinerary = message.itinerary);
    message.releaseDateAndTime !== undefined && (obj.releaseDateAndTime = message.releaseDateAndTime);
    message.incotermCode !== undefined && (obj.incotermCode = message.incotermCode);
    message.incotermLocation !== undefined && (obj.incotermLocation = message.incotermLocation);
    message.totalGrossMass !== undefined && (obj.totalGrossMass = message.totalGrossMass);
    message.goodsItemQuantity !== undefined && (obj.goodsItemQuantity = Math.round(message.goodsItemQuantity));
    message.totalPackagesQuantity !== undefined
      && (obj.totalPackagesQuantity = Math.round(message.totalPackagesQuantity));
    message.natureOfTransaction !== undefined && (obj.natureOfTransaction = Math.round(message.natureOfTransaction));
    message.totalAmountInvoiced !== undefined && (obj.totalAmountInvoiced = Math.round(message.totalAmountInvoiced));
    message.invoiceCurrency !== undefined && (obj.invoiceCurrency = message.invoiceCurrency);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<ExportTokenExporter>, I>>(object: I): ExportTokenExporter {
    const message = createBaseExportTokenExporter();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.exporter = (object.exporter !== undefined && object.exporter !== null)
      ? Company.fromPartial(object.exporter)
      : undefined;
    message.declarant = (object.declarant !== undefined && object.declarant !== null)
      ? Company.fromPartial(object.declarant)
      : undefined;
    message.representative = (object.representative !== undefined && object.representative !== null)
      ? Company.fromPartial(object.representative)
      : undefined;
    message.consignee = (object.consignee !== undefined && object.consignee !== null)
      ? Company.fromPartial(object.consignee)
      : undefined;
    message.customsOfficeOfExport =
      (object.customsOfficeOfExport !== undefined && object.customsOfficeOfExport !== null)
        ? CustomsOffice.fromPartial(object.customsOfficeOfExport)
        : undefined;
    message.customOfficeOfExit = (object.customOfficeOfExit !== undefined && object.customOfficeOfExit !== null)
      ? CustomsOffice.fromPartial(object.customOfficeOfExit)
      : undefined;
    message.goodsItems = object.goodsItems?.map((e) => GoodsItem.fromPartial(e)) || [];
    message.transportToBorder = (object.transportToBorder !== undefined && object.transportToBorder !== null)
      ? Transport.fromPartial(object.transportToBorder)
      : undefined;
    message.transportAtBorder = (object.transportAtBorder !== undefined && object.transportAtBorder !== null)
      ? Transport.fromPartial(object.transportAtBorder)
      : undefined;
    message.exportId = object.exportId ?? "";
    message.uniqueConsignmentReference = object.uniqueConsignmentReference ?? "";
    message.localReferenceNumber = object.localReferenceNumber ?? "";
    message.destinationCountry = object.destinationCountry ?? "";
    message.exportCountry = object.exportCountry ?? "";
    message.itinerary = object.itinerary ?? "";
    message.releaseDateAndTime = object.releaseDateAndTime ?? "";
    message.incotermCode = object.incotermCode ?? "";
    message.incotermLocation = object.incotermLocation ?? "";
    message.totalGrossMass = object.totalGrossMass ?? "";
    message.goodsItemQuantity = object.goodsItemQuantity ?? 0;
    message.totalPackagesQuantity = object.totalPackagesQuantity ?? 0;
    message.natureOfTransaction = object.natureOfTransaction ?? 0;
    message.totalAmountInvoiced = object.totalAmountInvoiced ?? 0;
    message.invoiceCurrency = object.invoiceCurrency ?? "";
    return message;
  },
};

function createBaseExportTokenImporter(): ExportTokenImporter {
  return {
    creator: "",
    id: "",
    exporter: undefined,
    declarant: undefined,
    representative: undefined,
    consignee: undefined,
    customsOfficeOfExport: undefined,
    customOfficeOfExit: undefined,
    goodsItems: [],
    transportToBorder: undefined,
    transportAtBorder: undefined,
    exportId: "",
    uniqueConsignmentReference: "",
    destinationCountry: "",
    exportCountry: "",
    itinerary: "",
    releaseDateAndTime: "",
    incotermCode: "",
    incotermLocation: "",
    totalGrossMass: "",
    goodsItemQuantity: 0,
    totalPackagesQuantity: 0,
    natureOfTransaction: 0,
    totalAmountInvoiced: 0,
    invoiceCurrency: "",
  };
}

export const ExportTokenImporter = {
  encode(message: ExportTokenImporter, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.exporter !== undefined) {
      Company.encode(message.exporter, writer.uint32(26).fork()).ldelim();
    }
    if (message.declarant !== undefined) {
      Company.encode(message.declarant, writer.uint32(34).fork()).ldelim();
    }
    if (message.representative !== undefined) {
      Company.encode(message.representative, writer.uint32(42).fork()).ldelim();
    }
    if (message.consignee !== undefined) {
      Company.encode(message.consignee, writer.uint32(50).fork()).ldelim();
    }
    if (message.customsOfficeOfExport !== undefined) {
      CustomsOffice.encode(message.customsOfficeOfExport, writer.uint32(58).fork()).ldelim();
    }
    if (message.customOfficeOfExit !== undefined) {
      CustomsOffice.encode(message.customOfficeOfExit, writer.uint32(66).fork()).ldelim();
    }
    for (const v of message.goodsItems) {
      GoodsItem.encode(v!, writer.uint32(74).fork()).ldelim();
    }
    if (message.transportToBorder !== undefined) {
      Transport.encode(message.transportToBorder, writer.uint32(82).fork()).ldelim();
    }
    if (message.transportAtBorder !== undefined) {
      Transport.encode(message.transportAtBorder, writer.uint32(90).fork()).ldelim();
    }
    if (message.exportId !== "") {
      writer.uint32(98).string(message.exportId);
    }
    if (message.uniqueConsignmentReference !== "") {
      writer.uint32(106).string(message.uniqueConsignmentReference);
    }
    if (message.destinationCountry !== "") {
      writer.uint32(122).string(message.destinationCountry);
    }
    if (message.exportCountry !== "") {
      writer.uint32(130).string(message.exportCountry);
    }
    if (message.itinerary !== "") {
      writer.uint32(138).string(message.itinerary);
    }
    if (message.releaseDateAndTime !== "") {
      writer.uint32(146).string(message.releaseDateAndTime);
    }
    if (message.incotermCode !== "") {
      writer.uint32(154).string(message.incotermCode);
    }
    if (message.incotermLocation !== "") {
      writer.uint32(162).string(message.incotermLocation);
    }
    if (message.totalGrossMass !== "") {
      writer.uint32(170).string(message.totalGrossMass);
    }
    if (message.goodsItemQuantity !== 0) {
      writer.uint32(176).int64(message.goodsItemQuantity);
    }
    if (message.totalPackagesQuantity !== 0) {
      writer.uint32(184).int64(message.totalPackagesQuantity);
    }
    if (message.natureOfTransaction !== 0) {
      writer.uint32(192).int64(message.natureOfTransaction);
    }
    if (message.totalAmountInvoiced !== 0) {
      writer.uint32(200).int64(message.totalAmountInvoiced);
    }
    if (message.invoiceCurrency !== "") {
      writer.uint32(210).string(message.invoiceCurrency);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ExportTokenImporter {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseExportTokenImporter();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.exporter = Company.decode(reader, reader.uint32());
          break;
        case 4:
          message.declarant = Company.decode(reader, reader.uint32());
          break;
        case 5:
          message.representative = Company.decode(reader, reader.uint32());
          break;
        case 6:
          message.consignee = Company.decode(reader, reader.uint32());
          break;
        case 7:
          message.customsOfficeOfExport = CustomsOffice.decode(reader, reader.uint32());
          break;
        case 8:
          message.customOfficeOfExit = CustomsOffice.decode(reader, reader.uint32());
          break;
        case 9:
          message.goodsItems.push(GoodsItem.decode(reader, reader.uint32()));
          break;
        case 10:
          message.transportToBorder = Transport.decode(reader, reader.uint32());
          break;
        case 11:
          message.transportAtBorder = Transport.decode(reader, reader.uint32());
          break;
        case 12:
          message.exportId = reader.string();
          break;
        case 13:
          message.uniqueConsignmentReference = reader.string();
          break;
        case 15:
          message.destinationCountry = reader.string();
          break;
        case 16:
          message.exportCountry = reader.string();
          break;
        case 17:
          message.itinerary = reader.string();
          break;
        case 18:
          message.releaseDateAndTime = reader.string();
          break;
        case 19:
          message.incotermCode = reader.string();
          break;
        case 20:
          message.incotermLocation = reader.string();
          break;
        case 21:
          message.totalGrossMass = reader.string();
          break;
        case 22:
          message.goodsItemQuantity = longToNumber(reader.int64() as Long);
          break;
        case 23:
          message.totalPackagesQuantity = longToNumber(reader.int64() as Long);
          break;
        case 24:
          message.natureOfTransaction = longToNumber(reader.int64() as Long);
          break;
        case 25:
          message.totalAmountInvoiced = longToNumber(reader.int64() as Long);
          break;
        case 26:
          message.invoiceCurrency = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ExportTokenImporter {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      exporter: isSet(object.exporter) ? Company.fromJSON(object.exporter) : undefined,
      declarant: isSet(object.declarant) ? Company.fromJSON(object.declarant) : undefined,
      representative: isSet(object.representative) ? Company.fromJSON(object.representative) : undefined,
      consignee: isSet(object.consignee) ? Company.fromJSON(object.consignee) : undefined,
      customsOfficeOfExport: isSet(object.customsOfficeOfExport)
        ? CustomsOffice.fromJSON(object.customsOfficeOfExport)
        : undefined,
      customOfficeOfExit: isSet(object.customOfficeOfExit)
        ? CustomsOffice.fromJSON(object.customOfficeOfExit)
        : undefined,
      goodsItems: Array.isArray(object?.goodsItems) ? object.goodsItems.map((e: any) => GoodsItem.fromJSON(e)) : [],
      transportToBorder: isSet(object.transportToBorder) ? Transport.fromJSON(object.transportToBorder) : undefined,
      transportAtBorder: isSet(object.transportAtBorder) ? Transport.fromJSON(object.transportAtBorder) : undefined,
      exportId: isSet(object.exportId) ? String(object.exportId) : "",
      uniqueConsignmentReference: isSet(object.uniqueConsignmentReference)
        ? String(object.uniqueConsignmentReference)
        : "",
      destinationCountry: isSet(object.destinationCountry) ? String(object.destinationCountry) : "",
      exportCountry: isSet(object.exportCountry) ? String(object.exportCountry) : "",
      itinerary: isSet(object.itinerary) ? String(object.itinerary) : "",
      releaseDateAndTime: isSet(object.releaseDateAndTime) ? String(object.releaseDateAndTime) : "",
      incotermCode: isSet(object.incotermCode) ? String(object.incotermCode) : "",
      incotermLocation: isSet(object.incotermLocation) ? String(object.incotermLocation) : "",
      totalGrossMass: isSet(object.totalGrossMass) ? String(object.totalGrossMass) : "",
      goodsItemQuantity: isSet(object.goodsItemQuantity) ? Number(object.goodsItemQuantity) : 0,
      totalPackagesQuantity: isSet(object.totalPackagesQuantity) ? Number(object.totalPackagesQuantity) : 0,
      natureOfTransaction: isSet(object.natureOfTransaction) ? Number(object.natureOfTransaction) : 0,
      totalAmountInvoiced: isSet(object.totalAmountInvoiced) ? Number(object.totalAmountInvoiced) : 0,
      invoiceCurrency: isSet(object.invoiceCurrency) ? String(object.invoiceCurrency) : "",
    };
  },

  toJSON(message: ExportTokenImporter): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.exporter !== undefined && (obj.exporter = message.exporter ? Company.toJSON(message.exporter) : undefined);
    message.declarant !== undefined
      && (obj.declarant = message.declarant ? Company.toJSON(message.declarant) : undefined);
    message.representative !== undefined
      && (obj.representative = message.representative ? Company.toJSON(message.representative) : undefined);
    message.consignee !== undefined
      && (obj.consignee = message.consignee ? Company.toJSON(message.consignee) : undefined);
    message.customsOfficeOfExport !== undefined && (obj.customsOfficeOfExport = message.customsOfficeOfExport
      ? CustomsOffice.toJSON(message.customsOfficeOfExport)
      : undefined);
    message.customOfficeOfExit !== undefined && (obj.customOfficeOfExit = message.customOfficeOfExit
      ? CustomsOffice.toJSON(message.customOfficeOfExit)
      : undefined);
    if (message.goodsItems) {
      obj.goodsItems = message.goodsItems.map((e) => e ? GoodsItem.toJSON(e) : undefined);
    } else {
      obj.goodsItems = [];
    }
    message.transportToBorder !== undefined
      && (obj.transportToBorder = message.transportToBorder ? Transport.toJSON(message.transportToBorder) : undefined);
    message.transportAtBorder !== undefined
      && (obj.transportAtBorder = message.transportAtBorder ? Transport.toJSON(message.transportAtBorder) : undefined);
    message.exportId !== undefined && (obj.exportId = message.exportId);
    message.uniqueConsignmentReference !== undefined
      && (obj.uniqueConsignmentReference = message.uniqueConsignmentReference);
    message.destinationCountry !== undefined && (obj.destinationCountry = message.destinationCountry);
    message.exportCountry !== undefined && (obj.exportCountry = message.exportCountry);
    message.itinerary !== undefined && (obj.itinerary = message.itinerary);
    message.releaseDateAndTime !== undefined && (obj.releaseDateAndTime = message.releaseDateAndTime);
    message.incotermCode !== undefined && (obj.incotermCode = message.incotermCode);
    message.incotermLocation !== undefined && (obj.incotermLocation = message.incotermLocation);
    message.totalGrossMass !== undefined && (obj.totalGrossMass = message.totalGrossMass);
    message.goodsItemQuantity !== undefined && (obj.goodsItemQuantity = Math.round(message.goodsItemQuantity));
    message.totalPackagesQuantity !== undefined
      && (obj.totalPackagesQuantity = Math.round(message.totalPackagesQuantity));
    message.natureOfTransaction !== undefined && (obj.natureOfTransaction = Math.round(message.natureOfTransaction));
    message.totalAmountInvoiced !== undefined && (obj.totalAmountInvoiced = Math.round(message.totalAmountInvoiced));
    message.invoiceCurrency !== undefined && (obj.invoiceCurrency = message.invoiceCurrency);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<ExportTokenImporter>, I>>(object: I): ExportTokenImporter {
    const message = createBaseExportTokenImporter();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.exporter = (object.exporter !== undefined && object.exporter !== null)
      ? Company.fromPartial(object.exporter)
      : undefined;
    message.declarant = (object.declarant !== undefined && object.declarant !== null)
      ? Company.fromPartial(object.declarant)
      : undefined;
    message.representative = (object.representative !== undefined && object.representative !== null)
      ? Company.fromPartial(object.representative)
      : undefined;
    message.consignee = (object.consignee !== undefined && object.consignee !== null)
      ? Company.fromPartial(object.consignee)
      : undefined;
    message.customsOfficeOfExport =
      (object.customsOfficeOfExport !== undefined && object.customsOfficeOfExport !== null)
        ? CustomsOffice.fromPartial(object.customsOfficeOfExport)
        : undefined;
    message.customOfficeOfExit = (object.customOfficeOfExit !== undefined && object.customOfficeOfExit !== null)
      ? CustomsOffice.fromPartial(object.customOfficeOfExit)
      : undefined;
    message.goodsItems = object.goodsItems?.map((e) => GoodsItem.fromPartial(e)) || [];
    message.transportToBorder = (object.transportToBorder !== undefined && object.transportToBorder !== null)
      ? Transport.fromPartial(object.transportToBorder)
      : undefined;
    message.transportAtBorder = (object.transportAtBorder !== undefined && object.transportAtBorder !== null)
      ? Transport.fromPartial(object.transportAtBorder)
      : undefined;
    message.exportId = object.exportId ?? "";
    message.uniqueConsignmentReference = object.uniqueConsignmentReference ?? "";
    message.destinationCountry = object.destinationCountry ?? "";
    message.exportCountry = object.exportCountry ?? "";
    message.itinerary = object.itinerary ?? "";
    message.releaseDateAndTime = object.releaseDateAndTime ?? "";
    message.incotermCode = object.incotermCode ?? "";
    message.incotermLocation = object.incotermLocation ?? "";
    message.totalGrossMass = object.totalGrossMass ?? "";
    message.goodsItemQuantity = object.goodsItemQuantity ?? 0;
    message.totalPackagesQuantity = object.totalPackagesQuantity ?? 0;
    message.natureOfTransaction = object.natureOfTransaction ?? 0;
    message.totalAmountInvoiced = object.totalAmountInvoiced ?? 0;
    message.invoiceCurrency = object.invoiceCurrency ?? "";
    return message;
  },
};

function createBaseExportTokenDriver(): ExportTokenDriver {
  return {
    creator: "",
    id: "",
    exporter: undefined,
    declarant: undefined,
    representative: undefined,
    consignee: undefined,
    customOfficeOfExit: undefined,
    goodsItems: [],
    transportToBorder: undefined,
    transportAtBorder: undefined,
    exportId: "",
    uniqueConsignmentReference: "",
    destinationCountry: "",
    exportCountry: "",
    itinerary: "",
    totalGrossMass: "",
    goodsItemQuantity: 0,
    totalPackagesQuantity: 0,
  };
}

export const ExportTokenDriver = {
  encode(message: ExportTokenDriver, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.exporter !== undefined) {
      CompanyDriver.encode(message.exporter, writer.uint32(26).fork()).ldelim();
    }
    if (message.declarant !== undefined) {
      CompanyDriver.encode(message.declarant, writer.uint32(34).fork()).ldelim();
    }
    if (message.representative !== undefined) {
      CompanyDriver.encode(message.representative, writer.uint32(42).fork()).ldelim();
    }
    if (message.consignee !== undefined) {
      CompanyDriver.encode(message.consignee, writer.uint32(50).fork()).ldelim();
    }
    if (message.customOfficeOfExit !== undefined) {
      CustomsOffice.encode(message.customOfficeOfExit, writer.uint32(66).fork()).ldelim();
    }
    for (const v of message.goodsItems) {
      GoodsItemDriver.encode(v!, writer.uint32(74).fork()).ldelim();
    }
    if (message.transportToBorder !== undefined) {
      TransportDriver.encode(message.transportToBorder, writer.uint32(82).fork()).ldelim();
    }
    if (message.transportAtBorder !== undefined) {
      TransportDriver.encode(message.transportAtBorder, writer.uint32(90).fork()).ldelim();
    }
    if (message.exportId !== "") {
      writer.uint32(98).string(message.exportId);
    }
    if (message.uniqueConsignmentReference !== "") {
      writer.uint32(106).string(message.uniqueConsignmentReference);
    }
    if (message.destinationCountry !== "") {
      writer.uint32(122).string(message.destinationCountry);
    }
    if (message.exportCountry !== "") {
      writer.uint32(130).string(message.exportCountry);
    }
    if (message.itinerary !== "") {
      writer.uint32(138).string(message.itinerary);
    }
    if (message.totalGrossMass !== "") {
      writer.uint32(170).string(message.totalGrossMass);
    }
    if (message.goodsItemQuantity !== 0) {
      writer.uint32(176).int64(message.goodsItemQuantity);
    }
    if (message.totalPackagesQuantity !== 0) {
      writer.uint32(184).int64(message.totalPackagesQuantity);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ExportTokenDriver {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseExportTokenDriver();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.exporter = CompanyDriver.decode(reader, reader.uint32());
          break;
        case 4:
          message.declarant = CompanyDriver.decode(reader, reader.uint32());
          break;
        case 5:
          message.representative = CompanyDriver.decode(reader, reader.uint32());
          break;
        case 6:
          message.consignee = CompanyDriver.decode(reader, reader.uint32());
          break;
        case 8:
          message.customOfficeOfExit = CustomsOffice.decode(reader, reader.uint32());
          break;
        case 9:
          message.goodsItems.push(GoodsItemDriver.decode(reader, reader.uint32()));
          break;
        case 10:
          message.transportToBorder = TransportDriver.decode(reader, reader.uint32());
          break;
        case 11:
          message.transportAtBorder = TransportDriver.decode(reader, reader.uint32());
          break;
        case 12:
          message.exportId = reader.string();
          break;
        case 13:
          message.uniqueConsignmentReference = reader.string();
          break;
        case 15:
          message.destinationCountry = reader.string();
          break;
        case 16:
          message.exportCountry = reader.string();
          break;
        case 17:
          message.itinerary = reader.string();
          break;
        case 21:
          message.totalGrossMass = reader.string();
          break;
        case 22:
          message.goodsItemQuantity = longToNumber(reader.int64() as Long);
          break;
        case 23:
          message.totalPackagesQuantity = longToNumber(reader.int64() as Long);
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ExportTokenDriver {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      exporter: isSet(object.exporter) ? CompanyDriver.fromJSON(object.exporter) : undefined,
      declarant: isSet(object.declarant) ? CompanyDriver.fromJSON(object.declarant) : undefined,
      representative: isSet(object.representative) ? CompanyDriver.fromJSON(object.representative) : undefined,
      consignee: isSet(object.consignee) ? CompanyDriver.fromJSON(object.consignee) : undefined,
      customOfficeOfExit: isSet(object.customOfficeOfExit)
        ? CustomsOffice.fromJSON(object.customOfficeOfExit)
        : undefined,
      goodsItems: Array.isArray(object?.goodsItems)
        ? object.goodsItems.map((e: any) => GoodsItemDriver.fromJSON(e))
        : [],
      transportToBorder: isSet(object.transportToBorder)
        ? TransportDriver.fromJSON(object.transportToBorder)
        : undefined,
      transportAtBorder: isSet(object.transportAtBorder)
        ? TransportDriver.fromJSON(object.transportAtBorder)
        : undefined,
      exportId: isSet(object.exportId) ? String(object.exportId) : "",
      uniqueConsignmentReference: isSet(object.uniqueConsignmentReference)
        ? String(object.uniqueConsignmentReference)
        : "",
      destinationCountry: isSet(object.destinationCountry) ? String(object.destinationCountry) : "",
      exportCountry: isSet(object.exportCountry) ? String(object.exportCountry) : "",
      itinerary: isSet(object.itinerary) ? String(object.itinerary) : "",
      totalGrossMass: isSet(object.totalGrossMass) ? String(object.totalGrossMass) : "",
      goodsItemQuantity: isSet(object.goodsItemQuantity) ? Number(object.goodsItemQuantity) : 0,
      totalPackagesQuantity: isSet(object.totalPackagesQuantity) ? Number(object.totalPackagesQuantity) : 0,
    };
  },

  toJSON(message: ExportTokenDriver): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.exporter !== undefined
      && (obj.exporter = message.exporter ? CompanyDriver.toJSON(message.exporter) : undefined);
    message.declarant !== undefined
      && (obj.declarant = message.declarant ? CompanyDriver.toJSON(message.declarant) : undefined);
    message.representative !== undefined
      && (obj.representative = message.representative ? CompanyDriver.toJSON(message.representative) : undefined);
    message.consignee !== undefined
      && (obj.consignee = message.consignee ? CompanyDriver.toJSON(message.consignee) : undefined);
    message.customOfficeOfExit !== undefined && (obj.customOfficeOfExit = message.customOfficeOfExit
      ? CustomsOffice.toJSON(message.customOfficeOfExit)
      : undefined);
    if (message.goodsItems) {
      obj.goodsItems = message.goodsItems.map((e) => e ? GoodsItemDriver.toJSON(e) : undefined);
    } else {
      obj.goodsItems = [];
    }
    message.transportToBorder !== undefined && (obj.transportToBorder = message.transportToBorder
      ? TransportDriver.toJSON(message.transportToBorder)
      : undefined);
    message.transportAtBorder !== undefined && (obj.transportAtBorder = message.transportAtBorder
      ? TransportDriver.toJSON(message.transportAtBorder)
      : undefined);
    message.exportId !== undefined && (obj.exportId = message.exportId);
    message.uniqueConsignmentReference !== undefined
      && (obj.uniqueConsignmentReference = message.uniqueConsignmentReference);
    message.destinationCountry !== undefined && (obj.destinationCountry = message.destinationCountry);
    message.exportCountry !== undefined && (obj.exportCountry = message.exportCountry);
    message.itinerary !== undefined && (obj.itinerary = message.itinerary);
    message.totalGrossMass !== undefined && (obj.totalGrossMass = message.totalGrossMass);
    message.goodsItemQuantity !== undefined && (obj.goodsItemQuantity = Math.round(message.goodsItemQuantity));
    message.totalPackagesQuantity !== undefined
      && (obj.totalPackagesQuantity = Math.round(message.totalPackagesQuantity));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<ExportTokenDriver>, I>>(object: I): ExportTokenDriver {
    const message = createBaseExportTokenDriver();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.exporter = (object.exporter !== undefined && object.exporter !== null)
      ? CompanyDriver.fromPartial(object.exporter)
      : undefined;
    message.declarant = (object.declarant !== undefined && object.declarant !== null)
      ? CompanyDriver.fromPartial(object.declarant)
      : undefined;
    message.representative = (object.representative !== undefined && object.representative !== null)
      ? CompanyDriver.fromPartial(object.representative)
      : undefined;
    message.consignee = (object.consignee !== undefined && object.consignee !== null)
      ? CompanyDriver.fromPartial(object.consignee)
      : undefined;
    message.customOfficeOfExit = (object.customOfficeOfExit !== undefined && object.customOfficeOfExit !== null)
      ? CustomsOffice.fromPartial(object.customOfficeOfExit)
      : undefined;
    message.goodsItems = object.goodsItems?.map((e) => GoodsItemDriver.fromPartial(e)) || [];
    message.transportToBorder = (object.transportToBorder !== undefined && object.transportToBorder !== null)
      ? TransportDriver.fromPartial(object.transportToBorder)
      : undefined;
    message.transportAtBorder = (object.transportAtBorder !== undefined && object.transportAtBorder !== null)
      ? TransportDriver.fromPartial(object.transportAtBorder)
      : undefined;
    message.exportId = object.exportId ?? "";
    message.uniqueConsignmentReference = object.uniqueConsignmentReference ?? "";
    message.destinationCountry = object.destinationCountry ?? "";
    message.exportCountry = object.exportCountry ?? "";
    message.itinerary = object.itinerary ?? "";
    message.totalGrossMass = object.totalGrossMass ?? "";
    message.goodsItemQuantity = object.goodsItemQuantity ?? 0;
    message.totalPackagesQuantity = object.totalPackagesQuantity ?? 0;
    return message;
  },
};

function createBaseCompany(): Company {
  return { address: undefined, customsId: "", name: "", subsidiaryNumber: "", identificationNumber: "" };
}

export const Company = {
  encode(message: Company, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.address !== undefined) {
      Address.encode(message.address, writer.uint32(10).fork()).ldelim();
    }
    if (message.customsId !== "") {
      writer.uint32(18).string(message.customsId);
    }
    if (message.name !== "") {
      writer.uint32(26).string(message.name);
    }
    if (message.subsidiaryNumber !== "") {
      writer.uint32(34).string(message.subsidiaryNumber);
    }
    if (message.identificationNumber !== "") {
      writer.uint32(42).string(message.identificationNumber);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Company {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseCompany();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.address = Address.decode(reader, reader.uint32());
          break;
        case 2:
          message.customsId = reader.string();
          break;
        case 3:
          message.name = reader.string();
          break;
        case 4:
          message.subsidiaryNumber = reader.string();
          break;
        case 5:
          message.identificationNumber = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Company {
    return {
      address: isSet(object.address) ? Address.fromJSON(object.address) : undefined,
      customsId: isSet(object.customsId) ? String(object.customsId) : "",
      name: isSet(object.name) ? String(object.name) : "",
      subsidiaryNumber: isSet(object.subsidiaryNumber) ? String(object.subsidiaryNumber) : "",
      identificationNumber: isSet(object.identificationNumber) ? String(object.identificationNumber) : "",
    };
  },

  toJSON(message: Company): unknown {
    const obj: any = {};
    message.address !== undefined && (obj.address = message.address ? Address.toJSON(message.address) : undefined);
    message.customsId !== undefined && (obj.customsId = message.customsId);
    message.name !== undefined && (obj.name = message.name);
    message.subsidiaryNumber !== undefined && (obj.subsidiaryNumber = message.subsidiaryNumber);
    message.identificationNumber !== undefined && (obj.identificationNumber = message.identificationNumber);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<Company>, I>>(object: I): Company {
    const message = createBaseCompany();
    message.address = (object.address !== undefined && object.address !== null)
      ? Address.fromPartial(object.address)
      : undefined;
    message.customsId = object.customsId ?? "";
    message.name = object.name ?? "";
    message.subsidiaryNumber = object.subsidiaryNumber ?? "";
    message.identificationNumber = object.identificationNumber ?? "";
    return message;
  },
};

function createBaseCustomsOffice(): CustomsOffice {
  return { address: undefined, customsOfficeCode: "", customsOfficeName: "" };
}

export const CustomsOffice = {
  encode(message: CustomsOffice, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.address !== undefined) {
      Address.encode(message.address, writer.uint32(10).fork()).ldelim();
    }
    if (message.customsOfficeCode !== "") {
      writer.uint32(18).string(message.customsOfficeCode);
    }
    if (message.customsOfficeName !== "") {
      writer.uint32(26).string(message.customsOfficeName);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): CustomsOffice {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseCustomsOffice();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.address = Address.decode(reader, reader.uint32());
          break;
        case 2:
          message.customsOfficeCode = reader.string();
          break;
        case 3:
          message.customsOfficeName = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CustomsOffice {
    return {
      address: isSet(object.address) ? Address.fromJSON(object.address) : undefined,
      customsOfficeCode: isSet(object.customsOfficeCode) ? String(object.customsOfficeCode) : "",
      customsOfficeName: isSet(object.customsOfficeName) ? String(object.customsOfficeName) : "",
    };
  },

  toJSON(message: CustomsOffice): unknown {
    const obj: any = {};
    message.address !== undefined && (obj.address = message.address ? Address.toJSON(message.address) : undefined);
    message.customsOfficeCode !== undefined && (obj.customsOfficeCode = message.customsOfficeCode);
    message.customsOfficeName !== undefined && (obj.customsOfficeName = message.customsOfficeName);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<CustomsOffice>, I>>(object: I): CustomsOffice {
    const message = createBaseCustomsOffice();
    message.address = (object.address !== undefined && object.address !== null)
      ? Address.fromPartial(object.address)
      : undefined;
    message.customsOfficeCode = object.customsOfficeCode ?? "";
    message.customsOfficeName = object.customsOfficeName ?? "";
    return message;
  },
};

function createBaseAddress(): Address {
  return { streetAndNumber: "", postcode: "", city: "", countryCode: "" };
}

export const Address = {
  encode(message: Address, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.streetAndNumber !== "") {
      writer.uint32(10).string(message.streetAndNumber);
    }
    if (message.postcode !== "") {
      writer.uint32(18).string(message.postcode);
    }
    if (message.city !== "") {
      writer.uint32(26).string(message.city);
    }
    if (message.countryCode !== "") {
      writer.uint32(34).string(message.countryCode);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Address {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseAddress();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.streetAndNumber = reader.string();
          break;
        case 2:
          message.postcode = reader.string();
          break;
        case 3:
          message.city = reader.string();
          break;
        case 4:
          message.countryCode = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Address {
    return {
      streetAndNumber: isSet(object.streetAndNumber) ? String(object.streetAndNumber) : "",
      postcode: isSet(object.postcode) ? String(object.postcode) : "",
      city: isSet(object.city) ? String(object.city) : "",
      countryCode: isSet(object.countryCode) ? String(object.countryCode) : "",
    };
  },

  toJSON(message: Address): unknown {
    const obj: any = {};
    message.streetAndNumber !== undefined && (obj.streetAndNumber = message.streetAndNumber);
    message.postcode !== undefined && (obj.postcode = message.postcode);
    message.city !== undefined && (obj.city = message.city);
    message.countryCode !== undefined && (obj.countryCode = message.countryCode);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<Address>, I>>(object: I): Address {
    const message = createBaseAddress();
    message.streetAndNumber = object.streetAndNumber ?? "";
    message.postcode = object.postcode ?? "";
    message.city = object.city ?? "";
    message.countryCode = object.countryCode ?? "";
    return message;
  },
};

function createBaseGoodsItem(): GoodsItem {
  return {
    documents: [],
    countryOfOrigin: "",
    sequenceNumber: 0,
    descriptionOfGoods: "",
    harmonizedSystemSubheadingCode: "",
    localClassificationCode: "",
    grossMass: "",
    netMass: "",
    numberOfPackages: 0,
    typeOfPackages: "",
    marksNumbers: "",
    containerId: "",
    consigneeOrderNumber: "",
    valueAtBorderExport: 0,
    valueAtBorderExportCurrency: "",
    amountInvoiced: 0,
    amountInvoicedCurrency: "",
    dangerousGoodsCode: 0,
  };
}

export const GoodsItem = {
  encode(message: GoodsItem, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.documents) {
      Document.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.countryOfOrigin !== "") {
      writer.uint32(18).string(message.countryOfOrigin);
    }
    if (message.sequenceNumber !== 0) {
      writer.uint32(24).int64(message.sequenceNumber);
    }
    if (message.descriptionOfGoods !== "") {
      writer.uint32(34).string(message.descriptionOfGoods);
    }
    if (message.harmonizedSystemSubheadingCode !== "") {
      writer.uint32(42).string(message.harmonizedSystemSubheadingCode);
    }
    if (message.localClassificationCode !== "") {
      writer.uint32(50).string(message.localClassificationCode);
    }
    if (message.grossMass !== "") {
      writer.uint32(58).string(message.grossMass);
    }
    if (message.netMass !== "") {
      writer.uint32(66).string(message.netMass);
    }
    if (message.numberOfPackages !== 0) {
      writer.uint32(72).int64(message.numberOfPackages);
    }
    if (message.typeOfPackages !== "") {
      writer.uint32(82).string(message.typeOfPackages);
    }
    if (message.marksNumbers !== "") {
      writer.uint32(90).string(message.marksNumbers);
    }
    if (message.containerId !== "") {
      writer.uint32(98).string(message.containerId);
    }
    if (message.consigneeOrderNumber !== "") {
      writer.uint32(106).string(message.consigneeOrderNumber);
    }
    if (message.valueAtBorderExport !== 0) {
      writer.uint32(112).int64(message.valueAtBorderExport);
    }
    if (message.valueAtBorderExportCurrency !== "") {
      writer.uint32(122).string(message.valueAtBorderExportCurrency);
    }
    if (message.amountInvoiced !== 0) {
      writer.uint32(128).int64(message.amountInvoiced);
    }
    if (message.amountInvoicedCurrency !== "") {
      writer.uint32(138).string(message.amountInvoicedCurrency);
    }
    if (message.dangerousGoodsCode !== 0) {
      writer.uint32(144).int64(message.dangerousGoodsCode);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): GoodsItem {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseGoodsItem();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.documents.push(Document.decode(reader, reader.uint32()));
          break;
        case 2:
          message.countryOfOrigin = reader.string();
          break;
        case 3:
          message.sequenceNumber = longToNumber(reader.int64() as Long);
          break;
        case 4:
          message.descriptionOfGoods = reader.string();
          break;
        case 5:
          message.harmonizedSystemSubheadingCode = reader.string();
          break;
        case 6:
          message.localClassificationCode = reader.string();
          break;
        case 7:
          message.grossMass = reader.string();
          break;
        case 8:
          message.netMass = reader.string();
          break;
        case 9:
          message.numberOfPackages = longToNumber(reader.int64() as Long);
          break;
        case 10:
          message.typeOfPackages = reader.string();
          break;
        case 11:
          message.marksNumbers = reader.string();
          break;
        case 12:
          message.containerId = reader.string();
          break;
        case 13:
          message.consigneeOrderNumber = reader.string();
          break;
        case 14:
          message.valueAtBorderExport = longToNumber(reader.int64() as Long);
          break;
        case 15:
          message.valueAtBorderExportCurrency = reader.string();
          break;
        case 16:
          message.amountInvoiced = longToNumber(reader.int64() as Long);
          break;
        case 17:
          message.amountInvoicedCurrency = reader.string();
          break;
        case 18:
          message.dangerousGoodsCode = longToNumber(reader.int64() as Long);
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GoodsItem {
    return {
      documents: Array.isArray(object?.documents) ? object.documents.map((e: any) => Document.fromJSON(e)) : [],
      countryOfOrigin: isSet(object.countryOfOrigin) ? String(object.countryOfOrigin) : "",
      sequenceNumber: isSet(object.sequenceNumber) ? Number(object.sequenceNumber) : 0,
      descriptionOfGoods: isSet(object.descriptionOfGoods) ? String(object.descriptionOfGoods) : "",
      harmonizedSystemSubheadingCode: isSet(object.harmonizedSystemSubheadingCode)
        ? String(object.harmonizedSystemSubheadingCode)
        : "",
      localClassificationCode: isSet(object.localClassificationCode) ? String(object.localClassificationCode) : "",
      grossMass: isSet(object.grossMass) ? String(object.grossMass) : "",
      netMass: isSet(object.netMass) ? String(object.netMass) : "",
      numberOfPackages: isSet(object.numberOfPackages) ? Number(object.numberOfPackages) : 0,
      typeOfPackages: isSet(object.typeOfPackages) ? String(object.typeOfPackages) : "",
      marksNumbers: isSet(object.marksNumbers) ? String(object.marksNumbers) : "",
      containerId: isSet(object.containerId) ? String(object.containerId) : "",
      consigneeOrderNumber: isSet(object.consigneeOrderNumber) ? String(object.consigneeOrderNumber) : "",
      valueAtBorderExport: isSet(object.valueAtBorderExport) ? Number(object.valueAtBorderExport) : 0,
      valueAtBorderExportCurrency: isSet(object.valueAtBorderExportCurrency)
        ? String(object.valueAtBorderExportCurrency)
        : "",
      amountInvoiced: isSet(object.amountInvoiced) ? Number(object.amountInvoiced) : 0,
      amountInvoicedCurrency: isSet(object.amountInvoicedCurrency) ? String(object.amountInvoicedCurrency) : "",
      dangerousGoodsCode: isSet(object.dangerousGoodsCode) ? Number(object.dangerousGoodsCode) : 0,
    };
  },

  toJSON(message: GoodsItem): unknown {
    const obj: any = {};
    if (message.documents) {
      obj.documents = message.documents.map((e) => e ? Document.toJSON(e) : undefined);
    } else {
      obj.documents = [];
    }
    message.countryOfOrigin !== undefined && (obj.countryOfOrigin = message.countryOfOrigin);
    message.sequenceNumber !== undefined && (obj.sequenceNumber = Math.round(message.sequenceNumber));
    message.descriptionOfGoods !== undefined && (obj.descriptionOfGoods = message.descriptionOfGoods);
    message.harmonizedSystemSubheadingCode !== undefined
      && (obj.harmonizedSystemSubheadingCode = message.harmonizedSystemSubheadingCode);
    message.localClassificationCode !== undefined && (obj.localClassificationCode = message.localClassificationCode);
    message.grossMass !== undefined && (obj.grossMass = message.grossMass);
    message.netMass !== undefined && (obj.netMass = message.netMass);
    message.numberOfPackages !== undefined && (obj.numberOfPackages = Math.round(message.numberOfPackages));
    message.typeOfPackages !== undefined && (obj.typeOfPackages = message.typeOfPackages);
    message.marksNumbers !== undefined && (obj.marksNumbers = message.marksNumbers);
    message.containerId !== undefined && (obj.containerId = message.containerId);
    message.consigneeOrderNumber !== undefined && (obj.consigneeOrderNumber = message.consigneeOrderNumber);
    message.valueAtBorderExport !== undefined && (obj.valueAtBorderExport = Math.round(message.valueAtBorderExport));
    message.valueAtBorderExportCurrency !== undefined
      && (obj.valueAtBorderExportCurrency = message.valueAtBorderExportCurrency);
    message.amountInvoiced !== undefined && (obj.amountInvoiced = Math.round(message.amountInvoiced));
    message.amountInvoicedCurrency !== undefined && (obj.amountInvoicedCurrency = message.amountInvoicedCurrency);
    message.dangerousGoodsCode !== undefined && (obj.dangerousGoodsCode = Math.round(message.dangerousGoodsCode));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<GoodsItem>, I>>(object: I): GoodsItem {
    const message = createBaseGoodsItem();
    message.documents = object.documents?.map((e) => Document.fromPartial(e)) || [];
    message.countryOfOrigin = object.countryOfOrigin ?? "";
    message.sequenceNumber = object.sequenceNumber ?? 0;
    message.descriptionOfGoods = object.descriptionOfGoods ?? "";
    message.harmonizedSystemSubheadingCode = object.harmonizedSystemSubheadingCode ?? "";
    message.localClassificationCode = object.localClassificationCode ?? "";
    message.grossMass = object.grossMass ?? "";
    message.netMass = object.netMass ?? "";
    message.numberOfPackages = object.numberOfPackages ?? 0;
    message.typeOfPackages = object.typeOfPackages ?? "";
    message.marksNumbers = object.marksNumbers ?? "";
    message.containerId = object.containerId ?? "";
    message.consigneeOrderNumber = object.consigneeOrderNumber ?? "";
    message.valueAtBorderExport = object.valueAtBorderExport ?? 0;
    message.valueAtBorderExportCurrency = object.valueAtBorderExportCurrency ?? "";
    message.amountInvoiced = object.amountInvoiced ?? 0;
    message.amountInvoicedCurrency = object.amountInvoicedCurrency ?? "";
    message.dangerousGoodsCode = object.dangerousGoodsCode ?? 0;
    return message;
  },
};

function createBaseTransport(): Transport {
  return {
    modeOfTransport: 0,
    typeOfIdentification: 0,
    identity: "",
    nationality: "",
    transportCost: 0,
    transportCostCurrency: "",
    transportOrderNumber: "",
  };
}

export const Transport = {
  encode(message: Transport, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.modeOfTransport !== 0) {
      writer.uint32(8).int64(message.modeOfTransport);
    }
    if (message.typeOfIdentification !== 0) {
      writer.uint32(16).int64(message.typeOfIdentification);
    }
    if (message.identity !== "") {
      writer.uint32(26).string(message.identity);
    }
    if (message.nationality !== "") {
      writer.uint32(34).string(message.nationality);
    }
    if (message.transportCost !== 0) {
      writer.uint32(40).int64(message.transportCost);
    }
    if (message.transportCostCurrency !== "") {
      writer.uint32(50).string(message.transportCostCurrency);
    }
    if (message.transportOrderNumber !== "") {
      writer.uint32(58).string(message.transportOrderNumber);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Transport {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseTransport();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.modeOfTransport = longToNumber(reader.int64() as Long);
          break;
        case 2:
          message.typeOfIdentification = longToNumber(reader.int64() as Long);
          break;
        case 3:
          message.identity = reader.string();
          break;
        case 4:
          message.nationality = reader.string();
          break;
        case 5:
          message.transportCost = longToNumber(reader.int64() as Long);
          break;
        case 6:
          message.transportCostCurrency = reader.string();
          break;
        case 7:
          message.transportOrderNumber = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Transport {
    return {
      modeOfTransport: isSet(object.modeOfTransport) ? Number(object.modeOfTransport) : 0,
      typeOfIdentification: isSet(object.typeOfIdentification) ? Number(object.typeOfIdentification) : 0,
      identity: isSet(object.identity) ? String(object.identity) : "",
      nationality: isSet(object.nationality) ? String(object.nationality) : "",
      transportCost: isSet(object.transportCost) ? Number(object.transportCost) : 0,
      transportCostCurrency: isSet(object.transportCostCurrency) ? String(object.transportCostCurrency) : "",
      transportOrderNumber: isSet(object.transportOrderNumber) ? String(object.transportOrderNumber) : "",
    };
  },

  toJSON(message: Transport): unknown {
    const obj: any = {};
    message.modeOfTransport !== undefined && (obj.modeOfTransport = Math.round(message.modeOfTransport));
    message.typeOfIdentification !== undefined && (obj.typeOfIdentification = Math.round(message.typeOfIdentification));
    message.identity !== undefined && (obj.identity = message.identity);
    message.nationality !== undefined && (obj.nationality = message.nationality);
    message.transportCost !== undefined && (obj.transportCost = Math.round(message.transportCost));
    message.transportCostCurrency !== undefined && (obj.transportCostCurrency = message.transportCostCurrency);
    message.transportOrderNumber !== undefined && (obj.transportOrderNumber = message.transportOrderNumber);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<Transport>, I>>(object: I): Transport {
    const message = createBaseTransport();
    message.modeOfTransport = object.modeOfTransport ?? 0;
    message.typeOfIdentification = object.typeOfIdentification ?? 0;
    message.identity = object.identity ?? "";
    message.nationality = object.nationality ?? "";
    message.transportCost = object.transportCost ?? 0;
    message.transportCostCurrency = object.transportCostCurrency ?? "";
    message.transportOrderNumber = object.transportOrderNumber ?? "";
    return message;
  },
};

function createBaseDocument(): Document {
  return { type: "", referenceNumber: "", complement: "", detail: "" };
}

export const Document = {
  encode(message: Document, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.type !== "") {
      writer.uint32(10).string(message.type);
    }
    if (message.referenceNumber !== "") {
      writer.uint32(18).string(message.referenceNumber);
    }
    if (message.complement !== "") {
      writer.uint32(26).string(message.complement);
    }
    if (message.detail !== "") {
      writer.uint32(34).string(message.detail);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Document {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseDocument();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.type = reader.string();
          break;
        case 2:
          message.referenceNumber = reader.string();
          break;
        case 3:
          message.complement = reader.string();
          break;
        case 4:
          message.detail = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Document {
    return {
      type: isSet(object.type) ? String(object.type) : "",
      referenceNumber: isSet(object.referenceNumber) ? String(object.referenceNumber) : "",
      complement: isSet(object.complement) ? String(object.complement) : "",
      detail: isSet(object.detail) ? String(object.detail) : "",
    };
  },

  toJSON(message: Document): unknown {
    const obj: any = {};
    message.type !== undefined && (obj.type = message.type);
    message.referenceNumber !== undefined && (obj.referenceNumber = message.referenceNumber);
    message.complement !== undefined && (obj.complement = message.complement);
    message.detail !== undefined && (obj.detail = message.detail);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<Document>, I>>(object: I): Document {
    const message = createBaseDocument();
    message.type = object.type ?? "";
    message.referenceNumber = object.referenceNumber ?? "";
    message.complement = object.complement ?? "";
    message.detail = object.detail ?? "";
    return message;
  },
};

function createBaseCompanyDriver(): CompanyDriver {
  return { address: undefined, name: "" };
}

export const CompanyDriver = {
  encode(message: CompanyDriver, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.address !== undefined) {
      Address.encode(message.address, writer.uint32(10).fork()).ldelim();
    }
    if (message.name !== "") {
      writer.uint32(26).string(message.name);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): CompanyDriver {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseCompanyDriver();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.address = Address.decode(reader, reader.uint32());
          break;
        case 3:
          message.name = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CompanyDriver {
    return {
      address: isSet(object.address) ? Address.fromJSON(object.address) : undefined,
      name: isSet(object.name) ? String(object.name) : "",
    };
  },

  toJSON(message: CompanyDriver): unknown {
    const obj: any = {};
    message.address !== undefined && (obj.address = message.address ? Address.toJSON(message.address) : undefined);
    message.name !== undefined && (obj.name = message.name);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<CompanyDriver>, I>>(object: I): CompanyDriver {
    const message = createBaseCompanyDriver();
    message.address = (object.address !== undefined && object.address !== null)
      ? Address.fromPartial(object.address)
      : undefined;
    message.name = object.name ?? "";
    return message;
  },
};

function createBaseGoodsItemDriver(): GoodsItemDriver {
  return {
    documents: [],
    countryOfOrigin: "",
    sequenceNumber: 0,
    descriptionOfGoods: "",
    harmonizedSystemSubheadingCode: "",
    localClassificationCode: "",
    grossMass: "",
    netMass: "",
    numberOfPackages: 0,
    typeOfPackages: "",
    marksNumbers: "",
    containerId: "",
    consigneeOrderNumber: "",
    dangerousGoodsCode: 0,
  };
}

export const GoodsItemDriver = {
  encode(message: GoodsItemDriver, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.documents) {
      Document.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.countryOfOrigin !== "") {
      writer.uint32(18).string(message.countryOfOrigin);
    }
    if (message.sequenceNumber !== 0) {
      writer.uint32(24).int64(message.sequenceNumber);
    }
    if (message.descriptionOfGoods !== "") {
      writer.uint32(34).string(message.descriptionOfGoods);
    }
    if (message.harmonizedSystemSubheadingCode !== "") {
      writer.uint32(42).string(message.harmonizedSystemSubheadingCode);
    }
    if (message.localClassificationCode !== "") {
      writer.uint32(50).string(message.localClassificationCode);
    }
    if (message.grossMass !== "") {
      writer.uint32(58).string(message.grossMass);
    }
    if (message.netMass !== "") {
      writer.uint32(66).string(message.netMass);
    }
    if (message.numberOfPackages !== 0) {
      writer.uint32(72).int64(message.numberOfPackages);
    }
    if (message.typeOfPackages !== "") {
      writer.uint32(82).string(message.typeOfPackages);
    }
    if (message.marksNumbers !== "") {
      writer.uint32(90).string(message.marksNumbers);
    }
    if (message.containerId !== "") {
      writer.uint32(98).string(message.containerId);
    }
    if (message.consigneeOrderNumber !== "") {
      writer.uint32(106).string(message.consigneeOrderNumber);
    }
    if (message.dangerousGoodsCode !== 0) {
      writer.uint32(144).int64(message.dangerousGoodsCode);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): GoodsItemDriver {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseGoodsItemDriver();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.documents.push(Document.decode(reader, reader.uint32()));
          break;
        case 2:
          message.countryOfOrigin = reader.string();
          break;
        case 3:
          message.sequenceNumber = longToNumber(reader.int64() as Long);
          break;
        case 4:
          message.descriptionOfGoods = reader.string();
          break;
        case 5:
          message.harmonizedSystemSubheadingCode = reader.string();
          break;
        case 6:
          message.localClassificationCode = reader.string();
          break;
        case 7:
          message.grossMass = reader.string();
          break;
        case 8:
          message.netMass = reader.string();
          break;
        case 9:
          message.numberOfPackages = longToNumber(reader.int64() as Long);
          break;
        case 10:
          message.typeOfPackages = reader.string();
          break;
        case 11:
          message.marksNumbers = reader.string();
          break;
        case 12:
          message.containerId = reader.string();
          break;
        case 13:
          message.consigneeOrderNumber = reader.string();
          break;
        case 18:
          message.dangerousGoodsCode = longToNumber(reader.int64() as Long);
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GoodsItemDriver {
    return {
      documents: Array.isArray(object?.documents) ? object.documents.map((e: any) => Document.fromJSON(e)) : [],
      countryOfOrigin: isSet(object.countryOfOrigin) ? String(object.countryOfOrigin) : "",
      sequenceNumber: isSet(object.sequenceNumber) ? Number(object.sequenceNumber) : 0,
      descriptionOfGoods: isSet(object.descriptionOfGoods) ? String(object.descriptionOfGoods) : "",
      harmonizedSystemSubheadingCode: isSet(object.harmonizedSystemSubheadingCode)
        ? String(object.harmonizedSystemSubheadingCode)
        : "",
      localClassificationCode: isSet(object.localClassificationCode) ? String(object.localClassificationCode) : "",
      grossMass: isSet(object.grossMass) ? String(object.grossMass) : "",
      netMass: isSet(object.netMass) ? String(object.netMass) : "",
      numberOfPackages: isSet(object.numberOfPackages) ? Number(object.numberOfPackages) : 0,
      typeOfPackages: isSet(object.typeOfPackages) ? String(object.typeOfPackages) : "",
      marksNumbers: isSet(object.marksNumbers) ? String(object.marksNumbers) : "",
      containerId: isSet(object.containerId) ? String(object.containerId) : "",
      consigneeOrderNumber: isSet(object.consigneeOrderNumber) ? String(object.consigneeOrderNumber) : "",
      dangerousGoodsCode: isSet(object.dangerousGoodsCode) ? Number(object.dangerousGoodsCode) : 0,
    };
  },

  toJSON(message: GoodsItemDriver): unknown {
    const obj: any = {};
    if (message.documents) {
      obj.documents = message.documents.map((e) => e ? Document.toJSON(e) : undefined);
    } else {
      obj.documents = [];
    }
    message.countryOfOrigin !== undefined && (obj.countryOfOrigin = message.countryOfOrigin);
    message.sequenceNumber !== undefined && (obj.sequenceNumber = Math.round(message.sequenceNumber));
    message.descriptionOfGoods !== undefined && (obj.descriptionOfGoods = message.descriptionOfGoods);
    message.harmonizedSystemSubheadingCode !== undefined
      && (obj.harmonizedSystemSubheadingCode = message.harmonizedSystemSubheadingCode);
    message.localClassificationCode !== undefined && (obj.localClassificationCode = message.localClassificationCode);
    message.grossMass !== undefined && (obj.grossMass = message.grossMass);
    message.netMass !== undefined && (obj.netMass = message.netMass);
    message.numberOfPackages !== undefined && (obj.numberOfPackages = Math.round(message.numberOfPackages));
    message.typeOfPackages !== undefined && (obj.typeOfPackages = message.typeOfPackages);
    message.marksNumbers !== undefined && (obj.marksNumbers = message.marksNumbers);
    message.containerId !== undefined && (obj.containerId = message.containerId);
    message.consigneeOrderNumber !== undefined && (obj.consigneeOrderNumber = message.consigneeOrderNumber);
    message.dangerousGoodsCode !== undefined && (obj.dangerousGoodsCode = Math.round(message.dangerousGoodsCode));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<GoodsItemDriver>, I>>(object: I): GoodsItemDriver {
    const message = createBaseGoodsItemDriver();
    message.documents = object.documents?.map((e) => Document.fromPartial(e)) || [];
    message.countryOfOrigin = object.countryOfOrigin ?? "";
    message.sequenceNumber = object.sequenceNumber ?? 0;
    message.descriptionOfGoods = object.descriptionOfGoods ?? "";
    message.harmonizedSystemSubheadingCode = object.harmonizedSystemSubheadingCode ?? "";
    message.localClassificationCode = object.localClassificationCode ?? "";
    message.grossMass = object.grossMass ?? "";
    message.netMass = object.netMass ?? "";
    message.numberOfPackages = object.numberOfPackages ?? 0;
    message.typeOfPackages = object.typeOfPackages ?? "";
    message.marksNumbers = object.marksNumbers ?? "";
    message.containerId = object.containerId ?? "";
    message.consigneeOrderNumber = object.consigneeOrderNumber ?? "";
    message.dangerousGoodsCode = object.dangerousGoodsCode ?? 0;
    return message;
  },
};

function createBaseTransportDriver(): TransportDriver {
  return { modeOfTransport: 0, typeOfIdentification: 0, identity: "", nationality: "" };
}

export const TransportDriver = {
  encode(message: TransportDriver, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.modeOfTransport !== 0) {
      writer.uint32(8).int64(message.modeOfTransport);
    }
    if (message.typeOfIdentification !== 0) {
      writer.uint32(16).int64(message.typeOfIdentification);
    }
    if (message.identity !== "") {
      writer.uint32(26).string(message.identity);
    }
    if (message.nationality !== "") {
      writer.uint32(34).string(message.nationality);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): TransportDriver {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseTransportDriver();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.modeOfTransport = longToNumber(reader.int64() as Long);
          break;
        case 2:
          message.typeOfIdentification = longToNumber(reader.int64() as Long);
          break;
        case 3:
          message.identity = reader.string();
          break;
        case 4:
          message.nationality = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): TransportDriver {
    return {
      modeOfTransport: isSet(object.modeOfTransport) ? Number(object.modeOfTransport) : 0,
      typeOfIdentification: isSet(object.typeOfIdentification) ? Number(object.typeOfIdentification) : 0,
      identity: isSet(object.identity) ? String(object.identity) : "",
      nationality: isSet(object.nationality) ? String(object.nationality) : "",
    };
  },

  toJSON(message: TransportDriver): unknown {
    const obj: any = {};
    message.modeOfTransport !== undefined && (obj.modeOfTransport = Math.round(message.modeOfTransport));
    message.typeOfIdentification !== undefined && (obj.typeOfIdentification = Math.round(message.typeOfIdentification));
    message.identity !== undefined && (obj.identity = message.identity);
    message.nationality !== undefined && (obj.nationality = message.nationality);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<TransportDriver>, I>>(object: I): TransportDriver {
    const message = createBaseTransportDriver();
    message.modeOfTransport = object.modeOfTransport ?? 0;
    message.typeOfIdentification = object.typeOfIdentification ?? 0;
    message.identity = object.identity ?? "";
    message.nationality = object.nationality ?? "";
    return message;
  },
};

function createBaseMsgFetchAllSegmentHistory(): MsgFetchAllSegmentHistory {
  return { creator: "" };
}

export const MsgFetchAllSegmentHistory = {
  encode(message: MsgFetchAllSegmentHistory, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchAllSegmentHistory {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchAllSegmentHistory();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllSegmentHistory {
    return { creator: isSet(object.creator) ? String(object.creator) : "" };
  },

  toJSON(message: MsgFetchAllSegmentHistory): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchAllSegmentHistory>, I>>(object: I): MsgFetchAllSegmentHistory {
    const message = createBaseMsgFetchAllSegmentHistory();
    message.creator = object.creator ?? "";
    return message;
  },
};

function createBaseMsgFetchGetWallet(): MsgFetchGetWallet {
  return { creator: "", id: "" };
}

export const MsgFetchGetWallet = {
  encode(message: MsgFetchGetWallet, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchGetWallet {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchGetWallet();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchGetWallet {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
    };
  },

  toJSON(message: MsgFetchGetWallet): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchGetWallet>, I>>(object: I): MsgFetchGetWallet {
    const message = createBaseMsgFetchGetWallet();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgFetchGetSegment(): MsgFetchGetSegment {
  return { creator: "", id: "" };
}

export const MsgFetchGetSegment = {
  encode(message: MsgFetchGetSegment, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchGetSegment {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchGetSegment();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchGetSegment {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
    };
  },

  toJSON(message: MsgFetchGetSegment): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchGetSegment>, I>>(object: I): MsgFetchGetSegment {
    const message = createBaseMsgFetchGetSegment();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgFetchAllSegment(): MsgFetchAllSegment {
  return { creator: "" };
}

export const MsgFetchAllSegment = {
  encode(message: MsgFetchAllSegment, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchAllSegment {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchAllSegment();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllSegment {
    return { creator: isSet(object.creator) ? String(object.creator) : "" };
  },

  toJSON(message: MsgFetchAllSegment): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchAllSegment>, I>>(object: I): MsgFetchAllSegment {
    const message = createBaseMsgFetchAllSegment();
    message.creator = object.creator ?? "";
    return message;
  },
};

function createBaseMsgUpdateWallet(): MsgUpdateWallet {
  return { creator: "", id: "", name: "" };
}

export const MsgUpdateWallet = {
  encode(message: MsgUpdateWallet, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.name !== "") {
      writer.uint32(26).string(message.name);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgUpdateWallet {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgUpdateWallet();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.name = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateWallet {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      name: isSet(object.name) ? String(object.name) : "",
    };
  },

  toJSON(message: MsgUpdateWallet): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.name !== undefined && (obj.name = message.name);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgUpdateWallet>, I>>(object: I): MsgUpdateWallet {
    const message = createBaseMsgUpdateWallet();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.name = object.name ?? "";
    return message;
  },
};

function createBaseMsgCreateWallet(): MsgCreateWallet {
  return { creator: "", name: "" };
}

export const MsgCreateWallet = {
  encode(message: MsgCreateWallet, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.name !== "") {
      writer.uint32(18).string(message.name);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateWallet {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateWallet();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.name = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateWallet {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      name: isSet(object.name) ? String(object.name) : "",
    };
  },

  toJSON(message: MsgCreateWallet): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.name !== undefined && (obj.name = message.name);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateWallet>, I>>(object: I): MsgCreateWallet {
    const message = createBaseMsgCreateWallet();
    message.creator = object.creator ?? "";
    message.name = object.name ?? "";
    return message;
  },
};

function createBaseMsgUpdateSegment(): MsgUpdateSegment {
  return { creator: "", id: "", name: "", info: "" };
}

export const MsgUpdateSegment = {
  encode(message: MsgUpdateSegment, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.name !== "") {
      writer.uint32(26).string(message.name);
    }
    if (message.info !== "") {
      writer.uint32(34).string(message.info);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgUpdateSegment {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgUpdateSegment();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.name = reader.string();
          break;
        case 4:
          message.info = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateSegment {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      name: isSet(object.name) ? String(object.name) : "",
      info: isSet(object.info) ? String(object.info) : "",
    };
  },

  toJSON(message: MsgUpdateSegment): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.name !== undefined && (obj.name = message.name);
    message.info !== undefined && (obj.info = message.info);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgUpdateSegment>, I>>(object: I): MsgUpdateSegment {
    const message = createBaseMsgUpdateSegment();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.name = object.name ?? "";
    message.info = object.info ?? "";
    return message;
  },
};

function createBaseMsgFetchGetWalletHistory(): MsgFetchGetWalletHistory {
  return { creator: "", id: "" };
}

export const MsgFetchGetWalletHistory = {
  encode(message: MsgFetchGetWalletHistory, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchGetWalletHistory {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchGetWalletHistory();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchGetWalletHistory {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
    };
  },

  toJSON(message: MsgFetchGetWalletHistory): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchGetWalletHistory>, I>>(object: I): MsgFetchGetWalletHistory {
    const message = createBaseMsgFetchGetWalletHistory();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgCreateSegment(): MsgCreateSegment {
  return { creator: "", name: "", info: "", walletId: "" };
}

export const MsgCreateSegment = {
  encode(message: MsgCreateSegment, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.name !== "") {
      writer.uint32(18).string(message.name);
    }
    if (message.info !== "") {
      writer.uint32(26).string(message.info);
    }
    if (message.walletId !== "") {
      writer.uint32(34).string(message.walletId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateSegment {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateSegment();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.name = reader.string();
          break;
        case 3:
          message.info = reader.string();
          break;
        case 4:
          message.walletId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateSegment {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      name: isSet(object.name) ? String(object.name) : "",
      info: isSet(object.info) ? String(object.info) : "",
      walletId: isSet(object.walletId) ? String(object.walletId) : "",
    };
  },

  toJSON(message: MsgCreateSegment): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.name !== undefined && (obj.name = message.name);
    message.info !== undefined && (obj.info = message.info);
    message.walletId !== undefined && (obj.walletId = message.walletId);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateSegment>, I>>(object: I): MsgCreateSegment {
    const message = createBaseMsgCreateSegment();
    message.creator = object.creator ?? "";
    message.name = object.name ?? "";
    message.info = object.info ?? "";
    message.walletId = object.walletId ?? "";
    return message;
  },
};

function createBaseMsgFetchAllTokenHistoryGlobal(): MsgFetchAllTokenHistoryGlobal {
  return { creator: "" };
}

export const MsgFetchAllTokenHistoryGlobal = {
  encode(message: MsgFetchAllTokenHistoryGlobal, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchAllTokenHistoryGlobal {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchAllTokenHistoryGlobal();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllTokenHistoryGlobal {
    return { creator: isSet(object.creator) ? String(object.creator) : "" };
  },

  toJSON(message: MsgFetchAllTokenHistoryGlobal): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchAllTokenHistoryGlobal>, I>>(
    object: I,
  ): MsgFetchAllTokenHistoryGlobal {
    const message = createBaseMsgFetchAllTokenHistoryGlobal();
    message.creator = object.creator ?? "";
    return message;
  },
};

function createBaseMsgFetchGetTokenHistoryGlobal(): MsgFetchGetTokenHistoryGlobal {
  return { creator: "", id: "" };
}

export const MsgFetchGetTokenHistoryGlobal = {
  encode(message: MsgFetchGetTokenHistoryGlobal, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchGetTokenHistoryGlobal {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchGetTokenHistoryGlobal();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchGetTokenHistoryGlobal {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
    };
  },

  toJSON(message: MsgFetchGetTokenHistoryGlobal): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchGetTokenHistoryGlobal>, I>>(
    object: I,
  ): MsgFetchGetTokenHistoryGlobal {
    const message = createBaseMsgFetchGetTokenHistoryGlobal();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgCreateWalletWithId(): MsgCreateWalletWithId {
  return { creator: "", id: "", name: "" };
}

export const MsgCreateWalletWithId = {
  encode(message: MsgCreateWalletWithId, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.name !== "") {
      writer.uint32(26).string(message.name);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateWalletWithId {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateWalletWithId();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.name = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateWalletWithId {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      name: isSet(object.name) ? String(object.name) : "",
    };
  },

  toJSON(message: MsgCreateWalletWithId): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.name !== undefined && (obj.name = message.name);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateWalletWithId>, I>>(object: I): MsgCreateWalletWithId {
    const message = createBaseMsgCreateWalletWithId();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.name = object.name ?? "";
    return message;
  },
};

function createBaseMsgMoveTokenToSegment(): MsgMoveTokenToSegment {
  return { creator: "", tokenRefId: "", sourceSegmentId: "", targetSegmentId: "" };
}

export const MsgMoveTokenToSegment = {
  encode(message: MsgMoveTokenToSegment, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.tokenRefId !== "") {
      writer.uint32(18).string(message.tokenRefId);
    }
    if (message.sourceSegmentId !== "") {
      writer.uint32(26).string(message.sourceSegmentId);
    }
    if (message.targetSegmentId !== "") {
      writer.uint32(34).string(message.targetSegmentId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgMoveTokenToSegment {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgMoveTokenToSegment();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.tokenRefId = reader.string();
          break;
        case 3:
          message.sourceSegmentId = reader.string();
          break;
        case 4:
          message.targetSegmentId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgMoveTokenToSegment {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      tokenRefId: isSet(object.tokenRefId) ? String(object.tokenRefId) : "",
      sourceSegmentId: isSet(object.sourceSegmentId) ? String(object.sourceSegmentId) : "",
      targetSegmentId: isSet(object.targetSegmentId) ? String(object.targetSegmentId) : "",
    };
  },

  toJSON(message: MsgMoveTokenToSegment): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.tokenRefId !== undefined && (obj.tokenRefId = message.tokenRefId);
    message.sourceSegmentId !== undefined && (obj.sourceSegmentId = message.sourceSegmentId);
    message.targetSegmentId !== undefined && (obj.targetSegmentId = message.targetSegmentId);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgMoveTokenToSegment>, I>>(object: I): MsgMoveTokenToSegment {
    const message = createBaseMsgMoveTokenToSegment();
    message.creator = object.creator ?? "";
    message.tokenRefId = object.tokenRefId ?? "";
    message.sourceSegmentId = object.sourceSegmentId ?? "";
    message.targetSegmentId = object.targetSegmentId ?? "";
    return message;
  },
};

function createBaseMsgFetchAllWalletHistory(): MsgFetchAllWalletHistory {
  return { creator: "" };
}

export const MsgFetchAllWalletHistory = {
  encode(message: MsgFetchAllWalletHistory, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchAllWalletHistory {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchAllWalletHistory();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllWalletHistory {
    return { creator: isSet(object.creator) ? String(object.creator) : "" };
  },

  toJSON(message: MsgFetchAllWalletHistory): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchAllWalletHistory>, I>>(object: I): MsgFetchAllWalletHistory {
    const message = createBaseMsgFetchAllWalletHistory();
    message.creator = object.creator ?? "";
    return message;
  },
};

function createBaseMsgFetchAllWallet(): MsgFetchAllWallet {
  return { creator: "" };
}

export const MsgFetchAllWallet = {
  encode(message: MsgFetchAllWallet, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchAllWallet {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchAllWallet();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllWallet {
    return { creator: isSet(object.creator) ? String(object.creator) : "" };
  },

  toJSON(message: MsgFetchAllWallet): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchAllWallet>, I>>(object: I): MsgFetchAllWallet {
    const message = createBaseMsgFetchAllWallet();
    message.creator = object.creator ?? "";
    return message;
  },
};

function createBaseMsgFetchSegmentHistory(): MsgFetchSegmentHistory {
  return { creator: "", id: "" };
}

export const MsgFetchSegmentHistory = {
  encode(message: MsgFetchSegmentHistory, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchSegmentHistory {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchSegmentHistory();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchSegmentHistory {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
    };
  },

  toJSON(message: MsgFetchSegmentHistory): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchSegmentHistory>, I>>(object: I): MsgFetchSegmentHistory {
    const message = createBaseMsgFetchSegmentHistory();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgCreateSegmentWithId(): MsgCreateSegmentWithId {
  return { creator: "", id: "", name: "", info: "", walletId: "" };
}

export const MsgCreateSegmentWithId = {
  encode(message: MsgCreateSegmentWithId, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.name !== "") {
      writer.uint32(26).string(message.name);
    }
    if (message.info !== "") {
      writer.uint32(34).string(message.info);
    }
    if (message.walletId !== "") {
      writer.uint32(42).string(message.walletId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateSegmentWithId {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateSegmentWithId();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.name = reader.string();
          break;
        case 4:
          message.info = reader.string();
          break;
        case 5:
          message.walletId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateSegmentWithId {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      name: isSet(object.name) ? String(object.name) : "",
      info: isSet(object.info) ? String(object.info) : "",
      walletId: isSet(object.walletId) ? String(object.walletId) : "",
    };
  },

  toJSON(message: MsgCreateSegmentWithId): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.name !== undefined && (obj.name = message.name);
    message.info !== undefined && (obj.info = message.info);
    message.walletId !== undefined && (obj.walletId = message.walletId);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateSegmentWithId>, I>>(object: I): MsgCreateSegmentWithId {
    const message = createBaseMsgCreateSegmentWithId();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.name = object.name ?? "";
    message.info = object.info ?? "";
    message.walletId = object.walletId ?? "";
    return message;
  },
};

function createBaseMsgFetchTokensByWalletId(): MsgFetchTokensByWalletId {
  return { creator: "", id: "" };
}

export const MsgFetchTokensByWalletId = {
  encode(message: MsgFetchTokensByWalletId, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchTokensByWalletId {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchTokensByWalletId();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchTokensByWalletId {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
    };
  },

  toJSON(message: MsgFetchTokensByWalletId): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchTokensByWalletId>, I>>(object: I): MsgFetchTokensByWalletId {
    const message = createBaseMsgFetchTokensByWalletId();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgFetchTokensBySegmentId(): MsgFetchTokensBySegmentId {
  return { creator: "", id: "" };
}

export const MsgFetchTokensBySegmentId = {
  encode(message: MsgFetchTokensBySegmentId, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchTokensBySegmentId {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchTokensBySegmentId();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchTokensBySegmentId {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
    };
  },

  toJSON(message: MsgFetchTokensBySegmentId): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchTokensBySegmentId>, I>>(object: I): MsgFetchTokensBySegmentId {
    const message = createBaseMsgFetchTokensBySegmentId();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgFetchDocumentHash(): MsgFetchDocumentHash {
  return { creator: "", id: "" };
}

export const MsgFetchDocumentHash = {
  encode(message: MsgFetchDocumentHash, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchDocumentHash {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchDocumentHash();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchDocumentHash {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
    };
  },

  toJSON(message: MsgFetchDocumentHash): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchDocumentHash>, I>>(object: I): MsgFetchDocumentHash {
    const message = createBaseMsgFetchDocumentHash();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgCreateTokenCopies(): MsgCreateTokenCopies {
  return { creator: "", index: "", tokens: [] };
}

export const MsgCreateTokenCopies = {
  encode(message: MsgCreateTokenCopies, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.index !== "") {
      writer.uint32(18).string(message.index);
    }
    for (const v of message.tokens) {
      writer.uint32(26).string(v!);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateTokenCopies {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateTokenCopies();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.index = reader.string();
          break;
        case 3:
          message.tokens.push(reader.string());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateTokenCopies {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      index: isSet(object.index) ? String(object.index) : "",
      tokens: Array.isArray(object?.tokens) ? object.tokens.map((e: any) => String(e)) : [],
    };
  },

  toJSON(message: MsgCreateTokenCopies): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.index !== undefined && (obj.index = message.index);
    if (message.tokens) {
      obj.tokens = message.tokens.map((e) => e);
    } else {
      obj.tokens = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateTokenCopies>, I>>(object: I): MsgCreateTokenCopies {
    const message = createBaseMsgCreateTokenCopies();
    message.creator = object.creator ?? "";
    message.index = object.index ?? "";
    message.tokens = object.tokens?.map((e) => e) || [];
    return message;
  },
};

function createBaseMsgCreateTokenCopiesResponse(): MsgCreateTokenCopiesResponse {
  return {};
}

export const MsgCreateTokenCopiesResponse = {
  encode(_: MsgCreateTokenCopiesResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateTokenCopiesResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateTokenCopiesResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgCreateTokenCopiesResponse {
    return {};
  },

  toJSON(_: MsgCreateTokenCopiesResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateTokenCopiesResponse>, I>>(_: I): MsgCreateTokenCopiesResponse {
    const message = createBaseMsgCreateTokenCopiesResponse();
    return message;
  },
};

function createBaseMsgUpdateTokenCopies(): MsgUpdateTokenCopies {
  return { creator: "", index: "", tokens: [] };
}

export const MsgUpdateTokenCopies = {
  encode(message: MsgUpdateTokenCopies, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.index !== "") {
      writer.uint32(18).string(message.index);
    }
    for (const v of message.tokens) {
      writer.uint32(26).string(v!);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgUpdateTokenCopies {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgUpdateTokenCopies();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.index = reader.string();
          break;
        case 3:
          message.tokens.push(reader.string());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateTokenCopies {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      index: isSet(object.index) ? String(object.index) : "",
      tokens: Array.isArray(object?.tokens) ? object.tokens.map((e: any) => String(e)) : [],
    };
  },

  toJSON(message: MsgUpdateTokenCopies): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.index !== undefined && (obj.index = message.index);
    if (message.tokens) {
      obj.tokens = message.tokens.map((e) => e);
    } else {
      obj.tokens = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgUpdateTokenCopies>, I>>(object: I): MsgUpdateTokenCopies {
    const message = createBaseMsgUpdateTokenCopies();
    message.creator = object.creator ?? "";
    message.index = object.index ?? "";
    message.tokens = object.tokens?.map((e) => e) || [];
    return message;
  },
};

function createBaseMsgUpdateTokenCopiesResponse(): MsgUpdateTokenCopiesResponse {
  return {};
}

export const MsgUpdateTokenCopiesResponse = {
  encode(_: MsgUpdateTokenCopiesResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgUpdateTokenCopiesResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgUpdateTokenCopiesResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgUpdateTokenCopiesResponse {
    return {};
  },

  toJSON(_: MsgUpdateTokenCopiesResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgUpdateTokenCopiesResponse>, I>>(_: I): MsgUpdateTokenCopiesResponse {
    const message = createBaseMsgUpdateTokenCopiesResponse();
    return message;
  },
};

function createBaseMsgDeleteTokenCopies(): MsgDeleteTokenCopies {
  return { creator: "", index: "" };
}

export const MsgDeleteTokenCopies = {
  encode(message: MsgDeleteTokenCopies, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.index !== "") {
      writer.uint32(18).string(message.index);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgDeleteTokenCopies {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgDeleteTokenCopies();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.index = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgDeleteTokenCopies {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      index: isSet(object.index) ? String(object.index) : "",
    };
  },

  toJSON(message: MsgDeleteTokenCopies): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.index !== undefined && (obj.index = message.index);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgDeleteTokenCopies>, I>>(object: I): MsgDeleteTokenCopies {
    const message = createBaseMsgDeleteTokenCopies();
    message.creator = object.creator ?? "";
    message.index = object.index ?? "";
    return message;
  },
};

function createBaseMsgDeleteTokenCopiesResponse(): MsgDeleteTokenCopiesResponse {
  return {};
}

export const MsgDeleteTokenCopiesResponse = {
  encode(_: MsgDeleteTokenCopiesResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgDeleteTokenCopiesResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgDeleteTokenCopiesResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgDeleteTokenCopiesResponse {
    return {};
  },

  toJSON(_: MsgDeleteTokenCopiesResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgDeleteTokenCopiesResponse>, I>>(_: I): MsgDeleteTokenCopiesResponse {
    const message = createBaseMsgDeleteTokenCopiesResponse();
    return message;
  },
};

function createBaseMsgCreateDocumentTokenMapper(): MsgCreateDocumentTokenMapper {
  return { creator: "", documentId: "", tokenId: "" };
}

export const MsgCreateDocumentTokenMapper = {
  encode(message: MsgCreateDocumentTokenMapper, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.documentId !== "") {
      writer.uint32(18).string(message.documentId);
    }
    if (message.tokenId !== "") {
      writer.uint32(26).string(message.tokenId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateDocumentTokenMapper {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateDocumentTokenMapper();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.documentId = reader.string();
          break;
        case 3:
          message.tokenId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateDocumentTokenMapper {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      documentId: isSet(object.documentId) ? String(object.documentId) : "",
      tokenId: isSet(object.tokenId) ? String(object.tokenId) : "",
    };
  },

  toJSON(message: MsgCreateDocumentTokenMapper): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.documentId !== undefined && (obj.documentId = message.documentId);
    message.tokenId !== undefined && (obj.tokenId = message.tokenId);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateDocumentTokenMapper>, I>>(object: I): MsgCreateDocumentTokenMapper {
    const message = createBaseMsgCreateDocumentTokenMapper();
    message.creator = object.creator ?? "";
    message.documentId = object.documentId ?? "";
    message.tokenId = object.tokenId ?? "";
    return message;
  },
};

function createBaseMsgCreateDocumentTokenMapperResponse(): MsgCreateDocumentTokenMapperResponse {
  return {};
}

export const MsgCreateDocumentTokenMapperResponse = {
  encode(_: MsgCreateDocumentTokenMapperResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateDocumentTokenMapperResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateDocumentTokenMapperResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgCreateDocumentTokenMapperResponse {
    return {};
  },

  toJSON(_: MsgCreateDocumentTokenMapperResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateDocumentTokenMapperResponse>, I>>(
    _: I,
  ): MsgCreateDocumentTokenMapperResponse {
    const message = createBaseMsgCreateDocumentTokenMapperResponse();
    return message;
  },
};

function createBaseMsgUpdateDocumentTokenMapper(): MsgUpdateDocumentTokenMapper {
  return { creator: "", documentId: "", tokenId: "" };
}

export const MsgUpdateDocumentTokenMapper = {
  encode(message: MsgUpdateDocumentTokenMapper, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.documentId !== "") {
      writer.uint32(18).string(message.documentId);
    }
    if (message.tokenId !== "") {
      writer.uint32(26).string(message.tokenId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgUpdateDocumentTokenMapper {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgUpdateDocumentTokenMapper();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.documentId = reader.string();
          break;
        case 3:
          message.tokenId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateDocumentTokenMapper {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      documentId: isSet(object.documentId) ? String(object.documentId) : "",
      tokenId: isSet(object.tokenId) ? String(object.tokenId) : "",
    };
  },

  toJSON(message: MsgUpdateDocumentTokenMapper): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.documentId !== undefined && (obj.documentId = message.documentId);
    message.tokenId !== undefined && (obj.tokenId = message.tokenId);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgUpdateDocumentTokenMapper>, I>>(object: I): MsgUpdateDocumentTokenMapper {
    const message = createBaseMsgUpdateDocumentTokenMapper();
    message.creator = object.creator ?? "";
    message.documentId = object.documentId ?? "";
    message.tokenId = object.tokenId ?? "";
    return message;
  },
};

function createBaseMsgUpdateDocumentTokenMapperResponse(): MsgUpdateDocumentTokenMapperResponse {
  return {};
}

export const MsgUpdateDocumentTokenMapperResponse = {
  encode(_: MsgUpdateDocumentTokenMapperResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgUpdateDocumentTokenMapperResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgUpdateDocumentTokenMapperResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgUpdateDocumentTokenMapperResponse {
    return {};
  },

  toJSON(_: MsgUpdateDocumentTokenMapperResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgUpdateDocumentTokenMapperResponse>, I>>(
    _: I,
  ): MsgUpdateDocumentTokenMapperResponse {
    const message = createBaseMsgUpdateDocumentTokenMapperResponse();
    return message;
  },
};

function createBaseMsgDeleteDocumentTokenMapper(): MsgDeleteDocumentTokenMapper {
  return { creator: "", index: "" };
}

export const MsgDeleteDocumentTokenMapper = {
  encode(message: MsgDeleteDocumentTokenMapper, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.index !== "") {
      writer.uint32(18).string(message.index);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgDeleteDocumentTokenMapper {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgDeleteDocumentTokenMapper();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.index = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgDeleteDocumentTokenMapper {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      index: isSet(object.index) ? String(object.index) : "",
    };
  },

  toJSON(message: MsgDeleteDocumentTokenMapper): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.index !== undefined && (obj.index = message.index);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgDeleteDocumentTokenMapper>, I>>(object: I): MsgDeleteDocumentTokenMapper {
    const message = createBaseMsgDeleteDocumentTokenMapper();
    message.creator = object.creator ?? "";
    message.index = object.index ?? "";
    return message;
  },
};

function createBaseMsgDeleteDocumentTokenMapperResponse(): MsgDeleteDocumentTokenMapperResponse {
  return {};
}

export const MsgDeleteDocumentTokenMapperResponse = {
  encode(_: MsgDeleteDocumentTokenMapperResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgDeleteDocumentTokenMapperResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgDeleteDocumentTokenMapperResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgDeleteDocumentTokenMapperResponse {
    return {};
  },

  toJSON(_: MsgDeleteDocumentTokenMapperResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgDeleteDocumentTokenMapperResponse>, I>>(
    _: I,
  ): MsgDeleteDocumentTokenMapperResponse {
    const message = createBaseMsgDeleteDocumentTokenMapperResponse();
    return message;
  },
};

function createBaseMsgMoveTokenToWallet(): MsgMoveTokenToWallet {
  return { creator: "", tokenRefId: "", sourceSegmentId: "", targetSegmentId: "" };
}

export const MsgMoveTokenToWallet = {
  encode(message: MsgMoveTokenToWallet, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.tokenRefId !== "") {
      writer.uint32(18).string(message.tokenRefId);
    }
    if (message.sourceSegmentId !== "") {
      writer.uint32(26).string(message.sourceSegmentId);
    }
    if (message.targetSegmentId !== "") {
      writer.uint32(34).string(message.targetSegmentId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgMoveTokenToWallet {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgMoveTokenToWallet();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.tokenRefId = reader.string();
          break;
        case 3:
          message.sourceSegmentId = reader.string();
          break;
        case 4:
          message.targetSegmentId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgMoveTokenToWallet {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      tokenRefId: isSet(object.tokenRefId) ? String(object.tokenRefId) : "",
      sourceSegmentId: isSet(object.sourceSegmentId) ? String(object.sourceSegmentId) : "",
      targetSegmentId: isSet(object.targetSegmentId) ? String(object.targetSegmentId) : "",
    };
  },

  toJSON(message: MsgMoveTokenToWallet): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.tokenRefId !== undefined && (obj.tokenRefId = message.tokenRefId);
    message.sourceSegmentId !== undefined && (obj.sourceSegmentId = message.sourceSegmentId);
    message.targetSegmentId !== undefined && (obj.targetSegmentId = message.targetSegmentId);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgMoveTokenToWallet>, I>>(object: I): MsgMoveTokenToWallet {
    const message = createBaseMsgMoveTokenToWallet();
    message.creator = object.creator ?? "";
    message.tokenRefId = object.tokenRefId ?? "";
    message.sourceSegmentId = object.sourceSegmentId ?? "";
    message.targetSegmentId = object.targetSegmentId ?? "";
    return message;
  },
};

function createBaseMsgCreateToken(): MsgCreateToken {
  return { creator: "", tokenType: "", changeMessage: "", segmentId: "", moduleRef: "" };
}

export const MsgCreateToken = {
  encode(message: MsgCreateToken, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.tokenType !== "") {
      writer.uint32(18).string(message.tokenType);
    }
    if (message.changeMessage !== "") {
      writer.uint32(26).string(message.changeMessage);
    }
    if (message.segmentId !== "") {
      writer.uint32(34).string(message.segmentId);
    }
    if (message.moduleRef !== "") {
      writer.uint32(42).string(message.moduleRef);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateToken {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateToken();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.tokenType = reader.string();
          break;
        case 3:
          message.changeMessage = reader.string();
          break;
        case 4:
          message.segmentId = reader.string();
          break;
        case 5:
          message.moduleRef = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateToken {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      tokenType: isSet(object.tokenType) ? String(object.tokenType) : "",
      changeMessage: isSet(object.changeMessage) ? String(object.changeMessage) : "",
      segmentId: isSet(object.segmentId) ? String(object.segmentId) : "",
      moduleRef: isSet(object.moduleRef) ? String(object.moduleRef) : "",
    };
  },

  toJSON(message: MsgCreateToken): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.tokenType !== undefined && (obj.tokenType = message.tokenType);
    message.changeMessage !== undefined && (obj.changeMessage = message.changeMessage);
    message.segmentId !== undefined && (obj.segmentId = message.segmentId);
    message.moduleRef !== undefined && (obj.moduleRef = message.moduleRef);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateToken>, I>>(object: I): MsgCreateToken {
    const message = createBaseMsgCreateToken();
    message.creator = object.creator ?? "";
    message.tokenType = object.tokenType ?? "";
    message.changeMessage = object.changeMessage ?? "";
    message.segmentId = object.segmentId ?? "";
    message.moduleRef = object.moduleRef ?? "";
    return message;
  },
};

function createBaseMsgUpdateToken(): MsgUpdateToken {
  return { creator: "", tokenRefId: "", tokenType: "", changeMessage: "", segmentId: "", moduleRef: "" };
}

export const MsgUpdateToken = {
  encode(message: MsgUpdateToken, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.tokenRefId !== "") {
      writer.uint32(18).string(message.tokenRefId);
    }
    if (message.tokenType !== "") {
      writer.uint32(34).string(message.tokenType);
    }
    if (message.changeMessage !== "") {
      writer.uint32(42).string(message.changeMessage);
    }
    if (message.segmentId !== "") {
      writer.uint32(50).string(message.segmentId);
    }
    if (message.moduleRef !== "") {
      writer.uint32(58).string(message.moduleRef);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgUpdateToken {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgUpdateToken();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.tokenRefId = reader.string();
          break;
        case 4:
          message.tokenType = reader.string();
          break;
        case 5:
          message.changeMessage = reader.string();
          break;
        case 6:
          message.segmentId = reader.string();
          break;
        case 7:
          message.moduleRef = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateToken {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      tokenRefId: isSet(object.tokenRefId) ? String(object.tokenRefId) : "",
      tokenType: isSet(object.tokenType) ? String(object.tokenType) : "",
      changeMessage: isSet(object.changeMessage) ? String(object.changeMessage) : "",
      segmentId: isSet(object.segmentId) ? String(object.segmentId) : "",
      moduleRef: isSet(object.moduleRef) ? String(object.moduleRef) : "",
    };
  },

  toJSON(message: MsgUpdateToken): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.tokenRefId !== undefined && (obj.tokenRefId = message.tokenRefId);
    message.tokenType !== undefined && (obj.tokenType = message.tokenType);
    message.changeMessage !== undefined && (obj.changeMessage = message.changeMessage);
    message.segmentId !== undefined && (obj.segmentId = message.segmentId);
    message.moduleRef !== undefined && (obj.moduleRef = message.moduleRef);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgUpdateToken>, I>>(object: I): MsgUpdateToken {
    const message = createBaseMsgUpdateToken();
    message.creator = object.creator ?? "";
    message.tokenRefId = object.tokenRefId ?? "";
    message.tokenType = object.tokenType ?? "";
    message.changeMessage = object.changeMessage ?? "";
    message.segmentId = object.segmentId ?? "";
    message.moduleRef = object.moduleRef ?? "";
    return message;
  },
};

function createBaseMsgIdResponse(): MsgIdResponse {
  return { id: "" };
}

export const MsgIdResponse = {
  encode(message: MsgIdResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.id !== "") {
      writer.uint32(10).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgIdResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgIdResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgIdResponse {
    return { id: isSet(object.id) ? String(object.id) : "" };
  },

  toJSON(message: MsgIdResponse): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgIdResponse>, I>>(object: I): MsgIdResponse {
    const message = createBaseMsgIdResponse();
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgActivateToken(): MsgActivateToken {
  return { creator: "", id: "", segmentId: "", moduleRef: "" };
}

export const MsgActivateToken = {
  encode(message: MsgActivateToken, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.segmentId !== "") {
      writer.uint32(26).string(message.segmentId);
    }
    if (message.moduleRef !== "") {
      writer.uint32(34).string(message.moduleRef);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgActivateToken {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgActivateToken();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.segmentId = reader.string();
          break;
        case 4:
          message.moduleRef = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgActivateToken {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      segmentId: isSet(object.segmentId) ? String(object.segmentId) : "",
      moduleRef: isSet(object.moduleRef) ? String(object.moduleRef) : "",
    };
  },

  toJSON(message: MsgActivateToken): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.segmentId !== undefined && (obj.segmentId = message.segmentId);
    message.moduleRef !== undefined && (obj.moduleRef = message.moduleRef);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgActivateToken>, I>>(object: I): MsgActivateToken {
    const message = createBaseMsgActivateToken();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.segmentId = object.segmentId ?? "";
    message.moduleRef = object.moduleRef ?? "";
    return message;
  },
};

function createBaseMsgDeactivateToken(): MsgDeactivateToken {
  return { creator: "", id: "" };
}

export const MsgDeactivateToken = {
  encode(message: MsgDeactivateToken, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgDeactivateToken {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgDeactivateToken();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgDeactivateToken {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
    };
  },

  toJSON(message: MsgDeactivateToken): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgDeactivateToken>, I>>(object: I): MsgDeactivateToken {
    const message = createBaseMsgDeactivateToken();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgCreateHashToken(): MsgCreateHashToken {
  return { creator: "", changeMessage: "", segmentId: "", document: "", hash: "", hashFunction: "", metadata: "" };
}

export const MsgCreateHashToken = {
  encode(message: MsgCreateHashToken, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.changeMessage !== "") {
      writer.uint32(18).string(message.changeMessage);
    }
    if (message.segmentId !== "") {
      writer.uint32(26).string(message.segmentId);
    }
    if (message.document !== "") {
      writer.uint32(34).string(message.document);
    }
    if (message.hash !== "") {
      writer.uint32(42).string(message.hash);
    }
    if (message.hashFunction !== "") {
      writer.uint32(50).string(message.hashFunction);
    }
    if (message.metadata !== "") {
      writer.uint32(58).string(message.metadata);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateHashToken {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateHashToken();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.changeMessage = reader.string();
          break;
        case 3:
          message.segmentId = reader.string();
          break;
        case 4:
          message.document = reader.string();
          break;
        case 5:
          message.hash = reader.string();
          break;
        case 6:
          message.hashFunction = reader.string();
          break;
        case 7:
          message.metadata = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateHashToken {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      changeMessage: isSet(object.changeMessage) ? String(object.changeMessage) : "",
      segmentId: isSet(object.segmentId) ? String(object.segmentId) : "",
      document: isSet(object.document) ? String(object.document) : "",
      hash: isSet(object.hash) ? String(object.hash) : "",
      hashFunction: isSet(object.hashFunction) ? String(object.hashFunction) : "",
      metadata: isSet(object.metadata) ? String(object.metadata) : "",
    };
  },

  toJSON(message: MsgCreateHashToken): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.changeMessage !== undefined && (obj.changeMessage = message.changeMessage);
    message.segmentId !== undefined && (obj.segmentId = message.segmentId);
    message.document !== undefined && (obj.document = message.document);
    message.hash !== undefined && (obj.hash = message.hash);
    message.hashFunction !== undefined && (obj.hashFunction = message.hashFunction);
    message.metadata !== undefined && (obj.metadata = message.metadata);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateHashToken>, I>>(object: I): MsgCreateHashToken {
    const message = createBaseMsgCreateHashToken();
    message.creator = object.creator ?? "";
    message.changeMessage = object.changeMessage ?? "";
    message.segmentId = object.segmentId ?? "";
    message.document = object.document ?? "";
    message.hash = object.hash ?? "";
    message.hashFunction = object.hashFunction ?? "";
    message.metadata = object.metadata ?? "";
    return message;
  },
};

function createBaseMsgCloneToken(): MsgCloneToken {
  return { creator: "", tokenId: "", walletId: "", moduleRef: "" };
}

export const MsgCloneToken = {
  encode(message: MsgCloneToken, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.tokenId !== "") {
      writer.uint32(18).string(message.tokenId);
    }
    if (message.walletId !== "") {
      writer.uint32(26).string(message.walletId);
    }
    if (message.moduleRef !== "") {
      writer.uint32(34).string(message.moduleRef);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCloneToken {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCloneToken();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.tokenId = reader.string();
          break;
        case 3:
          message.walletId = reader.string();
          break;
        case 4:
          message.moduleRef = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCloneToken {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      tokenId: isSet(object.tokenId) ? String(object.tokenId) : "",
      walletId: isSet(object.walletId) ? String(object.walletId) : "",
      moduleRef: isSet(object.moduleRef) ? String(object.moduleRef) : "",
    };
  },

  toJSON(message: MsgCloneToken): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.tokenId !== undefined && (obj.tokenId = message.tokenId);
    message.walletId !== undefined && (obj.walletId = message.walletId);
    message.moduleRef !== undefined && (obj.moduleRef = message.moduleRef);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCloneToken>, I>>(object: I): MsgCloneToken {
    const message = createBaseMsgCloneToken();
    message.creator = object.creator ?? "";
    message.tokenId = object.tokenId ?? "";
    message.walletId = object.walletId ?? "";
    message.moduleRef = object.moduleRef ?? "";
    return message;
  },
};

function createBaseMsgRevertToGenesis(): MsgRevertToGenesis {
  return { creator: "" };
}

export const MsgRevertToGenesis = {
  encode(message: MsgRevertToGenesis, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgRevertToGenesis {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgRevertToGenesis();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgRevertToGenesis {
    return { creator: isSet(object.creator) ? String(object.creator) : "" };
  },

  toJSON(message: MsgRevertToGenesis): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgRevertToGenesis>, I>>(object: I): MsgRevertToGenesis {
    const message = createBaseMsgRevertToGenesis();
    message.creator = object.creator ?? "";
    return message;
  },
};

function createBaseMsgEmptyResponse(): MsgEmptyResponse {
  return {};
}

export const MsgEmptyResponse = {
  encode(_: MsgEmptyResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgEmptyResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgEmptyResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgEmptyResponse {
    return {};
  },

  toJSON(_: MsgEmptyResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgEmptyResponse>, I>>(_: I): MsgEmptyResponse {
    const message = createBaseMsgEmptyResponse();
    return message;
  },
};

function createBaseMsgCreateExportToken(): MsgCreateExportToken {
  return {
    creator: "",
    id: "",
    exporter: undefined,
    declarant: undefined,
    representative: undefined,
    consignee: undefined,
    customsOfficeOfExport: undefined,
    customOfficeOfExit: undefined,
    goodsItems: [],
    transportToBorder: undefined,
    transportAtBorder: undefined,
    exportId: "",
    uniqueConsignmentReference: "",
    localReferenceNumber: "",
    destinationCountry: "",
    exportCountry: "",
    itinerary: "",
    releaseDateAndTime: "",
    incotermCode: "",
    incotermLocation: "",
    totalGrossMass: "",
    goodsItemQuantity: 0,
    totalPackagesQuantity: 0,
    natureOfTransaction: 0,
    totalAmountInvoiced: 0,
    invoiceCurrency: "",
    user: "",
    timestamp: "",
  };
}

export const MsgCreateExportToken = {
  encode(message: MsgCreateExportToken, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.exporter !== undefined) {
      Company1.encode(message.exporter, writer.uint32(26).fork()).ldelim();
    }
    if (message.declarant !== undefined) {
      Company1.encode(message.declarant, writer.uint32(34).fork()).ldelim();
    }
    if (message.representative !== undefined) {
      Company1.encode(message.representative, writer.uint32(42).fork()).ldelim();
    }
    if (message.consignee !== undefined) {
      Company1.encode(message.consignee, writer.uint32(50).fork()).ldelim();
    }
    if (message.customsOfficeOfExport !== undefined) {
      CustomsOffice2.encode(message.customsOfficeOfExport, writer.uint32(58).fork()).ldelim();
    }
    if (message.customOfficeOfExit !== undefined) {
      CustomsOffice2.encode(message.customOfficeOfExit, writer.uint32(66).fork()).ldelim();
    }
    for (const v of message.goodsItems) {
      GoodsItem3.encode(v!, writer.uint32(74).fork()).ldelim();
    }
    if (message.transportToBorder !== undefined) {
      Transport4.encode(message.transportToBorder, writer.uint32(82).fork()).ldelim();
    }
    if (message.transportAtBorder !== undefined) {
      Transport4.encode(message.transportAtBorder, writer.uint32(90).fork()).ldelim();
    }
    if (message.exportId !== "") {
      writer.uint32(98).string(message.exportId);
    }
    if (message.uniqueConsignmentReference !== "") {
      writer.uint32(106).string(message.uniqueConsignmentReference);
    }
    if (message.localReferenceNumber !== "") {
      writer.uint32(114).string(message.localReferenceNumber);
    }
    if (message.destinationCountry !== "") {
      writer.uint32(122).string(message.destinationCountry);
    }
    if (message.exportCountry !== "") {
      writer.uint32(130).string(message.exportCountry);
    }
    if (message.itinerary !== "") {
      writer.uint32(138).string(message.itinerary);
    }
    if (message.releaseDateAndTime !== "") {
      writer.uint32(146).string(message.releaseDateAndTime);
    }
    if (message.incotermCode !== "") {
      writer.uint32(154).string(message.incotermCode);
    }
    if (message.incotermLocation !== "") {
      writer.uint32(162).string(message.incotermLocation);
    }
    if (message.totalGrossMass !== "") {
      writer.uint32(170).string(message.totalGrossMass);
    }
    if (message.goodsItemQuantity !== 0) {
      writer.uint32(176).int64(message.goodsItemQuantity);
    }
    if (message.totalPackagesQuantity !== 0) {
      writer.uint32(184).int64(message.totalPackagesQuantity);
    }
    if (message.natureOfTransaction !== 0) {
      writer.uint32(192).int64(message.natureOfTransaction);
    }
    if (message.totalAmountInvoiced !== 0) {
      writer.uint32(200).int64(message.totalAmountInvoiced);
    }
    if (message.invoiceCurrency !== "") {
      writer.uint32(210).string(message.invoiceCurrency);
    }
    if (message.user !== "") {
      writer.uint32(226).string(message.user);
    }
    if (message.timestamp !== "") {
      writer.uint32(234).string(message.timestamp);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateExportToken {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateExportToken();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.exporter = Company1.decode(reader, reader.uint32());
          break;
        case 4:
          message.declarant = Company1.decode(reader, reader.uint32());
          break;
        case 5:
          message.representative = Company1.decode(reader, reader.uint32());
          break;
        case 6:
          message.consignee = Company1.decode(reader, reader.uint32());
          break;
        case 7:
          message.customsOfficeOfExport = CustomsOffice2.decode(reader, reader.uint32());
          break;
        case 8:
          message.customOfficeOfExit = CustomsOffice2.decode(reader, reader.uint32());
          break;
        case 9:
          message.goodsItems.push(GoodsItem3.decode(reader, reader.uint32()));
          break;
        case 10:
          message.transportToBorder = Transport4.decode(reader, reader.uint32());
          break;
        case 11:
          message.transportAtBorder = Transport4.decode(reader, reader.uint32());
          break;
        case 12:
          message.exportId = reader.string();
          break;
        case 13:
          message.uniqueConsignmentReference = reader.string();
          break;
        case 14:
          message.localReferenceNumber = reader.string();
          break;
        case 15:
          message.destinationCountry = reader.string();
          break;
        case 16:
          message.exportCountry = reader.string();
          break;
        case 17:
          message.itinerary = reader.string();
          break;
        case 18:
          message.releaseDateAndTime = reader.string();
          break;
        case 19:
          message.incotermCode = reader.string();
          break;
        case 20:
          message.incotermLocation = reader.string();
          break;
        case 21:
          message.totalGrossMass = reader.string();
          break;
        case 22:
          message.goodsItemQuantity = longToNumber(reader.int64() as Long);
          break;
        case 23:
          message.totalPackagesQuantity = longToNumber(reader.int64() as Long);
          break;
        case 24:
          message.natureOfTransaction = longToNumber(reader.int64() as Long);
          break;
        case 25:
          message.totalAmountInvoiced = longToNumber(reader.int64() as Long);
          break;
        case 26:
          message.invoiceCurrency = reader.string();
          break;
        case 28:
          message.user = reader.string();
          break;
        case 29:
          message.timestamp = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateExportToken {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      exporter: isSet(object.exporter) ? Company1.fromJSON(object.exporter) : undefined,
      declarant: isSet(object.declarant) ? Company1.fromJSON(object.declarant) : undefined,
      representative: isSet(object.representative) ? Company1.fromJSON(object.representative) : undefined,
      consignee: isSet(object.consignee) ? Company1.fromJSON(object.consignee) : undefined,
      customsOfficeOfExport: isSet(object.customsOfficeOfExport)
        ? CustomsOffice2.fromJSON(object.customsOfficeOfExport)
        : undefined,
      customOfficeOfExit: isSet(object.customOfficeOfExit)
        ? CustomsOffice2.fromJSON(object.customOfficeOfExit)
        : undefined,
      goodsItems: Array.isArray(object?.goodsItems) ? object.goodsItems.map((e: any) => GoodsItem3.fromJSON(e)) : [],
      transportToBorder: isSet(object.transportToBorder) ? Transport4.fromJSON(object.transportToBorder) : undefined,
      transportAtBorder: isSet(object.transportAtBorder) ? Transport4.fromJSON(object.transportAtBorder) : undefined,
      exportId: isSet(object.exportId) ? String(object.exportId) : "",
      uniqueConsignmentReference: isSet(object.uniqueConsignmentReference)
        ? String(object.uniqueConsignmentReference)
        : "",
      localReferenceNumber: isSet(object.localReferenceNumber) ? String(object.localReferenceNumber) : "",
      destinationCountry: isSet(object.destinationCountry) ? String(object.destinationCountry) : "",
      exportCountry: isSet(object.exportCountry) ? String(object.exportCountry) : "",
      itinerary: isSet(object.itinerary) ? String(object.itinerary) : "",
      releaseDateAndTime: isSet(object.releaseDateAndTime) ? String(object.releaseDateAndTime) : "",
      incotermCode: isSet(object.incotermCode) ? String(object.incotermCode) : "",
      incotermLocation: isSet(object.incotermLocation) ? String(object.incotermLocation) : "",
      totalGrossMass: isSet(object.totalGrossMass) ? String(object.totalGrossMass) : "",
      goodsItemQuantity: isSet(object.goodsItemQuantity) ? Number(object.goodsItemQuantity) : 0,
      totalPackagesQuantity: isSet(object.totalPackagesQuantity) ? Number(object.totalPackagesQuantity) : 0,
      natureOfTransaction: isSet(object.natureOfTransaction) ? Number(object.natureOfTransaction) : 0,
      totalAmountInvoiced: isSet(object.totalAmountInvoiced) ? Number(object.totalAmountInvoiced) : 0,
      invoiceCurrency: isSet(object.invoiceCurrency) ? String(object.invoiceCurrency) : "",
      user: isSet(object.user) ? String(object.user) : "",
      timestamp: isSet(object.timestamp) ? String(object.timestamp) : "",
    };
  },

  toJSON(message: MsgCreateExportToken): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.exporter !== undefined && (obj.exporter = message.exporter ? Company1.toJSON(message.exporter) : undefined);
    message.declarant !== undefined
      && (obj.declarant = message.declarant ? Company1.toJSON(message.declarant) : undefined);
    message.representative !== undefined
      && (obj.representative = message.representative ? Company1.toJSON(message.representative) : undefined);
    message.consignee !== undefined
      && (obj.consignee = message.consignee ? Company1.toJSON(message.consignee) : undefined);
    message.customsOfficeOfExport !== undefined && (obj.customsOfficeOfExport = message.customsOfficeOfExport
      ? CustomsOffice2.toJSON(message.customsOfficeOfExport)
      : undefined);
    message.customOfficeOfExit !== undefined && (obj.customOfficeOfExit = message.customOfficeOfExit
      ? CustomsOffice2.toJSON(message.customOfficeOfExit)
      : undefined);
    if (message.goodsItems) {
      obj.goodsItems = message.goodsItems.map((e) => e ? GoodsItem3.toJSON(e) : undefined);
    } else {
      obj.goodsItems = [];
    }
    message.transportToBorder !== undefined
      && (obj.transportToBorder = message.transportToBorder ? Transport4.toJSON(message.transportToBorder) : undefined);
    message.transportAtBorder !== undefined
      && (obj.transportAtBorder = message.transportAtBorder ? Transport4.toJSON(message.transportAtBorder) : undefined);
    message.exportId !== undefined && (obj.exportId = message.exportId);
    message.uniqueConsignmentReference !== undefined
      && (obj.uniqueConsignmentReference = message.uniqueConsignmentReference);
    message.localReferenceNumber !== undefined && (obj.localReferenceNumber = message.localReferenceNumber);
    message.destinationCountry !== undefined && (obj.destinationCountry = message.destinationCountry);
    message.exportCountry !== undefined && (obj.exportCountry = message.exportCountry);
    message.itinerary !== undefined && (obj.itinerary = message.itinerary);
    message.releaseDateAndTime !== undefined && (obj.releaseDateAndTime = message.releaseDateAndTime);
    message.incotermCode !== undefined && (obj.incotermCode = message.incotermCode);
    message.incotermLocation !== undefined && (obj.incotermLocation = message.incotermLocation);
    message.totalGrossMass !== undefined && (obj.totalGrossMass = message.totalGrossMass);
    message.goodsItemQuantity !== undefined && (obj.goodsItemQuantity = Math.round(message.goodsItemQuantity));
    message.totalPackagesQuantity !== undefined
      && (obj.totalPackagesQuantity = Math.round(message.totalPackagesQuantity));
    message.natureOfTransaction !== undefined && (obj.natureOfTransaction = Math.round(message.natureOfTransaction));
    message.totalAmountInvoiced !== undefined && (obj.totalAmountInvoiced = Math.round(message.totalAmountInvoiced));
    message.invoiceCurrency !== undefined && (obj.invoiceCurrency = message.invoiceCurrency);
    message.user !== undefined && (obj.user = message.user);
    message.timestamp !== undefined && (obj.timestamp = message.timestamp);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateExportToken>, I>>(object: I): MsgCreateExportToken {
    const message = createBaseMsgCreateExportToken();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.exporter = (object.exporter !== undefined && object.exporter !== null)
      ? Company1.fromPartial(object.exporter)
      : undefined;
    message.declarant = (object.declarant !== undefined && object.declarant !== null)
      ? Company1.fromPartial(object.declarant)
      : undefined;
    message.representative = (object.representative !== undefined && object.representative !== null)
      ? Company1.fromPartial(object.representative)
      : undefined;
    message.consignee = (object.consignee !== undefined && object.consignee !== null)
      ? Company1.fromPartial(object.consignee)
      : undefined;
    message.customsOfficeOfExport =
      (object.customsOfficeOfExport !== undefined && object.customsOfficeOfExport !== null)
        ? CustomsOffice2.fromPartial(object.customsOfficeOfExport)
        : undefined;
    message.customOfficeOfExit = (object.customOfficeOfExit !== undefined && object.customOfficeOfExit !== null)
      ? CustomsOffice2.fromPartial(object.customOfficeOfExit)
      : undefined;
    message.goodsItems = object.goodsItems?.map((e) => GoodsItem3.fromPartial(e)) || [];
    message.transportToBorder = (object.transportToBorder !== undefined && object.transportToBorder !== null)
      ? Transport4.fromPartial(object.transportToBorder)
      : undefined;
    message.transportAtBorder = (object.transportAtBorder !== undefined && object.transportAtBorder !== null)
      ? Transport4.fromPartial(object.transportAtBorder)
      : undefined;
    message.exportId = object.exportId ?? "";
    message.uniqueConsignmentReference = object.uniqueConsignmentReference ?? "";
    message.localReferenceNumber = object.localReferenceNumber ?? "";
    message.destinationCountry = object.destinationCountry ?? "";
    message.exportCountry = object.exportCountry ?? "";
    message.itinerary = object.itinerary ?? "";
    message.releaseDateAndTime = object.releaseDateAndTime ?? "";
    message.incotermCode = object.incotermCode ?? "";
    message.incotermLocation = object.incotermLocation ?? "";
    message.totalGrossMass = object.totalGrossMass ?? "";
    message.goodsItemQuantity = object.goodsItemQuantity ?? 0;
    message.totalPackagesQuantity = object.totalPackagesQuantity ?? 0;
    message.natureOfTransaction = object.natureOfTransaction ?? 0;
    message.totalAmountInvoiced = object.totalAmountInvoiced ?? 0;
    message.invoiceCurrency = object.invoiceCurrency ?? "";
    message.user = object.user ?? "";
    message.timestamp = object.timestamp ?? "";
    return message;
  },
};

function createBaseMsgCreateExportEvent(): MsgCreateExportEvent {
  return { creator: "", index: "", status: "", message: "", eventType: "" };
}

export const MsgCreateExportEvent = {
  encode(message: MsgCreateExportEvent, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.index !== "") {
      writer.uint32(18).string(message.index);
    }
    if (message.status !== "") {
      writer.uint32(26).string(message.status);
    }
    if (message.message !== "") {
      writer.uint32(34).string(message.message);
    }
    if (message.eventType !== "") {
      writer.uint32(42).string(message.eventType);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateExportEvent {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateExportEvent();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.index = reader.string();
          break;
        case 3:
          message.status = reader.string();
          break;
        case 4:
          message.message = reader.string();
          break;
        case 5:
          message.eventType = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateExportEvent {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      index: isSet(object.index) ? String(object.index) : "",
      status: isSet(object.status) ? String(object.status) : "",
      message: isSet(object.message) ? String(object.message) : "",
      eventType: isSet(object.eventType) ? String(object.eventType) : "",
    };
  },

  toJSON(message: MsgCreateExportEvent): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.index !== undefined && (obj.index = message.index);
    message.status !== undefined && (obj.status = message.status);
    message.message !== undefined && (obj.message = message.message);
    message.eventType !== undefined && (obj.eventType = message.eventType);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateExportEvent>, I>>(object: I): MsgCreateExportEvent {
    const message = createBaseMsgCreateExportEvent();
    message.creator = object.creator ?? "";
    message.index = object.index ?? "";
    message.status = object.status ?? "";
    message.message = object.message ?? "";
    message.eventType = object.eventType ?? "";
    return message;
  },
};

function createBaseMsgCreateExportEventResponse(): MsgCreateExportEventResponse {
  return {};
}

export const MsgCreateExportEventResponse = {
  encode(_: MsgCreateExportEventResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateExportEventResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateExportEventResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgCreateExportEventResponse {
    return {};
  },

  toJSON(_: MsgCreateExportEventResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateExportEventResponse>, I>>(_: I): MsgCreateExportEventResponse {
    const message = createBaseMsgCreateExportEventResponse();
    return message;
  },
};

function createBaseMsgForwardExportToken(): MsgForwardExportToken {
  return { creator: "", id: "" };
}

export const MsgForwardExportToken = {
  encode(message: MsgForwardExportToken, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgForwardExportToken {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgForwardExportToken();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgForwardExportToken {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
    };
  },

  toJSON(message: MsgForwardExportToken): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgForwardExportToken>, I>>(object: I): MsgForwardExportToken {
    const message = createBaseMsgForwardExportToken();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgForwardExportTokenResponse(): MsgForwardExportTokenResponse {
  return { exportToken: undefined, events: [] };
}

export const MsgForwardExportTokenResponse = {
  encode(message: MsgForwardExportTokenResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.exportToken !== undefined) {
      ExportToken.encode(message.exportToken, writer.uint32(10).fork()).ldelim();
    }
    for (const v of message.events) {
      ExportEvent.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgForwardExportTokenResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgForwardExportTokenResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.exportToken = ExportToken.decode(reader, reader.uint32());
          break;
        case 2:
          message.events.push(ExportEvent.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgForwardExportTokenResponse {
    return {
      exportToken: isSet(object.exportToken) ? ExportToken.fromJSON(object.exportToken) : undefined,
      events: Array.isArray(object?.events) ? object.events.map((e: any) => ExportEvent.fromJSON(e)) : [],
    };
  },

  toJSON(message: MsgForwardExportTokenResponse): unknown {
    const obj: any = {};
    message.exportToken !== undefined
      && (obj.exportToken = message.exportToken ? ExportToken.toJSON(message.exportToken) : undefined);
    if (message.events) {
      obj.events = message.events.map((e) => e ? ExportEvent.toJSON(e) : undefined);
    } else {
      obj.events = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgForwardExportTokenResponse>, I>>(
    object: I,
  ): MsgForwardExportTokenResponse {
    const message = createBaseMsgForwardExportTokenResponse();
    message.exportToken = (object.exportToken !== undefined && object.exportToken !== null)
      ? ExportToken.fromPartial(object.exportToken)
      : undefined;
    message.events = object.events?.map((e) => ExportEvent.fromPartial(e)) || [];
    return message;
  },
};

function createBaseMsgEventPresentQrCode(): MsgEventPresentQrCode {
  return { creator: "", id: "", message: "" };
}

export const MsgEventPresentQrCode = {
  encode(message: MsgEventPresentQrCode, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.message !== "") {
      writer.uint32(26).string(message.message);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgEventPresentQrCode {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgEventPresentQrCode();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.message = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgEventPresentQrCode {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      message: isSet(object.message) ? String(object.message) : "",
    };
  },

  toJSON(message: MsgEventPresentQrCode): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.message !== undefined && (obj.message = message.message);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgEventPresentQrCode>, I>>(object: I): MsgEventPresentQrCode {
    const message = createBaseMsgEventPresentQrCode();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.message = object.message ?? "";
    return message;
  },
};

function createBaseMsgEventAcceptGoodsForTransport(): MsgEventAcceptGoodsForTransport {
  return { creator: "", id: "", message: "" };
}

export const MsgEventAcceptGoodsForTransport = {
  encode(message: MsgEventAcceptGoodsForTransport, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.message !== "") {
      writer.uint32(26).string(message.message);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgEventAcceptGoodsForTransport {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgEventAcceptGoodsForTransport();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.message = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgEventAcceptGoodsForTransport {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      message: isSet(object.message) ? String(object.message) : "",
    };
  },

  toJSON(message: MsgEventAcceptGoodsForTransport): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.message !== undefined && (obj.message = message.message);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgEventAcceptGoodsForTransport>, I>>(
    object: I,
  ): MsgEventAcceptGoodsForTransport {
    const message = createBaseMsgEventAcceptGoodsForTransport();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.message = object.message ?? "";
    return message;
  },
};

function createBaseMsgEventPresentGoods(): MsgEventPresentGoods {
  return { creator: "", id: "", message: "" };
}

export const MsgEventPresentGoods = {
  encode(message: MsgEventPresentGoods, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.message !== "") {
      writer.uint32(26).string(message.message);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgEventPresentGoods {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgEventPresentGoods();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.message = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgEventPresentGoods {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      message: isSet(object.message) ? String(object.message) : "",
    };
  },

  toJSON(message: MsgEventPresentGoods): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.message !== undefined && (obj.message = message.message);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgEventPresentGoods>, I>>(object: I): MsgEventPresentGoods {
    const message = createBaseMsgEventPresentGoods();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.message = object.message ?? "";
    return message;
  },
};

function createBaseMsgEventCertifyExitOfGoods(): MsgEventCertifyExitOfGoods {
  return { creator: "", id: "", message: "" };
}

export const MsgEventCertifyExitOfGoods = {
  encode(message: MsgEventCertifyExitOfGoods, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.message !== "") {
      writer.uint32(26).string(message.message);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgEventCertifyExitOfGoods {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgEventCertifyExitOfGoods();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.message = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgEventCertifyExitOfGoods {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      message: isSet(object.message) ? String(object.message) : "",
    };
  },

  toJSON(message: MsgEventCertifyExitOfGoods): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.message !== undefined && (obj.message = message.message);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgEventCertifyExitOfGoods>, I>>(object: I): MsgEventCertifyExitOfGoods {
    const message = createBaseMsgEventCertifyExitOfGoods();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.message = object.message ?? "";
    return message;
  },
};

function createBaseMsgEventMoveToArchive(): MsgEventMoveToArchive {
  return { creator: "", id: "", message: "" };
}

export const MsgEventMoveToArchive = {
  encode(message: MsgEventMoveToArchive, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.message !== "") {
      writer.uint32(26).string(message.message);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgEventMoveToArchive {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgEventMoveToArchive();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.message = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgEventMoveToArchive {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      message: isSet(object.message) ? String(object.message) : "",
    };
  },

  toJSON(message: MsgEventMoveToArchive): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.message !== undefined && (obj.message = message.message);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgEventMoveToArchive>, I>>(object: I): MsgEventMoveToArchive {
    const message = createBaseMsgEventMoveToArchive();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.message = object.message ?? "";
    return message;
  },
};

function createBaseMsgEventExportCheck(): MsgEventExportCheck {
  return { creator: "", id: "", message: "" };
}

export const MsgEventExportCheck = {
  encode(message: MsgEventExportCheck, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.message !== "") {
      writer.uint32(26).string(message.message);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgEventExportCheck {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgEventExportCheck();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.message = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgEventExportCheck {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      message: isSet(object.message) ? String(object.message) : "",
    };
  },

  toJSON(message: MsgEventExportCheck): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.message !== undefined && (obj.message = message.message);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgEventExportCheck>, I>>(object: I): MsgEventExportCheck {
    const message = createBaseMsgEventExportCheck();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.message = object.message ?? "";
    return message;
  },
};

function createBaseMsgEventAcceptExport(): MsgEventAcceptExport {
  return { creator: "", id: "", message: "" };
}

export const MsgEventAcceptExport = {
  encode(message: MsgEventAcceptExport, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.message !== "") {
      writer.uint32(26).string(message.message);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgEventAcceptExport {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgEventAcceptExport();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.message = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgEventAcceptExport {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      message: isSet(object.message) ? String(object.message) : "",
    };
  },

  toJSON(message: MsgEventAcceptExport): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.message !== undefined && (obj.message = message.message);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgEventAcceptExport>, I>>(object: I): MsgEventAcceptExport {
    const message = createBaseMsgEventAcceptExport();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.message = object.message ?? "";
    return message;
  },
};

function createBaseMsgEventExportRefusal(): MsgEventExportRefusal {
  return { creator: "", id: "", message: "" };
}

export const MsgEventExportRefusal = {
  encode(message: MsgEventExportRefusal, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.message !== "") {
      writer.uint32(26).string(message.message);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgEventExportRefusal {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgEventExportRefusal();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.message = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgEventExportRefusal {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      message: isSet(object.message) ? String(object.message) : "",
    };
  },

  toJSON(message: MsgEventExportRefusal): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.message !== undefined && (obj.message = message.message);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgEventExportRefusal>, I>>(object: I): MsgEventExportRefusal {
    const message = createBaseMsgEventExportRefusal();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.message = object.message ?? "";
    return message;
  },
};

function createBaseMsgEventAcceptImport(): MsgEventAcceptImport {
  return { creator: "", id: "", message: "" };
}

export const MsgEventAcceptImport = {
  encode(message: MsgEventAcceptImport, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.message !== "") {
      writer.uint32(26).string(message.message);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgEventAcceptImport {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgEventAcceptImport();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.message = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgEventAcceptImport {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      message: isSet(object.message) ? String(object.message) : "",
    };
  },

  toJSON(message: MsgEventAcceptImport): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.message !== undefined && (obj.message = message.message);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgEventAcceptImport>, I>>(object: I): MsgEventAcceptImport {
    const message = createBaseMsgEventAcceptImport();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.message = object.message ?? "";
    return message;
  },
};

function createBaseMsgEventCompleteImport(): MsgEventCompleteImport {
  return { creator: "", id: "", message: "" };
}

export const MsgEventCompleteImport = {
  encode(message: MsgEventCompleteImport, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.message !== "") {
      writer.uint32(26).string(message.message);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgEventCompleteImport {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgEventCompleteImport();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.message = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgEventCompleteImport {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      message: isSet(object.message) ? String(object.message) : "",
    };
  },

  toJSON(message: MsgEventCompleteImport): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.message !== undefined && (obj.message = message.message);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgEventCompleteImport>, I>>(object: I): MsgEventCompleteImport {
    const message = createBaseMsgEventCompleteImport();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.message = object.message ?? "";
    return message;
  },
};

function createBaseMsgEventImportProposal(): MsgEventImportProposal {
  return { creator: "", id: "", message: "" };
}

export const MsgEventImportProposal = {
  encode(message: MsgEventImportProposal, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.message !== "") {
      writer.uint32(26).string(message.message);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgEventImportProposal {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgEventImportProposal();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.message = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgEventImportProposal {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      message: isSet(object.message) ? String(object.message) : "",
    };
  },

  toJSON(message: MsgEventImportProposal): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.message !== undefined && (obj.message = message.message);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgEventImportProposal>, I>>(object: I): MsgEventImportProposal {
    const message = createBaseMsgEventImportProposal();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.message = object.message ?? "";
    return message;
  },
};

function createBaseMsgEventImportRefusal(): MsgEventImportRefusal {
  return { creator: "", id: "", message: "" };
}

export const MsgEventImportRefusal = {
  encode(message: MsgEventImportRefusal, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.message !== "") {
      writer.uint32(26).string(message.message);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgEventImportRefusal {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgEventImportRefusal();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.message = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgEventImportRefusal {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      message: isSet(object.message) ? String(object.message) : "",
    };
  },

  toJSON(message: MsgEventImportRefusal): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.message !== undefined && (obj.message = message.message);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgEventImportRefusal>, I>>(object: I): MsgEventImportRefusal {
    const message = createBaseMsgEventImportRefusal();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.message = object.message ?? "";
    return message;
  },
};

function createBaseMsgEventImportSupplement(): MsgEventImportSupplement {
  return { creator: "", id: "", message: "" };
}

export const MsgEventImportSupplement = {
  encode(message: MsgEventImportSupplement, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.message !== "") {
      writer.uint32(26).string(message.message);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgEventImportSupplement {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgEventImportSupplement();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.message = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgEventImportSupplement {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      message: isSet(object.message) ? String(object.message) : "",
    };
  },

  toJSON(message: MsgEventImportSupplement): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.message !== undefined && (obj.message = message.message);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgEventImportSupplement>, I>>(object: I): MsgEventImportSupplement {
    const message = createBaseMsgEventImportSupplement();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.message = object.message ?? "";
    return message;
  },
};

function createBaseMsgEventImportExport(): MsgEventImportExport {
  return { creator: "", id: "", message: "" };
}

export const MsgEventImportExport = {
  encode(message: MsgEventImportExport, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.message !== "") {
      writer.uint32(26).string(message.message);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgEventImportExport {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgEventImportExport();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.message = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgEventImportExport {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      message: isSet(object.message) ? String(object.message) : "",
    };
  },

  toJSON(message: MsgEventImportExport): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.message !== undefined && (obj.message = message.message);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgEventImportExport>, I>>(object: I): MsgEventImportExport {
    const message = createBaseMsgEventImportExport();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.message = object.message ?? "";
    return message;
  },
};

function createBaseMsgEventImportSubmit(): MsgEventImportSubmit {
  return { creator: "", id: "", message: "" };
}

export const MsgEventImportSubmit = {
  encode(message: MsgEventImportSubmit, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.message !== "") {
      writer.uint32(26).string(message.message);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgEventImportSubmit {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgEventImportSubmit();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.message = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgEventImportSubmit {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      message: isSet(object.message) ? String(object.message) : "",
    };
  },

  toJSON(message: MsgEventImportSubmit): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.message !== undefined && (obj.message = message.message);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgEventImportSubmit>, I>>(object: I): MsgEventImportSubmit {
    const message = createBaseMsgEventImportSubmit();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.message = object.message ?? "";
    return message;
  },
};

function createBaseMsgEventImportRelease(): MsgEventImportRelease {
  return { creator: "", id: "", message: "" };
}

export const MsgEventImportRelease = {
  encode(message: MsgEventImportRelease, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.message !== "") {
      writer.uint32(26).string(message.message);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgEventImportRelease {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgEventImportRelease();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.message = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgEventImportRelease {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      message: isSet(object.message) ? String(object.message) : "",
    };
  },

  toJSON(message: MsgEventImportRelease): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.message !== undefined && (obj.message = message.message);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgEventImportRelease>, I>>(object: I): MsgEventImportRelease {
    const message = createBaseMsgEventImportRelease();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.message = object.message ?? "";
    return message;
  },
};

function createBaseMsgEventStatusResponseExport(): MsgEventStatusResponseExport {
  return { exportToken: undefined, events: [] };
}

export const MsgEventStatusResponseExport = {
  encode(message: MsgEventStatusResponseExport, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.exportToken !== undefined) {
      ExportToken.encode(message.exportToken, writer.uint32(10).fork()).ldelim();
    }
    for (const v of message.events) {
      ExportEvent.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgEventStatusResponseExport {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgEventStatusResponseExport();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.exportToken = ExportToken.decode(reader, reader.uint32());
          break;
        case 2:
          message.events.push(ExportEvent.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgEventStatusResponseExport {
    return {
      exportToken: isSet(object.exportToken) ? ExportToken.fromJSON(object.exportToken) : undefined,
      events: Array.isArray(object?.events) ? object.events.map((e: any) => ExportEvent.fromJSON(e)) : [],
    };
  },

  toJSON(message: MsgEventStatusResponseExport): unknown {
    const obj: any = {};
    message.exportToken !== undefined
      && (obj.exportToken = message.exportToken ? ExportToken.toJSON(message.exportToken) : undefined);
    if (message.events) {
      obj.events = message.events.map((e) => e ? ExportEvent.toJSON(e) : undefined);
    } else {
      obj.events = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgEventStatusResponseExport>, I>>(object: I): MsgEventStatusResponseExport {
    const message = createBaseMsgEventStatusResponseExport();
    message.exportToken = (object.exportToken !== undefined && object.exportToken !== null)
      ? ExportToken.fromPartial(object.exportToken)
      : undefined;
    message.events = object.events?.map((e) => ExportEvent.fromPartial(e)) || [];
    return message;
  },
};

function createBaseMsgEventStatusResponseImport(): MsgEventStatusResponseImport {
  return { importToken: undefined, events: [] };
}

export const MsgEventStatusResponseImport = {
  encode(message: MsgEventStatusResponseImport, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.importToken !== undefined) {
      ImportToken.encode(message.importToken, writer.uint32(10).fork()).ldelim();
    }
    for (const v of message.events) {
      ImportEvent.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgEventStatusResponseImport {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgEventStatusResponseImport();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.importToken = ImportToken.decode(reader, reader.uint32());
          break;
        case 2:
          message.events.push(ImportEvent.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgEventStatusResponseImport {
    return {
      importToken: isSet(object.importToken) ? ImportToken.fromJSON(object.importToken) : undefined,
      events: Array.isArray(object?.events) ? object.events.map((e: any) => ImportEvent.fromJSON(e)) : [],
    };
  },

  toJSON(message: MsgEventStatusResponseImport): unknown {
    const obj: any = {};
    message.importToken !== undefined
      && (obj.importToken = message.importToken ? ImportToken.toJSON(message.importToken) : undefined);
    if (message.events) {
      obj.events = message.events.map((e) => e ? ImportEvent.toJSON(e) : undefined);
    } else {
      obj.events = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgEventStatusResponseImport>, I>>(object: I): MsgEventStatusResponseImport {
    const message = createBaseMsgEventStatusResponseImport();
    message.importToken = (object.importToken !== undefined && object.importToken !== null)
      ? ImportToken.fromPartial(object.importToken)
      : undefined;
    message.events = object.events?.map((e) => ImportEvent.fromPartial(e)) || [];
    return message;
  },
};

function createBaseMsgCreateExportTokenResponse(): MsgCreateExportTokenResponse {
  return { exportToken: undefined, events: [] };
}

export const MsgCreateExportTokenResponse = {
  encode(message: MsgCreateExportTokenResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.exportToken !== undefined) {
      ExportToken.encode(message.exportToken, writer.uint32(10).fork()).ldelim();
    }
    for (const v of message.events) {
      ExportEvent.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateExportTokenResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateExportTokenResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.exportToken = ExportToken.decode(reader, reader.uint32());
          break;
        case 2:
          message.events.push(ExportEvent.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateExportTokenResponse {
    return {
      exportToken: isSet(object.exportToken) ? ExportToken.fromJSON(object.exportToken) : undefined,
      events: Array.isArray(object?.events) ? object.events.map((e: any) => ExportEvent.fromJSON(e)) : [],
    };
  },

  toJSON(message: MsgCreateExportTokenResponse): unknown {
    const obj: any = {};
    message.exportToken !== undefined
      && (obj.exportToken = message.exportToken ? ExportToken.toJSON(message.exportToken) : undefined);
    if (message.events) {
      obj.events = message.events.map((e) => e ? ExportEvent.toJSON(e) : undefined);
    } else {
      obj.events = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateExportTokenResponse>, I>>(object: I): MsgCreateExportTokenResponse {
    const message = createBaseMsgCreateExportTokenResponse();
    message.exportToken = (object.exportToken !== undefined && object.exportToken !== null)
      ? ExportToken.fromPartial(object.exportToken)
      : undefined;
    message.events = object.events?.map((e) => ExportEvent.fromPartial(e)) || [];
    return message;
  },
};

function createBaseMsgUpdateExportToken(): MsgUpdateExportToken {
  return {
    creator: "",
    id: "",
    exporter: undefined,
    declarant: undefined,
    representative: undefined,
    consignee: undefined,
    customsOfficeOfExport: undefined,
    customOfficeOfExit: undefined,
    goodsItems: [],
    transportToBorder: undefined,
    transportAtBorder: undefined,
    exportId: "",
    uniqueConsignmentReference: "",
    localReferenceNumber: "",
    destinationCountry: "",
    exportCountry: "",
    itinerary: "",
    releaseDateAndTime: "",
    incotermCode: "",
    incotermLocation: "",
    totalGrossMass: "",
    goodsItemQuantity: 0,
    totalPackagesQuantity: 0,
    natureOfTransaction: 0,
    totalAmountInvoiced: 0,
    invoiceCurrency: "",
    status: "",
    user: "",
    timestamp: "",
  };
}

export const MsgUpdateExportToken = {
  encode(message: MsgUpdateExportToken, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.exporter !== undefined) {
      Company1.encode(message.exporter, writer.uint32(26).fork()).ldelim();
    }
    if (message.declarant !== undefined) {
      Company1.encode(message.declarant, writer.uint32(34).fork()).ldelim();
    }
    if (message.representative !== undefined) {
      Company1.encode(message.representative, writer.uint32(42).fork()).ldelim();
    }
    if (message.consignee !== undefined) {
      Company1.encode(message.consignee, writer.uint32(50).fork()).ldelim();
    }
    if (message.customsOfficeOfExport !== undefined) {
      CustomsOffice2.encode(message.customsOfficeOfExport, writer.uint32(58).fork()).ldelim();
    }
    if (message.customOfficeOfExit !== undefined) {
      CustomsOffice2.encode(message.customOfficeOfExit, writer.uint32(66).fork()).ldelim();
    }
    for (const v of message.goodsItems) {
      GoodsItem3.encode(v!, writer.uint32(74).fork()).ldelim();
    }
    if (message.transportToBorder !== undefined) {
      Transport4.encode(message.transportToBorder, writer.uint32(82).fork()).ldelim();
    }
    if (message.transportAtBorder !== undefined) {
      Transport4.encode(message.transportAtBorder, writer.uint32(90).fork()).ldelim();
    }
    if (message.exportId !== "") {
      writer.uint32(98).string(message.exportId);
    }
    if (message.uniqueConsignmentReference !== "") {
      writer.uint32(106).string(message.uniqueConsignmentReference);
    }
    if (message.localReferenceNumber !== "") {
      writer.uint32(114).string(message.localReferenceNumber);
    }
    if (message.destinationCountry !== "") {
      writer.uint32(122).string(message.destinationCountry);
    }
    if (message.exportCountry !== "") {
      writer.uint32(130).string(message.exportCountry);
    }
    if (message.itinerary !== "") {
      writer.uint32(138).string(message.itinerary);
    }
    if (message.releaseDateAndTime !== "") {
      writer.uint32(146).string(message.releaseDateAndTime);
    }
    if (message.incotermCode !== "") {
      writer.uint32(154).string(message.incotermCode);
    }
    if (message.incotermLocation !== "") {
      writer.uint32(162).string(message.incotermLocation);
    }
    if (message.totalGrossMass !== "") {
      writer.uint32(170).string(message.totalGrossMass);
    }
    if (message.goodsItemQuantity !== 0) {
      writer.uint32(176).int64(message.goodsItemQuantity);
    }
    if (message.totalPackagesQuantity !== 0) {
      writer.uint32(184).int64(message.totalPackagesQuantity);
    }
    if (message.natureOfTransaction !== 0) {
      writer.uint32(192).int64(message.natureOfTransaction);
    }
    if (message.totalAmountInvoiced !== 0) {
      writer.uint32(200).int64(message.totalAmountInvoiced);
    }
    if (message.invoiceCurrency !== "") {
      writer.uint32(210).string(message.invoiceCurrency);
    }
    if (message.status !== "") {
      writer.uint32(218).string(message.status);
    }
    if (message.user !== "") {
      writer.uint32(226).string(message.user);
    }
    if (message.timestamp !== "") {
      writer.uint32(234).string(message.timestamp);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgUpdateExportToken {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgUpdateExportToken();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.exporter = Company1.decode(reader, reader.uint32());
          break;
        case 4:
          message.declarant = Company1.decode(reader, reader.uint32());
          break;
        case 5:
          message.representative = Company1.decode(reader, reader.uint32());
          break;
        case 6:
          message.consignee = Company1.decode(reader, reader.uint32());
          break;
        case 7:
          message.customsOfficeOfExport = CustomsOffice2.decode(reader, reader.uint32());
          break;
        case 8:
          message.customOfficeOfExit = CustomsOffice2.decode(reader, reader.uint32());
          break;
        case 9:
          message.goodsItems.push(GoodsItem3.decode(reader, reader.uint32()));
          break;
        case 10:
          message.transportToBorder = Transport4.decode(reader, reader.uint32());
          break;
        case 11:
          message.transportAtBorder = Transport4.decode(reader, reader.uint32());
          break;
        case 12:
          message.exportId = reader.string();
          break;
        case 13:
          message.uniqueConsignmentReference = reader.string();
          break;
        case 14:
          message.localReferenceNumber = reader.string();
          break;
        case 15:
          message.destinationCountry = reader.string();
          break;
        case 16:
          message.exportCountry = reader.string();
          break;
        case 17:
          message.itinerary = reader.string();
          break;
        case 18:
          message.releaseDateAndTime = reader.string();
          break;
        case 19:
          message.incotermCode = reader.string();
          break;
        case 20:
          message.incotermLocation = reader.string();
          break;
        case 21:
          message.totalGrossMass = reader.string();
          break;
        case 22:
          message.goodsItemQuantity = longToNumber(reader.int64() as Long);
          break;
        case 23:
          message.totalPackagesQuantity = longToNumber(reader.int64() as Long);
          break;
        case 24:
          message.natureOfTransaction = longToNumber(reader.int64() as Long);
          break;
        case 25:
          message.totalAmountInvoiced = longToNumber(reader.int64() as Long);
          break;
        case 26:
          message.invoiceCurrency = reader.string();
          break;
        case 27:
          message.status = reader.string();
          break;
        case 28:
          message.user = reader.string();
          break;
        case 29:
          message.timestamp = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateExportToken {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      exporter: isSet(object.exporter) ? Company1.fromJSON(object.exporter) : undefined,
      declarant: isSet(object.declarant) ? Company1.fromJSON(object.declarant) : undefined,
      representative: isSet(object.representative) ? Company1.fromJSON(object.representative) : undefined,
      consignee: isSet(object.consignee) ? Company1.fromJSON(object.consignee) : undefined,
      customsOfficeOfExport: isSet(object.customsOfficeOfExport)
        ? CustomsOffice2.fromJSON(object.customsOfficeOfExport)
        : undefined,
      customOfficeOfExit: isSet(object.customOfficeOfExit)
        ? CustomsOffice2.fromJSON(object.customOfficeOfExit)
        : undefined,
      goodsItems: Array.isArray(object?.goodsItems) ? object.goodsItems.map((e: any) => GoodsItem3.fromJSON(e)) : [],
      transportToBorder: isSet(object.transportToBorder) ? Transport4.fromJSON(object.transportToBorder) : undefined,
      transportAtBorder: isSet(object.transportAtBorder) ? Transport4.fromJSON(object.transportAtBorder) : undefined,
      exportId: isSet(object.exportId) ? String(object.exportId) : "",
      uniqueConsignmentReference: isSet(object.uniqueConsignmentReference)
        ? String(object.uniqueConsignmentReference)
        : "",
      localReferenceNumber: isSet(object.localReferenceNumber) ? String(object.localReferenceNumber) : "",
      destinationCountry: isSet(object.destinationCountry) ? String(object.destinationCountry) : "",
      exportCountry: isSet(object.exportCountry) ? String(object.exportCountry) : "",
      itinerary: isSet(object.itinerary) ? String(object.itinerary) : "",
      releaseDateAndTime: isSet(object.releaseDateAndTime) ? String(object.releaseDateAndTime) : "",
      incotermCode: isSet(object.incotermCode) ? String(object.incotermCode) : "",
      incotermLocation: isSet(object.incotermLocation) ? String(object.incotermLocation) : "",
      totalGrossMass: isSet(object.totalGrossMass) ? String(object.totalGrossMass) : "",
      goodsItemQuantity: isSet(object.goodsItemQuantity) ? Number(object.goodsItemQuantity) : 0,
      totalPackagesQuantity: isSet(object.totalPackagesQuantity) ? Number(object.totalPackagesQuantity) : 0,
      natureOfTransaction: isSet(object.natureOfTransaction) ? Number(object.natureOfTransaction) : 0,
      totalAmountInvoiced: isSet(object.totalAmountInvoiced) ? Number(object.totalAmountInvoiced) : 0,
      invoiceCurrency: isSet(object.invoiceCurrency) ? String(object.invoiceCurrency) : "",
      status: isSet(object.status) ? String(object.status) : "",
      user: isSet(object.user) ? String(object.user) : "",
      timestamp: isSet(object.timestamp) ? String(object.timestamp) : "",
    };
  },

  toJSON(message: MsgUpdateExportToken): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.exporter !== undefined && (obj.exporter = message.exporter ? Company1.toJSON(message.exporter) : undefined);
    message.declarant !== undefined
      && (obj.declarant = message.declarant ? Company1.toJSON(message.declarant) : undefined);
    message.representative !== undefined
      && (obj.representative = message.representative ? Company1.toJSON(message.representative) : undefined);
    message.consignee !== undefined
      && (obj.consignee = message.consignee ? Company1.toJSON(message.consignee) : undefined);
    message.customsOfficeOfExport !== undefined && (obj.customsOfficeOfExport = message.customsOfficeOfExport
      ? CustomsOffice2.toJSON(message.customsOfficeOfExport)
      : undefined);
    message.customOfficeOfExit !== undefined && (obj.customOfficeOfExit = message.customOfficeOfExit
      ? CustomsOffice2.toJSON(message.customOfficeOfExit)
      : undefined);
    if (message.goodsItems) {
      obj.goodsItems = message.goodsItems.map((e) => e ? GoodsItem3.toJSON(e) : undefined);
    } else {
      obj.goodsItems = [];
    }
    message.transportToBorder !== undefined
      && (obj.transportToBorder = message.transportToBorder ? Transport4.toJSON(message.transportToBorder) : undefined);
    message.transportAtBorder !== undefined
      && (obj.transportAtBorder = message.transportAtBorder ? Transport4.toJSON(message.transportAtBorder) : undefined);
    message.exportId !== undefined && (obj.exportId = message.exportId);
    message.uniqueConsignmentReference !== undefined
      && (obj.uniqueConsignmentReference = message.uniqueConsignmentReference);
    message.localReferenceNumber !== undefined && (obj.localReferenceNumber = message.localReferenceNumber);
    message.destinationCountry !== undefined && (obj.destinationCountry = message.destinationCountry);
    message.exportCountry !== undefined && (obj.exportCountry = message.exportCountry);
    message.itinerary !== undefined && (obj.itinerary = message.itinerary);
    message.releaseDateAndTime !== undefined && (obj.releaseDateAndTime = message.releaseDateAndTime);
    message.incotermCode !== undefined && (obj.incotermCode = message.incotermCode);
    message.incotermLocation !== undefined && (obj.incotermLocation = message.incotermLocation);
    message.totalGrossMass !== undefined && (obj.totalGrossMass = message.totalGrossMass);
    message.goodsItemQuantity !== undefined && (obj.goodsItemQuantity = Math.round(message.goodsItemQuantity));
    message.totalPackagesQuantity !== undefined
      && (obj.totalPackagesQuantity = Math.round(message.totalPackagesQuantity));
    message.natureOfTransaction !== undefined && (obj.natureOfTransaction = Math.round(message.natureOfTransaction));
    message.totalAmountInvoiced !== undefined && (obj.totalAmountInvoiced = Math.round(message.totalAmountInvoiced));
    message.invoiceCurrency !== undefined && (obj.invoiceCurrency = message.invoiceCurrency);
    message.status !== undefined && (obj.status = message.status);
    message.user !== undefined && (obj.user = message.user);
    message.timestamp !== undefined && (obj.timestamp = message.timestamp);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgUpdateExportToken>, I>>(object: I): MsgUpdateExportToken {
    const message = createBaseMsgUpdateExportToken();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.exporter = (object.exporter !== undefined && object.exporter !== null)
      ? Company1.fromPartial(object.exporter)
      : undefined;
    message.declarant = (object.declarant !== undefined && object.declarant !== null)
      ? Company1.fromPartial(object.declarant)
      : undefined;
    message.representative = (object.representative !== undefined && object.representative !== null)
      ? Company1.fromPartial(object.representative)
      : undefined;
    message.consignee = (object.consignee !== undefined && object.consignee !== null)
      ? Company1.fromPartial(object.consignee)
      : undefined;
    message.customsOfficeOfExport =
      (object.customsOfficeOfExport !== undefined && object.customsOfficeOfExport !== null)
        ? CustomsOffice2.fromPartial(object.customsOfficeOfExport)
        : undefined;
    message.customOfficeOfExit = (object.customOfficeOfExit !== undefined && object.customOfficeOfExit !== null)
      ? CustomsOffice2.fromPartial(object.customOfficeOfExit)
      : undefined;
    message.goodsItems = object.goodsItems?.map((e) => GoodsItem3.fromPartial(e)) || [];
    message.transportToBorder = (object.transportToBorder !== undefined && object.transportToBorder !== null)
      ? Transport4.fromPartial(object.transportToBorder)
      : undefined;
    message.transportAtBorder = (object.transportAtBorder !== undefined && object.transportAtBorder !== null)
      ? Transport4.fromPartial(object.transportAtBorder)
      : undefined;
    message.exportId = object.exportId ?? "";
    message.uniqueConsignmentReference = object.uniqueConsignmentReference ?? "";
    message.localReferenceNumber = object.localReferenceNumber ?? "";
    message.destinationCountry = object.destinationCountry ?? "";
    message.exportCountry = object.exportCountry ?? "";
    message.itinerary = object.itinerary ?? "";
    message.releaseDateAndTime = object.releaseDateAndTime ?? "";
    message.incotermCode = object.incotermCode ?? "";
    message.incotermLocation = object.incotermLocation ?? "";
    message.totalGrossMass = object.totalGrossMass ?? "";
    message.goodsItemQuantity = object.goodsItemQuantity ?? 0;
    message.totalPackagesQuantity = object.totalPackagesQuantity ?? 0;
    message.natureOfTransaction = object.natureOfTransaction ?? 0;
    message.totalAmountInvoiced = object.totalAmountInvoiced ?? 0;
    message.invoiceCurrency = object.invoiceCurrency ?? "";
    message.status = object.status ?? "";
    message.user = object.user ?? "";
    message.timestamp = object.timestamp ?? "";
    return message;
  },
};

function createBaseMsgUpdateExportTokenResponse(): MsgUpdateExportTokenResponse {
  return { exportToken: undefined, events: [] };
}

export const MsgUpdateExportTokenResponse = {
  encode(message: MsgUpdateExportTokenResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.exportToken !== undefined) {
      ExportToken.encode(message.exportToken, writer.uint32(10).fork()).ldelim();
    }
    for (const v of message.events) {
      ExportEvent.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgUpdateExportTokenResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgUpdateExportTokenResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.exportToken = ExportToken.decode(reader, reader.uint32());
          break;
        case 2:
          message.events.push(ExportEvent.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateExportTokenResponse {
    return {
      exportToken: isSet(object.exportToken) ? ExportToken.fromJSON(object.exportToken) : undefined,
      events: Array.isArray(object?.events) ? object.events.map((e: any) => ExportEvent.fromJSON(e)) : [],
    };
  },

  toJSON(message: MsgUpdateExportTokenResponse): unknown {
    const obj: any = {};
    message.exportToken !== undefined
      && (obj.exportToken = message.exportToken ? ExportToken.toJSON(message.exportToken) : undefined);
    if (message.events) {
      obj.events = message.events.map((e) => e ? ExportEvent.toJSON(e) : undefined);
    } else {
      obj.events = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgUpdateExportTokenResponse>, I>>(object: I): MsgUpdateExportTokenResponse {
    const message = createBaseMsgUpdateExportTokenResponse();
    message.exportToken = (object.exportToken !== undefined && object.exportToken !== null)
      ? ExportToken.fromPartial(object.exportToken)
      : undefined;
    message.events = object.events?.map((e) => ExportEvent.fromPartial(e)) || [];
    return message;
  },
};

function createBaseMsgDeleteExportTokenResponse(): MsgDeleteExportTokenResponse {
  return {};
}

export const MsgDeleteExportTokenResponse = {
  encode(_: MsgDeleteExportTokenResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgDeleteExportTokenResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgDeleteExportTokenResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgDeleteExportTokenResponse {
    return {};
  },

  toJSON(_: MsgDeleteExportTokenResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgDeleteExportTokenResponse>, I>>(_: I): MsgDeleteExportTokenResponse {
    const message = createBaseMsgDeleteExportTokenResponse();
    return message;
  },
};

function createBaseMsgCreateImportEvent(): MsgCreateImportEvent {
  return { creator: "", index: "", status: "", message: "", eventType: "" };
}

export const MsgCreateImportEvent = {
  encode(message: MsgCreateImportEvent, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.index !== "") {
      writer.uint32(18).string(message.index);
    }
    if (message.status !== "") {
      writer.uint32(26).string(message.status);
    }
    if (message.message !== "") {
      writer.uint32(34).string(message.message);
    }
    if (message.eventType !== "") {
      writer.uint32(42).string(message.eventType);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateImportEvent {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateImportEvent();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.index = reader.string();
          break;
        case 3:
          message.status = reader.string();
          break;
        case 4:
          message.message = reader.string();
          break;
        case 5:
          message.eventType = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateImportEvent {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      index: isSet(object.index) ? String(object.index) : "",
      status: isSet(object.status) ? String(object.status) : "",
      message: isSet(object.message) ? String(object.message) : "",
      eventType: isSet(object.eventType) ? String(object.eventType) : "",
    };
  },

  toJSON(message: MsgCreateImportEvent): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.index !== undefined && (obj.index = message.index);
    message.status !== undefined && (obj.status = message.status);
    message.message !== undefined && (obj.message = message.message);
    message.eventType !== undefined && (obj.eventType = message.eventType);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateImportEvent>, I>>(object: I): MsgCreateImportEvent {
    const message = createBaseMsgCreateImportEvent();
    message.creator = object.creator ?? "";
    message.index = object.index ?? "";
    message.status = object.status ?? "";
    message.message = object.message ?? "";
    message.eventType = object.eventType ?? "";
    return message;
  },
};

function createBaseMsgCreateImportEventResponse(): MsgCreateImportEventResponse {
  return {};
}

export const MsgCreateImportEventResponse = {
  encode(_: MsgCreateImportEventResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateImportEventResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateImportEventResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgCreateImportEventResponse {
    return {};
  },

  toJSON(_: MsgCreateImportEventResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateImportEventResponse>, I>>(_: I): MsgCreateImportEventResponse {
    const message = createBaseMsgCreateImportEventResponse();
    return message;
  },
};

function createBaseMsgCreateImportToken(): MsgCreateImportToken {
  return {
    creator: "",
    id: "",
    consignor: undefined,
    exporter: undefined,
    consignee: undefined,
    declarant: undefined,
    customOfficeOfExit: undefined,
    customOfficeOfEntry: undefined,
    customOfficeOfImport: undefined,
    goodsItems: [],
    transportAtBorder: undefined,
    transportFromBorder: undefined,
    importId: "",
    uniqueConsignmentReference: "",
    localReferenceNumber: "",
    destinationCountry: "",
    exportCountry: "",
    itinerary: "",
    incotermCode: "",
    incotermLocation: "",
    totalGrossMass: "",
    goodsItemQuantity: 0,
    totalPackagesQuantity: 0,
    releaseDateAndTime: "",
    natureOfTransaction: 0,
    totalAmountInvoiced: 0,
    invoiceCurrency: "",
    status: "",
    relatedExportToken: "",
    tokenWalletId: "",
  };
}

export const MsgCreateImportToken = {
  encode(message: MsgCreateImportToken, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.consignor !== undefined) {
      Company5.encode(message.consignor, writer.uint32(26).fork()).ldelim();
    }
    if (message.exporter !== undefined) {
      Company5.encode(message.exporter, writer.uint32(34).fork()).ldelim();
    }
    if (message.consignee !== undefined) {
      Company5.encode(message.consignee, writer.uint32(42).fork()).ldelim();
    }
    if (message.declarant !== undefined) {
      Company5.encode(message.declarant, writer.uint32(50).fork()).ldelim();
    }
    if (message.customOfficeOfExit !== undefined) {
      CustomsOffice6.encode(message.customOfficeOfExit, writer.uint32(58).fork()).ldelim();
    }
    if (message.customOfficeOfEntry !== undefined) {
      CustomsOffice6.encode(message.customOfficeOfEntry, writer.uint32(66).fork()).ldelim();
    }
    if (message.customOfficeOfImport !== undefined) {
      CustomsOffice6.encode(message.customOfficeOfImport, writer.uint32(74).fork()).ldelim();
    }
    for (const v of message.goodsItems) {
      GoodsItem7.encode(v!, writer.uint32(82).fork()).ldelim();
    }
    if (message.transportAtBorder !== undefined) {
      Transport8.encode(message.transportAtBorder, writer.uint32(90).fork()).ldelim();
    }
    if (message.transportFromBorder !== undefined) {
      Transport8.encode(message.transportFromBorder, writer.uint32(98).fork()).ldelim();
    }
    if (message.importId !== "") {
      writer.uint32(106).string(message.importId);
    }
    if (message.uniqueConsignmentReference !== "") {
      writer.uint32(114).string(message.uniqueConsignmentReference);
    }
    if (message.localReferenceNumber !== "") {
      writer.uint32(122).string(message.localReferenceNumber);
    }
    if (message.destinationCountry !== "") {
      writer.uint32(130).string(message.destinationCountry);
    }
    if (message.exportCountry !== "") {
      writer.uint32(138).string(message.exportCountry);
    }
    if (message.itinerary !== "") {
      writer.uint32(146).string(message.itinerary);
    }
    if (message.incotermCode !== "") {
      writer.uint32(154).string(message.incotermCode);
    }
    if (message.incotermLocation !== "") {
      writer.uint32(162).string(message.incotermLocation);
    }
    if (message.totalGrossMass !== "") {
      writer.uint32(170).string(message.totalGrossMass);
    }
    if (message.goodsItemQuantity !== 0) {
      writer.uint32(176).int64(message.goodsItemQuantity);
    }
    if (message.totalPackagesQuantity !== 0) {
      writer.uint32(184).int64(message.totalPackagesQuantity);
    }
    if (message.releaseDateAndTime !== "") {
      writer.uint32(194).string(message.releaseDateAndTime);
    }
    if (message.natureOfTransaction !== 0) {
      writer.uint32(200).int64(message.natureOfTransaction);
    }
    if (message.totalAmountInvoiced !== 0) {
      writer.uint32(208).int64(message.totalAmountInvoiced);
    }
    if (message.invoiceCurrency !== "") {
      writer.uint32(218).string(message.invoiceCurrency);
    }
    if (message.status !== "") {
      writer.uint32(226).string(message.status);
    }
    if (message.relatedExportToken !== "") {
      writer.uint32(234).string(message.relatedExportToken);
    }
    if (message.tokenWalletId !== "") {
      writer.uint32(242).string(message.tokenWalletId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateImportToken {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateImportToken();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.consignor = Company5.decode(reader, reader.uint32());
          break;
        case 4:
          message.exporter = Company5.decode(reader, reader.uint32());
          break;
        case 5:
          message.consignee = Company5.decode(reader, reader.uint32());
          break;
        case 6:
          message.declarant = Company5.decode(reader, reader.uint32());
          break;
        case 7:
          message.customOfficeOfExit = CustomsOffice6.decode(reader, reader.uint32());
          break;
        case 8:
          message.customOfficeOfEntry = CustomsOffice6.decode(reader, reader.uint32());
          break;
        case 9:
          message.customOfficeOfImport = CustomsOffice6.decode(reader, reader.uint32());
          break;
        case 10:
          message.goodsItems.push(GoodsItem7.decode(reader, reader.uint32()));
          break;
        case 11:
          message.transportAtBorder = Transport8.decode(reader, reader.uint32());
          break;
        case 12:
          message.transportFromBorder = Transport8.decode(reader, reader.uint32());
          break;
        case 13:
          message.importId = reader.string();
          break;
        case 14:
          message.uniqueConsignmentReference = reader.string();
          break;
        case 15:
          message.localReferenceNumber = reader.string();
          break;
        case 16:
          message.destinationCountry = reader.string();
          break;
        case 17:
          message.exportCountry = reader.string();
          break;
        case 18:
          message.itinerary = reader.string();
          break;
        case 19:
          message.incotermCode = reader.string();
          break;
        case 20:
          message.incotermLocation = reader.string();
          break;
        case 21:
          message.totalGrossMass = reader.string();
          break;
        case 22:
          message.goodsItemQuantity = longToNumber(reader.int64() as Long);
          break;
        case 23:
          message.totalPackagesQuantity = longToNumber(reader.int64() as Long);
          break;
        case 24:
          message.releaseDateAndTime = reader.string();
          break;
        case 25:
          message.natureOfTransaction = longToNumber(reader.int64() as Long);
          break;
        case 26:
          message.totalAmountInvoiced = longToNumber(reader.int64() as Long);
          break;
        case 27:
          message.invoiceCurrency = reader.string();
          break;
        case 28:
          message.status = reader.string();
          break;
        case 29:
          message.relatedExportToken = reader.string();
          break;
        case 30:
          message.tokenWalletId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateImportToken {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      consignor: isSet(object.consignor) ? Company5.fromJSON(object.consignor) : undefined,
      exporter: isSet(object.exporter) ? Company5.fromJSON(object.exporter) : undefined,
      consignee: isSet(object.consignee) ? Company5.fromJSON(object.consignee) : undefined,
      declarant: isSet(object.declarant) ? Company5.fromJSON(object.declarant) : undefined,
      customOfficeOfExit: isSet(object.customOfficeOfExit)
        ? CustomsOffice6.fromJSON(object.customOfficeOfExit)
        : undefined,
      customOfficeOfEntry: isSet(object.customOfficeOfEntry)
        ? CustomsOffice6.fromJSON(object.customOfficeOfEntry)
        : undefined,
      customOfficeOfImport: isSet(object.customOfficeOfImport)
        ? CustomsOffice6.fromJSON(object.customOfficeOfImport)
        : undefined,
      goodsItems: Array.isArray(object?.goodsItems) ? object.goodsItems.map((e: any) => GoodsItem7.fromJSON(e)) : [],
      transportAtBorder: isSet(object.transportAtBorder) ? Transport8.fromJSON(object.transportAtBorder) : undefined,
      transportFromBorder: isSet(object.transportFromBorder)
        ? Transport8.fromJSON(object.transportFromBorder)
        : undefined,
      importId: isSet(object.importId) ? String(object.importId) : "",
      uniqueConsignmentReference: isSet(object.uniqueConsignmentReference)
        ? String(object.uniqueConsignmentReference)
        : "",
      localReferenceNumber: isSet(object.localReferenceNumber) ? String(object.localReferenceNumber) : "",
      destinationCountry: isSet(object.destinationCountry) ? String(object.destinationCountry) : "",
      exportCountry: isSet(object.exportCountry) ? String(object.exportCountry) : "",
      itinerary: isSet(object.itinerary) ? String(object.itinerary) : "",
      incotermCode: isSet(object.incotermCode) ? String(object.incotermCode) : "",
      incotermLocation: isSet(object.incotermLocation) ? String(object.incotermLocation) : "",
      totalGrossMass: isSet(object.totalGrossMass) ? String(object.totalGrossMass) : "",
      goodsItemQuantity: isSet(object.goodsItemQuantity) ? Number(object.goodsItemQuantity) : 0,
      totalPackagesQuantity: isSet(object.totalPackagesQuantity) ? Number(object.totalPackagesQuantity) : 0,
      releaseDateAndTime: isSet(object.releaseDateAndTime) ? String(object.releaseDateAndTime) : "",
      natureOfTransaction: isSet(object.natureOfTransaction) ? Number(object.natureOfTransaction) : 0,
      totalAmountInvoiced: isSet(object.totalAmountInvoiced) ? Number(object.totalAmountInvoiced) : 0,
      invoiceCurrency: isSet(object.invoiceCurrency) ? String(object.invoiceCurrency) : "",
      status: isSet(object.status) ? String(object.status) : "",
      relatedExportToken: isSet(object.relatedExportToken) ? String(object.relatedExportToken) : "",
      tokenWalletId: isSet(object.tokenWalletId) ? String(object.tokenWalletId) : "",
    };
  },

  toJSON(message: MsgCreateImportToken): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.consignor !== undefined
      && (obj.consignor = message.consignor ? Company5.toJSON(message.consignor) : undefined);
    message.exporter !== undefined && (obj.exporter = message.exporter ? Company5.toJSON(message.exporter) : undefined);
    message.consignee !== undefined
      && (obj.consignee = message.consignee ? Company5.toJSON(message.consignee) : undefined);
    message.declarant !== undefined
      && (obj.declarant = message.declarant ? Company5.toJSON(message.declarant) : undefined);
    message.customOfficeOfExit !== undefined && (obj.customOfficeOfExit = message.customOfficeOfExit
      ? CustomsOffice6.toJSON(message.customOfficeOfExit)
      : undefined);
    message.customOfficeOfEntry !== undefined && (obj.customOfficeOfEntry = message.customOfficeOfEntry
      ? CustomsOffice6.toJSON(message.customOfficeOfEntry)
      : undefined);
    message.customOfficeOfImport !== undefined && (obj.customOfficeOfImport = message.customOfficeOfImport
      ? CustomsOffice6.toJSON(message.customOfficeOfImport)
      : undefined);
    if (message.goodsItems) {
      obj.goodsItems = message.goodsItems.map((e) => e ? GoodsItem7.toJSON(e) : undefined);
    } else {
      obj.goodsItems = [];
    }
    message.transportAtBorder !== undefined
      && (obj.transportAtBorder = message.transportAtBorder ? Transport8.toJSON(message.transportAtBorder) : undefined);
    message.transportFromBorder !== undefined && (obj.transportFromBorder = message.transportFromBorder
      ? Transport8.toJSON(message.transportFromBorder)
      : undefined);
    message.importId !== undefined && (obj.importId = message.importId);
    message.uniqueConsignmentReference !== undefined
      && (obj.uniqueConsignmentReference = message.uniqueConsignmentReference);
    message.localReferenceNumber !== undefined && (obj.localReferenceNumber = message.localReferenceNumber);
    message.destinationCountry !== undefined && (obj.destinationCountry = message.destinationCountry);
    message.exportCountry !== undefined && (obj.exportCountry = message.exportCountry);
    message.itinerary !== undefined && (obj.itinerary = message.itinerary);
    message.incotermCode !== undefined && (obj.incotermCode = message.incotermCode);
    message.incotermLocation !== undefined && (obj.incotermLocation = message.incotermLocation);
    message.totalGrossMass !== undefined && (obj.totalGrossMass = message.totalGrossMass);
    message.goodsItemQuantity !== undefined && (obj.goodsItemQuantity = Math.round(message.goodsItemQuantity));
    message.totalPackagesQuantity !== undefined
      && (obj.totalPackagesQuantity = Math.round(message.totalPackagesQuantity));
    message.releaseDateAndTime !== undefined && (obj.releaseDateAndTime = message.releaseDateAndTime);
    message.natureOfTransaction !== undefined && (obj.natureOfTransaction = Math.round(message.natureOfTransaction));
    message.totalAmountInvoiced !== undefined && (obj.totalAmountInvoiced = Math.round(message.totalAmountInvoiced));
    message.invoiceCurrency !== undefined && (obj.invoiceCurrency = message.invoiceCurrency);
    message.status !== undefined && (obj.status = message.status);
    message.relatedExportToken !== undefined && (obj.relatedExportToken = message.relatedExportToken);
    message.tokenWalletId !== undefined && (obj.tokenWalletId = message.tokenWalletId);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateImportToken>, I>>(object: I): MsgCreateImportToken {
    const message = createBaseMsgCreateImportToken();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.consignor = (object.consignor !== undefined && object.consignor !== null)
      ? Company5.fromPartial(object.consignor)
      : undefined;
    message.exporter = (object.exporter !== undefined && object.exporter !== null)
      ? Company5.fromPartial(object.exporter)
      : undefined;
    message.consignee = (object.consignee !== undefined && object.consignee !== null)
      ? Company5.fromPartial(object.consignee)
      : undefined;
    message.declarant = (object.declarant !== undefined && object.declarant !== null)
      ? Company5.fromPartial(object.declarant)
      : undefined;
    message.customOfficeOfExit = (object.customOfficeOfExit !== undefined && object.customOfficeOfExit !== null)
      ? CustomsOffice6.fromPartial(object.customOfficeOfExit)
      : undefined;
    message.customOfficeOfEntry = (object.customOfficeOfEntry !== undefined && object.customOfficeOfEntry !== null)
      ? CustomsOffice6.fromPartial(object.customOfficeOfEntry)
      : undefined;
    message.customOfficeOfImport = (object.customOfficeOfImport !== undefined && object.customOfficeOfImport !== null)
      ? CustomsOffice6.fromPartial(object.customOfficeOfImport)
      : undefined;
    message.goodsItems = object.goodsItems?.map((e) => GoodsItem7.fromPartial(e)) || [];
    message.transportAtBorder = (object.transportAtBorder !== undefined && object.transportAtBorder !== null)
      ? Transport8.fromPartial(object.transportAtBorder)
      : undefined;
    message.transportFromBorder = (object.transportFromBorder !== undefined && object.transportFromBorder !== null)
      ? Transport8.fromPartial(object.transportFromBorder)
      : undefined;
    message.importId = object.importId ?? "";
    message.uniqueConsignmentReference = object.uniqueConsignmentReference ?? "";
    message.localReferenceNumber = object.localReferenceNumber ?? "";
    message.destinationCountry = object.destinationCountry ?? "";
    message.exportCountry = object.exportCountry ?? "";
    message.itinerary = object.itinerary ?? "";
    message.incotermCode = object.incotermCode ?? "";
    message.incotermLocation = object.incotermLocation ?? "";
    message.totalGrossMass = object.totalGrossMass ?? "";
    message.goodsItemQuantity = object.goodsItemQuantity ?? 0;
    message.totalPackagesQuantity = object.totalPackagesQuantity ?? 0;
    message.releaseDateAndTime = object.releaseDateAndTime ?? "";
    message.natureOfTransaction = object.natureOfTransaction ?? 0;
    message.totalAmountInvoiced = object.totalAmountInvoiced ?? 0;
    message.invoiceCurrency = object.invoiceCurrency ?? "";
    message.status = object.status ?? "";
    message.relatedExportToken = object.relatedExportToken ?? "";
    message.tokenWalletId = object.tokenWalletId ?? "";
    return message;
  },
};

function createBaseMsgCreateImportTokenResponse(): MsgCreateImportTokenResponse {
  return { importToken: undefined, events: [] };
}

export const MsgCreateImportTokenResponse = {
  encode(message: MsgCreateImportTokenResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.importToken !== undefined) {
      ImportToken.encode(message.importToken, writer.uint32(10).fork()).ldelim();
    }
    for (const v of message.events) {
      ImportEvent.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateImportTokenResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateImportTokenResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.importToken = ImportToken.decode(reader, reader.uint32());
          break;
        case 2:
          message.events.push(ImportEvent.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateImportTokenResponse {
    return {
      importToken: isSet(object.importToken) ? ImportToken.fromJSON(object.importToken) : undefined,
      events: Array.isArray(object?.events) ? object.events.map((e: any) => ImportEvent.fromJSON(e)) : [],
    };
  },

  toJSON(message: MsgCreateImportTokenResponse): unknown {
    const obj: any = {};
    message.importToken !== undefined
      && (obj.importToken = message.importToken ? ImportToken.toJSON(message.importToken) : undefined);
    if (message.events) {
      obj.events = message.events.map((e) => e ? ImportEvent.toJSON(e) : undefined);
    } else {
      obj.events = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateImportTokenResponse>, I>>(object: I): MsgCreateImportTokenResponse {
    const message = createBaseMsgCreateImportTokenResponse();
    message.importToken = (object.importToken !== undefined && object.importToken !== null)
      ? ImportToken.fromPartial(object.importToken)
      : undefined;
    message.events = object.events?.map((e) => ImportEvent.fromPartial(e)) || [];
    return message;
  },
};

function createBaseMsgUpdateImportToken(): MsgUpdateImportToken {
  return {
    creator: "",
    id: "",
    consignor: undefined,
    exporter: undefined,
    consignee: undefined,
    declarant: undefined,
    customOfficeOfExit: undefined,
    customOfficeOfEntry: undefined,
    customOfficeOfImport: undefined,
    goodsItems: [],
    transportAtBorder: undefined,
    transportFromBorder: undefined,
    importId: "",
    uniqueConsignmentReference: "",
    localReferenceNumber: "",
    destinationCountry: "",
    exportCountry: "",
    itinerary: "",
    incotermCode: "",
    incotermLocation: "",
    totalGrossMass: "",
    goodsItemQuantity: 0,
    totalPackagesQuantity: 0,
    releaseDateAndTime: "",
    natureOfTransaction: 0,
    totalAmountInvoiced: 0,
    invoiceCurrency: "",
    status: "",
    relatedExportToken: "",
    eventType: "",
  };
}

export const MsgUpdateImportToken = {
  encode(message: MsgUpdateImportToken, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.consignor !== undefined) {
      Company5.encode(message.consignor, writer.uint32(26).fork()).ldelim();
    }
    if (message.exporter !== undefined) {
      Company5.encode(message.exporter, writer.uint32(34).fork()).ldelim();
    }
    if (message.consignee !== undefined) {
      Company5.encode(message.consignee, writer.uint32(42).fork()).ldelim();
    }
    if (message.declarant !== undefined) {
      Company5.encode(message.declarant, writer.uint32(50).fork()).ldelim();
    }
    if (message.customOfficeOfExit !== undefined) {
      CustomsOffice6.encode(message.customOfficeOfExit, writer.uint32(58).fork()).ldelim();
    }
    if (message.customOfficeOfEntry !== undefined) {
      CustomsOffice6.encode(message.customOfficeOfEntry, writer.uint32(66).fork()).ldelim();
    }
    if (message.customOfficeOfImport !== undefined) {
      CustomsOffice6.encode(message.customOfficeOfImport, writer.uint32(74).fork()).ldelim();
    }
    for (const v of message.goodsItems) {
      GoodsItem7.encode(v!, writer.uint32(82).fork()).ldelim();
    }
    if (message.transportAtBorder !== undefined) {
      Transport8.encode(message.transportAtBorder, writer.uint32(90).fork()).ldelim();
    }
    if (message.transportFromBorder !== undefined) {
      Transport8.encode(message.transportFromBorder, writer.uint32(98).fork()).ldelim();
    }
    if (message.importId !== "") {
      writer.uint32(106).string(message.importId);
    }
    if (message.uniqueConsignmentReference !== "") {
      writer.uint32(114).string(message.uniqueConsignmentReference);
    }
    if (message.localReferenceNumber !== "") {
      writer.uint32(122).string(message.localReferenceNumber);
    }
    if (message.destinationCountry !== "") {
      writer.uint32(130).string(message.destinationCountry);
    }
    if (message.exportCountry !== "") {
      writer.uint32(138).string(message.exportCountry);
    }
    if (message.itinerary !== "") {
      writer.uint32(146).string(message.itinerary);
    }
    if (message.incotermCode !== "") {
      writer.uint32(154).string(message.incotermCode);
    }
    if (message.incotermLocation !== "") {
      writer.uint32(162).string(message.incotermLocation);
    }
    if (message.totalGrossMass !== "") {
      writer.uint32(170).string(message.totalGrossMass);
    }
    if (message.goodsItemQuantity !== 0) {
      writer.uint32(176).int64(message.goodsItemQuantity);
    }
    if (message.totalPackagesQuantity !== 0) {
      writer.uint32(184).int64(message.totalPackagesQuantity);
    }
    if (message.releaseDateAndTime !== "") {
      writer.uint32(194).string(message.releaseDateAndTime);
    }
    if (message.natureOfTransaction !== 0) {
      writer.uint32(200).int64(message.natureOfTransaction);
    }
    if (message.totalAmountInvoiced !== 0) {
      writer.uint32(208).int64(message.totalAmountInvoiced);
    }
    if (message.invoiceCurrency !== "") {
      writer.uint32(218).string(message.invoiceCurrency);
    }
    if (message.status !== "") {
      writer.uint32(226).string(message.status);
    }
    if (message.relatedExportToken !== "") {
      writer.uint32(234).string(message.relatedExportToken);
    }
    if (message.eventType !== "") {
      writer.uint32(242).string(message.eventType);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgUpdateImportToken {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgUpdateImportToken();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.consignor = Company5.decode(reader, reader.uint32());
          break;
        case 4:
          message.exporter = Company5.decode(reader, reader.uint32());
          break;
        case 5:
          message.consignee = Company5.decode(reader, reader.uint32());
          break;
        case 6:
          message.declarant = Company5.decode(reader, reader.uint32());
          break;
        case 7:
          message.customOfficeOfExit = CustomsOffice6.decode(reader, reader.uint32());
          break;
        case 8:
          message.customOfficeOfEntry = CustomsOffice6.decode(reader, reader.uint32());
          break;
        case 9:
          message.customOfficeOfImport = CustomsOffice6.decode(reader, reader.uint32());
          break;
        case 10:
          message.goodsItems.push(GoodsItem7.decode(reader, reader.uint32()));
          break;
        case 11:
          message.transportAtBorder = Transport8.decode(reader, reader.uint32());
          break;
        case 12:
          message.transportFromBorder = Transport8.decode(reader, reader.uint32());
          break;
        case 13:
          message.importId = reader.string();
          break;
        case 14:
          message.uniqueConsignmentReference = reader.string();
          break;
        case 15:
          message.localReferenceNumber = reader.string();
          break;
        case 16:
          message.destinationCountry = reader.string();
          break;
        case 17:
          message.exportCountry = reader.string();
          break;
        case 18:
          message.itinerary = reader.string();
          break;
        case 19:
          message.incotermCode = reader.string();
          break;
        case 20:
          message.incotermLocation = reader.string();
          break;
        case 21:
          message.totalGrossMass = reader.string();
          break;
        case 22:
          message.goodsItemQuantity = longToNumber(reader.int64() as Long);
          break;
        case 23:
          message.totalPackagesQuantity = longToNumber(reader.int64() as Long);
          break;
        case 24:
          message.releaseDateAndTime = reader.string();
          break;
        case 25:
          message.natureOfTransaction = longToNumber(reader.int64() as Long);
          break;
        case 26:
          message.totalAmountInvoiced = longToNumber(reader.int64() as Long);
          break;
        case 27:
          message.invoiceCurrency = reader.string();
          break;
        case 28:
          message.status = reader.string();
          break;
        case 29:
          message.relatedExportToken = reader.string();
          break;
        case 30:
          message.eventType = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateImportToken {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      consignor: isSet(object.consignor) ? Company5.fromJSON(object.consignor) : undefined,
      exporter: isSet(object.exporter) ? Company5.fromJSON(object.exporter) : undefined,
      consignee: isSet(object.consignee) ? Company5.fromJSON(object.consignee) : undefined,
      declarant: isSet(object.declarant) ? Company5.fromJSON(object.declarant) : undefined,
      customOfficeOfExit: isSet(object.customOfficeOfExit)
        ? CustomsOffice6.fromJSON(object.customOfficeOfExit)
        : undefined,
      customOfficeOfEntry: isSet(object.customOfficeOfEntry)
        ? CustomsOffice6.fromJSON(object.customOfficeOfEntry)
        : undefined,
      customOfficeOfImport: isSet(object.customOfficeOfImport)
        ? CustomsOffice6.fromJSON(object.customOfficeOfImport)
        : undefined,
      goodsItems: Array.isArray(object?.goodsItems) ? object.goodsItems.map((e: any) => GoodsItem7.fromJSON(e)) : [],
      transportAtBorder: isSet(object.transportAtBorder) ? Transport8.fromJSON(object.transportAtBorder) : undefined,
      transportFromBorder: isSet(object.transportFromBorder)
        ? Transport8.fromJSON(object.transportFromBorder)
        : undefined,
      importId: isSet(object.importId) ? String(object.importId) : "",
      uniqueConsignmentReference: isSet(object.uniqueConsignmentReference)
        ? String(object.uniqueConsignmentReference)
        : "",
      localReferenceNumber: isSet(object.localReferenceNumber) ? String(object.localReferenceNumber) : "",
      destinationCountry: isSet(object.destinationCountry) ? String(object.destinationCountry) : "",
      exportCountry: isSet(object.exportCountry) ? String(object.exportCountry) : "",
      itinerary: isSet(object.itinerary) ? String(object.itinerary) : "",
      incotermCode: isSet(object.incotermCode) ? String(object.incotermCode) : "",
      incotermLocation: isSet(object.incotermLocation) ? String(object.incotermLocation) : "",
      totalGrossMass: isSet(object.totalGrossMass) ? String(object.totalGrossMass) : "",
      goodsItemQuantity: isSet(object.goodsItemQuantity) ? Number(object.goodsItemQuantity) : 0,
      totalPackagesQuantity: isSet(object.totalPackagesQuantity) ? Number(object.totalPackagesQuantity) : 0,
      releaseDateAndTime: isSet(object.releaseDateAndTime) ? String(object.releaseDateAndTime) : "",
      natureOfTransaction: isSet(object.natureOfTransaction) ? Number(object.natureOfTransaction) : 0,
      totalAmountInvoiced: isSet(object.totalAmountInvoiced) ? Number(object.totalAmountInvoiced) : 0,
      invoiceCurrency: isSet(object.invoiceCurrency) ? String(object.invoiceCurrency) : "",
      status: isSet(object.status) ? String(object.status) : "",
      relatedExportToken: isSet(object.relatedExportToken) ? String(object.relatedExportToken) : "",
      eventType: isSet(object.eventType) ? String(object.eventType) : "",
    };
  },

  toJSON(message: MsgUpdateImportToken): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.consignor !== undefined
      && (obj.consignor = message.consignor ? Company5.toJSON(message.consignor) : undefined);
    message.exporter !== undefined && (obj.exporter = message.exporter ? Company5.toJSON(message.exporter) : undefined);
    message.consignee !== undefined
      && (obj.consignee = message.consignee ? Company5.toJSON(message.consignee) : undefined);
    message.declarant !== undefined
      && (obj.declarant = message.declarant ? Company5.toJSON(message.declarant) : undefined);
    message.customOfficeOfExit !== undefined && (obj.customOfficeOfExit = message.customOfficeOfExit
      ? CustomsOffice6.toJSON(message.customOfficeOfExit)
      : undefined);
    message.customOfficeOfEntry !== undefined && (obj.customOfficeOfEntry = message.customOfficeOfEntry
      ? CustomsOffice6.toJSON(message.customOfficeOfEntry)
      : undefined);
    message.customOfficeOfImport !== undefined && (obj.customOfficeOfImport = message.customOfficeOfImport
      ? CustomsOffice6.toJSON(message.customOfficeOfImport)
      : undefined);
    if (message.goodsItems) {
      obj.goodsItems = message.goodsItems.map((e) => e ? GoodsItem7.toJSON(e) : undefined);
    } else {
      obj.goodsItems = [];
    }
    message.transportAtBorder !== undefined
      && (obj.transportAtBorder = message.transportAtBorder ? Transport8.toJSON(message.transportAtBorder) : undefined);
    message.transportFromBorder !== undefined && (obj.transportFromBorder = message.transportFromBorder
      ? Transport8.toJSON(message.transportFromBorder)
      : undefined);
    message.importId !== undefined && (obj.importId = message.importId);
    message.uniqueConsignmentReference !== undefined
      && (obj.uniqueConsignmentReference = message.uniqueConsignmentReference);
    message.localReferenceNumber !== undefined && (obj.localReferenceNumber = message.localReferenceNumber);
    message.destinationCountry !== undefined && (obj.destinationCountry = message.destinationCountry);
    message.exportCountry !== undefined && (obj.exportCountry = message.exportCountry);
    message.itinerary !== undefined && (obj.itinerary = message.itinerary);
    message.incotermCode !== undefined && (obj.incotermCode = message.incotermCode);
    message.incotermLocation !== undefined && (obj.incotermLocation = message.incotermLocation);
    message.totalGrossMass !== undefined && (obj.totalGrossMass = message.totalGrossMass);
    message.goodsItemQuantity !== undefined && (obj.goodsItemQuantity = Math.round(message.goodsItemQuantity));
    message.totalPackagesQuantity !== undefined
      && (obj.totalPackagesQuantity = Math.round(message.totalPackagesQuantity));
    message.releaseDateAndTime !== undefined && (obj.releaseDateAndTime = message.releaseDateAndTime);
    message.natureOfTransaction !== undefined && (obj.natureOfTransaction = Math.round(message.natureOfTransaction));
    message.totalAmountInvoiced !== undefined && (obj.totalAmountInvoiced = Math.round(message.totalAmountInvoiced));
    message.invoiceCurrency !== undefined && (obj.invoiceCurrency = message.invoiceCurrency);
    message.status !== undefined && (obj.status = message.status);
    message.relatedExportToken !== undefined && (obj.relatedExportToken = message.relatedExportToken);
    message.eventType !== undefined && (obj.eventType = message.eventType);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgUpdateImportToken>, I>>(object: I): MsgUpdateImportToken {
    const message = createBaseMsgUpdateImportToken();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.consignor = (object.consignor !== undefined && object.consignor !== null)
      ? Company5.fromPartial(object.consignor)
      : undefined;
    message.exporter = (object.exporter !== undefined && object.exporter !== null)
      ? Company5.fromPartial(object.exporter)
      : undefined;
    message.consignee = (object.consignee !== undefined && object.consignee !== null)
      ? Company5.fromPartial(object.consignee)
      : undefined;
    message.declarant = (object.declarant !== undefined && object.declarant !== null)
      ? Company5.fromPartial(object.declarant)
      : undefined;
    message.customOfficeOfExit = (object.customOfficeOfExit !== undefined && object.customOfficeOfExit !== null)
      ? CustomsOffice6.fromPartial(object.customOfficeOfExit)
      : undefined;
    message.customOfficeOfEntry = (object.customOfficeOfEntry !== undefined && object.customOfficeOfEntry !== null)
      ? CustomsOffice6.fromPartial(object.customOfficeOfEntry)
      : undefined;
    message.customOfficeOfImport = (object.customOfficeOfImport !== undefined && object.customOfficeOfImport !== null)
      ? CustomsOffice6.fromPartial(object.customOfficeOfImport)
      : undefined;
    message.goodsItems = object.goodsItems?.map((e) => GoodsItem7.fromPartial(e)) || [];
    message.transportAtBorder = (object.transportAtBorder !== undefined && object.transportAtBorder !== null)
      ? Transport8.fromPartial(object.transportAtBorder)
      : undefined;
    message.transportFromBorder = (object.transportFromBorder !== undefined && object.transportFromBorder !== null)
      ? Transport8.fromPartial(object.transportFromBorder)
      : undefined;
    message.importId = object.importId ?? "";
    message.uniqueConsignmentReference = object.uniqueConsignmentReference ?? "";
    message.localReferenceNumber = object.localReferenceNumber ?? "";
    message.destinationCountry = object.destinationCountry ?? "";
    message.exportCountry = object.exportCountry ?? "";
    message.itinerary = object.itinerary ?? "";
    message.incotermCode = object.incotermCode ?? "";
    message.incotermLocation = object.incotermLocation ?? "";
    message.totalGrossMass = object.totalGrossMass ?? "";
    message.goodsItemQuantity = object.goodsItemQuantity ?? 0;
    message.totalPackagesQuantity = object.totalPackagesQuantity ?? 0;
    message.releaseDateAndTime = object.releaseDateAndTime ?? "";
    message.natureOfTransaction = object.natureOfTransaction ?? 0;
    message.totalAmountInvoiced = object.totalAmountInvoiced ?? 0;
    message.invoiceCurrency = object.invoiceCurrency ?? "";
    message.status = object.status ?? "";
    message.relatedExportToken = object.relatedExportToken ?? "";
    message.eventType = object.eventType ?? "";
    return message;
  },
};

function createBaseMsgUpdateImportTokenResponse(): MsgUpdateImportTokenResponse {
  return { importToken: undefined, events: [] };
}

export const MsgUpdateImportTokenResponse = {
  encode(message: MsgUpdateImportTokenResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.importToken !== undefined) {
      ImportToken.encode(message.importToken, writer.uint32(10).fork()).ldelim();
    }
    for (const v of message.events) {
      ImportEvent.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgUpdateImportTokenResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgUpdateImportTokenResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.importToken = ImportToken.decode(reader, reader.uint32());
          break;
        case 2:
          message.events.push(ImportEvent.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateImportTokenResponse {
    return {
      importToken: isSet(object.importToken) ? ImportToken.fromJSON(object.importToken) : undefined,
      events: Array.isArray(object?.events) ? object.events.map((e: any) => ImportEvent.fromJSON(e)) : [],
    };
  },

  toJSON(message: MsgUpdateImportTokenResponse): unknown {
    const obj: any = {};
    message.importToken !== undefined
      && (obj.importToken = message.importToken ? ImportToken.toJSON(message.importToken) : undefined);
    if (message.events) {
      obj.events = message.events.map((e) => e ? ImportEvent.toJSON(e) : undefined);
    } else {
      obj.events = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgUpdateImportTokenResponse>, I>>(object: I): MsgUpdateImportTokenResponse {
    const message = createBaseMsgUpdateImportTokenResponse();
    message.importToken = (object.importToken !== undefined && object.importToken !== null)
      ? ImportToken.fromPartial(object.importToken)
      : undefined;
    message.events = object.events?.map((e) => ImportEvent.fromPartial(e)) || [];
    return message;
  },
};

function createBaseMsgDeleteImportToken(): MsgDeleteImportToken {
  return { creator: "", id: "" };
}

export const MsgDeleteImportToken = {
  encode(message: MsgDeleteImportToken, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgDeleteImportToken {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgDeleteImportToken();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgDeleteImportToken {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
    };
  },

  toJSON(message: MsgDeleteImportToken): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgDeleteImportToken>, I>>(object: I): MsgDeleteImportToken {
    const message = createBaseMsgDeleteImportToken();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgDeleteImportTokenResponse(): MsgDeleteImportTokenResponse {
  return {};
}

export const MsgDeleteImportTokenResponse = {
  encode(_: MsgDeleteImportTokenResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgDeleteImportTokenResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgDeleteImportTokenResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgDeleteImportTokenResponse {
    return {};
  },

  toJSON(_: MsgDeleteImportTokenResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgDeleteImportTokenResponse>, I>>(_: I): MsgDeleteImportTokenResponse {
    const message = createBaseMsgDeleteImportTokenResponse();
    return message;
  },
};

function createBaseMsgFetchExportTokenAllExporter(): MsgFetchExportTokenAllExporter {
  return { creator: "", timestamp: "", tokenWalletId: "" };
}

export const MsgFetchExportTokenAllExporter = {
  encode(message: MsgFetchExportTokenAllExporter, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.timestamp !== "") {
      writer.uint32(18).string(message.timestamp);
    }
    if (message.tokenWalletId !== "") {
      writer.uint32(82).string(message.tokenWalletId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchExportTokenAllExporter {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchExportTokenAllExporter();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.timestamp = reader.string();
          break;
        case 10:
          message.tokenWalletId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchExportTokenAllExporter {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      timestamp: isSet(object.timestamp) ? String(object.timestamp) : "",
      tokenWalletId: isSet(object.tokenWalletId) ? String(object.tokenWalletId) : "",
    };
  },

  toJSON(message: MsgFetchExportTokenAllExporter): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.timestamp !== undefined && (obj.timestamp = message.timestamp);
    message.tokenWalletId !== undefined && (obj.tokenWalletId = message.tokenWalletId);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchExportTokenAllExporter>, I>>(
    object: I,
  ): MsgFetchExportTokenAllExporter {
    const message = createBaseMsgFetchExportTokenAllExporter();
    message.creator = object.creator ?? "";
    message.timestamp = object.timestamp ?? "";
    message.tokenWalletId = object.tokenWalletId ?? "";
    return message;
  },
};

function createBaseMsgFetchExportTokenAllDriver(): MsgFetchExportTokenAllDriver {
  return { creator: "", timestamp: "" };
}

export const MsgFetchExportTokenAllDriver = {
  encode(message: MsgFetchExportTokenAllDriver, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.timestamp !== "") {
      writer.uint32(18).string(message.timestamp);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchExportTokenAllDriver {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchExportTokenAllDriver();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.timestamp = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchExportTokenAllDriver {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      timestamp: isSet(object.timestamp) ? String(object.timestamp) : "",
    };
  },

  toJSON(message: MsgFetchExportTokenAllDriver): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.timestamp !== undefined && (obj.timestamp = message.timestamp);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchExportTokenAllDriver>, I>>(object: I): MsgFetchExportTokenAllDriver {
    const message = createBaseMsgFetchExportTokenAllDriver();
    message.creator = object.creator ?? "";
    message.timestamp = object.timestamp ?? "";
    return message;
  },
};

function createBaseMsgFetchExportTokenAllImporter(): MsgFetchExportTokenAllImporter {
  return { creator: "", timestamp: "", companyName: "", companyCountry: "", identificationNumber: "" };
}

export const MsgFetchExportTokenAllImporter = {
  encode(message: MsgFetchExportTokenAllImporter, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.timestamp !== "") {
      writer.uint32(18).string(message.timestamp);
    }
    if (message.companyName !== "") {
      writer.uint32(82).string(message.companyName);
    }
    if (message.companyCountry !== "") {
      writer.uint32(90).string(message.companyCountry);
    }
    if (message.identificationNumber !== "") {
      writer.uint32(98).string(message.identificationNumber);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchExportTokenAllImporter {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchExportTokenAllImporter();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.timestamp = reader.string();
          break;
        case 10:
          message.companyName = reader.string();
          break;
        case 11:
          message.companyCountry = reader.string();
          break;
        case 12:
          message.identificationNumber = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchExportTokenAllImporter {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      timestamp: isSet(object.timestamp) ? String(object.timestamp) : "",
      companyName: isSet(object.companyName) ? String(object.companyName) : "",
      companyCountry: isSet(object.companyCountry) ? String(object.companyCountry) : "",
      identificationNumber: isSet(object.identificationNumber) ? String(object.identificationNumber) : "",
    };
  },

  toJSON(message: MsgFetchExportTokenAllImporter): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.timestamp !== undefined && (obj.timestamp = message.timestamp);
    message.companyName !== undefined && (obj.companyName = message.companyName);
    message.companyCountry !== undefined && (obj.companyCountry = message.companyCountry);
    message.identificationNumber !== undefined && (obj.identificationNumber = message.identificationNumber);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchExportTokenAllImporter>, I>>(
    object: I,
  ): MsgFetchExportTokenAllImporter {
    const message = createBaseMsgFetchExportTokenAllImporter();
    message.creator = object.creator ?? "";
    message.timestamp = object.timestamp ?? "";
    message.companyName = object.companyName ?? "";
    message.companyCountry = object.companyCountry ?? "";
    message.identificationNumber = object.identificationNumber ?? "";
    return message;
  },
};

function createBaseMsgFetchExportTokenAllResponse(): MsgFetchExportTokenAllResponse {
  return { tokens: [] };
}

export const MsgFetchExportTokenAllResponse = {
  encode(message: MsgFetchExportTokenAllResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.tokens) {
      MsgFetchExportTokenResponse.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchExportTokenAllResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchExportTokenAllResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.tokens.push(MsgFetchExportTokenResponse.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchExportTokenAllResponse {
    return {
      tokens: Array.isArray(object?.tokens)
        ? object.tokens.map((e: any) => MsgFetchExportTokenResponse.fromJSON(e))
        : [],
    };
  },

  toJSON(message: MsgFetchExportTokenAllResponse): unknown {
    const obj: any = {};
    if (message.tokens) {
      obj.tokens = message.tokens.map((e) => e ? MsgFetchExportTokenResponse.toJSON(e) : undefined);
    } else {
      obj.tokens = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchExportTokenAllResponse>, I>>(
    object: I,
  ): MsgFetchExportTokenAllResponse {
    const message = createBaseMsgFetchExportTokenAllResponse();
    message.tokens = object.tokens?.map((e) => MsgFetchExportTokenResponse.fromPartial(e)) || [];
    return message;
  },
};

function createBaseMsgFetchExportTokenAllExporterResponse(): MsgFetchExportTokenAllExporterResponse {
  return { tokens: [] };
}

export const MsgFetchExportTokenAllExporterResponse = {
  encode(message: MsgFetchExportTokenAllExporterResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.tokens) {
      MsgFetchExportTokenExporterResponse.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchExportTokenAllExporterResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchExportTokenAllExporterResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.tokens.push(MsgFetchExportTokenExporterResponse.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchExportTokenAllExporterResponse {
    return {
      tokens: Array.isArray(object?.tokens)
        ? object.tokens.map((e: any) => MsgFetchExportTokenExporterResponse.fromJSON(e))
        : [],
    };
  },

  toJSON(message: MsgFetchExportTokenAllExporterResponse): unknown {
    const obj: any = {};
    if (message.tokens) {
      obj.tokens = message.tokens.map((e) => e ? MsgFetchExportTokenExporterResponse.toJSON(e) : undefined);
    } else {
      obj.tokens = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchExportTokenAllExporterResponse>, I>>(
    object: I,
  ): MsgFetchExportTokenAllExporterResponse {
    const message = createBaseMsgFetchExportTokenAllExporterResponse();
    message.tokens = object.tokens?.map((e) => MsgFetchExportTokenExporterResponse.fromPartial(e)) || [];
    return message;
  },
};

function createBaseMsgFetchExportTokenAllDriverResponse(): MsgFetchExportTokenAllDriverResponse {
  return { tokens: [] };
}

export const MsgFetchExportTokenAllDriverResponse = {
  encode(message: MsgFetchExportTokenAllDriverResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.tokens) {
      MsgFetchExportTokenDriverResponse.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchExportTokenAllDriverResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchExportTokenAllDriverResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.tokens.push(MsgFetchExportTokenDriverResponse.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchExportTokenAllDriverResponse {
    return {
      tokens: Array.isArray(object?.tokens)
        ? object.tokens.map((e: any) => MsgFetchExportTokenDriverResponse.fromJSON(e))
        : [],
    };
  },

  toJSON(message: MsgFetchExportTokenAllDriverResponse): unknown {
    const obj: any = {};
    if (message.tokens) {
      obj.tokens = message.tokens.map((e) => e ? MsgFetchExportTokenDriverResponse.toJSON(e) : undefined);
    } else {
      obj.tokens = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchExportTokenAllDriverResponse>, I>>(
    object: I,
  ): MsgFetchExportTokenAllDriverResponse {
    const message = createBaseMsgFetchExportTokenAllDriverResponse();
    message.tokens = object.tokens?.map((e) => MsgFetchExportTokenDriverResponse.fromPartial(e)) || [];
    return message;
  },
};

function createBaseMsgFetchExportTokenAllImporterResponse(): MsgFetchExportTokenAllImporterResponse {
  return { tokens: [] };
}

export const MsgFetchExportTokenAllImporterResponse = {
  encode(message: MsgFetchExportTokenAllImporterResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.tokens) {
      MsgFetchExportTokenImporterResponse.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchExportTokenAllImporterResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchExportTokenAllImporterResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.tokens.push(MsgFetchExportTokenImporterResponse.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchExportTokenAllImporterResponse {
    return {
      tokens: Array.isArray(object?.tokens)
        ? object.tokens.map((e: any) => MsgFetchExportTokenImporterResponse.fromJSON(e))
        : [],
    };
  },

  toJSON(message: MsgFetchExportTokenAllImporterResponse): unknown {
    const obj: any = {};
    if (message.tokens) {
      obj.tokens = message.tokens.map((e) => e ? MsgFetchExportTokenImporterResponse.toJSON(e) : undefined);
    } else {
      obj.tokens = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchExportTokenAllImporterResponse>, I>>(
    object: I,
  ): MsgFetchExportTokenAllImporterResponse {
    const message = createBaseMsgFetchExportTokenAllImporterResponse();
    message.tokens = object.tokens?.map((e) => MsgFetchExportTokenImporterResponse.fromPartial(e)) || [];
    return message;
  },
};

function createBaseMsgFetchExportTokenExporter(): MsgFetchExportTokenExporter {
  return { creator: "", id: "", timestamp: "", tokenWalletId: "" };
}

export const MsgFetchExportTokenExporter = {
  encode(message: MsgFetchExportTokenExporter, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.timestamp !== "") {
      writer.uint32(26).string(message.timestamp);
    }
    if (message.tokenWalletId !== "") {
      writer.uint32(82).string(message.tokenWalletId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchExportTokenExporter {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchExportTokenExporter();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.timestamp = reader.string();
          break;
        case 10:
          message.tokenWalletId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchExportTokenExporter {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      timestamp: isSet(object.timestamp) ? String(object.timestamp) : "",
      tokenWalletId: isSet(object.tokenWalletId) ? String(object.tokenWalletId) : "",
    };
  },

  toJSON(message: MsgFetchExportTokenExporter): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.timestamp !== undefined && (obj.timestamp = message.timestamp);
    message.tokenWalletId !== undefined && (obj.tokenWalletId = message.tokenWalletId);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchExportTokenExporter>, I>>(object: I): MsgFetchExportTokenExporter {
    const message = createBaseMsgFetchExportTokenExporter();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.timestamp = object.timestamp ?? "";
    message.tokenWalletId = object.tokenWalletId ?? "";
    return message;
  },
};

function createBaseMsgFetchExportTokenDriver(): MsgFetchExportTokenDriver {
  return { creator: "", id: "", timestamp: "" };
}

export const MsgFetchExportTokenDriver = {
  encode(message: MsgFetchExportTokenDriver, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.timestamp !== "") {
      writer.uint32(26).string(message.timestamp);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchExportTokenDriver {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchExportTokenDriver();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.timestamp = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchExportTokenDriver {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      timestamp: isSet(object.timestamp) ? String(object.timestamp) : "",
    };
  },

  toJSON(message: MsgFetchExportTokenDriver): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.timestamp !== undefined && (obj.timestamp = message.timestamp);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchExportTokenDriver>, I>>(object: I): MsgFetchExportTokenDriver {
    const message = createBaseMsgFetchExportTokenDriver();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.timestamp = object.timestamp ?? "";
    return message;
  },
};

function createBaseMsgFetchExportTokenImporter(): MsgFetchExportTokenImporter {
  return { creator: "", id: "", timestamp: "", companyName: "", companyCountry: "" };
}

export const MsgFetchExportTokenImporter = {
  encode(message: MsgFetchExportTokenImporter, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.timestamp !== "") {
      writer.uint32(26).string(message.timestamp);
    }
    if (message.companyName !== "") {
      writer.uint32(82).string(message.companyName);
    }
    if (message.companyCountry !== "") {
      writer.uint32(90).string(message.companyCountry);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchExportTokenImporter {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchExportTokenImporter();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.timestamp = reader.string();
          break;
        case 10:
          message.companyName = reader.string();
          break;
        case 11:
          message.companyCountry = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchExportTokenImporter {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      timestamp: isSet(object.timestamp) ? String(object.timestamp) : "",
      companyName: isSet(object.companyName) ? String(object.companyName) : "",
      companyCountry: isSet(object.companyCountry) ? String(object.companyCountry) : "",
    };
  },

  toJSON(message: MsgFetchExportTokenImporter): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.timestamp !== undefined && (obj.timestamp = message.timestamp);
    message.companyName !== undefined && (obj.companyName = message.companyName);
    message.companyCountry !== undefined && (obj.companyCountry = message.companyCountry);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchExportTokenImporter>, I>>(object: I): MsgFetchExportTokenImporter {
    const message = createBaseMsgFetchExportTokenImporter();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.timestamp = object.timestamp ?? "";
    message.companyName = object.companyName ?? "";
    message.companyCountry = object.companyCountry ?? "";
    return message;
  },
};

function createBaseMsgFetchExportTokenExporterByExportId(): MsgFetchExportTokenExporterByExportId {
  return { creator: "", exportId: "", timestamp: "", tokenWalletId: "" };
}

export const MsgFetchExportTokenExporterByExportId = {
  encode(message: MsgFetchExportTokenExporterByExportId, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.exportId !== "") {
      writer.uint32(18).string(message.exportId);
    }
    if (message.timestamp !== "") {
      writer.uint32(26).string(message.timestamp);
    }
    if (message.tokenWalletId !== "") {
      writer.uint32(82).string(message.tokenWalletId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchExportTokenExporterByExportId {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchExportTokenExporterByExportId();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.exportId = reader.string();
          break;
        case 3:
          message.timestamp = reader.string();
          break;
        case 10:
          message.tokenWalletId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchExportTokenExporterByExportId {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      exportId: isSet(object.exportId) ? String(object.exportId) : "",
      timestamp: isSet(object.timestamp) ? String(object.timestamp) : "",
      tokenWalletId: isSet(object.tokenWalletId) ? String(object.tokenWalletId) : "",
    };
  },

  toJSON(message: MsgFetchExportTokenExporterByExportId): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.exportId !== undefined && (obj.exportId = message.exportId);
    message.timestamp !== undefined && (obj.timestamp = message.timestamp);
    message.tokenWalletId !== undefined && (obj.tokenWalletId = message.tokenWalletId);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchExportTokenExporterByExportId>, I>>(
    object: I,
  ): MsgFetchExportTokenExporterByExportId {
    const message = createBaseMsgFetchExportTokenExporterByExportId();
    message.creator = object.creator ?? "";
    message.exportId = object.exportId ?? "";
    message.timestamp = object.timestamp ?? "";
    message.tokenWalletId = object.tokenWalletId ?? "";
    return message;
  },
};

function createBaseMsgFetchExportTokenDriverByExportId(): MsgFetchExportTokenDriverByExportId {
  return { creator: "", exportId: "", timestamp: "" };
}

export const MsgFetchExportTokenDriverByExportId = {
  encode(message: MsgFetchExportTokenDriverByExportId, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.exportId !== "") {
      writer.uint32(18).string(message.exportId);
    }
    if (message.timestamp !== "") {
      writer.uint32(26).string(message.timestamp);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchExportTokenDriverByExportId {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchExportTokenDriverByExportId();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.exportId = reader.string();
          break;
        case 3:
          message.timestamp = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchExportTokenDriverByExportId {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      exportId: isSet(object.exportId) ? String(object.exportId) : "",
      timestamp: isSet(object.timestamp) ? String(object.timestamp) : "",
    };
  },

  toJSON(message: MsgFetchExportTokenDriverByExportId): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.exportId !== undefined && (obj.exportId = message.exportId);
    message.timestamp !== undefined && (obj.timestamp = message.timestamp);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchExportTokenDriverByExportId>, I>>(
    object: I,
  ): MsgFetchExportTokenDriverByExportId {
    const message = createBaseMsgFetchExportTokenDriverByExportId();
    message.creator = object.creator ?? "";
    message.exportId = object.exportId ?? "";
    message.timestamp = object.timestamp ?? "";
    return message;
  },
};

function createBaseMsgFetchExportTokenImporterByExportId(): MsgFetchExportTokenImporterByExportId {
  return { creator: "", exportId: "", timestamp: "", companyName: "", companyCountry: "" };
}

export const MsgFetchExportTokenImporterByExportId = {
  encode(message: MsgFetchExportTokenImporterByExportId, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.exportId !== "") {
      writer.uint32(18).string(message.exportId);
    }
    if (message.timestamp !== "") {
      writer.uint32(26).string(message.timestamp);
    }
    if (message.companyName !== "") {
      writer.uint32(82).string(message.companyName);
    }
    if (message.companyCountry !== "") {
      writer.uint32(90).string(message.companyCountry);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchExportTokenImporterByExportId {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchExportTokenImporterByExportId();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.exportId = reader.string();
          break;
        case 3:
          message.timestamp = reader.string();
          break;
        case 10:
          message.companyName = reader.string();
          break;
        case 11:
          message.companyCountry = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchExportTokenImporterByExportId {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      exportId: isSet(object.exportId) ? String(object.exportId) : "",
      timestamp: isSet(object.timestamp) ? String(object.timestamp) : "",
      companyName: isSet(object.companyName) ? String(object.companyName) : "",
      companyCountry: isSet(object.companyCountry) ? String(object.companyCountry) : "",
    };
  },

  toJSON(message: MsgFetchExportTokenImporterByExportId): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.exportId !== undefined && (obj.exportId = message.exportId);
    message.timestamp !== undefined && (obj.timestamp = message.timestamp);
    message.companyName !== undefined && (obj.companyName = message.companyName);
    message.companyCountry !== undefined && (obj.companyCountry = message.companyCountry);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchExportTokenImporterByExportId>, I>>(
    object: I,
  ): MsgFetchExportTokenImporterByExportId {
    const message = createBaseMsgFetchExportTokenImporterByExportId();
    message.creator = object.creator ?? "";
    message.exportId = object.exportId ?? "";
    message.timestamp = object.timestamp ?? "";
    message.companyName = object.companyName ?? "";
    message.companyCountry = object.companyCountry ?? "";
    return message;
  },
};

function createBaseMsgFetchExportTokenResponse(): MsgFetchExportTokenResponse {
  return { exportToken: undefined, events: [] };
}

export const MsgFetchExportTokenResponse = {
  encode(message: MsgFetchExportTokenResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.exportToken !== undefined) {
      ExportToken.encode(message.exportToken, writer.uint32(10).fork()).ldelim();
    }
    for (const v of message.events) {
      ExportEvent.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchExportTokenResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchExportTokenResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.exportToken = ExportToken.decode(reader, reader.uint32());
          break;
        case 2:
          message.events.push(ExportEvent.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchExportTokenResponse {
    return {
      exportToken: isSet(object.exportToken) ? ExportToken.fromJSON(object.exportToken) : undefined,
      events: Array.isArray(object?.events) ? object.events.map((e: any) => ExportEvent.fromJSON(e)) : [],
    };
  },

  toJSON(message: MsgFetchExportTokenResponse): unknown {
    const obj: any = {};
    message.exportToken !== undefined
      && (obj.exportToken = message.exportToken ? ExportToken.toJSON(message.exportToken) : undefined);
    if (message.events) {
      obj.events = message.events.map((e) => e ? ExportEvent.toJSON(e) : undefined);
    } else {
      obj.events = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchExportTokenResponse>, I>>(object: I): MsgFetchExportTokenResponse {
    const message = createBaseMsgFetchExportTokenResponse();
    message.exportToken = (object.exportToken !== undefined && object.exportToken !== null)
      ? ExportToken.fromPartial(object.exportToken)
      : undefined;
    message.events = object.events?.map((e) => ExportEvent.fromPartial(e)) || [];
    return message;
  },
};

function createBaseMsgFetchExportTokenExporterResponse(): MsgFetchExportTokenExporterResponse {
  return { exportToken: undefined, events: [] };
}

export const MsgFetchExportTokenExporterResponse = {
  encode(message: MsgFetchExportTokenExporterResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.exportToken !== undefined) {
      ExportTokenExporter.encode(message.exportToken, writer.uint32(10).fork()).ldelim();
    }
    for (const v of message.events) {
      ExportEvent.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchExportTokenExporterResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchExportTokenExporterResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.exportToken = ExportTokenExporter.decode(reader, reader.uint32());
          break;
        case 2:
          message.events.push(ExportEvent.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchExportTokenExporterResponse {
    return {
      exportToken: isSet(object.exportToken) ? ExportTokenExporter.fromJSON(object.exportToken) : undefined,
      events: Array.isArray(object?.events) ? object.events.map((e: any) => ExportEvent.fromJSON(e)) : [],
    };
  },

  toJSON(message: MsgFetchExportTokenExporterResponse): unknown {
    const obj: any = {};
    message.exportToken !== undefined
      && (obj.exportToken = message.exportToken ? ExportTokenExporter.toJSON(message.exportToken) : undefined);
    if (message.events) {
      obj.events = message.events.map((e) => e ? ExportEvent.toJSON(e) : undefined);
    } else {
      obj.events = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchExportTokenExporterResponse>, I>>(
    object: I,
  ): MsgFetchExportTokenExporterResponse {
    const message = createBaseMsgFetchExportTokenExporterResponse();
    message.exportToken = (object.exportToken !== undefined && object.exportToken !== null)
      ? ExportTokenExporter.fromPartial(object.exportToken)
      : undefined;
    message.events = object.events?.map((e) => ExportEvent.fromPartial(e)) || [];
    return message;
  },
};

function createBaseMsgFetchExportTokenDriverResponse(): MsgFetchExportTokenDriverResponse {
  return { exportToken: undefined, events: [] };
}

export const MsgFetchExportTokenDriverResponse = {
  encode(message: MsgFetchExportTokenDriverResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.exportToken !== undefined) {
      ExportTokenDriver.encode(message.exportToken, writer.uint32(10).fork()).ldelim();
    }
    for (const v of message.events) {
      ExportEvent.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchExportTokenDriverResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchExportTokenDriverResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.exportToken = ExportTokenDriver.decode(reader, reader.uint32());
          break;
        case 2:
          message.events.push(ExportEvent.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchExportTokenDriverResponse {
    return {
      exportToken: isSet(object.exportToken) ? ExportTokenDriver.fromJSON(object.exportToken) : undefined,
      events: Array.isArray(object?.events) ? object.events.map((e: any) => ExportEvent.fromJSON(e)) : [],
    };
  },

  toJSON(message: MsgFetchExportTokenDriverResponse): unknown {
    const obj: any = {};
    message.exportToken !== undefined
      && (obj.exportToken = message.exportToken ? ExportTokenDriver.toJSON(message.exportToken) : undefined);
    if (message.events) {
      obj.events = message.events.map((e) => e ? ExportEvent.toJSON(e) : undefined);
    } else {
      obj.events = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchExportTokenDriverResponse>, I>>(
    object: I,
  ): MsgFetchExportTokenDriverResponse {
    const message = createBaseMsgFetchExportTokenDriverResponse();
    message.exportToken = (object.exportToken !== undefined && object.exportToken !== null)
      ? ExportTokenDriver.fromPartial(object.exportToken)
      : undefined;
    message.events = object.events?.map((e) => ExportEvent.fromPartial(e)) || [];
    return message;
  },
};

function createBaseMsgFetchExportTokenImporterResponse(): MsgFetchExportTokenImporterResponse {
  return { exportToken: undefined, events: [] };
}

export const MsgFetchExportTokenImporterResponse = {
  encode(message: MsgFetchExportTokenImporterResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.exportToken !== undefined) {
      ExportTokenImporter.encode(message.exportToken, writer.uint32(10).fork()).ldelim();
    }
    for (const v of message.events) {
      ExportEvent.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchExportTokenImporterResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchExportTokenImporterResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.exportToken = ExportTokenImporter.decode(reader, reader.uint32());
          break;
        case 2:
          message.events.push(ExportEvent.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchExportTokenImporterResponse {
    return {
      exportToken: isSet(object.exportToken) ? ExportTokenImporter.fromJSON(object.exportToken) : undefined,
      events: Array.isArray(object?.events) ? object.events.map((e: any) => ExportEvent.fromJSON(e)) : [],
    };
  },

  toJSON(message: MsgFetchExportTokenImporterResponse): unknown {
    const obj: any = {};
    message.exportToken !== undefined
      && (obj.exportToken = message.exportToken ? ExportTokenImporter.toJSON(message.exportToken) : undefined);
    if (message.events) {
      obj.events = message.events.map((e) => e ? ExportEvent.toJSON(e) : undefined);
    } else {
      obj.events = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchExportTokenImporterResponse>, I>>(
    object: I,
  ): MsgFetchExportTokenImporterResponse {
    const message = createBaseMsgFetchExportTokenImporterResponse();
    message.exportToken = (object.exportToken !== undefined && object.exportToken !== null)
      ? ExportTokenImporter.fromPartial(object.exportToken)
      : undefined;
    message.events = object.events?.map((e) => ExportEvent.fromPartial(e)) || [];
    return message;
  },
};

function createBaseMsgFetchImportTokenAll(): MsgFetchImportTokenAll {
  return { creator: "", timestamp: "", tokenWalletId: "" };
}

export const MsgFetchImportTokenAll = {
  encode(message: MsgFetchImportTokenAll, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.timestamp !== "") {
      writer.uint32(18).string(message.timestamp);
    }
    if (message.tokenWalletId !== "") {
      writer.uint32(82).string(message.tokenWalletId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchImportTokenAll {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchImportTokenAll();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.timestamp = reader.string();
          break;
        case 10:
          message.tokenWalletId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchImportTokenAll {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      timestamp: isSet(object.timestamp) ? String(object.timestamp) : "",
      tokenWalletId: isSet(object.tokenWalletId) ? String(object.tokenWalletId) : "",
    };
  },

  toJSON(message: MsgFetchImportTokenAll): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.timestamp !== undefined && (obj.timestamp = message.timestamp);
    message.tokenWalletId !== undefined && (obj.tokenWalletId = message.tokenWalletId);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchImportTokenAll>, I>>(object: I): MsgFetchImportTokenAll {
    const message = createBaseMsgFetchImportTokenAll();
    message.creator = object.creator ?? "";
    message.timestamp = object.timestamp ?? "";
    message.tokenWalletId = object.tokenWalletId ?? "";
    return message;
  },
};

function createBaseMsgFetchImportTokenAllResponse(): MsgFetchImportTokenAllResponse {
  return { tokens: [] };
}

export const MsgFetchImportTokenAllResponse = {
  encode(message: MsgFetchImportTokenAllResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.tokens) {
      MsgCreateImportTokenResponse.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchImportTokenAllResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchImportTokenAllResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.tokens.push(MsgCreateImportTokenResponse.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchImportTokenAllResponse {
    return {
      tokens: Array.isArray(object?.tokens)
        ? object.tokens.map((e: any) => MsgCreateImportTokenResponse.fromJSON(e))
        : [],
    };
  },

  toJSON(message: MsgFetchImportTokenAllResponse): unknown {
    const obj: any = {};
    if (message.tokens) {
      obj.tokens = message.tokens.map((e) => e ? MsgCreateImportTokenResponse.toJSON(e) : undefined);
    } else {
      obj.tokens = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchImportTokenAllResponse>, I>>(
    object: I,
  ): MsgFetchImportTokenAllResponse {
    const message = createBaseMsgFetchImportTokenAllResponse();
    message.tokens = object.tokens?.map((e) => MsgCreateImportTokenResponse.fromPartial(e)) || [];
    return message;
  },
};

function createBaseMsgFetchImportToken(): MsgFetchImportToken {
  return { creator: "", id: "", timestamp: "", tokenWalletId: "" };
}

export const MsgFetchImportToken = {
  encode(message: MsgFetchImportToken, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.timestamp !== "") {
      writer.uint32(26).string(message.timestamp);
    }
    if (message.tokenWalletId !== "") {
      writer.uint32(82).string(message.tokenWalletId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchImportToken {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchImportToken();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.timestamp = reader.string();
          break;
        case 10:
          message.tokenWalletId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchImportToken {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      timestamp: isSet(object.timestamp) ? String(object.timestamp) : "",
      tokenWalletId: isSet(object.tokenWalletId) ? String(object.tokenWalletId) : "",
    };
  },

  toJSON(message: MsgFetchImportToken): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.timestamp !== undefined && (obj.timestamp = message.timestamp);
    message.tokenWalletId !== undefined && (obj.tokenWalletId = message.tokenWalletId);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchImportToken>, I>>(object: I): MsgFetchImportToken {
    const message = createBaseMsgFetchImportToken();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.timestamp = object.timestamp ?? "";
    message.tokenWalletId = object.tokenWalletId ?? "";
    return message;
  },
};

function createBaseMsgFetchImportTokenByImportId(): MsgFetchImportTokenByImportId {
  return { creator: "", importId: "", timestamp: "", tokenWalletId: "" };
}

export const MsgFetchImportTokenByImportId = {
  encode(message: MsgFetchImportTokenByImportId, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.importId !== "") {
      writer.uint32(18).string(message.importId);
    }
    if (message.timestamp !== "") {
      writer.uint32(26).string(message.timestamp);
    }
    if (message.tokenWalletId !== "") {
      writer.uint32(82).string(message.tokenWalletId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchImportTokenByImportId {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchImportTokenByImportId();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.importId = reader.string();
          break;
        case 3:
          message.timestamp = reader.string();
          break;
        case 10:
          message.tokenWalletId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchImportTokenByImportId {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      importId: isSet(object.importId) ? String(object.importId) : "",
      timestamp: isSet(object.timestamp) ? String(object.timestamp) : "",
      tokenWalletId: isSet(object.tokenWalletId) ? String(object.tokenWalletId) : "",
    };
  },

  toJSON(message: MsgFetchImportTokenByImportId): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.importId !== undefined && (obj.importId = message.importId);
    message.timestamp !== undefined && (obj.timestamp = message.timestamp);
    message.tokenWalletId !== undefined && (obj.tokenWalletId = message.tokenWalletId);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchImportTokenByImportId>, I>>(
    object: I,
  ): MsgFetchImportTokenByImportId {
    const message = createBaseMsgFetchImportTokenByImportId();
    message.creator = object.creator ?? "";
    message.importId = object.importId ?? "";
    message.timestamp = object.timestamp ?? "";
    message.tokenWalletId = object.tokenWalletId ?? "";
    return message;
  },
};

function createBaseMsgFetchImportTokenResponse(): MsgFetchImportTokenResponse {
  return { importToken: undefined, events: [] };
}

export const MsgFetchImportTokenResponse = {
  encode(message: MsgFetchImportTokenResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.importToken !== undefined) {
      ImportToken.encode(message.importToken, writer.uint32(10).fork()).ldelim();
    }
    for (const v of message.events) {
      ImportEvent.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchImportTokenResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchImportTokenResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.importToken = ImportToken.decode(reader, reader.uint32());
          break;
        case 2:
          message.events.push(ImportEvent.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchImportTokenResponse {
    return {
      importToken: isSet(object.importToken) ? ImportToken.fromJSON(object.importToken) : undefined,
      events: Array.isArray(object?.events) ? object.events.map((e: any) => ImportEvent.fromJSON(e)) : [],
    };
  },

  toJSON(message: MsgFetchImportTokenResponse): unknown {
    const obj: any = {};
    message.importToken !== undefined
      && (obj.importToken = message.importToken ? ImportToken.toJSON(message.importToken) : undefined);
    if (message.events) {
      obj.events = message.events.map((e) => e ? ImportEvent.toJSON(e) : undefined);
    } else {
      obj.events = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchImportTokenResponse>, I>>(object: I): MsgFetchImportTokenResponse {
    const message = createBaseMsgFetchImportTokenResponse();
    message.importToken = (object.importToken !== undefined && object.importToken !== null)
      ? ImportToken.fromPartial(object.importToken)
      : undefined;
    message.events = object.events?.map((e) => ImportEvent.fromPartial(e)) || [];
    return message;
  },
};

function createBaseMsgAssignCosmosAddressToWallet(): MsgAssignCosmosAddressToWallet {
  return { creator: "", cosmosAddress: "", walletId: "" };
}

export const MsgAssignCosmosAddressToWallet = {
  encode(message: MsgAssignCosmosAddressToWallet, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.cosmosAddress !== "") {
      writer.uint32(18).string(message.cosmosAddress);
    }
    if (message.walletId !== "") {
      writer.uint32(26).string(message.walletId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgAssignCosmosAddressToWallet {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgAssignCosmosAddressToWallet();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.cosmosAddress = reader.string();
          break;
        case 3:
          message.walletId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgAssignCosmosAddressToWallet {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      cosmosAddress: isSet(object.cosmosAddress) ? String(object.cosmosAddress) : "",
      walletId: isSet(object.walletId) ? String(object.walletId) : "",
    };
  },

  toJSON(message: MsgAssignCosmosAddressToWallet): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.cosmosAddress !== undefined && (obj.cosmosAddress = message.cosmosAddress);
    message.walletId !== undefined && (obj.walletId = message.walletId);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgAssignCosmosAddressToWallet>, I>>(
    object: I,
  ): MsgAssignCosmosAddressToWallet {
    const message = createBaseMsgAssignCosmosAddressToWallet();
    message.creator = object.creator ?? "";
    message.cosmosAddress = object.cosmosAddress ?? "";
    message.walletId = object.walletId ?? "";
    return message;
  },
};

function createBaseMsgFetchTokenHistory(): MsgFetchTokenHistory {
  return { creator: "", id: "" };
}

export const MsgFetchTokenHistory = {
  encode(message: MsgFetchTokenHistory, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchTokenHistory {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchTokenHistory();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchTokenHistory {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
    };
  },

  toJSON(message: MsgFetchTokenHistory): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchTokenHistory>, I>>(object: I): MsgFetchTokenHistory {
    const message = createBaseMsgFetchTokenHistory();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgFetchTokenHistoryResponse(): MsgFetchTokenHistoryResponse {
  return { TokenHistory: undefined };
}

export const MsgFetchTokenHistoryResponse = {
  encode(message: MsgFetchTokenHistoryResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.TokenHistory !== undefined) {
      TokenHistory.encode(message.TokenHistory, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchTokenHistoryResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchTokenHistoryResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.TokenHistory = TokenHistory.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchTokenHistoryResponse {
    return { TokenHistory: isSet(object.TokenHistory) ? TokenHistory.fromJSON(object.TokenHistory) : undefined };
  },

  toJSON(message: MsgFetchTokenHistoryResponse): unknown {
    const obj: any = {};
    message.TokenHistory !== undefined
      && (obj.TokenHistory = message.TokenHistory ? TokenHistory.toJSON(message.TokenHistory) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchTokenHistoryResponse>, I>>(object: I): MsgFetchTokenHistoryResponse {
    const message = createBaseMsgFetchTokenHistoryResponse();
    message.TokenHistory = (object.TokenHistory !== undefined && object.TokenHistory !== null)
      ? TokenHistory.fromPartial(object.TokenHistory)
      : undefined;
    return message;
  },
};

/** Msg defines the Msg service. */
export interface Msg {
  /** Wallet RPC */
  CreateWallet(request: MsgCreateWallet): Promise<MsgIdResponse9>;
  CreateWalletWithId(request: MsgCreateWalletWithId): Promise<MsgIdResponse9>;
  UpdateWallet(request: MsgUpdateWallet): Promise<MsgEmptyResponse10>;
  FetchAllWallet(request: MsgFetchAllWallet): Promise<MsgFetchAllWalletResponse>;
  FetchGetWallet(request: MsgFetchGetWallet): Promise<MsgFetchGetWalletResponse>;
  FetchGetWalletHistory(request: MsgFetchGetWalletHistory): Promise<MsgFetchGetWalletHistoryResponse>;
  /** Segment RPC */
  CreateSegment(request: MsgCreateSegment): Promise<MsgIdResponse9>;
  CreateSegmentWithId(request: MsgCreateSegmentWithId): Promise<MsgIdResponse9>;
  UpdateSegment(request: MsgUpdateSegment): Promise<MsgEmptyResponse10>;
  FetchAllSegment(request: MsgFetchAllSegment): Promise<MsgFetchAllSegmentResponse>;
  FetchGetSegment(request: MsgFetchGetSegment): Promise<MsgFetchGetSegmentResponse>;
  FetchSegmentHistory(request: MsgFetchSegmentHistory): Promise<MsgFetchGetSegmentHistoryResponse>;
  /**
   * --- Export Token ---
   * Token RPC
   */
  CreateExportToken(request: MsgCreateExportToken): Promise<MsgCreateExportTokenResponse>;
  UpdateExportToken(request: MsgUpdateExportToken): Promise<MsgUpdateExportTokenResponse>;
  ForwardExportToken(request: MsgForwardExportToken): Promise<MsgForwardExportTokenResponse>;
  /**
   * Event RPC
   * TODO: Deprecated
   */
  CreateExportEvent(request: MsgCreateExportEvent): Promise<MsgCreateExportEventResponse>;
  /** Export */
  EventPresentQrCode(request: MsgEventPresentQrCode): Promise<MsgEventStatusResponseExport>;
  EventAcceptGoodsForTransport(request: MsgEventAcceptGoodsForTransport): Promise<MsgEventStatusResponseExport>;
  EventPresentGoods(request: MsgEventPresentGoods): Promise<MsgEventStatusResponseExport>;
  EventCertifyExitOfGoods(request: MsgEventCertifyExitOfGoods): Promise<MsgEventStatusResponseExport>;
  EventMoveToArchive(request: MsgEventMoveToArchive): Promise<MsgEventStatusResponseExport>;
  EventExportCheck(request: MsgEventExportCheck): Promise<MsgEventStatusResponseExport>;
  EventAcceptExport(request: MsgEventAcceptExport): Promise<MsgEventStatusResponseExport>;
  EventExportRefusal(request: MsgEventExportRefusal): Promise<MsgEventStatusResponseExport>;
  /** Import */
  EventAcceptImport(request: MsgEventAcceptImport): Promise<MsgEventStatusResponseImport>;
  EventCompleteImport(request: MsgEventCompleteImport): Promise<MsgEventStatusResponseImport>;
  EventImportProposal(request: MsgEventImportProposal): Promise<MsgEventStatusResponseImport>;
  EventImportRefusal(request: MsgEventImportRefusal): Promise<MsgEventStatusResponseImport>;
  EventImportSupplement(request: MsgEventImportSupplement): Promise<MsgEventStatusResponseImport>;
  /** SOON TO BE IMPLEMENTED */
  EventImportExport(request: MsgEventImportExport): Promise<MsgEventStatusResponseImport>;
  /** SOON TO BE IMPLEMENTED */
  EventImportSubmit(request: MsgEventImportSubmit): Promise<MsgEventStatusResponseImport>;
  /** SOON TO BE IMPLEMENTED */
  EventImportRelease(request: MsgEventImportRelease): Promise<MsgEventStatusResponseImport>;
  /** Fetch RPC */
  FetchExportTokenAllExporter(request: MsgFetchExportTokenAllExporter): Promise<MsgFetchExportTokenAllExporterResponse>;
  FetchExportTokenAllDriver(request: MsgFetchExportTokenAllDriver): Promise<MsgFetchExportTokenAllDriverResponse>;
  FetchExportTokenAllImporter(request: MsgFetchExportTokenAllImporter): Promise<MsgFetchExportTokenAllImporterResponse>;
  FetchExportTokenExporter(request: MsgFetchExportTokenExporter): Promise<MsgFetchExportTokenExporterResponse>;
  FetchExportTokenDriver(request: MsgFetchExportTokenDriver): Promise<MsgFetchExportTokenDriverResponse>;
  FetchExportTokenImporter(request: MsgFetchExportTokenImporter): Promise<MsgFetchExportTokenImporterResponse>;
  /** Deprecated */
  FetchExportTokenExporterByExportId(
    request: MsgFetchExportTokenExporterByExportId,
  ): Promise<MsgFetchExportTokenResponse>;
  FetchExportTokenDriverByExportId(request: MsgFetchExportTokenDriverByExportId): Promise<MsgFetchExportTokenResponse>;
  FetchExportTokenImporterByExportId(
    request: MsgFetchExportTokenImporterByExportId,
  ): Promise<MsgFetchExportTokenResponse>;
  FetchTokenHistory(request: MsgFetchTokenHistory): Promise<MsgFetchTokenHistoryResponse>;
  /**
   * --- Import Token ---
   * Token RPC
   */
  CreateImportToken(request: MsgCreateImportToken): Promise<MsgCreateImportTokenResponse>;
  CreateImportEvent(request: MsgCreateImportEvent): Promise<MsgCreateImportEventResponse>;
  UpdateImportToken(request: MsgUpdateImportToken): Promise<MsgUpdateImportTokenResponse>;
  /** Fetch RPC */
  FetchImportTokenAll(request: MsgFetchImportTokenAll): Promise<MsgFetchImportTokenAllResponse>;
  FetchImportToken(request: MsgFetchImportToken): Promise<MsgFetchImportTokenResponse>;
  FetchImportTokenByImportId(request: MsgFetchImportTokenByImportId): Promise<MsgFetchImportTokenResponse>;
  AssignCosmosAddressToWallet(request: MsgAssignCosmosAddressToWallet): Promise<MsgEmptyResponse>;
}

export class MsgClientImpl implements Msg {
  private readonly rpc: Rpc;
  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.CreateWallet = this.CreateWallet.bind(this);
    this.CreateWalletWithId = this.CreateWalletWithId.bind(this);
    this.UpdateWallet = this.UpdateWallet.bind(this);
    this.FetchAllWallet = this.FetchAllWallet.bind(this);
    this.FetchGetWallet = this.FetchGetWallet.bind(this);
    this.FetchGetWalletHistory = this.FetchGetWalletHistory.bind(this);
    this.CreateSegment = this.CreateSegment.bind(this);
    this.CreateSegmentWithId = this.CreateSegmentWithId.bind(this);
    this.UpdateSegment = this.UpdateSegment.bind(this);
    this.FetchAllSegment = this.FetchAllSegment.bind(this);
    this.FetchGetSegment = this.FetchGetSegment.bind(this);
    this.FetchSegmentHistory = this.FetchSegmentHistory.bind(this);
    this.CreateExportToken = this.CreateExportToken.bind(this);
    this.UpdateExportToken = this.UpdateExportToken.bind(this);
    this.ForwardExportToken = this.ForwardExportToken.bind(this);
    this.CreateExportEvent = this.CreateExportEvent.bind(this);
    this.EventPresentQrCode = this.EventPresentQrCode.bind(this);
    this.EventAcceptGoodsForTransport = this.EventAcceptGoodsForTransport.bind(this);
    this.EventPresentGoods = this.EventPresentGoods.bind(this);
    this.EventCertifyExitOfGoods = this.EventCertifyExitOfGoods.bind(this);
    this.EventMoveToArchive = this.EventMoveToArchive.bind(this);
    this.EventExportCheck = this.EventExportCheck.bind(this);
    this.EventAcceptExport = this.EventAcceptExport.bind(this);
    this.EventExportRefusal = this.EventExportRefusal.bind(this);
    this.EventAcceptImport = this.EventAcceptImport.bind(this);
    this.EventCompleteImport = this.EventCompleteImport.bind(this);
    this.EventImportProposal = this.EventImportProposal.bind(this);
    this.EventImportRefusal = this.EventImportRefusal.bind(this);
    this.EventImportSupplement = this.EventImportSupplement.bind(this);
    this.EventImportExport = this.EventImportExport.bind(this);
    this.EventImportSubmit = this.EventImportSubmit.bind(this);
    this.EventImportRelease = this.EventImportRelease.bind(this);
    this.FetchExportTokenAllExporter = this.FetchExportTokenAllExporter.bind(this);
    this.FetchExportTokenAllDriver = this.FetchExportTokenAllDriver.bind(this);
    this.FetchExportTokenAllImporter = this.FetchExportTokenAllImporter.bind(this);
    this.FetchExportTokenExporter = this.FetchExportTokenExporter.bind(this);
    this.FetchExportTokenDriver = this.FetchExportTokenDriver.bind(this);
    this.FetchExportTokenImporter = this.FetchExportTokenImporter.bind(this);
    this.FetchExportTokenExporterByExportId = this.FetchExportTokenExporterByExportId.bind(this);
    this.FetchExportTokenDriverByExportId = this.FetchExportTokenDriverByExportId.bind(this);
    this.FetchExportTokenImporterByExportId = this.FetchExportTokenImporterByExportId.bind(this);
    this.FetchTokenHistory = this.FetchTokenHistory.bind(this);
    this.CreateImportToken = this.CreateImportToken.bind(this);
    this.CreateImportEvent = this.CreateImportEvent.bind(this);
    this.UpdateImportToken = this.UpdateImportToken.bind(this);
    this.FetchImportTokenAll = this.FetchImportTokenAll.bind(this);
    this.FetchImportToken = this.FetchImportToken.bind(this);
    this.FetchImportTokenByImportId = this.FetchImportTokenByImportId.bind(this);
    this.AssignCosmosAddressToWallet = this.AssignCosmosAddressToWallet.bind(this);
  }
  CreateWallet(request: MsgCreateWallet): Promise<MsgIdResponse9> {
    const data = MsgCreateWallet.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "CreateWallet", data);
    return promise.then((data) => MsgIdResponse9.decode(new _m0.Reader(data)));
  }

  CreateWalletWithId(request: MsgCreateWalletWithId): Promise<MsgIdResponse9> {
    const data = MsgCreateWalletWithId.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "CreateWalletWithId", data);
    return promise.then((data) => MsgIdResponse9.decode(new _m0.Reader(data)));
  }

  UpdateWallet(request: MsgUpdateWallet): Promise<MsgEmptyResponse10> {
    const data = MsgUpdateWallet.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "UpdateWallet", data);
    return promise.then((data) => MsgEmptyResponse10.decode(new _m0.Reader(data)));
  }

  FetchAllWallet(request: MsgFetchAllWallet): Promise<MsgFetchAllWalletResponse> {
    const data = MsgFetchAllWallet.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "FetchAllWallet", data);
    return promise.then((data) => MsgFetchAllWalletResponse.decode(new _m0.Reader(data)));
  }

  FetchGetWallet(request: MsgFetchGetWallet): Promise<MsgFetchGetWalletResponse> {
    const data = MsgFetchGetWallet.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "FetchGetWallet", data);
    return promise.then((data) => MsgFetchGetWalletResponse.decode(new _m0.Reader(data)));
  }

  FetchGetWalletHistory(request: MsgFetchGetWalletHistory): Promise<MsgFetchGetWalletHistoryResponse> {
    const data = MsgFetchGetWalletHistory.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "FetchGetWalletHistory", data);
    return promise.then((data) => MsgFetchGetWalletHistoryResponse.decode(new _m0.Reader(data)));
  }

  CreateSegment(request: MsgCreateSegment): Promise<MsgIdResponse9> {
    const data = MsgCreateSegment.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "CreateSegment", data);
    return promise.then((data) => MsgIdResponse9.decode(new _m0.Reader(data)));
  }

  CreateSegmentWithId(request: MsgCreateSegmentWithId): Promise<MsgIdResponse9> {
    const data = MsgCreateSegmentWithId.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "CreateSegmentWithId", data);
    return promise.then((data) => MsgIdResponse9.decode(new _m0.Reader(data)));
  }

  UpdateSegment(request: MsgUpdateSegment): Promise<MsgEmptyResponse10> {
    const data = MsgUpdateSegment.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "UpdateSegment", data);
    return promise.then((data) => MsgEmptyResponse10.decode(new _m0.Reader(data)));
  }

  FetchAllSegment(request: MsgFetchAllSegment): Promise<MsgFetchAllSegmentResponse> {
    const data = MsgFetchAllSegment.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "FetchAllSegment", data);
    return promise.then((data) => MsgFetchAllSegmentResponse.decode(new _m0.Reader(data)));
  }

  FetchGetSegment(request: MsgFetchGetSegment): Promise<MsgFetchGetSegmentResponse> {
    const data = MsgFetchGetSegment.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "FetchGetSegment", data);
    return promise.then((data) => MsgFetchGetSegmentResponse.decode(new _m0.Reader(data)));
  }

  FetchSegmentHistory(request: MsgFetchSegmentHistory): Promise<MsgFetchGetSegmentHistoryResponse> {
    const data = MsgFetchSegmentHistory.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "FetchSegmentHistory", data);
    return promise.then((data) => MsgFetchGetSegmentHistoryResponse.decode(new _m0.Reader(data)));
  }

  CreateExportToken(request: MsgCreateExportToken): Promise<MsgCreateExportTokenResponse> {
    const data = MsgCreateExportToken.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "CreateExportToken", data);
    return promise.then((data) => MsgCreateExportTokenResponse.decode(new _m0.Reader(data)));
  }

  UpdateExportToken(request: MsgUpdateExportToken): Promise<MsgUpdateExportTokenResponse> {
    const data = MsgUpdateExportToken.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "UpdateExportToken", data);
    return promise.then((data) => MsgUpdateExportTokenResponse.decode(new _m0.Reader(data)));
  }

  ForwardExportToken(request: MsgForwardExportToken): Promise<MsgForwardExportTokenResponse> {
    const data = MsgForwardExportToken.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "ForwardExportToken", data);
    return promise.then((data) => MsgForwardExportTokenResponse.decode(new _m0.Reader(data)));
  }

  CreateExportEvent(request: MsgCreateExportEvent): Promise<MsgCreateExportEventResponse> {
    const data = MsgCreateExportEvent.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "CreateExportEvent", data);
    return promise.then((data) => MsgCreateExportEventResponse.decode(new _m0.Reader(data)));
  }

  EventPresentQrCode(request: MsgEventPresentQrCode): Promise<MsgEventStatusResponseExport> {
    const data = MsgEventPresentQrCode.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "EventPresentQrCode", data);
    return promise.then((data) => MsgEventStatusResponseExport.decode(new _m0.Reader(data)));
  }

  EventAcceptGoodsForTransport(request: MsgEventAcceptGoodsForTransport): Promise<MsgEventStatusResponseExport> {
    const data = MsgEventAcceptGoodsForTransport.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "EventAcceptGoodsForTransport", data);
    return promise.then((data) => MsgEventStatusResponseExport.decode(new _m0.Reader(data)));
  }

  EventPresentGoods(request: MsgEventPresentGoods): Promise<MsgEventStatusResponseExport> {
    const data = MsgEventPresentGoods.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "EventPresentGoods", data);
    return promise.then((data) => MsgEventStatusResponseExport.decode(new _m0.Reader(data)));
  }

  EventCertifyExitOfGoods(request: MsgEventCertifyExitOfGoods): Promise<MsgEventStatusResponseExport> {
    const data = MsgEventCertifyExitOfGoods.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "EventCertifyExitOfGoods", data);
    return promise.then((data) => MsgEventStatusResponseExport.decode(new _m0.Reader(data)));
  }

  EventMoveToArchive(request: MsgEventMoveToArchive): Promise<MsgEventStatusResponseExport> {
    const data = MsgEventMoveToArchive.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "EventMoveToArchive", data);
    return promise.then((data) => MsgEventStatusResponseExport.decode(new _m0.Reader(data)));
  }

  EventExportCheck(request: MsgEventExportCheck): Promise<MsgEventStatusResponseExport> {
    const data = MsgEventExportCheck.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "EventExportCheck", data);
    return promise.then((data) => MsgEventStatusResponseExport.decode(new _m0.Reader(data)));
  }

  EventAcceptExport(request: MsgEventAcceptExport): Promise<MsgEventStatusResponseExport> {
    const data = MsgEventAcceptExport.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "EventAcceptExport", data);
    return promise.then((data) => MsgEventStatusResponseExport.decode(new _m0.Reader(data)));
  }

  EventExportRefusal(request: MsgEventExportRefusal): Promise<MsgEventStatusResponseExport> {
    const data = MsgEventExportRefusal.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "EventExportRefusal", data);
    return promise.then((data) => MsgEventStatusResponseExport.decode(new _m0.Reader(data)));
  }

  EventAcceptImport(request: MsgEventAcceptImport): Promise<MsgEventStatusResponseImport> {
    const data = MsgEventAcceptImport.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "EventAcceptImport", data);
    return promise.then((data) => MsgEventStatusResponseImport.decode(new _m0.Reader(data)));
  }

  EventCompleteImport(request: MsgEventCompleteImport): Promise<MsgEventStatusResponseImport> {
    const data = MsgEventCompleteImport.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "EventCompleteImport", data);
    return promise.then((data) => MsgEventStatusResponseImport.decode(new _m0.Reader(data)));
  }

  EventImportProposal(request: MsgEventImportProposal): Promise<MsgEventStatusResponseImport> {
    const data = MsgEventImportProposal.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "EventImportProposal", data);
    return promise.then((data) => MsgEventStatusResponseImport.decode(new _m0.Reader(data)));
  }

  EventImportRefusal(request: MsgEventImportRefusal): Promise<MsgEventStatusResponseImport> {
    const data = MsgEventImportRefusal.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "EventImportRefusal", data);
    return promise.then((data) => MsgEventStatusResponseImport.decode(new _m0.Reader(data)));
  }

  EventImportSupplement(request: MsgEventImportSupplement): Promise<MsgEventStatusResponseImport> {
    const data = MsgEventImportSupplement.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "EventImportSupplement", data);
    return promise.then((data) => MsgEventStatusResponseImport.decode(new _m0.Reader(data)));
  }

  EventImportExport(request: MsgEventImportExport): Promise<MsgEventStatusResponseImport> {
    const data = MsgEventImportExport.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "EventImportExport", data);
    return promise.then((data) => MsgEventStatusResponseImport.decode(new _m0.Reader(data)));
  }

  EventImportSubmit(request: MsgEventImportSubmit): Promise<MsgEventStatusResponseImport> {
    const data = MsgEventImportSubmit.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "EventImportSubmit", data);
    return promise.then((data) => MsgEventStatusResponseImport.decode(new _m0.Reader(data)));
  }

  EventImportRelease(request: MsgEventImportRelease): Promise<MsgEventStatusResponseImport> {
    const data = MsgEventImportRelease.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "EventImportRelease", data);
    return promise.then((data) => MsgEventStatusResponseImport.decode(new _m0.Reader(data)));
  }

  FetchExportTokenAllExporter(
    request: MsgFetchExportTokenAllExporter,
  ): Promise<MsgFetchExportTokenAllExporterResponse> {
    const data = MsgFetchExportTokenAllExporter.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "FetchExportTokenAllExporter", data);
    return promise.then((data) => MsgFetchExportTokenAllExporterResponse.decode(new _m0.Reader(data)));
  }

  FetchExportTokenAllDriver(request: MsgFetchExportTokenAllDriver): Promise<MsgFetchExportTokenAllDriverResponse> {
    const data = MsgFetchExportTokenAllDriver.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "FetchExportTokenAllDriver", data);
    return promise.then((data) => MsgFetchExportTokenAllDriverResponse.decode(new _m0.Reader(data)));
  }

  FetchExportTokenAllImporter(
    request: MsgFetchExportTokenAllImporter,
  ): Promise<MsgFetchExportTokenAllImporterResponse> {
    const data = MsgFetchExportTokenAllImporter.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "FetchExportTokenAllImporter", data);
    return promise.then((data) => MsgFetchExportTokenAllImporterResponse.decode(new _m0.Reader(data)));
  }

  FetchExportTokenExporter(request: MsgFetchExportTokenExporter): Promise<MsgFetchExportTokenExporterResponse> {
    const data = MsgFetchExportTokenExporter.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "FetchExportTokenExporter", data);
    return promise.then((data) => MsgFetchExportTokenExporterResponse.decode(new _m0.Reader(data)));
  }

  FetchExportTokenDriver(request: MsgFetchExportTokenDriver): Promise<MsgFetchExportTokenDriverResponse> {
    const data = MsgFetchExportTokenDriver.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "FetchExportTokenDriver", data);
    return promise.then((data) => MsgFetchExportTokenDriverResponse.decode(new _m0.Reader(data)));
  }

  FetchExportTokenImporter(request: MsgFetchExportTokenImporter): Promise<MsgFetchExportTokenImporterResponse> {
    const data = MsgFetchExportTokenImporter.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "FetchExportTokenImporter", data);
    return promise.then((data) => MsgFetchExportTokenImporterResponse.decode(new _m0.Reader(data)));
  }

  FetchExportTokenExporterByExportId(
    request: MsgFetchExportTokenExporterByExportId,
  ): Promise<MsgFetchExportTokenResponse> {
    const data = MsgFetchExportTokenExporterByExportId.encode(request).finish();
    const promise = this.rpc.request(
      "org.borderblockchain.businesslogic.Msg",
      "FetchExportTokenExporterByExportId",
      data,
    );
    return promise.then((data) => MsgFetchExportTokenResponse.decode(new _m0.Reader(data)));
  }

  FetchExportTokenDriverByExportId(request: MsgFetchExportTokenDriverByExportId): Promise<MsgFetchExportTokenResponse> {
    const data = MsgFetchExportTokenDriverByExportId.encode(request).finish();
    const promise = this.rpc.request(
      "org.borderblockchain.businesslogic.Msg",
      "FetchExportTokenDriverByExportId",
      data,
    );
    return promise.then((data) => MsgFetchExportTokenResponse.decode(new _m0.Reader(data)));
  }

  FetchExportTokenImporterByExportId(
    request: MsgFetchExportTokenImporterByExportId,
  ): Promise<MsgFetchExportTokenResponse> {
    const data = MsgFetchExportTokenImporterByExportId.encode(request).finish();
    const promise = this.rpc.request(
      "org.borderblockchain.businesslogic.Msg",
      "FetchExportTokenImporterByExportId",
      data,
    );
    return promise.then((data) => MsgFetchExportTokenResponse.decode(new _m0.Reader(data)));
  }

  FetchTokenHistory(request: MsgFetchTokenHistory): Promise<MsgFetchTokenHistoryResponse> {
    const data = MsgFetchTokenHistory.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "FetchTokenHistory", data);
    return promise.then((data) => MsgFetchTokenHistoryResponse.decode(new _m0.Reader(data)));
  }

  CreateImportToken(request: MsgCreateImportToken): Promise<MsgCreateImportTokenResponse> {
    const data = MsgCreateImportToken.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "CreateImportToken", data);
    return promise.then((data) => MsgCreateImportTokenResponse.decode(new _m0.Reader(data)));
  }

  CreateImportEvent(request: MsgCreateImportEvent): Promise<MsgCreateImportEventResponse> {
    const data = MsgCreateImportEvent.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "CreateImportEvent", data);
    return promise.then((data) => MsgCreateImportEventResponse.decode(new _m0.Reader(data)));
  }

  UpdateImportToken(request: MsgUpdateImportToken): Promise<MsgUpdateImportTokenResponse> {
    const data = MsgUpdateImportToken.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "UpdateImportToken", data);
    return promise.then((data) => MsgUpdateImportTokenResponse.decode(new _m0.Reader(data)));
  }

  FetchImportTokenAll(request: MsgFetchImportTokenAll): Promise<MsgFetchImportTokenAllResponse> {
    const data = MsgFetchImportTokenAll.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "FetchImportTokenAll", data);
    return promise.then((data) => MsgFetchImportTokenAllResponse.decode(new _m0.Reader(data)));
  }

  FetchImportToken(request: MsgFetchImportToken): Promise<MsgFetchImportTokenResponse> {
    const data = MsgFetchImportToken.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "FetchImportToken", data);
    return promise.then((data) => MsgFetchImportTokenResponse.decode(new _m0.Reader(data)));
  }

  FetchImportTokenByImportId(request: MsgFetchImportTokenByImportId): Promise<MsgFetchImportTokenResponse> {
    const data = MsgFetchImportTokenByImportId.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "FetchImportTokenByImportId", data);
    return promise.then((data) => MsgFetchImportTokenResponse.decode(new _m0.Reader(data)));
  }

  AssignCosmosAddressToWallet(request: MsgAssignCosmosAddressToWallet): Promise<MsgEmptyResponse> {
    const data = MsgAssignCosmosAddressToWallet.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.businesslogic.Msg", "AssignCosmosAddressToWallet", data);
    return promise.then((data) => MsgEmptyResponse.decode(new _m0.Reader(data)));
  }
}

interface Rpc {
  request(service: string, method: string, data: Uint8Array): Promise<Uint8Array>;
}

declare var self: any | undefined;
declare var window: any | undefined;
declare var global: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== "undefined") {
    return globalThis;
  }
  if (typeof self !== "undefined") {
    return self;
  }
  if (typeof window !== "undefined") {
    return window;
  }
  if (typeof global !== "undefined") {
    return global;
  }
  throw "Unable to locate global object";
})();

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
  }
  return long.toNumber();
}

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

/* eslint-disable */
import _m0 from "protobufjs/minimal";

export const protobufPackage = "org.borderblockchain.businesslogic";

/**
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */

export interface ExportSegmentInfo {
  ExportId: string;
  LRN: string;
}

function createBaseExportSegmentInfo(): ExportSegmentInfo {
  return { ExportId: "", LRN: "" };
}

export const ExportSegmentInfo = {
  encode(message: ExportSegmentInfo, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.ExportId !== "") {
      writer.uint32(10).string(message.ExportId);
    }
    if (message.LRN !== "") {
      writer.uint32(18).string(message.LRN);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ExportSegmentInfo {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseExportSegmentInfo();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.ExportId = reader.string();
          break;
        case 2:
          message.LRN = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ExportSegmentInfo {
    return {
      ExportId: isSet(object.ExportId) ? String(object.ExportId) : "",
      LRN: isSet(object.LRN) ? String(object.LRN) : "",
    };
  },

  toJSON(message: ExportSegmentInfo): unknown {
    const obj: any = {};
    message.ExportId !== undefined && (obj.ExportId = message.ExportId);
    message.LRN !== undefined && (obj.LRN = message.LRN);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<ExportSegmentInfo>, I>>(object: I): ExportSegmentInfo {
    const message = createBaseExportSegmentInfo();
    message.ExportId = object.ExportId ?? "";
    message.LRN = object.LRN ?? "";
    return message;
  },
};

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

/* eslint-disable */
import _m0 from "protobufjs/minimal";

export const protobufPackage = "org.borderblockchain.businesslogic";

/**
 * Copyright 2021 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */

export interface ExportNotice {
  company: string;
  exportTokenIds: string[];
}

export interface TransportNotice {
  exportTokenId: string;
  carrierUser: string;
}

export interface TransportNotices {
  notices: TransportNotice[];
}

function createBaseExportNotice(): ExportNotice {
  return { company: "", exportTokenIds: [] };
}

export const ExportNotice = {
  encode(message: ExportNotice, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.company !== "") {
      writer.uint32(10).string(message.company);
    }
    for (const v of message.exportTokenIds) {
      writer.uint32(82).string(v!);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ExportNotice {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseExportNotice();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.company = reader.string();
          break;
        case 10:
          message.exportTokenIds.push(reader.string());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ExportNotice {
    return {
      company: isSet(object.company) ? String(object.company) : "",
      exportTokenIds: Array.isArray(object?.exportTokenIds) ? object.exportTokenIds.map((e: any) => String(e)) : [],
    };
  },

  toJSON(message: ExportNotice): unknown {
    const obj: any = {};
    message.company !== undefined && (obj.company = message.company);
    if (message.exportTokenIds) {
      obj.exportTokenIds = message.exportTokenIds.map((e) => e);
    } else {
      obj.exportTokenIds = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<ExportNotice>, I>>(object: I): ExportNotice {
    const message = createBaseExportNotice();
    message.company = object.company ?? "";
    message.exportTokenIds = object.exportTokenIds?.map((e) => e) || [];
    return message;
  },
};

function createBaseTransportNotice(): TransportNotice {
  return { exportTokenId: "", carrierUser: "" };
}

export const TransportNotice = {
  encode(message: TransportNotice, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.exportTokenId !== "") {
      writer.uint32(10).string(message.exportTokenId);
    }
    if (message.carrierUser !== "") {
      writer.uint32(18).string(message.carrierUser);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): TransportNotice {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseTransportNotice();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.exportTokenId = reader.string();
          break;
        case 2:
          message.carrierUser = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): TransportNotice {
    return {
      exportTokenId: isSet(object.exportTokenId) ? String(object.exportTokenId) : "",
      carrierUser: isSet(object.carrierUser) ? String(object.carrierUser) : "",
    };
  },

  toJSON(message: TransportNotice): unknown {
    const obj: any = {};
    message.exportTokenId !== undefined && (obj.exportTokenId = message.exportTokenId);
    message.carrierUser !== undefined && (obj.carrierUser = message.carrierUser);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<TransportNotice>, I>>(object: I): TransportNotice {
    const message = createBaseTransportNotice();
    message.exportTokenId = object.exportTokenId ?? "";
    message.carrierUser = object.carrierUser ?? "";
    return message;
  },
};

function createBaseTransportNotices(): TransportNotices {
  return { notices: [] };
}

export const TransportNotices = {
  encode(message: TransportNotices, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.notices) {
      TransportNotice.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): TransportNotices {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseTransportNotices();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.notices.push(TransportNotice.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): TransportNotices {
    return {
      notices: Array.isArray(object?.notices) ? object.notices.map((e: any) => TransportNotice.fromJSON(e)) : [],
    };
  },

  toJSON(message: TransportNotices): unknown {
    const obj: any = {};
    if (message.notices) {
      obj.notices = message.notices.map((e) => e ? TransportNotice.toJSON(e) : undefined);
    } else {
      obj.notices = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<TransportNotices>, I>>(object: I): TransportNotices {
    const message = createBaseTransportNotices();
    message.notices = object.notices?.map((e) => TransportNotice.fromPartial(e)) || [];
    return message;
  },
};

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

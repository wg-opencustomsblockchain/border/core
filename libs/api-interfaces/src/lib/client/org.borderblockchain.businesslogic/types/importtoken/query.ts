/* eslint-disable */
import _m0 from "protobufjs/minimal";
import { PageRequest, PageResponse } from "../cosmos/base/query/v1beta1/pagination";
import { EventHistory, ImportEvent } from "./event-history";
import { ImportMapping } from "./import-mapping";
import { ImportToken } from "./importtoken";

export const protobufPackage = "org.borderblockchain.importtoken";

/** --- Token Message DTOs--- */
export interface QueryGetImportTokenRequest {
  id: string;
}

export interface QueryGetImportTokenResponse {
  importToken: ImportToken | undefined;
  events: ImportEvent[];
}

export interface QueryAllImportTokenRequest {
  pagination: PageRequest | undefined;
}

export interface QueryAllImportTokenResponse {
  importToken: QueryGetImportTokenResponse[];
  pagination: PageResponse | undefined;
}

/** --- Mapping Message DTOs--- */
export interface QueryGetImportMappingRequest {
  index: string;
}

export interface QueryGetImportMappingResponse {
  importToken: ImportToken | undefined;
  events: ImportEvent[];
}

export interface QueryAllImportMappingRequest {
  pagination: PageRequest | undefined;
}

export interface QueryAllImportMappingResponse {
  importMapping: ImportMapping[];
  pagination: PageResponse | undefined;
}

/** --- History Message DTOs--- */
export interface QueryGetEventHistoryRequest {
  index: string;
}

export interface QueryGetEventHistoryResponse {
  eventHistory: EventHistory | undefined;
}

export interface QueryAllEventHistoryRequest {
  pagination: PageRequest | undefined;
}

export interface QueryAllEventHistoryResponse {
  eventHistory: EventHistory[];
  pagination: PageResponse | undefined;
}

function createBaseQueryGetImportTokenRequest(): QueryGetImportTokenRequest {
  return { id: "" };
}

export const QueryGetImportTokenRequest = {
  encode(message: QueryGetImportTokenRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.id !== "") {
      writer.uint32(10).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryGetImportTokenRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryGetImportTokenRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetImportTokenRequest {
    return { id: isSet(object.id) ? String(object.id) : "" };
  },

  toJSON(message: QueryGetImportTokenRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryGetImportTokenRequest>, I>>(object: I): QueryGetImportTokenRequest {
    const message = createBaseQueryGetImportTokenRequest();
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseQueryGetImportTokenResponse(): QueryGetImportTokenResponse {
  return { importToken: undefined, events: [] };
}

export const QueryGetImportTokenResponse = {
  encode(message: QueryGetImportTokenResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.importToken !== undefined) {
      ImportToken.encode(message.importToken, writer.uint32(10).fork()).ldelim();
    }
    for (const v of message.events) {
      ImportEvent.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryGetImportTokenResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryGetImportTokenResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.importToken = ImportToken.decode(reader, reader.uint32());
          break;
        case 2:
          message.events.push(ImportEvent.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetImportTokenResponse {
    return {
      importToken: isSet(object.importToken) ? ImportToken.fromJSON(object.importToken) : undefined,
      events: Array.isArray(object?.events) ? object.events.map((e: any) => ImportEvent.fromJSON(e)) : [],
    };
  },

  toJSON(message: QueryGetImportTokenResponse): unknown {
    const obj: any = {};
    message.importToken !== undefined
      && (obj.importToken = message.importToken ? ImportToken.toJSON(message.importToken) : undefined);
    if (message.events) {
      obj.events = message.events.map((e) => e ? ImportEvent.toJSON(e) : undefined);
    } else {
      obj.events = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryGetImportTokenResponse>, I>>(object: I): QueryGetImportTokenResponse {
    const message = createBaseQueryGetImportTokenResponse();
    message.importToken = (object.importToken !== undefined && object.importToken !== null)
      ? ImportToken.fromPartial(object.importToken)
      : undefined;
    message.events = object.events?.map((e) => ImportEvent.fromPartial(e)) || [];
    return message;
  },
};

function createBaseQueryAllImportTokenRequest(): QueryAllImportTokenRequest {
  return { pagination: undefined };
}

export const QueryAllImportTokenRequest = {
  encode(message: QueryAllImportTokenRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.pagination !== undefined) {
      PageRequest.encode(message.pagination, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryAllImportTokenRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryAllImportTokenRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.pagination = PageRequest.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllImportTokenRequest {
    return { pagination: isSet(object.pagination) ? PageRequest.fromJSON(object.pagination) : undefined };
  },

  toJSON(message: QueryAllImportTokenRequest): unknown {
    const obj: any = {};
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageRequest.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryAllImportTokenRequest>, I>>(object: I): QueryAllImportTokenRequest {
    const message = createBaseQueryAllImportTokenRequest();
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageRequest.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

function createBaseQueryAllImportTokenResponse(): QueryAllImportTokenResponse {
  return { importToken: [], pagination: undefined };
}

export const QueryAllImportTokenResponse = {
  encode(message: QueryAllImportTokenResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.importToken) {
      QueryGetImportTokenResponse.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.pagination !== undefined) {
      PageResponse.encode(message.pagination, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryAllImportTokenResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryAllImportTokenResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.importToken.push(QueryGetImportTokenResponse.decode(reader, reader.uint32()));
          break;
        case 2:
          message.pagination = PageResponse.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllImportTokenResponse {
    return {
      importToken: Array.isArray(object?.importToken)
        ? object.importToken.map((e: any) => QueryGetImportTokenResponse.fromJSON(e))
        : [],
      pagination: isSet(object.pagination) ? PageResponse.fromJSON(object.pagination) : undefined,
    };
  },

  toJSON(message: QueryAllImportTokenResponse): unknown {
    const obj: any = {};
    if (message.importToken) {
      obj.importToken = message.importToken.map((e) => e ? QueryGetImportTokenResponse.toJSON(e) : undefined);
    } else {
      obj.importToken = [];
    }
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageResponse.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryAllImportTokenResponse>, I>>(object: I): QueryAllImportTokenResponse {
    const message = createBaseQueryAllImportTokenResponse();
    message.importToken = object.importToken?.map((e) => QueryGetImportTokenResponse.fromPartial(e)) || [];
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageResponse.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

function createBaseQueryGetImportMappingRequest(): QueryGetImportMappingRequest {
  return { index: "" };
}

export const QueryGetImportMappingRequest = {
  encode(message: QueryGetImportMappingRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.index !== "") {
      writer.uint32(10).string(message.index);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryGetImportMappingRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryGetImportMappingRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.index = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetImportMappingRequest {
    return { index: isSet(object.index) ? String(object.index) : "" };
  },

  toJSON(message: QueryGetImportMappingRequest): unknown {
    const obj: any = {};
    message.index !== undefined && (obj.index = message.index);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryGetImportMappingRequest>, I>>(object: I): QueryGetImportMappingRequest {
    const message = createBaseQueryGetImportMappingRequest();
    message.index = object.index ?? "";
    return message;
  },
};

function createBaseQueryGetImportMappingResponse(): QueryGetImportMappingResponse {
  return { importToken: undefined, events: [] };
}

export const QueryGetImportMappingResponse = {
  encode(message: QueryGetImportMappingResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.importToken !== undefined) {
      ImportToken.encode(message.importToken, writer.uint32(10).fork()).ldelim();
    }
    for (const v of message.events) {
      ImportEvent.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryGetImportMappingResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryGetImportMappingResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.importToken = ImportToken.decode(reader, reader.uint32());
          break;
        case 2:
          message.events.push(ImportEvent.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetImportMappingResponse {
    return {
      importToken: isSet(object.importToken) ? ImportToken.fromJSON(object.importToken) : undefined,
      events: Array.isArray(object?.events) ? object.events.map((e: any) => ImportEvent.fromJSON(e)) : [],
    };
  },

  toJSON(message: QueryGetImportMappingResponse): unknown {
    const obj: any = {};
    message.importToken !== undefined
      && (obj.importToken = message.importToken ? ImportToken.toJSON(message.importToken) : undefined);
    if (message.events) {
      obj.events = message.events.map((e) => e ? ImportEvent.toJSON(e) : undefined);
    } else {
      obj.events = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryGetImportMappingResponse>, I>>(
    object: I,
  ): QueryGetImportMappingResponse {
    const message = createBaseQueryGetImportMappingResponse();
    message.importToken = (object.importToken !== undefined && object.importToken !== null)
      ? ImportToken.fromPartial(object.importToken)
      : undefined;
    message.events = object.events?.map((e) => ImportEvent.fromPartial(e)) || [];
    return message;
  },
};

function createBaseQueryAllImportMappingRequest(): QueryAllImportMappingRequest {
  return { pagination: undefined };
}

export const QueryAllImportMappingRequest = {
  encode(message: QueryAllImportMappingRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.pagination !== undefined) {
      PageRequest.encode(message.pagination, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryAllImportMappingRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryAllImportMappingRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.pagination = PageRequest.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllImportMappingRequest {
    return { pagination: isSet(object.pagination) ? PageRequest.fromJSON(object.pagination) : undefined };
  },

  toJSON(message: QueryAllImportMappingRequest): unknown {
    const obj: any = {};
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageRequest.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryAllImportMappingRequest>, I>>(object: I): QueryAllImportMappingRequest {
    const message = createBaseQueryAllImportMappingRequest();
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageRequest.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

function createBaseQueryAllImportMappingResponse(): QueryAllImportMappingResponse {
  return { importMapping: [], pagination: undefined };
}

export const QueryAllImportMappingResponse = {
  encode(message: QueryAllImportMappingResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.importMapping) {
      ImportMapping.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.pagination !== undefined) {
      PageResponse.encode(message.pagination, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryAllImportMappingResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryAllImportMappingResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.importMapping.push(ImportMapping.decode(reader, reader.uint32()));
          break;
        case 2:
          message.pagination = PageResponse.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllImportMappingResponse {
    return {
      importMapping: Array.isArray(object?.importMapping)
        ? object.importMapping.map((e: any) => ImportMapping.fromJSON(e))
        : [],
      pagination: isSet(object.pagination) ? PageResponse.fromJSON(object.pagination) : undefined,
    };
  },

  toJSON(message: QueryAllImportMappingResponse): unknown {
    const obj: any = {};
    if (message.importMapping) {
      obj.importMapping = message.importMapping.map((e) => e ? ImportMapping.toJSON(e) : undefined);
    } else {
      obj.importMapping = [];
    }
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageResponse.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryAllImportMappingResponse>, I>>(
    object: I,
  ): QueryAllImportMappingResponse {
    const message = createBaseQueryAllImportMappingResponse();
    message.importMapping = object.importMapping?.map((e) => ImportMapping.fromPartial(e)) || [];
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageResponse.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

function createBaseQueryGetEventHistoryRequest(): QueryGetEventHistoryRequest {
  return { index: "" };
}

export const QueryGetEventHistoryRequest = {
  encode(message: QueryGetEventHistoryRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.index !== "") {
      writer.uint32(10).string(message.index);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryGetEventHistoryRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryGetEventHistoryRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.index = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetEventHistoryRequest {
    return { index: isSet(object.index) ? String(object.index) : "" };
  },

  toJSON(message: QueryGetEventHistoryRequest): unknown {
    const obj: any = {};
    message.index !== undefined && (obj.index = message.index);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryGetEventHistoryRequest>, I>>(object: I): QueryGetEventHistoryRequest {
    const message = createBaseQueryGetEventHistoryRequest();
    message.index = object.index ?? "";
    return message;
  },
};

function createBaseQueryGetEventHistoryResponse(): QueryGetEventHistoryResponse {
  return { eventHistory: undefined };
}

export const QueryGetEventHistoryResponse = {
  encode(message: QueryGetEventHistoryResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.eventHistory !== undefined) {
      EventHistory.encode(message.eventHistory, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryGetEventHistoryResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryGetEventHistoryResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.eventHistory = EventHistory.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetEventHistoryResponse {
    return { eventHistory: isSet(object.eventHistory) ? EventHistory.fromJSON(object.eventHistory) : undefined };
  },

  toJSON(message: QueryGetEventHistoryResponse): unknown {
    const obj: any = {};
    message.eventHistory !== undefined
      && (obj.eventHistory = message.eventHistory ? EventHistory.toJSON(message.eventHistory) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryGetEventHistoryResponse>, I>>(object: I): QueryGetEventHistoryResponse {
    const message = createBaseQueryGetEventHistoryResponse();
    message.eventHistory = (object.eventHistory !== undefined && object.eventHistory !== null)
      ? EventHistory.fromPartial(object.eventHistory)
      : undefined;
    return message;
  },
};

function createBaseQueryAllEventHistoryRequest(): QueryAllEventHistoryRequest {
  return { pagination: undefined };
}

export const QueryAllEventHistoryRequest = {
  encode(message: QueryAllEventHistoryRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.pagination !== undefined) {
      PageRequest.encode(message.pagination, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryAllEventHistoryRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryAllEventHistoryRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.pagination = PageRequest.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllEventHistoryRequest {
    return { pagination: isSet(object.pagination) ? PageRequest.fromJSON(object.pagination) : undefined };
  },

  toJSON(message: QueryAllEventHistoryRequest): unknown {
    const obj: any = {};
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageRequest.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryAllEventHistoryRequest>, I>>(object: I): QueryAllEventHistoryRequest {
    const message = createBaseQueryAllEventHistoryRequest();
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageRequest.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

function createBaseQueryAllEventHistoryResponse(): QueryAllEventHistoryResponse {
  return { eventHistory: [], pagination: undefined };
}

export const QueryAllEventHistoryResponse = {
  encode(message: QueryAllEventHistoryResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.eventHistory) {
      EventHistory.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.pagination !== undefined) {
      PageResponse.encode(message.pagination, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryAllEventHistoryResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryAllEventHistoryResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.eventHistory.push(EventHistory.decode(reader, reader.uint32()));
          break;
        case 2:
          message.pagination = PageResponse.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllEventHistoryResponse {
    return {
      eventHistory: Array.isArray(object?.eventHistory)
        ? object.eventHistory.map((e: any) => EventHistory.fromJSON(e))
        : [],
      pagination: isSet(object.pagination) ? PageResponse.fromJSON(object.pagination) : undefined,
    };
  },

  toJSON(message: QueryAllEventHistoryResponse): unknown {
    const obj: any = {};
    if (message.eventHistory) {
      obj.eventHistory = message.eventHistory.map((e) => e ? EventHistory.toJSON(e) : undefined);
    } else {
      obj.eventHistory = [];
    }
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageResponse.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryAllEventHistoryResponse>, I>>(object: I): QueryAllEventHistoryResponse {
    const message = createBaseQueryAllEventHistoryResponse();
    message.eventHistory = object.eventHistory?.map((e) => EventHistory.fromPartial(e)) || [];
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageResponse.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

/** Query defines the gRPC querier service. */
export interface Query {
}

export class QueryClientImpl implements Query {
  private readonly rpc: Rpc;
  constructor(rpc: Rpc) {
    this.rpc = rpc;
  }
}

interface Rpc {
  request(service: string, method: string, data: Uint8Array): Promise<Uint8Array>;
}

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

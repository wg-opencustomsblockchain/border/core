/* eslint-disable */
import _m0 from "protobufjs/minimal";
import { Segment } from "./segment";

export const protobufPackage = "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet";

export interface SegmentHistory {
  creator: string;
  id: string;
  history: Segment[];
}

function createBaseSegmentHistory(): SegmentHistory {
  return { creator: "", id: "", history: [] };
}

export const SegmentHistory = {
  encode(message: SegmentHistory, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    for (const v of message.history) {
      Segment.encode(v!, writer.uint32(26).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): SegmentHistory {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseSegmentHistory();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.history.push(Segment.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): SegmentHistory {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      history: Array.isArray(object?.history) ? object.history.map((e: any) => Segment.fromJSON(e)) : [],
    };
  },

  toJSON(message: SegmentHistory): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    if (message.history) {
      obj.history = message.history.map((e) => e ? Segment.toJSON(e) : undefined);
    } else {
      obj.history = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<SegmentHistory>, I>>(object: I): SegmentHistory {
    const message = createBaseSegmentHistory();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.history = object.history?.map((e) => Segment.fromPartial(e)) || [];
    return message;
  },
};

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

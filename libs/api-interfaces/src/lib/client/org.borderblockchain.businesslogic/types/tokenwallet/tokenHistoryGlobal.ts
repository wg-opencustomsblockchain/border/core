/* eslint-disable */
import _m0 from "protobufjs/minimal";

export const protobufPackage = "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet";

export interface TokenHistoryGlobal {
  creator: string;
  id: string;
  walletId: string[];
}

function createBaseTokenHistoryGlobal(): TokenHistoryGlobal {
  return { creator: "", id: "", walletId: [] };
}

export const TokenHistoryGlobal = {
  encode(message: TokenHistoryGlobal, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    for (const v of message.walletId) {
      writer.uint32(26).string(v!);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): TokenHistoryGlobal {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseTokenHistoryGlobal();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.walletId.push(reader.string());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): TokenHistoryGlobal {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      walletId: Array.isArray(object?.walletId) ? object.walletId.map((e: any) => String(e)) : [],
    };
  },

  toJSON(message: TokenHistoryGlobal): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    if (message.walletId) {
      obj.walletId = message.walletId.map((e) => e);
    } else {
      obj.walletId = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<TokenHistoryGlobal>, I>>(object: I): TokenHistoryGlobal {
    const message = createBaseTokenHistoryGlobal();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.walletId = object.walletId?.map((e) => e) || [];
    return message;
  },
};

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

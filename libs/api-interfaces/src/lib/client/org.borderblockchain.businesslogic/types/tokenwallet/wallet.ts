/* eslint-disable */
import _m0 from "protobufjs/minimal";

export const protobufPackage = "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet";

export interface Wallet {
  creator: string;
  id: string;
  name: string;
  timestamp: string;
  segmentIds: string[];
  walletAccounts: WalletAccount[];
}

export interface WalletAccount {
  address: string;
  active: boolean;
}

function createBaseWallet(): Wallet {
  return { creator: "", id: "", name: "", timestamp: "", segmentIds: [], walletAccounts: [] };
}

export const Wallet = {
  encode(message: Wallet, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.name !== "") {
      writer.uint32(26).string(message.name);
    }
    if (message.timestamp !== "") {
      writer.uint32(34).string(message.timestamp);
    }
    for (const v of message.segmentIds) {
      writer.uint32(42).string(v!);
    }
    for (const v of message.walletAccounts) {
      WalletAccount.encode(v!, writer.uint32(50).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Wallet {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseWallet();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.name = reader.string();
          break;
        case 4:
          message.timestamp = reader.string();
          break;
        case 5:
          message.segmentIds.push(reader.string());
          break;
        case 6:
          message.walletAccounts.push(WalletAccount.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Wallet {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      name: isSet(object.name) ? String(object.name) : "",
      timestamp: isSet(object.timestamp) ? String(object.timestamp) : "",
      segmentIds: Array.isArray(object?.segmentIds) ? object.segmentIds.map((e: any) => String(e)) : [],
      walletAccounts: Array.isArray(object?.walletAccounts)
        ? object.walletAccounts.map((e: any) => WalletAccount.fromJSON(e))
        : [],
    };
  },

  toJSON(message: Wallet): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.name !== undefined && (obj.name = message.name);
    message.timestamp !== undefined && (obj.timestamp = message.timestamp);
    if (message.segmentIds) {
      obj.segmentIds = message.segmentIds.map((e) => e);
    } else {
      obj.segmentIds = [];
    }
    if (message.walletAccounts) {
      obj.walletAccounts = message.walletAccounts.map((e) => e ? WalletAccount.toJSON(e) : undefined);
    } else {
      obj.walletAccounts = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<Wallet>, I>>(object: I): Wallet {
    const message = createBaseWallet();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.name = object.name ?? "";
    message.timestamp = object.timestamp ?? "";
    message.segmentIds = object.segmentIds?.map((e) => e) || [];
    message.walletAccounts = object.walletAccounts?.map((e) => WalletAccount.fromPartial(e)) || [];
    return message;
  },
};

function createBaseWalletAccount(): WalletAccount {
  return { address: "", active: false };
}

export const WalletAccount = {
  encode(message: WalletAccount, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.address !== "") {
      writer.uint32(10).string(message.address);
    }
    if (message.active === true) {
      writer.uint32(16).bool(message.active);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): WalletAccount {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseWalletAccount();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.address = reader.string();
          break;
        case 2:
          message.active = reader.bool();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): WalletAccount {
    return {
      address: isSet(object.address) ? String(object.address) : "",
      active: isSet(object.active) ? Boolean(object.active) : false,
    };
  },

  toJSON(message: WalletAccount): unknown {
    const obj: any = {};
    message.address !== undefined && (obj.address = message.address);
    message.active !== undefined && (obj.active = message.active);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<WalletAccount>, I>>(object: I): WalletAccount {
    const message = createBaseWalletAccount();
    message.address = object.address ?? "";
    message.active = object.active ?? false;
    return message;
  },
};

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

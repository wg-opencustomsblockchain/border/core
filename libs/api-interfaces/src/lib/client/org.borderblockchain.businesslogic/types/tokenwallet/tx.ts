/* eslint-disable */
import _m0 from "protobufjs/minimal";
import { PageRequest, PageResponse } from "../cosmos/base/query/v1beta1/pagination";
import { Segment } from "./segment";
import { SegmentHistory } from "./segmentHistory";
import { TokenHistoryGlobal } from "./tokenHistoryGlobal";
import { Wallet } from "./wallet";
import { WalletHistory } from "./walletHistory";

export const protobufPackage = "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet";

/** this line is used by starport scaffolding # proto/tx/message */
export interface MsgRemoveTokenRefFromSegment {
  creator: string;
  tokenRefId: string;
  segmentId: string;
}

export interface MsgMoveTokenToSegment {
  creator: string;
  tokenRefId: string;
  sourceSegmentId: string;
  targetSegmentId: string;
}

export interface MsgMoveTokenToWallet {
  creator: string;
  tokenRefId: string;
  sourceSegmentId: string;
  targetWalletId: string;
}

export interface MsgCreateTokenHistoryGlobal {
  creator: string;
  tokenId: string;
  walletIds: string[];
}

export interface MsgUpdateTokenHistoryGlobal {
  creator: string;
  id: string;
}

export interface MsgCreateSegmentHistory {
  creator: string;
  id: string;
  history: Segment[];
}

export interface MsgUpdateSegmentHistory {
  creator: string;
  id: string;
}

export interface MsgCreateWalletHistory {
  creator: string;
  id: string;
  history: Wallet[];
}

export interface MsgUpdateWalletHistory {
  creator: string;
  id: string;
}

export interface MsgCreateSegment {
  creator: string;
  name: string;
  info: string;
  walletId: string;
}

export interface MsgCreateSegmentWithId {
  creator: string;
  Id: string;
  name: string;
  info: string;
  walletId: string;
}

export interface MsgUpdateSegment {
  creator: string;
  id: string;
  name: string;
  info: string;
}

export interface MsgCreateWallet {
  creator: string;
  name: string;
}

export interface MsgCreateWalletWithId {
  creator: string;
  Id: string;
  name: string;
}

export interface MsgAssignCosmosAddressToWallet {
  creator: string;
  cosmosAddress: string;
  walletId: string;
}

export interface MsgUpdateWallet {
  creator: string;
  id: string;
  name: string;
}

export interface MsgCreateTokenRef {
  creator: string;
  id: string;
  moduleRef: string;
  segmentId: string;
}

export interface MsgEmptyResponse {
}

export interface MsgIdResponse {
  id: string;
}

/** this line is used by starport scaffolding # 3 */
export interface MsgFetchGetTokenHistoryGlobal {
  creator: string;
  id: string;
}

export interface MsgFetchGetTokenHistoryGlobalResponse {
  TokenHistoryGlobal: TokenHistoryGlobal | undefined;
}

export interface MsgFetchAllTokenHistoryGlobal {
  creator: string;
  pagination: PageRequest | undefined;
}

export interface MsgFetchAllTokenHistoryGlobalResponse {
  TokenHistoryGlobal: TokenHistoryGlobal[];
  pagination: PageResponse | undefined;
}

export interface MsgFetchGetSegmentHistory {
  creator: string;
  id: string;
}

export interface MsgFetchGetSegmentHistoryResponse {
  SegmentHistory: SegmentHistory | undefined;
}

export interface MsgFetchAllSegmentHistory {
  creator: string;
  pagination: PageRequest | undefined;
}

export interface MsgFetchAllSegmentHistoryResponse {
  SegmentHistory: SegmentHistory[];
  pagination: PageResponse | undefined;
}

export interface MsgFetchGetSegment {
  creator: string;
  id: string;
}

export interface MsgFetchGetSegmentResponse {
  Segment: Segment | undefined;
}

export interface MsgFetchAllSegment {
  creator: string;
  pagination: PageRequest | undefined;
}

export interface MsgFetchAllSegmentResponse {
  Segment: Segment[];
  pagination: PageResponse | undefined;
}

export interface MsgFetchGetWalletHistory {
  creator: string;
  id: string;
}

export interface MsgFetchGetWalletHistoryResponse {
  WalletHistory: WalletHistory | undefined;
}

export interface MsgFetchAllWalletHistory {
  creator: string;
  pagination: PageRequest | undefined;
}

export interface MsgFetchAllWalletHistoryResponse {
  WalletHistory: WalletHistory[];
  pagination: PageResponse | undefined;
}

export interface MsgFetchGetWallet {
  creator: string;
  id: string;
}

export interface MsgFetchGetWalletResponse {
  Wallet: Wallet | undefined;
}

export interface MsgFetchAllWallet {
  creator: string;
  pagination: PageRequest | undefined;
}

export interface MsgFetchAllWalletResponse {
  Wallet: Wallet[];
  pagination: PageResponse | undefined;
}

function createBaseMsgRemoveTokenRefFromSegment(): MsgRemoveTokenRefFromSegment {
  return { creator: "", tokenRefId: "", segmentId: "" };
}

export const MsgRemoveTokenRefFromSegment = {
  encode(message: MsgRemoveTokenRefFromSegment, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.tokenRefId !== "") {
      writer.uint32(18).string(message.tokenRefId);
    }
    if (message.segmentId !== "") {
      writer.uint32(26).string(message.segmentId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgRemoveTokenRefFromSegment {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgRemoveTokenRefFromSegment();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.tokenRefId = reader.string();
          break;
        case 3:
          message.segmentId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgRemoveTokenRefFromSegment {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      tokenRefId: isSet(object.tokenRefId) ? String(object.tokenRefId) : "",
      segmentId: isSet(object.segmentId) ? String(object.segmentId) : "",
    };
  },

  toJSON(message: MsgRemoveTokenRefFromSegment): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.tokenRefId !== undefined && (obj.tokenRefId = message.tokenRefId);
    message.segmentId !== undefined && (obj.segmentId = message.segmentId);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgRemoveTokenRefFromSegment>, I>>(object: I): MsgRemoveTokenRefFromSegment {
    const message = createBaseMsgRemoveTokenRefFromSegment();
    message.creator = object.creator ?? "";
    message.tokenRefId = object.tokenRefId ?? "";
    message.segmentId = object.segmentId ?? "";
    return message;
  },
};

function createBaseMsgMoveTokenToSegment(): MsgMoveTokenToSegment {
  return { creator: "", tokenRefId: "", sourceSegmentId: "", targetSegmentId: "" };
}

export const MsgMoveTokenToSegment = {
  encode(message: MsgMoveTokenToSegment, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.tokenRefId !== "") {
      writer.uint32(18).string(message.tokenRefId);
    }
    if (message.sourceSegmentId !== "") {
      writer.uint32(26).string(message.sourceSegmentId);
    }
    if (message.targetSegmentId !== "") {
      writer.uint32(34).string(message.targetSegmentId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgMoveTokenToSegment {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgMoveTokenToSegment();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.tokenRefId = reader.string();
          break;
        case 3:
          message.sourceSegmentId = reader.string();
          break;
        case 4:
          message.targetSegmentId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgMoveTokenToSegment {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      tokenRefId: isSet(object.tokenRefId) ? String(object.tokenRefId) : "",
      sourceSegmentId: isSet(object.sourceSegmentId) ? String(object.sourceSegmentId) : "",
      targetSegmentId: isSet(object.targetSegmentId) ? String(object.targetSegmentId) : "",
    };
  },

  toJSON(message: MsgMoveTokenToSegment): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.tokenRefId !== undefined && (obj.tokenRefId = message.tokenRefId);
    message.sourceSegmentId !== undefined && (obj.sourceSegmentId = message.sourceSegmentId);
    message.targetSegmentId !== undefined && (obj.targetSegmentId = message.targetSegmentId);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgMoveTokenToSegment>, I>>(object: I): MsgMoveTokenToSegment {
    const message = createBaseMsgMoveTokenToSegment();
    message.creator = object.creator ?? "";
    message.tokenRefId = object.tokenRefId ?? "";
    message.sourceSegmentId = object.sourceSegmentId ?? "";
    message.targetSegmentId = object.targetSegmentId ?? "";
    return message;
  },
};

function createBaseMsgMoveTokenToWallet(): MsgMoveTokenToWallet {
  return { creator: "", tokenRefId: "", sourceSegmentId: "", targetWalletId: "" };
}

export const MsgMoveTokenToWallet = {
  encode(message: MsgMoveTokenToWallet, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.tokenRefId !== "") {
      writer.uint32(18).string(message.tokenRefId);
    }
    if (message.sourceSegmentId !== "") {
      writer.uint32(26).string(message.sourceSegmentId);
    }
    if (message.targetWalletId !== "") {
      writer.uint32(34).string(message.targetWalletId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgMoveTokenToWallet {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgMoveTokenToWallet();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.tokenRefId = reader.string();
          break;
        case 3:
          message.sourceSegmentId = reader.string();
          break;
        case 4:
          message.targetWalletId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgMoveTokenToWallet {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      tokenRefId: isSet(object.tokenRefId) ? String(object.tokenRefId) : "",
      sourceSegmentId: isSet(object.sourceSegmentId) ? String(object.sourceSegmentId) : "",
      targetWalletId: isSet(object.targetWalletId) ? String(object.targetWalletId) : "",
    };
  },

  toJSON(message: MsgMoveTokenToWallet): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.tokenRefId !== undefined && (obj.tokenRefId = message.tokenRefId);
    message.sourceSegmentId !== undefined && (obj.sourceSegmentId = message.sourceSegmentId);
    message.targetWalletId !== undefined && (obj.targetWalletId = message.targetWalletId);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgMoveTokenToWallet>, I>>(object: I): MsgMoveTokenToWallet {
    const message = createBaseMsgMoveTokenToWallet();
    message.creator = object.creator ?? "";
    message.tokenRefId = object.tokenRefId ?? "";
    message.sourceSegmentId = object.sourceSegmentId ?? "";
    message.targetWalletId = object.targetWalletId ?? "";
    return message;
  },
};

function createBaseMsgCreateTokenHistoryGlobal(): MsgCreateTokenHistoryGlobal {
  return { creator: "", tokenId: "", walletIds: [] };
}

export const MsgCreateTokenHistoryGlobal = {
  encode(message: MsgCreateTokenHistoryGlobal, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.tokenId !== "") {
      writer.uint32(18).string(message.tokenId);
    }
    for (const v of message.walletIds) {
      writer.uint32(26).string(v!);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateTokenHistoryGlobal {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateTokenHistoryGlobal();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.tokenId = reader.string();
          break;
        case 3:
          message.walletIds.push(reader.string());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateTokenHistoryGlobal {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      tokenId: isSet(object.tokenId) ? String(object.tokenId) : "",
      walletIds: Array.isArray(object?.walletIds) ? object.walletIds.map((e: any) => String(e)) : [],
    };
  },

  toJSON(message: MsgCreateTokenHistoryGlobal): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.tokenId !== undefined && (obj.tokenId = message.tokenId);
    if (message.walletIds) {
      obj.walletIds = message.walletIds.map((e) => e);
    } else {
      obj.walletIds = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateTokenHistoryGlobal>, I>>(object: I): MsgCreateTokenHistoryGlobal {
    const message = createBaseMsgCreateTokenHistoryGlobal();
    message.creator = object.creator ?? "";
    message.tokenId = object.tokenId ?? "";
    message.walletIds = object.walletIds?.map((e) => e) || [];
    return message;
  },
};

function createBaseMsgUpdateTokenHistoryGlobal(): MsgUpdateTokenHistoryGlobal {
  return { creator: "", id: "" };
}

export const MsgUpdateTokenHistoryGlobal = {
  encode(message: MsgUpdateTokenHistoryGlobal, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgUpdateTokenHistoryGlobal {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgUpdateTokenHistoryGlobal();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateTokenHistoryGlobal {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
    };
  },

  toJSON(message: MsgUpdateTokenHistoryGlobal): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgUpdateTokenHistoryGlobal>, I>>(object: I): MsgUpdateTokenHistoryGlobal {
    const message = createBaseMsgUpdateTokenHistoryGlobal();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgCreateSegmentHistory(): MsgCreateSegmentHistory {
  return { creator: "", id: "", history: [] };
}

export const MsgCreateSegmentHistory = {
  encode(message: MsgCreateSegmentHistory, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    for (const v of message.history) {
      Segment.encode(v!, writer.uint32(26).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateSegmentHistory {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateSegmentHistory();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.history.push(Segment.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateSegmentHistory {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      history: Array.isArray(object?.history) ? object.history.map((e: any) => Segment.fromJSON(e)) : [],
    };
  },

  toJSON(message: MsgCreateSegmentHistory): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    if (message.history) {
      obj.history = message.history.map((e) => e ? Segment.toJSON(e) : undefined);
    } else {
      obj.history = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateSegmentHistory>, I>>(object: I): MsgCreateSegmentHistory {
    const message = createBaseMsgCreateSegmentHistory();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.history = object.history?.map((e) => Segment.fromPartial(e)) || [];
    return message;
  },
};

function createBaseMsgUpdateSegmentHistory(): MsgUpdateSegmentHistory {
  return { creator: "", id: "" };
}

export const MsgUpdateSegmentHistory = {
  encode(message: MsgUpdateSegmentHistory, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgUpdateSegmentHistory {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgUpdateSegmentHistory();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateSegmentHistory {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
    };
  },

  toJSON(message: MsgUpdateSegmentHistory): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgUpdateSegmentHistory>, I>>(object: I): MsgUpdateSegmentHistory {
    const message = createBaseMsgUpdateSegmentHistory();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgCreateWalletHistory(): MsgCreateWalletHistory {
  return { creator: "", id: "", history: [] };
}

export const MsgCreateWalletHistory = {
  encode(message: MsgCreateWalletHistory, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    for (const v of message.history) {
      Wallet.encode(v!, writer.uint32(26).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateWalletHistory {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateWalletHistory();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.history.push(Wallet.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateWalletHistory {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      history: Array.isArray(object?.history) ? object.history.map((e: any) => Wallet.fromJSON(e)) : [],
    };
  },

  toJSON(message: MsgCreateWalletHistory): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    if (message.history) {
      obj.history = message.history.map((e) => e ? Wallet.toJSON(e) : undefined);
    } else {
      obj.history = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateWalletHistory>, I>>(object: I): MsgCreateWalletHistory {
    const message = createBaseMsgCreateWalletHistory();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.history = object.history?.map((e) => Wallet.fromPartial(e)) || [];
    return message;
  },
};

function createBaseMsgUpdateWalletHistory(): MsgUpdateWalletHistory {
  return { creator: "", id: "" };
}

export const MsgUpdateWalletHistory = {
  encode(message: MsgUpdateWalletHistory, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgUpdateWalletHistory {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgUpdateWalletHistory();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateWalletHistory {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
    };
  },

  toJSON(message: MsgUpdateWalletHistory): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgUpdateWalletHistory>, I>>(object: I): MsgUpdateWalletHistory {
    const message = createBaseMsgUpdateWalletHistory();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgCreateSegment(): MsgCreateSegment {
  return { creator: "", name: "", info: "", walletId: "" };
}

export const MsgCreateSegment = {
  encode(message: MsgCreateSegment, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.name !== "") {
      writer.uint32(18).string(message.name);
    }
    if (message.info !== "") {
      writer.uint32(26).string(message.info);
    }
    if (message.walletId !== "") {
      writer.uint32(34).string(message.walletId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateSegment {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateSegment();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.name = reader.string();
          break;
        case 3:
          message.info = reader.string();
          break;
        case 4:
          message.walletId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateSegment {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      name: isSet(object.name) ? String(object.name) : "",
      info: isSet(object.info) ? String(object.info) : "",
      walletId: isSet(object.walletId) ? String(object.walletId) : "",
    };
  },

  toJSON(message: MsgCreateSegment): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.name !== undefined && (obj.name = message.name);
    message.info !== undefined && (obj.info = message.info);
    message.walletId !== undefined && (obj.walletId = message.walletId);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateSegment>, I>>(object: I): MsgCreateSegment {
    const message = createBaseMsgCreateSegment();
    message.creator = object.creator ?? "";
    message.name = object.name ?? "";
    message.info = object.info ?? "";
    message.walletId = object.walletId ?? "";
    return message;
  },
};

function createBaseMsgCreateSegmentWithId(): MsgCreateSegmentWithId {
  return { creator: "", Id: "", name: "", info: "", walletId: "" };
}

export const MsgCreateSegmentWithId = {
  encode(message: MsgCreateSegmentWithId, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.Id !== "") {
      writer.uint32(18).string(message.Id);
    }
    if (message.name !== "") {
      writer.uint32(26).string(message.name);
    }
    if (message.info !== "") {
      writer.uint32(34).string(message.info);
    }
    if (message.walletId !== "") {
      writer.uint32(42).string(message.walletId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateSegmentWithId {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateSegmentWithId();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.Id = reader.string();
          break;
        case 3:
          message.name = reader.string();
          break;
        case 4:
          message.info = reader.string();
          break;
        case 5:
          message.walletId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateSegmentWithId {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      Id: isSet(object.Id) ? String(object.Id) : "",
      name: isSet(object.name) ? String(object.name) : "",
      info: isSet(object.info) ? String(object.info) : "",
      walletId: isSet(object.walletId) ? String(object.walletId) : "",
    };
  },

  toJSON(message: MsgCreateSegmentWithId): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.Id !== undefined && (obj.Id = message.Id);
    message.name !== undefined && (obj.name = message.name);
    message.info !== undefined && (obj.info = message.info);
    message.walletId !== undefined && (obj.walletId = message.walletId);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateSegmentWithId>, I>>(object: I): MsgCreateSegmentWithId {
    const message = createBaseMsgCreateSegmentWithId();
    message.creator = object.creator ?? "";
    message.Id = object.Id ?? "";
    message.name = object.name ?? "";
    message.info = object.info ?? "";
    message.walletId = object.walletId ?? "";
    return message;
  },
};

function createBaseMsgUpdateSegment(): MsgUpdateSegment {
  return { creator: "", id: "", name: "", info: "" };
}

export const MsgUpdateSegment = {
  encode(message: MsgUpdateSegment, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.name !== "") {
      writer.uint32(26).string(message.name);
    }
    if (message.info !== "") {
      writer.uint32(34).string(message.info);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgUpdateSegment {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgUpdateSegment();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.name = reader.string();
          break;
        case 4:
          message.info = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateSegment {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      name: isSet(object.name) ? String(object.name) : "",
      info: isSet(object.info) ? String(object.info) : "",
    };
  },

  toJSON(message: MsgUpdateSegment): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.name !== undefined && (obj.name = message.name);
    message.info !== undefined && (obj.info = message.info);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgUpdateSegment>, I>>(object: I): MsgUpdateSegment {
    const message = createBaseMsgUpdateSegment();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.name = object.name ?? "";
    message.info = object.info ?? "";
    return message;
  },
};

function createBaseMsgCreateWallet(): MsgCreateWallet {
  return { creator: "", name: "" };
}

export const MsgCreateWallet = {
  encode(message: MsgCreateWallet, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.name !== "") {
      writer.uint32(18).string(message.name);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateWallet {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateWallet();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.name = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateWallet {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      name: isSet(object.name) ? String(object.name) : "",
    };
  },

  toJSON(message: MsgCreateWallet): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.name !== undefined && (obj.name = message.name);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateWallet>, I>>(object: I): MsgCreateWallet {
    const message = createBaseMsgCreateWallet();
    message.creator = object.creator ?? "";
    message.name = object.name ?? "";
    return message;
  },
};

function createBaseMsgCreateWalletWithId(): MsgCreateWalletWithId {
  return { creator: "", Id: "", name: "" };
}

export const MsgCreateWalletWithId = {
  encode(message: MsgCreateWalletWithId, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.Id !== "") {
      writer.uint32(18).string(message.Id);
    }
    if (message.name !== "") {
      writer.uint32(26).string(message.name);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateWalletWithId {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateWalletWithId();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.Id = reader.string();
          break;
        case 3:
          message.name = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateWalletWithId {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      Id: isSet(object.Id) ? String(object.Id) : "",
      name: isSet(object.name) ? String(object.name) : "",
    };
  },

  toJSON(message: MsgCreateWalletWithId): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.Id !== undefined && (obj.Id = message.Id);
    message.name !== undefined && (obj.name = message.name);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateWalletWithId>, I>>(object: I): MsgCreateWalletWithId {
    const message = createBaseMsgCreateWalletWithId();
    message.creator = object.creator ?? "";
    message.Id = object.Id ?? "";
    message.name = object.name ?? "";
    return message;
  },
};

function createBaseMsgAssignCosmosAddressToWallet(): MsgAssignCosmosAddressToWallet {
  return { creator: "", cosmosAddress: "", walletId: "" };
}

export const MsgAssignCosmosAddressToWallet = {
  encode(message: MsgAssignCosmosAddressToWallet, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.cosmosAddress !== "") {
      writer.uint32(18).string(message.cosmosAddress);
    }
    if (message.walletId !== "") {
      writer.uint32(26).string(message.walletId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgAssignCosmosAddressToWallet {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgAssignCosmosAddressToWallet();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.cosmosAddress = reader.string();
          break;
        case 3:
          message.walletId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgAssignCosmosAddressToWallet {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      cosmosAddress: isSet(object.cosmosAddress) ? String(object.cosmosAddress) : "",
      walletId: isSet(object.walletId) ? String(object.walletId) : "",
    };
  },

  toJSON(message: MsgAssignCosmosAddressToWallet): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.cosmosAddress !== undefined && (obj.cosmosAddress = message.cosmosAddress);
    message.walletId !== undefined && (obj.walletId = message.walletId);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgAssignCosmosAddressToWallet>, I>>(
    object: I,
  ): MsgAssignCosmosAddressToWallet {
    const message = createBaseMsgAssignCosmosAddressToWallet();
    message.creator = object.creator ?? "";
    message.cosmosAddress = object.cosmosAddress ?? "";
    message.walletId = object.walletId ?? "";
    return message;
  },
};

function createBaseMsgUpdateWallet(): MsgUpdateWallet {
  return { creator: "", id: "", name: "" };
}

export const MsgUpdateWallet = {
  encode(message: MsgUpdateWallet, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.name !== "") {
      writer.uint32(26).string(message.name);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgUpdateWallet {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgUpdateWallet();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.name = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateWallet {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      name: isSet(object.name) ? String(object.name) : "",
    };
  },

  toJSON(message: MsgUpdateWallet): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.name !== undefined && (obj.name = message.name);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgUpdateWallet>, I>>(object: I): MsgUpdateWallet {
    const message = createBaseMsgUpdateWallet();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.name = object.name ?? "";
    return message;
  },
};

function createBaseMsgCreateTokenRef(): MsgCreateTokenRef {
  return { creator: "", id: "", moduleRef: "", segmentId: "" };
}

export const MsgCreateTokenRef = {
  encode(message: MsgCreateTokenRef, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.moduleRef !== "") {
      writer.uint32(26).string(message.moduleRef);
    }
    if (message.segmentId !== "") {
      writer.uint32(34).string(message.segmentId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateTokenRef {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateTokenRef();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.moduleRef = reader.string();
          break;
        case 4:
          message.segmentId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateTokenRef {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      moduleRef: isSet(object.moduleRef) ? String(object.moduleRef) : "",
      segmentId: isSet(object.segmentId) ? String(object.segmentId) : "",
    };
  },

  toJSON(message: MsgCreateTokenRef): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.moduleRef !== undefined && (obj.moduleRef = message.moduleRef);
    message.segmentId !== undefined && (obj.segmentId = message.segmentId);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateTokenRef>, I>>(object: I): MsgCreateTokenRef {
    const message = createBaseMsgCreateTokenRef();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.moduleRef = object.moduleRef ?? "";
    message.segmentId = object.segmentId ?? "";
    return message;
  },
};

function createBaseMsgEmptyResponse(): MsgEmptyResponse {
  return {};
}

export const MsgEmptyResponse = {
  encode(_: MsgEmptyResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgEmptyResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgEmptyResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgEmptyResponse {
    return {};
  },

  toJSON(_: MsgEmptyResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgEmptyResponse>, I>>(_: I): MsgEmptyResponse {
    const message = createBaseMsgEmptyResponse();
    return message;
  },
};

function createBaseMsgIdResponse(): MsgIdResponse {
  return { id: "" };
}

export const MsgIdResponse = {
  encode(message: MsgIdResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.id !== "") {
      writer.uint32(10).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgIdResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgIdResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgIdResponse {
    return { id: isSet(object.id) ? String(object.id) : "" };
  },

  toJSON(message: MsgIdResponse): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgIdResponse>, I>>(object: I): MsgIdResponse {
    const message = createBaseMsgIdResponse();
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgFetchGetTokenHistoryGlobal(): MsgFetchGetTokenHistoryGlobal {
  return { creator: "", id: "" };
}

export const MsgFetchGetTokenHistoryGlobal = {
  encode(message: MsgFetchGetTokenHistoryGlobal, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchGetTokenHistoryGlobal {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchGetTokenHistoryGlobal();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchGetTokenHistoryGlobal {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
    };
  },

  toJSON(message: MsgFetchGetTokenHistoryGlobal): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchGetTokenHistoryGlobal>, I>>(
    object: I,
  ): MsgFetchGetTokenHistoryGlobal {
    const message = createBaseMsgFetchGetTokenHistoryGlobal();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgFetchGetTokenHistoryGlobalResponse(): MsgFetchGetTokenHistoryGlobalResponse {
  return { TokenHistoryGlobal: undefined };
}

export const MsgFetchGetTokenHistoryGlobalResponse = {
  encode(message: MsgFetchGetTokenHistoryGlobalResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.TokenHistoryGlobal !== undefined) {
      TokenHistoryGlobal.encode(message.TokenHistoryGlobal, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchGetTokenHistoryGlobalResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchGetTokenHistoryGlobalResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.TokenHistoryGlobal = TokenHistoryGlobal.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchGetTokenHistoryGlobalResponse {
    return {
      TokenHistoryGlobal: isSet(object.TokenHistoryGlobal)
        ? TokenHistoryGlobal.fromJSON(object.TokenHistoryGlobal)
        : undefined,
    };
  },

  toJSON(message: MsgFetchGetTokenHistoryGlobalResponse): unknown {
    const obj: any = {};
    message.TokenHistoryGlobal !== undefined && (obj.TokenHistoryGlobal = message.TokenHistoryGlobal
      ? TokenHistoryGlobal.toJSON(message.TokenHistoryGlobal)
      : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchGetTokenHistoryGlobalResponse>, I>>(
    object: I,
  ): MsgFetchGetTokenHistoryGlobalResponse {
    const message = createBaseMsgFetchGetTokenHistoryGlobalResponse();
    message.TokenHistoryGlobal = (object.TokenHistoryGlobal !== undefined && object.TokenHistoryGlobal !== null)
      ? TokenHistoryGlobal.fromPartial(object.TokenHistoryGlobal)
      : undefined;
    return message;
  },
};

function createBaseMsgFetchAllTokenHistoryGlobal(): MsgFetchAllTokenHistoryGlobal {
  return { creator: "", pagination: undefined };
}

export const MsgFetchAllTokenHistoryGlobal = {
  encode(message: MsgFetchAllTokenHistoryGlobal, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.pagination !== undefined) {
      PageRequest.encode(message.pagination, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchAllTokenHistoryGlobal {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchAllTokenHistoryGlobal();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.pagination = PageRequest.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllTokenHistoryGlobal {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      pagination: isSet(object.pagination) ? PageRequest.fromJSON(object.pagination) : undefined,
    };
  },

  toJSON(message: MsgFetchAllTokenHistoryGlobal): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageRequest.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchAllTokenHistoryGlobal>, I>>(
    object: I,
  ): MsgFetchAllTokenHistoryGlobal {
    const message = createBaseMsgFetchAllTokenHistoryGlobal();
    message.creator = object.creator ?? "";
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageRequest.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

function createBaseMsgFetchAllTokenHistoryGlobalResponse(): MsgFetchAllTokenHistoryGlobalResponse {
  return { TokenHistoryGlobal: [], pagination: undefined };
}

export const MsgFetchAllTokenHistoryGlobalResponse = {
  encode(message: MsgFetchAllTokenHistoryGlobalResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.TokenHistoryGlobal) {
      TokenHistoryGlobal.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.pagination !== undefined) {
      PageResponse.encode(message.pagination, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchAllTokenHistoryGlobalResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchAllTokenHistoryGlobalResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.TokenHistoryGlobal.push(TokenHistoryGlobal.decode(reader, reader.uint32()));
          break;
        case 2:
          message.pagination = PageResponse.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllTokenHistoryGlobalResponse {
    return {
      TokenHistoryGlobal: Array.isArray(object?.TokenHistoryGlobal)
        ? object.TokenHistoryGlobal.map((e: any) => TokenHistoryGlobal.fromJSON(e))
        : [],
      pagination: isSet(object.pagination) ? PageResponse.fromJSON(object.pagination) : undefined,
    };
  },

  toJSON(message: MsgFetchAllTokenHistoryGlobalResponse): unknown {
    const obj: any = {};
    if (message.TokenHistoryGlobal) {
      obj.TokenHistoryGlobal = message.TokenHistoryGlobal.map((e) => e ? TokenHistoryGlobal.toJSON(e) : undefined);
    } else {
      obj.TokenHistoryGlobal = [];
    }
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageResponse.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchAllTokenHistoryGlobalResponse>, I>>(
    object: I,
  ): MsgFetchAllTokenHistoryGlobalResponse {
    const message = createBaseMsgFetchAllTokenHistoryGlobalResponse();
    message.TokenHistoryGlobal = object.TokenHistoryGlobal?.map((e) => TokenHistoryGlobal.fromPartial(e)) || [];
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageResponse.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

function createBaseMsgFetchGetSegmentHistory(): MsgFetchGetSegmentHistory {
  return { creator: "", id: "" };
}

export const MsgFetchGetSegmentHistory = {
  encode(message: MsgFetchGetSegmentHistory, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchGetSegmentHistory {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchGetSegmentHistory();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchGetSegmentHistory {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
    };
  },

  toJSON(message: MsgFetchGetSegmentHistory): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchGetSegmentHistory>, I>>(object: I): MsgFetchGetSegmentHistory {
    const message = createBaseMsgFetchGetSegmentHistory();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgFetchGetSegmentHistoryResponse(): MsgFetchGetSegmentHistoryResponse {
  return { SegmentHistory: undefined };
}

export const MsgFetchGetSegmentHistoryResponse = {
  encode(message: MsgFetchGetSegmentHistoryResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.SegmentHistory !== undefined) {
      SegmentHistory.encode(message.SegmentHistory, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchGetSegmentHistoryResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchGetSegmentHistoryResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.SegmentHistory = SegmentHistory.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchGetSegmentHistoryResponse {
    return {
      SegmentHistory: isSet(object.SegmentHistory) ? SegmentHistory.fromJSON(object.SegmentHistory) : undefined,
    };
  },

  toJSON(message: MsgFetchGetSegmentHistoryResponse): unknown {
    const obj: any = {};
    message.SegmentHistory !== undefined
      && (obj.SegmentHistory = message.SegmentHistory ? SegmentHistory.toJSON(message.SegmentHistory) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchGetSegmentHistoryResponse>, I>>(
    object: I,
  ): MsgFetchGetSegmentHistoryResponse {
    const message = createBaseMsgFetchGetSegmentHistoryResponse();
    message.SegmentHistory = (object.SegmentHistory !== undefined && object.SegmentHistory !== null)
      ? SegmentHistory.fromPartial(object.SegmentHistory)
      : undefined;
    return message;
  },
};

function createBaseMsgFetchAllSegmentHistory(): MsgFetchAllSegmentHistory {
  return { creator: "", pagination: undefined };
}

export const MsgFetchAllSegmentHistory = {
  encode(message: MsgFetchAllSegmentHistory, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.pagination !== undefined) {
      PageRequest.encode(message.pagination, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchAllSegmentHistory {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchAllSegmentHistory();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.pagination = PageRequest.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllSegmentHistory {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      pagination: isSet(object.pagination) ? PageRequest.fromJSON(object.pagination) : undefined,
    };
  },

  toJSON(message: MsgFetchAllSegmentHistory): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageRequest.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchAllSegmentHistory>, I>>(object: I): MsgFetchAllSegmentHistory {
    const message = createBaseMsgFetchAllSegmentHistory();
    message.creator = object.creator ?? "";
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageRequest.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

function createBaseMsgFetchAllSegmentHistoryResponse(): MsgFetchAllSegmentHistoryResponse {
  return { SegmentHistory: [], pagination: undefined };
}

export const MsgFetchAllSegmentHistoryResponse = {
  encode(message: MsgFetchAllSegmentHistoryResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.SegmentHistory) {
      SegmentHistory.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.pagination !== undefined) {
      PageResponse.encode(message.pagination, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchAllSegmentHistoryResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchAllSegmentHistoryResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.SegmentHistory.push(SegmentHistory.decode(reader, reader.uint32()));
          break;
        case 2:
          message.pagination = PageResponse.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllSegmentHistoryResponse {
    return {
      SegmentHistory: Array.isArray(object?.SegmentHistory)
        ? object.SegmentHistory.map((e: any) => SegmentHistory.fromJSON(e))
        : [],
      pagination: isSet(object.pagination) ? PageResponse.fromJSON(object.pagination) : undefined,
    };
  },

  toJSON(message: MsgFetchAllSegmentHistoryResponse): unknown {
    const obj: any = {};
    if (message.SegmentHistory) {
      obj.SegmentHistory = message.SegmentHistory.map((e) => e ? SegmentHistory.toJSON(e) : undefined);
    } else {
      obj.SegmentHistory = [];
    }
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageResponse.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchAllSegmentHistoryResponse>, I>>(
    object: I,
  ): MsgFetchAllSegmentHistoryResponse {
    const message = createBaseMsgFetchAllSegmentHistoryResponse();
    message.SegmentHistory = object.SegmentHistory?.map((e) => SegmentHistory.fromPartial(e)) || [];
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageResponse.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

function createBaseMsgFetchGetSegment(): MsgFetchGetSegment {
  return { creator: "", id: "" };
}

export const MsgFetchGetSegment = {
  encode(message: MsgFetchGetSegment, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchGetSegment {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchGetSegment();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchGetSegment {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
    };
  },

  toJSON(message: MsgFetchGetSegment): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchGetSegment>, I>>(object: I): MsgFetchGetSegment {
    const message = createBaseMsgFetchGetSegment();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgFetchGetSegmentResponse(): MsgFetchGetSegmentResponse {
  return { Segment: undefined };
}

export const MsgFetchGetSegmentResponse = {
  encode(message: MsgFetchGetSegmentResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.Segment !== undefined) {
      Segment.encode(message.Segment, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchGetSegmentResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchGetSegmentResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.Segment = Segment.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchGetSegmentResponse {
    return { Segment: isSet(object.Segment) ? Segment.fromJSON(object.Segment) : undefined };
  },

  toJSON(message: MsgFetchGetSegmentResponse): unknown {
    const obj: any = {};
    message.Segment !== undefined && (obj.Segment = message.Segment ? Segment.toJSON(message.Segment) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchGetSegmentResponse>, I>>(object: I): MsgFetchGetSegmentResponse {
    const message = createBaseMsgFetchGetSegmentResponse();
    message.Segment = (object.Segment !== undefined && object.Segment !== null)
      ? Segment.fromPartial(object.Segment)
      : undefined;
    return message;
  },
};

function createBaseMsgFetchAllSegment(): MsgFetchAllSegment {
  return { creator: "", pagination: undefined };
}

export const MsgFetchAllSegment = {
  encode(message: MsgFetchAllSegment, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.pagination !== undefined) {
      PageRequest.encode(message.pagination, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchAllSegment {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchAllSegment();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.pagination = PageRequest.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllSegment {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      pagination: isSet(object.pagination) ? PageRequest.fromJSON(object.pagination) : undefined,
    };
  },

  toJSON(message: MsgFetchAllSegment): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageRequest.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchAllSegment>, I>>(object: I): MsgFetchAllSegment {
    const message = createBaseMsgFetchAllSegment();
    message.creator = object.creator ?? "";
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageRequest.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

function createBaseMsgFetchAllSegmentResponse(): MsgFetchAllSegmentResponse {
  return { Segment: [], pagination: undefined };
}

export const MsgFetchAllSegmentResponse = {
  encode(message: MsgFetchAllSegmentResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.Segment) {
      Segment.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.pagination !== undefined) {
      PageResponse.encode(message.pagination, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchAllSegmentResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchAllSegmentResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.Segment.push(Segment.decode(reader, reader.uint32()));
          break;
        case 2:
          message.pagination = PageResponse.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllSegmentResponse {
    return {
      Segment: Array.isArray(object?.Segment) ? object.Segment.map((e: any) => Segment.fromJSON(e)) : [],
      pagination: isSet(object.pagination) ? PageResponse.fromJSON(object.pagination) : undefined,
    };
  },

  toJSON(message: MsgFetchAllSegmentResponse): unknown {
    const obj: any = {};
    if (message.Segment) {
      obj.Segment = message.Segment.map((e) => e ? Segment.toJSON(e) : undefined);
    } else {
      obj.Segment = [];
    }
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageResponse.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchAllSegmentResponse>, I>>(object: I): MsgFetchAllSegmentResponse {
    const message = createBaseMsgFetchAllSegmentResponse();
    message.Segment = object.Segment?.map((e) => Segment.fromPartial(e)) || [];
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageResponse.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

function createBaseMsgFetchGetWalletHistory(): MsgFetchGetWalletHistory {
  return { creator: "", id: "" };
}

export const MsgFetchGetWalletHistory = {
  encode(message: MsgFetchGetWalletHistory, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchGetWalletHistory {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchGetWalletHistory();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchGetWalletHistory {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
    };
  },

  toJSON(message: MsgFetchGetWalletHistory): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchGetWalletHistory>, I>>(object: I): MsgFetchGetWalletHistory {
    const message = createBaseMsgFetchGetWalletHistory();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgFetchGetWalletHistoryResponse(): MsgFetchGetWalletHistoryResponse {
  return { WalletHistory: undefined };
}

export const MsgFetchGetWalletHistoryResponse = {
  encode(message: MsgFetchGetWalletHistoryResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.WalletHistory !== undefined) {
      WalletHistory.encode(message.WalletHistory, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchGetWalletHistoryResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchGetWalletHistoryResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.WalletHistory = WalletHistory.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchGetWalletHistoryResponse {
    return { WalletHistory: isSet(object.WalletHistory) ? WalletHistory.fromJSON(object.WalletHistory) : undefined };
  },

  toJSON(message: MsgFetchGetWalletHistoryResponse): unknown {
    const obj: any = {};
    message.WalletHistory !== undefined
      && (obj.WalletHistory = message.WalletHistory ? WalletHistory.toJSON(message.WalletHistory) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchGetWalletHistoryResponse>, I>>(
    object: I,
  ): MsgFetchGetWalletHistoryResponse {
    const message = createBaseMsgFetchGetWalletHistoryResponse();
    message.WalletHistory = (object.WalletHistory !== undefined && object.WalletHistory !== null)
      ? WalletHistory.fromPartial(object.WalletHistory)
      : undefined;
    return message;
  },
};

function createBaseMsgFetchAllWalletHistory(): MsgFetchAllWalletHistory {
  return { creator: "", pagination: undefined };
}

export const MsgFetchAllWalletHistory = {
  encode(message: MsgFetchAllWalletHistory, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.pagination !== undefined) {
      PageRequest.encode(message.pagination, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchAllWalletHistory {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchAllWalletHistory();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.pagination = PageRequest.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllWalletHistory {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      pagination: isSet(object.pagination) ? PageRequest.fromJSON(object.pagination) : undefined,
    };
  },

  toJSON(message: MsgFetchAllWalletHistory): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageRequest.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchAllWalletHistory>, I>>(object: I): MsgFetchAllWalletHistory {
    const message = createBaseMsgFetchAllWalletHistory();
    message.creator = object.creator ?? "";
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageRequest.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

function createBaseMsgFetchAllWalletHistoryResponse(): MsgFetchAllWalletHistoryResponse {
  return { WalletHistory: [], pagination: undefined };
}

export const MsgFetchAllWalletHistoryResponse = {
  encode(message: MsgFetchAllWalletHistoryResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.WalletHistory) {
      WalletHistory.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.pagination !== undefined) {
      PageResponse.encode(message.pagination, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchAllWalletHistoryResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchAllWalletHistoryResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.WalletHistory.push(WalletHistory.decode(reader, reader.uint32()));
          break;
        case 2:
          message.pagination = PageResponse.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllWalletHistoryResponse {
    return {
      WalletHistory: Array.isArray(object?.WalletHistory)
        ? object.WalletHistory.map((e: any) => WalletHistory.fromJSON(e))
        : [],
      pagination: isSet(object.pagination) ? PageResponse.fromJSON(object.pagination) : undefined,
    };
  },

  toJSON(message: MsgFetchAllWalletHistoryResponse): unknown {
    const obj: any = {};
    if (message.WalletHistory) {
      obj.WalletHistory = message.WalletHistory.map((e) => e ? WalletHistory.toJSON(e) : undefined);
    } else {
      obj.WalletHistory = [];
    }
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageResponse.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchAllWalletHistoryResponse>, I>>(
    object: I,
  ): MsgFetchAllWalletHistoryResponse {
    const message = createBaseMsgFetchAllWalletHistoryResponse();
    message.WalletHistory = object.WalletHistory?.map((e) => WalletHistory.fromPartial(e)) || [];
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageResponse.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

function createBaseMsgFetchGetWallet(): MsgFetchGetWallet {
  return { creator: "", id: "" };
}

export const MsgFetchGetWallet = {
  encode(message: MsgFetchGetWallet, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchGetWallet {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchGetWallet();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchGetWallet {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
    };
  },

  toJSON(message: MsgFetchGetWallet): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchGetWallet>, I>>(object: I): MsgFetchGetWallet {
    const message = createBaseMsgFetchGetWallet();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgFetchGetWalletResponse(): MsgFetchGetWalletResponse {
  return { Wallet: undefined };
}

export const MsgFetchGetWalletResponse = {
  encode(message: MsgFetchGetWalletResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.Wallet !== undefined) {
      Wallet.encode(message.Wallet, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchGetWalletResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchGetWalletResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.Wallet = Wallet.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchGetWalletResponse {
    return { Wallet: isSet(object.Wallet) ? Wallet.fromJSON(object.Wallet) : undefined };
  },

  toJSON(message: MsgFetchGetWalletResponse): unknown {
    const obj: any = {};
    message.Wallet !== undefined && (obj.Wallet = message.Wallet ? Wallet.toJSON(message.Wallet) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchGetWalletResponse>, I>>(object: I): MsgFetchGetWalletResponse {
    const message = createBaseMsgFetchGetWalletResponse();
    message.Wallet = (object.Wallet !== undefined && object.Wallet !== null)
      ? Wallet.fromPartial(object.Wallet)
      : undefined;
    return message;
  },
};

function createBaseMsgFetchAllWallet(): MsgFetchAllWallet {
  return { creator: "", pagination: undefined };
}

export const MsgFetchAllWallet = {
  encode(message: MsgFetchAllWallet, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.pagination !== undefined) {
      PageRequest.encode(message.pagination, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchAllWallet {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchAllWallet();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.pagination = PageRequest.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllWallet {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      pagination: isSet(object.pagination) ? PageRequest.fromJSON(object.pagination) : undefined,
    };
  },

  toJSON(message: MsgFetchAllWallet): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageRequest.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchAllWallet>, I>>(object: I): MsgFetchAllWallet {
    const message = createBaseMsgFetchAllWallet();
    message.creator = object.creator ?? "";
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageRequest.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

function createBaseMsgFetchAllWalletResponse(): MsgFetchAllWalletResponse {
  return { Wallet: [], pagination: undefined };
}

export const MsgFetchAllWalletResponse = {
  encode(message: MsgFetchAllWalletResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.Wallet) {
      Wallet.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.pagination !== undefined) {
      PageResponse.encode(message.pagination, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchAllWalletResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchAllWalletResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.Wallet.push(Wallet.decode(reader, reader.uint32()));
          break;
        case 2:
          message.pagination = PageResponse.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllWalletResponse {
    return {
      Wallet: Array.isArray(object?.Wallet) ? object.Wallet.map((e: any) => Wallet.fromJSON(e)) : [],
      pagination: isSet(object.pagination) ? PageResponse.fromJSON(object.pagination) : undefined,
    };
  },

  toJSON(message: MsgFetchAllWalletResponse): unknown {
    const obj: any = {};
    if (message.Wallet) {
      obj.Wallet = message.Wallet.map((e) => e ? Wallet.toJSON(e) : undefined);
    } else {
      obj.Wallet = [];
    }
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageResponse.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchAllWalletResponse>, I>>(object: I): MsgFetchAllWalletResponse {
    const message = createBaseMsgFetchAllWalletResponse();
    message.Wallet = object.Wallet?.map((e) => Wallet.fromPartial(e)) || [];
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageResponse.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

/** Msg defines the Msg service. */
export interface Msg {
  /** this line is used by starport scaffolding # proto/tx/rpc */
  CreateSegment(request: MsgCreateSegment): Promise<MsgIdResponse>;
  CreateSegmentWithId(request: MsgCreateSegmentWithId): Promise<MsgIdResponse>;
  UpdateSegment(request: MsgUpdateSegment): Promise<MsgEmptyResponse>;
  CreateWallet(request: MsgCreateWallet): Promise<MsgIdResponse>;
  CreateWalletWithId(request: MsgCreateWalletWithId): Promise<MsgIdResponse>;
  UpdateWallet(request: MsgUpdateWallet): Promise<MsgEmptyResponse>;
  AssignCosmosAddressToWallet(request: MsgAssignCosmosAddressToWallet): Promise<MsgEmptyResponse>;
  CreateTokenRef(request: MsgCreateTokenRef): Promise<MsgIdResponse>;
  MoveTokenToSegment(request: MsgMoveTokenToSegment): Promise<MsgEmptyResponse>;
  MoveTokenToWallet(request: MsgMoveTokenToWallet): Promise<MsgEmptyResponse>;
  RemoveTokenRefFromSegment(request: MsgRemoveTokenRefFromSegment): Promise<MsgEmptyResponse>;
  /** Query */
  FetchTokenHistoryGlobal(request: MsgFetchGetTokenHistoryGlobal): Promise<MsgFetchGetTokenHistoryGlobalResponse>;
  FetchTokenHistoryGlobalAll(request: MsgFetchAllTokenHistoryGlobal): Promise<MsgFetchAllTokenHistoryGlobalResponse>;
  FetchSegmentHistory(request: MsgFetchGetSegmentHistory): Promise<MsgFetchGetSegmentHistoryResponse>;
  FetchSegmentHistoryAll(request: MsgFetchAllSegmentHistory): Promise<MsgFetchAllSegmentHistoryResponse>;
  FetchSegment(request: MsgFetchGetSegment): Promise<MsgFetchGetSegmentResponse>;
  FetchSegmentAll(request: MsgFetchAllSegment): Promise<MsgFetchAllSegmentResponse>;
  FetchWalletHistory(request: MsgFetchGetWalletHistory): Promise<MsgFetchGetWalletHistoryResponse>;
  FetchWalletHistoryAll(request: MsgFetchAllWalletHistory): Promise<MsgFetchAllWalletHistoryResponse>;
  FetchWallet(request: MsgFetchGetWallet): Promise<MsgFetchGetWalletResponse>;
  FetchWalletAll(request: MsgFetchAllWallet): Promise<MsgFetchAllWalletResponse>;
}

export class MsgClientImpl implements Msg {
  private readonly rpc: Rpc;
  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.CreateSegment = this.CreateSegment.bind(this);
    this.CreateSegmentWithId = this.CreateSegmentWithId.bind(this);
    this.UpdateSegment = this.UpdateSegment.bind(this);
    this.CreateWallet = this.CreateWallet.bind(this);
    this.CreateWalletWithId = this.CreateWalletWithId.bind(this);
    this.UpdateWallet = this.UpdateWallet.bind(this);
    this.AssignCosmosAddressToWallet = this.AssignCosmosAddressToWallet.bind(this);
    this.CreateTokenRef = this.CreateTokenRef.bind(this);
    this.MoveTokenToSegment = this.MoveTokenToSegment.bind(this);
    this.MoveTokenToWallet = this.MoveTokenToWallet.bind(this);
    this.RemoveTokenRefFromSegment = this.RemoveTokenRefFromSegment.bind(this);
    this.FetchTokenHistoryGlobal = this.FetchTokenHistoryGlobal.bind(this);
    this.FetchTokenHistoryGlobalAll = this.FetchTokenHistoryGlobalAll.bind(this);
    this.FetchSegmentHistory = this.FetchSegmentHistory.bind(this);
    this.FetchSegmentHistoryAll = this.FetchSegmentHistoryAll.bind(this);
    this.FetchSegment = this.FetchSegment.bind(this);
    this.FetchSegmentAll = this.FetchSegmentAll.bind(this);
    this.FetchWalletHistory = this.FetchWalletHistory.bind(this);
    this.FetchWalletHistoryAll = this.FetchWalletHistoryAll.bind(this);
    this.FetchWallet = this.FetchWallet.bind(this);
    this.FetchWalletAll = this.FetchWalletAll.bind(this);
  }
  CreateSegment(request: MsgCreateSegment): Promise<MsgIdResponse> {
    const data = MsgCreateSegment.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "CreateSegment",
      data,
    );
    return promise.then((data) => MsgIdResponse.decode(new _m0.Reader(data)));
  }

  CreateSegmentWithId(request: MsgCreateSegmentWithId): Promise<MsgIdResponse> {
    const data = MsgCreateSegmentWithId.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "CreateSegmentWithId",
      data,
    );
    return promise.then((data) => MsgIdResponse.decode(new _m0.Reader(data)));
  }

  UpdateSegment(request: MsgUpdateSegment): Promise<MsgEmptyResponse> {
    const data = MsgUpdateSegment.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "UpdateSegment",
      data,
    );
    return promise.then((data) => MsgEmptyResponse.decode(new _m0.Reader(data)));
  }

  CreateWallet(request: MsgCreateWallet): Promise<MsgIdResponse> {
    const data = MsgCreateWallet.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "CreateWallet",
      data,
    );
    return promise.then((data) => MsgIdResponse.decode(new _m0.Reader(data)));
  }

  CreateWalletWithId(request: MsgCreateWalletWithId): Promise<MsgIdResponse> {
    const data = MsgCreateWalletWithId.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "CreateWalletWithId",
      data,
    );
    return promise.then((data) => MsgIdResponse.decode(new _m0.Reader(data)));
  }

  UpdateWallet(request: MsgUpdateWallet): Promise<MsgEmptyResponse> {
    const data = MsgUpdateWallet.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "UpdateWallet",
      data,
    );
    return promise.then((data) => MsgEmptyResponse.decode(new _m0.Reader(data)));
  }

  AssignCosmosAddressToWallet(request: MsgAssignCosmosAddressToWallet): Promise<MsgEmptyResponse> {
    const data = MsgAssignCosmosAddressToWallet.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "AssignCosmosAddressToWallet",
      data,
    );
    return promise.then((data) => MsgEmptyResponse.decode(new _m0.Reader(data)));
  }

  CreateTokenRef(request: MsgCreateTokenRef): Promise<MsgIdResponse> {
    const data = MsgCreateTokenRef.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "CreateTokenRef",
      data,
    );
    return promise.then((data) => MsgIdResponse.decode(new _m0.Reader(data)));
  }

  MoveTokenToSegment(request: MsgMoveTokenToSegment): Promise<MsgEmptyResponse> {
    const data = MsgMoveTokenToSegment.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "MoveTokenToSegment",
      data,
    );
    return promise.then((data) => MsgEmptyResponse.decode(new _m0.Reader(data)));
  }

  MoveTokenToWallet(request: MsgMoveTokenToWallet): Promise<MsgEmptyResponse> {
    const data = MsgMoveTokenToWallet.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "MoveTokenToWallet",
      data,
    );
    return promise.then((data) => MsgEmptyResponse.decode(new _m0.Reader(data)));
  }

  RemoveTokenRefFromSegment(request: MsgRemoveTokenRefFromSegment): Promise<MsgEmptyResponse> {
    const data = MsgRemoveTokenRefFromSegment.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "RemoveTokenRefFromSegment",
      data,
    );
    return promise.then((data) => MsgEmptyResponse.decode(new _m0.Reader(data)));
  }

  FetchTokenHistoryGlobal(request: MsgFetchGetTokenHistoryGlobal): Promise<MsgFetchGetTokenHistoryGlobalResponse> {
    const data = MsgFetchGetTokenHistoryGlobal.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "FetchTokenHistoryGlobal",
      data,
    );
    return promise.then((data) => MsgFetchGetTokenHistoryGlobalResponse.decode(new _m0.Reader(data)));
  }

  FetchTokenHistoryGlobalAll(request: MsgFetchAllTokenHistoryGlobal): Promise<MsgFetchAllTokenHistoryGlobalResponse> {
    const data = MsgFetchAllTokenHistoryGlobal.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "FetchTokenHistoryGlobalAll",
      data,
    );
    return promise.then((data) => MsgFetchAllTokenHistoryGlobalResponse.decode(new _m0.Reader(data)));
  }

  FetchSegmentHistory(request: MsgFetchGetSegmentHistory): Promise<MsgFetchGetSegmentHistoryResponse> {
    const data = MsgFetchGetSegmentHistory.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "FetchSegmentHistory",
      data,
    );
    return promise.then((data) => MsgFetchGetSegmentHistoryResponse.decode(new _m0.Reader(data)));
  }

  FetchSegmentHistoryAll(request: MsgFetchAllSegmentHistory): Promise<MsgFetchAllSegmentHistoryResponse> {
    const data = MsgFetchAllSegmentHistory.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "FetchSegmentHistoryAll",
      data,
    );
    return promise.then((data) => MsgFetchAllSegmentHistoryResponse.decode(new _m0.Reader(data)));
  }

  FetchSegment(request: MsgFetchGetSegment): Promise<MsgFetchGetSegmentResponse> {
    const data = MsgFetchGetSegment.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "FetchSegment",
      data,
    );
    return promise.then((data) => MsgFetchGetSegmentResponse.decode(new _m0.Reader(data)));
  }

  FetchSegmentAll(request: MsgFetchAllSegment): Promise<MsgFetchAllSegmentResponse> {
    const data = MsgFetchAllSegment.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "FetchSegmentAll",
      data,
    );
    return promise.then((data) => MsgFetchAllSegmentResponse.decode(new _m0.Reader(data)));
  }

  FetchWalletHistory(request: MsgFetchGetWalletHistory): Promise<MsgFetchGetWalletHistoryResponse> {
    const data = MsgFetchGetWalletHistory.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "FetchWalletHistory",
      data,
    );
    return promise.then((data) => MsgFetchGetWalletHistoryResponse.decode(new _m0.Reader(data)));
  }

  FetchWalletHistoryAll(request: MsgFetchAllWalletHistory): Promise<MsgFetchAllWalletHistoryResponse> {
    const data = MsgFetchAllWalletHistory.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "FetchWalletHistoryAll",
      data,
    );
    return promise.then((data) => MsgFetchAllWalletHistoryResponse.decode(new _m0.Reader(data)));
  }

  FetchWallet(request: MsgFetchGetWallet): Promise<MsgFetchGetWalletResponse> {
    const data = MsgFetchGetWallet.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "FetchWallet",
      data,
    );
    return promise.then((data) => MsgFetchGetWalletResponse.decode(new _m0.Reader(data)));
  }

  FetchWalletAll(request: MsgFetchAllWallet): Promise<MsgFetchAllWalletResponse> {
    const data = MsgFetchAllWallet.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet.Msg",
      "FetchWalletAll",
      data,
    );
    return promise.then((data) => MsgFetchAllWalletResponse.decode(new _m0.Reader(data)));
  }
}

interface Rpc {
  request(service: string, method: string, data: Uint8Array): Promise<Uint8Array>;
}

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

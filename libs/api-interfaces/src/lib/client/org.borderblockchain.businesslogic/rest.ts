/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export interface BorderblockchainbusinesslogicAddress {
  streetAndNumber?: string;
  postcode?: string;
  city?: string;
  countryCode?: string;
}

export interface BorderblockchainbusinesslogicCompany {
  address?: BorderblockchainbusinesslogicAddress;
  customsId?: string;
  name?: string;
  subsidiaryNumber?: string;
  identificationNumber?: string;
}

export interface BorderblockchainbusinesslogicCustomsOffice {
  address?: BorderblockchainbusinesslogicAddress;
  customsOfficeCode?: string;
  customsOfficeName?: string;
}

export interface BorderblockchainbusinesslogicDocument {
  type?: string;
  referenceNumber?: string;
  complement?: string;
  detail?: string;
}

export interface BorderblockchainbusinesslogicGoodsItem {
  documents?: BorderblockchainbusinesslogicDocument[];
  countryOfOrigin?: string;

  /** @format int64 */
  sequenceNumber?: string;
  descriptionOfGoods?: string;
  harmonizedSystemSubheadingCode?: string;
  localClassificationCode?: string;
  grossMass?: string;
  netMass?: string;

  /** @format int64 */
  numberOfPackages?: string;
  typeOfPackages?: string;
  marksNumbers?: string;
  containerId?: string;
  consigneeOrderNumber?: string;

  /** @format int64 */
  valueAtBorderExport?: string;
  valueAtBorderExportCurrency?: string;

  /** @format int64 */
  amountInvoiced?: string;
  amountInvoicedCurrency?: string;

  /** @format int64 */
  dangerousGoodsCode?: string;
}

export type BorderblockchainbusinesslogicMsgEmptyResponse = object;

export interface BorderblockchainbusinesslogicTransport {
  /** @format int64 */
  modeOfTransport?: string;

  /** @format int64 */
  typeOfIdentification?: string;
  identity?: string;
  nationality?: string;

  /** @format int64 */
  transportCost?: string;
  transportCostCurrency?: string;
  transportOrderNumber?: string;
}

export interface BorderblockchainexporttokenAddress {
  streetAndNumber?: string;
  postcode?: string;
  city?: string;
  countryCode?: string;
}

export interface BorderblockchainexporttokenCompany {
  address?: BorderblockchainexporttokenAddress;
  customsId?: string;
  name?: string;
  subsidiaryNumber?: string;
  identificationNumber?: string;
}

export interface BorderblockchainexporttokenCustomsOffice {
  address?: BorderblockchainexporttokenAddress;
  customsOfficeCode?: string;
  customsOfficeName?: string;
}

export interface BorderblockchainexporttokenDocument {
  type?: string;
  referenceNumber?: string;
  complement?: string;
  detail?: string;
}

export interface BorderblockchainexporttokenGoodsItem {
  documents?: BorderblockchainexporttokenDocument[];
  countryOfOrigin?: string;

  /** @format int64 */
  sequenceNumber?: string;
  descriptionOfGoods?: string;
  harmonizedSystemSubheadingCode?: string;
  localClassificationCode?: string;
  grossMass?: string;
  netMass?: string;

  /** @format int64 */
  numberOfPackages?: string;
  typeOfPackages?: string;
  marksNumbers?: string;
  containerId?: string;
  consigneeOrderNumber?: string;

  /** @format int64 */
  valueAtBorderExport?: string;
  valueAtBorderExportCurrency?: string;

  /** @format int64 */
  amountInvoiced?: string;
  amountInvoicedCurrency?: string;

  /** @format int64 */
  dangerousGoodsCode?: string;
}

export interface BorderblockchainexporttokenTransport {
  /** @format int64 */
  modeOfTransport?: string;

  /** @format int64 */
  typeOfIdentification?: string;
  identity?: string;
  nationality?: string;

  /** @format int64 */
  transportCost?: string;
  transportCostCurrency?: string;
  transportOrderNumber?: string;
}

export interface BorderblockchainimporttokenAddress {
  streetAndNumber?: string;
  postcode?: string;
  city?: string;
  countryCode?: string;
}

export interface BorderblockchainimporttokenCompany {
  address?: BorderblockchainimporttokenAddress;
  customsId?: string;
  name?: string;
  subsidiaryNumber?: string;
  identificationNumber?: string;
}

export interface BorderblockchainimporttokenCustomsOffice {
  address?: BorderblockchainimporttokenAddress;
  customsOfficeCode?: string;
  customsOfficeName?: string;
}

export interface BorderblockchainimporttokenDocument {
  type?: string;
  referenceNumber?: string;
  complement?: string;
  detail?: string;
}

export interface BorderblockchainimporttokenGoodsItem {
  documents?: BorderblockchainimporttokenDocument[];
  countryOfOrigin?: string;

  /** @format int64 */
  sequenceNumber?: string;
  descriptionOfGoods?: string;
  harmonizedSystemSubheadingCode?: string;
  localClassificationCode?: string;
  grossMass?: string;
  netMass?: string;

  /** @format int64 */
  numberOfPackages?: string;
  typeOfPackages?: string;
  marksNumbers?: string;
  containerId?: string;
  consigneeOrderNumber?: string;

  /** @format int64 */
  valueAtBorder?: string;
  valueAtBorderCurrency?: string;

  /** @format int64 */
  amountInvoiced?: string;
  amountInvoicedCurrency?: string;

  /** @format int64 */
  dangerousGoodsCode?: string;
}

export interface BorderblockchainimporttokenTransport {
  /** @format int64 */
  modeOfTransport?: string;

  /** @format int64 */
  typeOfIdentification?: string;
  identity?: string;
  nationality?: string;

  /** @format int64 */
  transportCosts?: string;
  transportCostsCurrency?: string;
  transportOrderNumber?: string;
}

export interface BusinesslogicCompanyDriver {
  address?: BorderblockchainbusinesslogicAddress;
  name?: string;
}

export interface BusinesslogicExportTokenDriver {
  creator?: string;
  id?: string;
  exporter?: BusinesslogicCompanyDriver;
  declarant?: BusinesslogicCompanyDriver;
  representative?: BusinesslogicCompanyDriver;
  consignee?: BusinesslogicCompanyDriver;
  customOfficeOfExit?: BorderblockchainbusinesslogicCustomsOffice;
  goodsItems?: BusinesslogicGoodsItemDriver[];
  transportToBorder?: BusinesslogicTransportDriver;
  transportAtBorder?: BusinesslogicTransportDriver;
  exportId?: string;
  uniqueConsignmentReference?: string;
  destinationCountry?: string;
  exportCountry?: string;
  itinerary?: string;
  totalGrossMass?: string;

  /** @format int64 */
  goodsItemQuantity?: string;

  /** @format int64 */
  totalPackagesQuantity?: string;
}

export interface BusinesslogicExportTokenExporter {
  creator?: string;
  id?: string;
  exporter?: BorderblockchainbusinesslogicCompany;
  declarant?: BorderblockchainbusinesslogicCompany;
  representative?: BorderblockchainbusinesslogicCompany;
  consignee?: BorderblockchainbusinesslogicCompany;
  customsOfficeOfExport?: BorderblockchainbusinesslogicCustomsOffice;
  customOfficeOfExit?: BorderblockchainbusinesslogicCustomsOffice;
  goodsItems?: BorderblockchainbusinesslogicGoodsItem[];
  transportToBorder?: BorderblockchainbusinesslogicTransport;
  transportAtBorder?: BorderblockchainbusinesslogicTransport;
  exportId?: string;
  uniqueConsignmentReference?: string;
  localReferenceNumber?: string;
  destinationCountry?: string;
  exportCountry?: string;
  itinerary?: string;
  releaseDateAndTime?: string;
  incotermCode?: string;
  incotermLocation?: string;
  totalGrossMass?: string;

  /** @format int64 */
  goodsItemQuantity?: string;

  /** @format int64 */
  totalPackagesQuantity?: string;

  /** @format int64 */
  natureOfTransaction?: string;

  /** @format int64 */
  totalAmountInvoiced?: string;
  invoiceCurrency?: string;
}

export interface BusinesslogicExportTokenImporter {
  creator?: string;
  id?: string;
  exporter?: BorderblockchainbusinesslogicCompany;
  declarant?: BorderblockchainbusinesslogicCompany;
  representative?: BorderblockchainbusinesslogicCompany;
  consignee?: BorderblockchainbusinesslogicCompany;
  customsOfficeOfExport?: BorderblockchainbusinesslogicCustomsOffice;
  customOfficeOfExit?: BorderblockchainbusinesslogicCustomsOffice;
  goodsItems?: BorderblockchainbusinesslogicGoodsItem[];
  transportToBorder?: BorderblockchainbusinesslogicTransport;
  transportAtBorder?: BorderblockchainbusinesslogicTransport;
  exportId?: string;
  uniqueConsignmentReference?: string;
  destinationCountry?: string;
  exportCountry?: string;
  itinerary?: string;
  releaseDateAndTime?: string;
  incotermCode?: string;
  incotermLocation?: string;
  totalGrossMass?: string;

  /** @format int64 */
  goodsItemQuantity?: string;

  /** @format int64 */
  totalPackagesQuantity?: string;

  /** @format int64 */
  natureOfTransaction?: string;

  /** @format int64 */
  totalAmountInvoiced?: string;
  invoiceCurrency?: string;
}

export interface BusinesslogicGoodsItemDriver {
  documents?: BorderblockchainbusinesslogicDocument[];
  countryOfOrigin?: string;

  /** @format int64 */
  sequenceNumber?: string;
  descriptionOfGoods?: string;
  harmonizedSystemSubheadingCode?: string;
  localClassificationCode?: string;
  grossMass?: string;
  netMass?: string;

  /** @format int64 */
  numberOfPackages?: string;
  typeOfPackages?: string;
  marksNumbers?: string;
  containerId?: string;
  consigneeOrderNumber?: string;

  /** @format int64 */
  dangerousGoodsCode?: string;
}

/**
 * We might want to return a Response in the future.
 */
export type BusinesslogicMsgCreateExportEventResponse = object;

export interface BusinesslogicMsgCreateExportTokenResponse {
  exportToken?: ExporttokenExportToken;
  events?: ExporttokenExportEvent[];
}

/**
 * We might want to return a Response in the future.
 */
export type BusinesslogicMsgCreateImportEventResponse = object;

export interface BusinesslogicMsgCreateImportTokenResponse {
  importToken?: ImporttokenImportToken;
  events?: ImporttokenImportEvent[];
}

export interface BusinesslogicMsgEventStatusResponseExport {
  exportToken?: ExporttokenExportToken;
  events?: ExporttokenExportEvent[];
}

export interface BusinesslogicMsgEventStatusResponseImport {
  importToken?: ImporttokenImportToken;
  events?: ImporttokenImportEvent[];
}

export interface BusinesslogicMsgFetchExportTokenAllDriverResponse {
  tokens?: BusinesslogicMsgFetchExportTokenDriverResponse[];
}

export interface BusinesslogicMsgFetchExportTokenAllExporterResponse {
  tokens?: BusinesslogicMsgFetchExportTokenExporterResponse[];
}

export interface BusinesslogicMsgFetchExportTokenAllImporterResponse {
  tokens?: BusinesslogicMsgFetchExportTokenImporterResponse[];
}

export interface BusinesslogicMsgFetchExportTokenDriverResponse {
  exportToken?: BusinesslogicExportTokenDriver;
  events?: ExporttokenExportEvent[];
}

export interface BusinesslogicMsgFetchExportTokenExporterResponse {
  exportToken?: BusinesslogicExportTokenExporter;
  events?: ExporttokenExportEvent[];
}

export interface BusinesslogicMsgFetchExportTokenImporterResponse {
  exportToken?: BusinesslogicExportTokenImporter;
  events?: ExporttokenExportEvent[];
}

export interface BusinesslogicMsgFetchExportTokenResponse {
  exportToken?: ExporttokenExportToken;
  events?: ExporttokenExportEvent[];
}

export interface BusinesslogicMsgFetchImportTokenAllResponse {
  tokens?: BusinesslogicMsgCreateImportTokenResponse[];
}

export interface BusinesslogicMsgFetchImportTokenResponse {
  importToken?: ImporttokenImportToken;
  events?: ImporttokenImportEvent[];
}

export interface BusinesslogicMsgFetchTokenHistoryResponse {
  TokenHistory?: TokenTokenHistory;
}

export interface BusinesslogicMsgForwardExportTokenResponse {
  exportToken?: ExporttokenExportToken;
  events?: ExporttokenExportEvent[];
}

export interface BusinesslogicMsgUpdateExportTokenResponse {
  exportToken?: ExporttokenExportToken;
  events?: ExporttokenExportEvent[];
}

export interface BusinesslogicMsgUpdateImportTokenResponse {
  importToken?: ImporttokenImportToken;
  events?: ImporttokenImportEvent[];
}

export interface BusinesslogicTransportDriver {
  /** @format int64 */
  modeOfTransport?: string;

  /** @format int64 */
  typeOfIdentification?: string;
  identity?: string;
  nationality?: string;
}

/**
* Creator: Cosmos Address
Status : Status enum from the Import Status Process.
Message: More Information about the Status Transaction.
Type : Type enum of all accepted state transitions/Transaction Type.
*/
export interface ExporttokenExportEvent {
  creator?: string;
  status?: string;
  message?: string;
  eventType?: string;
  timestamp?: string;
}

export interface ExporttokenExportToken {
  creator?: string;
  id?: string;
  exporter?: BorderblockchainexporttokenCompany;
  declarant?: BorderblockchainexporttokenCompany;
  representative?: BorderblockchainexporttokenCompany;
  consignee?: BorderblockchainexporttokenCompany;
  customsOfficeOfExport?: BorderblockchainexporttokenCustomsOffice;
  customOfficeOfExit?: BorderblockchainexporttokenCustomsOffice;
  goodsItems?: BorderblockchainexporttokenGoodsItem[];
  transportToBorder?: BorderblockchainexporttokenTransport;
  transportAtBorder?: BorderblockchainexporttokenTransport;
  exportId?: string;
  uniqueConsignmentReference?: string;
  localReferenceNumber?: string;
  destinationCountry?: string;
  exportCountry?: string;
  itinerary?: string;
  releaseDateAndTime?: string;
  incotermCode?: string;
  incotermLocation?: string;
  totalGrossMass?: string;

  /** @format int64 */
  goodsItemQuantity?: string;

  /** @format int64 */
  totalPackagesQuantity?: string;

  /** @format int64 */
  natureOfTransaction?: string;

  /** @format int64 */
  totalAmountInvoiced?: string;
  invoiceCurrency?: string;
}

/**
* Creator: Cosmos Address
Status : Status enum from the Import Status Process.
Message: More Information about the Status Transaction.
Type : Type enum of all accepted state transitions/Transaction Type.
*/
export interface ImporttokenImportEvent {
  creator?: string;
  status?: string;
  message?: string;
  eventType?: string;
  timestamp?: string;
}

export interface ImporttokenImportToken {
  creator?: string;
  id?: string;
  consignor?: BorderblockchainimporttokenCompany;
  exporter?: BorderblockchainimporttokenCompany;
  consignee?: BorderblockchainimporttokenCompany;
  declarant?: BorderblockchainimporttokenCompany;
  customOfficeOfExit?: BorderblockchainimporttokenCustomsOffice;
  customOfficeOfEntry?: BorderblockchainimporttokenCustomsOffice;
  customOfficeOfImport?: BorderblockchainimporttokenCustomsOffice;
  goodsItems?: BorderblockchainimporttokenGoodsItem[];
  transportAtBorder?: BorderblockchainimporttokenTransport;
  transportFromBorder?: BorderblockchainimporttokenTransport;
  importId?: string;
  uniqueConsignmentReference?: string;
  localReferenceNumber?: string;
  destinationCountry?: string;
  exportCountry?: string;
  itinerary?: string;
  incotermCode?: string;
  incotermLocation?: string;
  totalGrossMass?: string;

  /** @format int64 */
  goodsItemQuantity?: string;

  /** @format int64 */
  totalPackagesQuantity?: string;
  releaseDateAndTime?: string;

  /** @format int64 */
  natureOfTransaction?: string;

  /** @format int64 */
  totalAmountInvoiced?: string;
  invoiceCurrency?: string;
  relatedExportToken?: string;
}

export type ModulestokenwalletMsgEmptyResponse = object;

export interface ModulestokenwalletMsgIdResponse {
  id?: string;
}

export interface ProtobufAny {
  '@type'?: string;
}

export interface RpcStatus {
  /** @format int32 */
  code?: number;
  message?: string;
  details?: ProtobufAny[];
}

export interface TokenInfo {
  data?: string;
}

export interface TokenToken {
  creator?: string;
  id?: string;
  tokenType?: string;
  timestamp?: string;
  changeMessage?: string;
  valid?: boolean;
  info?: TokenInfo;
  segmentId?: string;
}

export interface TokenTokenHistory {
  creator?: string;
  id?: string;
  history?: TokenToken[];
}

export interface TokenwalletMsgFetchAllSegmentResponse {
  Segment?: TokenwalletSegment[];

  /**
   * PageResponse is to be embedded in gRPC response messages where the
   * corresponding request message has used PageRequest.
   *
   *  message SomeResponse {
   *          repeated Bar results = 1;
   *          PageResponse page = 2;
   *  }
   */
  pagination?: V1Beta1PageResponse;
}

export interface TokenwalletMsgFetchAllWalletResponse {
  Wallet?: TokenwalletWallet[];

  /**
   * PageResponse is to be embedded in gRPC response messages where the
   * corresponding request message has used PageRequest.
   *
   *  message SomeResponse {
   *          repeated Bar results = 1;
   *          PageResponse page = 2;
   *  }
   */
  pagination?: V1Beta1PageResponse;
}

export interface TokenwalletMsgFetchGetSegmentHistoryResponse {
  SegmentHistory?: TokenwalletSegmentHistory;
}

export interface TokenwalletMsgFetchGetSegmentResponse {
  Segment?: TokenwalletSegment;
}

export interface TokenwalletMsgFetchGetWalletHistoryResponse {
  WalletHistory?: TokenwalletWalletHistory;
}

export interface TokenwalletMsgFetchGetWalletResponse {
  Wallet?: TokenwalletWallet;
}

export interface TokenwalletSegment {
  creator?: string;
  id?: string;
  name?: string;
  timestamp?: string;
  info?: string;
  walletId?: string;
  tokenRefs?: TokenwalletTokenRef[];
}

export interface TokenwalletSegmentHistory {
  creator?: string;
  id?: string;
  history?: TokenwalletSegment[];
}

export interface TokenwalletTokenRef {
  id?: string;
  moduleRef?: string;
  valid?: boolean;
}

export interface TokenwalletWallet {
  creator?: string;
  id?: string;
  name?: string;
  timestamp?: string;
  segmentIds?: string[];
  walletAccounts?: TokenwalletWalletAccount[];
}

export interface TokenwalletWalletAccount {
  address?: string;
  active?: boolean;
}

export interface TokenwalletWalletHistory {
  creator?: string;
  id?: string;
  history?: TokenwalletWallet[];
}

/**
* PageResponse is to be embedded in gRPC response messages where the
corresponding request message has used PageRequest.

 message SomeResponse {
         repeated Bar results = 1;
         PageResponse page = 2;
 }
*/
export interface V1Beta1PageResponse {
  /**
   * next_key is the key to be passed to PageRequest.key to
   * query the next page most efficiently
   * @format byte
   */
  next_key?: string;

  /**
   * total is total number of results available if PageRequest.count_total
   * was set, its value is undefined otherwise
   * @format uint64
   */
  total?: string;
}

import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse, ResponseType } from 'axios';

export type QueryParamsType = Record<string | number, any>;

export interface FullRequestParams extends Omit<AxiosRequestConfig, 'data' | 'params' | 'url' | 'responseType'> {
  /** set parameter to `true` for call `securityWorker` for this request */
  secure?: boolean;
  /** request path */
  path: string;
  /** content type of request body */
  type?: ContentType;
  /** query params */
  query?: QueryParamsType;
  /** format of response (i.e. response.json() -> format: "json") */
  format?: ResponseType;
  /** request body */
  body?: unknown;
}

export type RequestParams = Omit<FullRequestParams, 'body' | 'method' | 'query' | 'path'>;

export interface ApiConfig<SecurityDataType = unknown> extends Omit<AxiosRequestConfig, 'data' | 'cancelToken'> {
  securityWorker?: (
    securityData: SecurityDataType | null
  ) => Promise<AxiosRequestConfig | void> | AxiosRequestConfig | void;
  secure?: boolean;
  format?: ResponseType;
}

export enum ContentType {
  Json = 'application/json',
  FormData = 'multipart/form-data',
  UrlEncoded = 'application/x-www-form-urlencoded',
}

export class HttpClient<SecurityDataType = unknown> {
  public instance: AxiosInstance;
  private securityData: SecurityDataType | null = null;
  private securityWorker?: ApiConfig<SecurityDataType>['securityWorker'];
  private secure?: boolean;
  private format?: ResponseType;

  constructor({ securityWorker, secure, format, ...axiosConfig }: ApiConfig<SecurityDataType> = {}) {
    this.instance = axios.create({ ...axiosConfig, baseURL: axiosConfig.baseURL || '' });
    this.secure = secure;
    this.format = format;
    this.securityWorker = securityWorker;
  }

  public setSecurityData = (data: SecurityDataType | null) => {
    this.securityData = data;
  };

  private mergeRequestParams(params1: AxiosRequestConfig, params2?: AxiosRequestConfig): AxiosRequestConfig {
    return {
      ...this.instance.defaults,
      ...params1,
      ...(params2 || {}),
      headers: {
        ...(this.instance.defaults.headers || {}),
        ...(params1.headers || {}),
        ...((params2 && params2.headers) || {}),
      },
    };
  }

  private createFormData(input: Record<string, unknown>): FormData {
    return Object.keys(input || {}).reduce((formData, key) => {
      const property = input[key];
      formData.append(
        key,
        property instanceof Blob
          ? property
          : typeof property === 'object' && property !== null
          ? JSON.stringify(property)
          : `${property}`
      );
      return formData;
    }, new FormData());
  }

  public request = async <T = any, _E = any>({
    secure,
    path,
    type,
    query,
    format,
    body,
    ...params
  }: FullRequestParams): Promise<AxiosResponse<T>> => {
    const secureParams =
      ((typeof secure === 'boolean' ? secure : this.secure) &&
        this.securityWorker &&
        (await this.securityWorker(this.securityData))) ||
      {};
    const requestParams = this.mergeRequestParams(params, secureParams);
    const responseFormat = (format && this.format) || void 0;

    if (type === ContentType.FormData && body && body !== null && typeof body === 'object') {
      requestParams.headers.common = { Accept: '*/*' };
      requestParams.headers.post = {};
      requestParams.headers.put = {};

      body = this.createFormData(body as Record<string, unknown>);
    }

    return this.instance.request({
      ...requestParams,
      headers: {
        ...(type && type !== ContentType.FormData ? { 'Content-Type': type } : {}),
        ...(requestParams.headers || {}),
      },
      params: query,
      responseType: responseFormat,
      data: body,
      url: path,
    });
  };
}

/**
 * @title businesslogic/ExportSegmentInfo.proto
 * @version version not set
 */
export class Api<SecurityDataType extends unknown> extends HttpClient<SecurityDataType> {}

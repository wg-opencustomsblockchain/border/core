import { GeneratedType } from "@cosmjs/proto-signing";
import { MsgCreateImportToken } from "./types/businesslogic/tx";
import { MsgFetchExportTokenDriver } from "./types/businesslogic/tx";
import { MsgUpdateImportToken } from "./types/businesslogic/tx";
import { MsgFetchImportToken } from "./types/businesslogic/tx";
import { MsgActivateToken } from "./types/businesslogic/tx";
import { MsgCreateTokenCopies } from "./types/businesslogic/tx";
import { MsgFetchExportTokenExporterByExportId } from "./types/businesslogic/tx";
import { MsgFetchGetTokenHistoryGlobal } from "./types/businesslogic/tx";
import { MsgFetchTokensByWalletId } from "./types/businesslogic/tx";
import { MsgUpdateWallet } from "./types/businesslogic/tx";
import { MsgFetchAllWallet } from "./types/businesslogic/tx";
import { MsgEventPresentGoods } from "./types/businesslogic/tx";
import { MsgFetchExportTokenAllExporter } from "./types/businesslogic/tx";
import { MsgCreateSegmentWithId } from "./types/businesslogic/tx";
import { MsgFetchAllTokenHistoryGlobal } from "./types/businesslogic/tx";
import { MsgEventImportRefusal } from "./types/businesslogic/tx";
import { MsgEventAcceptGoodsForTransport } from "./types/businesslogic/tx";
import { MsgFetchExportTokenImporterByExportId } from "./types/businesslogic/tx";
import { MsgDeleteTokenCopies } from "./types/businesslogic/tx";
import { MsgFetchExportTokenAllImporter } from "./types/businesslogic/tx";
import { MsgCreateToken } from "./types/businesslogic/tx";
import { MsgUpdateDocumentTokenMapper } from "./types/businesslogic/tx";
import { MsgUpdateExportToken } from "./types/businesslogic/tx";
import { MsgFetchTokenHistory } from "./types/businesslogic/tx";
import { MsgDeleteDocumentTokenMapper } from "./types/businesslogic/tx";
import { MsgCreateImportEvent } from "./types/businesslogic/tx";
import { MsgFetchExportTokenExporter } from "./types/businesslogic/tx";
import { MsgFetchExportTokenDriverByExportId } from "./types/businesslogic/tx";
import { MsgFetchDocumentHash } from "./types/businesslogic/tx";
import { MsgForwardExportToken } from "./types/businesslogic/tx";
import { MsgEventMoveToArchive } from "./types/businesslogic/tx";
import { MsgFetchExportTokenImporter } from "./types/businesslogic/tx";
import { MsgUpdateToken } from "./types/businesslogic/tx";
import { MsgFetchGetWallet } from "./types/businesslogic/tx";
import { MsgFetchAllWalletHistory } from "./types/businesslogic/tx";
import { MsgFetchSegmentHistory } from "./types/businesslogic/tx";
import { MsgCloneToken } from "./types/businesslogic/tx";
import { MsgCreateWallet } from "./types/businesslogic/tx";
import { MsgEventExportRefusal } from "./types/businesslogic/tx";
import { MsgCreateHashToken } from "./types/businesslogic/tx";
import { MsgEventCertifyExitOfGoods } from "./types/businesslogic/tx";
import { MsgEventExportCheck } from "./types/businesslogic/tx";
import { MsgEventImportSubmit } from "./types/businesslogic/tx";
import { MsgFetchGetWalletHistory } from "./types/businesslogic/tx";
import { MsgEventImportSupplement } from "./types/businesslogic/tx";
import { MsgMoveTokenToWallet } from "./types/businesslogic/tx";
import { MsgRevertToGenesis } from "./types/businesslogic/tx";
import { MsgFetchExportTokenAllDriver } from "./types/businesslogic/tx";
import { MsgFetchImportTokenAll } from "./types/businesslogic/tx";
import { MsgFetchGetSegment } from "./types/businesslogic/tx";
import { MsgCreateExportToken } from "./types/businesslogic/tx";
import { MsgEventImportExport } from "./types/businesslogic/tx";
import { MsgAssignCosmosAddressToWallet } from "./types/businesslogic/tx";
import { MsgCreateExportEvent } from "./types/businesslogic/tx";
import { MsgCreateWalletWithId } from "./types/businesslogic/tx";
import { MsgEventPresentQrCode } from "./types/businesslogic/tx";
import { MsgEventAcceptExport } from "./types/businesslogic/tx";
import { MsgEventCompleteImport } from "./types/businesslogic/tx";
import { MsgFetchAllSegment } from "./types/businesslogic/tx";
import { MsgFetchAllSegmentHistory } from "./types/businesslogic/tx";
import { MsgEventImportRelease } from "./types/businesslogic/tx";
import { MsgDeactivateToken } from "./types/businesslogic/tx";
import { MsgFetchImportTokenByImportId } from "./types/businesslogic/tx";
import { MsgMoveTokenToSegment } from "./types/businesslogic/tx";
import { MsgUpdateTokenCopies } from "./types/businesslogic/tx";
import { MsgCreateSegment } from "./types/businesslogic/tx";
import { MsgCreateDocumentTokenMapper } from "./types/businesslogic/tx";
import { MsgDeleteImportToken } from "./types/businesslogic/tx";
import { MsgEventImportProposal } from "./types/businesslogic/tx";
import { MsgEventAcceptImport } from "./types/businesslogic/tx";
import { MsgUpdateSegment } from "./types/businesslogic/tx";
import { MsgFetchTokensBySegmentId } from "./types/businesslogic/tx";

const msgTypes: Array<[string, GeneratedType]>  = [
    ["/org.borderblockchain.businesslogic.MsgCreateImportToken", MsgCreateImportToken],
    ["/org.borderblockchain.businesslogic.MsgFetchExportTokenDriver", MsgFetchExportTokenDriver],
    ["/org.borderblockchain.businesslogic.MsgUpdateImportToken", MsgUpdateImportToken],
    ["/org.borderblockchain.businesslogic.MsgFetchImportToken", MsgFetchImportToken],
    ["/org.borderblockchain.businesslogic.MsgActivateToken", MsgActivateToken],
    ["/org.borderblockchain.businesslogic.MsgCreateTokenCopies", MsgCreateTokenCopies],
    ["/org.borderblockchain.businesslogic.MsgFetchExportTokenExporterByExportId", MsgFetchExportTokenExporterByExportId],
    ["/org.borderblockchain.businesslogic.MsgFetchGetTokenHistoryGlobal", MsgFetchGetTokenHistoryGlobal],
    ["/org.borderblockchain.businesslogic.MsgFetchTokensByWalletId", MsgFetchTokensByWalletId],
    ["/org.borderblockchain.businesslogic.MsgUpdateWallet", MsgUpdateWallet],
    ["/org.borderblockchain.businesslogic.MsgFetchAllWallet", MsgFetchAllWallet],
    ["/org.borderblockchain.businesslogic.MsgEventPresentGoods", MsgEventPresentGoods],
    ["/org.borderblockchain.businesslogic.MsgFetchExportTokenAllExporter", MsgFetchExportTokenAllExporter],
    ["/org.borderblockchain.businesslogic.MsgCreateSegmentWithId", MsgCreateSegmentWithId],
    ["/org.borderblockchain.businesslogic.MsgFetchAllTokenHistoryGlobal", MsgFetchAllTokenHistoryGlobal],
    ["/org.borderblockchain.businesslogic.MsgEventImportRefusal", MsgEventImportRefusal],
    ["/org.borderblockchain.businesslogic.MsgEventAcceptGoodsForTransport", MsgEventAcceptGoodsForTransport],
    ["/org.borderblockchain.businesslogic.MsgFetchExportTokenImporterByExportId", MsgFetchExportTokenImporterByExportId],
    ["/org.borderblockchain.businesslogic.MsgDeleteTokenCopies", MsgDeleteTokenCopies],
    ["/org.borderblockchain.businesslogic.MsgFetchExportTokenAllImporter", MsgFetchExportTokenAllImporter],
    ["/org.borderblockchain.businesslogic.MsgCreateToken", MsgCreateToken],
    ["/org.borderblockchain.businesslogic.MsgUpdateDocumentTokenMapper", MsgUpdateDocumentTokenMapper],
    ["/org.borderblockchain.businesslogic.MsgUpdateExportToken", MsgUpdateExportToken],
    ["/org.borderblockchain.businesslogic.MsgFetchTokenHistory", MsgFetchTokenHistory],
    ["/org.borderblockchain.businesslogic.MsgDeleteDocumentTokenMapper", MsgDeleteDocumentTokenMapper],
    ["/org.borderblockchain.businesslogic.MsgCreateImportEvent", MsgCreateImportEvent],
    ["/org.borderblockchain.businesslogic.MsgFetchExportTokenExporter", MsgFetchExportTokenExporter],
    ["/org.borderblockchain.businesslogic.MsgFetchExportTokenDriverByExportId", MsgFetchExportTokenDriverByExportId],
    ["/org.borderblockchain.businesslogic.MsgFetchDocumentHash", MsgFetchDocumentHash],
    ["/org.borderblockchain.businesslogic.MsgForwardExportToken", MsgForwardExportToken],
    ["/org.borderblockchain.businesslogic.MsgEventMoveToArchive", MsgEventMoveToArchive],
    ["/org.borderblockchain.businesslogic.MsgFetchExportTokenImporter", MsgFetchExportTokenImporter],
    ["/org.borderblockchain.businesslogic.MsgUpdateToken", MsgUpdateToken],
    ["/org.borderblockchain.businesslogic.MsgFetchGetWallet", MsgFetchGetWallet],
    ["/org.borderblockchain.businesslogic.MsgFetchAllWalletHistory", MsgFetchAllWalletHistory],
    ["/org.borderblockchain.businesslogic.MsgFetchSegmentHistory", MsgFetchSegmentHistory],
    ["/org.borderblockchain.businesslogic.MsgCloneToken", MsgCloneToken],
    ["/org.borderblockchain.businesslogic.MsgCreateWallet", MsgCreateWallet],
    ["/org.borderblockchain.businesslogic.MsgEventExportRefusal", MsgEventExportRefusal],
    ["/org.borderblockchain.businesslogic.MsgCreateHashToken", MsgCreateHashToken],
    ["/org.borderblockchain.businesslogic.MsgEventCertifyExitOfGoods", MsgEventCertifyExitOfGoods],
    ["/org.borderblockchain.businesslogic.MsgEventExportCheck", MsgEventExportCheck],
    ["/org.borderblockchain.businesslogic.MsgEventImportSubmit", MsgEventImportSubmit],
    ["/org.borderblockchain.businesslogic.MsgFetchGetWalletHistory", MsgFetchGetWalletHistory],
    ["/org.borderblockchain.businesslogic.MsgEventImportSupplement", MsgEventImportSupplement],
    ["/org.borderblockchain.businesslogic.MsgMoveTokenToWallet", MsgMoveTokenToWallet],
    ["/org.borderblockchain.businesslogic.MsgRevertToGenesis", MsgRevertToGenesis],
    ["/org.borderblockchain.businesslogic.MsgFetchExportTokenAllDriver", MsgFetchExportTokenAllDriver],
    ["/org.borderblockchain.businesslogic.MsgFetchImportTokenAll", MsgFetchImportTokenAll],
    ["/org.borderblockchain.businesslogic.MsgFetchGetSegment", MsgFetchGetSegment],
    ["/org.borderblockchain.businesslogic.MsgCreateExportToken", MsgCreateExportToken],
    ["/org.borderblockchain.businesslogic.MsgEventImportExport", MsgEventImportExport],
    ["/org.borderblockchain.businesslogic.MsgAssignCosmosAddressToWallet", MsgAssignCosmosAddressToWallet],
    ["/org.borderblockchain.businesslogic.MsgCreateExportEvent", MsgCreateExportEvent],
    ["/org.borderblockchain.businesslogic.MsgCreateWalletWithId", MsgCreateWalletWithId],
    ["/org.borderblockchain.businesslogic.MsgEventPresentQrCode", MsgEventPresentQrCode],
    ["/org.borderblockchain.businesslogic.MsgEventAcceptExport", MsgEventAcceptExport],
    ["/org.borderblockchain.businesslogic.MsgEventCompleteImport", MsgEventCompleteImport],
    ["/org.borderblockchain.businesslogic.MsgFetchAllSegment", MsgFetchAllSegment],
    ["/org.borderblockchain.businesslogic.MsgFetchAllSegmentHistory", MsgFetchAllSegmentHistory],
    ["/org.borderblockchain.businesslogic.MsgEventImportRelease", MsgEventImportRelease],
    ["/org.borderblockchain.businesslogic.MsgDeactivateToken", MsgDeactivateToken],
    ["/org.borderblockchain.businesslogic.MsgFetchImportTokenByImportId", MsgFetchImportTokenByImportId],
    ["/org.borderblockchain.businesslogic.MsgMoveTokenToSegment", MsgMoveTokenToSegment],
    ["/org.borderblockchain.businesslogic.MsgUpdateTokenCopies", MsgUpdateTokenCopies],
    ["/org.borderblockchain.businesslogic.MsgCreateSegment", MsgCreateSegment],
    ["/org.borderblockchain.businesslogic.MsgCreateDocumentTokenMapper", MsgCreateDocumentTokenMapper],
    ["/org.borderblockchain.businesslogic.MsgDeleteImportToken", MsgDeleteImportToken],
    ["/org.borderblockchain.businesslogic.MsgEventImportProposal", MsgEventImportProposal],
    ["/org.borderblockchain.businesslogic.MsgEventAcceptImport", MsgEventAcceptImport],
    ["/org.borderblockchain.businesslogic.MsgUpdateSegment", MsgUpdateSegment],
    ["/org.borderblockchain.businesslogic.MsgFetchTokensBySegmentId", MsgFetchTokensBySegmentId],
    
];

export { msgTypes }
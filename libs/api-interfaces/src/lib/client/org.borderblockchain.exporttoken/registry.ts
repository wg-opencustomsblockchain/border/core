import { GeneratedType } from "@cosmjs/proto-signing";
import { MsgFetchExportToken } from "./types/exporttoken/tx";
import { MsgUpdateExportToken } from "./types/exporttoken/tx";
import { MsgCreateExportToken } from "./types/exporttoken/tx";
import { MsgFetchExportMapping } from "./types/exporttoken/tx";
import { MsgCreateEvent } from "./types/exporttoken/tx";

const msgTypes: Array<[string, GeneratedType]>  = [
    ["/org.borderblockchain.exporttoken.MsgFetchExportToken", MsgFetchExportToken],
    ["/org.borderblockchain.exporttoken.MsgUpdateExportToken", MsgUpdateExportToken],
    ["/org.borderblockchain.exporttoken.MsgCreateExportToken", MsgCreateExportToken],
    ["/org.borderblockchain.exporttoken.MsgFetchExportMapping", MsgFetchExportMapping],
    ["/org.borderblockchain.exporttoken.MsgCreateEvent", MsgCreateEvent],
    
];

export { msgTypes }
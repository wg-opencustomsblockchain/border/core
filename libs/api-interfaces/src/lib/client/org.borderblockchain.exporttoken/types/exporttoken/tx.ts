/* eslint-disable */
import Long from "long";
import _m0 from "protobufjs/minimal";
import { ExportEvent } from "./event-history";
import { Company, CustomsOffice, ExportToken, GoodsItem, Transport } from "./exporttoken";

export const protobufPackage = "org.borderblockchain.exporttoken";

/**
 * This Messages is used in place of an update on the Token.
 * If you query a token the Event History will be appended and sent alongside the Token.
 */
export interface MsgCreateEvent {
  creator: string;
  index: string;
  status: string;
  message: string;
  eventType: string;
}

/** We might want to return a Response in the future. */
export interface MsgCreateEventResponse {
}

export interface MsgCreateExportToken {
  creator: string;
  id: string;
  exporter: Company | undefined;
  declarant: Company | undefined;
  representative: Company | undefined;
  consignee: Company | undefined;
  customsOfficeOfExport: CustomsOffice | undefined;
  customOfficeOfExit: CustomsOffice | undefined;
  goodsItems: GoodsItem[];
  transportToBorder: Transport | undefined;
  transportAtBorder: Transport | undefined;
  exportId: string;
  uniqueConsignmentReference: string;
  localReferenceNumber: string;
  destinationCountry: string;
  exportCountry: string;
  itinerary: string;
  releaseDateAndTime: string;
  incotermCode: string;
  incotermLocation: string;
  totalGrossMass: string;
  goodsItemQuantity: number;
  totalPackagesQuantity: number;
  natureOfTransaction: number;
  totalAmountInvoiced: number;
  invoiceCurrency: string;
  status: string;
  user: string;
  timestamp: string;
}

export interface MsgCreateExportTokenResponse {
  exportToken: ExportToken | undefined;
  events: ExportEvent[];
}

export interface MsgUpdateExportToken {
  creator: string;
  id: string;
  exporter: Company | undefined;
  declarant: Company | undefined;
  representative: Company | undefined;
  consignee: Company | undefined;
  customsOfficeOfExport: CustomsOffice | undefined;
  customOfficeOfExit: CustomsOffice | undefined;
  goodsItems: GoodsItem[];
  transportToBorder: Transport | undefined;
  transportAtBorder: Transport | undefined;
  exportId: string;
  uniqueConsignmentReference: string;
  localReferenceNumber: string;
  destinationCountry: string;
  exportCountry: string;
  itinerary: string;
  releaseDateAndTime: string;
  incotermCode: string;
  incotermLocation: string;
  totalGrossMass: string;
  goodsItemQuantity: number;
  totalPackagesQuantity: number;
  natureOfTransaction: number;
  totalAmountInvoiced: number;
  invoiceCurrency: string;
  status: string;
  user: string;
  timestamp: string;
}

export interface MsgUpdateExportTokenResponse {
  exportToken: ExportToken | undefined;
  events: ExportEvent[];
}

export interface MsgDeleteExportTokenResponse {
}

export interface MsgFetchExportToken {
  creator: string;
  id: string;
}

export interface MsgFetchExportTokenResponse {
  exportToken: ExportToken | undefined;
  events: ExportEvent[];
}

export interface MsgFetchExportMapping {
  creator: string;
  id: string;
}

export interface MsgFetchExportMappingResponse {
  exportToken: ExportToken | undefined;
  events: ExportEvent[];
}

function createBaseMsgCreateEvent(): MsgCreateEvent {
  return { creator: "", index: "", status: "", message: "", eventType: "" };
}

export const MsgCreateEvent = {
  encode(message: MsgCreateEvent, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.index !== "") {
      writer.uint32(18).string(message.index);
    }
    if (message.status !== "") {
      writer.uint32(26).string(message.status);
    }
    if (message.message !== "") {
      writer.uint32(34).string(message.message);
    }
    if (message.eventType !== "") {
      writer.uint32(42).string(message.eventType);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateEvent {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateEvent();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.index = reader.string();
          break;
        case 3:
          message.status = reader.string();
          break;
        case 4:
          message.message = reader.string();
          break;
        case 5:
          message.eventType = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateEvent {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      index: isSet(object.index) ? String(object.index) : "",
      status: isSet(object.status) ? String(object.status) : "",
      message: isSet(object.message) ? String(object.message) : "",
      eventType: isSet(object.eventType) ? String(object.eventType) : "",
    };
  },

  toJSON(message: MsgCreateEvent): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.index !== undefined && (obj.index = message.index);
    message.status !== undefined && (obj.status = message.status);
    message.message !== undefined && (obj.message = message.message);
    message.eventType !== undefined && (obj.eventType = message.eventType);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateEvent>, I>>(object: I): MsgCreateEvent {
    const message = createBaseMsgCreateEvent();
    message.creator = object.creator ?? "";
    message.index = object.index ?? "";
    message.status = object.status ?? "";
    message.message = object.message ?? "";
    message.eventType = object.eventType ?? "";
    return message;
  },
};

function createBaseMsgCreateEventResponse(): MsgCreateEventResponse {
  return {};
}

export const MsgCreateEventResponse = {
  encode(_: MsgCreateEventResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateEventResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateEventResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgCreateEventResponse {
    return {};
  },

  toJSON(_: MsgCreateEventResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateEventResponse>, I>>(_: I): MsgCreateEventResponse {
    const message = createBaseMsgCreateEventResponse();
    return message;
  },
};

function createBaseMsgCreateExportToken(): MsgCreateExportToken {
  return {
    creator: "",
    id: "",
    exporter: undefined,
    declarant: undefined,
    representative: undefined,
    consignee: undefined,
    customsOfficeOfExport: undefined,
    customOfficeOfExit: undefined,
    goodsItems: [],
    transportToBorder: undefined,
    transportAtBorder: undefined,
    exportId: "",
    uniqueConsignmentReference: "",
    localReferenceNumber: "",
    destinationCountry: "",
    exportCountry: "",
    itinerary: "",
    releaseDateAndTime: "",
    incotermCode: "",
    incotermLocation: "",
    totalGrossMass: "",
    goodsItemQuantity: 0,
    totalPackagesQuantity: 0,
    natureOfTransaction: 0,
    totalAmountInvoiced: 0,
    invoiceCurrency: "",
    status: "",
    user: "",
    timestamp: "",
  };
}

export const MsgCreateExportToken = {
  encode(message: MsgCreateExportToken, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.exporter !== undefined) {
      Company.encode(message.exporter, writer.uint32(26).fork()).ldelim();
    }
    if (message.declarant !== undefined) {
      Company.encode(message.declarant, writer.uint32(34).fork()).ldelim();
    }
    if (message.representative !== undefined) {
      Company.encode(message.representative, writer.uint32(42).fork()).ldelim();
    }
    if (message.consignee !== undefined) {
      Company.encode(message.consignee, writer.uint32(50).fork()).ldelim();
    }
    if (message.customsOfficeOfExport !== undefined) {
      CustomsOffice.encode(message.customsOfficeOfExport, writer.uint32(58).fork()).ldelim();
    }
    if (message.customOfficeOfExit !== undefined) {
      CustomsOffice.encode(message.customOfficeOfExit, writer.uint32(66).fork()).ldelim();
    }
    for (const v of message.goodsItems) {
      GoodsItem.encode(v!, writer.uint32(74).fork()).ldelim();
    }
    if (message.transportToBorder !== undefined) {
      Transport.encode(message.transportToBorder, writer.uint32(82).fork()).ldelim();
    }
    if (message.transportAtBorder !== undefined) {
      Transport.encode(message.transportAtBorder, writer.uint32(90).fork()).ldelim();
    }
    if (message.exportId !== "") {
      writer.uint32(98).string(message.exportId);
    }
    if (message.uniqueConsignmentReference !== "") {
      writer.uint32(106).string(message.uniqueConsignmentReference);
    }
    if (message.localReferenceNumber !== "") {
      writer.uint32(114).string(message.localReferenceNumber);
    }
    if (message.destinationCountry !== "") {
      writer.uint32(122).string(message.destinationCountry);
    }
    if (message.exportCountry !== "") {
      writer.uint32(130).string(message.exportCountry);
    }
    if (message.itinerary !== "") {
      writer.uint32(138).string(message.itinerary);
    }
    if (message.releaseDateAndTime !== "") {
      writer.uint32(146).string(message.releaseDateAndTime);
    }
    if (message.incotermCode !== "") {
      writer.uint32(154).string(message.incotermCode);
    }
    if (message.incotermLocation !== "") {
      writer.uint32(162).string(message.incotermLocation);
    }
    if (message.totalGrossMass !== "") {
      writer.uint32(170).string(message.totalGrossMass);
    }
    if (message.goodsItemQuantity !== 0) {
      writer.uint32(176).int64(message.goodsItemQuantity);
    }
    if (message.totalPackagesQuantity !== 0) {
      writer.uint32(184).int64(message.totalPackagesQuantity);
    }
    if (message.natureOfTransaction !== 0) {
      writer.uint32(192).int64(message.natureOfTransaction);
    }
    if (message.totalAmountInvoiced !== 0) {
      writer.uint32(200).int64(message.totalAmountInvoiced);
    }
    if (message.invoiceCurrency !== "") {
      writer.uint32(210).string(message.invoiceCurrency);
    }
    if (message.status !== "") {
      writer.uint32(218).string(message.status);
    }
    if (message.user !== "") {
      writer.uint32(226).string(message.user);
    }
    if (message.timestamp !== "") {
      writer.uint32(234).string(message.timestamp);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateExportToken {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateExportToken();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.exporter = Company.decode(reader, reader.uint32());
          break;
        case 4:
          message.declarant = Company.decode(reader, reader.uint32());
          break;
        case 5:
          message.representative = Company.decode(reader, reader.uint32());
          break;
        case 6:
          message.consignee = Company.decode(reader, reader.uint32());
          break;
        case 7:
          message.customsOfficeOfExport = CustomsOffice.decode(reader, reader.uint32());
          break;
        case 8:
          message.customOfficeOfExit = CustomsOffice.decode(reader, reader.uint32());
          break;
        case 9:
          message.goodsItems.push(GoodsItem.decode(reader, reader.uint32()));
          break;
        case 10:
          message.transportToBorder = Transport.decode(reader, reader.uint32());
          break;
        case 11:
          message.transportAtBorder = Transport.decode(reader, reader.uint32());
          break;
        case 12:
          message.exportId = reader.string();
          break;
        case 13:
          message.uniqueConsignmentReference = reader.string();
          break;
        case 14:
          message.localReferenceNumber = reader.string();
          break;
        case 15:
          message.destinationCountry = reader.string();
          break;
        case 16:
          message.exportCountry = reader.string();
          break;
        case 17:
          message.itinerary = reader.string();
          break;
        case 18:
          message.releaseDateAndTime = reader.string();
          break;
        case 19:
          message.incotermCode = reader.string();
          break;
        case 20:
          message.incotermLocation = reader.string();
          break;
        case 21:
          message.totalGrossMass = reader.string();
          break;
        case 22:
          message.goodsItemQuantity = longToNumber(reader.int64() as Long);
          break;
        case 23:
          message.totalPackagesQuantity = longToNumber(reader.int64() as Long);
          break;
        case 24:
          message.natureOfTransaction = longToNumber(reader.int64() as Long);
          break;
        case 25:
          message.totalAmountInvoiced = longToNumber(reader.int64() as Long);
          break;
        case 26:
          message.invoiceCurrency = reader.string();
          break;
        case 27:
          message.status = reader.string();
          break;
        case 28:
          message.user = reader.string();
          break;
        case 29:
          message.timestamp = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateExportToken {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      exporter: isSet(object.exporter) ? Company.fromJSON(object.exporter) : undefined,
      declarant: isSet(object.declarant) ? Company.fromJSON(object.declarant) : undefined,
      representative: isSet(object.representative) ? Company.fromJSON(object.representative) : undefined,
      consignee: isSet(object.consignee) ? Company.fromJSON(object.consignee) : undefined,
      customsOfficeOfExport: isSet(object.customsOfficeOfExport)
        ? CustomsOffice.fromJSON(object.customsOfficeOfExport)
        : undefined,
      customOfficeOfExit: isSet(object.customOfficeOfExit)
        ? CustomsOffice.fromJSON(object.customOfficeOfExit)
        : undefined,
      goodsItems: Array.isArray(object?.goodsItems) ? object.goodsItems.map((e: any) => GoodsItem.fromJSON(e)) : [],
      transportToBorder: isSet(object.transportToBorder) ? Transport.fromJSON(object.transportToBorder) : undefined,
      transportAtBorder: isSet(object.transportAtBorder) ? Transport.fromJSON(object.transportAtBorder) : undefined,
      exportId: isSet(object.exportId) ? String(object.exportId) : "",
      uniqueConsignmentReference: isSet(object.uniqueConsignmentReference)
        ? String(object.uniqueConsignmentReference)
        : "",
      localReferenceNumber: isSet(object.localReferenceNumber) ? String(object.localReferenceNumber) : "",
      destinationCountry: isSet(object.destinationCountry) ? String(object.destinationCountry) : "",
      exportCountry: isSet(object.exportCountry) ? String(object.exportCountry) : "",
      itinerary: isSet(object.itinerary) ? String(object.itinerary) : "",
      releaseDateAndTime: isSet(object.releaseDateAndTime) ? String(object.releaseDateAndTime) : "",
      incotermCode: isSet(object.incotermCode) ? String(object.incotermCode) : "",
      incotermLocation: isSet(object.incotermLocation) ? String(object.incotermLocation) : "",
      totalGrossMass: isSet(object.totalGrossMass) ? String(object.totalGrossMass) : "",
      goodsItemQuantity: isSet(object.goodsItemQuantity) ? Number(object.goodsItemQuantity) : 0,
      totalPackagesQuantity: isSet(object.totalPackagesQuantity) ? Number(object.totalPackagesQuantity) : 0,
      natureOfTransaction: isSet(object.natureOfTransaction) ? Number(object.natureOfTransaction) : 0,
      totalAmountInvoiced: isSet(object.totalAmountInvoiced) ? Number(object.totalAmountInvoiced) : 0,
      invoiceCurrency: isSet(object.invoiceCurrency) ? String(object.invoiceCurrency) : "",
      status: isSet(object.status) ? String(object.status) : "",
      user: isSet(object.user) ? String(object.user) : "",
      timestamp: isSet(object.timestamp) ? String(object.timestamp) : "",
    };
  },

  toJSON(message: MsgCreateExportToken): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.exporter !== undefined && (obj.exporter = message.exporter ? Company.toJSON(message.exporter) : undefined);
    message.declarant !== undefined
      && (obj.declarant = message.declarant ? Company.toJSON(message.declarant) : undefined);
    message.representative !== undefined
      && (obj.representative = message.representative ? Company.toJSON(message.representative) : undefined);
    message.consignee !== undefined
      && (obj.consignee = message.consignee ? Company.toJSON(message.consignee) : undefined);
    message.customsOfficeOfExport !== undefined && (obj.customsOfficeOfExport = message.customsOfficeOfExport
      ? CustomsOffice.toJSON(message.customsOfficeOfExport)
      : undefined);
    message.customOfficeOfExit !== undefined && (obj.customOfficeOfExit = message.customOfficeOfExit
      ? CustomsOffice.toJSON(message.customOfficeOfExit)
      : undefined);
    if (message.goodsItems) {
      obj.goodsItems = message.goodsItems.map((e) => e ? GoodsItem.toJSON(e) : undefined);
    } else {
      obj.goodsItems = [];
    }
    message.transportToBorder !== undefined
      && (obj.transportToBorder = message.transportToBorder ? Transport.toJSON(message.transportToBorder) : undefined);
    message.transportAtBorder !== undefined
      && (obj.transportAtBorder = message.transportAtBorder ? Transport.toJSON(message.transportAtBorder) : undefined);
    message.exportId !== undefined && (obj.exportId = message.exportId);
    message.uniqueConsignmentReference !== undefined
      && (obj.uniqueConsignmentReference = message.uniqueConsignmentReference);
    message.localReferenceNumber !== undefined && (obj.localReferenceNumber = message.localReferenceNumber);
    message.destinationCountry !== undefined && (obj.destinationCountry = message.destinationCountry);
    message.exportCountry !== undefined && (obj.exportCountry = message.exportCountry);
    message.itinerary !== undefined && (obj.itinerary = message.itinerary);
    message.releaseDateAndTime !== undefined && (obj.releaseDateAndTime = message.releaseDateAndTime);
    message.incotermCode !== undefined && (obj.incotermCode = message.incotermCode);
    message.incotermLocation !== undefined && (obj.incotermLocation = message.incotermLocation);
    message.totalGrossMass !== undefined && (obj.totalGrossMass = message.totalGrossMass);
    message.goodsItemQuantity !== undefined && (obj.goodsItemQuantity = Math.round(message.goodsItemQuantity));
    message.totalPackagesQuantity !== undefined
      && (obj.totalPackagesQuantity = Math.round(message.totalPackagesQuantity));
    message.natureOfTransaction !== undefined && (obj.natureOfTransaction = Math.round(message.natureOfTransaction));
    message.totalAmountInvoiced !== undefined && (obj.totalAmountInvoiced = Math.round(message.totalAmountInvoiced));
    message.invoiceCurrency !== undefined && (obj.invoiceCurrency = message.invoiceCurrency);
    message.status !== undefined && (obj.status = message.status);
    message.user !== undefined && (obj.user = message.user);
    message.timestamp !== undefined && (obj.timestamp = message.timestamp);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateExportToken>, I>>(object: I): MsgCreateExportToken {
    const message = createBaseMsgCreateExportToken();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.exporter = (object.exporter !== undefined && object.exporter !== null)
      ? Company.fromPartial(object.exporter)
      : undefined;
    message.declarant = (object.declarant !== undefined && object.declarant !== null)
      ? Company.fromPartial(object.declarant)
      : undefined;
    message.representative = (object.representative !== undefined && object.representative !== null)
      ? Company.fromPartial(object.representative)
      : undefined;
    message.consignee = (object.consignee !== undefined && object.consignee !== null)
      ? Company.fromPartial(object.consignee)
      : undefined;
    message.customsOfficeOfExport =
      (object.customsOfficeOfExport !== undefined && object.customsOfficeOfExport !== null)
        ? CustomsOffice.fromPartial(object.customsOfficeOfExport)
        : undefined;
    message.customOfficeOfExit = (object.customOfficeOfExit !== undefined && object.customOfficeOfExit !== null)
      ? CustomsOffice.fromPartial(object.customOfficeOfExit)
      : undefined;
    message.goodsItems = object.goodsItems?.map((e) => GoodsItem.fromPartial(e)) || [];
    message.transportToBorder = (object.transportToBorder !== undefined && object.transportToBorder !== null)
      ? Transport.fromPartial(object.transportToBorder)
      : undefined;
    message.transportAtBorder = (object.transportAtBorder !== undefined && object.transportAtBorder !== null)
      ? Transport.fromPartial(object.transportAtBorder)
      : undefined;
    message.exportId = object.exportId ?? "";
    message.uniqueConsignmentReference = object.uniqueConsignmentReference ?? "";
    message.localReferenceNumber = object.localReferenceNumber ?? "";
    message.destinationCountry = object.destinationCountry ?? "";
    message.exportCountry = object.exportCountry ?? "";
    message.itinerary = object.itinerary ?? "";
    message.releaseDateAndTime = object.releaseDateAndTime ?? "";
    message.incotermCode = object.incotermCode ?? "";
    message.incotermLocation = object.incotermLocation ?? "";
    message.totalGrossMass = object.totalGrossMass ?? "";
    message.goodsItemQuantity = object.goodsItemQuantity ?? 0;
    message.totalPackagesQuantity = object.totalPackagesQuantity ?? 0;
    message.natureOfTransaction = object.natureOfTransaction ?? 0;
    message.totalAmountInvoiced = object.totalAmountInvoiced ?? 0;
    message.invoiceCurrency = object.invoiceCurrency ?? "";
    message.status = object.status ?? "";
    message.user = object.user ?? "";
    message.timestamp = object.timestamp ?? "";
    return message;
  },
};

function createBaseMsgCreateExportTokenResponse(): MsgCreateExportTokenResponse {
  return { exportToken: undefined, events: [] };
}

export const MsgCreateExportTokenResponse = {
  encode(message: MsgCreateExportTokenResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.exportToken !== undefined) {
      ExportToken.encode(message.exportToken, writer.uint32(10).fork()).ldelim();
    }
    for (const v of message.events) {
      ExportEvent.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateExportTokenResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateExportTokenResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.exportToken = ExportToken.decode(reader, reader.uint32());
          break;
        case 2:
          message.events.push(ExportEvent.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateExportTokenResponse {
    return {
      exportToken: isSet(object.exportToken) ? ExportToken.fromJSON(object.exportToken) : undefined,
      events: Array.isArray(object?.events) ? object.events.map((e: any) => ExportEvent.fromJSON(e)) : [],
    };
  },

  toJSON(message: MsgCreateExportTokenResponse): unknown {
    const obj: any = {};
    message.exportToken !== undefined
      && (obj.exportToken = message.exportToken ? ExportToken.toJSON(message.exportToken) : undefined);
    if (message.events) {
      obj.events = message.events.map((e) => e ? ExportEvent.toJSON(e) : undefined);
    } else {
      obj.events = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateExportTokenResponse>, I>>(object: I): MsgCreateExportTokenResponse {
    const message = createBaseMsgCreateExportTokenResponse();
    message.exportToken = (object.exportToken !== undefined && object.exportToken !== null)
      ? ExportToken.fromPartial(object.exportToken)
      : undefined;
    message.events = object.events?.map((e) => ExportEvent.fromPartial(e)) || [];
    return message;
  },
};

function createBaseMsgUpdateExportToken(): MsgUpdateExportToken {
  return {
    creator: "",
    id: "",
    exporter: undefined,
    declarant: undefined,
    representative: undefined,
    consignee: undefined,
    customsOfficeOfExport: undefined,
    customOfficeOfExit: undefined,
    goodsItems: [],
    transportToBorder: undefined,
    transportAtBorder: undefined,
    exportId: "",
    uniqueConsignmentReference: "",
    localReferenceNumber: "",
    destinationCountry: "",
    exportCountry: "",
    itinerary: "",
    releaseDateAndTime: "",
    incotermCode: "",
    incotermLocation: "",
    totalGrossMass: "",
    goodsItemQuantity: 0,
    totalPackagesQuantity: 0,
    natureOfTransaction: 0,
    totalAmountInvoiced: 0,
    invoiceCurrency: "",
    status: "",
    user: "",
    timestamp: "",
  };
}

export const MsgUpdateExportToken = {
  encode(message: MsgUpdateExportToken, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.exporter !== undefined) {
      Company.encode(message.exporter, writer.uint32(26).fork()).ldelim();
    }
    if (message.declarant !== undefined) {
      Company.encode(message.declarant, writer.uint32(34).fork()).ldelim();
    }
    if (message.representative !== undefined) {
      Company.encode(message.representative, writer.uint32(42).fork()).ldelim();
    }
    if (message.consignee !== undefined) {
      Company.encode(message.consignee, writer.uint32(50).fork()).ldelim();
    }
    if (message.customsOfficeOfExport !== undefined) {
      CustomsOffice.encode(message.customsOfficeOfExport, writer.uint32(58).fork()).ldelim();
    }
    if (message.customOfficeOfExit !== undefined) {
      CustomsOffice.encode(message.customOfficeOfExit, writer.uint32(66).fork()).ldelim();
    }
    for (const v of message.goodsItems) {
      GoodsItem.encode(v!, writer.uint32(74).fork()).ldelim();
    }
    if (message.transportToBorder !== undefined) {
      Transport.encode(message.transportToBorder, writer.uint32(82).fork()).ldelim();
    }
    if (message.transportAtBorder !== undefined) {
      Transport.encode(message.transportAtBorder, writer.uint32(90).fork()).ldelim();
    }
    if (message.exportId !== "") {
      writer.uint32(98).string(message.exportId);
    }
    if (message.uniqueConsignmentReference !== "") {
      writer.uint32(106).string(message.uniqueConsignmentReference);
    }
    if (message.localReferenceNumber !== "") {
      writer.uint32(114).string(message.localReferenceNumber);
    }
    if (message.destinationCountry !== "") {
      writer.uint32(122).string(message.destinationCountry);
    }
    if (message.exportCountry !== "") {
      writer.uint32(130).string(message.exportCountry);
    }
    if (message.itinerary !== "") {
      writer.uint32(138).string(message.itinerary);
    }
    if (message.releaseDateAndTime !== "") {
      writer.uint32(146).string(message.releaseDateAndTime);
    }
    if (message.incotermCode !== "") {
      writer.uint32(154).string(message.incotermCode);
    }
    if (message.incotermLocation !== "") {
      writer.uint32(162).string(message.incotermLocation);
    }
    if (message.totalGrossMass !== "") {
      writer.uint32(170).string(message.totalGrossMass);
    }
    if (message.goodsItemQuantity !== 0) {
      writer.uint32(176).int64(message.goodsItemQuantity);
    }
    if (message.totalPackagesQuantity !== 0) {
      writer.uint32(184).int64(message.totalPackagesQuantity);
    }
    if (message.natureOfTransaction !== 0) {
      writer.uint32(192).int64(message.natureOfTransaction);
    }
    if (message.totalAmountInvoiced !== 0) {
      writer.uint32(200).int64(message.totalAmountInvoiced);
    }
    if (message.invoiceCurrency !== "") {
      writer.uint32(210).string(message.invoiceCurrency);
    }
    if (message.status !== "") {
      writer.uint32(218).string(message.status);
    }
    if (message.user !== "") {
      writer.uint32(226).string(message.user);
    }
    if (message.timestamp !== "") {
      writer.uint32(234).string(message.timestamp);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgUpdateExportToken {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgUpdateExportToken();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.exporter = Company.decode(reader, reader.uint32());
          break;
        case 4:
          message.declarant = Company.decode(reader, reader.uint32());
          break;
        case 5:
          message.representative = Company.decode(reader, reader.uint32());
          break;
        case 6:
          message.consignee = Company.decode(reader, reader.uint32());
          break;
        case 7:
          message.customsOfficeOfExport = CustomsOffice.decode(reader, reader.uint32());
          break;
        case 8:
          message.customOfficeOfExit = CustomsOffice.decode(reader, reader.uint32());
          break;
        case 9:
          message.goodsItems.push(GoodsItem.decode(reader, reader.uint32()));
          break;
        case 10:
          message.transportToBorder = Transport.decode(reader, reader.uint32());
          break;
        case 11:
          message.transportAtBorder = Transport.decode(reader, reader.uint32());
          break;
        case 12:
          message.exportId = reader.string();
          break;
        case 13:
          message.uniqueConsignmentReference = reader.string();
          break;
        case 14:
          message.localReferenceNumber = reader.string();
          break;
        case 15:
          message.destinationCountry = reader.string();
          break;
        case 16:
          message.exportCountry = reader.string();
          break;
        case 17:
          message.itinerary = reader.string();
          break;
        case 18:
          message.releaseDateAndTime = reader.string();
          break;
        case 19:
          message.incotermCode = reader.string();
          break;
        case 20:
          message.incotermLocation = reader.string();
          break;
        case 21:
          message.totalGrossMass = reader.string();
          break;
        case 22:
          message.goodsItemQuantity = longToNumber(reader.int64() as Long);
          break;
        case 23:
          message.totalPackagesQuantity = longToNumber(reader.int64() as Long);
          break;
        case 24:
          message.natureOfTransaction = longToNumber(reader.int64() as Long);
          break;
        case 25:
          message.totalAmountInvoiced = longToNumber(reader.int64() as Long);
          break;
        case 26:
          message.invoiceCurrency = reader.string();
          break;
        case 27:
          message.status = reader.string();
          break;
        case 28:
          message.user = reader.string();
          break;
        case 29:
          message.timestamp = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateExportToken {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      exporter: isSet(object.exporter) ? Company.fromJSON(object.exporter) : undefined,
      declarant: isSet(object.declarant) ? Company.fromJSON(object.declarant) : undefined,
      representative: isSet(object.representative) ? Company.fromJSON(object.representative) : undefined,
      consignee: isSet(object.consignee) ? Company.fromJSON(object.consignee) : undefined,
      customsOfficeOfExport: isSet(object.customsOfficeOfExport)
        ? CustomsOffice.fromJSON(object.customsOfficeOfExport)
        : undefined,
      customOfficeOfExit: isSet(object.customOfficeOfExit)
        ? CustomsOffice.fromJSON(object.customOfficeOfExit)
        : undefined,
      goodsItems: Array.isArray(object?.goodsItems) ? object.goodsItems.map((e: any) => GoodsItem.fromJSON(e)) : [],
      transportToBorder: isSet(object.transportToBorder) ? Transport.fromJSON(object.transportToBorder) : undefined,
      transportAtBorder: isSet(object.transportAtBorder) ? Transport.fromJSON(object.transportAtBorder) : undefined,
      exportId: isSet(object.exportId) ? String(object.exportId) : "",
      uniqueConsignmentReference: isSet(object.uniqueConsignmentReference)
        ? String(object.uniqueConsignmentReference)
        : "",
      localReferenceNumber: isSet(object.localReferenceNumber) ? String(object.localReferenceNumber) : "",
      destinationCountry: isSet(object.destinationCountry) ? String(object.destinationCountry) : "",
      exportCountry: isSet(object.exportCountry) ? String(object.exportCountry) : "",
      itinerary: isSet(object.itinerary) ? String(object.itinerary) : "",
      releaseDateAndTime: isSet(object.releaseDateAndTime) ? String(object.releaseDateAndTime) : "",
      incotermCode: isSet(object.incotermCode) ? String(object.incotermCode) : "",
      incotermLocation: isSet(object.incotermLocation) ? String(object.incotermLocation) : "",
      totalGrossMass: isSet(object.totalGrossMass) ? String(object.totalGrossMass) : "",
      goodsItemQuantity: isSet(object.goodsItemQuantity) ? Number(object.goodsItemQuantity) : 0,
      totalPackagesQuantity: isSet(object.totalPackagesQuantity) ? Number(object.totalPackagesQuantity) : 0,
      natureOfTransaction: isSet(object.natureOfTransaction) ? Number(object.natureOfTransaction) : 0,
      totalAmountInvoiced: isSet(object.totalAmountInvoiced) ? Number(object.totalAmountInvoiced) : 0,
      invoiceCurrency: isSet(object.invoiceCurrency) ? String(object.invoiceCurrency) : "",
      status: isSet(object.status) ? String(object.status) : "",
      user: isSet(object.user) ? String(object.user) : "",
      timestamp: isSet(object.timestamp) ? String(object.timestamp) : "",
    };
  },

  toJSON(message: MsgUpdateExportToken): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.exporter !== undefined && (obj.exporter = message.exporter ? Company.toJSON(message.exporter) : undefined);
    message.declarant !== undefined
      && (obj.declarant = message.declarant ? Company.toJSON(message.declarant) : undefined);
    message.representative !== undefined
      && (obj.representative = message.representative ? Company.toJSON(message.representative) : undefined);
    message.consignee !== undefined
      && (obj.consignee = message.consignee ? Company.toJSON(message.consignee) : undefined);
    message.customsOfficeOfExport !== undefined && (obj.customsOfficeOfExport = message.customsOfficeOfExport
      ? CustomsOffice.toJSON(message.customsOfficeOfExport)
      : undefined);
    message.customOfficeOfExit !== undefined && (obj.customOfficeOfExit = message.customOfficeOfExit
      ? CustomsOffice.toJSON(message.customOfficeOfExit)
      : undefined);
    if (message.goodsItems) {
      obj.goodsItems = message.goodsItems.map((e) => e ? GoodsItem.toJSON(e) : undefined);
    } else {
      obj.goodsItems = [];
    }
    message.transportToBorder !== undefined
      && (obj.transportToBorder = message.transportToBorder ? Transport.toJSON(message.transportToBorder) : undefined);
    message.transportAtBorder !== undefined
      && (obj.transportAtBorder = message.transportAtBorder ? Transport.toJSON(message.transportAtBorder) : undefined);
    message.exportId !== undefined && (obj.exportId = message.exportId);
    message.uniqueConsignmentReference !== undefined
      && (obj.uniqueConsignmentReference = message.uniqueConsignmentReference);
    message.localReferenceNumber !== undefined && (obj.localReferenceNumber = message.localReferenceNumber);
    message.destinationCountry !== undefined && (obj.destinationCountry = message.destinationCountry);
    message.exportCountry !== undefined && (obj.exportCountry = message.exportCountry);
    message.itinerary !== undefined && (obj.itinerary = message.itinerary);
    message.releaseDateAndTime !== undefined && (obj.releaseDateAndTime = message.releaseDateAndTime);
    message.incotermCode !== undefined && (obj.incotermCode = message.incotermCode);
    message.incotermLocation !== undefined && (obj.incotermLocation = message.incotermLocation);
    message.totalGrossMass !== undefined && (obj.totalGrossMass = message.totalGrossMass);
    message.goodsItemQuantity !== undefined && (obj.goodsItemQuantity = Math.round(message.goodsItemQuantity));
    message.totalPackagesQuantity !== undefined
      && (obj.totalPackagesQuantity = Math.round(message.totalPackagesQuantity));
    message.natureOfTransaction !== undefined && (obj.natureOfTransaction = Math.round(message.natureOfTransaction));
    message.totalAmountInvoiced !== undefined && (obj.totalAmountInvoiced = Math.round(message.totalAmountInvoiced));
    message.invoiceCurrency !== undefined && (obj.invoiceCurrency = message.invoiceCurrency);
    message.status !== undefined && (obj.status = message.status);
    message.user !== undefined && (obj.user = message.user);
    message.timestamp !== undefined && (obj.timestamp = message.timestamp);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgUpdateExportToken>, I>>(object: I): MsgUpdateExportToken {
    const message = createBaseMsgUpdateExportToken();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.exporter = (object.exporter !== undefined && object.exporter !== null)
      ? Company.fromPartial(object.exporter)
      : undefined;
    message.declarant = (object.declarant !== undefined && object.declarant !== null)
      ? Company.fromPartial(object.declarant)
      : undefined;
    message.representative = (object.representative !== undefined && object.representative !== null)
      ? Company.fromPartial(object.representative)
      : undefined;
    message.consignee = (object.consignee !== undefined && object.consignee !== null)
      ? Company.fromPartial(object.consignee)
      : undefined;
    message.customsOfficeOfExport =
      (object.customsOfficeOfExport !== undefined && object.customsOfficeOfExport !== null)
        ? CustomsOffice.fromPartial(object.customsOfficeOfExport)
        : undefined;
    message.customOfficeOfExit = (object.customOfficeOfExit !== undefined && object.customOfficeOfExit !== null)
      ? CustomsOffice.fromPartial(object.customOfficeOfExit)
      : undefined;
    message.goodsItems = object.goodsItems?.map((e) => GoodsItem.fromPartial(e)) || [];
    message.transportToBorder = (object.transportToBorder !== undefined && object.transportToBorder !== null)
      ? Transport.fromPartial(object.transportToBorder)
      : undefined;
    message.transportAtBorder = (object.transportAtBorder !== undefined && object.transportAtBorder !== null)
      ? Transport.fromPartial(object.transportAtBorder)
      : undefined;
    message.exportId = object.exportId ?? "";
    message.uniqueConsignmentReference = object.uniqueConsignmentReference ?? "";
    message.localReferenceNumber = object.localReferenceNumber ?? "";
    message.destinationCountry = object.destinationCountry ?? "";
    message.exportCountry = object.exportCountry ?? "";
    message.itinerary = object.itinerary ?? "";
    message.releaseDateAndTime = object.releaseDateAndTime ?? "";
    message.incotermCode = object.incotermCode ?? "";
    message.incotermLocation = object.incotermLocation ?? "";
    message.totalGrossMass = object.totalGrossMass ?? "";
    message.goodsItemQuantity = object.goodsItemQuantity ?? 0;
    message.totalPackagesQuantity = object.totalPackagesQuantity ?? 0;
    message.natureOfTransaction = object.natureOfTransaction ?? 0;
    message.totalAmountInvoiced = object.totalAmountInvoiced ?? 0;
    message.invoiceCurrency = object.invoiceCurrency ?? "";
    message.status = object.status ?? "";
    message.user = object.user ?? "";
    message.timestamp = object.timestamp ?? "";
    return message;
  },
};

function createBaseMsgUpdateExportTokenResponse(): MsgUpdateExportTokenResponse {
  return { exportToken: undefined, events: [] };
}

export const MsgUpdateExportTokenResponse = {
  encode(message: MsgUpdateExportTokenResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.exportToken !== undefined) {
      ExportToken.encode(message.exportToken, writer.uint32(10).fork()).ldelim();
    }
    for (const v of message.events) {
      ExportEvent.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgUpdateExportTokenResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgUpdateExportTokenResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.exportToken = ExportToken.decode(reader, reader.uint32());
          break;
        case 2:
          message.events.push(ExportEvent.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateExportTokenResponse {
    return {
      exportToken: isSet(object.exportToken) ? ExportToken.fromJSON(object.exportToken) : undefined,
      events: Array.isArray(object?.events) ? object.events.map((e: any) => ExportEvent.fromJSON(e)) : [],
    };
  },

  toJSON(message: MsgUpdateExportTokenResponse): unknown {
    const obj: any = {};
    message.exportToken !== undefined
      && (obj.exportToken = message.exportToken ? ExportToken.toJSON(message.exportToken) : undefined);
    if (message.events) {
      obj.events = message.events.map((e) => e ? ExportEvent.toJSON(e) : undefined);
    } else {
      obj.events = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgUpdateExportTokenResponse>, I>>(object: I): MsgUpdateExportTokenResponse {
    const message = createBaseMsgUpdateExportTokenResponse();
    message.exportToken = (object.exportToken !== undefined && object.exportToken !== null)
      ? ExportToken.fromPartial(object.exportToken)
      : undefined;
    message.events = object.events?.map((e) => ExportEvent.fromPartial(e)) || [];
    return message;
  },
};

function createBaseMsgDeleteExportTokenResponse(): MsgDeleteExportTokenResponse {
  return {};
}

export const MsgDeleteExportTokenResponse = {
  encode(_: MsgDeleteExportTokenResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgDeleteExportTokenResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgDeleteExportTokenResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgDeleteExportTokenResponse {
    return {};
  },

  toJSON(_: MsgDeleteExportTokenResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgDeleteExportTokenResponse>, I>>(_: I): MsgDeleteExportTokenResponse {
    const message = createBaseMsgDeleteExportTokenResponse();
    return message;
  },
};

function createBaseMsgFetchExportToken(): MsgFetchExportToken {
  return { creator: "", id: "" };
}

export const MsgFetchExportToken = {
  encode(message: MsgFetchExportToken, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchExportToken {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchExportToken();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchExportToken {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
    };
  },

  toJSON(message: MsgFetchExportToken): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchExportToken>, I>>(object: I): MsgFetchExportToken {
    const message = createBaseMsgFetchExportToken();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgFetchExportTokenResponse(): MsgFetchExportTokenResponse {
  return { exportToken: undefined, events: [] };
}

export const MsgFetchExportTokenResponse = {
  encode(message: MsgFetchExportTokenResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.exportToken !== undefined) {
      ExportToken.encode(message.exportToken, writer.uint32(10).fork()).ldelim();
    }
    for (const v of message.events) {
      ExportEvent.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchExportTokenResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchExportTokenResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.exportToken = ExportToken.decode(reader, reader.uint32());
          break;
        case 2:
          message.events.push(ExportEvent.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchExportTokenResponse {
    return {
      exportToken: isSet(object.exportToken) ? ExportToken.fromJSON(object.exportToken) : undefined,
      events: Array.isArray(object?.events) ? object.events.map((e: any) => ExportEvent.fromJSON(e)) : [],
    };
  },

  toJSON(message: MsgFetchExportTokenResponse): unknown {
    const obj: any = {};
    message.exportToken !== undefined
      && (obj.exportToken = message.exportToken ? ExportToken.toJSON(message.exportToken) : undefined);
    if (message.events) {
      obj.events = message.events.map((e) => e ? ExportEvent.toJSON(e) : undefined);
    } else {
      obj.events = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchExportTokenResponse>, I>>(object: I): MsgFetchExportTokenResponse {
    const message = createBaseMsgFetchExportTokenResponse();
    message.exportToken = (object.exportToken !== undefined && object.exportToken !== null)
      ? ExportToken.fromPartial(object.exportToken)
      : undefined;
    message.events = object.events?.map((e) => ExportEvent.fromPartial(e)) || [];
    return message;
  },
};

function createBaseMsgFetchExportMapping(): MsgFetchExportMapping {
  return { creator: "", id: "" };
}

export const MsgFetchExportMapping = {
  encode(message: MsgFetchExportMapping, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchExportMapping {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchExportMapping();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchExportMapping {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
    };
  },

  toJSON(message: MsgFetchExportMapping): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchExportMapping>, I>>(object: I): MsgFetchExportMapping {
    const message = createBaseMsgFetchExportMapping();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgFetchExportMappingResponse(): MsgFetchExportMappingResponse {
  return { exportToken: undefined, events: [] };
}

export const MsgFetchExportMappingResponse = {
  encode(message: MsgFetchExportMappingResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.exportToken !== undefined) {
      ExportToken.encode(message.exportToken, writer.uint32(10).fork()).ldelim();
    }
    for (const v of message.events) {
      ExportEvent.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchExportMappingResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchExportMappingResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.exportToken = ExportToken.decode(reader, reader.uint32());
          break;
        case 2:
          message.events.push(ExportEvent.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchExportMappingResponse {
    return {
      exportToken: isSet(object.exportToken) ? ExportToken.fromJSON(object.exportToken) : undefined,
      events: Array.isArray(object?.events) ? object.events.map((e: any) => ExportEvent.fromJSON(e)) : [],
    };
  },

  toJSON(message: MsgFetchExportMappingResponse): unknown {
    const obj: any = {};
    message.exportToken !== undefined
      && (obj.exportToken = message.exportToken ? ExportToken.toJSON(message.exportToken) : undefined);
    if (message.events) {
      obj.events = message.events.map((e) => e ? ExportEvent.toJSON(e) : undefined);
    } else {
      obj.events = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchExportMappingResponse>, I>>(
    object: I,
  ): MsgFetchExportMappingResponse {
    const message = createBaseMsgFetchExportMappingResponse();
    message.exportToken = (object.exportToken !== undefined && object.exportToken !== null)
      ? ExportToken.fromPartial(object.exportToken)
      : undefined;
    message.events = object.events?.map((e) => ExportEvent.fromPartial(e)) || [];
    return message;
  },
};

/** Msg defines the Msg service. */
export interface Msg {
}

export class MsgClientImpl implements Msg {
  private readonly rpc: Rpc;
  constructor(rpc: Rpc) {
    this.rpc = rpc;
  }
}

interface Rpc {
  request(service: string, method: string, data: Uint8Array): Promise<Uint8Array>;
}

declare var self: any | undefined;
declare var window: any | undefined;
declare var global: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== "undefined") {
    return globalThis;
  }
  if (typeof self !== "undefined") {
    return self;
  }
  if (typeof window !== "undefined") {
    return window;
  }
  if (typeof global !== "undefined") {
    return global;
  }
  throw "Unable to locate global object";
})();

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
  }
  return long.toNumber();
}

if (true) {
  _m0.util.Long = Long as any;
  _m0.configure();
}

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

/* eslint-disable */
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';

export const protobufPackage = 'org.borderblockchain.exporttoken';

export interface ExportToken {
  creator: string;
  id: string;
  exporter: Company | undefined;
  declarant: Company | undefined;
  representative: Company | undefined;
  consignee: Company | undefined;
  customsOfficeOfExport: CustomsOffice | undefined;
  customOfficeOfExit: CustomsOffice | undefined;
  goodsItems: GoodsItem[];
  transportToBorder: Transport | undefined;
  transportAtBorder: Transport | undefined;
  exportId: string;
  uniqueConsignmentReference: string;
  localReferenceNumber: string;
  destinationCountry: string;
  exportCountry: string;
  itinerary: string;
  releaseDateAndTime: string;
  incotermCode: string;
  incotermLocation: string;
  totalGrossMass: string;
  goodsItemQuantity: number;
  totalPackagesQuantity: number;
  natureOfTransaction: number;
  totalAmountInvoiced: number;
  invoiceCurrency: string;
}

export interface Company {
  address: Address | undefined;
  customsId: string;
  name: string;
  subsidiaryNumber: string;
  identificationNumber: string;
}

export interface CustomsOffice {
  address: Address | undefined;
  customsOfficeCode: string;
  customsOfficeName: string;
}

export interface Address {
  streetAndNumber: string;
  postcode: string;
  city: string;
  countryCode: string;
}

export interface GoodsItem {
  documents: Document[];
  countryOfOrigin: string;
  sequenceNumber: number;
  descriptionOfGoods: string;
  harmonizedSystemSubheadingCode: string;
  localClassificationCode: string;
  grossMass: string;
  netMass: string;
  numberOfPackages: number;
  typeOfPackages: string;
  marksNumbers: string;
  containerId: string;
  consigneeOrderNumber: string;
  valueAtBorderExport: number;
  valueAtBorderExportCurrency: string;
  amountInvoiced: number;
  amountInvoicedCurrency: string;
  dangerousGoodsCode: number;
}

export interface Transport {
  modeOfTransport: number;
  typeOfIdentification: number;
  identity: string;
  nationality: string;
  transportCost: number;
  transportCostCurrency: string;
  transportOrderNumber: string;
}

export interface Document {
  type: string;
  referenceNumber: string;
  complement: string;
  detail: string;
}

function createBaseExportToken(): ExportToken {
  return {
    creator: '',
    id: '',
    exporter: undefined,
    declarant: undefined,
    representative: undefined,
    consignee: undefined,
    customsOfficeOfExport: undefined,
    customOfficeOfExit: undefined,
    goodsItems: [],
    transportToBorder: undefined,
    transportAtBorder: undefined,
    exportId: '',
    uniqueConsignmentReference: '',
    localReferenceNumber: '',
    destinationCountry: '',
    exportCountry: '',
    itinerary: '',
    releaseDateAndTime: '',
    incotermCode: '',
    incotermLocation: '',
    totalGrossMass: '',
    goodsItemQuantity: 0,
    totalPackagesQuantity: 0,
    natureOfTransaction: 0,
    totalAmountInvoiced: 0,
    invoiceCurrency: '',
  };
}

export const ExportToken = {
  encode(message: ExportToken, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== '') {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== '') {
      writer.uint32(18).string(message.id);
    }
    if (message.exporter !== undefined) {
      Company.encode(message.exporter, writer.uint32(26).fork()).ldelim();
    }
    if (message.declarant !== undefined) {
      Company.encode(message.declarant, writer.uint32(34).fork()).ldelim();
    }
    if (message.representative !== undefined) {
      Company.encode(message.representative, writer.uint32(42).fork()).ldelim();
    }
    if (message.consignee !== undefined) {
      Company.encode(message.consignee, writer.uint32(50).fork()).ldelim();
    }
    if (message.customsOfficeOfExport !== undefined) {
      CustomsOffice.encode(message.customsOfficeOfExport, writer.uint32(58).fork()).ldelim();
    }
    if (message.customOfficeOfExit !== undefined) {
      CustomsOffice.encode(message.customOfficeOfExit, writer.uint32(66).fork()).ldelim();
    }
    for (const v of message.goodsItems) {
      GoodsItem.encode(v!, writer.uint32(74).fork()).ldelim();
    }
    if (message.transportToBorder !== undefined) {
      Transport.encode(message.transportToBorder, writer.uint32(82).fork()).ldelim();
    }
    if (message.transportAtBorder !== undefined) {
      Transport.encode(message.transportAtBorder, writer.uint32(90).fork()).ldelim();
    }
    if (message.exportId !== '') {
      writer.uint32(98).string(message.exportId);
    }
    if (message.uniqueConsignmentReference !== '') {
      writer.uint32(106).string(message.uniqueConsignmentReference);
    }
    if (message.localReferenceNumber !== '') {
      writer.uint32(114).string(message.localReferenceNumber);
    }
    if (message.destinationCountry !== '') {
      writer.uint32(122).string(message.destinationCountry);
    }
    if (message.exportCountry !== '') {
      writer.uint32(130).string(message.exportCountry);
    }
    if (message.itinerary !== '') {
      writer.uint32(138).string(message.itinerary);
    }
    if (message.releaseDateAndTime !== '') {
      writer.uint32(146).string(message.releaseDateAndTime);
    }
    if (message.incotermCode !== '') {
      writer.uint32(154).string(message.incotermCode);
    }
    if (message.incotermLocation !== '') {
      writer.uint32(162).string(message.incotermLocation);
    }
    if (message.totalGrossMass !== '') {
      writer.uint32(170).string(message.totalGrossMass);
    }
    if (message.goodsItemQuantity !== 0) {
      writer.uint32(176).int64(message.goodsItemQuantity);
    }
    if (message.totalPackagesQuantity !== 0) {
      writer.uint32(184).int64(message.totalPackagesQuantity);
    }
    if (message.natureOfTransaction !== 0) {
      writer.uint32(192).int64(message.natureOfTransaction);
    }
    if (message.totalAmountInvoiced !== 0) {
      writer.uint32(200).int64(message.totalAmountInvoiced);
    }
    if (message.invoiceCurrency !== '') {
      writer.uint32(210).string(message.invoiceCurrency);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ExportToken {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseExportToken();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.exporter = Company.decode(reader, reader.uint32());
          break;
        case 4:
          message.declarant = Company.decode(reader, reader.uint32());
          break;
        case 5:
          message.representative = Company.decode(reader, reader.uint32());
          break;
        case 6:
          message.consignee = Company.decode(reader, reader.uint32());
          break;
        case 7:
          message.customsOfficeOfExport = CustomsOffice.decode(reader, reader.uint32());
          break;
        case 8:
          message.customOfficeOfExit = CustomsOffice.decode(reader, reader.uint32());
          break;
        case 9:
          message.goodsItems.push(GoodsItem.decode(reader, reader.uint32()));
          break;
        case 10:
          message.transportToBorder = Transport.decode(reader, reader.uint32());
          break;
        case 11:
          message.transportAtBorder = Transport.decode(reader, reader.uint32());
          break;
        case 12:
          message.exportId = reader.string();
          break;
        case 13:
          message.uniqueConsignmentReference = reader.string();
          break;
        case 14:
          message.localReferenceNumber = reader.string();
          break;
        case 15:
          message.destinationCountry = reader.string();
          break;
        case 16:
          message.exportCountry = reader.string();
          break;
        case 17:
          message.itinerary = reader.string();
          break;
        case 18:
          message.releaseDateAndTime = reader.string();
          break;
        case 19:
          message.incotermCode = reader.string();
          break;
        case 20:
          message.incotermLocation = reader.string();
          break;
        case 21:
          message.totalGrossMass = reader.string();
          break;
        case 22:
          message.goodsItemQuantity = longToNumber(reader.int64() as Long);
          break;
        case 23:
          message.totalPackagesQuantity = longToNumber(reader.int64() as Long);
          break;
        case 24:
          message.natureOfTransaction = longToNumber(reader.int64() as Long);
          break;
        case 25:
          message.totalAmountInvoiced = longToNumber(reader.int64() as Long);
          break;
        case 26:
          message.invoiceCurrency = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ExportToken {
    return {
      creator: isSet(object.creator) ? String(object.creator) : '',
      id: isSet(object.id) ? String(object.id) : '',
      exporter: isSet(object.exporter) ? Company.fromJSON(object.exporter) : undefined,
      declarant: isSet(object.declarant) ? Company.fromJSON(object.declarant) : undefined,
      representative: isSet(object.representative) ? Company.fromJSON(object.representative) : undefined,
      consignee: isSet(object.consignee) ? Company.fromJSON(object.consignee) : undefined,
      customsOfficeOfExport: isSet(object.customsOfficeOfExport)
        ? CustomsOffice.fromJSON(object.customsOfficeOfExport)
        : undefined,
      customOfficeOfExit: isSet(object.customOfficeOfExit)
        ? CustomsOffice.fromJSON(object.customOfficeOfExit)
        : undefined,
      goodsItems: Array.isArray(object?.goodsItems) ? object.goodsItems.map((e: any) => GoodsItem.fromJSON(e)) : [],
      transportToBorder: isSet(object.transportToBorder) ? Transport.fromJSON(object.transportToBorder) : undefined,
      transportAtBorder: isSet(object.transportAtBorder) ? Transport.fromJSON(object.transportAtBorder) : undefined,
      exportId: isSet(object.exportId) ? String(object.exportId) : '',
      uniqueConsignmentReference: isSet(object.uniqueConsignmentReference)
        ? String(object.uniqueConsignmentReference)
        : '',
      localReferenceNumber: isSet(object.localReferenceNumber) ? String(object.localReferenceNumber) : '',
      destinationCountry: isSet(object.destinationCountry) ? String(object.destinationCountry) : '',
      exportCountry: isSet(object.exportCountry) ? String(object.exportCountry) : '',
      itinerary: isSet(object.itinerary) ? String(object.itinerary) : '',
      releaseDateAndTime: isSet(object.releaseDateAndTime) ? String(object.releaseDateAndTime) : '',
      incotermCode: isSet(object.incotermCode) ? String(object.incotermCode) : '',
      incotermLocation: isSet(object.incotermLocation) ? String(object.incotermLocation) : '',
      totalGrossMass: isSet(object.totalGrossMass) ? String(object.totalGrossMass) : '',
      goodsItemQuantity: isSet(object.goodsItemQuantity) ? Number(object.goodsItemQuantity) : 0,
      totalPackagesQuantity: isSet(object.totalPackagesQuantity) ? Number(object.totalPackagesQuantity) : 0,
      natureOfTransaction: isSet(object.natureOfTransaction) ? Number(object.natureOfTransaction) : 0,
      totalAmountInvoiced: isSet(object.totalAmountInvoiced) ? Number(object.totalAmountInvoiced) : 0,
      invoiceCurrency: isSet(object.invoiceCurrency) ? String(object.invoiceCurrency) : '',
    };
  },

  toJSON(message: ExportToken): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.exporter !== undefined && (obj.exporter = message.exporter ? Company.toJSON(message.exporter) : undefined);
    message.declarant !== undefined &&
    (obj.declarant = message.declarant ? Company.toJSON(message.declarant) : undefined);
    message.representative !== undefined &&
    (obj.representative = message.representative ? Company.toJSON(message.representative) : undefined);
    message.consignee !== undefined &&
    (obj.consignee = message.consignee ? Company.toJSON(message.consignee) : undefined);
    message.customsOfficeOfExport !== undefined &&
    (obj.customsOfficeOfExport = message.customsOfficeOfExport
      ? CustomsOffice.toJSON(message.customsOfficeOfExport)
      : undefined);
    message.customOfficeOfExit !== undefined &&
    (obj.customOfficeOfExit = message.customOfficeOfExit
      ? CustomsOffice.toJSON(message.customOfficeOfExit)
      : undefined);
    if (message.goodsItems) {
      obj.goodsItems = message.goodsItems.map((e) => (e ? GoodsItem.toJSON(e) : undefined));
    } else {
      obj.goodsItems = [];
    }
    message.transportToBorder !== undefined &&
    (obj.transportToBorder = message.transportToBorder ? Transport.toJSON(message.transportToBorder) : undefined);
    message.transportAtBorder !== undefined &&
    (obj.transportAtBorder = message.transportAtBorder ? Transport.toJSON(message.transportAtBorder) : undefined);
    message.exportId !== undefined && (obj.exportId = message.exportId);
    message.uniqueConsignmentReference !== undefined &&
    (obj.uniqueConsignmentReference = message.uniqueConsignmentReference);
    message.localReferenceNumber !== undefined && (obj.localReferenceNumber = message.localReferenceNumber);
    message.destinationCountry !== undefined && (obj.destinationCountry = message.destinationCountry);
    message.exportCountry !== undefined && (obj.exportCountry = message.exportCountry);
    message.itinerary !== undefined && (obj.itinerary = message.itinerary);
    message.releaseDateAndTime !== undefined && (obj.releaseDateAndTime = message.releaseDateAndTime);
    message.incotermCode !== undefined && (obj.incotermCode = message.incotermCode);
    message.incotermLocation !== undefined && (obj.incotermLocation = message.incotermLocation);
    message.totalGrossMass !== undefined && (obj.totalGrossMass = message.totalGrossMass);
    message.goodsItemQuantity !== undefined && (obj.goodsItemQuantity = Math.round(message.goodsItemQuantity));
    message.totalPackagesQuantity !== undefined &&
    (obj.totalPackagesQuantity = Math.round(message.totalPackagesQuantity));
    message.natureOfTransaction !== undefined && (obj.natureOfTransaction = Math.round(message.natureOfTransaction));
    message.totalAmountInvoiced !== undefined && (obj.totalAmountInvoiced = Math.round(message.totalAmountInvoiced));
    message.invoiceCurrency !== undefined && (obj.invoiceCurrency = message.invoiceCurrency);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<ExportToken>, I>>(object: I): ExportToken {
    const message = createBaseExportToken();
    message.creator = object.creator ?? '';
    message.id = object.id ?? '';
    message.exporter =
      object.exporter !== undefined && object.exporter !== null ? Company.fromPartial(object.exporter) : undefined;
    message.declarant =
      object.declarant !== undefined && object.declarant !== null ? Company.fromPartial(object.declarant) : undefined;
    message.representative =
      object.representative !== undefined && object.representative !== null
        ? Company.fromPartial(object.representative)
        : undefined;
    message.consignee =
      object.consignee !== undefined && object.consignee !== null ? Company.fromPartial(object.consignee) : undefined;
    message.customsOfficeOfExport =
      object.customsOfficeOfExport !== undefined && object.customsOfficeOfExport !== null
        ? CustomsOffice.fromPartial(object.customsOfficeOfExport)
        : undefined;
    message.customOfficeOfExit =
      object.customOfficeOfExit !== undefined && object.customOfficeOfExit !== null
        ? CustomsOffice.fromPartial(object.customOfficeOfExit)
        : undefined;
    message.goodsItems = object.goodsItems?.map((e) => GoodsItem.fromPartial(e)) || [];
    message.transportToBorder =
      object.transportToBorder !== undefined && object.transportToBorder !== null
        ? Transport.fromPartial(object.transportToBorder)
        : undefined;
    message.transportAtBorder =
      object.transportAtBorder !== undefined && object.transportAtBorder !== null
        ? Transport.fromPartial(object.transportAtBorder)
        : undefined;
    message.exportId = object.exportId ?? '';
    message.uniqueConsignmentReference = object.uniqueConsignmentReference ?? '';
    message.localReferenceNumber = object.localReferenceNumber ?? '';
    message.destinationCountry = object.destinationCountry ?? '';
    message.exportCountry = object.exportCountry ?? '';
    message.itinerary = object.itinerary ?? '';
    message.releaseDateAndTime = object.releaseDateAndTime ?? '';
    message.incotermCode = object.incotermCode ?? '';
    message.incotermLocation = object.incotermLocation ?? '';
    message.totalGrossMass = object.totalGrossMass ?? '';
    message.goodsItemQuantity = object.goodsItemQuantity ?? 0;
    message.totalPackagesQuantity = object.totalPackagesQuantity ?? 0;
    message.natureOfTransaction = object.natureOfTransaction ?? 0;
    message.totalAmountInvoiced = object.totalAmountInvoiced ?? 0;
    message.invoiceCurrency = object.invoiceCurrency ?? '';
    return message;
  },
};

function createBaseCompany(): Company {
  return { address: undefined, customsId: '', name: '', subsidiaryNumber: '', identificationNumber: '' };
}

export const Company = {
  encode(message: Company, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.address !== undefined) {
      Address.encode(message.address, writer.uint32(10).fork()).ldelim();
    }
    if (message.customsId !== '') {
      writer.uint32(18).string(message.customsId);
    }
    if (message.name !== '') {
      writer.uint32(26).string(message.name);
    }
    if (message.subsidiaryNumber !== '') {
      writer.uint32(34).string(message.subsidiaryNumber);
    }
    if (message.identificationNumber !== '') {
      writer.uint32(42).string(message.identificationNumber);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Company {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseCompany();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.address = Address.decode(reader, reader.uint32());
          break;
        case 2:
          message.customsId = reader.string();
          break;
        case 3:
          message.name = reader.string();
          break;
        case 4:
          message.subsidiaryNumber = reader.string();
          break;
        case 5:
          message.identificationNumber = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Company {
    return {
      address: isSet(object.address) ? Address.fromJSON(object.address) : undefined,
      customsId: isSet(object.customsId) ? String(object.customsId) : '',
      name: isSet(object.name) ? String(object.name) : '',
      subsidiaryNumber: isSet(object.subsidiaryNumber) ? String(object.subsidiaryNumber) : '',
      identificationNumber: isSet(object.identificationNumber) ? String(object.identificationNumber) : '',
    };
  },

  toJSON(message: Company): unknown {
    const obj: any = {};
    message.address !== undefined && (obj.address = message.address ? Address.toJSON(message.address) : undefined);
    message.customsId !== undefined && (obj.customsId = message.customsId);
    message.name !== undefined && (obj.name = message.name);
    message.subsidiaryNumber !== undefined && (obj.subsidiaryNumber = message.subsidiaryNumber);
    message.identificationNumber !== undefined && (obj.identificationNumber = message.identificationNumber);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<Company>, I>>(object: I): Company {
    const message = createBaseCompany();
    message.address =
      object.address !== undefined && object.address !== null ? Address.fromPartial(object.address) : undefined;
    message.customsId = object.customsId ?? '';
    message.name = object.name ?? '';
    message.subsidiaryNumber = object.subsidiaryNumber ?? '';
    message.identificationNumber = object.identificationNumber ?? '';
    return message;
  },
};

function createBaseCustomsOffice(): CustomsOffice {
  return { address: undefined, customsOfficeCode: '', customsOfficeName: '' };
}

export const CustomsOffice = {
  encode(message: CustomsOffice, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.address !== undefined) {
      Address.encode(message.address, writer.uint32(10).fork()).ldelim();
    }
    if (message.customsOfficeCode !== '') {
      writer.uint32(18).string(message.customsOfficeCode);
    }
    if (message.customsOfficeName !== '') {
      writer.uint32(26).string(message.customsOfficeName);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): CustomsOffice {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseCustomsOffice();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.address = Address.decode(reader, reader.uint32());
          break;
        case 2:
          message.customsOfficeCode = reader.string();
          break;
        case 3:
          message.customsOfficeName = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CustomsOffice {
    return {
      address: isSet(object.address) ? Address.fromJSON(object.address) : undefined,
      customsOfficeCode: isSet(object.customsOfficeCode) ? String(object.customsOfficeCode) : '',
      customsOfficeName: isSet(object.customsOfficeName) ? String(object.customsOfficeName) : '',
    };
  },

  toJSON(message: CustomsOffice): unknown {
    const obj: any = {};
    message.address !== undefined && (obj.address = message.address ? Address.toJSON(message.address) : undefined);
    message.customsOfficeCode !== undefined && (obj.customsOfficeCode = message.customsOfficeCode);
    message.customsOfficeName !== undefined && (obj.customsOfficeName = message.customsOfficeName);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<CustomsOffice>, I>>(object: I): CustomsOffice {
    const message = createBaseCustomsOffice();
    message.address =
      object.address !== undefined && object.address !== null ? Address.fromPartial(object.address) : undefined;
    message.customsOfficeCode = object.customsOfficeCode ?? '';
    message.customsOfficeName = object.customsOfficeName ?? '';
    return message;
  },
};

function createBaseAddress(): Address {
  return { streetAndNumber: '', postcode: '', city: '', countryCode: '' };
}

export const Address = {
  encode(message: Address, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.streetAndNumber !== '') {
      writer.uint32(10).string(message.streetAndNumber);
    }
    if (message.postcode !== '') {
      writer.uint32(18).string(message.postcode);
    }
    if (message.city !== '') {
      writer.uint32(26).string(message.city);
    }
    if (message.countryCode !== '') {
      writer.uint32(34).string(message.countryCode);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Address {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseAddress();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.streetAndNumber = reader.string();
          break;
        case 2:
          message.postcode = reader.string();
          break;
        case 3:
          message.city = reader.string();
          break;
        case 4:
          message.countryCode = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Address {
    return {
      streetAndNumber: isSet(object.streetAndNumber) ? String(object.streetAndNumber) : '',
      postcode: isSet(object.postcode) ? String(object.postcode) : '',
      city: isSet(object.city) ? String(object.city) : '',
      countryCode: isSet(object.countryCode) ? String(object.countryCode) : '',
    };
  },

  toJSON(message: Address): unknown {
    const obj: any = {};
    message.streetAndNumber !== undefined && (obj.streetAndNumber = message.streetAndNumber);
    message.postcode !== undefined && (obj.postcode = message.postcode);
    message.city !== undefined && (obj.city = message.city);
    message.countryCode !== undefined && (obj.countryCode = message.countryCode);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<Address>, I>>(object: I): Address {
    const message = createBaseAddress();
    message.streetAndNumber = object.streetAndNumber ?? '';
    message.postcode = object.postcode ?? '';
    message.city = object.city ?? '';
    message.countryCode = object.countryCode ?? '';
    return message;
  },
};

function createBaseGoodsItem(): GoodsItem {
  return {
    documents: [],
    countryOfOrigin: '',
    sequenceNumber: 0,
    descriptionOfGoods: '',
    harmonizedSystemSubheadingCode: '',
    localClassificationCode: '',
    grossMass: '',
    netMass: '',
    numberOfPackages: 0,
    typeOfPackages: '',
    marksNumbers: '',
    containerId: '',
    consigneeOrderNumber: '',
    valueAtBorderExport: 0,
    valueAtBorderExportCurrency: '',
    amountInvoiced: 0,
    amountInvoicedCurrency: '',
    dangerousGoodsCode: 0,
  };
}

export const GoodsItem = {
  encode(message: GoodsItem, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.documents) {
      Document.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.countryOfOrigin !== '') {
      writer.uint32(18).string(message.countryOfOrigin);
    }
    if (message.sequenceNumber !== 0) {
      writer.uint32(24).int64(message.sequenceNumber);
    }
    if (message.descriptionOfGoods !== '') {
      writer.uint32(34).string(message.descriptionOfGoods);
    }
    if (message.harmonizedSystemSubheadingCode !== '') {
      writer.uint32(42).string(message.harmonizedSystemSubheadingCode);
    }
    if (message.localClassificationCode !== '') {
      writer.uint32(50).string(message.localClassificationCode);
    }
    if (message.grossMass !== '') {
      writer.uint32(58).string(message.grossMass);
    }
    if (message.netMass !== '') {
      writer.uint32(66).string(message.netMass);
    }
    if (message.numberOfPackages !== 0) {
      writer.uint32(72).int64(message.numberOfPackages);
    }
    if (message.typeOfPackages !== '') {
      writer.uint32(82).string(message.typeOfPackages);
    }
    if (message.marksNumbers !== '') {
      writer.uint32(90).string(message.marksNumbers);
    }
    if (message.containerId !== '') {
      writer.uint32(98).string(message.containerId);
    }
    if (message.consigneeOrderNumber !== '') {
      writer.uint32(106).string(message.consigneeOrderNumber);
    }
    if (message.valueAtBorderExport !== 0) {
      writer.uint32(112).int64(message.valueAtBorderExport);
    }
    if (message.valueAtBorderExportCurrency !== '') {
      writer.uint32(122).string(message.valueAtBorderExportCurrency);
    }
    if (message.amountInvoiced !== 0) {
      writer.uint32(128).int64(message.amountInvoiced);
    }
    if (message.amountInvoicedCurrency !== '') {
      writer.uint32(138).string(message.amountInvoicedCurrency);
    }
    if (message.dangerousGoodsCode !== 0) {
      writer.uint32(144).int64(message.dangerousGoodsCode);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): GoodsItem {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseGoodsItem();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.documents.push(Document.decode(reader, reader.uint32()));
          break;
        case 2:
          message.countryOfOrigin = reader.string();
          break;
        case 3:
          message.sequenceNumber = longToNumber(reader.int64() as Long);
          break;
        case 4:
          message.descriptionOfGoods = reader.string();
          break;
        case 5:
          message.harmonizedSystemSubheadingCode = reader.string();
          break;
        case 6:
          message.localClassificationCode = reader.string();
          break;
        case 7:
          message.grossMass = reader.string();
          break;
        case 8:
          message.netMass = reader.string();
          break;
        case 9:
          message.numberOfPackages = longToNumber(reader.int64() as Long);
          break;
        case 10:
          message.typeOfPackages = reader.string();
          break;
        case 11:
          message.marksNumbers = reader.string();
          break;
        case 12:
          message.containerId = reader.string();
          break;
        case 13:
          message.consigneeOrderNumber = reader.string();
          break;
        case 14:
          message.valueAtBorderExport = longToNumber(reader.int64() as Long);
          break;
        case 15:
          message.valueAtBorderExportCurrency = reader.string();
          break;
        case 16:
          message.amountInvoiced = longToNumber(reader.int64() as Long);
          break;
        case 17:
          message.amountInvoicedCurrency = reader.string();
          break;
        case 18:
          message.dangerousGoodsCode = longToNumber(reader.int64() as Long);
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GoodsItem {
    return {
      documents: Array.isArray(object?.documents) ? object.documents.map((e: any) => Document.fromJSON(e)) : [],
      countryOfOrigin: isSet(object.countryOfOrigin) ? String(object.countryOfOrigin) : '',
      sequenceNumber: isSet(object.sequenceNumber) ? Number(object.sequenceNumber) : 0,
      descriptionOfGoods: isSet(object.descriptionOfGoods) ? String(object.descriptionOfGoods) : '',
      harmonizedSystemSubheadingCode: isSet(object.harmonizedSystemSubheadingCode)
        ? String(object.harmonizedSystemSubheadingCode)
        : '',
      localClassificationCode: isSet(object.localClassificationCode) ? String(object.localClassificationCode) : '',
      grossMass: isSet(object.grossMass) ? String(object.grossMass) : '',
      netMass: isSet(object.netMass) ? String(object.netMass) : '',
      numberOfPackages: isSet(object.numberOfPackages) ? Number(object.numberOfPackages) : 0,
      typeOfPackages: isSet(object.typeOfPackages) ? String(object.typeOfPackages) : '',
      marksNumbers: isSet(object.marksNumbers) ? String(object.marksNumbers) : '',
      containerId: isSet(object.containerId) ? String(object.containerId) : '',
      consigneeOrderNumber: isSet(object.consigneeOrderNumber) ? String(object.consigneeOrderNumber) : '',
      valueAtBorderExport: isSet(object.valueAtBorderExport) ? Number(object.valueAtBorderExport) : 0,
      valueAtBorderExportCurrency: isSet(object.valueAtBorderExportCurrency)
        ? String(object.valueAtBorderExportCurrency)
        : '',
      amountInvoiced: isSet(object.amountInvoiced) ? Number(object.amountInvoiced) : 0,
      amountInvoicedCurrency: isSet(object.amountInvoicedCurrency) ? String(object.amountInvoicedCurrency) : '',
      dangerousGoodsCode: isSet(object.dangerousGoodsCode) ? Number(object.dangerousGoodsCode) : 0,
    };
  },

  toJSON(message: GoodsItem): unknown {
    const obj: any = {};
    if (message.documents) {
      obj.documents = message.documents.map((e) => (e ? Document.toJSON(e) : undefined));
    } else {
      obj.documents = [];
    }
    message.countryOfOrigin !== undefined && (obj.countryOfOrigin = message.countryOfOrigin);
    message.sequenceNumber !== undefined && (obj.sequenceNumber = Math.round(message.sequenceNumber));
    message.descriptionOfGoods !== undefined && (obj.descriptionOfGoods = message.descriptionOfGoods);
    message.harmonizedSystemSubheadingCode !== undefined &&
    (obj.harmonizedSystemSubheadingCode = message.harmonizedSystemSubheadingCode);
    message.localClassificationCode !== undefined && (obj.localClassificationCode = message.localClassificationCode);
    message.grossMass !== undefined && (obj.grossMass = message.grossMass);
    message.netMass !== undefined && (obj.netMass = message.netMass);
    message.numberOfPackages !== undefined && (obj.numberOfPackages = Math.round(message.numberOfPackages));
    message.typeOfPackages !== undefined && (obj.typeOfPackages = message.typeOfPackages);
    message.marksNumbers !== undefined && (obj.marksNumbers = message.marksNumbers);
    message.containerId !== undefined && (obj.containerId = message.containerId);
    message.consigneeOrderNumber !== undefined && (obj.consigneeOrderNumber = message.consigneeOrderNumber);
    message.valueAtBorderExport !== undefined && (obj.valueAtBorderExport = Math.round(message.valueAtBorderExport));
    message.valueAtBorderExportCurrency !== undefined &&
    (obj.valueAtBorderExportCurrency = message.valueAtBorderExportCurrency);
    message.amountInvoiced !== undefined && (obj.amountInvoiced = Math.round(message.amountInvoiced));
    message.amountInvoicedCurrency !== undefined && (obj.amountInvoicedCurrency = message.amountInvoicedCurrency);
    message.dangerousGoodsCode !== undefined && (obj.dangerousGoodsCode = Math.round(message.dangerousGoodsCode));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<GoodsItem>, I>>(object: I): GoodsItem {
    const message = createBaseGoodsItem();
    message.documents = object.documents?.map((e) => Document.fromPartial(e)) || [];
    message.countryOfOrigin = object.countryOfOrigin ?? '';
    message.sequenceNumber = object.sequenceNumber ?? 0;
    message.descriptionOfGoods = object.descriptionOfGoods ?? '';
    message.harmonizedSystemSubheadingCode = object.harmonizedSystemSubheadingCode ?? '';
    message.localClassificationCode = object.localClassificationCode ?? '';
    message.grossMass = object.grossMass ?? '';
    message.netMass = object.netMass ?? '';
    message.numberOfPackages = object.numberOfPackages ?? 0;
    message.typeOfPackages = object.typeOfPackages ?? '';
    message.marksNumbers = object.marksNumbers ?? '';
    message.containerId = object.containerId ?? '';
    message.consigneeOrderNumber = object.consigneeOrderNumber ?? '';
    message.valueAtBorderExport = object.valueAtBorderExport ?? 0;
    message.valueAtBorderExportCurrency = object.valueAtBorderExportCurrency ?? '';
    message.amountInvoiced = object.amountInvoiced ?? 0;
    message.amountInvoicedCurrency = object.amountInvoicedCurrency ?? '';
    message.dangerousGoodsCode = object.dangerousGoodsCode ?? 0;
    return message;
  },
};

function createBaseTransport(): Transport {
  return {
    modeOfTransport: 0,
    typeOfIdentification: 0,
    identity: '',
    nationality: '',
    transportCost: 0,
    transportCostCurrency: '',
    transportOrderNumber: '',
  };
}

export const Transport = {
  encode(message: Transport, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.modeOfTransport !== 0) {
      writer.uint32(8).int64(message.modeOfTransport);
    }
    if (message.typeOfIdentification !== 0) {
      writer.uint32(16).int64(message.typeOfIdentification);
    }
    if (message.identity !== '') {
      writer.uint32(26).string(message.identity);
    }
    if (message.nationality !== '') {
      writer.uint32(34).string(message.nationality);
    }
    if (message.transportCost !== 0) {
      writer.uint32(40).int64(message.transportCost);
    }
    if (message.transportCostCurrency !== '') {
      writer.uint32(50).string(message.transportCostCurrency);
    }
    if (message.transportOrderNumber !== '') {
      writer.uint32(58).string(message.transportOrderNumber);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Transport {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseTransport();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.modeOfTransport = longToNumber(reader.int64() as Long);
          break;
        case 2:
          message.typeOfIdentification = longToNumber(reader.int64() as Long);
          break;
        case 3:
          message.identity = reader.string();
          break;
        case 4:
          message.nationality = reader.string();
          break;
        case 5:
          message.transportCost = longToNumber(reader.int64() as Long);
          break;
        case 6:
          message.transportCostCurrency = reader.string();
          break;
        case 7:
          message.transportOrderNumber = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Transport {
    return {
      modeOfTransport: isSet(object.modeOfTransport) ? Number(object.modeOfTransport) : 0,
      typeOfIdentification: isSet(object.typeOfIdentification) ? Number(object.typeOfIdentification) : 0,
      identity: isSet(object.identity) ? String(object.identity) : '',
      nationality: isSet(object.nationality) ? String(object.nationality) : '',
      transportCost: isSet(object.transportCost) ? Number(object.transportCost) : 0,
      transportCostCurrency: isSet(object.transportCostCurrency) ? String(object.transportCostCurrency) : '',
      transportOrderNumber: isSet(object.transportOrderNumber) ? String(object.transportOrderNumber) : '',
    };
  },

  toJSON(message: Transport): unknown {
    const obj: any = {};
    message.modeOfTransport !== undefined && (obj.modeOfTransport = Math.round(message.modeOfTransport));
    message.typeOfIdentification !== undefined && (obj.typeOfIdentification = Math.round(message.typeOfIdentification));
    message.identity !== undefined && (obj.identity = message.identity);
    message.nationality !== undefined && (obj.nationality = message.nationality);
    message.transportCost !== undefined && (obj.transportCost = Math.round(message.transportCost));
    message.transportCostCurrency !== undefined && (obj.transportCostCurrency = message.transportCostCurrency);
    message.transportOrderNumber !== undefined && (obj.transportOrderNumber = message.transportOrderNumber);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<Transport>, I>>(object: I): Transport {
    const message = createBaseTransport();
    message.modeOfTransport = object.modeOfTransport ?? 0;
    message.typeOfIdentification = object.typeOfIdentification ?? 0;
    message.identity = object.identity ?? '';
    message.nationality = object.nationality ?? '';
    message.transportCost = object.transportCost ?? 0;
    message.transportCostCurrency = object.transportCostCurrency ?? '';
    message.transportOrderNumber = object.transportOrderNumber ?? '';
    return message;
  },
};

function createBaseDocument(): Document {
  return { type: '', referenceNumber: '', complement: '', detail: '' };
}

export const Document = {
  encode(message: Document, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.type !== '') {
      writer.uint32(10).string(message.type);
    }
    if (message.referenceNumber !== '') {
      writer.uint32(18).string(message.referenceNumber);
    }
    if (message.complement !== '') {
      writer.uint32(26).string(message.complement);
    }
    if (message.detail !== '') {
      writer.uint32(34).string(message.detail);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Document {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseDocument();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.type = reader.string();
          break;
        case 2:
          message.referenceNumber = reader.string();
          break;
        case 3:
          message.complement = reader.string();
          break;
        case 4:
          message.detail = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Document {
    return {
      type: isSet(object.type) ? String(object.type) : '',
      referenceNumber: isSet(object.referenceNumber) ? String(object.referenceNumber) : '',
      complement: isSet(object.complement) ? String(object.complement) : '',
      detail: isSet(object.detail) ? String(object.detail) : '',
    };
  },

  toJSON(message: Document): unknown {
    const obj: any = {};
    message.type !== undefined && (obj.type = message.type);
    message.referenceNumber !== undefined && (obj.referenceNumber = message.referenceNumber);
    message.complement !== undefined && (obj.complement = message.complement);
    message.detail !== undefined && (obj.detail = message.detail);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<Document>, I>>(object: I): Document {
    const message = createBaseDocument();
    message.type = object.type ?? '';
    message.referenceNumber = object.referenceNumber ?? '';
    message.complement = object.complement ?? '';
    message.detail = object.detail ?? '';
    return message;
  },
};

declare var self: any | undefined;
declare var window: any | undefined;
declare var global: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== 'undefined') {
    return globalThis;
  }
  if (typeof self !== 'undefined') {
    return self;
  }
  if (typeof window !== 'undefined') {
    return window;
  }
  if (typeof global !== 'undefined') {
    return global;
  }
  throw 'Unable to locate global object';
})();

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
    ? Array<DeepPartial<U>>
    : T extends ReadonlyArray<infer U>
      ? ReadonlyArray<DeepPartial<U>>
      : T extends {}
        ? { [K in keyof T]?: DeepPartial<T[K]> }
        : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin
  ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error('Value is larger than Number.MAX_SAFE_INTEGER');
  }
  return long.toNumber();
}

if (true) {
  _m0.util.Long = Long as any;
  _m0.configure();
}

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

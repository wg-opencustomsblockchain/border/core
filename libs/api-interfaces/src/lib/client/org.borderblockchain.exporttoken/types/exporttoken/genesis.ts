/* eslint-disable */
import _m0 from "protobufjs/minimal";
import { EventHistory } from "./event-history";
import { ExportMapping } from "./export-mapping";
import { ExportToken } from "./exporttoken";

export const protobufPackage = "org.borderblockchain.exporttoken";

/** GenesisState defines the capability module's genesis state. */
export interface GenesisState {
  /** this line is used by starport scaffolding # genesis/proto/state */
  eventHistoryList: EventHistory[];
  /** this line is used by starport scaffolding # genesis/proto/stateField */
  exportMappingList: ExportMapping[];
  /** this line is used by starport scaffolding # genesis/proto/stateField */
  exportTokenList: ExportToken[];
}

function createBaseGenesisState(): GenesisState {
  return { eventHistoryList: [], exportMappingList: [], exportTokenList: [] };
}

export const GenesisState = {
  encode(message: GenesisState, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.eventHistoryList) {
      EventHistory.encode(v!, writer.uint32(26).fork()).ldelim();
    }
    for (const v of message.exportMappingList) {
      ExportMapping.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    for (const v of message.exportTokenList) {
      ExportToken.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): GenesisState {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseGenesisState();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 3:
          message.eventHistoryList.push(EventHistory.decode(reader, reader.uint32()));
          break;
        case 2:
          message.exportMappingList.push(ExportMapping.decode(reader, reader.uint32()));
          break;
        case 1:
          message.exportTokenList.push(ExportToken.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GenesisState {
    return {
      eventHistoryList: Array.isArray(object?.eventHistoryList)
        ? object.eventHistoryList.map((e: any) => EventHistory.fromJSON(e))
        : [],
      exportMappingList: Array.isArray(object?.exportMappingList)
        ? object.exportMappingList.map((e: any) => ExportMapping.fromJSON(e))
        : [],
      exportTokenList: Array.isArray(object?.exportTokenList)
        ? object.exportTokenList.map((e: any) => ExportToken.fromJSON(e))
        : [],
    };
  },

  toJSON(message: GenesisState): unknown {
    const obj: any = {};
    if (message.eventHistoryList) {
      obj.eventHistoryList = message.eventHistoryList.map((e) => e ? EventHistory.toJSON(e) : undefined);
    } else {
      obj.eventHistoryList = [];
    }
    if (message.exportMappingList) {
      obj.exportMappingList = message.exportMappingList.map((e) => e ? ExportMapping.toJSON(e) : undefined);
    } else {
      obj.exportMappingList = [];
    }
    if (message.exportTokenList) {
      obj.exportTokenList = message.exportTokenList.map((e) => e ? ExportToken.toJSON(e) : undefined);
    } else {
      obj.exportTokenList = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<GenesisState>, I>>(object: I): GenesisState {
    const message = createBaseGenesisState();
    message.eventHistoryList = object.eventHistoryList?.map((e) => EventHistory.fromPartial(e)) || [];
    message.exportMappingList = object.exportMappingList?.map((e) => ExportMapping.fromPartial(e)) || [];
    message.exportTokenList = object.exportTokenList?.map((e) => ExportToken.fromPartial(e)) || [];
    return message;
  },
};

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

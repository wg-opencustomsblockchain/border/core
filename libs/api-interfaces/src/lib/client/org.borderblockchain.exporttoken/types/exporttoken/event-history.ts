/* eslint-disable */
import _m0 from "protobufjs/minimal";

export const protobufPackage = "org.borderblockchain.exporttoken";

/**
 * Creator: Cosmos Address
 * Status : Status enum from the Import Status Process.
 * Message: More Information about the Status Transaction.
 * Type : Type enum of all accepted state transitions/Transaction Type.
 */
export interface ExportEvent {
  creator: string;
  status: string;
  message: string;
  eventType: string;
  timestamp: string;
}

/**
 * Creator: Cosmos Address. Might be different from Import Event.
 * Index: The TokenId from the associated UUID of the Import Token.
 * Events: List of Events.
 */
export interface EventHistory {
  creator: string;
  index: string;
  events: ExportEvent[];
}

function createBaseExportEvent(): ExportEvent {
  return { creator: "", status: "", message: "", eventType: "", timestamp: "" };
}

export const ExportEvent = {
  encode(message: ExportEvent, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.status !== "") {
      writer.uint32(18).string(message.status);
    }
    if (message.message !== "") {
      writer.uint32(26).string(message.message);
    }
    if (message.eventType !== "") {
      writer.uint32(34).string(message.eventType);
    }
    if (message.timestamp !== "") {
      writer.uint32(42).string(message.timestamp);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ExportEvent {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseExportEvent();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.status = reader.string();
          break;
        case 3:
          message.message = reader.string();
          break;
        case 4:
          message.eventType = reader.string();
          break;
        case 5:
          message.timestamp = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ExportEvent {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      status: isSet(object.status) ? String(object.status) : "",
      message: isSet(object.message) ? String(object.message) : "",
      eventType: isSet(object.eventType) ? String(object.eventType) : "",
      timestamp: isSet(object.timestamp) ? String(object.timestamp) : "",
    };
  },

  toJSON(message: ExportEvent): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.status !== undefined && (obj.status = message.status);
    message.message !== undefined && (obj.message = message.message);
    message.eventType !== undefined && (obj.eventType = message.eventType);
    message.timestamp !== undefined && (obj.timestamp = message.timestamp);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<ExportEvent>, I>>(object: I): ExportEvent {
    const message = createBaseExportEvent();
    message.creator = object.creator ?? "";
    message.status = object.status ?? "";
    message.message = object.message ?? "";
    message.eventType = object.eventType ?? "";
    message.timestamp = object.timestamp ?? "";
    return message;
  },
};

function createBaseEventHistory(): EventHistory {
  return { creator: "", index: "", events: [] };
}

export const EventHistory = {
  encode(message: EventHistory, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.index !== "") {
      writer.uint32(18).string(message.index);
    }
    for (const v of message.events) {
      ExportEvent.encode(v!, writer.uint32(26).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): EventHistory {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseEventHistory();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.index = reader.string();
          break;
        case 3:
          message.events.push(ExportEvent.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): EventHistory {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      index: isSet(object.index) ? String(object.index) : "",
      events: Array.isArray(object?.events) ? object.events.map((e: any) => ExportEvent.fromJSON(e)) : [],
    };
  },

  toJSON(message: EventHistory): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.index !== undefined && (obj.index = message.index);
    if (message.events) {
      obj.events = message.events.map((e) => e ? ExportEvent.toJSON(e) : undefined);
    } else {
      obj.events = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<EventHistory>, I>>(object: I): EventHistory {
    const message = createBaseEventHistory();
    message.creator = object.creator ?? "";
    message.index = object.index ?? "";
    message.events = object.events?.map((e) => ExportEvent.fromPartial(e)) || [];
    return message;
  },
};

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

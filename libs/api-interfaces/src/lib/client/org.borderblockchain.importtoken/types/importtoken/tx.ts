/* eslint-disable */
import Long from "long";
import _m0 from "protobufjs/minimal";
import { ImportEvent } from "./event-history";
import { Company, CustomsOffice, GoodsItem, ImportToken, Transport } from "./importtoken";

export const protobufPackage = "org.borderblockchain.importtoken";

/**
 * This Messages is used in place of an update on the Token.
 * If you query a token the Event History will be appended and sent alongside the Token.
 */
export interface MsgCreateEvent {
  creator: string;
  index: string;
  status: string;
  message: string;
  eventType: string;
}

/** We might want to return a Response in the future. */
export interface MsgCreateEventResponse {
}

export interface MsgCreateImportToken {
  creator: string;
  id: string;
  consignor: Company | undefined;
  exporter: Company | undefined;
  consignee: Company | undefined;
  declarant: Company | undefined;
  customOfficeOfExit: CustomsOffice | undefined;
  customOfficeOfEntry: CustomsOffice | undefined;
  customOfficeOfImport: CustomsOffice | undefined;
  goodsItems: GoodsItem[];
  transportAtBorder: Transport | undefined;
  transportFromBorder: Transport | undefined;
  importId: string;
  uniqueConsignmentReference: string;
  localReferenceNumber: string;
  destinationCountry: string;
  exportCountry: string;
  itinerary: string;
  incotermCode: string;
  incotermLocation: string;
  totalGrossMass: string;
  goodsItemQuantity: number;
  totalPackagesQuantity: number;
  releaseDateAndTime: string;
  natureOfTransaction: number;
  totalAmountInvoiced: number;
  invoiceCurrency: string;
  status: string;
  relatedExportToken: string;
}

export interface MsgCreateImportTokenResponse {
  importToken: ImportToken | undefined;
  events: ImportEvent[];
}

export interface MsgUpdateImportToken {
  creator: string;
  id: string;
  consignor: Company | undefined;
  exporter: Company | undefined;
  consignee: Company | undefined;
  declarant: Company | undefined;
  customOfficeOfExit: CustomsOffice | undefined;
  customOfficeOfEntry: CustomsOffice | undefined;
  customOfficeOfImport: CustomsOffice | undefined;
  goodsItems: GoodsItem[];
  transportAtBorder: Transport | undefined;
  transportFromBorder: Transport | undefined;
  importId: string;
  uniqueConsignmentReference: string;
  localReferenceNumber: string;
  destinationCountry: string;
  exportCountry: string;
  itinerary: string;
  incotermCode: string;
  incotermLocation: string;
  totalGrossMass: string;
  goodsItemQuantity: number;
  totalPackagesQuantity: number;
  releaseDateAndTime: string;
  natureOfTransaction: number;
  totalAmountInvoiced: number;
  invoiceCurrency: string;
  status: string;
  relatedExportToken: string;
  eventType: string;
}

export interface MsgUpdateImportTokenResponse {
  importToken: ImportToken | undefined;
  events: ImportEvent[];
}

export interface MsgDeleteImportToken {
  creator: string;
  id: string;
}

export interface MsgDeleteImportTokenResponse {
}

export interface MsgFetchImportToken {
  creator: string;
  id: string;
}

export interface MsgFetchImportTokenResponse {
  ImportToken: ImportToken | undefined;
  events: ImportEvent[];
}

export interface MsgFetchImportMapping {
  creator: string;
  id: string;
}

export interface MsgFetchImportMappingResponse {
  ImportToken: ImportToken | undefined;
  events: ImportEvent[];
}

function createBaseMsgCreateEvent(): MsgCreateEvent {
  return { creator: "", index: "", status: "", message: "", eventType: "" };
}

export const MsgCreateEvent = {
  encode(message: MsgCreateEvent, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.index !== "") {
      writer.uint32(18).string(message.index);
    }
    if (message.status !== "") {
      writer.uint32(26).string(message.status);
    }
    if (message.message !== "") {
      writer.uint32(34).string(message.message);
    }
    if (message.eventType !== "") {
      writer.uint32(42).string(message.eventType);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateEvent {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateEvent();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.index = reader.string();
          break;
        case 3:
          message.status = reader.string();
          break;
        case 4:
          message.message = reader.string();
          break;
        case 5:
          message.eventType = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateEvent {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      index: isSet(object.index) ? String(object.index) : "",
      status: isSet(object.status) ? String(object.status) : "",
      message: isSet(object.message) ? String(object.message) : "",
      eventType: isSet(object.eventType) ? String(object.eventType) : "",
    };
  },

  toJSON(message: MsgCreateEvent): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.index !== undefined && (obj.index = message.index);
    message.status !== undefined && (obj.status = message.status);
    message.message !== undefined && (obj.message = message.message);
    message.eventType !== undefined && (obj.eventType = message.eventType);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateEvent>, I>>(object: I): MsgCreateEvent {
    const message = createBaseMsgCreateEvent();
    message.creator = object.creator ?? "";
    message.index = object.index ?? "";
    message.status = object.status ?? "";
    message.message = object.message ?? "";
    message.eventType = object.eventType ?? "";
    return message;
  },
};

function createBaseMsgCreateEventResponse(): MsgCreateEventResponse {
  return {};
}

export const MsgCreateEventResponse = {
  encode(_: MsgCreateEventResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateEventResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateEventResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgCreateEventResponse {
    return {};
  },

  toJSON(_: MsgCreateEventResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateEventResponse>, I>>(_: I): MsgCreateEventResponse {
    const message = createBaseMsgCreateEventResponse();
    return message;
  },
};

function createBaseMsgCreateImportToken(): MsgCreateImportToken {
  return {
    creator: "",
    id: "",
    consignor: undefined,
    exporter: undefined,
    consignee: undefined,
    declarant: undefined,
    customOfficeOfExit: undefined,
    customOfficeOfEntry: undefined,
    customOfficeOfImport: undefined,
    goodsItems: [],
    transportAtBorder: undefined,
    transportFromBorder: undefined,
    importId: "",
    uniqueConsignmentReference: "",
    localReferenceNumber: "",
    destinationCountry: "",
    exportCountry: "",
    itinerary: "",
    incotermCode: "",
    incotermLocation: "",
    totalGrossMass: "",
    goodsItemQuantity: 0,
    totalPackagesQuantity: 0,
    releaseDateAndTime: "",
    natureOfTransaction: 0,
    totalAmountInvoiced: 0,
    invoiceCurrency: "",
    status: "",
    relatedExportToken: "",
  };
}

export const MsgCreateImportToken = {
  encode(message: MsgCreateImportToken, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.consignor !== undefined) {
      Company.encode(message.consignor, writer.uint32(26).fork()).ldelim();
    }
    if (message.exporter !== undefined) {
      Company.encode(message.exporter, writer.uint32(34).fork()).ldelim();
    }
    if (message.consignee !== undefined) {
      Company.encode(message.consignee, writer.uint32(42).fork()).ldelim();
    }
    if (message.declarant !== undefined) {
      Company.encode(message.declarant, writer.uint32(50).fork()).ldelim();
    }
    if (message.customOfficeOfExit !== undefined) {
      CustomsOffice.encode(message.customOfficeOfExit, writer.uint32(58).fork()).ldelim();
    }
    if (message.customOfficeOfEntry !== undefined) {
      CustomsOffice.encode(message.customOfficeOfEntry, writer.uint32(66).fork()).ldelim();
    }
    if (message.customOfficeOfImport !== undefined) {
      CustomsOffice.encode(message.customOfficeOfImport, writer.uint32(74).fork()).ldelim();
    }
    for (const v of message.goodsItems) {
      GoodsItem.encode(v!, writer.uint32(82).fork()).ldelim();
    }
    if (message.transportAtBorder !== undefined) {
      Transport.encode(message.transportAtBorder, writer.uint32(90).fork()).ldelim();
    }
    if (message.transportFromBorder !== undefined) {
      Transport.encode(message.transportFromBorder, writer.uint32(98).fork()).ldelim();
    }
    if (message.importId !== "") {
      writer.uint32(106).string(message.importId);
    }
    if (message.uniqueConsignmentReference !== "") {
      writer.uint32(114).string(message.uniqueConsignmentReference);
    }
    if (message.localReferenceNumber !== "") {
      writer.uint32(122).string(message.localReferenceNumber);
    }
    if (message.destinationCountry !== "") {
      writer.uint32(130).string(message.destinationCountry);
    }
    if (message.exportCountry !== "") {
      writer.uint32(138).string(message.exportCountry);
    }
    if (message.itinerary !== "") {
      writer.uint32(146).string(message.itinerary);
    }
    if (message.incotermCode !== "") {
      writer.uint32(154).string(message.incotermCode);
    }
    if (message.incotermLocation !== "") {
      writer.uint32(162).string(message.incotermLocation);
    }
    if (message.totalGrossMass !== "") {
      writer.uint32(170).string(message.totalGrossMass);
    }
    if (message.goodsItemQuantity !== 0) {
      writer.uint32(176).int64(message.goodsItemQuantity);
    }
    if (message.totalPackagesQuantity !== 0) {
      writer.uint32(184).int64(message.totalPackagesQuantity);
    }
    if (message.releaseDateAndTime !== "") {
      writer.uint32(194).string(message.releaseDateAndTime);
    }
    if (message.natureOfTransaction !== 0) {
      writer.uint32(200).int64(message.natureOfTransaction);
    }
    if (message.totalAmountInvoiced !== 0) {
      writer.uint32(208).int64(message.totalAmountInvoiced);
    }
    if (message.invoiceCurrency !== "") {
      writer.uint32(218).string(message.invoiceCurrency);
    }
    if (message.status !== "") {
      writer.uint32(226).string(message.status);
    }
    if (message.relatedExportToken !== "") {
      writer.uint32(234).string(message.relatedExportToken);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateImportToken {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateImportToken();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.consignor = Company.decode(reader, reader.uint32());
          break;
        case 4:
          message.exporter = Company.decode(reader, reader.uint32());
          break;
        case 5:
          message.consignee = Company.decode(reader, reader.uint32());
          break;
        case 6:
          message.declarant = Company.decode(reader, reader.uint32());
          break;
        case 7:
          message.customOfficeOfExit = CustomsOffice.decode(reader, reader.uint32());
          break;
        case 8:
          message.customOfficeOfEntry = CustomsOffice.decode(reader, reader.uint32());
          break;
        case 9:
          message.customOfficeOfImport = CustomsOffice.decode(reader, reader.uint32());
          break;
        case 10:
          message.goodsItems.push(GoodsItem.decode(reader, reader.uint32()));
          break;
        case 11:
          message.transportAtBorder = Transport.decode(reader, reader.uint32());
          break;
        case 12:
          message.transportFromBorder = Transport.decode(reader, reader.uint32());
          break;
        case 13:
          message.importId = reader.string();
          break;
        case 14:
          message.uniqueConsignmentReference = reader.string();
          break;
        case 15:
          message.localReferenceNumber = reader.string();
          break;
        case 16:
          message.destinationCountry = reader.string();
          break;
        case 17:
          message.exportCountry = reader.string();
          break;
        case 18:
          message.itinerary = reader.string();
          break;
        case 19:
          message.incotermCode = reader.string();
          break;
        case 20:
          message.incotermLocation = reader.string();
          break;
        case 21:
          message.totalGrossMass = reader.string();
          break;
        case 22:
          message.goodsItemQuantity = longToNumber(reader.int64() as Long);
          break;
        case 23:
          message.totalPackagesQuantity = longToNumber(reader.int64() as Long);
          break;
        case 24:
          message.releaseDateAndTime = reader.string();
          break;
        case 25:
          message.natureOfTransaction = longToNumber(reader.int64() as Long);
          break;
        case 26:
          message.totalAmountInvoiced = longToNumber(reader.int64() as Long);
          break;
        case 27:
          message.invoiceCurrency = reader.string();
          break;
        case 28:
          message.status = reader.string();
          break;
        case 29:
          message.relatedExportToken = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateImportToken {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      consignor: isSet(object.consignor) ? Company.fromJSON(object.consignor) : undefined,
      exporter: isSet(object.exporter) ? Company.fromJSON(object.exporter) : undefined,
      consignee: isSet(object.consignee) ? Company.fromJSON(object.consignee) : undefined,
      declarant: isSet(object.declarant) ? Company.fromJSON(object.declarant) : undefined,
      customOfficeOfExit: isSet(object.customOfficeOfExit)
        ? CustomsOffice.fromJSON(object.customOfficeOfExit)
        : undefined,
      customOfficeOfEntry: isSet(object.customOfficeOfEntry)
        ? CustomsOffice.fromJSON(object.customOfficeOfEntry)
        : undefined,
      customOfficeOfImport: isSet(object.customOfficeOfImport)
        ? CustomsOffice.fromJSON(object.customOfficeOfImport)
        : undefined,
      goodsItems: Array.isArray(object?.goodsItems) ? object.goodsItems.map((e: any) => GoodsItem.fromJSON(e)) : [],
      transportAtBorder: isSet(object.transportAtBorder) ? Transport.fromJSON(object.transportAtBorder) : undefined,
      transportFromBorder: isSet(object.transportFromBorder)
        ? Transport.fromJSON(object.transportFromBorder)
        : undefined,
      importId: isSet(object.importId) ? String(object.importId) : "",
      uniqueConsignmentReference: isSet(object.uniqueConsignmentReference)
        ? String(object.uniqueConsignmentReference)
        : "",
      localReferenceNumber: isSet(object.localReferenceNumber) ? String(object.localReferenceNumber) : "",
      destinationCountry: isSet(object.destinationCountry) ? String(object.destinationCountry) : "",
      exportCountry: isSet(object.exportCountry) ? String(object.exportCountry) : "",
      itinerary: isSet(object.itinerary) ? String(object.itinerary) : "",
      incotermCode: isSet(object.incotermCode) ? String(object.incotermCode) : "",
      incotermLocation: isSet(object.incotermLocation) ? String(object.incotermLocation) : "",
      totalGrossMass: isSet(object.totalGrossMass) ? String(object.totalGrossMass) : "",
      goodsItemQuantity: isSet(object.goodsItemQuantity) ? Number(object.goodsItemQuantity) : 0,
      totalPackagesQuantity: isSet(object.totalPackagesQuantity) ? Number(object.totalPackagesQuantity) : 0,
      releaseDateAndTime: isSet(object.releaseDateAndTime) ? String(object.releaseDateAndTime) : "",
      natureOfTransaction: isSet(object.natureOfTransaction) ? Number(object.natureOfTransaction) : 0,
      totalAmountInvoiced: isSet(object.totalAmountInvoiced) ? Number(object.totalAmountInvoiced) : 0,
      invoiceCurrency: isSet(object.invoiceCurrency) ? String(object.invoiceCurrency) : "",
      status: isSet(object.status) ? String(object.status) : "",
      relatedExportToken: isSet(object.relatedExportToken) ? String(object.relatedExportToken) : "",
    };
  },

  toJSON(message: MsgCreateImportToken): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.consignor !== undefined
      && (obj.consignor = message.consignor ? Company.toJSON(message.consignor) : undefined);
    message.exporter !== undefined && (obj.exporter = message.exporter ? Company.toJSON(message.exporter) : undefined);
    message.consignee !== undefined
      && (obj.consignee = message.consignee ? Company.toJSON(message.consignee) : undefined);
    message.declarant !== undefined
      && (obj.declarant = message.declarant ? Company.toJSON(message.declarant) : undefined);
    message.customOfficeOfExit !== undefined && (obj.customOfficeOfExit = message.customOfficeOfExit
      ? CustomsOffice.toJSON(message.customOfficeOfExit)
      : undefined);
    message.customOfficeOfEntry !== undefined && (obj.customOfficeOfEntry = message.customOfficeOfEntry
      ? CustomsOffice.toJSON(message.customOfficeOfEntry)
      : undefined);
    message.customOfficeOfImport !== undefined && (obj.customOfficeOfImport = message.customOfficeOfImport
      ? CustomsOffice.toJSON(message.customOfficeOfImport)
      : undefined);
    if (message.goodsItems) {
      obj.goodsItems = message.goodsItems.map((e) => e ? GoodsItem.toJSON(e) : undefined);
    } else {
      obj.goodsItems = [];
    }
    message.transportAtBorder !== undefined
      && (obj.transportAtBorder = message.transportAtBorder ? Transport.toJSON(message.transportAtBorder) : undefined);
    message.transportFromBorder !== undefined && (obj.transportFromBorder = message.transportFromBorder
      ? Transport.toJSON(message.transportFromBorder)
      : undefined);
    message.importId !== undefined && (obj.importId = message.importId);
    message.uniqueConsignmentReference !== undefined
      && (obj.uniqueConsignmentReference = message.uniqueConsignmentReference);
    message.localReferenceNumber !== undefined && (obj.localReferenceNumber = message.localReferenceNumber);
    message.destinationCountry !== undefined && (obj.destinationCountry = message.destinationCountry);
    message.exportCountry !== undefined && (obj.exportCountry = message.exportCountry);
    message.itinerary !== undefined && (obj.itinerary = message.itinerary);
    message.incotermCode !== undefined && (obj.incotermCode = message.incotermCode);
    message.incotermLocation !== undefined && (obj.incotermLocation = message.incotermLocation);
    message.totalGrossMass !== undefined && (obj.totalGrossMass = message.totalGrossMass);
    message.goodsItemQuantity !== undefined && (obj.goodsItemQuantity = Math.round(message.goodsItemQuantity));
    message.totalPackagesQuantity !== undefined
      && (obj.totalPackagesQuantity = Math.round(message.totalPackagesQuantity));
    message.releaseDateAndTime !== undefined && (obj.releaseDateAndTime = message.releaseDateAndTime);
    message.natureOfTransaction !== undefined && (obj.natureOfTransaction = Math.round(message.natureOfTransaction));
    message.totalAmountInvoiced !== undefined && (obj.totalAmountInvoiced = Math.round(message.totalAmountInvoiced));
    message.invoiceCurrency !== undefined && (obj.invoiceCurrency = message.invoiceCurrency);
    message.status !== undefined && (obj.status = message.status);
    message.relatedExportToken !== undefined && (obj.relatedExportToken = message.relatedExportToken);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateImportToken>, I>>(object: I): MsgCreateImportToken {
    const message = createBaseMsgCreateImportToken();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.consignor = (object.consignor !== undefined && object.consignor !== null)
      ? Company.fromPartial(object.consignor)
      : undefined;
    message.exporter = (object.exporter !== undefined && object.exporter !== null)
      ? Company.fromPartial(object.exporter)
      : undefined;
    message.consignee = (object.consignee !== undefined && object.consignee !== null)
      ? Company.fromPartial(object.consignee)
      : undefined;
    message.declarant = (object.declarant !== undefined && object.declarant !== null)
      ? Company.fromPartial(object.declarant)
      : undefined;
    message.customOfficeOfExit = (object.customOfficeOfExit !== undefined && object.customOfficeOfExit !== null)
      ? CustomsOffice.fromPartial(object.customOfficeOfExit)
      : undefined;
    message.customOfficeOfEntry = (object.customOfficeOfEntry !== undefined && object.customOfficeOfEntry !== null)
      ? CustomsOffice.fromPartial(object.customOfficeOfEntry)
      : undefined;
    message.customOfficeOfImport = (object.customOfficeOfImport !== undefined && object.customOfficeOfImport !== null)
      ? CustomsOffice.fromPartial(object.customOfficeOfImport)
      : undefined;
    message.goodsItems = object.goodsItems?.map((e) => GoodsItem.fromPartial(e)) || [];
    message.transportAtBorder = (object.transportAtBorder !== undefined && object.transportAtBorder !== null)
      ? Transport.fromPartial(object.transportAtBorder)
      : undefined;
    message.transportFromBorder = (object.transportFromBorder !== undefined && object.transportFromBorder !== null)
      ? Transport.fromPartial(object.transportFromBorder)
      : undefined;
    message.importId = object.importId ?? "";
    message.uniqueConsignmentReference = object.uniqueConsignmentReference ?? "";
    message.localReferenceNumber = object.localReferenceNumber ?? "";
    message.destinationCountry = object.destinationCountry ?? "";
    message.exportCountry = object.exportCountry ?? "";
    message.itinerary = object.itinerary ?? "";
    message.incotermCode = object.incotermCode ?? "";
    message.incotermLocation = object.incotermLocation ?? "";
    message.totalGrossMass = object.totalGrossMass ?? "";
    message.goodsItemQuantity = object.goodsItemQuantity ?? 0;
    message.totalPackagesQuantity = object.totalPackagesQuantity ?? 0;
    message.releaseDateAndTime = object.releaseDateAndTime ?? "";
    message.natureOfTransaction = object.natureOfTransaction ?? 0;
    message.totalAmountInvoiced = object.totalAmountInvoiced ?? 0;
    message.invoiceCurrency = object.invoiceCurrency ?? "";
    message.status = object.status ?? "";
    message.relatedExportToken = object.relatedExportToken ?? "";
    return message;
  },
};

function createBaseMsgCreateImportTokenResponse(): MsgCreateImportTokenResponse {
  return { importToken: undefined, events: [] };
}

export const MsgCreateImportTokenResponse = {
  encode(message: MsgCreateImportTokenResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.importToken !== undefined) {
      ImportToken.encode(message.importToken, writer.uint32(10).fork()).ldelim();
    }
    for (const v of message.events) {
      ImportEvent.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateImportTokenResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateImportTokenResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.importToken = ImportToken.decode(reader, reader.uint32());
          break;
        case 2:
          message.events.push(ImportEvent.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateImportTokenResponse {
    return {
      importToken: isSet(object.importToken) ? ImportToken.fromJSON(object.importToken) : undefined,
      events: Array.isArray(object?.events) ? object.events.map((e: any) => ImportEvent.fromJSON(e)) : [],
    };
  },

  toJSON(message: MsgCreateImportTokenResponse): unknown {
    const obj: any = {};
    message.importToken !== undefined
      && (obj.importToken = message.importToken ? ImportToken.toJSON(message.importToken) : undefined);
    if (message.events) {
      obj.events = message.events.map((e) => e ? ImportEvent.toJSON(e) : undefined);
    } else {
      obj.events = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateImportTokenResponse>, I>>(object: I): MsgCreateImportTokenResponse {
    const message = createBaseMsgCreateImportTokenResponse();
    message.importToken = (object.importToken !== undefined && object.importToken !== null)
      ? ImportToken.fromPartial(object.importToken)
      : undefined;
    message.events = object.events?.map((e) => ImportEvent.fromPartial(e)) || [];
    return message;
  },
};

function createBaseMsgUpdateImportToken(): MsgUpdateImportToken {
  return {
    creator: "",
    id: "",
    consignor: undefined,
    exporter: undefined,
    consignee: undefined,
    declarant: undefined,
    customOfficeOfExit: undefined,
    customOfficeOfEntry: undefined,
    customOfficeOfImport: undefined,
    goodsItems: [],
    transportAtBorder: undefined,
    transportFromBorder: undefined,
    importId: "",
    uniqueConsignmentReference: "",
    localReferenceNumber: "",
    destinationCountry: "",
    exportCountry: "",
    itinerary: "",
    incotermCode: "",
    incotermLocation: "",
    totalGrossMass: "",
    goodsItemQuantity: 0,
    totalPackagesQuantity: 0,
    releaseDateAndTime: "",
    natureOfTransaction: 0,
    totalAmountInvoiced: 0,
    invoiceCurrency: "",
    status: "",
    relatedExportToken: "",
    eventType: "",
  };
}

export const MsgUpdateImportToken = {
  encode(message: MsgUpdateImportToken, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.consignor !== undefined) {
      Company.encode(message.consignor, writer.uint32(26).fork()).ldelim();
    }
    if (message.exporter !== undefined) {
      Company.encode(message.exporter, writer.uint32(34).fork()).ldelim();
    }
    if (message.consignee !== undefined) {
      Company.encode(message.consignee, writer.uint32(42).fork()).ldelim();
    }
    if (message.declarant !== undefined) {
      Company.encode(message.declarant, writer.uint32(50).fork()).ldelim();
    }
    if (message.customOfficeOfExit !== undefined) {
      CustomsOffice.encode(message.customOfficeOfExit, writer.uint32(58).fork()).ldelim();
    }
    if (message.customOfficeOfEntry !== undefined) {
      CustomsOffice.encode(message.customOfficeOfEntry, writer.uint32(66).fork()).ldelim();
    }
    if (message.customOfficeOfImport !== undefined) {
      CustomsOffice.encode(message.customOfficeOfImport, writer.uint32(74).fork()).ldelim();
    }
    for (const v of message.goodsItems) {
      GoodsItem.encode(v!, writer.uint32(82).fork()).ldelim();
    }
    if (message.transportAtBorder !== undefined) {
      Transport.encode(message.transportAtBorder, writer.uint32(90).fork()).ldelim();
    }
    if (message.transportFromBorder !== undefined) {
      Transport.encode(message.transportFromBorder, writer.uint32(98).fork()).ldelim();
    }
    if (message.importId !== "") {
      writer.uint32(106).string(message.importId);
    }
    if (message.uniqueConsignmentReference !== "") {
      writer.uint32(114).string(message.uniqueConsignmentReference);
    }
    if (message.localReferenceNumber !== "") {
      writer.uint32(122).string(message.localReferenceNumber);
    }
    if (message.destinationCountry !== "") {
      writer.uint32(130).string(message.destinationCountry);
    }
    if (message.exportCountry !== "") {
      writer.uint32(138).string(message.exportCountry);
    }
    if (message.itinerary !== "") {
      writer.uint32(146).string(message.itinerary);
    }
    if (message.incotermCode !== "") {
      writer.uint32(154).string(message.incotermCode);
    }
    if (message.incotermLocation !== "") {
      writer.uint32(162).string(message.incotermLocation);
    }
    if (message.totalGrossMass !== "") {
      writer.uint32(170).string(message.totalGrossMass);
    }
    if (message.goodsItemQuantity !== 0) {
      writer.uint32(176).int64(message.goodsItemQuantity);
    }
    if (message.totalPackagesQuantity !== 0) {
      writer.uint32(184).int64(message.totalPackagesQuantity);
    }
    if (message.releaseDateAndTime !== "") {
      writer.uint32(194).string(message.releaseDateAndTime);
    }
    if (message.natureOfTransaction !== 0) {
      writer.uint32(200).int64(message.natureOfTransaction);
    }
    if (message.totalAmountInvoiced !== 0) {
      writer.uint32(208).int64(message.totalAmountInvoiced);
    }
    if (message.invoiceCurrency !== "") {
      writer.uint32(218).string(message.invoiceCurrency);
    }
    if (message.status !== "") {
      writer.uint32(226).string(message.status);
    }
    if (message.relatedExportToken !== "") {
      writer.uint32(234).string(message.relatedExportToken);
    }
    if (message.eventType !== "") {
      writer.uint32(242).string(message.eventType);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgUpdateImportToken {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgUpdateImportToken();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.consignor = Company.decode(reader, reader.uint32());
          break;
        case 4:
          message.exporter = Company.decode(reader, reader.uint32());
          break;
        case 5:
          message.consignee = Company.decode(reader, reader.uint32());
          break;
        case 6:
          message.declarant = Company.decode(reader, reader.uint32());
          break;
        case 7:
          message.customOfficeOfExit = CustomsOffice.decode(reader, reader.uint32());
          break;
        case 8:
          message.customOfficeOfEntry = CustomsOffice.decode(reader, reader.uint32());
          break;
        case 9:
          message.customOfficeOfImport = CustomsOffice.decode(reader, reader.uint32());
          break;
        case 10:
          message.goodsItems.push(GoodsItem.decode(reader, reader.uint32()));
          break;
        case 11:
          message.transportAtBorder = Transport.decode(reader, reader.uint32());
          break;
        case 12:
          message.transportFromBorder = Transport.decode(reader, reader.uint32());
          break;
        case 13:
          message.importId = reader.string();
          break;
        case 14:
          message.uniqueConsignmentReference = reader.string();
          break;
        case 15:
          message.localReferenceNumber = reader.string();
          break;
        case 16:
          message.destinationCountry = reader.string();
          break;
        case 17:
          message.exportCountry = reader.string();
          break;
        case 18:
          message.itinerary = reader.string();
          break;
        case 19:
          message.incotermCode = reader.string();
          break;
        case 20:
          message.incotermLocation = reader.string();
          break;
        case 21:
          message.totalGrossMass = reader.string();
          break;
        case 22:
          message.goodsItemQuantity = longToNumber(reader.int64() as Long);
          break;
        case 23:
          message.totalPackagesQuantity = longToNumber(reader.int64() as Long);
          break;
        case 24:
          message.releaseDateAndTime = reader.string();
          break;
        case 25:
          message.natureOfTransaction = longToNumber(reader.int64() as Long);
          break;
        case 26:
          message.totalAmountInvoiced = longToNumber(reader.int64() as Long);
          break;
        case 27:
          message.invoiceCurrency = reader.string();
          break;
        case 28:
          message.status = reader.string();
          break;
        case 29:
          message.relatedExportToken = reader.string();
          break;
        case 30:
          message.eventType = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateImportToken {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      consignor: isSet(object.consignor) ? Company.fromJSON(object.consignor) : undefined,
      exporter: isSet(object.exporter) ? Company.fromJSON(object.exporter) : undefined,
      consignee: isSet(object.consignee) ? Company.fromJSON(object.consignee) : undefined,
      declarant: isSet(object.declarant) ? Company.fromJSON(object.declarant) : undefined,
      customOfficeOfExit: isSet(object.customOfficeOfExit)
        ? CustomsOffice.fromJSON(object.customOfficeOfExit)
        : undefined,
      customOfficeOfEntry: isSet(object.customOfficeOfEntry)
        ? CustomsOffice.fromJSON(object.customOfficeOfEntry)
        : undefined,
      customOfficeOfImport: isSet(object.customOfficeOfImport)
        ? CustomsOffice.fromJSON(object.customOfficeOfImport)
        : undefined,
      goodsItems: Array.isArray(object?.goodsItems) ? object.goodsItems.map((e: any) => GoodsItem.fromJSON(e)) : [],
      transportAtBorder: isSet(object.transportAtBorder) ? Transport.fromJSON(object.transportAtBorder) : undefined,
      transportFromBorder: isSet(object.transportFromBorder)
        ? Transport.fromJSON(object.transportFromBorder)
        : undefined,
      importId: isSet(object.importId) ? String(object.importId) : "",
      uniqueConsignmentReference: isSet(object.uniqueConsignmentReference)
        ? String(object.uniqueConsignmentReference)
        : "",
      localReferenceNumber: isSet(object.localReferenceNumber) ? String(object.localReferenceNumber) : "",
      destinationCountry: isSet(object.destinationCountry) ? String(object.destinationCountry) : "",
      exportCountry: isSet(object.exportCountry) ? String(object.exportCountry) : "",
      itinerary: isSet(object.itinerary) ? String(object.itinerary) : "",
      incotermCode: isSet(object.incotermCode) ? String(object.incotermCode) : "",
      incotermLocation: isSet(object.incotermLocation) ? String(object.incotermLocation) : "",
      totalGrossMass: isSet(object.totalGrossMass) ? String(object.totalGrossMass) : "",
      goodsItemQuantity: isSet(object.goodsItemQuantity) ? Number(object.goodsItemQuantity) : 0,
      totalPackagesQuantity: isSet(object.totalPackagesQuantity) ? Number(object.totalPackagesQuantity) : 0,
      releaseDateAndTime: isSet(object.releaseDateAndTime) ? String(object.releaseDateAndTime) : "",
      natureOfTransaction: isSet(object.natureOfTransaction) ? Number(object.natureOfTransaction) : 0,
      totalAmountInvoiced: isSet(object.totalAmountInvoiced) ? Number(object.totalAmountInvoiced) : 0,
      invoiceCurrency: isSet(object.invoiceCurrency) ? String(object.invoiceCurrency) : "",
      status: isSet(object.status) ? String(object.status) : "",
      relatedExportToken: isSet(object.relatedExportToken) ? String(object.relatedExportToken) : "",
      eventType: isSet(object.eventType) ? String(object.eventType) : "",
    };
  },

  toJSON(message: MsgUpdateImportToken): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.consignor !== undefined
      && (obj.consignor = message.consignor ? Company.toJSON(message.consignor) : undefined);
    message.exporter !== undefined && (obj.exporter = message.exporter ? Company.toJSON(message.exporter) : undefined);
    message.consignee !== undefined
      && (obj.consignee = message.consignee ? Company.toJSON(message.consignee) : undefined);
    message.declarant !== undefined
      && (obj.declarant = message.declarant ? Company.toJSON(message.declarant) : undefined);
    message.customOfficeOfExit !== undefined && (obj.customOfficeOfExit = message.customOfficeOfExit
      ? CustomsOffice.toJSON(message.customOfficeOfExit)
      : undefined);
    message.customOfficeOfEntry !== undefined && (obj.customOfficeOfEntry = message.customOfficeOfEntry
      ? CustomsOffice.toJSON(message.customOfficeOfEntry)
      : undefined);
    message.customOfficeOfImport !== undefined && (obj.customOfficeOfImport = message.customOfficeOfImport
      ? CustomsOffice.toJSON(message.customOfficeOfImport)
      : undefined);
    if (message.goodsItems) {
      obj.goodsItems = message.goodsItems.map((e) => e ? GoodsItem.toJSON(e) : undefined);
    } else {
      obj.goodsItems = [];
    }
    message.transportAtBorder !== undefined
      && (obj.transportAtBorder = message.transportAtBorder ? Transport.toJSON(message.transportAtBorder) : undefined);
    message.transportFromBorder !== undefined && (obj.transportFromBorder = message.transportFromBorder
      ? Transport.toJSON(message.transportFromBorder)
      : undefined);
    message.importId !== undefined && (obj.importId = message.importId);
    message.uniqueConsignmentReference !== undefined
      && (obj.uniqueConsignmentReference = message.uniqueConsignmentReference);
    message.localReferenceNumber !== undefined && (obj.localReferenceNumber = message.localReferenceNumber);
    message.destinationCountry !== undefined && (obj.destinationCountry = message.destinationCountry);
    message.exportCountry !== undefined && (obj.exportCountry = message.exportCountry);
    message.itinerary !== undefined && (obj.itinerary = message.itinerary);
    message.incotermCode !== undefined && (obj.incotermCode = message.incotermCode);
    message.incotermLocation !== undefined && (obj.incotermLocation = message.incotermLocation);
    message.totalGrossMass !== undefined && (obj.totalGrossMass = message.totalGrossMass);
    message.goodsItemQuantity !== undefined && (obj.goodsItemQuantity = Math.round(message.goodsItemQuantity));
    message.totalPackagesQuantity !== undefined
      && (obj.totalPackagesQuantity = Math.round(message.totalPackagesQuantity));
    message.releaseDateAndTime !== undefined && (obj.releaseDateAndTime = message.releaseDateAndTime);
    message.natureOfTransaction !== undefined && (obj.natureOfTransaction = Math.round(message.natureOfTransaction));
    message.totalAmountInvoiced !== undefined && (obj.totalAmountInvoiced = Math.round(message.totalAmountInvoiced));
    message.invoiceCurrency !== undefined && (obj.invoiceCurrency = message.invoiceCurrency);
    message.status !== undefined && (obj.status = message.status);
    message.relatedExportToken !== undefined && (obj.relatedExportToken = message.relatedExportToken);
    message.eventType !== undefined && (obj.eventType = message.eventType);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgUpdateImportToken>, I>>(object: I): MsgUpdateImportToken {
    const message = createBaseMsgUpdateImportToken();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.consignor = (object.consignor !== undefined && object.consignor !== null)
      ? Company.fromPartial(object.consignor)
      : undefined;
    message.exporter = (object.exporter !== undefined && object.exporter !== null)
      ? Company.fromPartial(object.exporter)
      : undefined;
    message.consignee = (object.consignee !== undefined && object.consignee !== null)
      ? Company.fromPartial(object.consignee)
      : undefined;
    message.declarant = (object.declarant !== undefined && object.declarant !== null)
      ? Company.fromPartial(object.declarant)
      : undefined;
    message.customOfficeOfExit = (object.customOfficeOfExit !== undefined && object.customOfficeOfExit !== null)
      ? CustomsOffice.fromPartial(object.customOfficeOfExit)
      : undefined;
    message.customOfficeOfEntry = (object.customOfficeOfEntry !== undefined && object.customOfficeOfEntry !== null)
      ? CustomsOffice.fromPartial(object.customOfficeOfEntry)
      : undefined;
    message.customOfficeOfImport = (object.customOfficeOfImport !== undefined && object.customOfficeOfImport !== null)
      ? CustomsOffice.fromPartial(object.customOfficeOfImport)
      : undefined;
    message.goodsItems = object.goodsItems?.map((e) => GoodsItem.fromPartial(e)) || [];
    message.transportAtBorder = (object.transportAtBorder !== undefined && object.transportAtBorder !== null)
      ? Transport.fromPartial(object.transportAtBorder)
      : undefined;
    message.transportFromBorder = (object.transportFromBorder !== undefined && object.transportFromBorder !== null)
      ? Transport.fromPartial(object.transportFromBorder)
      : undefined;
    message.importId = object.importId ?? "";
    message.uniqueConsignmentReference = object.uniqueConsignmentReference ?? "";
    message.localReferenceNumber = object.localReferenceNumber ?? "";
    message.destinationCountry = object.destinationCountry ?? "";
    message.exportCountry = object.exportCountry ?? "";
    message.itinerary = object.itinerary ?? "";
    message.incotermCode = object.incotermCode ?? "";
    message.incotermLocation = object.incotermLocation ?? "";
    message.totalGrossMass = object.totalGrossMass ?? "";
    message.goodsItemQuantity = object.goodsItemQuantity ?? 0;
    message.totalPackagesQuantity = object.totalPackagesQuantity ?? 0;
    message.releaseDateAndTime = object.releaseDateAndTime ?? "";
    message.natureOfTransaction = object.natureOfTransaction ?? 0;
    message.totalAmountInvoiced = object.totalAmountInvoiced ?? 0;
    message.invoiceCurrency = object.invoiceCurrency ?? "";
    message.status = object.status ?? "";
    message.relatedExportToken = object.relatedExportToken ?? "";
    message.eventType = object.eventType ?? "";
    return message;
  },
};

function createBaseMsgUpdateImportTokenResponse(): MsgUpdateImportTokenResponse {
  return { importToken: undefined, events: [] };
}

export const MsgUpdateImportTokenResponse = {
  encode(message: MsgUpdateImportTokenResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.importToken !== undefined) {
      ImportToken.encode(message.importToken, writer.uint32(10).fork()).ldelim();
    }
    for (const v of message.events) {
      ImportEvent.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgUpdateImportTokenResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgUpdateImportTokenResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.importToken = ImportToken.decode(reader, reader.uint32());
          break;
        case 2:
          message.events.push(ImportEvent.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateImportTokenResponse {
    return {
      importToken: isSet(object.importToken) ? ImportToken.fromJSON(object.importToken) : undefined,
      events: Array.isArray(object?.events) ? object.events.map((e: any) => ImportEvent.fromJSON(e)) : [],
    };
  },

  toJSON(message: MsgUpdateImportTokenResponse): unknown {
    const obj: any = {};
    message.importToken !== undefined
      && (obj.importToken = message.importToken ? ImportToken.toJSON(message.importToken) : undefined);
    if (message.events) {
      obj.events = message.events.map((e) => e ? ImportEvent.toJSON(e) : undefined);
    } else {
      obj.events = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgUpdateImportTokenResponse>, I>>(object: I): MsgUpdateImportTokenResponse {
    const message = createBaseMsgUpdateImportTokenResponse();
    message.importToken = (object.importToken !== undefined && object.importToken !== null)
      ? ImportToken.fromPartial(object.importToken)
      : undefined;
    message.events = object.events?.map((e) => ImportEvent.fromPartial(e)) || [];
    return message;
  },
};

function createBaseMsgDeleteImportToken(): MsgDeleteImportToken {
  return { creator: "", id: "" };
}

export const MsgDeleteImportToken = {
  encode(message: MsgDeleteImportToken, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgDeleteImportToken {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgDeleteImportToken();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgDeleteImportToken {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
    };
  },

  toJSON(message: MsgDeleteImportToken): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgDeleteImportToken>, I>>(object: I): MsgDeleteImportToken {
    const message = createBaseMsgDeleteImportToken();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgDeleteImportTokenResponse(): MsgDeleteImportTokenResponse {
  return {};
}

export const MsgDeleteImportTokenResponse = {
  encode(_: MsgDeleteImportTokenResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgDeleteImportTokenResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgDeleteImportTokenResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgDeleteImportTokenResponse {
    return {};
  },

  toJSON(_: MsgDeleteImportTokenResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgDeleteImportTokenResponse>, I>>(_: I): MsgDeleteImportTokenResponse {
    const message = createBaseMsgDeleteImportTokenResponse();
    return message;
  },
};

function createBaseMsgFetchImportToken(): MsgFetchImportToken {
  return { creator: "", id: "" };
}

export const MsgFetchImportToken = {
  encode(message: MsgFetchImportToken, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchImportToken {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchImportToken();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchImportToken {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
    };
  },

  toJSON(message: MsgFetchImportToken): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchImportToken>, I>>(object: I): MsgFetchImportToken {
    const message = createBaseMsgFetchImportToken();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgFetchImportTokenResponse(): MsgFetchImportTokenResponse {
  return { ImportToken: undefined, events: [] };
}

export const MsgFetchImportTokenResponse = {
  encode(message: MsgFetchImportTokenResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.ImportToken !== undefined) {
      ImportToken.encode(message.ImportToken, writer.uint32(10).fork()).ldelim();
    }
    for (const v of message.events) {
      ImportEvent.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchImportTokenResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchImportTokenResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.ImportToken = ImportToken.decode(reader, reader.uint32());
          break;
        case 2:
          message.events.push(ImportEvent.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchImportTokenResponse {
    return {
      ImportToken: isSet(object.ImportToken) ? ImportToken.fromJSON(object.ImportToken) : undefined,
      events: Array.isArray(object?.events) ? object.events.map((e: any) => ImportEvent.fromJSON(e)) : [],
    };
  },

  toJSON(message: MsgFetchImportTokenResponse): unknown {
    const obj: any = {};
    message.ImportToken !== undefined
      && (obj.ImportToken = message.ImportToken ? ImportToken.toJSON(message.ImportToken) : undefined);
    if (message.events) {
      obj.events = message.events.map((e) => e ? ImportEvent.toJSON(e) : undefined);
    } else {
      obj.events = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchImportTokenResponse>, I>>(object: I): MsgFetchImportTokenResponse {
    const message = createBaseMsgFetchImportTokenResponse();
    message.ImportToken = (object.ImportToken !== undefined && object.ImportToken !== null)
      ? ImportToken.fromPartial(object.ImportToken)
      : undefined;
    message.events = object.events?.map((e) => ImportEvent.fromPartial(e)) || [];
    return message;
  },
};

function createBaseMsgFetchImportMapping(): MsgFetchImportMapping {
  return { creator: "", id: "" };
}

export const MsgFetchImportMapping = {
  encode(message: MsgFetchImportMapping, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchImportMapping {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchImportMapping();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchImportMapping {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
    };
  },

  toJSON(message: MsgFetchImportMapping): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchImportMapping>, I>>(object: I): MsgFetchImportMapping {
    const message = createBaseMsgFetchImportMapping();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgFetchImportMappingResponse(): MsgFetchImportMappingResponse {
  return { ImportToken: undefined, events: [] };
}

export const MsgFetchImportMappingResponse = {
  encode(message: MsgFetchImportMappingResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.ImportToken !== undefined) {
      ImportToken.encode(message.ImportToken, writer.uint32(10).fork()).ldelim();
    }
    for (const v of message.events) {
      ImportEvent.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchImportMappingResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchImportMappingResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.ImportToken = ImportToken.decode(reader, reader.uint32());
          break;
        case 2:
          message.events.push(ImportEvent.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchImportMappingResponse {
    return {
      ImportToken: isSet(object.ImportToken) ? ImportToken.fromJSON(object.ImportToken) : undefined,
      events: Array.isArray(object?.events) ? object.events.map((e: any) => ImportEvent.fromJSON(e)) : [],
    };
  },

  toJSON(message: MsgFetchImportMappingResponse): unknown {
    const obj: any = {};
    message.ImportToken !== undefined
      && (obj.ImportToken = message.ImportToken ? ImportToken.toJSON(message.ImportToken) : undefined);
    if (message.events) {
      obj.events = message.events.map((e) => e ? ImportEvent.toJSON(e) : undefined);
    } else {
      obj.events = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchImportMappingResponse>, I>>(
    object: I,
  ): MsgFetchImportMappingResponse {
    const message = createBaseMsgFetchImportMappingResponse();
    message.ImportToken = (object.ImportToken !== undefined && object.ImportToken !== null)
      ? ImportToken.fromPartial(object.ImportToken)
      : undefined;
    message.events = object.events?.map((e) => ImportEvent.fromPartial(e)) || [];
    return message;
  },
};

/** Msg defines the Msg service. */
export interface Msg {
  /**
   * this line is used by starport scaffolding # proto/tx/rpc
   *  rpc CreateEvent(MsgCreateEvent) returns (MsgCreateEventResponse);
   *  rpc CreateImportToken(MsgCreateImportToken) returns (MsgCreateImportTokenResponse);
   *  rpc UpdateImportToken(MsgUpdateImportToken) returns (MsgUpdateImportTokenResponse);
   *  rpc DeleteImportToken(MsgDeleteImportToken) returns (MsgDeleteImportTokenResponse);
   */
  FetchImportToken(request: MsgFetchImportToken): Promise<MsgFetchImportTokenResponse>;
  FetchImportMapping(request: MsgFetchImportMapping): Promise<MsgFetchImportMappingResponse>;
}

export class MsgClientImpl implements Msg {
  private readonly rpc: Rpc;
  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.FetchImportToken = this.FetchImportToken.bind(this);
    this.FetchImportMapping = this.FetchImportMapping.bind(this);
  }
  FetchImportToken(request: MsgFetchImportToken): Promise<MsgFetchImportTokenResponse> {
    const data = MsgFetchImportToken.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.importtoken.Msg", "FetchImportToken", data);
    return promise.then((data) => MsgFetchImportTokenResponse.decode(new _m0.Reader(data)));
  }

  FetchImportMapping(request: MsgFetchImportMapping): Promise<MsgFetchImportMappingResponse> {
    const data = MsgFetchImportMapping.encode(request).finish();
    const promise = this.rpc.request("org.borderblockchain.importtoken.Msg", "FetchImportMapping", data);
    return promise.then((data) => MsgFetchImportMappingResponse.decode(new _m0.Reader(data)));
  }
}

interface Rpc {
  request(service: string, method: string, data: Uint8Array): Promise<Uint8Array>;
}

declare var self: any | undefined;
declare var window: any | undefined;
declare var global: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== "undefined") {
    return globalThis;
  }
  if (typeof self !== "undefined") {
    return self;
  }
  if (typeof window !== "undefined") {
    return window;
  }
  if (typeof global !== "undefined") {
    return global;
  }
  throw "Unable to locate global object";
})();

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
  }
  return long.toNumber();
}

if (true) {
  _m0.util.Long = Long as any;
  _m0.configure();
}

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

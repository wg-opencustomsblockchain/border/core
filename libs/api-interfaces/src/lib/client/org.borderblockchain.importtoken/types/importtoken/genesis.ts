/* eslint-disable */
import _m0 from "protobufjs/minimal";
import { EventHistory } from "./event-history";
import { ImportMapping } from "./import-mapping";
import { ImportToken } from "./importtoken";

export const protobufPackage = "org.borderblockchain.importtoken";

/** GenesisState defines the capability module's genesis state. */
export interface GenesisState {
  /** this line is used by starport scaffolding # genesis/proto/state */
  eventHistoryList: EventHistory[];
  /** this line is used by starport scaffolding # genesis/proto/stateField */
  importTokenList: ImportToken[];
  /** this line is used by starport scaffolding # genesis/proto/stateField */
  importMappingList: ImportMapping[];
}

function createBaseGenesisState(): GenesisState {
  return { eventHistoryList: [], importTokenList: [], importMappingList: [] };
}

export const GenesisState = {
  encode(message: GenesisState, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.eventHistoryList) {
      EventHistory.encode(v!, writer.uint32(26).fork()).ldelim();
    }
    for (const v of message.importTokenList) {
      ImportToken.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    for (const v of message.importMappingList) {
      ImportMapping.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): GenesisState {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseGenesisState();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 3:
          message.eventHistoryList.push(EventHistory.decode(reader, reader.uint32()));
          break;
        case 1:
          message.importTokenList.push(ImportToken.decode(reader, reader.uint32()));
          break;
        case 2:
          message.importMappingList.push(ImportMapping.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GenesisState {
    return {
      eventHistoryList: Array.isArray(object?.eventHistoryList)
        ? object.eventHistoryList.map((e: any) => EventHistory.fromJSON(e))
        : [],
      importTokenList: Array.isArray(object?.importTokenList)
        ? object.importTokenList.map((e: any) => ImportToken.fromJSON(e))
        : [],
      importMappingList: Array.isArray(object?.importMappingList)
        ? object.importMappingList.map((e: any) => ImportMapping.fromJSON(e))
        : [],
    };
  },

  toJSON(message: GenesisState): unknown {
    const obj: any = {};
    if (message.eventHistoryList) {
      obj.eventHistoryList = message.eventHistoryList.map((e) => e ? EventHistory.toJSON(e) : undefined);
    } else {
      obj.eventHistoryList = [];
    }
    if (message.importTokenList) {
      obj.importTokenList = message.importTokenList.map((e) => e ? ImportToken.toJSON(e) : undefined);
    } else {
      obj.importTokenList = [];
    }
    if (message.importMappingList) {
      obj.importMappingList = message.importMappingList.map((e) => e ? ImportMapping.toJSON(e) : undefined);
    } else {
      obj.importMappingList = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<GenesisState>, I>>(object: I): GenesisState {
    const message = createBaseGenesisState();
    message.eventHistoryList = object.eventHistoryList?.map((e) => EventHistory.fromPartial(e)) || [];
    message.importTokenList = object.importTokenList?.map((e) => ImportToken.fromPartial(e)) || [];
    message.importMappingList = object.importMappingList?.map((e) => ImportMapping.fromPartial(e)) || [];
    return message;
  },
};

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

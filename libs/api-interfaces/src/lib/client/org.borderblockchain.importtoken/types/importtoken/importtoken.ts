/* eslint-disable */
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';

export const protobufPackage = 'org.borderblockchain.importtoken';

export interface ImportToken {
  creator: string;
  id: string;
  consignor: Company | undefined;
  exporter: Company | undefined;
  consignee: Company | undefined;
  declarant: Company | undefined;
  customOfficeOfExit: CustomsOffice | undefined;
  customOfficeOfEntry: CustomsOffice | undefined;
  customOfficeOfImport: CustomsOffice | undefined;
  goodsItems: GoodsItem[];
  transportAtBorder: Transport | undefined;
  transportFromBorder: Transport | undefined;
  importId: string;
  uniqueConsignmentReference: string;
  localReferenceNumber: string;
  destinationCountry: string;
  exportCountry: string;
  itinerary: string;
  incotermCode: string;
  incotermLocation: string;
  totalGrossMass: string;
  goodsItemQuantity: number;
  totalPackagesQuantity: number;
  releaseDateAndTime: string;
  natureOfTransaction: number;
  totalAmountInvoiced: number;
  invoiceCurrency: string;
  relatedExportToken: string;
}

export interface Company {
  address: Address | undefined;
  customsId: string;
  name: string;
  subsidiaryNumber: string;
  identificationNumber: string;
}

export interface CustomsOffice {
  address: Address | undefined;
  customsOfficeCode: string;
  customsOfficeName: string;
}

export interface Address {
  streetAndNumber: string;
  postcode: string;
  city: string;
  countryCode: string;
}

export interface GoodsItem {
  documents: Document[];
  countryOfOrigin: string;
  sequenceNumber: number;
  descriptionOfGoods: string;
  harmonizedSystemSubheadingCode: string;
  localClassificationCode: string;
  grossMass: string;
  netMass: string;
  numberOfPackages: number;
  typeOfPackages: string;
  marksNumbers: string;
  containerId: string;
  consigneeOrderNumber: string;
  valueAtBorder: number;
  valueAtBorderCurrency: string;
  amountInvoiced: number;
  amountInvoicedCurrency: string;
  dangerousGoodsCode: number;
}

export interface Transport {
  modeOfTransport: number;
  typeOfIdentification: number;
  identity: string;
  nationality: string;
  transportCosts: number;
  transportCostsCurrency: string;
  transportOrderNumber: string;
}

export interface Document {
  type: string;
  referenceNumber: string;
  complement: string;
  detail: string;
}

function createBaseImportToken(): ImportToken {
  return {
    creator: '',
    id: '',
    consignor: undefined,
    exporter: undefined,
    consignee: undefined,
    declarant: undefined,
    customOfficeOfExit: undefined,
    customOfficeOfEntry: undefined,
    customOfficeOfImport: undefined,
    goodsItems: [],
    transportAtBorder: undefined,
    transportFromBorder: undefined,
    importId: '',
    uniqueConsignmentReference: '',
    localReferenceNumber: '',
    destinationCountry: '',
    exportCountry: '',
    itinerary: '',
    incotermCode: '',
    incotermLocation: '',
    totalGrossMass: '',
    goodsItemQuantity: 0,
    totalPackagesQuantity: 0,
    releaseDateAndTime: '',
    natureOfTransaction: 0,
    totalAmountInvoiced: 0,
    invoiceCurrency: '',
    relatedExportToken: '',
  };
}

export const ImportToken = {
  encode(message: ImportToken, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== '') {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== '') {
      writer.uint32(18).string(message.id);
    }
    if (message.consignor !== undefined) {
      Company.encode(message.consignor, writer.uint32(26).fork()).ldelim();
    }
    if (message.exporter !== undefined) {
      Company.encode(message.exporter, writer.uint32(34).fork()).ldelim();
    }
    if (message.consignee !== undefined) {
      Company.encode(message.consignee, writer.uint32(42).fork()).ldelim();
    }
    if (message.declarant !== undefined) {
      Company.encode(message.declarant, writer.uint32(50).fork()).ldelim();
    }
    if (message.customOfficeOfExit !== undefined) {
      CustomsOffice.encode(message.customOfficeOfExit, writer.uint32(58).fork()).ldelim();
    }
    if (message.customOfficeOfEntry !== undefined) {
      CustomsOffice.encode(message.customOfficeOfEntry, writer.uint32(66).fork()).ldelim();
    }
    if (message.customOfficeOfImport !== undefined) {
      CustomsOffice.encode(message.customOfficeOfImport, writer.uint32(74).fork()).ldelim();
    }
    for (const v of message.goodsItems) {
      GoodsItem.encode(v!, writer.uint32(82).fork()).ldelim();
    }
    if (message.transportAtBorder !== undefined) {
      Transport.encode(message.transportAtBorder, writer.uint32(90).fork()).ldelim();
    }
    if (message.transportFromBorder !== undefined) {
      Transport.encode(message.transportFromBorder, writer.uint32(98).fork()).ldelim();
    }
    if (message.importId !== '') {
      writer.uint32(106).string(message.importId);
    }
    if (message.uniqueConsignmentReference !== '') {
      writer.uint32(114).string(message.uniqueConsignmentReference);
    }
    if (message.localReferenceNumber !== '') {
      writer.uint32(122).string(message.localReferenceNumber);
    }
    if (message.destinationCountry !== '') {
      writer.uint32(130).string(message.destinationCountry);
    }
    if (message.exportCountry !== '') {
      writer.uint32(138).string(message.exportCountry);
    }
    if (message.itinerary !== '') {
      writer.uint32(146).string(message.itinerary);
    }
    if (message.incotermCode !== '') {
      writer.uint32(154).string(message.incotermCode);
    }
    if (message.incotermLocation !== '') {
      writer.uint32(162).string(message.incotermLocation);
    }
    if (message.totalGrossMass !== '') {
      writer.uint32(170).string(message.totalGrossMass);
    }
    if (message.goodsItemQuantity !== 0) {
      writer.uint32(176).int64(message.goodsItemQuantity);
    }
    if (message.totalPackagesQuantity !== 0) {
      writer.uint32(184).int64(message.totalPackagesQuantity);
    }
    if (message.releaseDateAndTime !== '') {
      writer.uint32(194).string(message.releaseDateAndTime);
    }
    if (message.natureOfTransaction !== 0) {
      writer.uint32(200).int64(message.natureOfTransaction);
    }
    if (message.totalAmountInvoiced !== 0) {
      writer.uint32(208).int64(message.totalAmountInvoiced);
    }
    if (message.invoiceCurrency !== '') {
      writer.uint32(218).string(message.invoiceCurrency);
    }
    if (message.relatedExportToken !== '') {
      writer.uint32(226).string(message.relatedExportToken);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ImportToken {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseImportToken();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.consignor = Company.decode(reader, reader.uint32());
          break;
        case 4:
          message.exporter = Company.decode(reader, reader.uint32());
          break;
        case 5:
          message.consignee = Company.decode(reader, reader.uint32());
          break;
        case 6:
          message.declarant = Company.decode(reader, reader.uint32());
          break;
        case 7:
          message.customOfficeOfExit = CustomsOffice.decode(reader, reader.uint32());
          break;
        case 8:
          message.customOfficeOfEntry = CustomsOffice.decode(reader, reader.uint32());
          break;
        case 9:
          message.customOfficeOfImport = CustomsOffice.decode(reader, reader.uint32());
          break;
        case 10:
          message.goodsItems.push(GoodsItem.decode(reader, reader.uint32()));
          break;
        case 11:
          message.transportAtBorder = Transport.decode(reader, reader.uint32());
          break;
        case 12:
          message.transportFromBorder = Transport.decode(reader, reader.uint32());
          break;
        case 13:
          message.importId = reader.string();
          break;
        case 14:
          message.uniqueConsignmentReference = reader.string();
          break;
        case 15:
          message.localReferenceNumber = reader.string();
          break;
        case 16:
          message.destinationCountry = reader.string();
          break;
        case 17:
          message.exportCountry = reader.string();
          break;
        case 18:
          message.itinerary = reader.string();
          break;
        case 19:
          message.incotermCode = reader.string();
          break;
        case 20:
          message.incotermLocation = reader.string();
          break;
        case 21:
          message.totalGrossMass = reader.string();
          break;
        case 22:
          message.goodsItemQuantity = longToNumber(reader.int64() as Long);
          break;
        case 23:
          message.totalPackagesQuantity = longToNumber(reader.int64() as Long);
          break;
        case 24:
          message.releaseDateAndTime = reader.string();
          break;
        case 25:
          message.natureOfTransaction = longToNumber(reader.int64() as Long);
          break;
        case 26:
          message.totalAmountInvoiced = longToNumber(reader.int64() as Long);
          break;
        case 27:
          message.invoiceCurrency = reader.string();
          break;
        case 28:
          message.relatedExportToken = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ImportToken {
    return {
      creator: isSet(object.creator) ? String(object.creator) : '',
      id: isSet(object.id) ? String(object.id) : '',
      consignor: isSet(object.consignor) ? Company.fromJSON(object.consignor) : undefined,
      exporter: isSet(object.exporter) ? Company.fromJSON(object.exporter) : undefined,
      consignee: isSet(object.consignee) ? Company.fromJSON(object.consignee) : undefined,
      declarant: isSet(object.declarant) ? Company.fromJSON(object.declarant) : undefined,
      customOfficeOfExit: isSet(object.customOfficeOfExit)
        ? CustomsOffice.fromJSON(object.customOfficeOfExit)
        : undefined,
      customOfficeOfEntry: isSet(object.customOfficeOfEntry)
        ? CustomsOffice.fromJSON(object.customOfficeOfEntry)
        : undefined,
      customOfficeOfImport: isSet(object.customOfficeOfImport)
        ? CustomsOffice.fromJSON(object.customOfficeOfImport)
        : undefined,
      goodsItems: Array.isArray(object?.goodsItems) ? object.goodsItems.map((e: any) => GoodsItem.fromJSON(e)) : [],
      transportAtBorder: isSet(object.transportAtBorder) ? Transport.fromJSON(object.transportAtBorder) : undefined,
      transportFromBorder: isSet(object.transportFromBorder)
        ? Transport.fromJSON(object.transportFromBorder)
        : undefined,
      importId: isSet(object.importId) ? String(object.importId) : '',
      uniqueConsignmentReference: isSet(object.uniqueConsignmentReference)
        ? String(object.uniqueConsignmentReference)
        : '',
      localReferenceNumber: isSet(object.localReferenceNumber) ? String(object.localReferenceNumber) : '',
      destinationCountry: isSet(object.destinationCountry) ? String(object.destinationCountry) : '',
      exportCountry: isSet(object.exportCountry) ? String(object.exportCountry) : '',
      itinerary: isSet(object.itinerary) ? String(object.itinerary) : '',
      incotermCode: isSet(object.incotermCode) ? String(object.incotermCode) : '',
      incotermLocation: isSet(object.incotermLocation) ? String(object.incotermLocation) : '',
      totalGrossMass: isSet(object.totalGrossMass) ? String(object.totalGrossMass) : '',
      goodsItemQuantity: isSet(object.goodsItemQuantity) ? Number(object.goodsItemQuantity) : 0,
      totalPackagesQuantity: isSet(object.totalPackagesQuantity) ? Number(object.totalPackagesQuantity) : 0,
      releaseDateAndTime: isSet(object.releaseDateAndTime) ? String(object.releaseDateAndTime) : '',
      natureOfTransaction: isSet(object.natureOfTransaction) ? Number(object.natureOfTransaction) : 0,
      totalAmountInvoiced: isSet(object.totalAmountInvoiced) ? Number(object.totalAmountInvoiced) : 0,
      invoiceCurrency: isSet(object.invoiceCurrency) ? String(object.invoiceCurrency) : '',
      relatedExportToken: isSet(object.relatedExportToken) ? String(object.relatedExportToken) : '',
    };
  },

  toJSON(message: ImportToken): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.consignor !== undefined &&
    (obj.consignor = message.consignor ? Company.toJSON(message.consignor) : undefined);
    message.exporter !== undefined && (obj.exporter = message.exporter ? Company.toJSON(message.exporter) : undefined);
    message.consignee !== undefined &&
    (obj.consignee = message.consignee ? Company.toJSON(message.consignee) : undefined);
    message.declarant !== undefined &&
    (obj.declarant = message.declarant ? Company.toJSON(message.declarant) : undefined);
    message.customOfficeOfExit !== undefined &&
    (obj.customOfficeOfExit = message.customOfficeOfExit
      ? CustomsOffice.toJSON(message.customOfficeOfExit)
      : undefined);
    message.customOfficeOfEntry !== undefined &&
    (obj.customOfficeOfEntry = message.customOfficeOfEntry
      ? CustomsOffice.toJSON(message.customOfficeOfEntry)
      : undefined);
    message.customOfficeOfImport !== undefined &&
    (obj.customOfficeOfImport = message.customOfficeOfImport
      ? CustomsOffice.toJSON(message.customOfficeOfImport)
      : undefined);
    if (message.goodsItems) {
      obj.goodsItems = message.goodsItems.map((e) => (e ? GoodsItem.toJSON(e) : undefined));
    } else {
      obj.goodsItems = [];
    }
    message.transportAtBorder !== undefined &&
    (obj.transportAtBorder = message.transportAtBorder ? Transport.toJSON(message.transportAtBorder) : undefined);
    message.transportFromBorder !== undefined &&
    (obj.transportFromBorder = message.transportFromBorder
      ? Transport.toJSON(message.transportFromBorder)
      : undefined);
    message.importId !== undefined && (obj.importId = message.importId);
    message.uniqueConsignmentReference !== undefined &&
    (obj.uniqueConsignmentReference = message.uniqueConsignmentReference);
    message.localReferenceNumber !== undefined && (obj.localReferenceNumber = message.localReferenceNumber);
    message.destinationCountry !== undefined && (obj.destinationCountry = message.destinationCountry);
    message.exportCountry !== undefined && (obj.exportCountry = message.exportCountry);
    message.itinerary !== undefined && (obj.itinerary = message.itinerary);
    message.incotermCode !== undefined && (obj.incotermCode = message.incotermCode);
    message.incotermLocation !== undefined && (obj.incotermLocation = message.incotermLocation);
    message.totalGrossMass !== undefined && (obj.totalGrossMass = message.totalGrossMass);
    message.goodsItemQuantity !== undefined && (obj.goodsItemQuantity = Math.round(message.goodsItemQuantity));
    message.totalPackagesQuantity !== undefined &&
    (obj.totalPackagesQuantity = Math.round(message.totalPackagesQuantity));
    message.releaseDateAndTime !== undefined && (obj.releaseDateAndTime = message.releaseDateAndTime);
    message.natureOfTransaction !== undefined && (obj.natureOfTransaction = Math.round(message.natureOfTransaction));
    message.totalAmountInvoiced !== undefined && (obj.totalAmountInvoiced = Math.round(message.totalAmountInvoiced));
    message.invoiceCurrency !== undefined && (obj.invoiceCurrency = message.invoiceCurrency);
    message.relatedExportToken !== undefined && (obj.relatedExportToken = message.relatedExportToken);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<ImportToken>, I>>(object: I): ImportToken {
    const message = createBaseImportToken();
    message.creator = object.creator ?? '';
    message.id = object.id ?? '';
    message.consignor =
      object.consignor !== undefined && object.consignor !== null ? Company.fromPartial(object.consignor) : undefined;
    message.exporter =
      object.exporter !== undefined && object.exporter !== null ? Company.fromPartial(object.exporter) : undefined;
    message.consignee =
      object.consignee !== undefined && object.consignee !== null ? Company.fromPartial(object.consignee) : undefined;
    message.declarant =
      object.declarant !== undefined && object.declarant !== null ? Company.fromPartial(object.declarant) : undefined;
    message.customOfficeOfExit =
      object.customOfficeOfExit !== undefined && object.customOfficeOfExit !== null
        ? CustomsOffice.fromPartial(object.customOfficeOfExit)
        : undefined;
    message.customOfficeOfEntry =
      object.customOfficeOfEntry !== undefined && object.customOfficeOfEntry !== null
        ? CustomsOffice.fromPartial(object.customOfficeOfEntry)
        : undefined;
    message.customOfficeOfImport =
      object.customOfficeOfImport !== undefined && object.customOfficeOfImport !== null
        ? CustomsOffice.fromPartial(object.customOfficeOfImport)
        : undefined;
    message.goodsItems = object.goodsItems?.map((e) => GoodsItem.fromPartial(e)) || [];
    message.transportAtBorder =
      object.transportAtBorder !== undefined && object.transportAtBorder !== null
        ? Transport.fromPartial(object.transportAtBorder)
        : undefined;
    message.transportFromBorder =
      object.transportFromBorder !== undefined && object.transportFromBorder !== null
        ? Transport.fromPartial(object.transportFromBorder)
        : undefined;
    message.importId = object.importId ?? '';
    message.uniqueConsignmentReference = object.uniqueConsignmentReference ?? '';
    message.localReferenceNumber = object.localReferenceNumber ?? '';
    message.destinationCountry = object.destinationCountry ?? '';
    message.exportCountry = object.exportCountry ?? '';
    message.itinerary = object.itinerary ?? '';
    message.incotermCode = object.incotermCode ?? '';
    message.incotermLocation = object.incotermLocation ?? '';
    message.totalGrossMass = object.totalGrossMass ?? '';
    message.goodsItemQuantity = object.goodsItemQuantity ?? 0;
    message.totalPackagesQuantity = object.totalPackagesQuantity ?? 0;
    message.releaseDateAndTime = object.releaseDateAndTime ?? '';
    message.natureOfTransaction = object.natureOfTransaction ?? 0;
    message.totalAmountInvoiced = object.totalAmountInvoiced ?? 0;
    message.invoiceCurrency = object.invoiceCurrency ?? '';
    message.relatedExportToken = object.relatedExportToken ?? '';
    return message;
  },
};

function createBaseCompany(): Company {
  return { address: undefined, customsId: '', name: '', subsidiaryNumber: '', identificationNumber: '' };
}

export const Company = {
  encode(message: Company, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.address !== undefined) {
      Address.encode(message.address, writer.uint32(10).fork()).ldelim();
    }
    if (message.customsId !== '') {
      writer.uint32(18).string(message.customsId);
    }
    if (message.name !== '') {
      writer.uint32(26).string(message.name);
    }
    if (message.subsidiaryNumber !== '') {
      writer.uint32(34).string(message.subsidiaryNumber);
    }
    if (message.identificationNumber !== '') {
      writer.uint32(42).string(message.identificationNumber);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Company {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseCompany();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.address = Address.decode(reader, reader.uint32());
          break;
        case 2:
          message.customsId = reader.string();
          break;
        case 3:
          message.name = reader.string();
          break;
        case 4:
          message.subsidiaryNumber = reader.string();
          break;
        case 5:
          message.identificationNumber = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Company {
    return {
      address: isSet(object.address) ? Address.fromJSON(object.address) : undefined,
      customsId: isSet(object.customsId) ? String(object.customsId) : '',
      name: isSet(object.name) ? String(object.name) : '',
      subsidiaryNumber: isSet(object.subsidiaryNumber) ? String(object.subsidiaryNumber) : '',
      identificationNumber: isSet(object.identificationNumber) ? String(object.identificationNumber) : '',
    };
  },

  toJSON(message: Company): unknown {
    const obj: any = {};
    message.address !== undefined && (obj.address = message.address ? Address.toJSON(message.address) : undefined);
    message.customsId !== undefined && (obj.customsId = message.customsId);
    message.name !== undefined && (obj.name = message.name);
    message.subsidiaryNumber !== undefined && (obj.subsidiaryNumber = message.subsidiaryNumber);
    message.identificationNumber !== undefined && (obj.identificationNumber = message.identificationNumber);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<Company>, I>>(object: I): Company {
    const message = createBaseCompany();
    message.address =
      object.address !== undefined && object.address !== null ? Address.fromPartial(object.address) : undefined;
    message.customsId = object.customsId ?? '';
    message.name = object.name ?? '';
    message.subsidiaryNumber = object.subsidiaryNumber ?? '';
    message.identificationNumber = object.identificationNumber ?? '';
    return message;
  },
};

function createBaseCustomsOffice(): CustomsOffice {
  return { address: undefined, customsOfficeCode: '', customsOfficeName: '' };
}

export const CustomsOffice = {
  encode(message: CustomsOffice, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.address !== undefined) {
      Address.encode(message.address, writer.uint32(10).fork()).ldelim();
    }
    if (message.customsOfficeCode !== '') {
      writer.uint32(18).string(message.customsOfficeCode);
    }
    if (message.customsOfficeName !== '') {
      writer.uint32(26).string(message.customsOfficeName);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): CustomsOffice {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseCustomsOffice();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.address = Address.decode(reader, reader.uint32());
          break;
        case 2:
          message.customsOfficeCode = reader.string();
          break;
        case 3:
          message.customsOfficeName = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CustomsOffice {
    return {
      address: isSet(object.address) ? Address.fromJSON(object.address) : undefined,
      customsOfficeCode: isSet(object.customsOfficeCode) ? String(object.customsOfficeCode) : '',
      customsOfficeName: isSet(object.customsOfficeName) ? String(object.customsOfficeName) : '',
    };
  },

  toJSON(message: CustomsOffice): unknown {
    const obj: any = {};
    message.address !== undefined && (obj.address = message.address ? Address.toJSON(message.address) : undefined);
    message.customsOfficeCode !== undefined && (obj.customsOfficeCode = message.customsOfficeCode);
    message.customsOfficeName !== undefined && (obj.customsOfficeName = message.customsOfficeName);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<CustomsOffice>, I>>(object: I): CustomsOffice {
    const message = createBaseCustomsOffice();
    message.address =
      object.address !== undefined && object.address !== null ? Address.fromPartial(object.address) : undefined;
    message.customsOfficeCode = object.customsOfficeCode ?? '';
    message.customsOfficeName = object.customsOfficeName ?? '';
    return message;
  },
};

function createBaseAddress(): Address {
  return { streetAndNumber: '', postcode: '', city: '', countryCode: '' };
}

export const Address = {
  encode(message: Address, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.streetAndNumber !== '') {
      writer.uint32(10).string(message.streetAndNumber);
    }
    if (message.postcode !== '') {
      writer.uint32(18).string(message.postcode);
    }
    if (message.city !== '') {
      writer.uint32(26).string(message.city);
    }
    if (message.countryCode !== '') {
      writer.uint32(34).string(message.countryCode);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Address {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseAddress();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.streetAndNumber = reader.string();
          break;
        case 2:
          message.postcode = reader.string();
          break;
        case 3:
          message.city = reader.string();
          break;
        case 4:
          message.countryCode = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Address {
    return {
      streetAndNumber: isSet(object.streetAndNumber) ? String(object.streetAndNumber) : '',
      postcode: isSet(object.postcode) ? String(object.postcode) : '',
      city: isSet(object.city) ? String(object.city) : '',
      countryCode: isSet(object.countryCode) ? String(object.countryCode) : '',
    };
  },

  toJSON(message: Address): unknown {
    const obj: any = {};
    message.streetAndNumber !== undefined && (obj.streetAndNumber = message.streetAndNumber);
    message.postcode !== undefined && (obj.postcode = message.postcode);
    message.city !== undefined && (obj.city = message.city);
    message.countryCode !== undefined && (obj.countryCode = message.countryCode);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<Address>, I>>(object: I): Address {
    const message = createBaseAddress();
    message.streetAndNumber = object.streetAndNumber ?? '';
    message.postcode = object.postcode ?? '';
    message.city = object.city ?? '';
    message.countryCode = object.countryCode ?? '';
    return message;
  },
};

function createBaseGoodsItem(): GoodsItem {
  return {
    documents: [],
    countryOfOrigin: '',
    sequenceNumber: 0,
    descriptionOfGoods: '',
    harmonizedSystemSubheadingCode: '',
    localClassificationCode: '',
    grossMass: '',
    netMass: '',
    numberOfPackages: 0,
    typeOfPackages: '',
    marksNumbers: '',
    containerId: '',
    consigneeOrderNumber: '',
    valueAtBorder: 0,
    valueAtBorderCurrency: '',
    amountInvoiced: 0,
    amountInvoicedCurrency: '',
    dangerousGoodsCode: 0,
  };
}

export const GoodsItem = {
  encode(message: GoodsItem, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.documents) {
      Document.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.countryOfOrigin !== '') {
      writer.uint32(18).string(message.countryOfOrigin);
    }
    if (message.sequenceNumber !== 0) {
      writer.uint32(24).int64(message.sequenceNumber);
    }
    if (message.descriptionOfGoods !== '') {
      writer.uint32(34).string(message.descriptionOfGoods);
    }
    if (message.harmonizedSystemSubheadingCode !== '') {
      writer.uint32(42).string(message.harmonizedSystemSubheadingCode);
    }
    if (message.localClassificationCode !== '') {
      writer.uint32(50).string(message.localClassificationCode);
    }
    if (message.grossMass !== '') {
      writer.uint32(58).string(message.grossMass);
    }
    if (message.netMass !== '') {
      writer.uint32(66).string(message.netMass);
    }
    if (message.numberOfPackages !== 0) {
      writer.uint32(72).int64(message.numberOfPackages);
    }
    if (message.typeOfPackages !== '') {
      writer.uint32(82).string(message.typeOfPackages);
    }
    if (message.marksNumbers !== '') {
      writer.uint32(90).string(message.marksNumbers);
    }
    if (message.containerId !== '') {
      writer.uint32(98).string(message.containerId);
    }
    if (message.consigneeOrderNumber !== '') {
      writer.uint32(106).string(message.consigneeOrderNumber);
    }
    if (message.valueAtBorder !== 0) {
      writer.uint32(112).int64(message.valueAtBorder);
    }
    if (message.valueAtBorderCurrency !== '') {
      writer.uint32(122).string(message.valueAtBorderCurrency);
    }
    if (message.amountInvoiced !== 0) {
      writer.uint32(128).int64(message.amountInvoiced);
    }
    if (message.amountInvoicedCurrency !== '') {
      writer.uint32(138).string(message.amountInvoicedCurrency);
    }
    if (message.dangerousGoodsCode !== 0) {
      writer.uint32(144).int64(message.dangerousGoodsCode);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): GoodsItem {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseGoodsItem();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.documents.push(Document.decode(reader, reader.uint32()));
          break;
        case 2:
          message.countryOfOrigin = reader.string();
          break;
        case 3:
          message.sequenceNumber = longToNumber(reader.int64() as Long);
          break;
        case 4:
          message.descriptionOfGoods = reader.string();
          break;
        case 5:
          message.harmonizedSystemSubheadingCode = reader.string();
          break;
        case 6:
          message.localClassificationCode = reader.string();
          break;
        case 7:
          message.grossMass = reader.string();
          break;
        case 8:
          message.netMass = reader.string();
          break;
        case 9:
          message.numberOfPackages = longToNumber(reader.int64() as Long);
          break;
        case 10:
          message.typeOfPackages = reader.string();
          break;
        case 11:
          message.marksNumbers = reader.string();
          break;
        case 12:
          message.containerId = reader.string();
          break;
        case 13:
          message.consigneeOrderNumber = reader.string();
          break;
        case 14:
          message.valueAtBorder = longToNumber(reader.int64() as Long);
          break;
        case 15:
          message.valueAtBorderCurrency = reader.string();
          break;
        case 16:
          message.amountInvoiced = longToNumber(reader.int64() as Long);
          break;
        case 17:
          message.amountInvoicedCurrency = reader.string();
          break;
        case 18:
          message.dangerousGoodsCode = longToNumber(reader.int64() as Long);
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GoodsItem {
    return {
      documents: Array.isArray(object?.documents) ? object.documents.map((e: any) => Document.fromJSON(e)) : [],
      countryOfOrigin: isSet(object.countryOfOrigin) ? String(object.countryOfOrigin) : '',
      sequenceNumber: isSet(object.sequenceNumber) ? Number(object.sequenceNumber) : 0,
      descriptionOfGoods: isSet(object.descriptionOfGoods) ? String(object.descriptionOfGoods) : '',
      harmonizedSystemSubheadingCode: isSet(object.harmonizedSystemSubheadingCode)
        ? String(object.harmonizedSystemSubheadingCode)
        : '',
      localClassificationCode: isSet(object.localClassificationCode) ? String(object.localClassificationCode) : '',
      grossMass: isSet(object.grossMass) ? String(object.grossMass) : '',
      netMass: isSet(object.netMass) ? String(object.netMass) : '',
      numberOfPackages: isSet(object.numberOfPackages) ? Number(object.numberOfPackages) : 0,
      typeOfPackages: isSet(object.typeOfPackages) ? String(object.typeOfPackages) : '',
      marksNumbers: isSet(object.marksNumbers) ? String(object.marksNumbers) : '',
      containerId: isSet(object.containerId) ? String(object.containerId) : '',
      consigneeOrderNumber: isSet(object.consigneeOrderNumber) ? String(object.consigneeOrderNumber) : '',
      valueAtBorder: isSet(object.valueAtBorder) ? Number(object.valueAtBorder) : 0,
      valueAtBorderCurrency: isSet(object.valueAtBorderCurrency) ? String(object.valueAtBorderCurrency) : '',
      amountInvoiced: isSet(object.amountInvoiced) ? Number(object.amountInvoiced) : 0,
      amountInvoicedCurrency: isSet(object.amountInvoicedCurrency) ? String(object.amountInvoicedCurrency) : '',
      dangerousGoodsCode: isSet(object.dangerousGoodsCode) ? Number(object.dangerousGoodsCode) : 0,
    };
  },

  toJSON(message: GoodsItem): unknown {
    const obj: any = {};
    if (message.documents) {
      obj.documents = message.documents.map((e) => (e ? Document.toJSON(e) : undefined));
    } else {
      obj.documents = [];
    }
    message.countryOfOrigin !== undefined && (obj.countryOfOrigin = message.countryOfOrigin);
    message.sequenceNumber !== undefined && (obj.sequenceNumber = Math.round(message.sequenceNumber));
    message.descriptionOfGoods !== undefined && (obj.descriptionOfGoods = message.descriptionOfGoods);
    message.harmonizedSystemSubheadingCode !== undefined &&
    (obj.harmonizedSystemSubheadingCode = message.harmonizedSystemSubheadingCode);
    message.localClassificationCode !== undefined && (obj.localClassificationCode = message.localClassificationCode);
    message.grossMass !== undefined && (obj.grossMass = message.grossMass);
    message.netMass !== undefined && (obj.netMass = message.netMass);
    message.numberOfPackages !== undefined && (obj.numberOfPackages = Math.round(message.numberOfPackages));
    message.typeOfPackages !== undefined && (obj.typeOfPackages = message.typeOfPackages);
    message.marksNumbers !== undefined && (obj.marksNumbers = message.marksNumbers);
    message.containerId !== undefined && (obj.containerId = message.containerId);
    message.consigneeOrderNumber !== undefined && (obj.consigneeOrderNumber = message.consigneeOrderNumber);
    message.valueAtBorder !== undefined && (obj.valueAtBorder = Math.round(message.valueAtBorder));
    message.valueAtBorderCurrency !== undefined && (obj.valueAtBorderCurrency = message.valueAtBorderCurrency);
    message.amountInvoiced !== undefined && (obj.amountInvoiced = Math.round(message.amountInvoiced));
    message.amountInvoicedCurrency !== undefined && (obj.amountInvoicedCurrency = message.amountInvoicedCurrency);
    message.dangerousGoodsCode !== undefined && (obj.dangerousGoodsCode = Math.round(message.dangerousGoodsCode));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<GoodsItem>, I>>(object: I): GoodsItem {
    const message = createBaseGoodsItem();
    message.documents = object.documents?.map((e) => Document.fromPartial(e)) || [];
    message.countryOfOrigin = object.countryOfOrigin ?? '';
    message.sequenceNumber = object.sequenceNumber ?? 0;
    message.descriptionOfGoods = object.descriptionOfGoods ?? '';
    message.harmonizedSystemSubheadingCode = object.harmonizedSystemSubheadingCode ?? '';
    message.localClassificationCode = object.localClassificationCode ?? '';
    message.grossMass = object.grossMass ?? '';
    message.netMass = object.netMass ?? '';
    message.numberOfPackages = object.numberOfPackages ?? 0;
    message.typeOfPackages = object.typeOfPackages ?? '';
    message.marksNumbers = object.marksNumbers ?? '';
    message.containerId = object.containerId ?? '';
    message.consigneeOrderNumber = object.consigneeOrderNumber ?? '';
    message.valueAtBorder = object.valueAtBorder ?? 0;
    message.valueAtBorderCurrency = object.valueAtBorderCurrency ?? '';
    message.amountInvoiced = object.amountInvoiced ?? 0;
    message.amountInvoicedCurrency = object.amountInvoicedCurrency ?? '';
    message.dangerousGoodsCode = object.dangerousGoodsCode ?? 0;
    return message;
  },
};

function createBaseTransport(): Transport {
  return {
    modeOfTransport: 0,
    typeOfIdentification: 0,
    identity: '',
    nationality: '',
    transportCosts: 0,
    transportCostsCurrency: '',
    transportOrderNumber: '',
  };
}

export const Transport = {
  encode(message: Transport, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.modeOfTransport !== 0) {
      writer.uint32(8).int64(message.modeOfTransport);
    }
    if (message.typeOfIdentification !== 0) {
      writer.uint32(16).int64(message.typeOfIdentification);
    }
    if (message.identity !== '') {
      writer.uint32(26).string(message.identity);
    }
    if (message.nationality !== '') {
      writer.uint32(34).string(message.nationality);
    }
    if (message.transportCosts !== 0) {
      writer.uint32(40).int64(message.transportCosts);
    }
    if (message.transportCostsCurrency !== '') {
      writer.uint32(50).string(message.transportCostsCurrency);
    }
    if (message.transportOrderNumber !== '') {
      writer.uint32(58).string(message.transportOrderNumber);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Transport {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseTransport();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.modeOfTransport = longToNumber(reader.int64() as Long);
          break;
        case 2:
          message.typeOfIdentification = longToNumber(reader.int64() as Long);
          break;
        case 3:
          message.identity = reader.string();
          break;
        case 4:
          message.nationality = reader.string();
          break;
        case 5:
          message.transportCosts = longToNumber(reader.int64() as Long);
          break;
        case 6:
          message.transportCostsCurrency = reader.string();
          break;
        case 7:
          message.transportOrderNumber = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Transport {
    return {
      modeOfTransport: isSet(object.modeOfTransport) ? Number(object.modeOfTransport) : 0,
      typeOfIdentification: isSet(object.typeOfIdentification) ? Number(object.typeOfIdentification) : 0,
      identity: isSet(object.identity) ? String(object.identity) : '',
      nationality: isSet(object.nationality) ? String(object.nationality) : '',
      transportCosts: isSet(object.transportCosts) ? Number(object.transportCosts) : 0,
      transportCostsCurrency: isSet(object.transportCostsCurrency) ? String(object.transportCostsCurrency) : '',
      transportOrderNumber: isSet(object.transportOrderNumber) ? String(object.transportOrderNumber) : '',
    };
  },

  toJSON(message: Transport): unknown {
    const obj: any = {};
    message.modeOfTransport !== undefined && (obj.modeOfTransport = Math.round(message.modeOfTransport));
    message.typeOfIdentification !== undefined && (obj.typeOfIdentification = Math.round(message.typeOfIdentification));
    message.identity !== undefined && (obj.identity = message.identity);
    message.nationality !== undefined && (obj.nationality = message.nationality);
    message.transportCosts !== undefined && (obj.transportCosts = Math.round(message.transportCosts));
    message.transportCostsCurrency !== undefined && (obj.transportCostsCurrency = message.transportCostsCurrency);
    message.transportOrderNumber !== undefined && (obj.transportOrderNumber = message.transportOrderNumber);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<Transport>, I>>(object: I): Transport {
    const message = createBaseTransport();
    message.modeOfTransport = object.modeOfTransport ?? 0;
    message.typeOfIdentification = object.typeOfIdentification ?? 0;
    message.identity = object.identity ?? '';
    message.nationality = object.nationality ?? '';
    message.transportCosts = object.transportCosts ?? 0;
    message.transportCostsCurrency = object.transportCostsCurrency ?? '';
    message.transportOrderNumber = object.transportOrderNumber ?? '';
    return message;
  },
};

function createBaseDocument(): Document {
  return { type: '', referenceNumber: '', complement: '', detail: '' };
}

export const Document = {
  encode(message: Document, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.type !== '') {
      writer.uint32(10).string(message.type);
    }
    if (message.referenceNumber !== '') {
      writer.uint32(18).string(message.referenceNumber);
    }
    if (message.complement !== '') {
      writer.uint32(26).string(message.complement);
    }
    if (message.detail !== '') {
      writer.uint32(34).string(message.detail);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Document {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseDocument();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.type = reader.string();
          break;
        case 2:
          message.referenceNumber = reader.string();
          break;
        case 3:
          message.complement = reader.string();
          break;
        case 4:
          message.detail = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Document {
    return {
      type: isSet(object.type) ? String(object.type) : '',
      referenceNumber: isSet(object.referenceNumber) ? String(object.referenceNumber) : '',
      complement: isSet(object.complement) ? String(object.complement) : '',
      detail: isSet(object.detail) ? String(object.detail) : '',
    };
  },

  toJSON(message: Document): unknown {
    const obj: any = {};
    message.type !== undefined && (obj.type = message.type);
    message.referenceNumber !== undefined && (obj.referenceNumber = message.referenceNumber);
    message.complement !== undefined && (obj.complement = message.complement);
    message.detail !== undefined && (obj.detail = message.detail);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<Document>, I>>(object: I): Document {
    const message = createBaseDocument();
    message.type = object.type ?? '';
    message.referenceNumber = object.referenceNumber ?? '';
    message.complement = object.complement ?? '';
    message.detail = object.detail ?? '';
    return message;
  },
};

declare var self: any | undefined;
declare var window: any | undefined;
declare var global: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== 'undefined') {
    return globalThis;
  }
  if (typeof self !== 'undefined') {
    return self;
  }
  if (typeof window !== 'undefined') {
    return window;
  }
  if (typeof global !== 'undefined') {
    return global;
  }
  throw 'Unable to locate global object';
})();

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
    ? Array<DeepPartial<U>>
    : T extends ReadonlyArray<infer U>
      ? ReadonlyArray<DeepPartial<U>>
      : T extends {}
        ? { [K in keyof T]?: DeepPartial<T[K]> }
        : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin
  ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error('Value is larger than Number.MAX_SAFE_INTEGER');
  }
  return long.toNumber();
}

if (true) {
  _m0.util.Long = Long as any;
  _m0.configure();
}

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

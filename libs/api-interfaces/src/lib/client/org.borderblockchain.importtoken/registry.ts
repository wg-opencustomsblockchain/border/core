import { GeneratedType } from "@cosmjs/proto-signing";
import { MsgUpdateImportToken } from "./types/importtoken/tx";
import { MsgCreateEvent } from "./types/importtoken/tx";
import { MsgDeleteImportToken } from "./types/importtoken/tx";
import { MsgFetchImportToken } from "./types/importtoken/tx";
import { MsgCreateImportToken } from "./types/importtoken/tx";
import { MsgFetchImportMapping } from "./types/importtoken/tx";

const msgTypes: Array<[string, GeneratedType]>  = [
    ["/org.borderblockchain.importtoken.MsgUpdateImportToken", MsgUpdateImportToken],
    ["/org.borderblockchain.importtoken.MsgCreateEvent", MsgCreateEvent],
    ["/org.borderblockchain.importtoken.MsgDeleteImportToken", MsgDeleteImportToken],
    ["/org.borderblockchain.importtoken.MsgFetchImportToken", MsgFetchImportToken],
    ["/org.borderblockchain.importtoken.MsgCreateImportToken", MsgCreateImportToken],
    ["/org.borderblockchain.importtoken.MsgFetchImportMapping", MsgFetchImportMapping],
    
];

export { msgTypes }
/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export interface ImporttokenAddress {
  streetAndNumber?: string;
  postcode?: string;
  city?: string;
  countryCode?: string;
}

export interface ImporttokenCompany {
  address?: ImporttokenAddress;
  customsId?: string;
  name?: string;
  subsidiaryNumber?: string;
  identificationNumber?: string;
}

export interface ImporttokenCustomsOffice {
  address?: ImporttokenAddress;
  customsOfficeCode?: string;
  customsOfficeName?: string;
}

export interface ImporttokenDocument {
  type?: string;
  referenceNumber?: string;
  complement?: string;
  detail?: string;
}

export interface ImporttokenGoodsItem {
  documents?: ImporttokenDocument[];
  countryOfOrigin?: string;

  /** @format int64 */
  sequenceNumber?: string;
  descriptionOfGoods?: string;
  harmonizedSystemSubheadingCode?: string;
  localClassificationCode?: string;
  grossMass?: string;
  netMass?: string;

  /** @format int64 */
  numberOfPackages?: string;
  typeOfPackages?: string;
  marksNumbers?: string;
  containerId?: string;
  consigneeOrderNumber?: string;

  /** @format int64 */
  valueAtBorder?: string;
  valueAtBorderCurrency?: string;

  /** @format int64 */
  amountInvoiced?: string;
  amountInvoicedCurrency?: string;

  /** @format int64 */
  dangerousGoodsCode?: string;
}

/**
* Creator: Cosmos Address
Status : Status enum from the Import Status Process.
Message: More Information about the Status Transaction.
Type : Type enum of all accepted state transitions/Transaction Type.
*/
export interface ImporttokenImportEvent {
  creator?: string;
  status?: string;
  message?: string;
  eventType?: string;
  timestamp?: string;
}

export interface ImporttokenImportToken {
  creator?: string;
  id?: string;
  consignor?: ImporttokenCompany;
  exporter?: ImporttokenCompany;
  consignee?: ImporttokenCompany;
  declarant?: ImporttokenCompany;
  customOfficeOfExit?: ImporttokenCustomsOffice;
  customOfficeOfEntry?: ImporttokenCustomsOffice;
  customOfficeOfImport?: ImporttokenCustomsOffice;
  goodsItems?: ImporttokenGoodsItem[];
  transportAtBorder?: ImporttokenTransport;
  transportFromBorder?: ImporttokenTransport;
  importId?: string;
  uniqueConsignmentReference?: string;
  localReferenceNumber?: string;
  destinationCountry?: string;
  exportCountry?: string;
  itinerary?: string;
  incotermCode?: string;
  incotermLocation?: string;
  totalGrossMass?: string;

  /** @format int64 */
  goodsItemQuantity?: string;

  /** @format int64 */
  totalPackagesQuantity?: string;
  releaseDateAndTime?: string;

  /** @format int64 */
  natureOfTransaction?: string;

  /** @format int64 */
  totalAmountInvoiced?: string;
  invoiceCurrency?: string;
  relatedExportToken?: string;
}

export interface ImporttokenMsgFetchImportMappingResponse {
  ImportToken?: ImporttokenImportToken;
  events?: ImporttokenImportEvent[];
}

export interface ImporttokenMsgFetchImportTokenResponse {
  ImportToken?: ImporttokenImportToken;
  events?: ImporttokenImportEvent[];
}

export interface ImporttokenTransport {
  /** @format int64 */
  modeOfTransport?: string;

  /** @format int64 */
  typeOfIdentification?: string;
  identity?: string;
  nationality?: string;

  /** @format int64 */
  transportCosts?: string;
  transportCostsCurrency?: string;
  transportOrderNumber?: string;
}

export interface ProtobufAny {
  '@type'?: string;
}

export interface RpcStatus {
  /** @format int32 */
  code?: number;
  message?: string;
  details?: ProtobufAny[];
}

import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse, ResponseType } from 'axios';

export type QueryParamsType = Record<string | number, any>;

export interface FullRequestParams extends Omit<AxiosRequestConfig, 'data' | 'params' | 'url' | 'responseType'> {
  /** set parameter to `true` for call `securityWorker` for this request */
  secure?: boolean;
  /** request path */
  path: string;
  /** content type of request body */
  type?: ContentType;
  /** query params */
  query?: QueryParamsType;
  /** format of response (i.e. response.json() -> format: "json") */
  format?: ResponseType;
  /** request body */
  body?: unknown;
}

export type RequestParams = Omit<FullRequestParams, 'body' | 'method' | 'query' | 'path'>;

export interface ApiConfig<SecurityDataType = unknown> extends Omit<AxiosRequestConfig, 'data' | 'cancelToken'> {
  securityWorker?: (
    securityData: SecurityDataType | null
  ) => Promise<AxiosRequestConfig | void> | AxiosRequestConfig | void;
  secure?: boolean;
  format?: ResponseType;
}

export enum ContentType {
  Json = 'application/json',
  FormData = 'multipart/form-data',
  UrlEncoded = 'application/x-www-form-urlencoded',
}

export class HttpClient<SecurityDataType = unknown> {
  public instance: AxiosInstance;
  private securityData: SecurityDataType | null = null;
  private securityWorker?: ApiConfig<SecurityDataType>['securityWorker'];
  private secure?: boolean;
  private format?: ResponseType;

  constructor({ securityWorker, secure, format, ...axiosConfig }: ApiConfig<SecurityDataType> = {}) {
    this.instance = axios.create({ ...axiosConfig, baseURL: axiosConfig.baseURL || '' });
    this.secure = secure;
    this.format = format;
    this.securityWorker = securityWorker;
  }

  public setSecurityData = (data: SecurityDataType | null) => {
    this.securityData = data;
  };

  private mergeRequestParams(params1: AxiosRequestConfig, params2?: AxiosRequestConfig): AxiosRequestConfig {
    return {
      ...this.instance.defaults,
      ...params1,
      ...(params2 || {}),
      headers: {
        ...(this.instance.defaults.headers || {}),
        ...(params1.headers || {}),
        ...((params2 && params2.headers) || {}),
      },
    };
  }

  private createFormData(input: Record<string, unknown>): FormData {
    return Object.keys(input || {}).reduce((formData, key) => {
      const property = input[key];
      formData.append(
        key,
        property instanceof Blob
          ? property
          : typeof property === 'object' && property !== null
          ? JSON.stringify(property)
          : `${property}`
      );
      return formData;
    }, new FormData());
  }

  public request = async <T = any, _E = any>({
    secure,
    path,
    type,
    query,
    format,
    body,
    ...params
  }: FullRequestParams): Promise<AxiosResponse<T>> => {
    const secureParams =
      ((typeof secure === 'boolean' ? secure : this.secure) &&
        this.securityWorker &&
        (await this.securityWorker(this.securityData))) ||
      {};
    const requestParams = this.mergeRequestParams(params, secureParams);
    const responseFormat = (format && this.format) || void 0;

    if (type === ContentType.FormData && body && body !== null && typeof body === 'object') {
      requestParams.headers.common = { Accept: '*/*' };
      requestParams.headers.post = {};
      requestParams.headers.put = {};

      body = this.createFormData(body as Record<string, unknown>);
    }

    return this.instance.request({
      ...requestParams,
      headers: {
        ...(type && type !== ContentType.FormData ? { 'Content-Type': type } : {}),
        ...(requestParams.headers || {}),
      },
      params: query,
      responseType: responseFormat,
      data: body,
      url: path,
    });
  };
}

/**
 * @title importtoken/event-history.proto
 * @version version not set
 */
export class Api<SecurityDataType extends unknown> extends HttpClient<SecurityDataType> {}

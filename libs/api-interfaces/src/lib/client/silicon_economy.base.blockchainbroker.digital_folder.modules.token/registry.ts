import { GeneratedType } from "@cosmjs/proto-signing";
import { MsgFetchToken } from "./types/token/tx";
import { MsgCreateTokenHistory } from "./types/token/tx";
import { MsgActivateToken } from "./types/token/tx";
import { MsgFetchTokenHistory } from "./types/token/tx";
import { MsgCreateToken } from "./types/token/tx";
import { MsgUpdateTokenInformation } from "./types/token/tx";
import { MsgFetchAllToken } from "./types/token/tx";
import { MsgFetchAllTokenHistory } from "./types/token/tx";
import { MsgUpdateTokenHistory } from "./types/token/tx";
import { MsgUpdateToken } from "./types/token/tx";
import { MsgDeactivateToken } from "./types/token/tx";

const msgTypes: Array<[string, GeneratedType]>  = [
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.token.MsgFetchToken", MsgFetchToken],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.token.MsgCreateTokenHistory", MsgCreateTokenHistory],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.token.MsgActivateToken", MsgActivateToken],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.token.MsgFetchTokenHistory", MsgFetchTokenHistory],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.token.MsgCreateToken", MsgCreateToken],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.token.MsgUpdateTokenInformation", MsgUpdateTokenInformation],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.token.MsgFetchAllToken", MsgFetchAllToken],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.token.MsgFetchAllTokenHistory", MsgFetchAllTokenHistory],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.token.MsgUpdateTokenHistory", MsgUpdateTokenHistory],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.token.MsgUpdateToken", MsgUpdateToken],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.token.MsgDeactivateToken", MsgDeactivateToken],
    
];

export { msgTypes }
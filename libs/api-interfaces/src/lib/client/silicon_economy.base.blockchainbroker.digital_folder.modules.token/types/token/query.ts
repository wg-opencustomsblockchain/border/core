/* eslint-disable */
import _m0 from "protobufjs/minimal";
import { PageRequest, PageResponse } from "../cosmos/base/query/v1beta1/pagination";
import { Token } from "./token";
import { TokenHistory } from "./tokenHistory";

export const protobufPackage = "silicon_economy.base.blockchainbroker.digital_folder.modules.token";

/** this line is used by starport scaffolding # 3 */
export interface QueryGetTokenHistoryRequest {
  id: string;
}

export interface QueryGetTokenHistoryResponse {
  TokenHistory: TokenHistory | undefined;
}

export interface QueryAllTokenHistoryRequest {
  pagination: PageRequest | undefined;
}

export interface QueryAllTokenHistoryResponse {
  TokenHistory: TokenHistory[];
  pagination: PageResponse | undefined;
}

export interface QueryGetTokenRequest {
  id: string;
}

export interface QueryGetTokenResponse {
  Token: Token | undefined;
}

export interface QueryAllTokenRequest {
  pagination: PageRequest | undefined;
}

export interface QueryAllTokenResponse {
  Token: Token[];
  pagination: PageResponse | undefined;
}

function createBaseQueryGetTokenHistoryRequest(): QueryGetTokenHistoryRequest {
  return { id: "" };
}

export const QueryGetTokenHistoryRequest = {
  encode(message: QueryGetTokenHistoryRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.id !== "") {
      writer.uint32(10).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryGetTokenHistoryRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryGetTokenHistoryRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetTokenHistoryRequest {
    return { id: isSet(object.id) ? String(object.id) : "" };
  },

  toJSON(message: QueryGetTokenHistoryRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryGetTokenHistoryRequest>, I>>(object: I): QueryGetTokenHistoryRequest {
    const message = createBaseQueryGetTokenHistoryRequest();
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseQueryGetTokenHistoryResponse(): QueryGetTokenHistoryResponse {
  return { TokenHistory: undefined };
}

export const QueryGetTokenHistoryResponse = {
  encode(message: QueryGetTokenHistoryResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.TokenHistory !== undefined) {
      TokenHistory.encode(message.TokenHistory, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryGetTokenHistoryResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryGetTokenHistoryResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.TokenHistory = TokenHistory.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetTokenHistoryResponse {
    return { TokenHistory: isSet(object.TokenHistory) ? TokenHistory.fromJSON(object.TokenHistory) : undefined };
  },

  toJSON(message: QueryGetTokenHistoryResponse): unknown {
    const obj: any = {};
    message.TokenHistory !== undefined
      && (obj.TokenHistory = message.TokenHistory ? TokenHistory.toJSON(message.TokenHistory) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryGetTokenHistoryResponse>, I>>(object: I): QueryGetTokenHistoryResponse {
    const message = createBaseQueryGetTokenHistoryResponse();
    message.TokenHistory = (object.TokenHistory !== undefined && object.TokenHistory !== null)
      ? TokenHistory.fromPartial(object.TokenHistory)
      : undefined;
    return message;
  },
};

function createBaseQueryAllTokenHistoryRequest(): QueryAllTokenHistoryRequest {
  return { pagination: undefined };
}

export const QueryAllTokenHistoryRequest = {
  encode(message: QueryAllTokenHistoryRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.pagination !== undefined) {
      PageRequest.encode(message.pagination, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryAllTokenHistoryRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryAllTokenHistoryRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.pagination = PageRequest.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllTokenHistoryRequest {
    return { pagination: isSet(object.pagination) ? PageRequest.fromJSON(object.pagination) : undefined };
  },

  toJSON(message: QueryAllTokenHistoryRequest): unknown {
    const obj: any = {};
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageRequest.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryAllTokenHistoryRequest>, I>>(object: I): QueryAllTokenHistoryRequest {
    const message = createBaseQueryAllTokenHistoryRequest();
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageRequest.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

function createBaseQueryAllTokenHistoryResponse(): QueryAllTokenHistoryResponse {
  return { TokenHistory: [], pagination: undefined };
}

export const QueryAllTokenHistoryResponse = {
  encode(message: QueryAllTokenHistoryResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.TokenHistory) {
      TokenHistory.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.pagination !== undefined) {
      PageResponse.encode(message.pagination, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryAllTokenHistoryResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryAllTokenHistoryResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.TokenHistory.push(TokenHistory.decode(reader, reader.uint32()));
          break;
        case 2:
          message.pagination = PageResponse.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllTokenHistoryResponse {
    return {
      TokenHistory: Array.isArray(object?.TokenHistory)
        ? object.TokenHistory.map((e: any) => TokenHistory.fromJSON(e))
        : [],
      pagination: isSet(object.pagination) ? PageResponse.fromJSON(object.pagination) : undefined,
    };
  },

  toJSON(message: QueryAllTokenHistoryResponse): unknown {
    const obj: any = {};
    if (message.TokenHistory) {
      obj.TokenHistory = message.TokenHistory.map((e) => e ? TokenHistory.toJSON(e) : undefined);
    } else {
      obj.TokenHistory = [];
    }
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageResponse.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryAllTokenHistoryResponse>, I>>(object: I): QueryAllTokenHistoryResponse {
    const message = createBaseQueryAllTokenHistoryResponse();
    message.TokenHistory = object.TokenHistory?.map((e) => TokenHistory.fromPartial(e)) || [];
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageResponse.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

function createBaseQueryGetTokenRequest(): QueryGetTokenRequest {
  return { id: "" };
}

export const QueryGetTokenRequest = {
  encode(message: QueryGetTokenRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.id !== "") {
      writer.uint32(10).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryGetTokenRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryGetTokenRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetTokenRequest {
    return { id: isSet(object.id) ? String(object.id) : "" };
  },

  toJSON(message: QueryGetTokenRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryGetTokenRequest>, I>>(object: I): QueryGetTokenRequest {
    const message = createBaseQueryGetTokenRequest();
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseQueryGetTokenResponse(): QueryGetTokenResponse {
  return { Token: undefined };
}

export const QueryGetTokenResponse = {
  encode(message: QueryGetTokenResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.Token !== undefined) {
      Token.encode(message.Token, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryGetTokenResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryGetTokenResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.Token = Token.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryGetTokenResponse {
    return { Token: isSet(object.Token) ? Token.fromJSON(object.Token) : undefined };
  },

  toJSON(message: QueryGetTokenResponse): unknown {
    const obj: any = {};
    message.Token !== undefined && (obj.Token = message.Token ? Token.toJSON(message.Token) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryGetTokenResponse>, I>>(object: I): QueryGetTokenResponse {
    const message = createBaseQueryGetTokenResponse();
    message.Token = (object.Token !== undefined && object.Token !== null) ? Token.fromPartial(object.Token) : undefined;
    return message;
  },
};

function createBaseQueryAllTokenRequest(): QueryAllTokenRequest {
  return { pagination: undefined };
}

export const QueryAllTokenRequest = {
  encode(message: QueryAllTokenRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.pagination !== undefined) {
      PageRequest.encode(message.pagination, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryAllTokenRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryAllTokenRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.pagination = PageRequest.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllTokenRequest {
    return { pagination: isSet(object.pagination) ? PageRequest.fromJSON(object.pagination) : undefined };
  },

  toJSON(message: QueryAllTokenRequest): unknown {
    const obj: any = {};
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageRequest.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryAllTokenRequest>, I>>(object: I): QueryAllTokenRequest {
    const message = createBaseQueryAllTokenRequest();
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageRequest.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

function createBaseQueryAllTokenResponse(): QueryAllTokenResponse {
  return { Token: [], pagination: undefined };
}

export const QueryAllTokenResponse = {
  encode(message: QueryAllTokenResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.Token) {
      Token.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.pagination !== undefined) {
      PageResponse.encode(message.pagination, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryAllTokenResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryAllTokenResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.Token.push(Token.decode(reader, reader.uint32()));
          break;
        case 2:
          message.pagination = PageResponse.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QueryAllTokenResponse {
    return {
      Token: Array.isArray(object?.Token) ? object.Token.map((e: any) => Token.fromJSON(e)) : [],
      pagination: isSet(object.pagination) ? PageResponse.fromJSON(object.pagination) : undefined,
    };
  },

  toJSON(message: QueryAllTokenResponse): unknown {
    const obj: any = {};
    if (message.Token) {
      obj.Token = message.Token.map((e) => e ? Token.toJSON(e) : undefined);
    } else {
      obj.Token = [];
    }
    message.pagination !== undefined
      && (obj.pagination = message.pagination ? PageResponse.toJSON(message.pagination) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<QueryAllTokenResponse>, I>>(object: I): QueryAllTokenResponse {
    const message = createBaseQueryAllTokenResponse();
    message.Token = object.Token?.map((e) => Token.fromPartial(e)) || [];
    message.pagination = (object.pagination !== undefined && object.pagination !== null)
      ? PageResponse.fromPartial(object.pagination)
      : undefined;
    return message;
  },
};

/** Query defines the gRPC querier service. */
export interface Query {
  /** this line is used by starport scaffolding # 2 */
  TokenHistory(request: QueryGetTokenHistoryRequest): Promise<QueryGetTokenHistoryResponse>;
  /** option (google.api.http).get = "/silicon-economy/base/blockchainbroker/digital-folder/tokenmanager/Token/tokenHistory"; */
  TokenHistoryAll(request: QueryAllTokenHistoryRequest): Promise<QueryAllTokenHistoryResponse>;
  /** option (google.api.http).get = "/silicon-economy/base/blockchainbroker/digital-folder/tokenmanager/Token/token/{id}"; */
  Token(request: QueryGetTokenRequest): Promise<QueryGetTokenResponse>;
  /** option (google.api.http).get = "/silicon-economy/base/blockchainbroker/digital-folder/tokenmanager/Token/token"; */
  TokenAll(request: QueryAllTokenRequest): Promise<QueryAllTokenResponse>;
}

export class QueryClientImpl implements Query {
  private readonly rpc: Rpc;
  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.TokenHistory = this.TokenHistory.bind(this);
    this.TokenHistoryAll = this.TokenHistoryAll.bind(this);
    this.Token = this.Token.bind(this);
    this.TokenAll = this.TokenAll.bind(this);
  }
  TokenHistory(request: QueryGetTokenHistoryRequest): Promise<QueryGetTokenHistoryResponse> {
    const data = QueryGetTokenHistoryRequest.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.token.Query",
      "TokenHistory",
      data,
    );
    return promise.then((data) => QueryGetTokenHistoryResponse.decode(new _m0.Reader(data)));
  }

  TokenHistoryAll(request: QueryAllTokenHistoryRequest): Promise<QueryAllTokenHistoryResponse> {
    const data = QueryAllTokenHistoryRequest.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.token.Query",
      "TokenHistoryAll",
      data,
    );
    return promise.then((data) => QueryAllTokenHistoryResponse.decode(new _m0.Reader(data)));
  }

  Token(request: QueryGetTokenRequest): Promise<QueryGetTokenResponse> {
    const data = QueryGetTokenRequest.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.token.Query",
      "Token",
      data,
    );
    return promise.then((data) => QueryGetTokenResponse.decode(new _m0.Reader(data)));
  }

  TokenAll(request: QueryAllTokenRequest): Promise<QueryAllTokenResponse> {
    const data = QueryAllTokenRequest.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.token.Query",
      "TokenAll",
      data,
    );
    return promise.then((data) => QueryAllTokenResponse.decode(new _m0.Reader(data)));
  }
}

interface Rpc {
  request(service: string, method: string, data: Uint8Array): Promise<Uint8Array>;
}

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

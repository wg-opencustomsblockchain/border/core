/* eslint-disable */
import _m0 from "protobufjs/minimal";
import { Token } from "./token";
import { TokenHistory } from "./tokenHistory";

export const protobufPackage = "silicon_economy.base.blockchainbroker.digital_folder.modules.token";

/** this line is used by starport scaffolding # proto/tx/message */
export interface MsgFetchAllToken {
  creator: string;
}

export interface MsgFetchAllTokenResponse {
  Token: Token[];
}

export interface MsgFetchAllTokenHistory {
  creator: string;
}

export interface MsgFetchAllTokenHistoryResponse {
  TokenHistory: TokenHistory[];
}

export interface MsgFetchTokenHistory {
  creator: string;
  id: string;
}

export interface MsgFetchTokenHistoryResponse {
  TokenHistory: TokenHistory | undefined;
}

export interface MsgFetchToken {
  creator: string;
  id: string;
}

export interface MsgFetchTokenResponse {
  Token: Token | undefined;
}

export interface MsgCreateTokenHistory {
  creator: string;
  id: string;
  history: Token[];
}

export interface MsgUpdateTokenHistory {
  creator: string;
  id: string;
}

export interface MsgCreateToken {
  creator: string;
  id: string;
  timestamp: string;
  tokenType: string;
  changeMessage: string;
  segmentId: string;
}

export interface MsgUpdateToken {
  creator: string;
  id: string;
  timestamp: string;
  tokenType: string;
  changeMessage: string;
  segmentId: string;
}

export interface MsgDeactivateToken {
  creator: string;
  id: string;
}

export interface MsgActivateToken {
  creator: string;
  id: string;
}

export interface MsgUpdateTokenInformation {
  creator: string;
  tokenId: string;
  data: string;
}

export interface MsgEmptyResponse {
}

export interface MsgIdResponse {
  id: string;
}

function createBaseMsgFetchAllToken(): MsgFetchAllToken {
  return { creator: "" };
}

export const MsgFetchAllToken = {
  encode(message: MsgFetchAllToken, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchAllToken {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchAllToken();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllToken {
    return { creator: isSet(object.creator) ? String(object.creator) : "" };
  },

  toJSON(message: MsgFetchAllToken): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchAllToken>, I>>(object: I): MsgFetchAllToken {
    const message = createBaseMsgFetchAllToken();
    message.creator = object.creator ?? "";
    return message;
  },
};

function createBaseMsgFetchAllTokenResponse(): MsgFetchAllTokenResponse {
  return { Token: [] };
}

export const MsgFetchAllTokenResponse = {
  encode(message: MsgFetchAllTokenResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.Token) {
      Token.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchAllTokenResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchAllTokenResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.Token.push(Token.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllTokenResponse {
    return { Token: Array.isArray(object?.Token) ? object.Token.map((e: any) => Token.fromJSON(e)) : [] };
  },

  toJSON(message: MsgFetchAllTokenResponse): unknown {
    const obj: any = {};
    if (message.Token) {
      obj.Token = message.Token.map((e) => e ? Token.toJSON(e) : undefined);
    } else {
      obj.Token = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchAllTokenResponse>, I>>(object: I): MsgFetchAllTokenResponse {
    const message = createBaseMsgFetchAllTokenResponse();
    message.Token = object.Token?.map((e) => Token.fromPartial(e)) || [];
    return message;
  },
};

function createBaseMsgFetchAllTokenHistory(): MsgFetchAllTokenHistory {
  return { creator: "" };
}

export const MsgFetchAllTokenHistory = {
  encode(message: MsgFetchAllTokenHistory, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchAllTokenHistory {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchAllTokenHistory();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllTokenHistory {
    return { creator: isSet(object.creator) ? String(object.creator) : "" };
  },

  toJSON(message: MsgFetchAllTokenHistory): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchAllTokenHistory>, I>>(object: I): MsgFetchAllTokenHistory {
    const message = createBaseMsgFetchAllTokenHistory();
    message.creator = object.creator ?? "";
    return message;
  },
};

function createBaseMsgFetchAllTokenHistoryResponse(): MsgFetchAllTokenHistoryResponse {
  return { TokenHistory: [] };
}

export const MsgFetchAllTokenHistoryResponse = {
  encode(message: MsgFetchAllTokenHistoryResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.TokenHistory) {
      TokenHistory.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchAllTokenHistoryResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchAllTokenHistoryResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.TokenHistory.push(TokenHistory.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchAllTokenHistoryResponse {
    return {
      TokenHistory: Array.isArray(object?.TokenHistory)
        ? object.TokenHistory.map((e: any) => TokenHistory.fromJSON(e))
        : [],
    };
  },

  toJSON(message: MsgFetchAllTokenHistoryResponse): unknown {
    const obj: any = {};
    if (message.TokenHistory) {
      obj.TokenHistory = message.TokenHistory.map((e) => e ? TokenHistory.toJSON(e) : undefined);
    } else {
      obj.TokenHistory = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchAllTokenHistoryResponse>, I>>(
    object: I,
  ): MsgFetchAllTokenHistoryResponse {
    const message = createBaseMsgFetchAllTokenHistoryResponse();
    message.TokenHistory = object.TokenHistory?.map((e) => TokenHistory.fromPartial(e)) || [];
    return message;
  },
};

function createBaseMsgFetchTokenHistory(): MsgFetchTokenHistory {
  return { creator: "", id: "" };
}

export const MsgFetchTokenHistory = {
  encode(message: MsgFetchTokenHistory, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchTokenHistory {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchTokenHistory();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchTokenHistory {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
    };
  },

  toJSON(message: MsgFetchTokenHistory): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchTokenHistory>, I>>(object: I): MsgFetchTokenHistory {
    const message = createBaseMsgFetchTokenHistory();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgFetchTokenHistoryResponse(): MsgFetchTokenHistoryResponse {
  return { TokenHistory: undefined };
}

export const MsgFetchTokenHistoryResponse = {
  encode(message: MsgFetchTokenHistoryResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.TokenHistory !== undefined) {
      TokenHistory.encode(message.TokenHistory, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchTokenHistoryResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchTokenHistoryResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.TokenHistory = TokenHistory.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchTokenHistoryResponse {
    return { TokenHistory: isSet(object.TokenHistory) ? TokenHistory.fromJSON(object.TokenHistory) : undefined };
  },

  toJSON(message: MsgFetchTokenHistoryResponse): unknown {
    const obj: any = {};
    message.TokenHistory !== undefined
      && (obj.TokenHistory = message.TokenHistory ? TokenHistory.toJSON(message.TokenHistory) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchTokenHistoryResponse>, I>>(object: I): MsgFetchTokenHistoryResponse {
    const message = createBaseMsgFetchTokenHistoryResponse();
    message.TokenHistory = (object.TokenHistory !== undefined && object.TokenHistory !== null)
      ? TokenHistory.fromPartial(object.TokenHistory)
      : undefined;
    return message;
  },
};

function createBaseMsgFetchToken(): MsgFetchToken {
  return { creator: "", id: "" };
}

export const MsgFetchToken = {
  encode(message: MsgFetchToken, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchToken {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchToken();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchToken {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
    };
  },

  toJSON(message: MsgFetchToken): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchToken>, I>>(object: I): MsgFetchToken {
    const message = createBaseMsgFetchToken();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgFetchTokenResponse(): MsgFetchTokenResponse {
  return { Token: undefined };
}

export const MsgFetchTokenResponse = {
  encode(message: MsgFetchTokenResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.Token !== undefined) {
      Token.encode(message.Token, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgFetchTokenResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgFetchTokenResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.Token = Token.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgFetchTokenResponse {
    return { Token: isSet(object.Token) ? Token.fromJSON(object.Token) : undefined };
  },

  toJSON(message: MsgFetchTokenResponse): unknown {
    const obj: any = {};
    message.Token !== undefined && (obj.Token = message.Token ? Token.toJSON(message.Token) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgFetchTokenResponse>, I>>(object: I): MsgFetchTokenResponse {
    const message = createBaseMsgFetchTokenResponse();
    message.Token = (object.Token !== undefined && object.Token !== null) ? Token.fromPartial(object.Token) : undefined;
    return message;
  },
};

function createBaseMsgCreateTokenHistory(): MsgCreateTokenHistory {
  return { creator: "", id: "", history: [] };
}

export const MsgCreateTokenHistory = {
  encode(message: MsgCreateTokenHistory, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    for (const v of message.history) {
      Token.encode(v!, writer.uint32(26).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateTokenHistory {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateTokenHistory();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.history.push(Token.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateTokenHistory {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      history: Array.isArray(object?.history) ? object.history.map((e: any) => Token.fromJSON(e)) : [],
    };
  },

  toJSON(message: MsgCreateTokenHistory): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    if (message.history) {
      obj.history = message.history.map((e) => e ? Token.toJSON(e) : undefined);
    } else {
      obj.history = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateTokenHistory>, I>>(object: I): MsgCreateTokenHistory {
    const message = createBaseMsgCreateTokenHistory();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.history = object.history?.map((e) => Token.fromPartial(e)) || [];
    return message;
  },
};

function createBaseMsgUpdateTokenHistory(): MsgUpdateTokenHistory {
  return { creator: "", id: "" };
}

export const MsgUpdateTokenHistory = {
  encode(message: MsgUpdateTokenHistory, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgUpdateTokenHistory {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgUpdateTokenHistory();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateTokenHistory {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
    };
  },

  toJSON(message: MsgUpdateTokenHistory): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgUpdateTokenHistory>, I>>(object: I): MsgUpdateTokenHistory {
    const message = createBaseMsgUpdateTokenHistory();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgCreateToken(): MsgCreateToken {
  return { creator: "", id: "", timestamp: "", tokenType: "", changeMessage: "", segmentId: "" };
}

export const MsgCreateToken = {
  encode(message: MsgCreateToken, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.timestamp !== "") {
      writer.uint32(26).string(message.timestamp);
    }
    if (message.tokenType !== "") {
      writer.uint32(34).string(message.tokenType);
    }
    if (message.changeMessage !== "") {
      writer.uint32(42).string(message.changeMessage);
    }
    if (message.segmentId !== "") {
      writer.uint32(50).string(message.segmentId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgCreateToken {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgCreateToken();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.timestamp = reader.string();
          break;
        case 4:
          message.tokenType = reader.string();
          break;
        case 5:
          message.changeMessage = reader.string();
          break;
        case 6:
          message.segmentId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgCreateToken {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      timestamp: isSet(object.timestamp) ? String(object.timestamp) : "",
      tokenType: isSet(object.tokenType) ? String(object.tokenType) : "",
      changeMessage: isSet(object.changeMessage) ? String(object.changeMessage) : "",
      segmentId: isSet(object.segmentId) ? String(object.segmentId) : "",
    };
  },

  toJSON(message: MsgCreateToken): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.timestamp !== undefined && (obj.timestamp = message.timestamp);
    message.tokenType !== undefined && (obj.tokenType = message.tokenType);
    message.changeMessage !== undefined && (obj.changeMessage = message.changeMessage);
    message.segmentId !== undefined && (obj.segmentId = message.segmentId);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgCreateToken>, I>>(object: I): MsgCreateToken {
    const message = createBaseMsgCreateToken();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.timestamp = object.timestamp ?? "";
    message.tokenType = object.tokenType ?? "";
    message.changeMessage = object.changeMessage ?? "";
    message.segmentId = object.segmentId ?? "";
    return message;
  },
};

function createBaseMsgUpdateToken(): MsgUpdateToken {
  return { creator: "", id: "", timestamp: "", tokenType: "", changeMessage: "", segmentId: "" };
}

export const MsgUpdateToken = {
  encode(message: MsgUpdateToken, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.timestamp !== "") {
      writer.uint32(26).string(message.timestamp);
    }
    if (message.tokenType !== "") {
      writer.uint32(34).string(message.tokenType);
    }
    if (message.changeMessage !== "") {
      writer.uint32(42).string(message.changeMessage);
    }
    if (message.segmentId !== "") {
      writer.uint32(50).string(message.segmentId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgUpdateToken {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgUpdateToken();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.timestamp = reader.string();
          break;
        case 4:
          message.tokenType = reader.string();
          break;
        case 5:
          message.changeMessage = reader.string();
          break;
        case 6:
          message.segmentId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateToken {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      timestamp: isSet(object.timestamp) ? String(object.timestamp) : "",
      tokenType: isSet(object.tokenType) ? String(object.tokenType) : "",
      changeMessage: isSet(object.changeMessage) ? String(object.changeMessage) : "",
      segmentId: isSet(object.segmentId) ? String(object.segmentId) : "",
    };
  },

  toJSON(message: MsgUpdateToken): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.timestamp !== undefined && (obj.timestamp = message.timestamp);
    message.tokenType !== undefined && (obj.tokenType = message.tokenType);
    message.changeMessage !== undefined && (obj.changeMessage = message.changeMessage);
    message.segmentId !== undefined && (obj.segmentId = message.segmentId);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgUpdateToken>, I>>(object: I): MsgUpdateToken {
    const message = createBaseMsgUpdateToken();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.timestamp = object.timestamp ?? "";
    message.tokenType = object.tokenType ?? "";
    message.changeMessage = object.changeMessage ?? "";
    message.segmentId = object.segmentId ?? "";
    return message;
  },
};

function createBaseMsgDeactivateToken(): MsgDeactivateToken {
  return { creator: "", id: "" };
}

export const MsgDeactivateToken = {
  encode(message: MsgDeactivateToken, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgDeactivateToken {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgDeactivateToken();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgDeactivateToken {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
    };
  },

  toJSON(message: MsgDeactivateToken): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgDeactivateToken>, I>>(object: I): MsgDeactivateToken {
    const message = createBaseMsgDeactivateToken();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgActivateToken(): MsgActivateToken {
  return { creator: "", id: "" };
}

export const MsgActivateToken = {
  encode(message: MsgActivateToken, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgActivateToken {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgActivateToken();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgActivateToken {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
    };
  },

  toJSON(message: MsgActivateToken): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgActivateToken>, I>>(object: I): MsgActivateToken {
    const message = createBaseMsgActivateToken();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseMsgUpdateTokenInformation(): MsgUpdateTokenInformation {
  return { creator: "", tokenId: "", data: "" };
}

export const MsgUpdateTokenInformation = {
  encode(message: MsgUpdateTokenInformation, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.tokenId !== "") {
      writer.uint32(18).string(message.tokenId);
    }
    if (message.data !== "") {
      writer.uint32(26).string(message.data);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgUpdateTokenInformation {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgUpdateTokenInformation();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.tokenId = reader.string();
          break;
        case 3:
          message.data = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgUpdateTokenInformation {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      tokenId: isSet(object.tokenId) ? String(object.tokenId) : "",
      data: isSet(object.data) ? String(object.data) : "",
    };
  },

  toJSON(message: MsgUpdateTokenInformation): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.tokenId !== undefined && (obj.tokenId = message.tokenId);
    message.data !== undefined && (obj.data = message.data);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgUpdateTokenInformation>, I>>(object: I): MsgUpdateTokenInformation {
    const message = createBaseMsgUpdateTokenInformation();
    message.creator = object.creator ?? "";
    message.tokenId = object.tokenId ?? "";
    message.data = object.data ?? "";
    return message;
  },
};

function createBaseMsgEmptyResponse(): MsgEmptyResponse {
  return {};
}

export const MsgEmptyResponse = {
  encode(_: MsgEmptyResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgEmptyResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgEmptyResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgEmptyResponse {
    return {};
  },

  toJSON(_: MsgEmptyResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgEmptyResponse>, I>>(_: I): MsgEmptyResponse {
    const message = createBaseMsgEmptyResponse();
    return message;
  },
};

function createBaseMsgIdResponse(): MsgIdResponse {
  return { id: "" };
}

export const MsgIdResponse = {
  encode(message: MsgIdResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.id !== "") {
      writer.uint32(10).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgIdResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgIdResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgIdResponse {
    return { id: isSet(object.id) ? String(object.id) : "" };
  },

  toJSON(message: MsgIdResponse): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgIdResponse>, I>>(object: I): MsgIdResponse {
    const message = createBaseMsgIdResponse();
    message.id = object.id ?? "";
    return message;
  },
};

/** Msg defines the Msg service. */
export interface Msg {
  /** this line is used by starport scaffolding # proto/tx/rpc */
  FetchAllToken(request: MsgFetchAllToken): Promise<MsgFetchAllTokenResponse>;
  FetchAllTokenHistory(request: MsgFetchAllTokenHistory): Promise<MsgFetchAllTokenHistoryResponse>;
  FetchTokenHistory(request: MsgFetchTokenHistory): Promise<MsgFetchTokenHistoryResponse>;
  FetchToken(request: MsgFetchToken): Promise<MsgFetchTokenResponse>;
  CreateToken(request: MsgCreateToken): Promise<MsgIdResponse>;
  UpdateToken(request: MsgUpdateToken): Promise<MsgEmptyResponse>;
  DeactivateToken(request: MsgDeactivateToken): Promise<MsgEmptyResponse>;
  ActivateToken(request: MsgActivateToken): Promise<MsgEmptyResponse>;
  UpdateTokenInformation(request: MsgUpdateTokenInformation): Promise<MsgEmptyResponse>;
}

export class MsgClientImpl implements Msg {
  private readonly rpc: Rpc;
  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.FetchAllToken = this.FetchAllToken.bind(this);
    this.FetchAllTokenHistory = this.FetchAllTokenHistory.bind(this);
    this.FetchTokenHistory = this.FetchTokenHistory.bind(this);
    this.FetchToken = this.FetchToken.bind(this);
    this.CreateToken = this.CreateToken.bind(this);
    this.UpdateToken = this.UpdateToken.bind(this);
    this.DeactivateToken = this.DeactivateToken.bind(this);
    this.ActivateToken = this.ActivateToken.bind(this);
    this.UpdateTokenInformation = this.UpdateTokenInformation.bind(this);
  }
  FetchAllToken(request: MsgFetchAllToken): Promise<MsgFetchAllTokenResponse> {
    const data = MsgFetchAllToken.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.token.Msg",
      "FetchAllToken",
      data,
    );
    return promise.then((data) => MsgFetchAllTokenResponse.decode(new _m0.Reader(data)));
  }

  FetchAllTokenHistory(request: MsgFetchAllTokenHistory): Promise<MsgFetchAllTokenHistoryResponse> {
    const data = MsgFetchAllTokenHistory.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.token.Msg",
      "FetchAllTokenHistory",
      data,
    );
    return promise.then((data) => MsgFetchAllTokenHistoryResponse.decode(new _m0.Reader(data)));
  }

  FetchTokenHistory(request: MsgFetchTokenHistory): Promise<MsgFetchTokenHistoryResponse> {
    const data = MsgFetchTokenHistory.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.token.Msg",
      "FetchTokenHistory",
      data,
    );
    return promise.then((data) => MsgFetchTokenHistoryResponse.decode(new _m0.Reader(data)));
  }

  FetchToken(request: MsgFetchToken): Promise<MsgFetchTokenResponse> {
    const data = MsgFetchToken.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.token.Msg",
      "FetchToken",
      data,
    );
    return promise.then((data) => MsgFetchTokenResponse.decode(new _m0.Reader(data)));
  }

  CreateToken(request: MsgCreateToken): Promise<MsgIdResponse> {
    const data = MsgCreateToken.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.token.Msg",
      "CreateToken",
      data,
    );
    return promise.then((data) => MsgIdResponse.decode(new _m0.Reader(data)));
  }

  UpdateToken(request: MsgUpdateToken): Promise<MsgEmptyResponse> {
    const data = MsgUpdateToken.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.token.Msg",
      "UpdateToken",
      data,
    );
    return promise.then((data) => MsgEmptyResponse.decode(new _m0.Reader(data)));
  }

  DeactivateToken(request: MsgDeactivateToken): Promise<MsgEmptyResponse> {
    const data = MsgDeactivateToken.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.token.Msg",
      "DeactivateToken",
      data,
    );
    return promise.then((data) => MsgEmptyResponse.decode(new _m0.Reader(data)));
  }

  ActivateToken(request: MsgActivateToken): Promise<MsgEmptyResponse> {
    const data = MsgActivateToken.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.token.Msg",
      "ActivateToken",
      data,
    );
    return promise.then((data) => MsgEmptyResponse.decode(new _m0.Reader(data)));
  }

  UpdateTokenInformation(request: MsgUpdateTokenInformation): Promise<MsgEmptyResponse> {
    const data = MsgUpdateTokenInformation.encode(request).finish();
    const promise = this.rpc.request(
      "silicon_economy.base.blockchainbroker.digital_folder.modules.token.Msg",
      "UpdateTokenInformation",
      data,
    );
    return promise.then((data) => MsgEmptyResponse.decode(new _m0.Reader(data)));
  }
}

interface Rpc {
  request(service: string, method: string, data: Uint8Array): Promise<Uint8Array>;
}

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

/* eslint-disable */
import _m0 from "protobufjs/minimal";
import { Token } from "./token";
import { TokenHistory } from "./tokenHistory";

export const protobufPackage = "silicon_economy.base.blockchainbroker.digital_folder.modules.token";

/** GenesisState defines the Token module's genesis state. */
export interface GenesisState {
  /** this line is used by starport scaffolding # genesis/proto/state */
  tokenHistoryList: TokenHistory[];
  /** this line is used by starport scaffolding # genesis/proto/stateField */
  tokenList: Token[];
}

function createBaseGenesisState(): GenesisState {
  return { tokenHistoryList: [], tokenList: [] };
}

export const GenesisState = {
  encode(message: GenesisState, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.tokenHistoryList) {
      TokenHistory.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    for (const v of message.tokenList) {
      Token.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): GenesisState {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseGenesisState();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 2:
          message.tokenHistoryList.push(TokenHistory.decode(reader, reader.uint32()));
          break;
        case 1:
          message.tokenList.push(Token.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GenesisState {
    return {
      tokenHistoryList: Array.isArray(object?.tokenHistoryList)
        ? object.tokenHistoryList.map((e: any) => TokenHistory.fromJSON(e))
        : [],
      tokenList: Array.isArray(object?.tokenList) ? object.tokenList.map((e: any) => Token.fromJSON(e)) : [],
    };
  },

  toJSON(message: GenesisState): unknown {
    const obj: any = {};
    if (message.tokenHistoryList) {
      obj.tokenHistoryList = message.tokenHistoryList.map((e) => e ? TokenHistory.toJSON(e) : undefined);
    } else {
      obj.tokenHistoryList = [];
    }
    if (message.tokenList) {
      obj.tokenList = message.tokenList.map((e) => e ? Token.toJSON(e) : undefined);
    } else {
      obj.tokenList = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<GenesisState>, I>>(object: I): GenesisState {
    const message = createBaseGenesisState();
    message.tokenHistoryList = object.tokenHistoryList?.map((e) => TokenHistory.fromPartial(e)) || [];
    message.tokenList = object.tokenList?.map((e) => Token.fromPartial(e)) || [];
    return message;
  },
};

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

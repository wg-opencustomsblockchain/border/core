/* eslint-disable */
import _m0 from "protobufjs/minimal";

export const protobufPackage = "silicon_economy.base.blockchainbroker.digital_folder.modules.token";

export interface Token {
  creator: string;
  id: string;
  tokenType: string;
  timestamp: string;
  changeMessage: string;
  valid: boolean;
  info: Info | undefined;
  segmentId: string;
}

export interface Info {
  data: string;
}

function createBaseToken(): Token {
  return {
    creator: "",
    id: "",
    tokenType: "",
    timestamp: "",
    changeMessage: "",
    valid: false,
    info: undefined,
    segmentId: "",
  };
}

export const Token = {
  encode(message: Token, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.creator !== "") {
      writer.uint32(10).string(message.creator);
    }
    if (message.id !== "") {
      writer.uint32(18).string(message.id);
    }
    if (message.tokenType !== "") {
      writer.uint32(26).string(message.tokenType);
    }
    if (message.timestamp !== "") {
      writer.uint32(34).string(message.timestamp);
    }
    if (message.changeMessage !== "") {
      writer.uint32(42).string(message.changeMessage);
    }
    if (message.valid === true) {
      writer.uint32(48).bool(message.valid);
    }
    if (message.info !== undefined) {
      Info.encode(message.info, writer.uint32(58).fork()).ldelim();
    }
    if (message.segmentId !== "") {
      writer.uint32(66).string(message.segmentId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Token {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseToken();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string();
          break;
        case 2:
          message.id = reader.string();
          break;
        case 3:
          message.tokenType = reader.string();
          break;
        case 4:
          message.timestamp = reader.string();
          break;
        case 5:
          message.changeMessage = reader.string();
          break;
        case 6:
          message.valid = reader.bool();
          break;
        case 7:
          message.info = Info.decode(reader, reader.uint32());
          break;
        case 8:
          message.segmentId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Token {
    return {
      creator: isSet(object.creator) ? String(object.creator) : "",
      id: isSet(object.id) ? String(object.id) : "",
      tokenType: isSet(object.tokenType) ? String(object.tokenType) : "",
      timestamp: isSet(object.timestamp) ? String(object.timestamp) : "",
      changeMessage: isSet(object.changeMessage) ? String(object.changeMessage) : "",
      valid: isSet(object.valid) ? Boolean(object.valid) : false,
      info: isSet(object.info) ? Info.fromJSON(object.info) : undefined,
      segmentId: isSet(object.segmentId) ? String(object.segmentId) : "",
    };
  },

  toJSON(message: Token): unknown {
    const obj: any = {};
    message.creator !== undefined && (obj.creator = message.creator);
    message.id !== undefined && (obj.id = message.id);
    message.tokenType !== undefined && (obj.tokenType = message.tokenType);
    message.timestamp !== undefined && (obj.timestamp = message.timestamp);
    message.changeMessage !== undefined && (obj.changeMessage = message.changeMessage);
    message.valid !== undefined && (obj.valid = message.valid);
    message.info !== undefined && (obj.info = message.info ? Info.toJSON(message.info) : undefined);
    message.segmentId !== undefined && (obj.segmentId = message.segmentId);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<Token>, I>>(object: I): Token {
    const message = createBaseToken();
    message.creator = object.creator ?? "";
    message.id = object.id ?? "";
    message.tokenType = object.tokenType ?? "";
    message.timestamp = object.timestamp ?? "";
    message.changeMessage = object.changeMessage ?? "";
    message.valid = object.valid ?? false;
    message.info = (object.info !== undefined && object.info !== null) ? Info.fromPartial(object.info) : undefined;
    message.segmentId = object.segmentId ?? "";
    return message;
  },
};

function createBaseInfo(): Info {
  return { data: "" };
}

export const Info = {
  encode(message: Info, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.data !== "") {
      writer.uint32(10).string(message.data);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Info {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseInfo();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.data = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Info {
    return { data: isSet(object.data) ? String(object.data) : "" };
  },

  toJSON(message: Info): unknown {
    const obj: any = {};
    message.data !== undefined && (obj.data = message.data);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<Info>, I>>(object: I): Info {
    const message = createBaseInfo();
    message.data = object.data ?? "";
    return message;
  },
};

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

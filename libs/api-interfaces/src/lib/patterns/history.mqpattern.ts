/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export enum HistoryMqPattern {
  PREFIX = 'v1/',
  GENERATE_HISTORY = 'generate/export',
  GENERATE_HISTORY_IMPORT = 'generate/import',
  CREATE_HISTORY_IMPORT = 'create/import',
  CREATE_HISTORY_EXPORT = 'create/export',
  GET_HISTORY = 'get'
}

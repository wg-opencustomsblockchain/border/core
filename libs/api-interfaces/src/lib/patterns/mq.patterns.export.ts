/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/** Enum for MQ API. */
export enum ExportMQPatterns {
  // MQ EAD TAGS
  MQ_API_TAG = 'v2',
  MQ_EAD_LIST = '/blockchain/list',
  MQ_EAD_CREATE = '/blockchain/create',
  MQ_EAD_FIND = '/blockchain/find/mrn',
  MQ_EAD_UPDATE = '/blockchain/update',
  MQ_EAD_FIND_USER = '/blockchain/find/user',
  MQ_EAD_ARCHIVED = '/blockchain/archive',
  MQ_EAD_FIND_IMPORT = '/blockchain/find/import',
  TOKEN_FETCH_HISTORY = '/blockchain/history',

  // MQ IMPORT TAGS
  MQ_EAD_IMPORT_LIST = 'import/list',
  MQ_IMPORT_LIST = '/blockchain/import/list',
  MQ_IMPORT_CREATE = '/blockchain/import/create',
  MQ_IMPORT_UPDATE = '/blockchain/import/update',
  MQ_IMPORT_FIND = '/blockchain/import/find/id',
  MQ_IMPORT_FIND_IMPORT_ID = '/blockchain/import/find/importid',
  MQ_IMPORT_FIND_USER = '/blockchain/import/find/user',

  //MQ EVENT TAGS
  MQ_EXPORT_EVENTS = 'blockchain/export/events',
  MQ_EXPORT_EVENT = 'blockchain/export/event',
}

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export enum AmqpClientEnum {
  AMQP_QUEUE_AUTH = 'BORDER_AUTH',
  AMQP_QUEUE_BLOCKCHAIN = 'BORDER_BLOCKCHAIN',
  AMQP_QUEUE_COMPANY = 'BORDER_COMPANY',
  AMQP_QUEUE_HISTORY = 'BORDER_HISTORY',
  AMQP_QUEUE_DATA = 'BORDER_DATA',

  AMQP_CLIENT_AUTH = 'AUTH_SERVICE',
  AMQP_CLIENT_BLOCKCHAIN = 'BLOCKCHAIN_SERVICE',
  AMQP_CLIENT_COMPANY = 'COMPANY_SERVICE',
  AMQP_CLIENT_HISTORY = 'HISTORY_SERVICE',
  AMQP_CLIENT_DATA = 'DATA_SERVICE',
}

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/** Rest Patterns to for serving the API */
export enum BorderApi {
  GLOBAL_PREFIX = 'border',

  // Export
  EXPORT_CONTROLLER_API_TAG = 'DataControllerExport',
  EXPORT_API_VERSION = 'v1',
  EXPORT_CONTROLLER = 'exports',
  EXPORT_GET = ':id',
  EXPORT_GET_ALL = '',
  EXPORT_CREATE = '',
  EXPORT_UPDATE = '',
  EXPORT_GET_USER = 'user',
  EXPORT_GET_ARCHIVED = 'archived',
  EXPORT_GET_HISTORY = 'history',
  EXPORT_GET_IMPORT_READY = 'import',

  // Import
  IMPORT_CONTROLLER_API_TAG = 'DataControllerImport',
  IMPORT_API_VERSION = 'v1',
  IMPORT_CONTROLLER = 'imports',
  IMPORT_GET = ':id',
  IMPORT_GET_ALL = '',
  IMPORT_CREATE = '',
  IMPORT_UPDATE = '',
  IMPORT_GET_USER = 'user',
}

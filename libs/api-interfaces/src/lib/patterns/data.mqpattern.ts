/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export enum DataMqPattern {
  PREFIX = 'v2',
  DATA_IMPORT_CREATE = '/data/import/create',
  DATA_IMPORT_UPDATE = '/data/import/update',
  DATA_IMPORT_LIST = '/data/import/list',
  DATA_IMPORT_FIND = '/data/import/find/id',
  DATA_IMPORT_FIND_IMPORT = '/data/import/find/importId',
  DATA_IMPORT_USER = '/data/import/find/user',

  DATA_EXPORT_CREATE = '/data/export/create',
  DATA_EXPORT_UPDATE = '/data/export/update',
  DATA_EXPORT_ALL = '/data/export/list',
  DATA_EXPORT_FIND = '/data/export/find/id',
  DATA_EXPORT_USER = '/data/export/find/user',
  DATA_EXPORT_ARCHIVED = '/data/export/archived',
  DATA_EXPORT_IMPORTS = '/data/export/import',
}

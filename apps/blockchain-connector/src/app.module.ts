/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';

import { ExportTokenModule } from './exporttoken/exporttoken.module';
import { ImportTokenModule } from './importtoken/importtoken.module';

/**
 * Base Module for the App.
 * Imports the required submodules.
 */
@Module({
  imports: [
    ImportTokenModule,
    ExportTokenModule,
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: ['.env'],
    }),
  ],
  providers: [],
})
export class AppModule {}

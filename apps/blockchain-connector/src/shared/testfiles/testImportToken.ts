/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { QueryGetImportTokenResponse } from '@border/api-interfaces/lib/client/org.borderblockchain.importtoken';
import { ImportTokenDTO } from '../../importtoken/dto/import.dto';

export const IMPORT_MOCK: ImportTokenDTO = {
  creator: 'cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462y',
  id: 'f67d5df3-0d5c-4468-98f3-499844194df8',
  timestamp: new Date().toDateString(),
  importId: 'importId',
  status: '0',
  uniqueConsignmentReference: 'string',
  itinerary: 'string',
  relatedExportToken: '',
  consignor: {
    customsId: 'DE8998264',
    address: {
      streetAndNumber: 'string',
      postcode: 'string',
      city: 'string',
      countryCode: 'string',
    },
    name: 'string',
    subsidiaryNumber: 'string',
    identificationNumber: 'DE537400371045831',
  },
  exporter: {
    customsId: 'DE8998264',
    address: {
      streetAndNumber: 'string',
      postcode: 'string',
      city: 'string',
      countryCode: 'string',
    },
    name: 'string',
    subsidiaryNumber: 'string',
    identificationNumber: 'UK1234567891234',
  },
  consignee: {
    customsId: 'DE8998264',
    address: {
      streetAndNumber: 'string',
      postcode: 'string',
      city: 'string',
      countryCode: 'string',
    },
    name: 'string',
    subsidiaryNumber: 'string',
    identificationNumber: 'string',
  },
  declarant: {
    customsId: 'DE8998264',
    address: {
      streetAndNumber: 'string',
      postcode: 'string',
      city: 'string',
      countryCode: 'string',
    },
    name: 'string',
    subsidiaryNumber: 'string',
    identificationNumber: '',
  },
  customOfficeOfExit: {
    address: {
      streetAndNumber: 'string',
      postcode: 'string',
      city: 'string',
      countryCode: 'string',
    },
    customsOfficeCode: 'string',
    customsOfficeName: 'string',
  },
  customOfficeOfEntry: {
    address: {
      streetAndNumber: 'string',
      postcode: 'string',
      city: 'string',
      countryCode: 'string',
    },
    customsOfficeCode: 'string',
    customsOfficeName: 'string',
  },
  customOfficeOfImport: {
    address: {
      streetAndNumber: 'string',
      postcode: 'string',
      city: 'string',
      countryCode: 'string',
    },
    customsOfficeCode: 'string',
    customsOfficeName: 'string',
  },
  goodsItems: [
    {
      documents: [],
      consigneeOrderNumber: '1234',
      containerId: '12',
      countryOfOrigin: 'DE',
      localClassificationCode: '0',
      typeOfPackages: 'RTL',
      marksNumbers: undefined,
      dangerousGoodsCode: -1,
      valueAtBorder: 2000,
      valueAtBorderCurrency: 'EUR',
      amountInvoiced: 1000,
      amountInvoicedCurrency: 'EUR',
      sequenceNumber: 1,
      descriptionOfGoods: 'Biegeleiste',
      grossMass: '2100',
      netMass: '2000',
      numberOfPackages: 100,
      harmonizedSystemSubheadingCode: '0',
    },
  ],
  transportAtBorder: {
    transportCosts: undefined,
    transportCostsCurrency: 'EUR',
    transportOrderNumber: '12',
    modeOfTransport: 40,
    identity: 'DO FI 2222',
    nationality: 'DE',
    typeOfIdentification: 40,
  },
  transportFromBorder: {
    transportCosts: undefined,
    transportCostsCurrency: 'EUR',
    transportOrderNumber: '12',
    modeOfTransport: 40,
    identity: 'DO FI 2222',
    nationality: 'DE',
    typeOfIdentification: 40,
  },
  localReferenceNumber: 'string',
  destinationCountry: 'string',
  exportCountry: '',
  incotermCode: 'string',
  incotermLocation: 'string',
  totalGrossMass: '0',
  goodsItemQuantity: 0,
  totalPackagesQuantity: 0,
  releaseDateAndTime: 'string',
  natureOfTransaction: 0,
  totalAmountInvoiced: 0,
  invoiceCurrency: 'string',
  events: [
    {
      creator: 'cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462y',
      status: '0',
      message: 'Created',
      eventType: 'IMPORT_ACCEPTED',
      timestamp: '2021-10-14T19:42:40Z',
    },
  ],
};

export const IMPORT_MOCK_RESPONSE: QueryGetImportTokenResponse = {
  importToken: IMPORT_MOCK,
  events: [IMPORT_MOCK.events[0]],
};

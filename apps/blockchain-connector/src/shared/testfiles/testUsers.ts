/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { User } from '@border/api-interfaces';
import { Role } from '../model/entities/role.entity';

export const USER_MOCK: User = {
  attributes: {
    wallet: ['...'],
    mnemonic: ['...'],
    pubkey: ['...'],
    company: [
      {
        subsidiaryNumber: 1,
        name: 'Ina Import Ltd.',
        address: {
          streetAndNumber: '...',
          postcode: '...',
          city: '...',
          countryCode: 'GB',
        },
        identificationNumber: 'UK1234567891234',
        customsId: '...',
      },
    ],
  },
  id: '1',
  username: 'Ina',
};

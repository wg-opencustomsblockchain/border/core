/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ExportTokenDTO } from '../../exporttoken/dto/export.dto';
import {
  ExportTokenListResponse,
  GetExportTokenResponseDTO,
} from '../../exporttoken/dto/exporttoken.query.dto';
import { FieldInformationDto } from '@border/api-interfaces/lib/entities/history/history.dto';
import { ExportEvent } from '@border/api-interfaces/lib/client/org.borderblockchain.exporttoken';

export const EXPORT_MOCK: ExportTokenDTO = {
  creator: 'cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462y',
  tokenWalletId: ' DE537400371045831',
  id: 'E67d5df3-0d5c-4468-98f3-499844194df8',
  exportId: '20DE295638991213E1',
  localReferenceNumber: '',
  goodsItemQuantity: 5,
  timestamp: new Date().toDateString(),
  status: '0',
  totalPackagesQuantity: 5,
  destinationCountry: 'US',
  exportCountry: 'DE',
  incotermCode: 'DDP',
  totalGrossMass: '6300',
  uniqueConsignmentReference: undefined,
  itinerary: '["DE", "NL"]',
  incotermLocation: 'DE',
  natureOfTransaction: 15,
  totalAmountInvoiced: 4500,

  exporter: {
    customsId: 'DE8998264',
    name: 'Erik Exporteur GmbH',
    address: {
      city: 'Warshav',
      countryCode: 'RU',
      postcode: 'XD-FD-6142',
      streetAndNumber: '',
    },
    identificationNumber: 'DE537400371045831',
    subsidiaryNumber: '',
  },
  consignee: {
    name: 'Ina Import Ltd.',
    address: {
      city: 'Dortmund',
      countryCode: 'DE',
      postcode: '442277',
      streetAndNumber: '',
    },
    customsId: 'AF956D9DK',
    subsidiaryNumber: '50',
    identificationNumber: 'UK1234567891234',
  },
  declarant: {
    name: 'Ina Import Ltd.',
    address: {
      city: 'Dortmund',
      countryCode: 'DE',
      postcode: '442277',
      streetAndNumber: '',
    },
    customsId: 'AF956D9DK',
    subsidiaryNumber: '50',
    identificationNumber: 'UK1234567891234',
  },
  representative: {
    name: 'Ina Import Ltd.',
    address: {
      city: 'Dortmund',
      countryCode: 'DE',
      postcode: '442277',
      streetAndNumber: '',
    },
    customsId: 'AF956D9DK',
    subsidiaryNumber: '50',
    identificationNumber: 'UK1234567891234',
  },
  customOfficeOfExit: {
    customsOfficeCode: 'DE007154',
    address: {
      city: '',
      countryCode: '',
      postcode: '',
      streetAndNumber: '',
    },
    customsOfficeName: '',
  },
  customsOfficeOfExport: {
    customsOfficeCode: 'DE007154',
    address: {
      city: '',
      countryCode: '',
      postcode: '',
      streetAndNumber: '',
    },
    customsOfficeName: '',
  },
  transportToBorder: {
    transportCost: undefined,
    transportCostCurrency: 'EUR',
    transportOrderNumber: '12',
    modeOfTransport: 40,
    identity: 'DO FI 2222',
    nationality: 'DE',
    typeOfIdentification: 40,
  },
  transportAtBorder: {
    transportCost: undefined,
    transportCostCurrency: 'EUR',
    transportOrderNumber: '12',
    identity: 'RRFFSS',
    modeOfTransport: 0,
    nationality: '',
    typeOfIdentification: 0,
  },
  goodsItems: [
    {
      documents: [],
      consigneeOrderNumber: '1234',
      containerId: '12',
      countryOfOrigin: 'DE',
      localClassificationCode: '0',
      typeOfPackages: 'RTL',
      marksNumbers: undefined,
      dangerousGoodsCode: -1,
      valueAtBorderExport: 2000,
      valueAtBorderExportCurrency: 'EUR',
      amountInvoiced: 1000,
      amountInvoicedCurrency: 'EUR',
      sequenceNumber: 1,
      descriptionOfGoods: 'Biegeleiste',
      grossMass: '2100',
      netMass: '2000',
      numberOfPackages: 100,
      harmonizedSystemSubheadingCode: '0',
    },
    {
      documents: [],
      consigneeOrderNumber: '1234',
      containerId: '12',
      countryOfOrigin: 'DE',
      localClassificationCode: '0',
      typeOfPackages: 'RTL',
      marksNumbers: undefined,
      dangerousGoodsCode: -1,
      valueAtBorderExport: 2000,
      valueAtBorderExportCurrency: 'EUR',
      amountInvoiced: 1000,
      amountInvoicedCurrency: 'EUR',
      sequenceNumber: 1,
      descriptionOfGoods: 'Biegeleiste',
      grossMass: '2100',
      netMass: '2000',
      numberOfPackages: 100,
      harmonizedSystemSubheadingCode: '0',
    },
    {
      documents: [],
      consigneeOrderNumber: '1234',
      containerId: '12',
      countryOfOrigin: 'DE',
      localClassificationCode: '0',
      typeOfPackages: 'RTL',
      marksNumbers: undefined,
      dangerousGoodsCode: -1,
      valueAtBorderExport: 2000,
      valueAtBorderExportCurrency: 'EUR',
      amountInvoiced: 1000,
      amountInvoicedCurrency: 'EUR',
      sequenceNumber: 1,
      descriptionOfGoods: 'Biegeleiste',
      grossMass: '2100',
      netMass: '2000',
      numberOfPackages: 100,
      harmonizedSystemSubheadingCode: '0',
    },
  ],

  releaseDateAndTime: new Date().toDateString(),
  invoiceCurrency: 'EUR',
  events: [
    {
      creator: 'cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462y',
      status: '0',
      index: 'index',
      message: 'Created',
      eventType: 'EXPORT_RECORDED',
      timestamp: '2021-10-14T19:42:40Z',
    },
  ],
  eventType: '',
};

export const EXPORT_RESPONSE_MOCK: GetExportTokenResponseDTO = {
  exportToken: EXPORT_MOCK,
  events: EXPORT_MOCK.events as ExportEvent[],
};

export const HISTORY_RESPONSE_MOCK: FieldInformationDto[] = [
  {
    isEdited: true,
    lastEditor: 'TEST USER',
    fieldName: 'TEST',
    level: 'secure',
  },
];

export const EXPORT_TENDERMINT_TOKENLIST: ExportTokenListResponse = {
  tokens: [EXPORT_RESPONSE_MOCK],
};

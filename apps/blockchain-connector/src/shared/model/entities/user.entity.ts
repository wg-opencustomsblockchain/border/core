/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Company } from './company';
/**
 * User Entity object holding all data related to authentication and signing processes.
 */

export interface User {
  attributes?: {
    company?: [Company];
    mnemonic?: [string];
    pubkey?: [string];
    tokenWalletId?: [string];
    wallet?: [string];
    role?: [string];
  };
  id?: string;
  username?: string;
  email?: string;
  firstName?: string;
  lastName?: string;
  enabled?: boolean;
  emailVerified?: boolean;
  totp?: boolean;
  createdTimestamp?: number;
  mnemonic?: string;
  wallet?: string;
}

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Address } from './address';

export class Company {
  address: Address | undefined;
  customsId: string;
  name: string;
  subsidiaryNumber: string;
  identificationNumber: string;
}

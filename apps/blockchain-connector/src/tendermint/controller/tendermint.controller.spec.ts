/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { TendermintService } from '../service/tendermint.service';
import { TendermintModule } from '../tendermint.module';
import { TendermintController } from './tendermint.controller';

describe('TendermintController', () => {
  let controller: TendermintController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [TendermintModule, ConfigService],
      controllers: [TendermintController],
      providers: [TendermintService, ConfigService],
    }).compile();

    controller = module.get<TendermintController>(TendermintController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

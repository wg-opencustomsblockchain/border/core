/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Controller, Logger } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { QueryDto } from '../service/dto/query.dto';
import { TendermintService } from '../service/tendermint.service';

/**
 * Controller that accepts Messages via the Broker to query the shain directly.
 */
@Controller('tendermint')
export class TendermintController {
  logger = new Logger('Main');

  /** Creates a Tendermint Controller and injects a tendermint Service */
  constructor(private readonly appService: TendermintService) {}

  /**
   * Generic Endpoint to forward reading requests to the chain.
   * @param query - The Query to be resolved.
   * @returns - The Query Result from the Blockchain.
   */
  /*  @MessagePattern('query')
  async query(query: QueryDto) {
    return this.appService.query(query.nodeUri, query.chainId, query.path);
  }*/
}

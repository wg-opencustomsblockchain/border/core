/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Test, TestingModule } from '@nestjs/testing';
import { TendermintService } from './tendermint.service';

describe('TendermintService', () => {
  let service: TendermintService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [],
      providers: [TendermintService],
    }).compile();

    service = module.get<TendermintService>(TendermintService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

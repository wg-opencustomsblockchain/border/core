/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Coin } from '@cosmjs/launchpad';
import { DirectSecp256k1HdWallet, EncodeObject, Registry } from '@cosmjs/proto-signing';
import { DeliverTxResponse } from '@cosmjs/stargate';

import { Injectable, Logger } from '@nestjs/common';

import { Client } from '@border/api-interfaces/lib/client';
import { TxBody } from '@border/api-interfaces/lib/client/cosmos.tx.v1beta1';
import {
  MsgCreateExportEventResponse,
  MsgCreateExportTokenResponse,
  MsgCreateImportTokenResponse,
  MsgFetchExportTokenAllResponse,
  MsgFetchExportTokenResponse,
  MsgFetchImportTokenAllResponse,
  MsgFetchImportTokenResponse,
  MsgFetchTokenHistoryResponse,
  MsgUpdateExportTokenResponse,
  MsgFetchExportTokenDriverResponse,
  MsgFetchExportTokenAllDriverResponse,
  MsgFetchExportTokenImporterResponse,
  MsgFetchExportTokenAllImporterResponse,
} from '@border/api-interfaces/lib/client/org.borderblockchain.businesslogic/types/businesslogic/tx';
import { MsgUpdateImportTokenResponse } from '@border/api-interfaces/lib/client/org.borderblockchain.importtoken';

/**
 * Workhorse class for all Chain interactions of this application.
 * Handles both the details of writing and reading to the chain.
 */
@Injectable()
export class TendermintService {
  /** A standard coin object - Can be set to 0 as we do not use any monetary token systems.  */
  stdCoins: Coin[] = [{ denom: 'token', amount: '0' }];
  /** How much gas a transaction may use - Set to big enough value to accept all transactions.  */
  stdGas: string = process.env.TENDERMINT_MAX_GAS;
  /** Memo that is attached to a transaction  */
  stdMemo: 'Sent From Border Backend';
  /** The RPC endpoint to query/send transactions to. */
  blockchainRPCEndpoint = process.env.TENDERMINT_CHAIN_ENDPOINT_RPC;
  /** Type URL corresponsing to the Export Module int he chain. */
  exportTokenType = process.env.BLOCKCHAIN_EXPORT_TYPE_URL;
  /** Type URL corresponsing to the Import Module int he chain. */
  importTokenType = process.env.BLOCKCHAIN_IMPORT_TYPE_URL;
  /** Type URL corresponsing to the Import Module int he chain. */
  businesslogic = process.env.BLOCKCHAIN_BUSINESS_TYPE_URL;
  /** CosmosWallet Address to sign with. */
  wallet: string;
  /** Cosmos Mnemonic to use for signing. */
  mnemonic: string;
  /** Registry to decode Blockchain responses. */
  decodeRegistry: any;
  //** Instantiate a NestJS Logger */
  private readonly logger = new Logger(TendermintService.name);
  /** PART OF THE HOTFIX FOR VERSION 0.24 */
  private maxTimeout = 10;
  private timeoutMs = 1000;

  /** Creates a new Tendermint Service instance. Injects a Http Service. */
  constructor() {
    const responseTypes = [
      [`/org.borderblockchain.businesslogic.MsgCreateEvent`, MsgCreateExportEventResponse],
      [`/org.borderblockchain.businesslogic.MsgCreateExportEvent`, MsgCreateExportEventResponse],
      [`/org.borderblockchain.businesslogic.MsgCreateExportToken`, MsgCreateExportTokenResponse],
      [`/org.borderblockchain.businesslogic.MsgUpdateExportToken`, MsgUpdateExportTokenResponse],
      [`/org.borderblockchain.businesslogic.MsgCreateImportToken`, MsgCreateImportTokenResponse],
      [`/org.borderblockchain.businesslogic.MsgUpdateImportToken`, MsgUpdateImportTokenResponse],

      [`/org.borderblockchain.businesslogic.MsgFetchExportTokenExporter`, MsgFetchExportTokenResponse],
      [`/org.borderblockchain.businesslogic.MsgFetchExportTokenImporter`, MsgFetchExportTokenResponse],
      [`/org.borderblockchain.businesslogic.MsgFetchExportTokenDriver`, MsgFetchExportTokenResponse],

      [`/org.borderblockchain.businesslogic.MsgFetchExportTokenAllExporter`, MsgFetchExportTokenAllResponse],
      [`/org.borderblockchain.businesslogic.MsgFetchExportTokenAllDriver`, MsgFetchExportTokenAllResponse],
      [`/org.borderblockchain.businesslogic.MsgFetchExportTokenAllImporter`, MsgFetchExportTokenAllResponse],
      [`/org.borderblockchain.businesslogic.MsgFetchExportTokenExporterByExportId`, MsgFetchExportTokenResponse],
      [`/org.borderblockchain.businesslogic.MsgFetchExportTokenDriverByExportId`, MsgFetchExportTokenResponse],
      [`/org.borderblockchain.businesslogic.MsgFetchImportTokenAll`, MsgFetchImportTokenAllResponse],
      [`/org.borderblockchain.businesslogic.MsgFetchImportTokenByImportId`, MsgFetchImportTokenResponse],

      [`/org.borderblockchain.businesslogic.MsgFetchImportToken`, MsgFetchImportTokenResponse],

      [`/org.borderblockchain.businesslogic.MsgFetchImportTokenAll`, MsgFetchImportTokenAllResponse],
      [`/org.borderblockchain.businesslogic.MsgFetchExportTokenImporterByExportId`, MsgFetchExportTokenResponse],
      [`/org.borderblockchain.businesslogic.MsgFetchExportTokenAllExporter`, MsgFetchExportTokenAllResponse],
      [`/org.borderblockchain.businesslogic.MsgFetchTokenHistory`, MsgFetchTokenHistoryResponse],

      [`/org.borderblockchain.businesslogic.MsgFetchExportTokenDriver`, MsgFetchExportTokenDriverResponse],
      [`/org.borderblockchain.businesslogic.MsgFetchExportTokenAllDriver`, MsgFetchExportTokenAllDriverResponse],
      [`/org.borderblockchain.businesslogic.MsgFetchExportTokenImporter`, MsgFetchExportTokenImporterResponse],
      [`/org.borderblockchain.businesslogic.MsgFetchExportTokenAllImporter`, MsgFetchExportTokenAllImporterResponse],
    ];

    this.decodeRegistry = new Registry(<any>responseTypes);
  }

  /**
   * Prepare an incoming transaction into a Cosmos/Tendermint message format.
   * @param wallet The Wallet address used for signing.
   * @param mnemonic The private Key used to sign with.
   * @param txInput The transaction input to sign.
   * @param type The type of transaction Msg. For example: "MsgCreateExportToken"
   * @param typeUrl The URL under which the API is served. For example: "/org.borderblockchain.importtoken"
   * @returns
   */
  async buildSignAndBroadcast(mnemonic: string, txInput: any[], type: string): Promise<any> {
    const blockchainRPCEndpoint = `${process.env.TENDERMINT_CHAIN_ENDPOINT_RPC}`;
    const blockchainApiEndpoint = `${process.env.TENDERMINT_CHAIN_ENDPOINT_NODE}`;
    let response: DeliverTxResponse;
    const messages: EncodeObject[] = [];
    const walletAddress = await this.getCosmosAddress(mnemonic);

    const wallet = await DirectSecp256k1HdWallet.fromMnemonic(mnemonic);

    const client = new Client(
      {
        apiURL: blockchainApiEndpoint,
        rpcURL: blockchainRPCEndpoint,
        prefix: 'cosmos',
      },
      wallet
    );

    for (const tx of txInput) {
      tx.creator = walletAddress;
      const encodeObject: EncodeObject = { typeUrl: '/org.borderblockchain.businesslogic.' + type, value: tx };
      messages.push(encodeObject);
    }

    // Retry sending the message and handle errors.
    for (let i = 0; i < this.maxTimeout; i++) {
      try {
        Logger.debug('Transaction input:' + JSON.stringify(messages));
        response = await client.signAndBroadcast(messages, { amount: this.stdCoins, gas: this.stdGas }, this.stdMemo);

        Logger.debug('Transaction response' + JSON.stringify(response));
        const value = await client.CosmosTxV1Beta1.query.serviceGetTx(response.transactionHash);
        const hexString = value.data.tx_response.data;
        const arrayBuffer = Uint8Array.from(hexString.match(/.{1,2}/g).map((byte) => parseInt(byte, 16)));
        const decodedResult: TxBody = this.decodeRegistry.decodeTxBody(arrayBuffer);
        Logger.verbose('Transaction decoded' + JSON.stringify(decodedResult).substring(0, 500));

        if (await this.checkTxResult(response)) {
          try {
            return decodedResult.messages[0];
          } catch (e) {
            this.logger.warn(e);
            this.logger.warn('Transaction OK, But nothing to decode.');
            return null;
          }
        }
      } catch (e) {
        this.logger.error(e);
        await this.checkTxResult(e.code);
      }
    }
    throw new Error('Unexpected Error');
  }

  /**
   * Rudimentary Error Handling method based on Chain Responses.
   * Includes a Bandaid fix in case we send two messages from the same wallet at the same time.
   * @param code: The Error code
   * @returns
   */
  public async checkTxResult(response: DeliverTxResponse): Promise<boolean> {
    switch (response.code) {
      case 0: {
        return true;
      }
      case 1: {
        Logger.error(response);
        throw new Error('Misc Error: ' + response);
      }
      case 32: {
        this.logger.warn('Sequence missmatch: Retry in ' + this.timeoutMs + ' Seconds');
        await this.delay(this.timeoutMs);
        return false;
      }
      case -32603: {
        this.logger.warn('Tx already in Cache: Retry in ' + this.timeoutMs + ' Seconds');
        await this.delay(this.timeoutMs);
        return false;
      }
      // Authorization Errors
      case 4: {
        this.logger.warn('Authentication Error. Code: ' + response.code);
        throw new Error('Auth Error');
      }
      // Illegal Status Transaction
      case 18: {
        this.logger.warn('Attempted to perform an unauthorized transaction. Code: ' + response.code);
        throw new Error('Auth Transaction');
      }
      case 7: {
        this.logger.warn('Attempted to perform an transaction with an invalid address. Code: ' + response.code);
        throw new Error('Invalid Address');
      }
    }

    return true;
  }

  //** PART OF THE HOTFIX FOR VERSION 0.24 */
  async delay(ms: number) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  private async getCosmosAddress(mnemonic: string): Promise<string> {
    const [creatorAddress] = await (await DirectSecp256k1HdWallet.fromMnemonic(mnemonic, undefined)).getAccounts();

    return creatorAddress.address;
  }
}

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * Collection of the Borderblockchain API Information.
 * TENDERMINT_TYPE_URL: The base URL under which the API is served.
 * MODULE: Name of the corresponding Module in the Chain GO Code.
 * ENDPOINTS: The available Endpoints for the Module
 */

export enum TxMessages {
  MsgCreateExportToken = 'MsgCreateExportToken',
  MsgUpdateExportToken = 'MsgUpdateExportToken',
  MsgCreateExportEvent = 'MsgCreateExportEvent',
  MsgFetchExportTokenAllExporter = 'MsgFetchExportTokenAllExporter',
  MsgFetchExportTokenAllDriver = 'MsgFetchExportTokenAllDriver',
  MsgFetchExportTokenAllImporter = 'MsgFetchExportTokenAllImporter',
  MsgFetchImportTokenAll = 'MsgFetchImportTokenAll',
  MsgFetchExportTokenAllResponse = 'MsgFetchExportTokenAllResponse',
  MsgFetchImportTokenAllResponse = 'MsgFetchImportTokenAllResponse',

  MsgFetchExportTokenExporter = 'MsgFetchExportTokenExporter',
  MsgFetchExportTokenDriver = 'MsgFetchExportTokenDriver',
  MsgFetchExportTokenImporter = 'MsgFetchExportTokenImporter',
  MsgFetchExportTokenDriverByExportId = 'MsgFetchExportTokenDriverByExportId',
  MsgFetchExportTokenExporterByExportId = 'MsgFetchExportTokenExporterByExportId',
  MsgFetchExportTokenImporterByExportId = 'MsgFetchExportTokenImporterByExportId',
  MsgFetchExportTokenResponse = 'MsgFetchExportTokenResponse',
  MsgFetchImportTokenResponse = 'MsgFetchImportTokenResponse',
  MsgFetchImportTokenByImportId = 'MsgFetchImportTokenByImportId',
  MsgFetchImportToken = 'MsgFetchImportToken',
  MsgFetchTokenHistory = 'MsgFetchTokenHistory',
}

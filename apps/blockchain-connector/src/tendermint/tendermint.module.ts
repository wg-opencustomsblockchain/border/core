/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { TendermintController } from './controller/tendermint.controller';
import { TendermintService } from './service/tendermint.service';

/**
 * This module handles all tasks related to chain communication.
 */
@Module({
  imports: [ConfigService],
  providers: [TendermintService],
  controllers: [TendermintController],
})
export class TendermintModule {}

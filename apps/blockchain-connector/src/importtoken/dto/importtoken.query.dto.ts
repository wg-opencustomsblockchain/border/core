/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ImportEvent } from '@border/api-interfaces/lib/client/org.borderblockchain.importtoken';
import { ImportTokenDTO } from './import.dto';

/** DTO Objects to consume Blockchain responses.*/
export class ImportTokenListResponse {
  /** List of Blockchain Responses which hold both a Token and a History each. */
  tokens: GetImportTokenResponseDTO[];
}

/** DTO Object to consume Blockchain response.*/
export class GetImportTokenResponseDTO {
  /** The token without any process info. */
  importToken: ImportTokenDTO;
  /** List of process updates. Saved separately in the Blockchain. */
  events: ImportEvent[];
}

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ImportEvent, ImportToken } from '@border/api-interfaces/lib/client/org.borderblockchain.importtoken';

/**
 * @Interface for accepting incoming token writing requests.
 * Extends the Blockchain Data Model generated Protofile "ExportToken".
 */
export interface ImportSaveDTO extends ImportToken {
  /** Status as defined in the Import Process Enum. */
  status: string;
}
/** DTO Object to consume Blockchain response.*/
export interface ImportTokenDTO extends ImportToken {
  /**Status as defined in the Export Process Enum. */
  status: string;
  /** List of process updates. */
  events: ImportEvent[];
  /** The timestamp of the last update. */
  timestamp: string;
}

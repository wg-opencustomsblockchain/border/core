/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { EventDto } from './event.dto';
import { ImportSaveDTO } from './import.dto';

/** Ead DTO Definitions with Swagger API Properties. */
export class TokenQueryDto {
  wallet: string;
  mnemonic: string;
  query: string;
  tokenWalletId: string;

  constructor(wallet: string, mnemonic: string, query: string, tokenWalletId: string) {
    this.query = query;
    this.wallet = wallet;
    this.mnemonic = mnemonic;
    this.tokenWalletId = tokenWalletId;
  }
}

/** Ead DTO Definitions with Swagger API Properties. */
export class TokenTransactionDto {
  dto: ImportSaveDTO[];
  mnemonic: string;
  tokenWalletId: string;

  constructor(dto: ImportSaveDTO[], mnemonic: string, tokenWalletId: string) {
    this.dto = dto;
    this.mnemonic = mnemonic;
    this.tokenWalletId = tokenWalletId;
  }
}

export class EventTokenTransactionDto {
  dto: EventDto[];
  mnemonic: string;
  tokenWalletId: string;

  constructor(dto: EventDto[], mnemonic: string, tokenWalletId: string) {
    this.dto = dto;
    this.mnemonic = mnemonic;
    this.tokenWalletId = tokenWalletId;
  }
}

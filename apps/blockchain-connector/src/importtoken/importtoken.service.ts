/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {
  MsgFetchImportToken,
  MsgFetchImportTokenAll,
  MsgFetchImportTokenByImportId,
} from '@border/api-interfaces/lib/client/org.borderblockchain.businesslogic/module';
import {
  ImportEvent,
  ImportToken,
} from '@border/api-interfaces/lib/client/org.borderblockchain.importtoken';
import { Inject, Injectable, NotFoundException } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { AmqpClientEnum, HistoryMqPattern } from '@border/api-interfaces';

import * as MSG from '../importtoken/enum/txmessages.json';

import { TendermintService } from '../tendermint/service/tendermint.service';
import { TxMessages } from '../tendermint/service/tx-messages';
import { SystemMessage } from './../messages';
import { ImportTokenDTO } from './dto/import.dto';
import {
  GetImportTokenResponseDTO,
  ImportTokenListResponse,
} from './dto/importtoken.query.dto';
import { TokenQueryDto, TokenTransactionDto } from './dto/transaction.dto';

/**
 * Service class to pass requests to the corresponding chain endpoints.
 * Our Blockchain API delivers a full event history alongside each token to minimize API calls.
 */

@Injectable()
export class ImportTokenService {
  /** Creates a Service instance. Injects a Tendermint Service. */
  constructor(
    private tendermintService: TendermintService,
    @Inject(AmqpClientEnum.AMQP_CLIENT_HISTORY)
    private historyService: ClientProxy
  ) {}

  /**
   * Queries all Token and reads the current Timestamp + Event Status.
   * @returns A list of export token with current status, history and last timestamp.
   */
  public async findAll(dto: TokenQueryDto): Promise<ImportTokenDTO[]> {
    const msg: MsgFetchImportTokenAll = {
      creator: dto.wallet,
      timestamp: new Date().toString() + Math.random(),
      tokenWalletId: dto.tokenWalletId,
    };

    const tokens: ImportTokenListResponse =
      await this.tendermintService.buildSignAndBroadcast(
        dto.mnemonic,
        [msg],
        TxMessages.MsgFetchImportTokenAll
      );

    const result: ImportTokenDTO[] = [];

    if (tokens) {
      for (const token of tokens.tokens) {
        const entry: ImportTokenDTO = token.importToken;
        entry.events = token.events;
        const last: ImportEvent = token.events[token.events.length - 1];
        entry.timestamp = last.timestamp;
        entry.status = last.status;
        result.push(entry);
      }
    }
    return result;
  }

  /**
   * Finds a specific import token by its identifying token id.
   * @param dto Queries for the TokenId while using the identities from the dto.
   * @returns The token with current status, history and last timestamp.
   */
  public async findByImportId(dto: TokenQueryDto): Promise<ImportToken> {
    const msg: MsgFetchImportTokenByImportId = {
      creator: dto.wallet,
      importId: dto.query,
      timestamp: new Date().toString() + Math.random(),
      tokenWalletId: dto.tokenWalletId,
    };

    const token: GetImportTokenResponseDTO =
      await this.tendermintService.buildSignAndBroadcast(
        dto.mnemonic,
        [msg],
        TxMessages.MsgFetchImportTokenByImportId
      );

    if (!token) throw new NotFoundException(SystemMessage.ERROR_NOT_FOUND);
    const result: ImportTokenDTO = token.importToken;
    result.events = token.events;

    // Extract process info from event History.
    const last: ImportEvent = result.events[result.events.length - 1];
    result.timestamp = last.timestamp;
    result.status = last.status;
    return result;
  }

  /**
   * Finds a specific import token by its identifying process id.
   * @param dto Queries for the ProcessId while using the identities from the dto.
   * @returns The token with current status, history and last timestamp.
   */
  public async findOne(dto: TokenQueryDto): Promise<ImportToken> {
    const msg: MsgFetchImportToken = {
      creator: dto.wallet,
      id: dto.query,
      timestamp: new Date().toString() + Math.random(),
      tokenWalletId: dto.tokenWalletId,
    };

    const token: GetImportTokenResponseDTO =
      await this.tendermintService.buildSignAndBroadcast(
        dto.mnemonic,
        [msg],
        TxMessages.MsgFetchImportToken
      );

    if (!token) throw new NotFoundException(SystemMessage.ERROR_NOT_FOUND);
    const result: ImportTokenDTO = token.importToken;
    result.events = token.events;

    // Extract process info from event History.
    const last: ImportEvent = result.events[result.events.length - 1];
    result.timestamp = last.timestamp;
    result.status = last.status;
    return result;
  }

  /**
   * Returns all import token with which this Address has interacted.
   * @param dto Queries for the Wallet of a user while using the identities from the dto.
   * @returns A list of import token with current status, history and last timestamp.
   */
  public async filterByUser(dto: TokenQueryDto): Promise<ImportTokenDTO[]> {
    const tokens: ImportTokenDTO[] = await this.findAll(dto);
    const result: ImportTokenDTO[] = [];
    for (const token of tokens) {
      if (token.creator == dto.query) result.push(token);
    }
    if (result.length == 0)
      throw new NotFoundException(SystemMessage.ERROR_NOT_FOUND);
    return result;
  }

  // --- Transactions ---
  /**
   * Writes an import to the chain and creates a token.
   * @param msg DTO to be saved.
   * @returns The new token that has been written to the chain.
   */
  public async createOne(msg: TokenTransactionDto): Promise<ImportToken> {
    const result = await this.tendermintService.buildSignAndBroadcast(
      msg.mnemonic,
      msg.dto,
      MSG.MsgCreateImportToken
    );

    this.historyService
      .send(HistoryMqPattern.PREFIX + HistoryMqPattern.CREATE_HISTORY_IMPORT, {
        token: result['importToken'],
      })
      .subscribe();

    return result;
  }

  /**
   * Updates the token on the Chain.
   * @param msg DTO used for the update.
   * @returns The new Token with updated information.
   */

  public async updateImportToken(
    msg: TokenTransactionDto
  ): Promise<ImportToken> {
    const result = await this.tendermintService.buildSignAndBroadcast(
      msg.mnemonic,
      msg.dto,
      MSG.MsgUpdateImportToken
    );

    this.historyService
      .send(
        HistoryMqPattern.PREFIX + HistoryMqPattern.GENERATE_HISTORY_IMPORT,
        { token: result['importToken'] }
      )
      .subscribe();

    return result;
  }
}

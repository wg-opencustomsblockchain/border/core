/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * Available Messaging Queue endpoints for this Module.
 */
export enum ImportTokenEndpoints {
  MQ_PREFIX = 'v2/',
  MQ_TOKEN_LIST = 'blockchain/import/list',
  MQ_TOKEN_CREATE = 'blockchain/import/create',
  MQ_TOKEN_UPDATE = 'blockchain/import/update',
  MQ_TOKEN_FIND = 'blockchain/import/find/id',
  MQ_TOKEN_FIND_IMPORT_ID = 'blockchain/import/find/importid',
  MQ_TOKEN_FIND_USER = 'blockchain/import/find/user',
}

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * Import Stages as per process definition.
 * Used to identify which actions are allowed at what stage in the process chain.
 */

export enum ImportStatus {
  // Import proposal
  PROPOSAL = '0',
  // Advance information accepted
  ACCEPTED = '1',
  // Import data set completed
  COMPLETED = '2',
  // Import data set exported
  EXPORTED = '3',
  // Import declaration submitted
  SUBMITTED = '4',
  // Release of goods for free circulation
  RELEASE_GOODS = '5',
  // Archived
  // ARCHIVED = '6',
}

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export enum ImportTokenTypes {
  TENDERMINT_TYPE_URL_BUSINESSLOGIC = '/org.borderblockchain.businesslogic',
  TENDERMINT_TYPE_URL = '/org.borderblockchain.importtoken',
  IMPORT_MODULE = 'importtoken',
  IMPORT_TOKEN = 'token',
  IMPORT_HISTORY = 'event',
  IMPORT_MAPPING = 'mapping',
}

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { DynamicModule, Module } from '@nestjs/common';
import { TendermintService } from '../tendermint/service/tendermint.service';

import { ConfigService } from '@nestjs/config';
import { ImportTokenController } from './importtoken.controller';
import { ImportTokenService } from './importtoken.service';
import { MQBroker } from '@border/broker';

const mqBroker: DynamicModule = new MQBroker().getMsgBroker();
/** Module definitions for all modules relating to exporttoken functionalities. */
@Module({
  imports: [mqBroker],
  providers: [ImportTokenService, TendermintService, ConfigService],
  controllers: [ImportTokenController],
})
export class ImportTokenModule {}

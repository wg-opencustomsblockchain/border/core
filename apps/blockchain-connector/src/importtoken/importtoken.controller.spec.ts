/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ImportToken } from '@border/api-interfaces/lib/client/org.borderblockchain.importtoken';
import { DynamicModule } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

import { MQBroker } from '@border/api-interfaces/lib/broker';
import { ClientProxy } from '@nestjs/microservices';
import { Test, TestingModule } from '@nestjs/testing';
import { of } from 'rxjs';
import { User } from '../shared/model/entities/user.entity';

import { TendermintService } from '../tendermint/service/tendermint.service';
import { ImportSaveDTO, ImportTokenDTO } from './dto/import.dto';
import { TokenQueryDto, TokenTransactionDto } from './dto/transaction.dto';
import { ImportTokenController } from './importtoken.controller';
import { ImportTokenService } from './importtoken.service';

describe('ImportTokenController', () => {
  let controller: ImportTokenController;
  let importTokenService: ImportTokenService;
  let clientProxy;

  const imports: ImportTokenDTO[] = [];

  process.env.TENDERMINT_CHAIN_ENDPOINT_RPC = '-1';
  process.env.TENDERMINT_CHAIN_ENDPOINT_NODE = '-1';
  process.env.TENDERMINT_CHAIN_ID = '-1';
  process.env.WALLET_ADDRESS = '-1';
  process.env.WALLET_ADDRESS_MNEMONIC = '-1';
  process.env.WALLET_ADDRESS_PREFIX = '-1';
  process.env.TENDERMINT_TYPE_URL_EXPORT = '-1';

  const user: User = {
    id: '1',
    username: 'Ina',
    mnemonic: '...',
  };

  const mockDto: TokenQueryDto = {
    wallet: 'MOCK',
    mnemonic:
      'link wild parent false local room leaf people couch drift yard heavy trap coral husband scorpion paper donor retire enrich visual donkey birth involve',
    query: 'MOCK',
    tokenWalletId: 'INA_WALLET',
  };

  const mqBroker: DynamicModule = new MQBroker().getMsgBroker();
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [mqBroker],

      controllers: [ImportTokenController],
      providers: [
        ImportTokenService,
        ConfigService,
        TendermintService,
        {
          provide: 'broker',
          useValue: {
            send(pattern: any, data: string): any {
              return of(user);
            },
          },
        },
      ],
    }).compile();

    controller = module.get<ImportTokenController>(ImportTokenController);
    importTokenService = module.get<ImportTokenService>(ImportTokenService);
    clientProxy = module.get<ClientProxy>('broker');
  });

  describe('listAllImports', () => {
    it('should be listed', async () => {
      jest
        .spyOn(importTokenService, 'findAll')
        .mockReturnValue(Promise.resolve(imports));
      expect(await controller.listImport(mockDto)).toEqual(imports);
    });
  });

  describe('createImport', () => {
    it('should be created', async () => {
      const tokenRequest: ImportSaveDTO = null;
      const tokenResponse: ImportToken = null;
      jest
        .spyOn(importTokenService, 'createOne')
        .mockReturnValue(Promise.resolve(tokenResponse));
      expect(
        await controller.createImport(
          new TokenTransactionDto([tokenRequest], 'MOCK', 'TEST')
        )
      ).toEqual(tokenResponse);
    });
  });

  describe('findImportById', () => {
    it('should be found', async () => {
      const tokenResponse: ImportToken = null;
      mockDto.query = 'importId';
      jest
        .spyOn(importTokenService, 'findOne')
        .mockReturnValue(Promise.resolve(tokenResponse));
      expect(await controller.findImportById(mockDto)).toEqual(tokenResponse);
    });
  });

  describe('findImportByImportId', () => {
    it('should be found', async () => {
      const tokenResponse: ImportToken = null;
      mockDto.query = 'importId';
      jest
        .spyOn(importTokenService, 'findByImportId')
        .mockReturnValue(Promise.resolve(tokenResponse));
      expect(await controller.findImportByImportId(mockDto)).toEqual(
        tokenResponse
      );
    });
  });

  describe('updateImport', () => {
    it('should be updated', async () => {
      const tokenRequest: ImportSaveDTO = null;
      const tokenResponse: ImportToken = null;
      jest
        .spyOn(importTokenService, 'updateImportToken')
        .mockReturnValue(Promise.resolve(tokenResponse));
      expect(
        await controller.updateImport(
          new TokenTransactionDto([tokenRequest], 'MOCK', 'TEST')
        )
      ).toEqual(tokenResponse);
    });
  });
});

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ImportToken } from '@border/api-interfaces/lib/client/org.borderblockchain.importtoken';
import { ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { User } from '../shared/model/entities/user.entity';
import {
  IMPORT_MOCK,
  IMPORT_MOCK_RESPONSE,
} from '../shared/testfiles/testImportToken';

import { ClientsModule, Transport } from '@nestjs/microservices';
import { TendermintService } from '../tendermint/service/tendermint.service';
import { EventDto } from './dto/event.dto';
import { ImportTokenListResponse } from './dto/importtoken.query.dto';
import {
  EventTokenTransactionDto,
  TokenQueryDto,
  TokenTransactionDto,
} from './dto/transaction.dto';
import { ImportTokenService } from './importtoken.service';

describe('ImportTokenService', () => {
  let service: ImportTokenService;
  let blockchain: TendermintService;
  const mockUser: User = {};
  const mockDto: TokenQueryDto = {
    wallet: IMPORT_MOCK.creator,
    mnemonic:
      'link wild parent false local room leaf people couch drift yard heavy trap coral husband scorpion paper donor retire enrich visual donkey birth involve',
    query: 'MOCK',
    tokenWalletId: 'INA_WALLET',
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigService,
        ClientsModule.register([
          {
            name: 'HISTORY_SERVICE',
            transport: Transport.RMQ,
            options: {
              urls: [process.env.MSQ_QUEUE_HISTORY],
              queue: 'border_history',
              queueOptions: {
                durable: false,
              },
            },
          },
        ]),
      ],
      controllers: [ImportTokenService],
      providers: [TendermintService, ConfigService],
    }).compile();

    // Set the Process Environments. This could be used in the future for an actual testing environment.
    process.env.TENDERMINT_CHAIN_ENDPOINT_RPC = '-1';
    process.env.TENDERMINT_CHAIN_ENDPOINT_NODE = '-1';
    process.env.TENDERMINT_CHAIN_ID = '-1';
    process.env.WALLET_ADDRESS = IMPORT_MOCK.creator;
    process.env.WALLET_ADDRESS_MNEMONIC =
      'link wild parent false local room leaf people couch drift yard heavy trap coral husband scorpion paper donor retire enrich visual donkey birth involve';
    process.env.WALLET_ADDRESS_PREFIX = '-1';
    process.env.TENDERMINT_TYPE_URL_EXPORT = '-1';

    service = module.get<ImportTokenService>(ImportTokenService);
    blockchain = module.get<TendermintService>(TendermintService);

    mockUser.mnemonic = 'MOCK';
    mockUser.wallet = 'MOCK';
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findAll', () => {
    it('should find all', async () => {
      const response: ImportTokenListResponse = {
        tokens: [
          {
            importToken: IMPORT_MOCK,
            events: IMPORT_MOCK.events,
          },
        ],
      };
      jest
        .spyOn(blockchain, 'buildSignAndBroadcast')
        .mockReturnValue(Promise.resolve(response));
      expect(await service.findAll(mockDto)).toHaveLength(1);
    });
  });

  describe('findOne', () => {
    it('should find one', async () => {
      mockDto.query = 'importId';
      jest
        .spyOn(blockchain, 'buildSignAndBroadcast')
        .mockReturnValue(Promise.resolve(IMPORT_MOCK_RESPONSE));
      expect((await service.findOne(mockDto)).importId).toEqual(
        IMPORT_MOCK.importId
      );
    });
  });

  describe('findByImportId', () => {
    it('should find one', async () => {
      mockDto.query = 'importId';
      jest
        .spyOn(blockchain, 'buildSignAndBroadcast')
        .mockReturnValue(Promise.resolve(IMPORT_MOCK_RESPONSE));

      expect((await service.findByImportId(mockDto)).importId).toEqual(
        IMPORT_MOCK.importId
      );
    });
  });

  describe('filterByUser', () => {
    it('should filter by User Address', async () => {
      mockDto.query = 'cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462y';
      jest
        .spyOn(blockchain, 'buildSignAndBroadcast')
        .mockReturnValue(Promise.resolve(IMPORT_MOCK_RESPONSE));
      jest
        .spyOn(service, 'findAll')
        .mockReturnValue(Promise.resolve([IMPORT_MOCK]));
      expect(await service.filterByUser(mockDto)).toEqual([IMPORT_MOCK]);
    });
  });

  describe('createOne', () => {
    it('should create one', async () => {
      jest
        .spyOn(blockchain, 'buildSignAndBroadcast')
        .mockReturnValue(Promise.resolve(IMPORT_MOCK));
      jest
        .spyOn(service, 'findByImportId')
        .mockReturnValue(Promise.resolve(IMPORT_MOCK));
      const result: ImportToken = await service.createOne(
        new TokenTransactionDto([IMPORT_MOCK], 'MOCK', 'TEST')
      );
      expect(result.importId).toEqual(IMPORT_MOCK.importId);
    });
  });

  describe('updateImportToken', () => {
    it('should update an Import Token', async () => {
      jest
        .spyOn(blockchain, 'buildSignAndBroadcast')
        .mockReturnValue(Promise.resolve(IMPORT_MOCK));
      jest
        .spyOn(service, 'findByImportId')
        .mockReturnValue(Promise.resolve(IMPORT_MOCK));
      const result: ImportToken = await service.updateImportToken(
        new TokenTransactionDto([IMPORT_MOCK], 'MOCK', 'TEST')
      );
      expect(result).toEqual(IMPORT_MOCK);
    });
  });

  describe('createTokenTransactionDtos', () => {
    it('should properly create Transaction DTOs', () => {
      const testTrans: TokenTransactionDto = new TokenTransactionDto(
        [IMPORT_MOCK],
        'test',
        'test'
      );
      const testQuery: TokenQueryDto = new TokenQueryDto(
        'test',
        'test',
        'test',
        'test'
      );
      const testEvent: EventTokenTransactionDto = new EventTokenTransactionDto(
        [new EventDto()],
        'test',
        'test'
      );
      expect(testTrans).toBeDefined();
      expect(testQuery).toBeDefined();
      expect(testEvent).toBeDefined();
    });
  });
});

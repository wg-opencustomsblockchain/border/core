/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ImportToken } from '@border/api-interfaces/lib/client/org.borderblockchain.importtoken';
import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';

import { ImportTokenDTO } from './dto/import.dto';
import { TokenQueryDto, TokenTransactionDto } from './dto/transaction.dto';
import { ImportTokenEndpoints } from './enum/mq.patterns.import';
import { ImportTokenService } from './importtoken.service';

/**
 * Controller that accepts Messages via the MQ broker for the Token Module.
 * Passes requests to the corresponding Blockchain Service and returns Data
 * as DTO Objects per API Definition.
 */

@Controller()
export class ImportTokenController {
  constructor(private readonly importTokenService: ImportTokenService) {}

  /**
   * Writes a new Token to Blockchain. Uses the currently registered Wallet.
   * @param dto The new import to be tokenized.
   * @returns The new token that has been written to the chain.
   */
  @MessagePattern(ImportTokenEndpoints.MQ_PREFIX + ImportTokenEndpoints.MQ_TOKEN_CREATE)
  createImport(@Payload() dto: TokenTransactionDto): Promise<ImportToken> {
    return this.importTokenService.createOne(dto);
  }

  /**
   * Updates the import token on the chain.
   * @param dto The new information to be updated.
   * @returns The new token with updated information.
   */
  @MessagePattern(ImportTokenEndpoints.MQ_PREFIX + ImportTokenEndpoints.MQ_TOKEN_UPDATE)
  updateImport(@Payload() dto: TokenTransactionDto): Promise<ImportToken> {
    return this.importTokenService.updateImportToken(dto);
  }

  /**
   * Fetches a List of all saved Export token.
   * @returns A list of Import Token with current status, history and last timestamp.
   */
  @MessagePattern(ImportTokenEndpoints.MQ_PREFIX + ImportTokenEndpoints.MQ_TOKEN_LIST)
  listImport(@Payload() dto: TokenQueryDto): Promise<ImportTokenDTO[]> {
    return this.importTokenService.findAll(dto);
  }

  /**
   * Finds a specific import token by its token id.
   * @param id The token id.
   * @returns The token with current status, history and last timestamp.
   */
  @MessagePattern(ImportTokenEndpoints.MQ_PREFIX + ImportTokenEndpoints.MQ_TOKEN_FIND)
  findImportById(@Payload() dto: TokenQueryDto): Promise<ImportToken> {
    return this.importTokenService.findOne(dto);
  }

  /**
   * Finds a specific import token by its identifying import Id.
   * @param id The process ID which is mapped internally to a token ID.
   * @returns The token with current status, history and last timestamp.
   */
  @MessagePattern(ImportTokenEndpoints.MQ_PREFIX + ImportTokenEndpoints.MQ_TOKEN_FIND_IMPORT_ID)
  findImportByImportId(@Payload() dto: TokenQueryDto): Promise<ImportToken> {
    return this.importTokenService.findByImportId(dto);
  }

  /**
   * Finds all token which are owned by a wallet address.
   * @param user The wallet Address to be queried.
   * @returns A list of token with current status, history and last timestamp.
   * We are currently using a one user system in this Service as a place-holder until
   * our partner projects finishes work on an authorization Module.
   */

  @MessagePattern(ImportTokenEndpoints.MQ_PREFIX + ImportTokenEndpoints.MQ_TOKEN_FIND_USER)
  findUserImports(@Payload() dto: TokenQueryDto): Promise<ImportTokenDTO[]> {
    return this.importTokenService.filterByUser(dto);
  }
}

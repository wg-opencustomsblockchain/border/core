/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * Export Stages as per process definition.
 * Used to identify which actions are allowed at what stage in the process chain.
 */
export enum ExportStatus {
  // Status before recording on the Chain
  NEW = '0',
  // Recorded on blockchain
  RECORDED = '1',
  // Ready for pickup
  READY = '2',
  // Pickup complete
  PICKUP = '3',
  // Presentation of goods complete
  PRESENTED = '4',
  // Exit of goods certified
  EXIT = '5',
  // Archived
  ARCHIVED = '6',
}

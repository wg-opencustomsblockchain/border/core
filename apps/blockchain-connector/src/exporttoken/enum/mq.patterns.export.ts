/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * Available Messaging Queue Endpoints for this Module.
 */
export enum MQPatterns {
  PREFIX = 'v2/',
  TOKEN_LIST = 'blockchain/list',
  TOKEN_CREATE = 'blockchain/create',
  TOKEN_UPDATE = 'blockchain/update',
  TOKEN_FIND = 'blockchain/find/mrn',
  TOKEN_FIND_USER = 'blockchain/find/user',
  TOKEN_ARCHIVED = 'blockchain/archive',
  TOKEN_USER_MANAGEMENT_FIND_USER = 'user',
  EAD_FIND_IMPORT = 'blockchain/find/import',
  TOKEN_FETCH_HISTORY = 'blockchain/history',
}

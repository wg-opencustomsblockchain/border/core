/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * Collection of the Borderblockchain API Information.
 * TENDERMINT_TYPE_URL: The base URL under which the API is served.
 * MODULE: Name of the corresponding Module in the Chain GO Code.
 * ENDPOINTS: The available Endpoints for the Module
 */
export enum ExportTokenTypes {
  TENDERMINT_TYPE_URL = '/org.borderblockchain.exporttoken',
  TENDERMINT_TYPE_URL_BUSINESSLOGIC = '/org.borderblockchain.businesslogic',
  MODULE = 'exporttoken',
  ENDPOINT_TOKEN = 'token',
  ENDPOINT_EVENTS = 'event',
  ENDPOINT_MAPPING = 'mapping',
}

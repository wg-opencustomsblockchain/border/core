/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { UserRole } from '@border/api-interfaces';
import { EventDto } from '@border/api-interfaces/lib/dto/event.dto';

import { ExportSaveDTO } from './export.dto';

/** Ead DTO Definitions with Swagger API Properties. */
export class TokenQueryDto {
  wallet: string;
  mnemonic: string;
  query: any;
  tokenWalletId: string;
  role: UserRole;

  constructor(
    wallet: string,
    mnemonic: string,
    query: string,
    tokenWalletId: string,
    role: UserRole
  ) {
    this.query = query;
    this.wallet = wallet;
    this.mnemonic = mnemonic;
    this.tokenWalletId = tokenWalletId;
    this.role = role;
  }
}

/** Ead DTO Definitions with Swagger API Properties. */
export class TokenTransactionDto {
  dto: ExportSaveDTO[];
  mnemonic: string;
  tokenWalletId: string;
  role: UserRole;

  constructor(
    eadDto: ExportSaveDTO[],
    mnemonic: string,
    tokenWalletId: string,
    role: UserRole
  ) {
    this.dto = eadDto;
    this.mnemonic = mnemonic;
    this.tokenWalletId = tokenWalletId;
    this.role = role;
  }
}

export class ExportEventTokenTransactionDto {
  dto: EventDto[];
  mnemonic: string;
  tokenWalletId: string;

  constructor(dto: EventDto[], mnemonic: string, tokenWalletId: string) {
    this.dto = dto;
    this.mnemonic = mnemonic;
    this.tokenWalletId = tokenWalletId;
  }
}

export interface GetTokenHistoryDto {
  mnemonic: string;
  id: string;
}

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ExportEvent } from '@border/api-interfaces/lib/client/org.borderblockchain.exporttoken';
import { ExportTokenDTO } from './export.dto';

/** DTO Objects to consume Blockchain responses.*/
export class ExportTokenListResponse {
  /** List of Blockchain Responses which hold both a Token and a History each. */
  tokens: GetExportTokenResponseDTO[];
}

/** DTO Object to consume Blockchain response.*/
export class GetExportTokenResponseDTO {
  /** The token without any process info. */
  exportToken: ExportTokenDTO;
  /** List of process updates. Saved separately in the Blockchain. */
  events: ExportEvent[];
}

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Company } from '@border/api-interfaces/lib/client/org.borderblockchain.exporttoken';
import { ConfigService } from '@nestjs/config';
import { ClientProxy } from '@nestjs/microservices';
import { Test, TestingModule } from '@nestjs/testing';
import { of } from 'rxjs';
import { EXPORT_MOCK } from '../shared/testfiles/testExportToken';
import { USER_MOCK } from '../shared/testfiles/testUsers';

import { DynamicModule } from '@nestjs/common';
import { UserRole } from '@border/api-interfaces';
import { TendermintService } from '../tendermint/service/tendermint.service';
import { ExportSaveDTO, ExportTokenDTO } from './dto/export.dto';
import { TokenQueryDto, TokenTransactionDto } from './dto/transaction.dto';
import { ExportTokenController } from './exporttoken.controller';
import { ExportTokenService } from './exporttoken.service';
import { MQBroker } from '@border/broker';
describe('ExportTokenController', () => {
  let controller: ExportTokenController;
  let service: ExportTokenService;
  let clientProxy;
  const tokens: ExportTokenDTO[] = [];

  process.env.TENDERMINT_CHAIN_ENDPOINT_RPC = '-1';
  process.env.TENDERMINT_CHAIN_ENDPOINT_NODE = '-1';
  process.env.TENDERMINT_CHAIN_ID = '-1';
  process.env.WALLET_ADDRESS = '-1';
  process.env.WALLET_ADDRESS_MNEMONIC = '-1';
  process.env.WALLET_ADDRESS_PREFIX = '-1';
  process.env.TENDERMINT_TYPE_URL_EXPORT = '-1';

  const user = USER_MOCK;
  const mockDto: TokenQueryDto = {
    wallet: 'MOCK',
    mnemonic:
      'link wild parent false local room leaf people couch drift yard heavy trap coral husband scorpion paper donor retire enrich visual donkey birth involve',
    query: 'MOCK',
    tokenWalletId: 'ERIK_WALLET',
    role: UserRole.EXPORTER,
  };

  const mqBroker: DynamicModule = new MQBroker().getMsgBroker();
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [mqBroker],
      controllers: [ExportTokenController],
      providers: [
        ExportTokenService,
        ConfigService,
        TendermintService,
        {
          provide: 'broker',
          useValue: {
            send(pattern: any, data: string): any {
              return of(user);
            },
          },
        },
      ],
    }).compile();

    controller = module.get<ExportTokenController>(ExportTokenController);
    service = module.get<ExportTokenService>(ExportTokenService);
    clientProxy = module.get<ClientProxy>('broker');
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(service).toBeDefined();
  });

  describe('listAllToken', () => {
    it('should be listed', async () => {
      jest.spyOn(service, 'findAll').mockReturnValue(Promise.resolve(tokens));
      expect(await controller.listEAD(mockDto)).toEqual(tokens);
    });
  });

  describe('createEAD', () => {
    it('should be created', async () => {
      const tokenRequest: ExportSaveDTO = EXPORT_MOCK;
      tokenRequest.status = '0';
      const tokenResponse: ExportTokenDTO = EXPORT_MOCK;
      jest
        .spyOn(service, 'createExportToken')
        .mockReturnValue(Promise.resolve(tokenResponse));
      expect(
        await controller.createEAD(
          new TokenTransactionDto(
            [tokenRequest],
            'MOCK',
            'test',
            UserRole.EXPORTER
          )
        )
      ).toEqual(tokenResponse);
    });
  });

  describe('findEAD', () => {
    it('should be found', async () => {
      const tokenRequest: ExportSaveDTO = EXPORT_MOCK;
      mockDto.query = tokenRequest.exportId;
      jest
        .spyOn(service, 'findById')
        .mockReturnValue(Promise.resolve(EXPORT_MOCK));
      expect(await controller.findEAD(mockDto)).toEqual(EXPORT_MOCK);
    });
  });

  describe('updateEAD', () => {
    it('should be updated', async () => {
      const tokenRequest: ExportSaveDTO = EXPORT_MOCK;
      tokenRequest.status = '0';

      jest
        .spyOn(service, 'updateExportToken')
        .mockReturnValue(Promise.resolve(EXPORT_MOCK));
      expect(
        await controller.updateEAD(
          new TokenTransactionDto(
            [tokenRequest],
            'MOCK',
            'TEST',
            UserRole.EXPORTER
          )
        )
      ).toEqual(EXPORT_MOCK);
    });
  });

  describe('findDriverEad', () => {
    it('should be found', async () => {
      const tokenResponse: ExportTokenDTO = EXPORT_MOCK;
      jest
        .spyOn(service, 'filterByStatus')
        .mockReturnValue(Promise.resolve([tokenResponse]));
      mockDto.query = EXPORT_MOCK.creator;
      mockDto.wallet = EXPORT_MOCK.creator;
      expect(await controller.findDriverEad(mockDto)).toEqual([tokenResponse]);
    });
  });

  describe('findArchivedEADs', () => {
    it('should find EADS which are archived', async () => {
      const tokenResponse: ExportTokenDTO = EXPORT_MOCK;
      jest
        .spyOn(service, 'filterByStatus')
        .mockReturnValue(Promise.resolve([tokenResponse]));
      mockDto.query = EXPORT_MOCK.creator;
      mockDto.wallet = EXPORT_MOCK.creator;
      expect(await controller.findArchivedEADs(mockDto)).toEqual([
        tokenResponse,
      ]);
    });
  });

  describe('findImportEADs', () => {
    it('should be found', async () => {
      jest
        .spyOn(service, 'findAllAsImport')
        .mockReturnValue(Promise.resolve(tokens));
      jest.spyOn(clientProxy, 'send').mockReturnValue(user);
      const mockCompany: Company = {
        address: {
          countryCode: 'MOCK',
          streetAndNumber: 'MOCK',
          city: 'MOCK',
          postcode: 'MOCK',
        },
        name: 'MOCK',
        customsId: 'MOCK',
        subsidiaryNumber: 'MOCK',
        identificationNumber: 'MOCK',
      };

      mockDto.query = mockCompany;

      expect(await controller.findImportEADs(mockDto)).toEqual([]);
    });
  });
});

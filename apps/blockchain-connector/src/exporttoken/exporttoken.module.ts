/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { DynamicModule, Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { MQBroker } from '@border/api-interfaces/lib/broker';

import { TendermintService } from '../tendermint/service/tendermint.service';
import { ExportTokenController } from './exporttoken.controller';
import { ExportTokenService } from './exporttoken.service';

const mqBroker: DynamicModule = new MQBroker().getMsgBroker();
@Module({
  imports: [mqBroker],
  providers: [ExportTokenService, TendermintService, ConfigService],
  controllers: [ExportTokenController],
})
export class ExportTokenModule {}

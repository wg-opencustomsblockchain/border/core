/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Company } from '@border/api-interfaces/lib/client/org.borderblockchain.exporttoken';
import { Test, TestingModule } from '@nestjs/testing';
import { Address } from '../shared/model/entities/address';

import {
  EXPORT_MOCK,
  EXPORT_RESPONSE_MOCK,
  EXPORT_TENDERMINT_TOKENLIST,
  HISTORY_RESPONSE_MOCK,
} from '../shared/testfiles/testExportToken';
import { USER_MOCK } from '../shared/testfiles/testUsers';

import { AmqpClientEnum, UserRole } from '@border/api-interfaces';
import { MQBroker } from '@border/broker';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { TendermintService } from '../tendermint/service/tendermint.service';
import { ExportSaveDTO, ExportTokenDTO } from './dto/export.dto';
import { GetExportTokenResponseDTO } from './dto/exporttoken.query.dto';
import {
  TokenQueryDto,
  TokenTransactionDto,
  GetTokenHistoryDto,
} from './dto/transaction.dto';
import { ExportTokenModule } from './exporttoken.module';
import { ExportTokenService } from './exporttoken.service';
import { DynamicModule } from '@nestjs/common';

describe('ExportTokenService', () => {
  let service: ExportTokenService;
  let blockchain: TendermintService;
  const tokens: GetExportTokenResponseDTO[] = [];
  const mockDto: TokenQueryDto = {
    wallet: 'MOCK',
    mnemonic:
      'link wild parent false local room leaf people couch drift yard heavy trap coral husband scorpion paper donor retire enrich visual donkey birth involve',
    query: 'MOCK',
    tokenWalletId: 'ERIK_WALLET',
    role: UserRole.EXPORTER,
  };
  const mqBroker: DynamicModule = new MQBroker().getMsgBroker();
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [mqBroker],
      controllers: [ExportTokenService],
      providers: [TendermintService, ExportTokenModule],
    }).compile();

    // Set the Process Environments. This could be used in the future for an actual testing environment.
    process.env.TENDERMINT_CHAIN_ENDPOINT_RPC = '-1';
    process.env.TENDERMINT_CHAIN_ENDPOINT_NODE = '-1';
    process.env.TENDERMINT_CHAIN_ID = '-1';
    process.env.WALLET_ADDRESS = EXPORT_MOCK.creator;
    process.env.WALLET_ADDRESS_MNEMONIC =
      'link wild parent false local room leaf people couch drift yard heavy trap coral husband scorpion paper donor retire enrich visual donkey birth involve';
    process.env.WALLET_ADDRESS_PREFIX = '-1';
    process.env.TENDERMINT_TYPE_URL_EXPORT = '-1';

    service = module.get<ExportTokenService>(ExportTokenService);
    blockchain = module.get<TendermintService>(TendermintService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('createExportToken', () => {
    it('should createExportToken', async () => {
      jest
        .spyOn(blockchain, 'buildSignAndBroadcast')
        .mockReturnValue(Promise.resolve(EXPORT_RESPONSE_MOCK));
      const result = await service.createExportToken(
        new TokenTransactionDto(
          [EXPORT_MOCK],
          'MOCK',
          'TEST',
          UserRole.EXPORTER
        )
      );
      expect(result).toEqual(EXPORT_RESPONSE_MOCK);
    });
  });

  describe('findByMRN', () => {
    mockDto.query = EXPORT_MOCK.exportId;
    it('should find one by ID', async () => {
      jest
        .spyOn(blockchain, 'buildSignAndBroadcast')
        .mockReturnValue(Promise.resolve(EXPORT_RESPONSE_MOCK));
      expect(await service.findById(mockDto, UserRole.EXPORTER)).toEqual(
        EXPORT_MOCK
      );
    });
  });

  describe('getTokenHistory', () => {
    it('should find one by ID', async () => {
      const dto: GetTokenHistoryDto = {
        id: 'TEST',
        mnemonic: 'THIS IS A TEST MNEMONIC',
      };
      jest
        .spyOn(blockchain, 'buildSignAndBroadcast')
        .mockReturnValue(Promise.resolve(HISTORY_RESPONSE_MOCK));
      expect(await service.getTokenHistory(dto)).toEqual(HISTORY_RESPONSE_MOCK);
    });
  });

  describe('filterByStatus as Exporter', () => {
    it('should filter by status ', async () => {
      mockDto.wallet = EXPORT_MOCK.creator;
      mockDto.query = EXPORT_MOCK.creator;
      jest
        .spyOn(blockchain, 'buildSignAndBroadcast')
        .mockReturnValue(Promise.resolve(EXPORT_TENDERMINT_TOKENLIST));
      expect(
        (
          await service.filterByStatus(
            [EXPORT_RESPONSE_MOCK.events[0].status],
            mockDto,
            UserRole.EXPORTER
          )
        )[0].status
      ).toEqual(EXPORT_RESPONSE_MOCK.events[0].status);
    });
  });

  describe('filterByStatus as Driver', () => {
    it('should filter by status ', async () => {
      mockDto.wallet = EXPORT_MOCK.creator;
      mockDto.query = EXPORT_MOCK.creator;
      jest
        .spyOn(blockchain, 'buildSignAndBroadcast')
        .mockReturnValue(Promise.resolve(EXPORT_TENDERMINT_TOKENLIST));
      expect(
        (
          await service.filterByStatus(
            [EXPORT_RESPONSE_MOCK.events[0].status],
            mockDto,
            UserRole.DRIVER
          )
        )[0].status
      ).toEqual(EXPORT_RESPONSE_MOCK.events[0].status);
    });
  });

  describe('findAllAsImport', () => {
    it('should query only Token specifically viewable by the Import Role', async () => {
      mockDto.wallet = EXPORT_MOCK.creator;
      mockDto.query = EXPORT_MOCK.creator;
      const mockCompany: Company = JSON.parse(
        JSON.stringify(USER_MOCK.attributes.company[0])
      );
      Address;
      jest
        .spyOn(blockchain, 'buildSignAndBroadcast')
        .mockReturnValue(Promise.resolve(EXPORT_TENDERMINT_TOKENLIST));
      expect(
        (await service.findAllAsImport(mockDto, mockCompany))[0].status
      ).toEqual(EXPORT_RESPONSE_MOCK.events[0].status);
    });
  });

  describe('updateExportToken', () => {
    it('should update the export token with the given msg', async () => {
      jest.spyOn(service, 'findById').mockReturnValue(
        Promise.resolve({
          creator: 'cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462y',
          exportId: '123456',
          id: undefined,
        } as ExportTokenDTO)
      );
      const blockchainSpy = jest
        .spyOn(blockchain, 'buildSignAndBroadcast')
        .mockReturnValue(Promise.resolve(0 as unknown as any));

      await service.updateExportToken(
        new TokenTransactionDto(
          [{ exportId: '123456' } as ExportSaveDTO],
          'link wild parent false local room leaf people couch drift yard heavy trap coral husband scorpion paper donor retire enrich visual donkey birth involve',
          'TEST',
          UserRole.EXPORTER
        )
      );

      expect(blockchainSpy).toHaveBeenCalledWith(
        'link wild parent false local room leaf people couch drift yard heavy trap coral husband scorpion paper donor retire enrich visual donkey birth involve',
        [{ exportId: '123456', id: undefined }],
        'MsgUpdateExportToken'
      );
    });
  });

  describe('createTokenTransactionDtos', () => {
    it('should properly create Transaction DTOs', () => {
      const testTrans: TokenTransactionDto = new TokenTransactionDto(
        [EXPORT_MOCK],
        'test',
        'TEST',
        UserRole.EXPORTER
      );
      const testQuery: TokenQueryDto = new TokenQueryDto(
        'test',
        'test',
        'test',
        'test',
        UserRole.EXPORTER
      );
      expect(testTrans).toBeDefined();
      expect(testQuery).toBeDefined();
    });
  });
});

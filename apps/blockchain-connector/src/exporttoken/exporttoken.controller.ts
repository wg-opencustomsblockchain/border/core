/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Controller, Logger } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { ExportSaveDTO, ExportTokenDTO } from './dto/export.dto';
import {
  TokenQueryDto,
  TokenTransactionDto,
  GetTokenHistoryDto,
} from './dto/transaction.dto';
import { ExportStatus } from './enum/export-status';
import { MQPatterns } from './enum/mq.patterns.export';
import { ExportTokenService } from './exporttoken.service';

/**
 * Controller that accepts MQ from the broker for the Token Module.
 * Passes requests to the corresponding Blockchain Service and returns Data
 * as DTO Objects per API Definition.
 */
@Controller()
export class ExportTokenController {
  private logger: Logger = new Logger(ExportTokenService.name);
  constructor(private readonly exportTokenService: ExportTokenService) {}

  /**
   * Fetches a List of all saved export token.
   * @returns A list of export token with current status, history and last timestamp.
   */
  @MessagePattern(MQPatterns.PREFIX + MQPatterns.TOKEN_LIST)
  async listEAD(@Payload() dto: TokenQueryDto): Promise<ExportTokenDTO[]> {
    return this.exportTokenService.findAll(dto, dto.role);
  }

  /**
   * Writes a new Token to Blockchain. Uses the currently registered Wallet.
   * @param dto The new EAD to be tokenized.
   * @returns The new token that has been written to the chain.
   */
  @MessagePattern(MQPatterns.PREFIX + MQPatterns.TOKEN_CREATE)
  createEAD(@Payload() dto: TokenTransactionDto): Promise<ExportSaveDTO> {
    if (!dto.mnemonic || dto.mnemonic == '')
      dto.mnemonic = process.env.WALLET_ADDRESS_MNEMONIC;

    return this.exportTokenService.createExportToken(dto);
  }

  /**
   * Updates the export token on the Chain.
   * @param dto The new information to be updated.
   * @returns The new Token with updated information.
   */
  @MessagePattern(MQPatterns.PREFIX + MQPatterns.TOKEN_UPDATE)
  updateEAD(@Payload() dto: TokenTransactionDto): Promise<ExportTokenDTO> {
    return this.exportTokenService.updateExportToken(dto);
  }

  /**
   * Finds a specific export token by its identifying ID.
   * @param dto The Query DTO with the info necessary to perform the query TX.
   * @returns The token with current status, history and last timestamp.
   */
  @MessagePattern(MQPatterns.PREFIX + MQPatterns.TOKEN_FIND)
  findEAD(@Payload() dto: TokenQueryDto): Promise<ExportTokenDTO> {
    return this.exportTokenService.findById(dto, dto.role);
  }

  /**
   * Finds all token which are owned by a wallet address.
   * @param user The wallet Address to be queried.
   * @returns A list of token with current status, history and last timestamp.
   */
  @MessagePattern(MQPatterns.PREFIX + MQPatterns.TOKEN_FIND_USER)
  findDriverEad(@Payload() dto: TokenQueryDto): Promise<ExportTokenDTO[]> {
    return this.exportTokenService.filterByStatus(
      [ExportStatus.PICKUP],
      dto,
      dto.role
    );
  }

  /**
   * Returns a list of all token which have the "Archived" status
   * @returns A list of export token with current status, history and last timestamp.
   */
  @MessagePattern(MQPatterns.PREFIX + MQPatterns.TOKEN_ARCHIVED)
  findArchivedEADs(@Payload() dto: TokenQueryDto): Promise<ExportTokenDTO[]> {
    return this.exportTokenService.filterByStatus(
      [ExportStatus.ARCHIVED],
      dto,
      dto.role
    );
  }

  /**
   * Returns a list of all token which are ready for an import process.
   * @param user Addressee of the export process.
   * @returns A list of export token with current status, history and last timestamp.
   * userEntity.attributes.company[0].toString()
   */
  @MessagePattern(MQPatterns.PREFIX + MQPatterns.EAD_FIND_IMPORT)
  async findImportEADs(
    @Payload() dto: TokenQueryDto
  ): Promise<ExportTokenDTO[]> {
    return this.exportTokenService.filterByCompany(dto);
  }

  @MessagePattern(MQPatterns.PREFIX + MQPatterns.TOKEN_FETCH_HISTORY)
  async fetchTokenHistory(@Payload() dto: GetTokenHistoryDto): Promise<any> {
    return this.exportTokenService.getTokenHistory(dto);
  }
}

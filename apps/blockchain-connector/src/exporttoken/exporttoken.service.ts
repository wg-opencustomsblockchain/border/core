/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Inject, Injectable, Logger, NotFoundException } from '@nestjs/common';
import { Company } from '../shared/model/entities/company';

import { AmqpClientEnum, UserRole } from '@border/api-interfaces';
import {
  MsgFetchExportTokenAllDriver,
  MsgFetchExportTokenAllExporter,
  MsgFetchExportTokenAllImporter,
  MsgFetchExportTokenDriver,
  MsgFetchExportTokenExporter,
  MsgFetchExportTokenImporter,
} from '@border/api-interfaces/lib/client/org.borderblockchain.businesslogic/module';
import { ExportEvent } from '@border/api-interfaces/lib/client/org.borderblockchain.businesslogic/types/exporttoken/event-history';
import { HistoryMqPattern } from '@border/api-interfaces/lib/patterns/history.mqpattern';
import { ClientProxy } from '@nestjs/microservices';
import * as crypto from 'crypto';
import { SystemMessage } from '../messages';
import { TendermintService } from '../tendermint/service/tendermint.service';
import { TxMessages } from '../tendermint/service/tx-messages';
import { ExportTokenDTO } from './dto/export.dto';
import { ExportTokenListResponse, GetExportTokenResponseDTO } from './dto/exporttoken.query.dto';
import { TokenQueryDto, TokenTransactionDto, GetTokenHistoryDto } from './dto/transaction.dto';
import { EventDto } from '../../../../libs/api-interfaces/src/lib/dto/event.dto';

/**
 * Service class to pass requests to the corresponding chain endpoints.
 * Our Blockchain API delivers a full event history alongside each Token to minimize API calls.
 */
@Injectable()
export class ExportTokenService {
  //** Create the logger for this service. */
  private readonly logger = new Logger(ExportTokenService.name);

  /**Creates a Service instance. Injects a Tendermint Service. */
  constructor(
    private tendermintService: TendermintService,
    @Inject(AmqpClientEnum.AMQP_CLIENT_HISTORY) private client: ClientProxy
  ) {}

  /**
   * Queries all Token and reads the current Timestamp + Event Status.
   * @returns A list of export token with current status, history and last timestamp.
   */
  public async findAll(dto: TokenQueryDto, role: UserRole): Promise<ExportTokenDTO[]> {
    let msgArray;
    let msgType;

    const msg: any = {
      timestamp: new Date().toString() + crypto.randomBytes(4).readUInt32LE() / 0x100000000,
      tokenWalletId: dto.tokenWalletId,
    };

    switch (role) {
      case UserRole.EXPORTER: {
        msgArray = msg as MsgFetchExportTokenAllExporter[];
        msgType = TxMessages.MsgFetchExportTokenAllExporter as string;
        break;
      }

      case UserRole.IMPORTER: {
        msgArray = msg as MsgFetchExportTokenAllImporter[];
        msgType = TxMessages.MsgFetchExportTokenAllImporter as string;
        break;
      }

      case UserRole.DRIVER: {
        msgArray = msg as MsgFetchExportTokenAllDriver[];
        msgType = TxMessages.MsgFetchExportTokenAllDriver as string;
        break;
      }
    }

    const tokens: ExportTokenListResponse = await this.tendermintService.buildSignAndBroadcast(
      dto.mnemonic,
      [msgArray],
      msgType
    );

    const result: ExportTokenDTO[] = [];

    if (tokens) {
      for (const token of tokens.tokens) {
        const entry: ExportTokenDTO = token.exportToken;
        entry.events = token.events as EventDto[];
        const last: ExportEvent = token.events[token.events.length - 1];
        entry.timestamp = last.timestamp;
        entry.status = last.status;
        result.push(entry);
      }
    }
    return result;
  }

  /**
   * Queries all Token and reads the current Timestamp + Event Status.
   * @returns A list of export token with current status, history and last timestamp.
   */
  public async findAllAsImport(dto: TokenQueryDto, company: Company): Promise<ExportTokenDTO[]> {
    const msg: MsgFetchExportTokenAllImporter = {
      creator: '',
      timestamp: new Date().toString() + crypto.randomBytes(4).readUInt32LE() / 0x100000000,
      companyCountry: company.address.countryCode,
      companyName: company.name,
      identificationNumber: company.identificationNumber,
    };

    const tokens: ExportTokenListResponse = await this.tendermintService.buildSignAndBroadcast(
      dto.mnemonic,
      [msg],
      TxMessages.MsgFetchExportTokenAllImporter
    );

    const result: ExportTokenDTO[] = [];

    if (tokens) {
      for (const token of tokens.tokens) {
        const entry: ExportTokenDTO = token.exportToken;
        entry.events = token.events as EventDto[];
        const last: ExportEvent = token.events[token.events.length - 1];
        entry.timestamp = last.timestamp;
        entry.status = last.status;
        result.push(entry);
      }
    }
    return result;
  }

  /**
   * Finds a specific export token by its identifying ID.
   * Queries for the ID
   * @param mrn The process ID which is mapped internally to a token ID.
   * @returns The token with current status, history and last timestamp.
   */
  public async findById(dto: TokenQueryDto, role: UserRole): Promise<ExportTokenDTO> {
    let msgType;

    let msg: any = {
      creator: dto.wallet,
      id: dto.query,
      timestamp: new Date().toString() + crypto.randomBytes(4).readUInt32LE() / 0x100000000,
      tokenWalletId: dto.tokenWalletId,
    };

    switch (role) {
      case UserRole.EXPORTER: {
        msg = msg as MsgFetchExportTokenExporter;
        msgType = TxMessages.MsgFetchExportTokenExporter as string;
        break;
      }

      case UserRole.IMPORTER: {
        msg = msg as MsgFetchExportTokenImporter;
        msgType = TxMessages.MsgFetchExportTokenImporter as string;
        break;
      }

      case UserRole.DRIVER: {
        msg = msg as MsgFetchExportTokenDriver;
        msgType = TxMessages.MsgFetchExportTokenDriver as string;
        break;
      }
    }

    const token: GetExportTokenResponseDTO = await this.tendermintService.buildSignAndBroadcast(
      dto.mnemonic,
      [msg],
      msgType
    );

    if (!token) throw new NotFoundException(SystemMessage.ERROR_NOT_FOUND);
    const result: ExportTokenDTO = token.exportToken;
    result.events = token.events as EventDto[];

    // Extract process info from event History.
    const last = result.events[result.events.length - 1];
    result.timestamp = last.timestamp;
    result.status = last.status;
    return result;
  }

  /**
   * Filters Token by Company.
   * @param company The Company for filtering.
   * @returns A list of export token with current status, history and last timestamp.
   */
  public async filterByCompany(dto: TokenQueryDto): Promise<ExportTokenDTO[]> {
    const c: Company = JSON.parse(JSON.stringify(dto.query));
    let tokens: ExportTokenDTO[] = await this.findAllAsImport(dto, c);
    tokens = tokens.filter((ex) => +ex.status !== 0);
    return tokens;
  }

  /**
   * Returns only Token with a certain status under a wallet address.
   * Queries for user.
   * @param status The requested status.
   * @param user The wallet address to query for.
   * @returns A list of export token with current status, history and last timestamp.
   */

  public async filterByStatus(status: string[], dto: TokenQueryDto, view: UserRole): Promise<ExportTokenDTO[]> {
    const tokens: ExportTokenDTO[] = await this.findAll(dto, view);
    const result: ExportTokenDTO[] = [];
    for (const token of tokens) {
      if (status.includes(token.status)) {
        result.push(token);
      }
    }
    return result;
  }

  // --- Transactions ---

  /**
   * Writes an export to the chain and creates a token.
   * @param msg DTO to be saved.
   * @returns The new token that has been written to the chain.
   */
  public async createExportToken(msg: TokenTransactionDto): Promise<ExportTokenDTO> {
    const result = await this.tendermintService.buildSignAndBroadcast(
      msg.mnemonic,
      msg.dto,
      TxMessages.MsgCreateExportToken
    );

    return result;
  }

  /**
   * Creates a new Event for the Export Token.
   * @param msg DTO used for the update.
   * @returns The new Token with updated information.
   */

  public async updateExportToken(msg: TokenTransactionDto): Promise<ExportTokenDTO> {
    this.logger.debug(msg);

    const result = await this.tendermintService.buildSignAndBroadcast(
      msg.mnemonic,
      msg.dto,
      TxMessages.MsgUpdateExportToken
    );

    this.client
      .send(HistoryMqPattern.PREFIX + HistoryMqPattern.GENERATE_HISTORY, { token: result['exportToken'] })
      .subscribe();

    return result;
  }

  public async getTokenHistory(dto: GetTokenHistoryDto): Promise<any> {
    const msg: any = {
      id: dto.id,
    };
    this.logger.debug(msg);

    return this.tendermintService.buildSignAndBroadcast(dto.mnemonic, [msg], TxMessages.MsgFetchTokenHistory);
  }
}

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

const EVENT_IMPORT = {
  creator: 'cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462y',
  status: 'string',
  message: 'Created',
  eventType: 'IMPORT_ACCEPTED',
  timestamp: '2021-10-14T19:42:40Z',
};

const EVENT_EXPORT = {
  creator: 'cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462y',
  status: 'string',
  message: 'Created',
  eventType: 'EXPORT_RECORDED',
  timestamp: '2021-10-14T19:42:40Z',
};

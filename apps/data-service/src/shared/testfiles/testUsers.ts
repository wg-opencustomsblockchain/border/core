/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { User, UserRole } from '@border/api-interfaces';

export const USER_MOCK: User = {
  attributes: {
    wallet: ['...'],
    mnemonic: ['cabbage dynamic purse danger stairs bike solid bridge mushroom face winter fantasy virtual before recycle weasel emotion royal equip story undo poet tonight hub'],
    pubkey: ['...'],
    company: [
      {
        subsidiaryNumber: 1,
        name: 'Ina Import Ltd.',
        address: {
          streetAndNumber: '...',
          postcode: '...',
          city: '...',
          countryCode: 'GB',
        },
        identificationNumber: 'UK1234567891234',
        customsId: '...',
      },
    ],
  },
  id: '1',
  username: 'Ina',
  resource_access: { nest: { roles: [UserRole.EXPORTER] } },
};

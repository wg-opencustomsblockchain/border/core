/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * Enum for Endpoints which are shared by the Modules.
 * Parts of the application communicate with other MQ
 * Services such as the Placeholder User Service.
 */
export enum SharedEndpoints {
  MQ_PREFIX = 'usermanagement/v1/',
  MQ_TOKEN_USER_MANAGEMENT_FIND_USER = 'users',
}

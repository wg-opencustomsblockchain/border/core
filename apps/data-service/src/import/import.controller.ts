/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { DataMqPattern } from '@border/api-interfaces';
import { ImportService } from './import.service';
import { ImportTokenDTO } from '@border/api-interfaces/lib/dto/import.dto';

@Controller()
export class ImportController {
  constructor(private readonly importService: ImportService) {}

  /**
   * Writes a token to the chain via the BlockchainConnector.
   * @param dto - The import to be tokenized.
   * @returns - The import token.
   */
  @MessagePattern(DataMqPattern.PREFIX + DataMqPattern.DATA_IMPORT_CREATE)
  public async createImport(@Payload() body): Promise<ImportTokenDTO> {
    return this.importService.createImport(body.user, body.dto);
  }

  /**
   * Updates the current status of the import token.
   * @param dto - Token to be updated. Only status may be changed.
   * @returns The updated token
   */
  @MessagePattern(DataMqPattern.PREFIX + DataMqPattern.DATA_IMPORT_UPDATE)
  public async updateImport(@Payload() body): Promise<ImportTokenDTO> {
    return this.importService.updateImport(body.user, body.dto);
  }

  /**
   * Queries for all import token on the Chain.
   * @param body - Object containing the user element
   * @returns A List of EAD-DTOs.
   */
  @MessagePattern(DataMqPattern.PREFIX + DataMqPattern.DATA_IMPORT_LIST)
  public async listImport(@Payload() body): Promise<ImportTokenDTO[]> {
    return this.importService.findImports(body.user);
  }

  /**
   * Queries for a token by its token id.
   * @param body - The body.id and user under which the import token can be found.
   * @returns The corresponding token to the import token id.
   */
  @MessagePattern(DataMqPattern.PREFIX + DataMqPattern.DATA_IMPORT_FIND)
  public async findImportById(@Payload() body): Promise<ImportTokenDTO> {
    return this.importService.findImportById(body.user, body.id);
  }

  /**
   * Queries for a token by its token import id (mrn).
   * @param body - The mrn and user under which the import token can be found.
   * @returns The corresponding token to the import token id.
   */
  @MessagePattern(DataMqPattern.PREFIX + DataMqPattern.DATA_IMPORT_FIND_IMPORT)
  public async findImportByImportId(
    @Payload() body
  ): Promise<ImportTokenDTO[]> {
    return this.importService.findImportByImportId(body.user, body.mrn);
  }

  /**
   * Retrieves all import token which belong to a user (wallet).
   * @param wallet - The Wallet address to be used for the query.
   * @returns - A list of import token which belong to the user.
   */
  @MessagePattern(DataMqPattern.PREFIX + DataMqPattern.DATA_IMPORT_USER)
  public async findUserImports(@Payload() body): Promise<ImportTokenDTO[]> {
    return this.importService.findImportByUser(body.user, body.wallet);
  }
}

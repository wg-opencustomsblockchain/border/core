/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Test, TestingModule } from '@nestjs/testing';

import { ImportService } from './import.service';
import { MQBroker } from '@border/broker';
import { AmqpClientEnum, User, UserRole } from '@border/api-interfaces';
import { ImportTokenDTO } from '@border/api-interfaces/lib/dto/import.dto';
import { DynamicModule } from '@nestjs/common';
import { IMPORT_MOCK } from '../shared/testfiles/testImportToken';
import { TokenModule } from '../token/token.module';
import { ClientProxy } from '@nestjs/microservices';
import { of } from 'rxjs';
import { TokenService } from '../token/token.service';
import { USER_MOCK } from '../shared/testfiles/testUsers';

describe('DataService', () => {
  let service: ImportService;
  let blockchainClient: ClientProxy;
  let tokenService: TokenService;

  const mockUser: User = USER_MOCK;
  const mockImportToken: ImportTokenDTO = IMPORT_MOCK;
  const mqBroker: DynamicModule = new MQBroker().getMsgBroker();

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [mqBroker, TokenModule],
      providers: [ImportService],
    }).compile();

    service = module.get<ImportService>(ImportService);
    blockchainClient = module.get<ClientProxy>(
      AmqpClientEnum.AMQP_CLIENT_BLOCKCHAIN
    );
    tokenService = module.get<TokenService>(TokenService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('createImport', () => {
    it('should create import token', async () => {
      const blockchainClientSpy = jest
        .spyOn(blockchainClient, 'send')
        .mockReturnValue(of(mockImportToken));
      const tokenServiceSpy = jest
        .spyOn(tokenService, 'prepareTransportData')
        .mockReturnValue(Promise.resolve(mockImportToken));
      expect(await service.createImport(mockUser, [mockImportToken])).toEqual(
        mockImportToken
      );
      expect(blockchainClientSpy).toBeCalled();
      expect(tokenServiceSpy).toBeCalled();
    });
  });

  describe('updateImport', () => {
    it('should update import token', async () => {
      const blockchainClientSpy = jest
        .spyOn(blockchainClient, 'send')
        .mockReturnValue(of(mockImportToken));
      const tokenServiceSpy = jest
        .spyOn(tokenService, 'prepareTransportData')
        .mockReturnValue(Promise.resolve(mockImportToken));
      expect(await service.updateImport(mockUser, [mockImportToken])).toEqual(
        mockImportToken
      );
      expect(blockchainClientSpy).toBeCalled();
      expect(tokenServiceSpy).toBeCalled();
    });
  });

  describe('findImports', () => {
    it('should find all import token', async () => {
      const blockchainClientSpy = jest
        .spyOn(blockchainClient, 'send')
        .mockReturnValue(of(mockImportToken));
      const tokenServiceSpy = jest
        .spyOn(tokenService, 'prepareMultipleTransportData')
        .mockReturnValue(Promise.resolve([mockImportToken]));
      expect(await service.findImports(mockUser)).toEqual([mockImportToken]);
      expect(blockchainClientSpy).toBeCalled();
      expect(tokenServiceSpy).toBeCalled();
    });
  });

  describe('findImportById', () => {
    it('should find import token by id', async () => {
      const blockchainClientSpy = jest
        .spyOn(blockchainClient, 'send')
        .mockReturnValue(of(mockImportToken));
      const tokenServiceSpy = jest
        .spyOn(tokenService, 'prepareTransportData')
        .mockReturnValue(Promise.resolve(mockImportToken));
      expect(
        await service.findImportById(mockUser, mockImportToken.id)
      ).toEqual(mockImportToken);
      expect(blockchainClientSpy).toBeCalled();
      expect(tokenServiceSpy).toBeCalled();
    });
  });

  describe('findImportByImportId', () => {
    it('should find import token by import id', async () => {
      const blockchainClientSpy = jest
        .spyOn(blockchainClient, 'send')
        .mockReturnValue(of(mockImportToken));
      const tokenServiceSpy = jest
        .spyOn(tokenService, 'prepareMultipleTransportData')
        .mockReturnValue(Promise.resolve([mockImportToken]));
      expect(
        await service.findImportByImportId(mockUser, mockImportToken.importId)
      ).toEqual([mockImportToken]);
      expect(blockchainClientSpy).toBeCalled();
      expect(tokenServiceSpy).toBeCalled();
    });
  });

  describe('findImportByUser', () => {
    it('should find import token by user', async () => {
      const blockchainClientSpy = jest
        .spyOn(blockchainClient, 'send')
        .mockReturnValue(of(mockImportToken));
      const tokenServiceSpy = jest
        .spyOn(tokenService, 'prepareMultipleTransportData')
        .mockReturnValue(Promise.resolve([mockImportToken]));
      expect(await service.findImportByUser(mockUser, mockUser.wallet)).toEqual(
        [mockImportToken]
      );
      expect(blockchainClientSpy).toBeCalled();
      expect(tokenServiceSpy).toBeCalled();
    });
  });
});

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Test, TestingModule } from '@nestjs/testing';

import { DynamicModule } from '@nestjs/common';
import { TokenQueryDto, UserRole } from '@border/api-interfaces';

import { MQBroker } from '@border/broker';
import { IMPORT_MOCK } from '../shared/testfiles/testImportToken';
import { USER_MOCK } from '../shared/testfiles/testUsers';
import { ImportController } from './import.controller';
import { ImportService } from './import.service';
import { TokenModule } from '../token/token.module';
import { ImportTokenDTO } from '@border/api-interfaces/lib/dto/import.dto';

describe('ImportTokenController', () => {
  let controller: ImportController;
  let service: ImportService;
  const tokens: ImportTokenDTO[] = [];
  const user = USER_MOCK;

  const mockDto: TokenQueryDto = {
    wallet: 'MOCK',
    mnemonic:
      'link wild parent false local room leaf people couch drift yard heavy trap coral husband scorpion paper donor retire enrich visual donkey birth involve',
    query: 'MOCK',
    tokenWalletId: 'ERIK_WALLET',
    role: UserRole.EXPORTER,
  };

  const mqBroker: DynamicModule = new MQBroker().getMsgBroker();
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [mqBroker, TokenModule],
      controllers: [ImportController],
      providers: [ImportService],
    }).compile();

    controller = module.get<ImportController>(ImportController);
    service = module.get<ImportService>(ImportService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(service).toBeDefined();
  });

  describe('listAllImportToken', () => {
    it('should be listed', async () => {
      jest
        .spyOn(service, 'findImports')
        .mockReturnValue(Promise.resolve(tokens));
      expect(await controller.listImport(mockDto)).toEqual(tokens);
    });
  });

  describe('getImportTokenByID', () => {
    it('should be found', async () => {
      const tokenRequest: ImportTokenDTO = IMPORT_MOCK;
      mockDto.query = tokenRequest.id;
      jest
        .spyOn(service, 'findImportById')
        .mockReturnValue(Promise.resolve(IMPORT_MOCK));
      expect(await controller.findImportById(mockDto)).toEqual(IMPORT_MOCK);
    });
  });

  describe('createImport', () => {
    it('should be created', async () => {
      const tokenRequest: ImportTokenDTO = IMPORT_MOCK;
      const tokenResponse: ImportTokenDTO = IMPORT_MOCK;
      jest
        .spyOn(service, 'createImport')
        .mockReturnValue(Promise.resolve(tokenResponse));
      expect(await controller.createImport(tokenRequest)).toEqual(
        tokenResponse
      );
    });
  });

  describe('updateImport', () => {
    it('should update', async () => {
      const tokenRequest: ImportTokenDTO = IMPORT_MOCK;
      const tokenResponse: ImportTokenDTO = IMPORT_MOCK;
      jest
        .spyOn(service, 'updateImport')
        .mockReturnValue(Promise.resolve(tokenResponse));
      expect(await controller.updateImport(tokenRequest)).toEqual(
        tokenResponse
      );
    });
  });

  describe('findUserToken', () => {
    it('should be found', async () => {
      mockDto.query = user;
      jest
        .spyOn(service, 'findImportByUser')
        .mockReturnValue(Promise.resolve([IMPORT_MOCK]));
      expect(await controller.findUserImports(mockDto)).toEqual([IMPORT_MOCK]);
    });
  });
});

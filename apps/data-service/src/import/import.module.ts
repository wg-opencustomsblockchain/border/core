/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { DynamicModule, Module } from '@nestjs/common';
import { ImportController } from './import.controller';
import { ImportService } from './import.service';

import { TokenModule } from '../token/token.module';
import { MQBroker } from '@border/broker';

const mqBroker: DynamicModule = new MQBroker().getMsgBroker();
/** Module definitions for all modules relating to exporttoken functionalities. */
@Module({
  imports: [mqBroker, TokenModule],
  controllers: [ImportController],
  providers: [ImportService],
})
export class ImportModule {}

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { firstValueFrom } from 'rxjs';
import { AmqpClientEnum, ExportMQPatterns, User } from '@border/api-interfaces';
import { ImportTokenDTO } from '@border/api-interfaces/lib/dto/import.dto';
import { TokenService } from '../token/token.service';

@Injectable()
export class ImportService {
  constructor(
    @Inject(AmqpClientEnum.AMQP_CLIENT_BLOCKCHAIN)
    private blockchainClient: ClientProxy,
    public readonly tokenService: TokenService
  ) {}

  /**
   * Sends a creation request to the Blockchain Connector.
   * @param dto - The import(s) to be tokenized.
   * @returns - The import token which holds the import.
   */
  public async createImport(
    user: User,
    dto: ImportTokenDTO[]
  ): Promise<ImportTokenDTO> {
    const token = await this.tokenService.prepareToken(user, dto);
    const tx = this.tokenService.createTransaction(user, token);

    const result = await firstValueFrom(
      this.blockchainClient.send(
        ExportMQPatterns.MQ_API_TAG + ExportMQPatterns.MQ_IMPORT_CREATE,
        tx
      )
    );

    return (await this.tokenService.prepareTransportData(
      user,
      result
    )) as ImportTokenDTO;
  }

  /**
   * Sends an update request to the Blockchain Connector.
   * An Import may be updated completely in contrast to an export.
   * @param dto - The token(s) to be updated.
   * @returns - The updated token.
   */
  public async updateImport(
    user: User,
    dto: ImportTokenDTO[]
  ): Promise<ImportTokenDTO> {
    const token = await this.tokenService.prepareToken(user, dto);
    const tx = this.tokenService.createTransaction(user, token);

    const result = await firstValueFrom(
      this.blockchainClient.send(
        ExportMQPatterns.MQ_API_TAG + ExportMQPatterns.MQ_IMPORT_UPDATE,
        tx
      )
    );

    return (await this.tokenService.prepareTransportData(
      user,
      result
    )) as ImportTokenDTO;
  }

  /**
   * Queries all import token.
   * @param user - user for which to query
   * @returns - A list of all export token.
   */
  public async findImports(user: User): Promise<ImportTokenDTO[]> {
    const query = this.tokenService.createQuery(user, '');

    const result = await firstValueFrom(
      this.blockchainClient.send(
        ExportMQPatterns.MQ_API_TAG + ExportMQPatterns.MQ_IMPORT_LIST,
        query
      )
    );

    return (await this.tokenService.prepareMultipleTransportData(
      user,
      result
    )) as ImportTokenDTO[];
  }

  /**
   * Queries for a single import token. Uses token id.
   * @param id - The token id of the token.
   * @returns - A token corresponding to the token id.
   */
  public async findImportById(user: User, id: string): Promise<ImportTokenDTO> {
    const query = this.tokenService.createQuery(user, id);

    const result = await firstValueFrom(
      this.blockchainClient.send(
        ExportMQPatterns.MQ_API_TAG + ExportMQPatterns.MQ_IMPORT_FIND,
        query
      )
    );

    return (await this.tokenService.prepareTransportData(
      user,
      result
    )) as ImportTokenDTO;
  }

  /**
   * Queries for a single import token. Uses import id.
   * @param mrn - The import id of the token.
   * @returns - A token corresponding to the import id.
   * @deprecated
   */
  public async findImportByImportId(
    user: User,
    mrn: string
  ): Promise<ImportTokenDTO[]> {
    const query = this.tokenService.createQuery(user, mrn);

    const result = await firstValueFrom(
      this.blockchainClient.send(
        ExportMQPatterns.MQ_API_TAG + ExportMQPatterns.MQ_IMPORT_FIND_IMPORT_ID,
        query
      )
    );

    return (await this.tokenService.prepareMultipleTransportData(
      user,
      result
    )) as ImportTokenDTO[];
  }

  /**
   * Queries for all token belonging to a User
   * @param user - The user wallet address.
   * @returns - The list of token belonging to the address.
   */
  public async findImportByUser(
    user: User,
    wallet: string
  ): Promise<ImportTokenDTO[]> {
    const query = this.tokenService.createQuery(user, wallet);

    const result = await firstValueFrom(
      this.blockchainClient.send(
        ExportMQPatterns.MQ_API_TAG + ExportMQPatterns.MQ_IMPORT_FIND_USER,
        query
      )
    );

    return (await this.tokenService.prepareMultipleTransportData(
      user,
      result
    )) as ImportTokenDTO[];
  }
}

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Logger, LogLevel } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';

import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { AmqpClientEnum } from '@border/api-interfaces';
import { AppModule } from './app.module';

async function bootstrap() {
  const data = await NestFactory.createMicroservice<MicroserviceOptions>(
    AppModule,
    {
      transport: Transport.RMQ,
      options: {
        urls: [process.env.MSG_QUEUE_URL],
        queue: AmqpClientEnum.AMQP_QUEUE_DATA,
        queueOptions: {
          durable: false,
        },
      },

      logger:
        process.env.LOG_LEVEL === 'dev'
          ? (process.env.LOG_SETTINGS_DEV.split(',') as LogLevel[])
          : (process.env.LOG_SETTINGS_PROD.split(',') as LogLevel[]),
    }
  );

  const logger = new Logger('DATA-SERVICE');

  data
    .listen()
    .then(() =>
      logger.log(
        `🚀 - Data Service started with RMQ: ${process.env.MSG_QUEUE_URL}`
      )
    );
}

bootstrap();

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Inject, Injectable, Logger } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { firstValueFrom } from 'rxjs';
import {
  Address,
  AmqpClientEnum,
  CompanyAMQPatterns,
  ExportMQPatterns,
  TokenTransactionDto,
  User,
  UserMsgPatterns,
} from '@border/api-interfaces';
import { TokenService } from '../token/token.service';

import { ExportTokenDTO } from '@border/api-interfaces/lib/dto/export.dto';

@Injectable()
export class ExportService {
  private readonly logger = new Logger(ExportService.name);
  private COMPANY_BY_USER_ID =
    CompanyAMQPatterns.API_TAG +
    CompanyAMQPatterns.API_VERSION +
    CompanyAMQPatterns.GET_ONE_BY_ID;
  private USER_BY_COSMOS =
    UserMsgPatterns.API_TAG +
    UserMsgPatterns.API_VERSION +
    UserMsgPatterns.GET_ONE_BY_COSMOS;
  private USER_BY_ID =
    CompanyAMQPatterns.API_TAG +
    CompanyAMQPatterns.API_VERSION +
    CompanyAMQPatterns.GET_ONE_BY_ID;

  constructor(
    @Inject(AmqpClientEnum.AMQP_CLIENT_BLOCKCHAIN)
    public readonly blockchainService: ClientProxy,
    @Inject(AmqpClientEnum.AMQP_CLIENT_AUTH)
    public readonly authService: ClientProxy,
    @Inject(AmqpClientEnum.AMQP_CLIENT_COMPANY)
    public readonly compService: ClientProxy,
    @Inject(AmqpClientEnum.AMQP_CLIENT_HISTORY)
    public readonly historyService: ClientProxy,
    @Inject(AmqpClientEnum.AMQP_CLIENT_DATA)
    public readonly dataService: ClientProxy,
    public readonly tokenService: TokenService
  ) {}

  /**
   * Queries all export token.
   * @param user - for which user to get the tokens
   * @returns a list of export tokens
   */
  public async getAllExport(user: User): Promise<ExportTokenDTO[]> {
    const tx = this.tokenService.createQuery(user, '');
    const token: ExportTokenDTO[] = await firstValueFrom(
      this.blockchainService.send(
        ExportMQPatterns.MQ_API_TAG + ExportMQPatterns.MQ_EAD_LIST,
        tx
      )
    );
    return (await this.tokenService.prepareMultipleTransportData(
      user,
      token
    )) as ExportTokenDTO[];
  }

  /**
   * Queries for a single export token.
   * @param id - The uuid of the token.
   * @returns - A token corresponding to the mrn.
   */
  public async getExport(user: User, id: string): Promise<ExportTokenDTO> {
    const tx = this.tokenService.createQuery(user, id);
    const ead = await firstValueFrom(
      this.blockchainService.send<ExportTokenDTO>(
        ExportMQPatterns.MQ_API_TAG + ExportMQPatterns.MQ_EAD_FIND,
        tx
      )
    );
    return (await this.tokenService.prepareTransportData(
      user,
      ead
    )) as ExportTokenDTO;
  }

  /**
   * Sends a creation request to the Blockchain Connector.
   * @param user
   * @param ead - The EAD to be tokenized.
   * @returns - The export token which holds the EAD.
   */
  public async createExportToken(
    user: User,
    ead: ExportTokenDTO[]
  ): Promise<ExportTokenDTO> {
    const tx = this.tokenService.createQuery(user, ead);
    const token = await firstValueFrom(
      this.blockchainService.send(
        ExportMQPatterns.MQ_API_TAG + ExportMQPatterns.MQ_EAD_CREATE,
        tx
      )
    );
    return (await this.tokenService.prepareTransportData(
      user,
      token
    )) as ExportTokenDTO;
  }

  /**
   * Sends an update request to the Blockchain Connector.
   * @param user
   * @param token - The token to be updated.
   * @returns - The updated token.
   */

  public async updateExportToken(
    user: User,
    ead: ExportTokenDTO[]
  ): Promise<ExportTokenDTO> {
    const tx = this.tokenService.createTransaction(user, ead);
    const token = await firstValueFrom(
      this.blockchainService.send(
        ExportMQPatterns.MQ_API_TAG + ExportMQPatterns.MQ_EAD_UPDATE,
        tx
      )
    );

    return (await this.tokenService.prepareTransportData(
      user,
      token
    )) as ExportTokenDTO;
  }

  /**
   * Queries for all token belonging to a User
   * @param user - The user wallet address.
   * @returns - The list of token belonging to the address.
   */
  public async getUserToken(user: User): Promise<ExportTokenDTO[]> {
    const tx = this.tokenService.createQuery(user, user.wallet);
    const token = await firstValueFrom(
      this.blockchainService.send(
        ExportMQPatterns.MQ_API_TAG + ExportMQPatterns.MQ_EAD_FIND_USER,
        tx
      )
    );

    return (await this.tokenService.prepareMultipleTransportData(
      user,
      token
    )) as ExportTokenDTO[];
  }

  /**
   * Queries for all token with the status "Archived"-
   * @param user
   * @returns - All Archived token.
   */
  public async getArchivedEADs(user: User): Promise<ExportTokenDTO[]> {
    const tx = this.tokenService.createQuery(user, '');
    const token = await firstValueFrom(
      this.blockchainService.send(
        ExportMQPatterns.MQ_API_TAG + ExportMQPatterns.MQ_EAD_ARCHIVED,
        tx
      )
    );

    return (await this.tokenService.prepareMultipleTransportData(
      user,
      token
    )) as ExportTokenDTO[];
  }

  /**
   * Queries for all export token which are ready to be accepted as an import token.
   * @param user - The Addressee of the import process as a wallet address.
   * @param address - Physical address
   * @returns - A list of ready for import export token.
   */
  public async getImportEADs(
    user: User,
    address: Address
  ): Promise<ExportTokenDTO[]> {
    const tx = this.tokenService.createQuery(user, address);
    const token = await firstValueFrom(
      this.blockchainService.send(
        ExportMQPatterns.MQ_API_TAG + ExportMQPatterns.MQ_EAD_FIND_IMPORT,
        tx
      )
    );

    return (await this.tokenService.prepareMultipleTransportData(
      user,
      token
    )) as ExportTokenDTO[];
  }

  /**
   * Sends a creation request to the Blockchain Connector.
   * @param user
   * @param ead - The EAD to be tokenized.
   * @returns - The export token which holds the EAD.
   */
  public async testToken(): Promise<any> {
    const test =
      '{\r\n    "dto": [\r\n        {\r\n            "exporter": {\r\n                "address": {\r\n                    "streetAndNumber": "Jospeh von Fraunhofer Stra\u00DFe 2-4",\r\n                    "postcode": "44227",\r\n                    "city": "Dortmund",\r\n                    "countryCode": "DE"\r\n                },\r\n                "customsId": "AEB_ERIK_WALLET",\r\n                "name": "Fraunhofer IML",\r\n                "subsidiaryNumber": "0000"\r\n            },\r\n            "declarant": {\r\n                "address": {\r\n                    "streetAndNumber": "Export Stra\u00DFe 7",\r\n                    "postcode": "44787",\r\n                    "city": "Bochum",\r\n                    "countryCode": "DE"\r\n                },\r\n                "customsId": "DE805504768317777",\r\n                "name": "IML Export Inc.",\r\n                "subsidiaryNumber": "0000"\r\n            },\r\n            "representative": {},\r\n            "consignee": {\r\n                "address": {\r\n                    "streetAndNumber": "Logistics Way 89",\r\n                    "postcode": "SR5 3TW",\r\n                    "city": "Sunderland",\r\n                    "countryCode": "GB"\r\n                },\r\n                "name": "Ina Import Ltd."\r\n            },\r\n            "customsOfficeOfExport": {\r\n                "customsOfficeCode": "DE009303"\r\n            },\r\n            "customOfficeOfExit": {\r\n                "customsOfficeCode": "FR000740"\r\n            },\r\n            "goodsItems": [\r\n                {\r\n                    "documents": [\r\n                        {\r\n                            "type": "N380",\r\n                            "referenceNumber": "EXP0000002344"\r\n                        }\r\n                    ],\r\n                    "countryOfOrigin": "99",\r\n                    "sequenceNumber": 1,\r\n                    "descriptionOfGoods": "Arzneiwaren; hier: andere, Antibiotika enthaltend",\r\n                    "harmonizedSystemSubheadingCode": "300420",\r\n                    "localClassificationCode": "00",\r\n                    "grossMass": "2751.2",\r\n                    "netMass": "350",\r\n                    "numberOfPackages": 7,\r\n                    "typeOfPackages": "PK",\r\n                    "marksNumbers": "gezeichnet Adresse",\r\n                    "valueAtBorderExport": 94900,\r\n                    "valueAtBorderExportCurrency": "EUR"\r\n                },\r\n                {\r\n                    "documents": [\r\n                        {\r\n                            "type": "N380",\r\n                            "referenceNumber": "EXP0000002344"\r\n                        }\r\n                    ],\r\n                    "countryOfOrigin": "99",\r\n                    "sequenceNumber": 2,\r\n                    "descriptionOfGoods": "Arzneiwaren; hier: Insulin enthaltend",\r\n                    "harmonizedSystemSubheadingCode": "300431",\r\n                    "localClassificationCode": "00",\r\n                    "grossMass": "0",\r\n                    "netMass": "350",\r\n                    "typeOfPackages": "PK",\r\n                    "marksNumbers": "gezeichnet Adresse",\r\n                    "valueAtBorderExport": 94300,\r\n                    "valueAtBorderExportCurrency": "EUR"\r\n                },\r\n                {\r\n                    "documents": [\r\n                        {\r\n                            "type": "N380",\r\n                            "referenceNumber": "EXP0000002344"\r\n                        }\r\n                    ],\r\n                    "countryOfOrigin": "99",\r\n                    "sequenceNumber": 3,\r\n                    "descriptionOfGoods": "Arzneiwaren; hier: Ephedrin oder seine Salze enthaltend",\r\n                    "harmonizedSystemSubheadingCode": "300441",\r\n                    "localClassificationCode": "00",\r\n                    "grossMass": "0",\r\n                    "netMass": "390",\r\n                    "typeOfPackages": "PK",\r\n                    "marksNumbers": "gezeichnet Adresse",\r\n                    "valueAtBorderExport": 197500,\r\n                    "valueAtBorderExportCurrency": "EUR"\r\n                },\r\n                {\r\n                    "documents": [\r\n                        {\r\n                            "type": "N380",\r\n                            "referenceNumber": "EXP0000002344"\r\n                        }\r\n                    ],\r\n                    "countryOfOrigin": "99",\r\n                    "sequenceNumber": 4,\r\n                    "descriptionOfGoods": "Arzneiwaren; hier: Norephedrin oder seine Salze enthaltend",\r\n                    "harmonizedSystemSubheadingCode": "300443",\r\n                    "localClassificationCode": "00",\r\n                    "grossMass": "0",\r\n                    "netMass": "420",\r\n                    "typeOfPackages": "PK",\r\n                    "marksNumbers": "gezeichnet Adresse",\r\n                    "valueAtBorderExport": 90600,\r\n                    "valueAtBorderExportCurrency": "EUR"\r\n                },\r\n                {\r\n                    "documents": [\r\n                        {\r\n                            "type": "N380",\r\n                            "referenceNumber": "EXP0000002344"\r\n                        }\r\n                    ],\r\n                    "countryOfOrigin": "99",\r\n                    "sequenceNumber": 5,\r\n                    "descriptionOfGoods": "Arzneiwaren; hier: andere, Vitamine oder andere Erzeugnisse der Position 2936 enthaltend",\r\n                    "harmonizedSystemSubheadingCode": "300450",\r\n                    "localClassificationCode": "00",\r\n                    "grossMass": "0",\r\n                    "netMass": "1180",\r\n                    "typeOfPackages": "PK",\r\n                    "marksNumbers": "gezeichnet Adresse",\r\n                    "valueAtBorderExport": 221200,\r\n                    "valueAtBorderExportCurrency": "EUR"\r\n                }\r\n            ],\r\n            "transportToBorder": {\r\n                "modeOfTransport": 3\r\n            },\r\n            "transportAtBorder": {\r\n                "modeOfTransport": 3,\r\n                "typeOfIdentification": 33,\r\n                "nationality": "QU"\r\n            },\r\n            "exportId": "TESTABD",\r\n            "localReferenceNumber": "EXP0000002344",\r\n            "destinationCountry": "GB",\r\n            "exportCountry": "DE",\r\n            "itinerary": "DE, FR, GB",\r\n            "releaseDateAndTime": "2021-02-01T16:01:00",\r\n            "incotermCode": "DAP",\r\n            "incotermLocation": "Sunderland",\r\n            "totalGrossMass": "2751.2",\r\n            "goodsItemQuantity": 5,\r\n            "totalPackagesQuantity": 7,\r\n            "natureOfTransaction": 11,\r\n            "totalAmountInvoiced": 698522,\r\n            "invoiceCurrency": "EUR",\r\n            "user": "DEPRECATED",\r\n            "timestamp": "2022-08-12T12:40:31Z"\r\n        }\r\n    ],\r\n    "mnemonic": ""\r\n}';

    const object: TokenTransactionDto = JSON.parse(test);
    console.log(object);

    return this.blockchainService.send(
      ExportMQPatterns.MQ_API_TAG + ExportMQPatterns.MQ_EAD_CREATE,
      object
    );
  }
}

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Logger,
} from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { DataMqPattern } from '@border/api-interfaces';
import { ExportTokenDTO } from '@border/api-interfaces/lib/dto/export.dto';

import { ExportService } from './export.service';

@Controller()
export class ExportController {
  private readonly logger = new Logger(ExportController.name);
  constructor(private readonly exportService: ExportService) {}

  /**
   * Updates the current status of the import token.
   * @param dto - Token to be updated. Only status may be changed.
   * @returns The updated token
   */
  @MessagePattern(DataMqPattern.PREFIX + DataMqPattern.DATA_EXPORT_ALL)
  public async getExportTokenAll(@Payload() body): Promise<ExportTokenDTO[]> {
    try {
      return await this.exportService.getAllExport(body.user);
    } catch (e) {
      this.logger.error(e);
      throw new HttpException(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Updates the current status of the import token.
   * @param dto - Token to be updated. Only status may be changed.
   * @returns The updated token
   */
  @MessagePattern(DataMqPattern.PREFIX + DataMqPattern.DATA_EXPORT_FIND)
  public async getExportToken(@Payload() body): Promise<ExportTokenDTO> {
    try {
      return await this.exportService.getExport(body.user, body.id);
    } catch (e) {
      this.logger.error(e);
      throw new HttpException(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Updates the current status of the import token.
   * @param dto - Token to be updated. Only status may be changed.
   * @returns The updated token
   */
  @MessagePattern(DataMqPattern.PREFIX + DataMqPattern.DATA_EXPORT_ARCHIVED)
  public async getArchived(@Payload() body): Promise<ExportTokenDTO[]> {
    try {
      return await this.exportService.getArchivedEADs(body.user);
    } catch (e) {
      this.logger.error(e);
      throw new HttpException(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Updates the current status of the import token.
   * @param dto - Token to be updated. Only status may be changed.
   * @returns The updated token
   */
  @MessagePattern(DataMqPattern.PREFIX + DataMqPattern.DATA_EXPORT_CREATE)
  public async createExport(@Payload() body): Promise<ExportTokenDTO> {
    try {
      return await this.exportService.createExportToken(body.user, body.dto);
    } catch (e) {
      this.logger.error(e);
      throw new HttpException(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Updates the current status of the import token.
   * @param dto - Token to be updated. Only status may be changed.
   * @returns The updated token
   */
  @MessagePattern(DataMqPattern.PREFIX + DataMqPattern.DATA_EXPORT_UPDATE)
  public async updateExport(@Payload() body): Promise<ExportTokenDTO> {
    try {
      return await this.exportService.updateExportToken(body.user, body.dto);
    } catch (e) {
      this.logger.error(e);
      throw new HttpException(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Updates the current status of the import token.
   * @param dto - Token to be updated. Only status may be changed.
   * @returns The updated token
   */
  @MessagePattern(DataMqPattern.PREFIX + DataMqPattern.DATA_EXPORT_USER)
  public async findUserToken(@Payload() body): Promise<ExportTokenDTO[]> {
    try {
      return await this.exportService.getUserToken(body.user);
    } catch (e) {
      this.logger.error(e);
      throw new HttpException(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Updates the current status of the import token.
   * @param dto - Token to be updated. Only status may be changed.
   * @returns The updated token
   */
  @MessagePattern(DataMqPattern.PREFIX + DataMqPattern.DATA_EXPORT_IMPORTS)
  public async findNewImports(@Payload() body): Promise<ExportTokenDTO[]> {
    try {
      return await this.exportService.getImportEADs(body.user, body.address);
    } catch (e) {
      this.logger.error(e);
      throw new HttpException(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}

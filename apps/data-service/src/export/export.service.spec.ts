/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { DynamicModule } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { Test, TestingModule } from '@nestjs/testing';
import { of } from 'rxjs';
import { AmqpClientEnum } from '@border/api-interfaces';
import { MQBroker } from '@border/broker';
import { EXPORT_MOCK } from '../shared/testfiles/testExportToken';
import { USER_MOCK } from '../shared/testfiles/testUsers';
import { TokenModule } from '../token/token.module';
import { TokenService } from '../token/token.service';
import { ExportService } from './export.service';

describe('DataService', () => {
  let service: ExportService;
  let blockchainClient: ClientProxy;
  let tokenService: TokenService;

  const mqBroker: DynamicModule = new MQBroker().getMsgBroker();

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [mqBroker, TokenModule],
      providers: [ExportService],
    }).compile();

    service = module.get<ExportService>(ExportService);

    blockchainClient = module.get<ClientProxy>(
      AmqpClientEnum.AMQP_CLIENT_BLOCKCHAIN
    );
    tokenService = module.get<TokenService>(TokenService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('createExport', () => {
    it('should create export token', async () => {
      const blockchainClientSpy = jest
        .spyOn(blockchainClient, 'send')
        .mockReturnValue(of(EXPORT_MOCK));
      const tokenServiceSpy = jest
        .spyOn(tokenService, 'prepareTransportData')
        .mockReturnValue(Promise.resolve(EXPORT_MOCK));
      expect(await service.createExportToken(USER_MOCK, [EXPORT_MOCK])).toEqual(
        EXPORT_MOCK
      );
      expect(blockchainClientSpy).toBeCalled();
      expect(tokenServiceSpy).toBeCalled();
    });
  });

  describe('updateExportToken', () => {
    it('should update export token', async () => {
      const blockchainClientSpy = jest
        .spyOn(blockchainClient, 'send')
        .mockReturnValue(of(EXPORT_MOCK));
      const tokenServiceSpy = jest
        .spyOn(tokenService, 'prepareTransportData')
        .mockReturnValue(Promise.resolve(EXPORT_MOCK));
      expect(await service.createExportToken(USER_MOCK, [EXPORT_MOCK])).toEqual(
        EXPORT_MOCK
      );
      expect(blockchainClientSpy).toBeCalled();
      expect(tokenServiceSpy).toBeCalled();
    });
  });
  describe('getExport', () => {
    it('should get an export token', async () => {
      const blockchainClientSpy = jest
        .spyOn(blockchainClient, 'send')
        .mockReturnValue(of(EXPORT_MOCK));
      const tokenServiceSpy = jest
        .spyOn(tokenService, 'prepareTransportData')
        .mockReturnValue(Promise.resolve(EXPORT_MOCK));
      expect(await service.getExport(USER_MOCK, EXPORT_MOCK.id)).toEqual(
        EXPORT_MOCK
      );
      expect(blockchainClientSpy).toBeCalled();
      expect(tokenServiceSpy).toBeCalled();
    });
  });

  describe('getallExport', () => {
    it('should get all export token', async () => {
      const blockchainClientSpy = jest
        .spyOn(blockchainClient, 'send')
        .mockReturnValue(of(EXPORT_MOCK));
      const tokenServiceSpy = jest
        .spyOn(tokenService, 'prepareMultipleTransportData')
        .mockReturnValue(Promise.resolve([EXPORT_MOCK]));
      expect(await service.getAllExport(USER_MOCK)).toEqual([EXPORT_MOCK]);
      expect(blockchainClientSpy).toBeCalled();
      expect(tokenServiceSpy).toBeCalled();
    });
  });

  describe('getUserToken', () => {
    it('should get all export token belonging to a user', async () => {
      const blockchainClientSpy = jest
        .spyOn(blockchainClient, 'send')
        .mockReturnValue(of(EXPORT_MOCK));
      const tokenServiceSpy = jest
        .spyOn(tokenService, 'prepareMultipleTransportData')
        .mockReturnValue(Promise.resolve([EXPORT_MOCK]));
      expect(await service.getUserToken(USER_MOCK)).toEqual([EXPORT_MOCK]);
      expect(blockchainClientSpy).toBeCalled();
      expect(tokenServiceSpy).toBeCalled();
    });
  });

  describe('getArchivedEADs', () => {
    it('should get all export token which are archived', async () => {
      const blockchainClientSpy = jest
        .spyOn(blockchainClient, 'send')
        .mockReturnValue(of(EXPORT_MOCK));
      const tokenServiceSpy = jest
        .spyOn(tokenService, 'prepareMultipleTransportData')
        .mockReturnValue(Promise.resolve([EXPORT_MOCK]));
      expect(await service.getArchivedEADs(USER_MOCK)).toEqual([EXPORT_MOCK]);
      expect(blockchainClientSpy).toBeCalled();
      expect(tokenServiceSpy).toBeCalled();
    });
  });

  describe('getImportEADs', () => {
    it('should get all token related to a company', async () => {
      const blockchainClientSpy = jest
        .spyOn(blockchainClient, 'send')
        .mockReturnValue(of(EXPORT_MOCK));
      const tokenServiceSpy = jest
        .spyOn(tokenService, 'prepareMultipleTransportData')
        .mockReturnValue(Promise.resolve([EXPORT_MOCK]));
      expect(
        await service.getImportEADs(
          USER_MOCK,
          USER_MOCK.attributes.company[0].address
        )
      ).toEqual([EXPORT_MOCK]);
      expect(blockchainClientSpy).toBeCalled();
      expect(tokenServiceSpy).toBeCalled();
    });
  });
});

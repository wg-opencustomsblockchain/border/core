/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { DynamicModule, Module } from '@nestjs/common';
import { MQBroker } from '@border/broker';
import { TokenModule } from '../token/token.module';
import { ExportController } from './export.controller';
import { ExportService } from './export.service';

const mqBroker: DynamicModule = new MQBroker().getMsgBroker();
/** Module definitions for all modules relating to exporttoken functionalities. */
@Module({
  imports: [mqBroker, TokenModule],
  controllers: [ExportController],
  providers: [ExportService],
})
export class ExportModule {}

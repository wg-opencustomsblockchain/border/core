/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Test, TestingModule } from '@nestjs/testing';

import { DynamicModule } from '@nestjs/common';
import { TokenQueryDto, UserRole } from '@border/api-interfaces';

import { MQBroker } from '@border/broker';
import { ExportTokenDTO } from '@border/api-interfaces/lib/dto/export.dto';
import { EXPORT_MOCK } from '../shared/testfiles/testExportToken';
import { USER_MOCK } from '../shared/testfiles/testUsers';
import { TokenModule } from '../token/token.module';
import { ExportController } from './export.controller';
import { ExportService } from './export.service';

describe('ExportTokenController', () => {
  let controller: ExportController;
  let service: ExportService;
  const tokens: ExportTokenDTO[] = [];
  const user = USER_MOCK;

  const mockDto: TokenQueryDto = {
    wallet: 'MOCK',
    mnemonic:
      'link wild parent false local room leaf people couch drift yard heavy trap coral husband scorpion paper donor retire enrich visual donkey birth involve',
    query: 'MOCK',
    tokenWalletId: 'ERIK_WALLET',
    role: UserRole.EXPORTER,
  };

  const mqBroker: DynamicModule = new MQBroker().getMsgBroker();
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [mqBroker, TokenModule],
      controllers: [ExportController],
      providers: [ExportService],
    }).compile();
    controller = module.get<ExportController>(ExportController);
    service = module.get<ExportService>(ExportService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(service).toBeDefined();
  });

  describe('listAllToken', () => {
    it('should be listed', async () => {
      jest
        .spyOn(service, 'getAllExport')
        .mockReturnValue(Promise.resolve(tokens));
      expect(await controller.getExportTokenAll(mockDto)).toEqual(tokens);
    });
  });

  describe('getExportToken', () => {
    it('should be found', async () => {
      const tokenRequest: ExportTokenDTO = EXPORT_MOCK;
      mockDto.query = tokenRequest.exportId;
      jest
        .spyOn(service, 'getExport')
        .mockReturnValue(Promise.resolve(EXPORT_MOCK));
      expect(await controller.getExportToken(mockDto)).toEqual(EXPORT_MOCK);
    });
  });

  describe('getArchived', () => {
    it('should be found', async () => {
      const tokenRequest: ExportTokenDTO = EXPORT_MOCK;
      mockDto.query = tokenRequest.exportId;
      jest
        .spyOn(service, 'getArchivedEADs')
        .mockReturnValue(Promise.resolve([EXPORT_MOCK]));
      expect(await controller.getArchived([mockDto])).toEqual([EXPORT_MOCK]);
    });
  });

  describe('createEAD', () => {
    it('should be created', async () => {
      const tokenRequest: ExportTokenDTO = EXPORT_MOCK;
      const tokenResponse: ExportTokenDTO = EXPORT_MOCK;
      jest
        .spyOn(service, 'createExportToken')
        .mockReturnValue(Promise.resolve(tokenResponse));
      expect(await controller.createExport(tokenRequest)).toEqual(
        tokenResponse
      );
    });
  });

  describe('updateExport', () => {
    it('should update', async () => {
      const tokenRequest: ExportTokenDTO = EXPORT_MOCK;
      const tokenResponse: ExportTokenDTO = EXPORT_MOCK;
      jest
        .spyOn(service, 'updateExportToken')
        .mockReturnValue(Promise.resolve(tokenResponse));
      expect(await controller.updateExport(tokenRequest)).toEqual(
        tokenResponse
      );
    });
  });

  describe('findUserToken', () => {
    it('should be found', async () => {
      mockDto.query = user;
      jest
        .spyOn(service, 'getUserToken')
        .mockReturnValue(Promise.resolve([EXPORT_MOCK]));
      expect(await controller.findUserToken(mockDto)).toEqual([EXPORT_MOCK]);
    });
  });

  describe('findNewImports', () => {
    it('should be found', async () => {
      mockDto.query = user;
      jest
        .spyOn(service, 'getImportEADs')
        .mockReturnValue(Promise.resolve([EXPORT_MOCK]));
      expect(await controller.findNewImports(mockDto)).toEqual([EXPORT_MOCK]);
    });
  });
});

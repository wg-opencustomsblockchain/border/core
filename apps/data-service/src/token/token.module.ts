/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { DynamicModule, Module } from '@nestjs/common';
import { TokenService } from './token.service';

import { MQBroker } from '@border/api-interfaces/lib/broker';

const mqBroker: DynamicModule = new MQBroker().getMsgBroker();
/** Module definitions for all modules relating to exporttoken functionalities. */
@Module({
  imports: [mqBroker],

  providers: [TokenService],
  exports: [TokenService],
})
export class TokenModule {}

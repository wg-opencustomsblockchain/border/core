/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { DynamicModule } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import {
  AmqpClientEnum,
  TokenQueryDto,
  TokenTransactionDto,
  User,
} from '@border/api-interfaces';
import { ImportDTO } from '../../../../libs/api-interfaces/src/lib/entities/token/import.dto';
import { TokenService } from './token.service';
import { EXPORT_MOCK } from '../shared/testfiles/testExportToken';
import { IMPORT_MOCK } from '../shared/testfiles/testImportToken';
import { ClientProxy } from '@nestjs/microservices';
import { of } from 'rxjs';
import { USER_MOCK } from '../shared/testfiles/testUsers';
import { MQBroker } from '@border/broker';
describe('BlockchainConnectorService', () => {
  let service: TokenService;
  const mockEad: any = EXPORT_MOCK;
  const mockImportToken: any = IMPORT_MOCK;

  const mqBroker: DynamicModule = new MQBroker().getMsgBroker();
  const mockUser: User = USER_MOCK;

  let compClient: ClientProxy;
  let authClient: ClientProxy;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [mqBroker],
      providers: [TokenService],
    }).compile();

    service = module.get<TokenService>(TokenService);

    compClient = module.get<ClientProxy>(AmqpClientEnum.AMQP_CLIENT_COMPANY);
    authClient = module.get<ClientProxy>(AmqpClientEnum.AMQP_CLIENT_AUTH);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('createTransaction', () => {
    it('should create a TokenTransactionDto from a user and a dto', () => {
      const mockTransaction: TokenTransactionDto = {
        dto: mockEad,
        mnemonic: mockUser.mnemonic,
        tokenWalletId: mockUser.tokenWalletId,
        role: mockUser.resource_access.nest.roles[0],
      };
      expect(service.createTransaction(mockUser, mockEad)).toEqual(
        mockTransaction
      );
    });
  });

  describe('createQuery', () => {
    it('should create a query', () => {
      const mockQueryString = '';

      const mockQuery: TokenQueryDto = {
        wallet: mockUser.wallet,
        mnemonic: mockUser.mnemonic,
        query: mockQueryString,
        tokenWalletId: mockUser.tokenWalletId,
        role: mockUser.resource_access.nest.roles[0],
      };
      expect(service.createQuery(mockUser, mockQueryString)).toEqual(mockQuery);
    });
  });

  describe('prepareToken', () => {
    it('should add the necessary info to an import Token', () => {
      const mock = new ImportDTO();
      mock.importId = '#1';
      const result = service.prepareToken(mockUser, [mock]);
      expect(result).toBeDefined();
    });
  });

  describe('prepareTransportData', () => {
    it('should prepare export token data', async () => {
      const compClientSpy = jest
        .spyOn(compClient, 'send')
        .mockReturnValue(of(mockUser.attributes.company));

      const authClientSpy = jest
        .spyOn(authClient, 'send')
        .mockReturnValue(of(mockUser));

      const token = await service.prepareTransportData(mockUser, mockEad);
      expect(compClientSpy).toBeCalled();
      expect(authClientSpy).toBeCalled();

      expect(token).toEqual(mockEad);
    });

    it('should prepare import token data', async () => {
      const compClientSpy = jest
        .spyOn(compClient, 'send')
        .mockReturnValue(of(mockUser.attributes.company));

      const authClientSpy = jest
        .spyOn(authClient, 'send')
        .mockReturnValue(of(mockUser));

      const token = await service.prepareTransportData(mockUser, mockImportToken);
      expect(compClientSpy).toBeCalled();
      expect(authClientSpy).toBeCalled();

      expect(token).toEqual(mockImportToken);
    });
  });

  describe('prepareMultipleTransportData', () => {
    it('should prerpare multiple export tokens', async () => {
      const compClientSpy = jest
        .spyOn(compClient, 'send')
        .mockReturnValue(of(mockUser.attributes.company));

      const authClientSpy = jest
        .spyOn(authClient, 'send')
        .mockReturnValue(of(mockUser));

      await service.prepareMultipleTransportData(mockUser, [mockEad]);
      expect(compClientSpy).toBeCalled();
      expect(authClientSpy).toBeCalled();
    });

    it('should prepare multiple import tokens', async () => {
      const compClientSpy = jest
        .spyOn(compClient, 'send')
        .mockReturnValue(of(mockUser.attributes.company));

      const authClientSpy = jest
        .spyOn(authClient, 'send')
        .mockReturnValue(of(mockUser));

      await service.prepareMultipleTransportData(mockUser, [mockImportToken]);
      expect(compClientSpy).toBeCalled();
      expect(authClientSpy).toBeCalled();
    });
  });

  describe('prepareEvent', () => {
    it('should return an eventlist with an export token', async () => {
      const eventList = await service.prepareEvent(mockUser.wallet, [mockEad]);

      expect(eventList).toBeDefined();
    });

    it('should return an eventlist with an import token', async () => {
      const eventList = await service.prepareEvent(mockUser.wallet, [
        mockImportToken,
      ]);

      expect(eventList).toBeDefined();
    });
  });

  describe('getCosmosAddress', () => {
    it('should return cosmos address', async () => {
      const cosmos_address = await service.getCosmosAddress(
        mockUser.attributes.mnemonic[0]
      );

      expect(cosmos_address).toBeDefined();
    });
  });
});

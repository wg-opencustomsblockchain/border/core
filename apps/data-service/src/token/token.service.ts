/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { DirectSecp256k1HdWallet } from '@cosmjs/proto-signing';
import { Inject, Injectable, Logger } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { firstValueFrom } from 'rxjs';
import {
  AmqpClientEnum,
  Company,
  CompanyAMQPatterns,
  TokenQueryDto,
  TokenTransactionDto,
  User,
  UserMsgPatterns,
} from '@border/api-interfaces';
import { ImportTokenDTO } from '@border/api-interfaces/lib/dto/import.dto';
import { ExportTokenDTO } from '@border/api-interfaces/lib/dto/export.dto';
import { EventDto } from '@border/api-interfaces/lib/dto/event.dto';

@Injectable()
export class TokenService {
  private readonly logger = new Logger(TokenService.name);
  private COMPANY_BY_USER_ID =
    CompanyAMQPatterns.API_TAG +
    CompanyAMQPatterns.API_VERSION +
    CompanyAMQPatterns.GET_ONE_BY_ID;
  private USER_BY_COSMOS =
    UserMsgPatterns.API_TAG +
    UserMsgPatterns.API_VERSION +
    UserMsgPatterns.GET_ONE_BY_COSMOS;
  private USER_BY_ID =
    CompanyAMQPatterns.API_TAG +
    CompanyAMQPatterns.API_VERSION +
    CompanyAMQPatterns.GET_ONE_BY_ID;

  constructor(
    @Inject(AmqpClientEnum.AMQP_CLIENT_AUTH)
    public readonly authService: ClientProxy,
    @Inject(AmqpClientEnum.AMQP_CLIENT_BLOCKCHAIN)
    public readonly blockchainService: ClientProxy,
    @Inject(AmqpClientEnum.AMQP_CLIENT_COMPANY)
    public readonly compService: ClientProxy,
    @Inject(AmqpClientEnum.AMQP_CLIENT_HISTORY)
    public readonly historyService: ClientProxy,
    @Inject(AmqpClientEnum.AMQP_CLIENT_DATA)
    public readonly dataService: ClientProxy
  ) {}
  /**
   * @param user The user, which includes the Mnemonic to sign transactions with. Also includes the walletId for the TokenWallet System.
   * @param dto Which object to persist.
   * @returns A token transaction
   */
  public createTransaction(user: User, dto: any): TokenTransactionDto {
    return {
      dto,
      mnemonic: user.mnemonic,
      tokenWalletId: user.tokenWalletId,
      role: user.resource_access.nest.roles[0],
    };
  }

  /**
   * @param user The user, which includes the Mnemonic to sign transactions with. Also includes the walletId for the TokenWallet System.
   * @param query What to Query for. This can be just a string such as a wallet address, or a Company Object for filter purposes.
   * @returns A token query
   */
  public createQuery(user: User, query: any): TokenQueryDto {
    return {
      wallet: user.wallet,
      mnemonic: user.mnemonic,
      query,
      tokenWalletId: user.tokenWalletId,
      role: user.resource_access.nest.roles[0],
    };
  }

  /**
   * Adds the necessary additional info required to persist a list of imports to the blockchain.
   * @param token a list of imports to be tokenized.
   * @returns A list of messages which can be consumed by the Cosmos/Tendermint API.
   */
  public async prepareToken(user: User, token: any[]): Promise<any[]> {
    const msgArray: any = [];
    for (const content of token) {
      content.creator = user.wallet;
      content.tokenWalletId = user.tokenWalletId;
      msgArray.push(content);
    }
    return msgArray;
  }

  /**
   * Prepares a token to fit certain requirements
   * @param user - User from which the request is sent
   * @param token - Token which has to be prepared
   * @param creatorNameCache - A cache of company names
   * @returns A prepared token
   */
  public async prepareTransportData(
    user: User,
    token: ExportTokenDTO | ImportTokenDTO,
    creatorNameCache: Map<string, string> = new Map<string, string>()
  ): Promise<ExportTokenDTO | ImportTokenDTO> {
    const ownCompany: Company = await firstValueFrom(
      this.compService.send(this.USER_BY_ID, user.sub)
    );

    for (const event of token.events) {
      let creatorName = creatorNameCache.get(event.creator);
      if (creatorName === undefined) {
        try {
          const otherUser: User = await firstValueFrom(
            this.authService.send(this.USER_BY_COSMOS, event.creator)
          );
          const otherCompany: Company = await firstValueFrom(
            this.compService.send(this.COMPANY_BY_USER_ID, otherUser.id)
          );
          creatorName =
            ownCompany.customsId == otherCompany.customsId
              ? otherUser.username
              : otherCompany.name;
          creatorNameCache.set(event.creator, creatorName);
        } catch {
          creatorNameCache.set(event.creator, 'unknown');
        }
      }
      event.creatorName = creatorName;
    }

    return token;
  }

  /**
   * Prepares multiple tokens like prepareTransportData
   * @param user - User from which the request is sent
   * @param tokens - List of tokens to prepare
   * @returns Prepared tokens
   */
  public async prepareMultipleTransportData(
    user: User,
    tokens: ExportTokenDTO[] | ImportTokenDTO[]
  ): Promise<ExportTokenDTO[] | ImportTokenDTO[]> {
    const creatorNameCache = new Map<string, string>();
    const out = [];
    for (const transport of tokens) {
      out.push(
        await this.prepareTransportData(user, transport, creatorNameCache)
      );
    }
    return out;
  }
  /**
   * Adds the necessary additional info required to append events to their corresponding export token.
   * At this point limited for export events.
   * @param wallet
   * @param token a list events to be tokenized.
   * @returns A list of messages which can be consumed by the Cosmos/Tendermint API.
   */
  public async prepareEvent(
    wallet: string,
    token: ExportTokenDTO[] | ImportTokenDTO[]
  ): Promise<EventDto[]> {
    const eventList: EventDto[] = [];

    for (const content of token) {
      const msgCreateExportEvent: EventDto = {
        creator: wallet,
        message: '',
        eventType: content.eventType,
        index: content.id,
        status: content.status,
      };
      eventList.push(msgCreateExportEvent);
    }
    return eventList;
  }

  /**
   * Gets the cosmos address via a mnemonic
   * @param mnemonic - A mnemonic to derive a cosmos address
   * @returns A cosmos address
   */
  public async getCosmosAddress(mnemonic: string): Promise<string> {
    const [creatorAddress] = await (
      await DirectSecp256k1HdWallet.fromMnemonic(mnemonic)
    ).getAccounts();

    return creatorAddress.address;
  }
}

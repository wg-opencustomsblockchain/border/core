/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { APP_GUARD } from '@nestjs/core';
import { AuthGuard, KeycloakConnectModule, ResourceGuard, RoleGuard } from 'nest-keycloak-connect';

import { ExportModule } from './export/export.module';
import { ImportModule } from './import/import.module';
import { TokenModule } from './token/token.module';

@Module({
  imports: [
    ExportModule,
    ImportModule,
    TokenModule,

    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: ['.env'],
    }),
    KeycloakConnectModule.register({
      authServerUrl: process.env.KEYCLOAK_URL,
      realm: process.env.KEYCLOAK_REALM,
      clientId: process.env.KEYCLOAK_CONTROLLER_CLIENT_ID,
      secret: process.env.KEYCLOAK_CONTROLLER_SECRET,
      useNestLogger: true,
    }),
  ],

  controllers: [],
  providers: [
    {
      provide: APP_GUARD,
      useClass: AuthGuard,
    },
    {
      provide: APP_GUARD,
      useClass: ResourceGuard,
    },
    {
      provide: APP_GUARD,
      useClass: RoleGuard,
    },
  ],
})
export class AppModule {}

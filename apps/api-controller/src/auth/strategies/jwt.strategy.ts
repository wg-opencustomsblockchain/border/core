/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';

/** Basic JWT Authentication strategy to secure the application endpoints. */
@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  /** Create a Strategy with a new JWT Token*/
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: process.env.JWT_SECRET,
    });
  }

  /** Basic validation functionality. */
  async validate(payload: any) {
    return { userId: payload.sub, username: payload.username };
  }
}

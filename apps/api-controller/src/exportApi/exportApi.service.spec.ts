/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { AmqpClientEnum, UserRole } from '@border/api-interfaces';
import { DynamicModule } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { Test, TestingModule } from '@nestjs/testing';
import { of } from 'rxjs';

import { SocketService } from '../socket/socket.service';

import { EADDto } from './models/ead.dto';

import { User } from '@border/api-interfaces';
import { CompanyDto } from '@border/api-interfaces/lib/entities/token/company.dto';
import { MQBroker } from '@border/broker';
import { ExportApiService } from './exportApi.service';

describe('BlockchainConnectorService', () => {
  let service: ExportApiService;
  let dataService: ClientProxy;
  let authClient: ClientProxy;
  let compClient: ClientProxy;
  const mqBroker: DynamicModule = new MQBroker().getMsgBroker();
  const mockUser: User = {
    resource_access: { nest: { roles: [UserRole.EXPORTER] } },
  };

  const ead1 = new EADDto();

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [mqBroker],
      providers: [
        ExportApiService,
        {
          provide: SocketService,
          useValue: {
            socket: {
              emit() {
                console.log('Emitting');
              },
            },
          },
        },
      ],
    }).compile();

    service = module.get<ExportApiService>(ExportApiService);
    dataService = module.get<ClientProxy>(AmqpClientEnum.AMQP_CLIENT_DATA);
    authClient = module.get<ClientProxy>(AmqpClientEnum.AMQP_CLIENT_AUTH);
    compClient = module.get<ClientProxy>(AmqpClientEnum.AMQP_CLIENT_COMPANY);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('getEad', () => {
    it('should find an EAD by ID', async () => {
      const spy = jest.spyOn(dataService, 'send').mockReturnValue(of([ead1]));
      await service.getAllEAD(mockUser);
      expect(spy).toBeCalled();
    });
  });

  describe('getAllEAD', () => {
    it('should find all eads', async () => {
      const ead1 = new EADDto();
      ead1.events = [
        {
          creator: '...',
          status: '...',
          message: '...',
          index: '...',
          eventType: '...',
        },
      ];
      const spy = jest.spyOn(dataService, 'send').mockReturnValue(of([ead1]));
      jest.spyOn(authClient, 'send').mockReturnValue(of(new CompanyDto()));
      jest.spyOn(compClient, 'send').mockReturnValue(of(mockUser));
      await service.getAllEAD(mockUser);
      expect(spy).toBeCalled();
    });
  });
});

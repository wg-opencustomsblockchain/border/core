/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { User, UserRole } from '@border/api-interfaces';
import { DynamicModule } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';

import { FieldInformationDto } from '@border/api-interfaces/lib/entities/history/history.dto';
import { SocketService } from '../socket/socket.service';

import { EADDto } from './models/ead.dto';
import { MQBroker } from '@border/broker';
import { ExportApiService } from './exportApi.service';
import { ExportApiController } from './exportApi.controller';

export const HISTORY_RESPONSE_MOCK: FieldInformationDto[] = [
  {
    isEdited: true,
    lastEditor: 'TEST USER',
    fieldName: 'TEST',
    level: 'company',
  },
];

describe('BlockchainConnectorController', () => {
  let controller: ExportApiController;
  let service: ExportApiService;

  const mockUser: User = {
    resource_access: { nest: { roles: [UserRole.EXPORTER] } },
  };
  const mqBroker: DynamicModule = new MQBroker().getMsgBroker();
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [mqBroker],
      providers: [
        ExportApiService,
        {
          provide: SocketService,
          useValue: {
            socket: {
              emit() {
                console.log('Emitting');
              },
            },
          },
        },
      ],
      controllers: [ExportApiController],
    }).compile();

    mockUser.mnemonic = 'MOCK';
    mockUser.wallet = 'MOCK';
    controller = module.get<ExportApiController>(ExportApiController);
    service = module.get<ExportApiService>(ExportApiService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(controller).toBeDefined();
  });

  describe('getAllEAD', () => {
    it('should return a list of eads', async () => {
      const ead1 = new EADDto();
      ead1.exportId = '#1';

      const ead2 = new EADDto();
      ead2.exportId = '#2';

      jest
        .spyOn(service, 'getAllEAD')
        .mockReturnValue(Promise.resolve([ead1, ead2]));
      expect(await controller.getAllEAD(mockUser)).toHaveLength(2);
    });
  });

  describe('getEAD', () => {
    it('should return a list of eads', async () => {
      const ead1 = new EADDto();
      ead1.exportId = '#1';

      jest.spyOn(service, 'getEAD').mockReturnValue(Promise.resolve(ead1));
      expect(await controller.getEAD(mockUser, '#1')).toEqual(ead1);
    });
  });

  describe('createExportToken', () => {
    it('should create an ead', async () => {
      const ead1 = new EADDto();
      ead1.exportId = '#1';

      const spy = jest
        .spyOn(service, 'createExportToken')
        .mockReturnValue(Promise.resolve(ead1));
      await controller.createExportToken(mockUser, [ead1]);
      expect(spy).toBeCalled();
    });
  });

  describe('updateToken', () => {
    it('should update a token', async () => {
      const ead1 = new EADDto();
      ead1.exportId = '#1';

      const spy = jest
        .spyOn(service, 'updateExportToken')
        .mockReturnValue(Promise.resolve(ead1));
      await controller.updateToken(mockUser, [ead1]);
      expect(spy).toBeCalled();
    });
  });

  describe('getUserEADs', () => {
    it('should get ead for the user', async () => {
      const ead1 = new EADDto();
      const spy = jest
        .spyOn(service, 'getUserEADs')
        .mockReturnValue(Promise.resolve([ead1]));
      await controller.getUserEADs('user');
      expect(spy).toBeCalled();
    });
  });

  describe('findImportEADs', () => {
    it('should find eads for the user', async () => {
      const ead1 = new EADDto();
      const spy = jest
        .spyOn(service, 'getImportEADs')
        .mockReturnValue(Promise.resolve([ead1]));
      await controller.findImportEADs('user');
      expect(spy).toBeCalled();
    });
  });

  describe('getTokenHistory', () => {
    it('should find eads for the user', async () => {
      const ead1 = new EADDto();
      const spy = jest
        .spyOn(service, 'getTokenHistory')
        .mockReturnValue(Promise.resolve(HISTORY_RESPONSE_MOCK));
      await controller.getTokenHistory(mockUser, 'TEST');
      expect(spy).toBeCalled();
    });
  });

  describe('getArchivedEADs', () => {
    it('should find archived eads', async () => {
      const ead1 = new EADDto();
      const spy = jest
        .spyOn(service, 'getArchivedEADs')
        .mockReturnValue(Promise.resolve([ead1]));
      await controller.getArchivedEADs(mockUser);
      expect(spy).toBeCalled();
    });
  });
});

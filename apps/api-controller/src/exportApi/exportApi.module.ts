/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { DynamicModule, Module } from '@nestjs/common';
import { MQBroker } from '@border/api-interfaces/lib/broker';

import { SocketModule } from '../socket/socket.module';
import { ExportApiController } from './exportApi.controller';
import { ExportApiService } from './exportApi.service';

const mqBroker: DynamicModule = new MQBroker().getMsgBroker();
/** Module definitions for all modules relating to exporttoken functionalities. */
@Module({
  imports: [mqBroker, SocketModule],
  controllers: [ExportApiController],
  providers: [ExportApiService, ExportApiController],
  exports: [ExportApiService],
})
export class ExportApiModule {}

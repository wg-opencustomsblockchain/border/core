/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ApiProperty } from '@nestjs/swagger';
import { EventDto } from '@border/api-interfaces/lib/dto/event.dto';
import { CompanyDto } from '@border/api-interfaces/lib/entities/token/company.dto';
import { CustomsOfficeDto } from '@border/api-interfaces/lib/entities/token/customs-office.dto';
import { GoodItemDto } from '@border/api-interfaces/lib/entities/token/good-item.dto';
import { TransportDto } from '@border/api-interfaces/lib/entities/token/transport.dto';

/** Ead DTO Definitions with Swagger API Properties. */
export class EADDto {
  @ApiProperty() creator: string;
  @ApiProperty() id: string;
  @ApiProperty() exporter: CompanyDto;
  @ApiProperty() declarant: CompanyDto;
  @ApiProperty() representative: CompanyDto;
  @ApiProperty() consignee: CompanyDto;
  @ApiProperty() customOfficeOfExit: CustomsOfficeDto;
  @ApiProperty() customOfficeOfExport: CustomsOfficeDto;
  @ApiProperty() goodsItems: GoodItemDto[];
  @ApiProperty() transportToBorder: TransportDto;
  @ApiProperty() transportAtBorder: TransportDto;

  @ApiProperty() exportId: string;
  @ApiProperty() localReferenceNumber: string;
  @ApiProperty() destinationCountry: string;
  @ApiProperty() exportCountry: string;
  @ApiProperty() releaseDateAndTime: string;
  @ApiProperty() itinerary: string;
  @ApiProperty() incotermCode: string;
  @ApiProperty() incotermLocation: string;
  @ApiProperty() totalGrossMass: string;
  @ApiProperty() goodsItemQuantity: number;
  @ApiProperty() totalPackagesQuantity: number;
  @ApiProperty() natureOfTransaction: string;
  @ApiProperty() totalAmountInvoiced: string;
  @ApiProperty() invoiceCurrency: string;

  @ApiProperty() status: string;
  @ApiProperty() user: string;
  @ApiProperty() timestamp: string;

  @ApiProperty() events: EventDto[];
  @ApiProperty() tokenWalletId: string;
  @ApiProperty() eventType: string;
}

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Logger,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { AuthenticatedUser, Roles } from 'nest-keycloak-connect';
import { BorderApi, UserRole } from '@border/api-interfaces';

import { FieldInformationDto } from '@border/api-interfaces/lib/entities/history/history.dto';
import { EADDto } from './models/ead.dto';
import { ExportApiService } from './exportApi.service';

/** Controller that serves all functionalities related to writing/reading Export Token. */
@ApiTags(BorderApi.EXPORT_CONTROLLER_API_TAG)
@Controller(`${BorderApi.EXPORT_API_VERSION}/${BorderApi.EXPORT_CONTROLLER}`)
export class ExportApiController {
  /** Creates a new Controller and injects the required services. */
  constructor(private readonly exportApiService: ExportApiService) {}
  /**
   * Queries for all export token on the Chain.
   * @returns A List of EAD-DTOs.
   */
  @Get(BorderApi.EXPORT_GET_ALL)
  @ApiOperation({})
  @Roles({ roles: [UserRole.EXPORTER] })
  public async getAllEAD(@AuthenticatedUser() user: any): Promise<EADDto[]> {
    return this.exportApiService.getAllEAD(user);
  }

  /**
   * Queries for a token by its id.
   * @param id - The id under which an export process can be identified.
   * @returns The corresponding token to the id.
   */
  @Get(`/:id`)
  @ApiOperation({})
  @Roles({ roles: [UserRole.EXPORTER, UserRole.DRIVER, UserRole.IMPORTER] })
  public async getEAD(
    @AuthenticatedUser() user: any,
    @Param('id') id: string
  ): Promise<EADDto> {
    return this.exportApiService.getEAD(user, id);
  }

  /**
   * Queries for all archived export token
   * @returns A list of EAD-DTOs which have the Export status "Archived"
   */
  @Get(BorderApi.EXPORT_GET_ARCHIVED)
  @ApiOperation({})
  @Roles({ roles: [UserRole.EXPORTER] })
  public async getArchivedEADs(
    @AuthenticatedUser() user: any
  ): Promise<EADDto[]> {
    try {
      return await this.exportApiService.getArchivedEADs(user);
    } catch (e) {
      Logger.error(e);
      throw new HttpException(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Writes a token to the chain via the BlockchainConnector.
   * @param ead - The EAD to be tokenized.
   * @returns - The export token.
   */
  @Post(BorderApi.EXPORT_CREATE)
  @ApiOperation({})
  @Roles({ roles: [UserRole.EXPORTER] })
  public async createExportToken(
    @AuthenticatedUser() user: any,
    @Body() ead: EADDto[]
  ): Promise<EADDto> {
    return this.exportApiService.createExportToken(user, ead);
  }

  /**
   * Updates the current status of the export token.
   * @param ead - Token to be updated. Only status may be changed.
   * @returns The updated token
   */
  @Put(BorderApi.EXPORT_UPDATE)
  @ApiOperation({})
  @Roles({ roles: [UserRole.EXPORTER] })
  public async updateToken(
    @AuthenticatedUser() user: any,
    @Body() ead: EADDto[]
  ): Promise<EADDto> {
    try {
      return await this.exportApiService.updateExportToken(user, ead);
    } catch (e) {
      Logger.error(e);
      throw new HttpException(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Retrieves all export token which belong to a user (wallet).
   * @param user - The Wallet address to be used for the query.
   * @returns - A list of export token which belong to the user.
   */
  @Get(`${BorderApi.EXPORT_GET_USER}/:user`)
  @ApiOperation({})
  @Roles({ roles: [UserRole.EXPORTER, UserRole.DRIVER] })
  public async getUserEADs(@AuthenticatedUser() user: any): Promise<EADDto[]> {
    try {
      return await this.exportApiService.getUserEADs(user);
    } catch (e) {
      Logger.error(e);
      throw new HttpException(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Retrieves a list of all export token which are ready to be transferred into an import token.
   * @param user - The Addressee of the export as a wallet address
   * @returns all export token ready for import.
   */
  @Get(`${BorderApi.EXPORT_GET_IMPORT_READY}/:user`)
  @ApiOperation({})
  @Roles({ roles: [UserRole.IMPORTER] })
  public async findImportEADs(
    @AuthenticatedUser() user: any
  ): Promise<EADDto[]> {
    try {
      return await this.exportApiService.getImportEADs(user, user.company);
    } catch (e) {
      Logger.error(e);
      throw new HttpException(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Retrieves any changes done to an export token and by whom.
   * @param user - Which user is requesting the history data
   * @param id - Id of the token
   * @returns An array of Information of the token
   */
  @Get(`:id/${BorderApi.EXPORT_GET_HISTORY}`)
  @Roles({ roles: [UserRole.EXPORTER, UserRole.IMPORTER] })
  public getTokenHistory(
    @AuthenticatedUser() user: any,
    @Param('id') id: string
  ): Promise<FieldInformationDto[]> {
    return this.exportApiService.getTokenHistory(user, id);
  }
}

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {
  Address,
  AmqpClientEnum,
  DataMqPattern,
  ExportMQPatterns,
  HistoryMqPattern,
  User,
} from '@border/api-interfaces';
import { FieldInformationDto } from '@border/api-interfaces/lib/entities/history/history.dto';
import { Inject, Injectable, Logger } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { firstValueFrom } from 'rxjs';
import { SocketService } from '../socket/socket.service';
import { EADDto } from './models/ead.dto';

/**
 * Service class which communicates with an MQ broker and accesses resources from our microservices.
 * All functionality relating to ead import and export token read/write.
 */
@Injectable()
export class ExportApiService {
  //** Instantiate a NestJS Logger */
  private readonly logger = new Logger(ExportApiService.name);

  /** Creates an instance and injects an AMQP client to communicate with the broker. */
  constructor(
    @Inject(AmqpClientEnum.AMQP_CLIENT_BLOCKCHAIN)
    public readonly blockchainService: ClientProxy,
    @Inject(AmqpClientEnum.AMQP_CLIENT_HISTORY)
    public readonly historyService: ClientProxy,
    @Inject(AmqpClientEnum.AMQP_CLIENT_DATA)
    public readonly dataService: ClientProxy,

    private readonly socketService: SocketService
  ) {}

  /**
   * Queries all export token.
   * @returns - A list of all export token.
   */
  public async getAllEAD(user: User): Promise<EADDto[]> {
    return await firstValueFrom(
      this.dataService.send(
        DataMqPattern.PREFIX + DataMqPattern.DATA_EXPORT_ALL,
        { user: user }
      )
    );
  }

  /**
   * Queries for a single export token.
   * @param id - The uuid of the token.
   * @returns - A token corresponding to the mrn.
   */
  public async getEAD(user: User, id: string): Promise<EADDto> {
    return firstValueFrom(
      this.dataService.send(
        DataMqPattern.PREFIX + DataMqPattern.DATA_EXPORT_FIND,
        { user: user, id: id }
      )
    );
  }

  public async queryTokenHistory(id: string) {
    return firstValueFrom(
      this.historyService.send(
        ExportMQPatterns.MQ_API_TAG + ExportMQPatterns.TOKEN_FETCH_HISTORY,
        { id, mnemonic: process.env.BROKER_MNEMONIC }
      )
    );
  }

  /**
   * Sends a creation request to the Blockchain Connector.
   * @param user
   * @param ead - The EAD to be tokenized.
   * @returns - The export token which holds the EAD.
   */
  public async createExportToken(user: User, dto: EADDto[]): Promise<EADDto> {
    return await firstValueFrom(
      this.dataService.send(
        DataMqPattern.PREFIX + DataMqPattern.DATA_EXPORT_CREATE,
        { user: user, dto: dto }
      )
    );
  }

  /**
   * Sends an update request to the Blockchain Connector.
   * @param user
   * @param ead - The token to be updated.
   * @returns - The updated token.
   */
  public async updateExportToken(user: User, dto: EADDto[]): Promise<EADDto> {
    this.socketService.socket.emit('ead-update', { dto });
    return await firstValueFrom(
      this.dataService.send(
        DataMqPattern.PREFIX + DataMqPattern.DATA_EXPORT_UPDATE,
        { user: user, dto: dto }
      )
    );
  }

  /**
   * Queries for all token belonging to a User
   * @param user - The user wallet address.
   * @returns - The list of token belonging to the address.
   */
  public async getUserEADs(user: User): Promise<EADDto[]> {
    return await firstValueFrom(
      this.dataService.send(
        DataMqPattern.PREFIX + DataMqPattern.DATA_EXPORT_USER,
        { user: user }
      )
    );
  }

  /**
   * Queries for all token with the status "Archived"-
   * @returns - All Archived token.
   */
  public async getArchivedEADs(user: User): Promise<EADDto[]> {
    return await firstValueFrom(
      this.dataService.send(
        DataMqPattern.PREFIX + DataMqPattern.DATA_EXPORT_ARCHIVED,
        { user: user }
      )
    );
  }

  /**
   * Queries for all export token which are ready to be accepted as an import token.
   * @param user - The Addressee of the import process as a wallet address.
   * @param address
   * @returns - A list of ready for import export token.
   */
  public async getImportEADs(user: User, address: Address): Promise<EADDto[]> {
    return await firstValueFrom(
      this.dataService.send(
        DataMqPattern.PREFIX + DataMqPattern.DATA_EXPORT_IMPORTS,
        {
          user: user,
          address: address,
        }
      )
    );
  }

  /**
   * Queries the History of a Token
   * @param user - Which user is requesting the history data
   * @param id - Id of the token
   * @returns An array of Information of the token
   */
  public async getTokenHistory(
    user: User,
    id: string
  ): Promise<FieldInformationDto[]> {
    return firstValueFrom(
      this.historyService.send(
        HistoryMqPattern.PREFIX + HistoryMqPattern.GET_HISTORY,
        { id, user }
      )
    );
  }
}

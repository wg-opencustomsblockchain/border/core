/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { INestApplication, Logger, LogLevel } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, OpenAPIObject, SwaggerModule } from '@nestjs/swagger';
import { BorderApi } from '@border/api-interfaces';

import { AppModule } from './app.module';
import { Config } from './config/config';

/**
 * Main entrypoint of the Application.
 * Sets up both MQ communication and defines the port to listen on.
 */
async function bootstrap() {
  const app: INestApplication = await NestFactory.create(AppModule, {
    logger:
      process.env.LOG_LEVEL === 'dev'
        ? (process.env.LOG_SETTINGS_DEV.split(',') as LogLevel[])
        : (process.env.LOG_SETTINGS_PROD.split(',') as LogLevel[]),
  });
  app.setGlobalPrefix(BorderApi.GLOBAL_PREFIX);

  /** Swagger Documentation Setup */
  const config: Pick<
    OpenAPIObject,
    'openapi' | 'info' | 'servers' | 'security' | 'tags' | 'externalDocs'
  > = new DocumentBuilder()
    .setTitle(Config.APPLICATION_TITLE)
    .setDescription(Config.APPLICATION_DESCRIPTION)
    .setVersion(Config.APPLICATION_VERSION)
    .build();
  const document: OpenAPIObject = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup(Config.SWAGGER_PATH, app, document);
  app.enableCors();
  const port = process.env.DATA_PORT || 8081;
  const logger = new Logger('DATA-CONTROLLER', {});
  await app.listen(port, () =>
    logger.log(
      `🚀 - Server started at port ${port}. Open API: http://localhost:${port}/api/`
    )
  );
}
bootstrap();

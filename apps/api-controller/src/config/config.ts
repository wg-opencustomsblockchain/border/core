/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/** Basic Configuration settings. */
export enum Config {
  // Application configuration
  APPLICATION_TITLE = 'border.backend.data.controller',
  APPLICATION_DESCRIPTION = 'border - Blockchainbasierte Organisation relevanter Dokumente im Extrahandel mit Rechtssicherheit',
  APPLICATION_VERSION = '1.0',
  SWAGGER_PATH = 'api',
}

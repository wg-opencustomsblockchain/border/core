/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { DynamicModule } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { MQBroker } from '@border/broker';

import { User } from '@border/api-interfaces';
import { UserRole } from '@border/api-interfaces';
import { SocketService } from '../socket/socket.service';
import { ImportDTO } from './model/import.dto';
import { ImportApiController } from './importApi.controller';
import { ImportApiService } from './importApi.service';

describe('BlockchainConnectorController', () => {
  let controller: ImportApiController;
  let service: ImportApiService;
  const mqBroker: DynamicModule = new MQBroker().getMsgBroker();
  const mockUser: User = {
    resource_access: { nest: { roles: [UserRole.EXPORTER] } },
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [mqBroker],
      providers: [
        ImportApiService,
        {
          provide: SocketService,
          useValue: {
            socket: {
              emit() {
                console.log('Emitting');
              },
            },
          },
        },
      ],
      controllers: [ImportApiController],
    }).compile();

    mockUser.mnemonic = 'MOCK';
    mockUser.wallet = 'MOCK';

    controller = module.get<ImportApiController>(ImportApiController);
    service = module.get<ImportApiService>(ImportApiService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(controller).toBeDefined();
  });

  describe('findImports', () => {
    it('should return a list of imports', async () => {
      const import1 = new ImportDTO();
      import1.relatedExportToken = '#1';

      const import2 = new ImportDTO();
      import2.relatedExportToken = '#2';

      jest
        .spyOn(service, 'findImports')
        .mockReturnValue(Promise.resolve([import1, import2]));
      expect(await controller.findImports(mockUser)).toHaveLength(2);
    });

    it('should throw an exception', async () => {
      jest
        .spyOn(service, 'findImports')
        .mockReturnValue(Promise.reject('some error'));
      expect(controller.findImports(mockUser)).rejects.toThrow();
    });
  });

  // Failing
  describe('findImportById', () => {
    it('should return an import identified by id', async () => {
      const import1 = new ImportDTO();
      import1.relatedExportToken = '#1';

      jest
        .spyOn(service, 'findImportById')
        .mockReturnValue(Promise.resolve(import1));
      expect(await controller.findImportByTokenId(mockUser, '#1')).toEqual(
        import1
      );
    });

    it('should throw an exception', async () => {
      jest
        .spyOn(service, 'findImportById')
        .mockReturnValue(Promise.reject('some error'));
      expect(controller.findImportByTokenId(mockUser, '#1')).rejects.toThrow();
    });
  });

  describe('createImport', () => {
    it('should create an import', async () => {
      const import1 = new ImportDTO();
      import1.relatedExportToken = '#1';

      const spy = jest
        .spyOn(service, 'createImport')
        .mockReturnValue(Promise.resolve([import1]));
      await controller.createImport(mockUser, [import1]);
      expect(spy).toBeCalled();
    });

    it('should throw an exception', async () => {
      const import1 = new ImportDTO();
      jest
        .spyOn(service, 'createImport')
        .mockReturnValue(Promise.reject('some error'));
      expect(controller.createImport(mockUser, [import1])).rejects.toThrow();
    });
  });

  describe('updateImport', () => {
    it('should update an import', async () => {
      const import1 = new ImportDTO();
      import1.relatedExportToken = '#1';

      const spy = jest
        .spyOn(service, 'updateImport')
        .mockReturnValue(Promise.resolve(import1));
      await controller.updateImport(mockUser, [import1]);
      expect(spy).toBeCalled();
    });

    it('should throw an exception', async () => {
      jest
        .spyOn(service, 'updateImport')
        .mockReturnValue(Promise.reject('some error'));
      expect(
        controller.updateImport(mockUser, [new ImportDTO()])
      ).rejects.toThrow();
    });
  });

  describe('findImportByUser', () => {
    it('should get imports for the user', async () => {
      const import1 = new ImportDTO();
      const spy = jest
        .spyOn(service, 'findImportByUser')
        .mockReturnValue(Promise.resolve([import1]));
      await controller.findImportByUser(mockUser, 'user');
      expect(spy).toBeCalled();
    });

    it('should throw an exception', async () => {
      jest
        .spyOn(service, 'findImportByUser')
        .mockReturnValue(Promise.reject('some error'));
      expect(controller.findImportByUser(mockUser, 'user')).rejects.toThrow();
    });
  });
});

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {
  AmqpClientEnum,
  CompanyAMQPatterns,
  User,
  UserMsgPatterns,
} from '@border/api-interfaces';
import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { firstValueFrom } from 'rxjs';

import { DataMqPattern } from '@border/api-interfaces';
import { ImportDTO } from './model/import.dto';

/**
 * Service class which communicates with an MQ broker and accesses resources from our microservices.
 * All functionality relating to import token read/write.
 */
@Injectable()
export class ImportApiService {
  private COMPANY_BY_USER_ID =
    CompanyAMQPatterns.API_TAG +
    CompanyAMQPatterns.API_VERSION +
    CompanyAMQPatterns.GET_ONE_BY_ID;
  private USER_BY_COSMOS =
    UserMsgPatterns.API_TAG +
    UserMsgPatterns.API_VERSION +
    UserMsgPatterns.GET_ONE_BY_COSMOS;
  private USER_BY_ID =
    CompanyAMQPatterns.API_TAG +
    CompanyAMQPatterns.API_VERSION +
    CompanyAMQPatterns.GET_ONE_BY_ID;

  /** Creates an instance and injects an MQ blockchainClient to communicate with the broker. */
  constructor(
    @Inject(AmqpClientEnum.AMQP_CLIENT_BLOCKCHAIN)
    public readonly blockchainClient: ClientProxy,
    @Inject(AmqpClientEnum.AMQP_CLIENT_AUTH)
    public readonly authClient: ClientProxy,
    @Inject(AmqpClientEnum.AMQP_CLIENT_COMPANY)
    public readonly compClient: ClientProxy,
    @Inject(AmqpClientEnum.AMQP_CLIENT_DATA)
    public readonly dataClient: ClientProxy
  ) {}

  /**
   * Sends a creation request to the Blockchain Connector.
   * @param dto - The import to be tokenized.
   * @returns - The import token which holds the import.
   */
  public async createImport(
    user: User,
    dto: ImportDTO[]
  ): Promise<ImportDTO[]> {
    return await firstValueFrom(
      this.dataClient.send(
        DataMqPattern.PREFIX + DataMqPattern.DATA_IMPORT_CREATE,
        { user: user, dto: dto }
      )
    );
  }

  /**
   * Sends an update request to the Blockchain Connector.
   * An Import may be updated completely in contrast to an export.
   * @param ead - The token to be updated.
   * @returns - The updated token.
   */
  public async updateImport(user: User, dto: ImportDTO[]): Promise<ImportDTO> {
    return await firstValueFrom(
      this.dataClient.send(
        DataMqPattern.PREFIX + DataMqPattern.DATA_IMPORT_UPDATE,
        { user: user, dto: dto }
      )
    );
  }

  /**
   * Queries all import token.
   * @returns - A list of all export token.
   */
  public async findImports(user: User): Promise<ImportDTO[]> {
    return await firstValueFrom(
      this.dataClient.send(
        DataMqPattern.PREFIX + DataMqPattern.DATA_IMPORT_LIST,
        { user: user }
      )
    );
  }

  /**
   * Queries for a single import token. Uses token id.
   * @param id - The token id of the token.
   * @returns - A token corresponding to the token id.
   */
  public async findImportById(user: User, id: string): Promise<ImportDTO> {
    return firstValueFrom(
      this.dataClient.send(
        DataMqPattern.PREFIX + DataMqPattern.DATA_IMPORT_FIND,
        { user: user, id: id }
      )
    );
  }

  /**
   * Queries for a single import token. Uses import id.
   * @param mrn - The import id of the token.
   * @returns - A token corresponding to the import id.
   */
  public async findImportByImportId(
    user: User,
    mrn: string
  ): Promise<ImportDTO[]> {
    return firstValueFrom(
      this.dataClient.send(
        DataMqPattern.PREFIX + DataMqPattern.DATA_IMPORT_FIND_IMPORT,
        { user: user, mrn: mrn }
      )
    );
  }

  /**
   * Queries for all token belonging to a User
   * @param user - The user wallet address.
   * @returns - The list of token belonging to the address.
   */
  public async findImportByUser(
    user: User,
    wallet: string
  ): Promise<ImportDTO[]> {
    return firstValueFrom(
      this.dataClient.send(
        DataMqPattern.PREFIX + DataMqPattern.DATA_IMPORT_USER,
        { user: user, wallet: wallet }
      )
    );
  }
}

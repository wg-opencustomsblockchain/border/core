/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { DynamicModule } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { User, UserRole } from '@border/api-interfaces';
import { MQBroker } from '@border/broker';

import { SocketService } from '../socket/socket.service';

import { ImportDTO } from './model/import.dto';
import { ImportApiService } from './importApi.service';
import { ImportApiController } from './importApi.controller';

describe('BlockchainConnectorService', () => {
  let service: ImportApiService;
  let controller: ImportApiController;
  const mqBroker: DynamicModule = new MQBroker().getMsgBroker();
  const mockUser: User = {
    resource_access: { nest: { roles: [UserRole.EXPORTER] } },
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [mqBroker],
      providers: [
        ImportApiService,
        {
          provide: SocketService,
          useValue: {
            socket: {
              emit() {
                console.log('Emitting');
              },
            },
          },
        },
      ],
      controllers: [ImportApiController],
    }).compile();

    controller = module.get<ImportApiController>(ImportApiController);
    service = module.get<ImportApiService>(ImportApiService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(controller).toBeDefined();
  });

  describe('createImport', () => {
    it('should create an ead', async () => {
      const mock = new ImportDTO();
      mock.importId = '#1';

      const spy = jest
        .spyOn(service, 'createImport')
        .mockReturnValue(Promise.resolve([mock]));
      await controller.createImport(mockUser, [mock]);
      expect(spy).toBeCalled();
    });
  });

  describe('updateImport', () => {
    it('should update an ead', async () => {
      const mock = new ImportDTO();
      mock.importId = '#1';

      const spy = jest
        .spyOn(service, 'updateImport')
        .mockReturnValue(Promise.resolve(mock));
      await controller.updateImport(mockUser, [mock]);
      expect(spy).toBeCalled();
    });
  });
});

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Logger,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';

import { AuthenticatedUser } from 'nest-keycloak-connect';
import { BorderApi } from '@border/api-interfaces';
import { ImportDTO } from './model/import.dto';
import { ImportApiService } from './importApi.service';

/** Controller that serves all functionalities related to writing/reading Import Token. */
@ApiTags(BorderApi.EXPORT_CONTROLLER_API_TAG)
@Controller(`${BorderApi.EXPORT_API_VERSION}/${BorderApi.IMPORT_CONTROLLER}`)
export class ImportApiController {
  /** Creates a new Controller and injects the required services. */
  constructor(private readonly blockchainConnectorService: ImportApiService) {}

  /**
   * Writes a token to the chain via the BlockchainConnector.
   * @param dto - The import to be tokenized.
   * @returns - The import token.
   */
  @Post(BorderApi.IMPORT_CREATE)
  @ApiOperation({})
  public async createImport(
    @AuthenticatedUser() user: any,
    @Body() dto: ImportDTO[]
  ): Promise<ImportDTO[]> {
    try {
      return await this.blockchainConnectorService.createImport(user, dto);
    } catch (e) {
      Logger.error(e);
      throw new HttpException(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Updates the current status of the import token.
   * @param dto - Token to be updated. Only status may be changed.
   * @returns The updated token
   */
  @Put(BorderApi.IMPORT_UPDATE)
  @ApiOperation({})
  public async updateImport(
    @AuthenticatedUser() user: any,
    @Body() dto: ImportDTO[]
  ): Promise<ImportDTO> {
    try {
      return await this.blockchainConnectorService.updateImport(user, dto);
    } catch (e) {
      Logger.error(e);
      throw new HttpException(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Queries for all import token on the Chain.
   * @returns A List of EAD-DTOs.
   */
  @Get(BorderApi.IMPORT_GET_ALL)
  @ApiOperation({})
  public async findImports(
    @AuthenticatedUser() user: any
  ): Promise<ImportDTO[]> {
    try {
      return await this.blockchainConnectorService.findImports(user);
    } catch (e) {
      Logger.error(e);
      throw new HttpException(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Queries for a token by its token id.
   * @param id - The id under which the import token can be found.
   * @returns The corresponding token to the import token id.
   */
  @Get(BorderApi.IMPORT_GET)
  @ApiOperation({})
  public async findImportByTokenId(
    @AuthenticatedUser() user: any,
    @Param('id') id: string
  ): Promise<ImportDTO> {
    try {
      return await this.blockchainConnectorService.findImportById(user, id);
    } catch (e) {
      Logger.error(e);
      throw new HttpException(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Retrieves all import token which belong to a user (wallet).
   * @param user - The Wallet address to be used for the query.
   * @returns - A list of import token which belong to the user.
   */
  @Get(`${BorderApi.IMPORT_GET_USER}/:user`)
  @ApiOperation({})
  public async findImportByUser(
    @AuthenticatedUser() user: any,
    @Param('user') wallet: string
  ): Promise<ImportDTO[]> {
    try {
      return await this.blockchainConnectorService.findImportByUser(
        user,
        wallet
      );
    } catch (e) {
      Logger.error(e);
      throw new HttpException(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}

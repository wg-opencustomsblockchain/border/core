/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { MessageBody, OnGatewayInit, SubscribeMessage, WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { Server } from 'socket.io';
import { SocketService } from './socket.service';

@WebSocketGateway({
  cors: true,
})
//** Basic Websocket setup */
export class AppGateway implements OnGatewayInit {
  constructor(private socketService: SocketService) {}

  //** The Websocket server */
  @WebSocketServer() public server: Server;

  /** Create the websocket server after initialization */
  afterInit(server: Server) {
    this.socketService.socket = server;
  }

  @SubscribeMessage('gate')
  handleGateEvent(@MessageBody() data: string): void {
    this.socketService.socket.emit('gate', data);
  }
}

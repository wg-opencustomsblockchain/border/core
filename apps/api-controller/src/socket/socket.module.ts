/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Module } from '@nestjs/common';
import { AppGateway } from './socket.gateway';
import { SocketService } from './socket.service';

/** Module decription for the websocket module.  */
@Module({
  providers: [SocketService, AppGateway],
  exports: [SocketService, AppGateway],
})
export class SocketModule {}

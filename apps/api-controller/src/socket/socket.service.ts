/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@nestjs/common';
import { Server } from 'socket.io';

@Injectable()
/** Basic Socket Service */
export class SocketService {
  /** The Websocket Server */
  public socket: Server = null;
}

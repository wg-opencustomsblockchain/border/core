/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Test, TestingModule } from '@nestjs/testing';
import { Server } from 'socket.io';
import { AppGateway } from '../socket.gateway';
import { SocketModule } from '../socket.module';
import { SocketService } from '../socket.service';

describe('BlockchainConnectorService', () => {
  let service: AppGateway;
  let socketService: SocketService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [SocketModule],
      providers: [SocketService],
    }).compile();

    service = module.get<AppGateway>(AppGateway);
    socketService = module.get<SocketService>(SocketService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should set socket', () => {
    const server = new Server();
    service.afterInit(server);
    expect(socketService.socket).toEqual(server);
  });
});

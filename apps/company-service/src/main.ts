/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Logger, LogLevel } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { AmqpClientEnum } from '@border/api-interfaces';
import { ServiceModule } from './service.module';

/**
 * Main entrypoint of the Application.
 * Sets up both Message Queue communication and defines the HTTP port to listen on.
 */
async function bootstrap() {
  /** Basic Microservice options to serve a Message Queue Endpoint. */

  const app = await NestFactory.createMicroservice<MicroserviceOptions>(
    ServiceModule,
    {
      transport: Transport.RMQ,
      options: {
        urls: [process.env.MSG_QUEUE_URL],
        queue: AmqpClientEnum.AMQP_QUEUE_COMPANY,
        queueOptions: {
          durable: false,
        },
      },
      logger:
        process.env.LOG_LEVEL === 'dev'
          ? (process.env.LOG_SETTINGS_DEV.split(',') as LogLevel[])
          : (process.env.LOG_SETTINGS_PROD.split(',') as LogLevel[]),
    }
  );
  const logger = new Logger('COMPANY-SERVICE');
  app
    .listen()
    .then(() =>
      logger.log(
        `🚀 - Auth Company started with RMQ: ${process.env.MSG_QUEUE_URL}`
      )
    );
}
/**
 * Calling the startup process
 */
bootstrap();

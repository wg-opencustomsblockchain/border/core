/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Module } from '@nestjs/common';
import { CompanyService } from './company.service';
import { CompaniesController } from './company.controller';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { AmqpClientEnum } from '@border/api-interfaces';

@Module({
  providers: [CompanyService],
  exports: [CompanyService],
  controllers: [CompaniesController],
  imports: [
    ClientsModule.register([
      {
        name: AmqpClientEnum.AMQP_CLIENT_AUTH,
        transport: Transport.RMQ,
        options: {
          urls: [process.env.MSG_QUEUE_URL],
          queue: AmqpClientEnum.AMQP_QUEUE_AUTH,
          queueOptions: {
            durable: false,
          },
        },
      },
    ]),
  ],
})
export class CompaniesModule {}

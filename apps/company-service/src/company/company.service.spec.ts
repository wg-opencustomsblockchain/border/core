/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Company, User } from '@border/api-interfaces';
import { KeycloakService } from '@mschoenbo/nestjs-keycloak-admin';
import { DynamicModule } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { Test, TestingModule } from '@nestjs/testing';
import { of } from 'rxjs';
import { MQBroker } from '@border/broker';
import { CompanyService } from './company.service';

const user = {
  attributes: {
    wallet: ['cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462y'],
  },
  userId: 1,
  username: 'Erik',
  password: '$2a$10$MgivBg2HiInddrCTHopFU.olFiOe.hejTA7lBZgq361n.S3uJfDwG',
  role: 1,
  mnemonic:
    'link wild parent false local room leaf people couch drift yard heavy trap coral husband scorpion paper donor retire enrich visual donkey birth involve',
  wallet: 'cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462y',
  pubkey:
    'cosmospub1addwnpepqth4xx46xws36gnpmdgcslszsv8g7gnek5km3ugp4syvnfg4xvgxyx4t379',
  tokenWalletId: 'DE537400371045831',
  company: {
    name: 'Erik Export GmbH',
    address: {
      streetAndNumber: '',
      postcode: '',
      countryCode: '',
      city: '',
    },
  },
  id: '1',
} as User;
const company = {
  name: 'Erik Export GmbH',
  address: {
    streetAndNumber: '',
    postcode: '',
    countryCode: '',
    city: '',
  },
  users: ['1'],
} as Company;
const companies = new Map<string, Company>([[user.id, company]]);

describe('CompanyService', () => {
  let compService: CompanyService;
  let client: ClientProxy;
  const mqBroker: DynamicModule = new MQBroker().getMsgBroker();
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [mqBroker],
      providers: [
        {
          provide: KeycloakService,
          useValue: {
            client: {
              Companies: {
                // eslint-disable-next-line @typescript-eslint/no-unused-vars
                async find(_payload: { username: string }) {
                  return Promise.resolve([user]);
                },
              },
            },
          },
        },
        CompanyService,
      ],
    }).compile();

    compService = module.get<CompanyService>(CompanyService);
  });

  it('should be defined', () => {
    expect(compService).toBeDefined();
  });
});

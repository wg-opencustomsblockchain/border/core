/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {
  AmqpClientEnum,
  Company,
  User,
  UserMsgPatterns,
} from '@border/api-interfaces';
import { Inject, Injectable, Logger } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { firstValueFrom } from 'rxjs';

/**
 * A Service for handling Companies
 * Utilizes a simple map cache so that our service doesn't spam our resource too much
 * Cleans the cache after a minute to reflect database/keycloak changes.
 */
@Injectable()
export class CompanyService {
  /**Simple map Cache so that our service doesn't spam our resource too much */
  private companyCache: Map<string, Company>;
  private lastChecked: number;
  /**Clear the Cache after a set time to reflect changes and speed up presentations */
  private checkIntervalSeconds = 60;
  private readonly logger = new Logger(CompanyService.name);

  constructor(
    @Inject(AmqpClientEnum.AMQP_CLIENT_AUTH) private userService: ClientProxy
  ) {
    this.logger.log('Initialized Company Cache');
    this.companyCache = new Map<string, Company>();
    this.lastChecked = Date.now();
  }

  /**
   * Returns the Company of a given User identified by the user id.
   * @param id - The id to search for
   * @returns a user or null
   */
  public async findOneById(id: string): Promise<Company | undefined> {
    return await this.getCompanyFromIdCache(id);
  }

  /**
   * Returns the Company of a given User identified by the user wallet.
   * @param wallet - The wallet to search for
   * @returns a user or null
   */
  public async findOneByWallet(address: string): Promise<Company | undefined> {
    return await this.getCompanyFromWalletCache(address);
  }

  /**
   * Returns the Company of a given User identified by the wallet.
   * @param id
   * @returns a company
   */
  private async getCompanyFromIdCache(id: string): Promise<Company> {
    this.checkCacheTimer();
    let result = this.companyCache.get(id);
    if (!result) {
      const user = await firstValueFrom(
        this.userService.send<User>(
          UserMsgPatterns.API_TAG +
            UserMsgPatterns.API_VERSION +
            UserMsgPatterns.GET_ONE_BY_ID,
          id
        )
      );
      result = user.attributes.company[0];
      this.logger.verbose(`Cache miss - setting: ${id}-${result}`);
      this.companyCache.set(id, result);
    }

    return result;
  }

  /**
   * Returns the Company of a given User identified by the cosmos address.
   * @param address - The address to search for
   * @returns a company or undefined
   */
  private async getCompanyFromWalletCache(
    wallet: string
  ): Promise<Company | undefined> {
    this.checkCacheTimer();
    let result = this.companyCache.get(wallet);
    if (!result) {
      const user = await firstValueFrom(
        this.userService.send<User>(
          UserMsgPatterns.API_TAG +
            UserMsgPatterns.API_VERSION +
            UserMsgPatterns.GET_ONE_BY_COSMOS,
          wallet
        )
      );
      result = user.attributes.company[0];
      this.logger.verbose(`Cache miss - setting: ${wallet}-${result}`);
      this.companyCache.set(wallet, result);
    }
    return result;
  }

  /**
   * Checks how much time has passed and cleans the cache.
   * @returns true if the cache needed cleaning, false otherwise
   */
  private checkCacheTimer(): boolean {
    if ((Date.now() - this.lastChecked) / 1000 > this.checkIntervalSeconds) {
      this.lastChecked = Date.now();
      this.companyCache = new Map<string, Company>();
      this.logger.log('Cleaning Company cache');
      return true;
    }
    return false;
  }
}

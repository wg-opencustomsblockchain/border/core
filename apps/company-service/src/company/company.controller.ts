/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Company, CompanyAMQPatterns } from '@border/api-interfaces';
import { Controller, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';

import { CompanyService } from './company.service';

/**
 * A MQ microservice controller for handling user requests
 */
@Controller()
export class CompaniesController {
  /**
   * Provides an Companies controller
   * @param companyService - Injects Companies service
   */
  constructor(private readonly companyService: CompanyService) {}
  private readonly logger = new Logger(CompaniesController.name);

  /**
   * Requests a Company from the Companies service and strips the password
   * @param address - The cosmos address to search for
   * @returns a user
   */
  @MessagePattern(CompanyAMQPatterns.API_TAG + CompanyAMQPatterns.API_VERSION + CompanyAMQPatterns.GET_ONE_BY_COSMOS)
  async getUserFromCosmosAddress(@Payload() address: string): Promise<Company> {
    try {
      return await this.companyService.findOneByWallet(address);
    } catch (e) {
      this.logger.error(e);
      throw new HttpException(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Tries to find the user with the given id. If no user can be found, will return null
   * @param id - The id to search for
   * @returns a user or null
   */
  @MessagePattern(CompanyAMQPatterns.API_TAG + CompanyAMQPatterns.API_VERSION + CompanyAMQPatterns.GET_ONE_BY_ID)
  async findOneById(@Payload() id: string): Promise<Company> {
    try {
      return await this.companyService.findOneById(id);
    } catch (e) {
      this.logger.error(e);
      throw new HttpException(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}

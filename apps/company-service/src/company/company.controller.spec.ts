/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Company } from '@border/api-interfaces';
import { KeycloakService } from '@mschoenbo/nestjs-keycloak-admin';
import { Test, TestingModule } from '@nestjs/testing';
import { MQBroker } from '@border/broker';
import { DynamicModule } from '@nestjs/common';
import { CompaniesController } from './company.controller';
import { CompanyService } from './company.service';

describe('CompaniesController', () => {
  let controller: CompaniesController;
  let service: CompanyService;
  const returnValue = {} as Company;

  const mqBroker: DynamicModule = new MQBroker().getMsgBroker();
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [mqBroker],
      controllers: [CompaniesController],
      providers: [CompanyService, { provide: KeycloakService, useValue: {} }],
    }).compile();

    controller = module.get<CompaniesController>(CompaniesController);
    service = module.get<CompanyService>(CompanyService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('getUserFromCosmosAddress', () => {
    it('should find a Company belonging to the wallet', async () => {
      jest
        .spyOn(service, 'findOneByWallet')
        .mockReturnValue(Promise.resolve(returnValue));
      expect(await controller.getUserFromCosmosAddress('address')).toEqual(
        returnValue
      );
    });
  });

  describe('findOneById', () => {
    it('should find a Company belonging to an ID', async () => {
      jest
        .spyOn(service, 'findOneById')
        .mockReturnValue(Promise.resolve(returnValue));
      expect(await controller.findOneById('address')).toEqual(returnValue);
    });
  });
});

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Module } from '@nestjs/common';
import { UsersModule } from './users/users.module';
import { ConfigModule } from '@nestjs/config';
import KeycloakModule, { KeycloakModuleOptions } from '@mschoenbo/nestjs-keycloak-admin';

@Module({
  imports: [
    UsersModule,
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    KeycloakModule.register({
      baseUrl: process.env.KEYCLOAK_URL,
      realmName: process.env.KEYCLOAK_REALM,
      clientSecret: process.env.KEYCLOAK_AUTH_SECRET,
      clientId: process.env.KEYCLOAK_AUTH_CLIENT_ID,
    } as KeycloakModuleOptions),
  ],
  controllers: [],
  providers: [],
})
export class ServiceModule {}

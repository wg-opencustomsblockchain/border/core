/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { User } from '@border/api-interfaces';

import { DirectSecp256k1HdWallet } from '@cosmjs/proto-signing';
import { KeycloakService } from '@mschoenbo/nestjs-keycloak-admin';
import { HttpStatus, Injectable } from '@nestjs/common';

/**
 * A Service for handling users
 */
@Injectable()
export class UsersService {
  /**
   * The array that holds the users data. This can be interchanged with a user database or something else to provide user data storage
   */
  private userMap: Map<string, User>;

  constructor(private readonly keycloak: KeycloakService) {}

  private async getMap(): Promise<Map<string, User>> {
    if (this.userMap) return this.userMap;
    try {
      const userMap = new Map<string, User>();
      await this.keycloak.client.users.find().then(async (users: User[]) => {
        for (const user of users) {
          if (user.attributes?.mnemonic) {
            const cosmosAddress = await this.getCosmosAddress(
              user.attributes?.mnemonic[0]
            );
            userMap.set(cosmosAddress, user);
          }
        }
      });
      this.userMap = userMap;

      return this.userMap;
    } catch (error) {
      if (error['response'].status === HttpStatus.UNAUTHORIZED) {
        this.keycloak.refreshGrant();
        return this.getMap();
      }
      throw error;
    }
  }

  /**
   * Returns the cosmos address via the mnemonic
   * @param mnemonic 
   * @returns a cosmos address
   */
  async getCosmosAddress(mnemonic: string): Promise<string> {
    const [creatorAddress] = await (
      await DirectSecp256k1HdWallet.fromMnemonic(mnemonic)
    ).getAccounts();

    return creatorAddress.address;
  }

  /**
   * Tries to find the user with the given username in the array. If no user can be found, will return null
   * @param username - The username to search for
   * @returns a user or null
   */
  async findOne(username: string): Promise<User | undefined> {
    try {
      return (await this.keycloak.client.users.find({ username }))[0];
    } catch (error) {
      if (error['response'].status === HttpStatus.UNAUTHORIZED) {
        this.keycloak.refreshGrant();
        return this.findOne(username);
      }
    }
  }

  /**
   * Tries to find the user with the given user id in the array. If no user can be found, will return null
   * @param username - The username to search for
   * @returns a user or null
   */
  async findByUserId(id: string): Promise<User | undefined> {
    try {
      return (await this.keycloak.client.users.find({ id }))[0];
    } catch (error) {
      if (error['response'].status === HttpStatus.UNAUTHORIZED) {
        this.keycloak.refreshGrant();
        return this.findByUserId(id);
      }
    }
  }

  /**
   * Tries to find the user with the given cosmos address in the array. If no user can be found, will return null
   * @param address - The address to search for
   * @returns a user or null
   */
  async findOneByCosmosAddress(address: string): Promise<User | undefined> {
    return (await this.getMap()).get(address);
  }
}

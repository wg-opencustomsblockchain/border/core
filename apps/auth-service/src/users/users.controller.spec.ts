/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { User } from '@border/api-interfaces';
import { Test, TestingModule } from '@nestjs/testing';
import { KeycloakService } from '@mschoenbo/nestjs-keycloak-admin';

import { UsersController } from './users.controller';
import { UsersService } from './users.service';

const returnValue = {
  userId: 0,
  username: 'Test',
} as User;

describe('UsersController', () => {
  let controller: UsersController;
  let usersService: UsersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [UsersService, { provide: KeycloakService, useValue: {} }],
    }).compile();

    controller = module.get<UsersController>(UsersController);
    usersService = module.get<UsersService>(UsersService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('getUser should return the user by the username', () => {
    jest.spyOn(usersService, 'findOne').mockResolvedValue(Promise.resolve(returnValue));
    expect(controller.getUser(returnValue.username)).resolves.toEqual(returnValue);
  });

  it('getUser should return the user by the username', () => {
    jest.spyOn(usersService, 'findOneByCosmosAddress').mockResolvedValue(Promise.resolve(returnValue));
    expect(controller.getUserFromCosmosAddress(returnValue.username)).resolves.toEqual(returnValue);
  });
});

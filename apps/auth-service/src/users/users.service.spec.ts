/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { User } from '@border/api-interfaces';
import { KeycloakService } from '@mschoenbo/nestjs-keycloak-admin';
import { DynamicModule } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { MQBroker } from '@border/broker';
import { UsersService } from './users.service';

const user = {
  attributes: {
    wallet: ['cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462y'],
    mnemonic: [
      'link wild parent false local room leaf people couch drift yard heavy trap coral husband scorpion paper donor retire enrich visual donkey birth involve',
    ],
  },
  userId: 1,
  username: 'Erik',
  password: '$2a$10$MgivBg2HiInddrCTHopFU.olFiOe.hejTA7lBZgq361n.S3uJfDwG',
  role: 1,

  wallet: 'cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462y',
  pubkey:
    'cosmospub1addwnpepqth4xx46xws36gnpmdgcslszsv8g7gnek5km3ugp4syvnfg4xvgxyx4t379',
  tokenWalletId: 'DE537400371045831',
  company: {
    name: 'Erik Export GmbH',
    address: {
      streetAndNumber: '',
      postcode: '',
      countryCode: '',
      city: '',
    },
  },
} as User;

describe('UsersService', () => {
  let service: UsersService;

  const mqBroker: DynamicModule = new MQBroker().getMsgBroker();
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [mqBroker],
      providers: [
        UsersService,
        {
          provide: KeycloakService,
          useValue: {
            client: {
              users: {
                // eslint-disable-next-line @typescript-eslint/no-unused-vars
                async find(_payload?: { username: string }) {
                  return Promise.resolve([user]);
                },
              },
            },
          },
        },
      ],
    }).compile();

    service = module.get<UsersService>(UsersService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('findOne should return a user by username', () => {
    expect(service.findOne('Erik'))
      .resolves.toEqual(user)
      .catch((error) => console.error(error));
  });

  it('findOneByCosmosAddress should return a user by cosmos address', () => {
    jest
      .spyOn(service as any, 'getCosmosAddress')
      .mockResolvedValue(
        Promise.resolve('cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462y')
      );

    expect(
      service.findOneByCosmosAddress(
        'cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462y'
      )
    )
      .resolves.toEqual(user)
      .catch((err) => console.error(err));
  });

  it('getCosmosAddress should return correct cosmos address', async () => {
    expect(await service.getCosmosAddress(user.attributes.mnemonic[0])).toEqual(
      user.attributes.wallet[0]
    );
  });
});

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { User, UserMsgPatterns } from '@border/api-interfaces';
import { Controller, Logger, NotFoundException } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';

import { UsersService } from './users.service';

/**
 * A microservice controller for handling user requests
 */
@Controller()
export class UsersController {
  /**
   * Provides an users controller
   * @param usersService - Injects users service
   */
  private readonly logger = new Logger(UsersController.name);
  constructor(private readonly usersService: UsersService) {}

  /**
   * Requests a user from the users service and strips the password
   * @param username - The username to search for
   * @returns a user
   */
  @MessagePattern(UserMsgPatterns.API_TAG + UserMsgPatterns.API_VERSION + UserMsgPatterns.GET_ONE_BY_USERNAME)
  async getUser(@Payload() username: string): Promise<User> {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    return await this.usersService.findOne(username);
  }

  /**
   * Requests a user from the users service and strips the password
   * @param id - The user id to search for
   * @returns a user
   */
  @MessagePattern(UserMsgPatterns.API_TAG + UserMsgPatterns.API_VERSION + UserMsgPatterns.GET_ONE_BY_ID)
  async getUserById(@Payload() id: string): Promise<User> {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    return await this.usersService.findByUserId(id);
  }

  /**
   * Requests a user from the users service and strips the password
   * @param username - The cosmos address to search for
   * @returns a user
   */
  @MessagePattern(UserMsgPatterns.API_TAG + UserMsgPatterns.API_VERSION + UserMsgPatterns.GET_ONE_BY_COSMOS)
  async getUserFromCosmosAddress(@Payload() address: string): Promise<User> {
    this.logger.log('Looking up: ' + address);
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const user = await this.usersService.findOneByCosmosAddress(address);
    if (!user) throw new NotFoundException('User not found: ' + address);
    return user;
  }
}

// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

syntax = "proto3";
package silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet;

import "cosmos/base/query/v1beta1/pagination.proto";

import "tokenwallet/tokenHistoryGlobal.proto";
import "tokenwallet/segmentHistory.proto";
import "tokenwallet/segment.proto";
import "tokenwallet/walletHistory.proto";
import "tokenwallet/wallet.proto";

// this line is used by starport scaffolding # proto/tx/import

option go_package = "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types";

// Msg defines the Msg service.
service Msg {
  // this line is used by starport scaffolding # proto/tx/rpc
  rpc CreateSegment(MsgCreateSegment) returns (MsgIdResponse);
  rpc CreateSegmentWithId(MsgCreateSegmentWithId) returns (MsgIdResponse);
  rpc UpdateSegment(MsgUpdateSegment) returns (MsgEmptyResponse);
  rpc CreateWallet(MsgCreateWallet) returns (MsgIdResponse);
  rpc CreateWalletWithId(MsgCreateWalletWithId) returns (MsgIdResponse);
  rpc UpdateWallet(MsgUpdateWallet) returns (MsgEmptyResponse);
  rpc AssignCosmosAddressToWallet(MsgAssignCosmosAddressToWallet) returns (MsgEmptyResponse);
  rpc CreateTokenRef(MsgCreateTokenRef) returns (MsgIdResponse);
  rpc MoveTokenToSegment(MsgMoveTokenToSegment) returns (MsgEmptyResponse);
  rpc MoveTokenToWallet(MsgMoveTokenToWallet) returns (MsgEmptyResponse);
  rpc RemoveTokenRefFromSegment(MsgRemoveTokenRefFromSegment) returns (MsgEmptyResponse);

  // Query
  rpc FetchTokenHistoryGlobal(MsgFetchGetTokenHistoryGlobal) returns (MsgFetchGetTokenHistoryGlobalResponse);
  rpc FetchTokenHistoryGlobalAll(MsgFetchAllTokenHistoryGlobal) returns (MsgFetchAllTokenHistoryGlobalResponse);
  rpc FetchSegmentHistory(MsgFetchGetSegmentHistory) returns (MsgFetchGetSegmentHistoryResponse);
  rpc FetchSegmentHistoryAll(MsgFetchAllSegmentHistory) returns (MsgFetchAllSegmentHistoryResponse);
  rpc FetchSegment(MsgFetchGetSegment) returns (MsgFetchGetSegmentResponse);
  rpc FetchSegmentAll(MsgFetchAllSegment) returns (MsgFetchAllSegmentResponse);
  rpc FetchWalletHistory(MsgFetchGetWalletHistory) returns (MsgFetchGetWalletHistoryResponse);
  rpc FetchWalletHistoryAll(MsgFetchAllWalletHistory) returns (MsgFetchAllWalletHistoryResponse);
  rpc FetchWallet(MsgFetchGetWallet) returns (MsgFetchGetWalletResponse);
  rpc FetchWalletAll(MsgFetchAllWallet) returns (MsgFetchAllWalletResponse);
}

// this line is used by starport scaffolding # proto/tx/message
message MsgRemoveTokenRefFromSegment {
  string creator = 1;
  string tokenRefId = 2;
  string segmentId = 3;
}

message MsgMoveTokenToSegment {
  string creator = 1;
  string tokenRefId = 2;
  string sourceSegmentId = 3;
  string targetSegmentId = 4;
}

message MsgMoveTokenToWallet {
  string creator = 1;
  string tokenRefId = 2;
  string sourceSegmentId = 3;
  string targetWalletId = 4;
}

message MsgCreateTokenHistoryGlobal {
  string creator = 1;
  string tokenId = 2;
  repeated string walletIds = 3;
}

message MsgUpdateTokenHistoryGlobal {
  string creator = 1;
  string id = 2;
}

message MsgCreateSegmentHistory {
  string creator = 1;
  string id = 2;
  repeated Segment history = 3;
}

message MsgUpdateSegmentHistory {
  string creator = 1;
  string id = 2;
}

message MsgCreateWalletHistory {
  string creator = 1;
  string id = 2;
  repeated Wallet history = 3;
}

message MsgUpdateWalletHistory {
  string creator = 1;
  string id = 2;
}

message MsgCreateSegment {
  string creator = 1;
  string name = 2;
  string info = 3;
  string walletId = 4;
}

message MsgCreateSegmentWithId {
  string creator = 1;
  string Id = 2;
  string name = 3;
  string info = 4;
  string walletId = 5;
}

message MsgUpdateSegment {
  string creator = 1;
  string id = 2;
  string name = 3;
  string info = 4;
}

message MsgCreateWallet {
  string creator = 1;
  string name = 2;
}

message MsgCreateWalletWithId {
  string creator = 1;
  string Id = 2;
  string name = 3;
}

message MsgAssignCosmosAddressToWallet {
  string creator = 1;
  string cosmosAddress = 2;
  string walletId = 3;
}

message MsgUpdateWallet {
  string creator = 1;
  string id = 2;
  string name = 3;
}

message MsgCreateTokenRef {
  string creator = 1;
  string id = 2;
  string moduleRef = 3;
  string segmentId = 4;
}

message MsgEmptyResponse {}

message MsgIdResponse {
  string id = 1;
}


// Query

// this line is used by starport scaffolding # 3
message MsgFetchGetTokenHistoryGlobal {
  string creator = 1;
  string id = 2;
}

message MsgFetchGetTokenHistoryGlobalResponse {
  TokenHistoryGlobal TokenHistoryGlobal = 1;
}

message MsgFetchAllTokenHistoryGlobal {
  string creator = 1;
  cosmos.base.query.v1beta1.PageRequest pagination = 2;
}

message MsgFetchAllTokenHistoryGlobalResponse {
  repeated TokenHistoryGlobal TokenHistoryGlobal = 1;
  cosmos.base.query.v1beta1.PageResponse pagination = 2;
}
message MsgFetchGetSegmentHistory {
  string creator = 1;
  string id = 2;
}

message MsgFetchGetSegmentHistoryResponse {
  SegmentHistory SegmentHistory = 1;
}

message MsgFetchAllSegmentHistory {
  string creator = 1;
  cosmos.base.query.v1beta1.PageRequest pagination = 2;
}

message MsgFetchAllSegmentHistoryResponse {
  repeated SegmentHistory SegmentHistory = 1;
  cosmos.base.query.v1beta1.PageResponse pagination = 2;
}
message MsgFetchGetSegment {
  string creator = 1;
  string id = 2;
}

message MsgFetchGetSegmentResponse {
  Segment Segment = 1;
}

message MsgFetchAllSegment {
  string creator = 1;
  cosmos.base.query.v1beta1.PageRequest pagination = 2;
}

message MsgFetchAllSegmentResponse {
  repeated Segment Segment = 1;
  cosmos.base.query.v1beta1.PageResponse pagination = 2;
}
message MsgFetchGetWalletHistory {
  string creator = 1;
  string id = 2;
}

message MsgFetchGetWalletHistoryResponse {
  WalletHistory WalletHistory = 1;
}

message MsgFetchAllWalletHistory {
  string creator = 1;
  cosmos.base.query.v1beta1.PageRequest pagination = 2;
}

message MsgFetchAllWalletHistoryResponse {
  repeated WalletHistory WalletHistory = 1;
  cosmos.base.query.v1beta1.PageResponse pagination = 2;
}
message MsgFetchGetWallet {
  string creator = 1;
  string id = 2;
}

message MsgFetchGetWalletResponse {
  Wallet Wallet = 1;
}

message MsgFetchAllWallet {
  string creator = 1;
  cosmos.base.query.v1beta1.PageRequest pagination = 2;
}

message MsgFetchAllWalletResponse {
  repeated Wallet Wallet = 1;
  cosmos.base.query.v1beta1.PageResponse pagination = 2;
}

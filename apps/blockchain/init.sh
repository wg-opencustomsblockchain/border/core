#!/bin/bash

# // Copyright Open Logistics Foundation
# //
# // Licensed under the Open Logistics Foundation License 1.3.
# // For details on the licensing terms, see the LICENSE file.
# // SPDX-License-Identifier: OLFL-1.3
ENVFILE=${PWD}/.env
test -f $ENVFILE && source $ENVFILE

# turn on bash's job control
set -m

# Start the primary process and put it in the background
TokenManagerd start --log_level "error" --pruning everything &

# Start the helper process
# the my_helper_process might need to know how to wait on the
# primary process to start before it does its work and returns
sleep 20s #Wait for tendermint demon start

TokenManagerd keys list | grep aeb > /dev/null || printf '%s\n' "$AEB_MNEMONIC" | TokenManagerd keys add aeb --recover || exit 1
yes | TokenManagerd tx bank send $(TokenManagerd keys show alice -a) $(TokenManagerd keys show aeb -a) 1token

TokenManagerd keys list | grep broker > /dev/null || printf '%s\n' "$BROKER_MNEMONIC" | TokenManagerd keys add broker --recover || exit 1
yes | TokenManagerd tx bank send $(TokenManagerd keys show alice -a) $(TokenManagerd keys show broker -a) 1token

TokenManagerd keys list | grep erik > /dev/null || printf '%s\n' "$ERIK_MNEMONIC" | TokenManagerd keys add erik --recover || exit 1
yes | TokenManagerd tx bank send $(TokenManagerd keys show alice -a) $(TokenManagerd keys show erik -a) 1token
yes | TokenManagerd tx BusinessLogic create-wallet-with-id "$ERIK_WALLET" "$ERIK_WALLET" --from erik || true
yes | TokenManagerd tx BusinessLogic assign-address-to-wallet $(TokenManagerd keys show broker -a) "$ERIK_WALLET" --from erik || true
yes | TokenManagerd tx BusinessLogic assign-address-to-wallet $(TokenManagerd keys show aeb -a) "$ERIK_WALLET" --from erik || true

TokenManagerd keys list | grep freddy > /dev/null || printf '%s\n' "$FREDDY_MNEMONIC" | TokenManagerd keys add freddy --recover || exit 1
yes | TokenManagerd tx bank send $(TokenManagerd keys show alice -a) $(TokenManagerd keys show freddy -a) 1token
yes | TokenManagerd tx BusinessLogic create-wallet-with-id "$FREDDY_WALLET" "$FREDDY_WALLET" --from freddy || true
yes | TokenManagerd tx BusinessLogic assign-address-to-wallet $(TokenManagerd keys show freddy -a) "$ERIK_WALLET" --from erik || true

TokenManagerd keys list | grep ina > /dev/null || printf '%s\n' "$INA_MNEMONIC" | TokenManagerd keys add ina --recover || exit 1
yes | TokenManagerd tx bank send $(TokenManagerd keys show alice -a) $(TokenManagerd keys show ina -a) 1token
yes | TokenManagerd tx BusinessLogic create-wallet-with-id "$INA_WALLET" "$INA_WALLET" --from ina || true
yes | TokenManagerd tx BusinessLogic assign-address-to-wallet $(TokenManagerd keys show ina -a) "$ERIK_WALLET" --from erik || true


# AEB Accounts
TokenManagerd keys list | grep aeb_erik > /dev/null || printf '%s\n' "$AEB_ERIK_MNEMONIC" | TokenManagerd keys add aeb_erik --recover || exit 1
yes | TokenManagerd tx bank send $(TokenManagerd keys show alice -a) $(TokenManagerd keys show aeb_erik -a) 1token
yes | TokenManagerd tx BusinessLogic create-wallet-with-id "$AEB_ERIK_WALLET" "$AEB_ERIK_WALLET" --from aeb_erik || true
# yes | TokenManagerd tx BusinessLogic assign-address-to-wallet $(TokenManagerd keys show broker -a) "$AEB_ERIK_WALLET" --from aeb_erik || true
yes | TokenManagerd tx BusinessLogic assign-address-to-wallet $(TokenManagerd keys show aeb -a) "$AEB_ERIK_WALLET" --from aeb_erik || true

TokenManagerd keys list | grep aeb_ina > /dev/null || printf '%s\n' "$AEB_INA_MNEMONIC" | TokenManagerd keys add aeb_ina --recover || exit 1
yes | TokenManagerd tx bank send $(TokenManagerd keys show alice -a) $(TokenManagerd keys show aeb_ina -a) 1token
yes | TokenManagerd tx BusinessLogic create-wallet-with-id "$AEB_INA_WALLET" "$AEB_INA_WALLET" --from aeb_ina || true
yes | TokenManagerd tx BusinessLogic assign-address-to-wallet $(TokenManagerd keys show aeb_ina -a) "$AEB_ERIK_WALLET" --from aeb_erik || true

# Users for demonstrator
TokenManagerd keys list | grep ingrid > /dev/null || printf '%s\n' "$INGRID_MNEMONIC" | TokenManagerd keys add ingrid --recover || exit 1
yes | TokenManagerd tx bank send $(TokenManagerd keys show alice -a) $(TokenManagerd keys show ingrid -a) 1token
yes | TokenManagerd tx BusinessLogic create-wallet-with-id "$INGRID_WALLET" "$INGRID_WALLET" --from ingrid || true
yes | TokenManagerd tx BusinessLogic assign-address-to-wallet $(TokenManagerd keys show ingrid -a) "$ERIK_WALLET" --from erik || true

TokenManagerd keys list | grep fabian > /dev/null || printf '%s\n' "$FABIAN_MNEMONIC" | TokenManagerd keys add fabian --recover || exit 1
yes | TokenManagerd tx bank send $(TokenManagerd keys show alice -a) $(TokenManagerd keys show fabian -a) 1token
yes | TokenManagerd tx BusinessLogic create-wallet-with-id "$FABIAN_WALLET" "$FABIAN_WALLET" --from fabian || true
yes | TokenManagerd tx BusinessLogic assign-address-to-wallet $(TokenManagerd keys show fabian -a) "$ERIK_WALLET" --from erik || true

for f in $(dirname $0)/testfiles/*; do

yes | TokenManagerd tx BusinessLogic create-export-token "$f" --gas 100000000 --from broker

done;

# create module related application roles

# Export Related - Erik/Broker
yes | TokenManagerd tx authorization create-application-role "EXPORT-TOKEN-MODULE_READ-TOKEN" "EXPORT-TOKEN-MODULE_READ-TOKEN" --from alice --gas 100000000

yes | TokenManagerd tx authorization create-application-role "EXPORT-TOKEN-MODULE_CREATE-TOKEN" "EXPORT-TOKEN-MODULE_CREATE-TOKEN" --from alice --gas 100000000
yes | TokenManagerd tx authorization create-application-role "EXPORT-TOKEN-MODULE_EXPORT-RECORDED" "EXPORT-TOKEN-MODULE_EXPORT-RECORDED" --from alice --gas 100000000
yes | TokenManagerd tx authorization create-application-role "EXPORT-TOKEN-MODULE_EXPORT-READY" "EXPORT-TOKEN-MODULE_EXPORT-READY" --from alice --gas 100000000
yes | TokenManagerd tx authorization create-application-role "EXPORT-TOKEN-MODULE_EXPORT-PICKUP" "EXPORT-TOKEN-MODULE_EXPORT-PICKUP" --from alice --gas 100000000
yes | TokenManagerd tx authorization create-application-role "EXPORT-TOKEN-MODULE_EXPORT-PRESENTED" "EXPORT-TOKEN-MODULE_EXPORT-PRESENTED" --from alice --gas 100000000
yes | TokenManagerd tx authorization create-application-role "EXPORT-TOKEN-MODULE_EXPORT-EXIT" "EXPORT-TOKEN-MODULE_EXPORT-EXIT" --from alice --gas 100000000
yes | TokenManagerd tx authorization create-application-role "EXPORT-TOKEN-MODULE_EXPORT-ARCHIVED" "EXPORT-TOKEN-MODULE_EXPORT-ARCHIVED" --from alice --gas 100000000
yes | TokenManagerd tx authorization create-application-role "EXPORT_NEW" "EXPORT_NEW" --from alice --gas 100000000
sleep 3s

# Export Related Freddy/Fabian
yes | TokenManagerd tx authorization create-application-role "EXPORT-TOKEN-MODULE_EXPORT_CHECK" "EXPORT-TOKEN-MODULE_EXPORT_CHECK" --from alice --gas 100000000
yes | TokenManagerd tx authorization create-application-role "EXPORT-TOKEN-MODULE_EXPORT_MOBILECHECK" "EXPORT-TOKEN-MODULE_EXPORT_MOBILECHECK" --from alice --gas 100000000
yes | TokenManagerd tx authorization create-application-role "EXPORT-TOKEN-MODULE_EXPORT_REFUSAL" "EXPORT-TOKEN-MODULE_EXPORT_REFUSAL" --from alice --gas 100000000
yes | TokenManagerd tx authorization create-application-role "EXPORT-TOKEN-MODULE_EXPORT_ACCEPTED" "EXPORT-TOKEN-MODULE_EXPORT_ACCEPTED" --from alice --gas 100000000
sleep 3s

# Import Related Ina/Ingrid
yes | TokenManagerd tx authorization create-application-role "IMPORT-TOKEN-MODULE_READ-TOKEN" "IMPORT-TOKEN-MODULE_READ-TOKEN" --from alice --gas 100000000
yes | TokenManagerd tx authorization create-application-role "IMPORT-TOKEN-MODULE_CREATE-TOKEN" "IMPORT-TOKEN-MODULE_CREATE-TOKEN" --from alice --gas 100000000
yes | TokenManagerd tx authorization create-application-role "IMPORT-TOKEN-MODULE_UPDATE-TOKEN" "IMPORT-TOKEN-MODULE_UPDATE-TOKEN" --from alice --gas 100000000
yes | TokenManagerd tx authorization create-application-role "IMPORT-TOKEN-MODULE_IMPORT-ACCEPTED" "IMPORT-TOKEN-MODULE_IMPORT-ACCEPTED" --from alice --gas 100000000
yes | TokenManagerd tx authorization create-application-role "IMPORT-TOKEN-MODULE_IMPORT-COMPLETED" "IMPORT-TOKEN-MODULE_IMPORT-COMPLETED" --from alice --gas 100000000
yes | TokenManagerd tx authorization create-application-role "IMPORT-TOKEN-MODULE_IMPORT-PROPOSAL" "IMPORT-TOKEN-MODULE_IMPORT-PROPOSAL" --from alice --gas 100000000
yes | TokenManagerd tx authorization create-application-role "IMPORT-TOKEN-MODULE_IMPORT-REFUSED" "IMPORT-TOKEN-MODULE_IMPORT-REFUSED" --from alice --gas 100000000
yes | TokenManagerd tx authorization create-application-role "IMPORT-TOKEN-MODULE_IMPORT-SUPPLEMENT" "IMPORT-TOKEN-MODULE_IMPORT-SUPPLEMENT" --from alice --gas 100000000
yes | TokenManagerd tx authorization create-application-role "IMPORT-TOKEN-MODULE_IMPORT-EXPORTED" "IMPORT-TOKEN-MODULE_IMPORT-EXPORTED" --from alice --gas 100000000
yes | TokenManagerd tx authorization create-application-role "IMPORT-TOKEN-MODULE_IMPORT-SUBMITTED" "IMPORT-TOKEN-MODULE_IMPORT-SUBMITTED" --from alice --gas 100000000
yes | TokenManagerd tx authorization create-application-role "IMPORT-TOKEN-MODULE_IMPORT-RELEASE" "IMPORT-TOKEN-MODULE_IMPORT-RELEASE" --from alice --gas 100000000
sleep 3s

#yes | TokenManagerd tx authorization create-application-role "BUSINESSLOGIC-MODULE_CREATE-TOKEN" "BUSINESSLOGIC-MODULE_CREATE-TOKEN" --from alice
#yes | TokenManagerd tx authorization create-application-role "BUSINESSLOGIC-MODULE_UPDATE-TOKEN" "BUSINESSLOGIC-MODULE_UPDATE-TOKEN" --from alice
#yes | TokenManagerd tx authorization create-application-role "BUSINESSLOGIC-MODULE_MOVE-TOKEN-TO-WALLET" "BUSINESSLOGIC-MODULE_MOVE-TOKEN-TO-WALLET" --from alice
#yes | TokenManagerd tx authorization create-application-role "BUSINESSLOGIC-MODULE_SET-TOKEN-VALID-STATUS" "BUSINESSLOGIC-MODULE_SET-TOKEN-VALID-STATUS" --from alice
#yes | TokenManagerd tx authorization create-application-role "BUSINESSLOGIC-MODULE_ACTIVATE-TOKEN" "BUSINESSLOGIC-MODULE_ACTIVATE-TOKEN" --from alice
#yes | TokenManagerd tx authorization create-application-role "BUSINESSLOGIC-MODULE_DEACTIVATE-TOKEN" "BUSINESSLOGIC-MODULE_DEACTIVATE-TOKEN" --from alice
#yes | TokenManagerd tx authorization create-application-role "BUSINESSLOGIC-MODULE_STORE-DOCUMENT-HASH" "BUSINESSLOGIC-MODULE_STORE-DOCUMENT-HASH" --from alice
#yes | TokenManagerd tx authorization create-application-role "BUSINESSLOGIC-MODULE_CLONE-TOKEN" "BUSINESSLOGIC-MODULE_CLONE-TOKEN" --from alice
#yes | TokenManagerd tx authorization create-application-role "BUSINESSLOGIC-MODULE_GET-DOCUMENT-HASH" "BUSINESSLOGIC-MODULE_GET-DOCUMENT-HASH" --from alice
#yes | TokenManagerd tx authorization create-application-role "BUSINESSLOGIC-MODULE_GET-TOKENS-BY-TOKEN-WALLET-ID" "BUSINESSLOGIC-MODULE_GET-TOKENS-BY-TOKEN-WALLET-ID" --from alice
#yes | TokenManagerd tx authorization create-application-role "BUSINESSLOGIC-MODULE_GET-TOKENS-BY-SEGMENT-ID" "BUSINESSLOGIC-MODULE_GET-TOKENS-BY-SEGMENT-ID" --from alice
#yes | TokenManagerd tx authorization create-application-role "HASHTOKEN-MODULE_CREATE-TOKEN" "HASHTOKEN-MODULE_CREATE-TOKEN" --from alice
#yes | TokenManagerd tx authorization create-application-role "HASHTOKEN-MODULE_UPDATE-TOKEN" "HASHTOKEN-MODULE_UPDATE-TOKEN" --from alice
#yes | TokenManagerd tx authorization create-application-role "HASHTOKEN-MODULE_DEACTIVATE-TOKEN" "HASHTOKEN-MODULE_DEACTIVATE-TOKEN" --from alice
#yes | TokenManagerd tx authorization create-application-role "HASHTOKEN-MODULE_ACTIVATE-TOKEN" "HASHTOKEN-MODULE_ACTIVATE-TOKEN" --from alice
#yes | TokenManagerd tx authorization create-application-role "HASHTOKEN-MODULE_UPDATE-TOKEN-INFORMATION" "HASHTOKEN-MODULE_UPDATE-TOKEN-INFORMATION" --from alice
#yes | TokenManagerd tx authorization create-application-role "HASHTOKEN-MODULE_GET-TOKEN" "HASHTOKEN-MODULE_GET-TOKEN" --from alice
#yes | TokenManagerd tx authorization create-application-role "HASHTOKEN-MODULE_GET-ALL-TOKEN" "HASHTOKEN-MODULE_GET-ALL-TOKEN" --from alice
#yes | TokenManagerd tx authorization create-application-role "HASHTOKEN-MODULE_GET-TOKEN-HISTORY" "HASHTOKEN-MODULE_GET-TOKEN-HISTORY" --from alice
#yes | TokenManagerd tx authorization create-application-role "HASHTOKEN-MODULE_GET-ALL-TOKEN-HISTORY" "HASHTOKEN-MODULE_GET-ALL-TOKEN-HISTORY" --from alice
#yes | TokenManagerd tx authorization create-application-role "HASHTOKEN-MODULE_GET-CONFIGURATION" "HASHTOKEN-MODULE_GET-CONFIGURATION" --from alice
#yes | TokenManagerd tx authorization create-application-role "TOKEN-MODULE_CREATE-TOKEN" "TOKEN-MODULE_CREATE-TOKEN" --from alice
#yes | TokenManagerd tx authorization create-application-role "TOKEN-MODULE_UPDATE-TOKEN" "TOKEN-MODULE_UPDATE-TOKEN" --from alice
#yes | TokenManagerd tx authorization create-application-role "TOKEN-MODULE_DEACTIVATE-TOKEN" "TOKEN-MODULE_DEACTIVATE-TOKEN" --from alice
#yes | TokenManagerd tx authorization create-application-role "TOKEN-MODULE_ACTIVATE-TOKEN" "TOKEN-MODULE_ACTIVATE-TOKEN" --from alice
#yes | TokenManagerd tx authorization create-application-role "TOKEN-MODULE_UPDATE-TOKEN-INFORMATION" "TOKEN-MODULE_UPDATE-TOKEN-INFORMATION" --from alice
#yes | TokenManagerd tx authorization create-application-role "TOKEN-MODULE_GET-TOKEN" "TOKEN-MODULE_GET-TOKEN" --from alice
#yes | TokenManagerd tx authorization create-application-role "TOKEN-MODULE_GET-ALL-TOKEN" "TOKEN-MODULE_GET-ALL-TOKEN" --from alice
#yes | TokenManagerd tx authorization create-application-role "TOKEN-MODULE_GET-TOKEN-HISTORY" "TOKEN-MODULE_GET-TOKEN-HISTORY" --from alice
#yes | TokenManagerd tx authorization create-application-role "TOKEN-MODULE_GET-ALL-TOKEN-HISTORY" "TOKEN-MODULE_GET-ALL-TOKEN-HISTORY" --from alice
#yes | TokenManagerd tx authorization create-application-role "TOKEN-MODULE_GET-CONFIGURATION" "TOKEN-MODULE_GET-CONFIGURATION" --from alice
#yes | TokenManagerd tx authorization create-application-role "WALLET-MODULE_CREATE-SEGMENT" "WALLET-MODULE_CREATE-SEGMENT" --from alice
#yes | TokenManagerd tx authorization create-application-role "WALLET-MODULE_UPDATE-SEGMENT" "WALLET-MODULE_UPDATE-SEGMENT" --from alice
#yes | TokenManagerd tx authorization create-application-role "WALLET-MODULE_CREATE-SEGMENT-HISTORY" "WALLET-MODULE_CREATE-SEGMENT-HISTORY" --from alice
#yes | TokenManagerd tx authorization create-application-role "WALLET-MODULE_UPDATE-SEGMENT-HISTORY" "WALLET-MODULE_UPDATE-SEGMENT-HISTORY" --from alice
#yes | TokenManagerd tx authorization create-application-role "WALLET-MODULE_CREATE-WALLET" "WALLET-MODULE_CREATE-WALLET" --from alice
#yes | TokenManagerd tx authorization create-application-role "WALLET-MODULE_UPDATE-WALLET" "WALLET-MODULE_UPDATE-WALLET" --from alice
#yes | TokenManagerd tx authorization create-application-role "WALLET-MODULE_CREATE-WALLET-HISTORY" "WALLET-MODULE_CREATE-WALLET-HISTORY" --from alice
#yes | TokenManagerd tx authorization create-application-role "WALLET-MODULE_UPDATE-WALLET-HISTORY" "WALLET-MODULE_UPDATE-WALLET-HISTORY" --from alice
#yes | TokenManagerd tx authorization create-application-role "WALLET-MODULE_ASSIGN-COSMOSADRESS-TO-WALLET" "WALLET-MODULE_ASSIGN-COSMOSADRESS-TO-WALLET" --from alice
#yes | TokenManagerd tx authorization create-application-role "WALLET-MODULE_MOVE-TOKEN-TO-SEGMENT" "WALLET-MODULE_MOVE-TOKEN-TO-SEGMENT" --from alice
#yes | TokenManagerd tx authorization create-application-role "WALLET-MODULE_MOVE-TOKEN-TO-WALLET" "WALLET-MODULE_MOVE-TOKEN-TO-WALLET" --from alice
#yes | TokenManagerd tx authorization create-application-role "WALLET-MODULE_REMOVE-TOKENREF-FROM-SEGMENT" "WALLET-MODULE_REMOVE-TOKENREF-FROM-SEGMENT" --from alice
#yes | TokenManagerd tx authorization create-application-role "WALLET-MODULE_QUERY-SEGMENT-ALL" "WALLET-MODULE_QUERY-SEGMENT-ALL" --from alice
#yes | TokenManagerd tx authorization create-application-role "WALLET-MODULE_QUERY-SEGMENT" "WALLET-MODULE_QUERY-SEGMENT" --from alice
#yes | TokenManagerd tx authorization create-application-role "WALLET-MODULE_QUERY-WALLET-ALL" "WALLET-MODULE_QUERY-WALLET-ALL" --from alice
#yes | TokenManagerd tx authorization create-application-role "WALLET-MODULE_QUERY-WALLET" "WALLET-MODULE_QUERY-WALLET" --from alice
#yes | TokenManagerd tx authorization create-application-role "WALLET-MODULE_QUERY-TOKEN-HISTORY-GLOBAL-ALL" "WALLET-MODULE_QUERY-TOKEN-HISTORY-GLOBAL-ALL" --from alice
#yes | TokenManagerd tx authorization create-application-role "WALLET-MODULE_QUERY-TOKEN-HISTORY-GLOBAL" "WALLET-MODULE_QUERY-TOKEN-HISTORY-GLOBAL" --from alice
#yes | TokenManagerd tx authorization create-application-role "WALLET-MODULE_QUERY-TOKEN-WALLET-HISTORY-ALL" "WALLET-MODULE_QUERY-TOKEN-WALLET-HISTORY-ALL" --from alice
#yes | TokenManagerd tx authorization create-application-role "WALLET-MODULE_QUERY-TOKEN-WALLET-HISTORY" "WALLET-MODULE_QUERY-TOKEN-WALLET-HISTORY" --from alice
#yes | TokenManagerd tx authorization create-application-role "WALLET-MODULE_QUERY-CONFIGURATION-ALL" "WALLET-MODULE_QUERY-CONFIGURATION-ALL" --from alice
#yes | TokenManagerd tx authorization create-application-role "WALLET-MODULE_QUERY-CONFIGURATION" "WALLET-MODULE_QUERY-CONFIGURATION" --from alice
#yes | TokenManagerd tx authorization create-application-role "WALLET-MODULE_QUERY-SEGMENT-HISTORY-ALL" "WALLET-MODULE_QUERY-SEGMENT-HISTORY-ALL" --from alice
#yes | TokenManagerd tx authorization create-application-role "WALLET-MODULE_QUERY-SEGMENT-HISTORY" "WALLET-MODULE_QUERY-SEGMENT-HISTORY" --from alice

# create a new blockchain account for user
#yes | TokenManagerd tx authorization create-blockchain-account $(TokenManagerd keys show user -a) --from alice
yes | TokenManagerd tx authorization create-blockchain-account $(TokenManagerd keys show erik -a) --from alice --gas 100000000
yes | TokenManagerd tx authorization create-blockchain-account $(TokenManagerd keys show ina -a) --from alice --gas 100000000
yes | TokenManagerd tx authorization create-blockchain-account $(TokenManagerd keys show ingrid -a) --from alice --gas 100000000
yes | TokenManagerd tx authorization create-blockchain-account $(TokenManagerd keys show freddy -a) --from alice --gas 100000000
yes | TokenManagerd tx authorization create-blockchain-account $(TokenManagerd keys show fabian -a) --from alice --gas 100000000
sleep 3s

# grant new user all application roles

# Export Related - Erik/Broker/Freddy/Fabian

# Erik
yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show erik -a) "EXPORT-TOKEN-MODULE_READ-TOKEN" --from alice --gas 100000000
yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show erik -a) "EXPORT-TOKEN-MODULE_CREATE-TOKEN" --from alice --gas 100000000
yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show erik -a) "EXPORT-TOKEN-MODULE_EXPORT-RECORDED" --from alice --gas 100000000
yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show erik -a) "EXPORT-TOKEN-MODULE_EXPORT-READY" --from alice --gas 100000000
yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show erik -a) "EXPORT-TOKEN-MODULE_EXPORT-EXIT" --from alice --gas 100000000
yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show erik -a) "EXPORT-TOKEN-MODULE_EXPORT-ARCHIVED" --from alice --gas 100000000
yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show erik -a) "EXPORT-TOKEN-MODULE_EXPORT_ACCEPTED" --from alice --gas 100000000
sleep 3s

# Broker

yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show broker -a) "EXPORT-TOKEN-MODULE_CREATE-TOKEN" --from alice --gas 100000000

# Freddy
yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show freddy -a) "EXPORT-TOKEN-MODULE_READ-TOKEN" --from alice --gas 100000000
yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show freddy -a) "EXPORT-TOKEN-MODULE_EXPORT_CHECK" --from alice --gas 100000000
yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show freddy -a) "EXPORT-TOKEN-MODULE_EXPORT_MOBILECHECK" --from alice --gas 100000000
yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show freddy -a) "EXPORT-TOKEN-MODULE_EXPORT-PICKUP" --from alice --gas 100000000
yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show freddy -a) "EXPORT-TOKEN-MODULE_EXPORT-PRESENTED" --from alice --gas 100000000


# Fabian
yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show fabian -a) "EXPORT-TOKEN-MODULE_READ-TOKEN" --from alice --gas 100000000
yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show fabian -a) "EXPORT-TOKEN-MODULE_EXPORT_CHECK" --from alice --gas 100000000
yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show fabian -a) "EXPORT-TOKEN-MODULE_EXPORT_MOBILECHECK" --from alice --gas 100000000
yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show fabian -a) "EXPORT-TOKEN-MODULE_EXPORT-PICKUP" --from alice --gas 100000000
yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show fabian -a) "EXPORT-TOKEN-MODULE_EXPORT-PRESENTED" --from alice --gas 100000000


# Ina
# Export
yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show ina -a) "EXPORT-TOKEN-MODULE_READ-TOKEN" --from alice --gas 100000000
yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show ina -a) "EXPORT-TOKEN-MODULE_EXPORT_REFUSAL" --from alice --gas 100000000
# Import
yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show ina -a) "IMPORT-TOKEN-MODULE_CREATE-TOKEN" --from alice --gas 100000000
yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show ina -a) "IMPORT-TOKEN-MODULE_UPDATE-TOKEN" --from alice --gas 100000000
yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show ina -a) "IMPORT-TOKEN-MODULE_IMPORT-ACCEPTED" --from alice --gas 100000000
yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show ina -a) "IMPORT-TOKEN-MODULE_IMPORT-COMPLETED" --from alice --gas 100000000
yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show ina -a) "IMPORT-TOKEN-MODULE_IMPORT-PROPOSAL" --from alice --gas 100000000
yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show ina -a) "IMPORT-TOKEN-MODULE_IMPORT-REFUSED" --from alice --gas 100000000
yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show ina -a) "IMPORT-TOKEN-MODULE_IMPORT-SUPPLEMENT" --from alice --gas 100000000
yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show ina -a) "IMPORT-TOKEN-MODULE_IMPORT-SUBMITTED" --from alice --gas 100000000
yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show ina -a) "IMPORT-TOKEN-MODULE_IMPORT-RELEASE" --from alice --gas 100000000


# Ingrid
# Export
yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show ingrid -a) "EXPORT-TOKEN-MODULE_READ-TOKEN" --from alice --gas 100000000
yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show ingrid -a) "EXPORT-TOKEN-MODULE_EXPORT_REFUSAL" --from alice --gas 100000000
# Import
yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show ingrid -a) "IMPORT-TOKEN-MODULE_CREATE-TOKEN" --from alice --gas 100000000
yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show ingrid -a) "IMPORT-TOKEN-MODULE_UPDATE-TOKEN" --from alice --gas 100000000
yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show ingrid -a) "IMPORT-TOKEN-MODULE_IMPORT-ACCEPTED" --from alice --gas 100000000
yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show ingrid -a) "IMPORT-TOKEN-MODULE_IMPORT-COMPLETED" --from alice --gas 100000000
yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show ingrid -a) "IMPORT-TOKEN-MODULE_IMPORT-PROPOSAL" --from alice --gas 100000000
yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show ingrid -a) "IMPORT-TOKEN-MODULE_IMPORT-REFUSED" --from alice --gas 100000000
yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show ingrid -a) "IMPORT-TOKEN-MODULE_IMPORT-SUPPLEMENT" --from alice --gas 100000000
yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show ingrid -a) "IMPORT-TOKEN-MODULE_IMPORT-SUBMITTED" --from alice --gas 100000000
yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show ingrid -a) "IMPORT-TOKEN-MODULE_IMPORT-RELEASE" --from alice --gas 100000000
sleep 3s


#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "AUTHORIZATION-MODULE_CREATE-ACCOUNT" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "AUTHORIZATION-MODULE_GRANT-ROLES" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "AUTHORIZATION-MODULE_REVOKE-ROLES" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "AUTHORIZATION-MODULE_BLOCK-ACCOUNT" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "AUTHORIZATION-MODULE_CREATE-APPLICATION-ROLE" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "AUTHORIZATION-MODULE_UPDATE-APPLICATION-ROLE" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "AUTHORIZATION-MODULE_READ-APPLICATION-ROLE" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "AUTHORIZATION-MODULE_DEACTIVATE-APPLICATION-ROLE" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "AUTHORIZATION-NODULE_UPDATE_CONFIGURATION" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "BUSINESSLOGIC-MODULE_CREATE-TOKEN" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "BUSINESSLOGIC-MODULE_UPDATE-TOKEN" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "BUSINESSLOGIC-MODULE_MOVE-TOKEN-TO-WALLET" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "BUSINESSLOGIC-MODULE_SET-TOKEN-VALID-STATUS" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "BUSINESSLOGIC-MODULE_ACTIVATE-TOKEN" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "BUSINESSLOGIC-MODULE_DEACTIVATE-TOKEN" -- rom alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "BUSINESSLOGIC-MODULE_STORE-DOCUMENT-HASH" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "BUSINESSLOGIC-MODULE_CLONE-TOKEN" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "BUSINESSLOGIC-MODULE_GET-DOCUMENT-HASH" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "BUSINESSLOGIC-MODULE_GET-TOKENS-BY-TOKEN-WALLET-ID" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "BUSINESSLOGIC-MODULE_GET-TOKENS-BY-SEGMENT-ID" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "HASHTOKEN-MODULE_CREATE-TOKEN" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "HASHTOKEN-MODULE_UPDATE-TOKEN" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "HASHTOKEN-MODULE_DEACTIVATE-TOKEN" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "HASHTOKEN-MODULE_ACTIVATE-TOKEN" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "HASHTOKEN-MODULE_UPDATE-TOKEN-INFORMATION" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "HASHTOKEN-MODULE_GET-TOKEN" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "HASHTOKEN-MODULE_GET-ALL-TOKEN" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "HASHTOKEN-MODULE_GET-TOKEN-HISTORY" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "HASHTOKEN-MODULE_GET-ALL-TOKEN-HISTORY" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "HASHTOKEN-MODULE_GET-CONFIGURATION" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "TOKEN-MODULE_CREATE-TOKEN" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "TOKEN-MODULE_UPDATE-TOKEN" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "TOKEN-MODULE_DEACTIVATE-TOKEN" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "TOKEN-MODULE_ACTIVATE-TOKEN" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "TOKEN-MODULE_UPDATE-TOKEN-INFORMATION" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "TOKEN-MODULE_GET-TOKEN" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "TOKEN-MODULE_GET-ALL-TOKEN" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "TOKEN-MODULE_GET-TOKEN-HISTORY" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "TOKEN-MODULE_GET-ALL-TOKEN-HISTORY" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "TOKEN-MODULE_GET-CONFIGURATION" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "WALLET-MODULE_CREATE-SEGMENT" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "WALLET-MODULE_UPDATE-SEGMENT" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "WALLET-MODULE_CREATE-SEGMENT-HISTORY" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "WALLET-MODULE_UPDATE-SEGMENT-HISTORY" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "WALLET-MODULE_CREATE-WALLET" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "WALLET-MODULE_UPDATE-WALLET" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "WALLET-MODULE_CREATE-WALLET-HISTORY" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "WALLET-MODULE_UPDATE-WALLET-HISTORY" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "WALLET-MODULE_ASSIGN-COSMOSADRESS-TO-WALLET" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "WALLET-MODULE_MOVE-TOKEN-TO-SEGMENT" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "WALLET-MODULE_MOVE-TOKEN-TO-WALLET" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "WALLET-MODULE_REMOVE-TOKENREF-FROM-SEGMENT" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "WALLET-MODULE_QUERY-SEGMENT-ALL" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "WALLET-MODULE_QUERY-SEGMENT" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "WALLET-MODULE_QUERY-WALLET-ALL" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "WALLET-MODULE_QUERY-WALLET" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "WALLET-MODULE_QUERY-TOKEN-HISTORY-GLOBAL-ALL" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "WALLET-MODULE_QUERY-TOKEN-HISTORY-GLOBAL" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "WALLET-MODULE_QUERY-TOKEN-WALLET-HISTORY-ALL" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "WALLET-MODULE_QUERY-TOKEN-WALLET-HISTORY" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "WALLET-MODULE_QUERY-CONFIGURATION-ALL" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "WALLET-MODULE_QUERY-CONFIGURATION" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "WALLET-MODULE_QUERY-SEGMENT-HISTORY-ALL" --from alice
#yes | TokenManagerd tx authorization grant-application-role $(TokenManagerd keys show user -a) "WALLET-MODULE_QUERY-SEGMENT-HISTORY" --from alice
# after creating and granting all initial application roles we set the permission check to true

#yes | TokenManagerd tx authorization update-configuration 0 true --from alice --gas 100000000

# now we bring the primary process back into the foreground
# and leave it there
fg %1

//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	authorizationTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
	walletTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
)

func (k msgServer) FetchAllWallet(goCtx context.Context, msg *types.MsgFetchAllWallet) (*walletTypes.MsgFetchAllWalletResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Permission check
	check, err := k.authorizationKeeper.HasRole(ctx, msg.Creator, authorizationTypes.WalletQueryWalletAll)
	if err != nil || !check {
		return &walletTypes.MsgFetchAllWalletResponse{}, sdkerrors.Wrap(authorizationTypes.ErrNoPermission, authorizationTypes.WalletQueryWalletAll)
	}
	res, error := k.walletKeeper.FetchWalletAll(goCtx, &walletTypes.MsgFetchAllWallet{
		Creator: msg.Creator,
	})

	if error != nil {
		return &walletTypes.MsgFetchAllWalletResponse{}, error
	}

	return res, nil
}

//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	authorizationTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
	walletTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
)

func (k msgServer) UpdateSegment(goCtx context.Context, msg *types.MsgUpdateSegment) (*walletTypes.MsgEmptyResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Permission check
	check, err := k.authorizationKeeper.HasRole(ctx, msg.Creator, authorizationTypes.WalletUpdateSegment)
	if err != nil || !check {
		return &walletTypes.MsgEmptyResponse{}, sdkerrors.Wrap(authorizationTypes.ErrNoPermission, authorizationTypes.WalletUpdateSegment)
	}
	res, error := k.walletKeeper.UpdateSegment(goCtx, &walletTypes.MsgUpdateSegment{
		Creator: msg.Creator,
		Id:      msg.Id,
		Name:    msg.Name,
		Info:    msg.Info,
	})

	if error != nil {
		return &walletTypes.MsgEmptyResponse{}, error
	}

	return res, nil
}

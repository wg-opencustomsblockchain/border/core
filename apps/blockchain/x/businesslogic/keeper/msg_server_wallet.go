//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	authorizationTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
	walletTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
)

func (k msgServer) AssignCosmosAddressToWallet(goCtx context.Context, msg *types.MsgAssignCosmosAddressToWallet) (*types.MsgEmptyResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Permission check
	check, err := k.authorizationKeeper.HasRole(ctx, msg.Creator, authorizationTypes.WalletAssignCosmosAddressToWallet)
	if err != nil || !check {
		return &types.MsgEmptyResponse{}, sdkerrors.Wrap(authorizationTypes.ErrNoPermission, authorizationTypes.WalletAssignCosmosAddressToWallet)
	}
	_, error := k.walletKeeper.AssignCosmosAddressToWallet(goCtx, &walletTypes.MsgAssignCosmosAddressToWallet{
		Creator:       msg.Creator,
		CosmosAddress: msg.CosmosAddress,
		WalletId:      msg.WalletId,
	})
	if error != nil {
		return &types.MsgEmptyResponse{}, error
	}

	return &types.MsgEmptyResponse{}, nil
}

// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
package keeper

import (
	"context"
	authorizationTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

func (k msgServer) RevertModulesToGenesis(goCtx context.Context, msg *types.MsgRevertToGenesis) (*types.MsgEmptyResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	check, err := k.authorizationKeeper.HasRole(ctx, msg.Creator, authorizationTypes.BusinessLogicRevertModulesToGenesis)
	if err != nil || !check {
		return &types.MsgEmptyResponse{}, sdkerrors.Wrap(authorizationTypes.ErrNoPermission, authorizationTypes.BusinessLogicRevertModulesToGenesis)
	}

	k.authorizationKeeper.RevertToGenesis(ctx)
	k.walletKeeper.RevertToGenesis(ctx)
	k.RevertToGenesis(ctx)

	return &types.MsgEmptyResponse{}, nil
}

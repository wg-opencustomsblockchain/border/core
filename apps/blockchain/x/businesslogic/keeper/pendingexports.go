//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"fmt"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"
)

func (k Keeper) AddExportNotice(ctx sdk.Context, exportTokenId string, company string) error {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.ExportNoticeKey))
	var notices types.ExportNotice
	k.cdc.MustUnmarshal(store.Get(GetIDBytes(company)), &notices)

	// The Company String can either Contain a name or EORI
	notices.Company = company
	notices.ExportTokenIds = append(notices.ExportTokenIds, exportTokenId)
	b, err := k.cdc.Marshal(&notices)
	if err != nil {
		return err
	}
	store.Set(GetIDBytes(notices.Company), b)
	return nil
}

func (k Keeper) GetExportNotices(ctx sdk.Context, company string) []string {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.ExportNoticeKey))
	var notices types.ExportNotice
	k.cdc.MustUnmarshal(store.Get(GetIDBytes(company)), &notices)
	return notices.ExportTokenIds
}

func (k Keeper) RemoveExportNotice(ctx sdk.Context, exportTokenId string, company string) error {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.ExportNoticeKey))
	var notices types.ExportNotice
	k.cdc.MustUnmarshal(store.Get(GetIDBytes(company)), &notices)
	oldIds := notices.ExportTokenIds
	for i, token := range oldIds {
		if token == exportTokenId {
			newIds := append(oldIds[:i], oldIds[i+1:]...)
			notices.ExportTokenIds = newIds
			b, err := k.cdc.Marshal(&notices)
			if err != nil {
				return err
			}
			store.Set(GetIDBytes(notices.Company), b)
			return nil
		}
	}
	return types.ErrNoNotice
}

// GetIDBytes returns the byte representation of the ID
func GetIDBytes(id string) []byte {
	return []byte(id)
}

func BuildKey(companyName string, countryCode string) string {
	return fmt.Sprintf("%s###%s", companyName, countryCode)
}

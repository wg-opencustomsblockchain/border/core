// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
package keeper

import (
	"context"
	exportTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/exporttoken/types"
	"testing"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	tokenwalletTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
)

const (
	ImportTokenWalletId = "TokenWalletId"
)

func Test_msgServer_CreateImportToken(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	creatorA := "cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462y"

	goCtx := sdk.WrapSDKContext(ctx)
	type fields struct {
		Keeper Keeper
	}
	type args struct {
		goCtx context.Context
		msg   *types.MsgCreateImportToken
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *types.MsgIdResponse
		wantErr bool
	}{
		{
			name:   "Successfull Tx",
			fields: fields{Keeper: *keeper},
			args: args{goCtx: goCtx, msg: &types.MsgCreateImportToken{
				Creator:            creatorA,
				RelatedExportToken: ExportId,
				TokenWalletId:      ImportTokenWalletId,
			}},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			k := msgServer{
				Keeper: tt.fields.Keeper,
			}

			_, errCreateExportWallet := k.walletKeeper.CreateWalletWithId(sdk.WrapSDKContext(ctx), tokenwalletTypes.NewMsgCreateWalletWithId(creatorA, ExportTokenWalletId, ExportTokenWalletId))
			if errCreateExportWallet != nil {
				t.Errorf("msgServer.CreateWalletWithId() error = %v", errCreateExportWallet)
			}

			k.CreateExportToken(tt.args.goCtx, &types.MsgCreateExportToken{
				Creator:              creatorA,
				TokenWalletId:        ExportTokenWalletId,
				ExportId:             ExportId,
				LocalReferenceNumber: LRN,
				Consignee: &exportTypes.Company{
					Address: &exportTypes.Address{
						CountryCode: ConsigneeCountry,
					},
					Name: ConsigneeCompanyName,
				},
			})

			_, errCreateWallet := k.walletKeeper.CreateWalletWithId(sdk.WrapSDKContext(ctx), tokenwalletTypes.NewMsgCreateWalletWithId(creatorA, ImportTokenWalletId, ImportTokenWalletId))
			if errCreateWallet != nil {
				t.Errorf("msgServer.CreateWalletWithId() error = %v", errCreateWallet)
			}

			oldWallet, errOldWallet := k.walletKeeper.FetchWallet(sdk.WrapSDKContext(ctx), &tokenwalletTypes.MsgFetchGetWallet{Creator: creatorA, Id: ImportTokenWalletId})
			if errOldWallet != nil {
				t.Errorf("msgServer.FetchWallet() error = %v", errOldWallet)
			}

			got, err := k.CreateImportToken(tt.args.goCtx, tt.args.msg)
			if (err != nil) != tt.wantErr {
				t.Errorf("msgServer.CreateImportToken() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got == nil {
				t.Errorf("msgServer.CreateImportToken() error = %v, wantErr %v", got, "id not nil")
				return
			}

			newWallet, errNewWallet := k.walletKeeper.FetchWallet(sdk.WrapSDKContext(ctx), &tokenwalletTypes.MsgFetchGetWallet{Creator: creatorA, Id: ImportTokenWalletId})
			if errNewWallet != nil {
				t.Errorf("msgServer.FetchWallet() error = %v", errNewWallet)
			}

			if len(oldWallet.Wallet.SegmentIds) == len(newWallet.Wallet.SegmentIds) {
				t.Error("Number of Segments has not changed, Segment has not been created?")
			}
		})
	}
}

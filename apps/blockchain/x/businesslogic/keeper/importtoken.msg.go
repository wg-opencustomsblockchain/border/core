//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"
	"fmt"
	"strconv"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	exportTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/exporttoken/types"
	importTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/importtoken/types"
	walletTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
	"github.com/google/uuid"
	"github.com/jinzhu/copier"
)

func (k msgServer) CreateImportToken(goCtx context.Context, msg *types.MsgCreateImportToken) (*types.MsgCreateImportTokenResponse, error) {

	check, err := k.checkPermissionImport(goCtx, msg.Creator, importTypes.ImportTokenRoleCreate)
	if err != nil || !check {
		return nil, err
	}
	tmp := ConvertToImportMsgCreateImportToken(*msg)
	out, err := k.importTokenKeeper.CreateImportToken(goCtx, &tmp)

	if err != nil {
		return nil, err
	}

	segment, errCreateSegment := k.walletKeeper.CreateSegmentWithId(goCtx, &walletTypes.MsgCreateSegmentWithId{
		Creator:  msg.Creator,
		Id:       uuid.New().String(),
		Name:     out.GetImportToken().GetId(),
		Info:     out.GetImportToken().GetId(),
		WalletId: msg.TokenWalletId,
	})
	if errCreateSegment != nil {
		return nil, errCreateSegment
	}

	k.walletKeeper.CreateTokenRef(goCtx, &walletTypes.MsgCreateTokenRef{
		Creator:   msg.Creator,
		Id:        out.ImportToken.Id,
		ModuleRef: "ImportToken",
		SegmentId: segment.Id,
	})

	mapping, errMapping := k.exportTokenKeeper.FetchExportToken(goCtx, &exportTypes.MsgFetchExportToken{
		Creator: msg.Creator,
		Id:      msg.RelatedExportToken,
	})
	if errMapping != nil {
		return nil, errMapping
	}

	ctx := sdk.UnwrapSDKContext(goCtx)

	// Delete the Export Notice in the Key Value store.
	// Set the EORI as a key. If not applicable create a key from CountryCode and Name.
	var noticeKey string
	if mapping.ExportToken.Consignee.CustomsId == "" {
		noticeKey = BuildKey(mapping.ExportToken.Consignee.Name, mapping.ExportToken.Consignee.Address.CountryCode)
	} else {
		noticeKey = mapping.ExportToken.Consignee.CustomsId
	}

	k.RemoveExportNotice(ctx, mapping.ExportToken.Id, noticeKey)

	tmp2 := types.MsgCreateImportTokenResponse(*out)
	return &tmp2, nil
}

func (k msgServer) UpdateImportToken(goCtx context.Context, msg *types.MsgUpdateImportToken) (*types.MsgUpdateImportTokenResponse, error) {

	check, err := k.checkPermissionImport(goCtx, msg.Creator, importTypes.ImportTokenRoleUpdate)
	if err != nil || !check {
		return nil, err
	}

	tokenResponse, errTokenResponse := k.importTokenKeeper.FetchImportToken(goCtx, &importTypes.MsgFetchImportToken{
		Creator: msg.Creator,
		Id:      msg.Id,
	})
	if errTokenResponse != nil {
		return &types.MsgUpdateImportTokenResponse{}, errTokenResponse
	}

	status := tokenResponse.Events[len(tokenResponse.Events)-1].Status
	if !(status == "1" || status == "2") {
		return nil, sdkerrors.Wrap(types.ErrTokenWrongStatus, fmt.Sprint("token cant be updated, current status is %s", status))
	}

	tmp := importTypes.MsgUpdateImportToken(*msg)
	out, err := k.importTokenKeeper.UpdateImportToken(goCtx, &tmp)

	tmp2 := types.MsgUpdateImportTokenResponse(*out)
	return &tmp2, err
}

func (k msgServer) CreateImportEvent(goCtx context.Context, msg *types.MsgCreateImportEvent) (*types.MsgCreateImportEventResponse, error) {
	tmp := importTypes.MsgCreateEvent(*msg)
	out, err := k.importTokenKeeper.CreateImportEvent(goCtx, &tmp)

	tmp2 := types.MsgCreateImportEventResponse(*out)
	return &tmp2, err
}

func ConvertToImportMsgCreateImportToken(token types.MsgCreateImportToken) importTypes.MsgCreateImportToken {
	out := importTypes.MsgCreateImportToken{}
	copier.Copy(&out, &token)
	return out
}

func (k msgServer) FetchImportToken(goCtx context.Context, msg *types.MsgFetchImportToken) (*types.MsgFetchImportTokenResponse, error) {

	check, err := k.checkPermissionImport(goCtx, msg.Creator, importTypes.ImportTokenRoleReadToken)
	if err != nil || !check {
		return nil, err
	}

	tokenResponse, errTokenResponse := k.importTokenKeeper.FetchImportToken(goCtx, &importTypes.MsgFetchImportToken{
		Creator: msg.Creator,
		Id:      msg.Id,
	})
	if errTokenResponse != nil {
		return &types.MsgFetchImportTokenResponse{}, errTokenResponse
	}

	return &types.MsgFetchImportTokenResponse{
		ImportToken: tokenResponse.ImportToken,
		Events:      tokenResponse.Events,
	}, nil
}

func (k msgServer) FetchImportTokenByImportId(goCtx context.Context, msg *types.MsgFetchImportTokenByImportId) (*types.MsgFetchImportTokenResponse, error) {

	check, err := k.checkPermissionImport(goCtx, msg.Creator, importTypes.ImportTokenRoleReadToken)
	if err != nil || !check {
		return nil, err
	}

	tokenResponse, errTokenResponse := k.importTokenKeeper.FetchImportMapping(goCtx, &importTypes.MsgFetchImportMapping{
		Creator: msg.Creator,
		Id:      msg.ImportId,
	})

	if errTokenResponse != nil {
		return &types.MsgFetchImportTokenResponse{}, errTokenResponse
	}

	return &types.MsgFetchImportTokenResponse{
		ImportToken: tokenResponse.ImportToken,
		Events:      tokenResponse.Events,
	}, nil
}

func (k msgServer) FetchImportTokenAll(goCtx context.Context, msg *types.MsgFetchImportTokenAll) (*types.MsgFetchImportTokenAllResponse, error) {

	check, err := k.checkPermissionImport(goCtx, msg.Creator, importTypes.ImportTokenRoleReadToken)
	if err != nil || !check {
		return nil, err
	}

	segResponse, segError := k.walletKeeper.FetchSegmentAll(goCtx, &walletTypes.MsgFetchAllSegment{
		Creator:    msg.Creator,
		Pagination: nil,
	})

	if segError != nil {
		return &types.MsgFetchImportTokenAllResponse{}, segError
	}

	var tokens []*types.MsgCreateImportTokenResponse
	for _, seg := range segResponse.Segment {
		if seg.WalletId == msg.TokenWalletId {
			for _, tokenRef := range seg.TokenRefs {
				if tokenRef.ModuleRef == "ImportToken" {
					tokenResponse, errTokenResponse := k.importTokenKeeper.FetchImportToken(goCtx, &importTypes.MsgFetchImportToken{
						Creator: msg.Creator,
						Id:      tokenRef.Id,
					})
					if errTokenResponse != nil {
						return &types.MsgFetchImportTokenAllResponse{}, errTokenResponse
					}
					tokens = append(tokens, &types.MsgCreateImportTokenResponse{
						ImportToken: tokenResponse.ImportToken,
						Events:      tokenResponse.Events,
					})
				}
			}
		}
	}
	return &types.MsgFetchImportTokenAllResponse{
		Tokens: tokens,
	}, nil
}

// ----- Status Transactions -----

func (k msgServer) handleEventUpdateImport(goCtx context.Context, index string, creator string, status string, message string, eventType string) (*types.MsgEventStatusResponseImport, error) {
	//ctx := sdk.UnwrapSDKContext(goCtx)
	tokenResponse, errTokenResponse := k.importTokenKeeper.FetchImportToken(goCtx, &importTypes.MsgFetchImportToken{
		Creator: creator,
		Id:      index,
	})
	invalidUpdateError := sdkerrors.Wrapf(sdkerrors.ErrInvalidRequest, "%s", fmt.Errorf("invalid status update operation"))
	currStat, _ := strconv.Atoi((tokenResponse.Events[len(tokenResponse.Events)-1].Status))

	if errTokenResponse != nil {
		return &types.MsgEventStatusResponseImport{}, errTokenResponse
	}
	if status != "" {
		// We expect the status to move up if its set.
		nextStat, _ := strconv.Atoi(status)
		if currStat >= nextStat {
			return &types.MsgEventStatusResponseImport{}, invalidUpdateError
		}
	} else {
		// throw error because editing is only in status lower than 2 possible
		if eventType == "IMPORT_SUPPLEMENT" && currStat > 1 {
			return &types.MsgEventStatusResponseImport{}, invalidUpdateError
		}
		// Otherwise set the status to the current status in case we have a non status changing transaction.
		status = tokenResponse.Events[len(tokenResponse.Events)-1].Status
	}

	k.importTokenKeeper.CreateImportEvent(goCtx, &importTypes.MsgCreateEvent{
		Creator:   creator,
		Index:     index,
		Status:    status,
		Message:   message,
		EventType: eventType,
	})

	tokenResponse, errTokenResponse = k.importTokenKeeper.FetchImportToken(goCtx, &importTypes.MsgFetchImportToken{
		Creator: creator,
		Id:      index,
	})

	result := types.MsgEventStatusResponseImport(*tokenResponse)

	return &result, errTokenResponse
}

func (k msgServer) EventAcceptImport(goCtx context.Context, msg *types.MsgEventAcceptImport) (*types.MsgEventStatusResponseImport, error) {
	check, err := k.checkPermissionImport(goCtx, msg.Creator, importTypes.ImportTokenRoleAccept)
	if err != nil || !check {
		return nil, err
	}
	return k.handleEventUpdateImport(goCtx, msg.Id, msg.Creator, "1", "Accept Import", "IMPORT_ACCEPTED")
}

func (k msgServer) EventImportSupplement(goCtx context.Context, msg *types.MsgEventImportSupplement) (*types.MsgEventStatusResponseImport, error) {
	check, err := k.checkPermissionImport(goCtx, msg.Creator, importTypes.ImportTokenRoleSupplement)
	if err != nil || !check {
		return nil, err
	}
	return k.handleEventUpdateImport(goCtx, msg.Id, msg.Creator, "", "Supplement Import Data", "IMPORT_SUPPLEMENT")
}

func (k msgServer) EventCompleteImport(goCtx context.Context, msg *types.MsgEventCompleteImport) (*types.MsgEventStatusResponseImport, error) {
	check, err := k.checkPermissionImport(goCtx, msg.Creator, importTypes.ImportTokenRoleComplete)
	if err != nil || !check {
		return nil, err
	}
	return k.handleEventUpdateImport(goCtx, msg.Id, msg.Creator, "2", "Complete Import", "IMPORT_COMPLETED")
}

func (k msgServer) EventImportProposal(goCtx context.Context, msg *types.MsgEventImportProposal) (*types.MsgEventStatusResponseImport, error) {
	check, err := k.checkPermissionImport(goCtx, msg.Creator, importTypes.ImportTokenRolePropose)
	if err != nil || !check {
		return nil, err
	}
	return k.handleEventUpdateImport(goCtx, msg.Id, msg.Creator, "3", "Import Proposal", "IMPORT_PROPOSAL")
}

func (k msgServer) EventImportRefusal(goCtx context.Context, msg *types.MsgEventImportRefusal) (*types.MsgEventStatusResponseImport, error) {
	check, err := k.checkPermissionImport(goCtx, msg.Creator, importTypes.ImportTokenRoleRefuse)
	if err != nil || !check {
		return nil, err
	}
	return k.handleEventUpdateImport(goCtx, msg.Id, msg.Creator, "4", "Import Refusal", "IMPORT_REFUSED")
}

// SOON TO BE IMPLEMENTED
func (k msgServer) EventImportExport(goCtx context.Context, msg *types.MsgEventImportExport) (*types.MsgEventStatusResponseImport, error) {
	check, err := k.checkPermissionImport(goCtx, msg.Creator, importTypes.ImportTokenRoleExport)
	if err != nil || !check {
		return nil, err
	}
	return k.handleEventUpdateImport(goCtx, msg.Id, msg.Creator, "", "Export Import", "IMPORT_EXPORTED")
}

// SOON TO BE IMPLEMENTED
func (k msgServer) EventImportSubmit(goCtx context.Context, msg *types.MsgEventImportSubmit) (*types.MsgEventStatusResponseImport, error) {

	check, err := k.checkPermissionImport(goCtx, msg.Creator, importTypes.ImportTokenRoleSubmit)
	if err != nil || !check {
		return nil, err
	}
	return k.handleEventUpdateImport(goCtx, msg.Id, msg.Creator, "", "Submit Import Data", "IMPORT_SUBMITTED")
}

// SOON TO BE IMPLEMENTED
func (k msgServer) EventImportRelease(goCtx context.Context, msg *types.MsgEventImportRelease) (*types.MsgEventStatusResponseImport, error) {

	check, err := k.checkPermissionImport(goCtx, msg.Creator, importTypes.ImportTokenRoleRelease)
	if err != nil || !check {
		return nil, err
	}
	return k.handleEventUpdateImport(goCtx, msg.Id, msg.Creator, "", "Release Import", "IMPORT_RELEASE")
}

func (k msgServer) checkPermissionImport(goCtx context.Context, creator string, role string) (bool, error) {
	// Permission check
	check, err := k.authorizationKeeper.HasRole(sdk.UnwrapSDKContext(goCtx), creator, role)
	if err != nil || !check {
		return false, sdkerrors.Wrapf(sdkerrors.ErrUnauthorized, "%s", fmt.Errorf("unauthorized transaction: missing role"))
	}
	return true, nil
}

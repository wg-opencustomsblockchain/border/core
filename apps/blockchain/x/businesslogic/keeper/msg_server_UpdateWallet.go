//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	authorizationTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
	walletTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
)

func (k msgServer) UpdateWallet(goCtx context.Context, msg *types.MsgUpdateWallet) (*walletTypes.MsgEmptyResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Permission check
	check, err := k.authorizationKeeper.HasRole(ctx, msg.Creator, authorizationTypes.WalletUpdateWallet)
	if err != nil || !check {
		return &walletTypes.MsgEmptyResponse{}, sdkerrors.Wrap(authorizationTypes.ErrNoPermission, authorizationTypes.WalletUpdateWallet)
	}
	res, error := k.walletKeeper.UpdateWallet(goCtx, &walletTypes.MsgUpdateWallet{
		Creator: msg.Creator,
		Id:      msg.Id,
		Name:    msg.Name,
	})

	if error != nil {
		return &walletTypes.MsgEmptyResponse{}, error
	}

	return res, nil
}

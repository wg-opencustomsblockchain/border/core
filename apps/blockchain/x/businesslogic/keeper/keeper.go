// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
package keeper

import (
	"fmt"
	"github.com/tendermint/tendermint/libs/log"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	"github.com/cosmos/cosmos-sdk/codec"
	sdk "github.com/cosmos/cosmos-sdk/types"
)

type (
	Keeper struct {
		cdc                 codec.Codec
		storeKey            sdk.StoreKey
		walletKeeper        types.WalletKeeper
		exportTokenKeeper   types.ExportTokenKeeper
		importTokenKeeper   types.ImportTokenKeeper
		authorizationKeeper types.AuthorizationKeeper
		tokenKeeper         types.TokenKeeper
		memKey              sdk.StoreKey
	}
)

func NewKeeper(
	cdc codec.Codec,
	storeKey sdk.StoreKey,
	walletKeeper types.WalletKeeper,
	authorizationKeeper types.AuthorizationKeeper,
	exportTokenKeeper types.ExportTokenKeeper,
	importTokenKeeper types.ImportTokenKeeper,
	tokenKeeper types.TokenKeeper,
	memKey sdk.StoreKey) *Keeper {
	return &Keeper{
		cdc:                 cdc,
		storeKey:            storeKey,
		walletKeeper:        walletKeeper,
		exportTokenKeeper:   exportTokenKeeper,
		importTokenKeeper:   importTokenKeeper,
		authorizationKeeper: authorizationKeeper,
		tokenKeeper:         tokenKeeper,
		memKey:              memKey,
	}
}

func (k Keeper) Logger(ctx sdk.Context) log.Logger {
	return ctx.Logger().With("module", fmt.Sprintf("x/%s", types.ModuleName))
}

// InitGenesis initializes the capability module's state from a provided genesis
// state.
func (k Keeper) InitGenesis(ctx sdk.Context, genState types.GenesisState) {
	// this line is used by starport scaffolding # genesis/module/init
}

func (k Keeper) RevertToGenesis(ctx sdk.Context) {
	store := ctx.KVStore(k.storeKey)
	it := store.Iterator(nil, nil)
	defer it.Close()

	for ; it.Valid(); it.Next() {
		store.Delete(it.Key())
	}

	k.InitGenesis(ctx, *types.DefaultGenesis())
}

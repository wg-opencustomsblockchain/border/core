//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package keeper

// this line is used by starport scaffolding # 2

// --- Token ---
// Queries all existing Export Tokens
//func (k Keeper) ExportTokenAll(c context.Context, req *types.QueryAllExportTokenRequest) (*types.QueryAllExportTokenResponse, error) {
//	return k.exportTokenKeeper.ExportTokenAll(c, req)
//}
//
//// Queries an existing Export Token by its Id
//func (k Keeper) ExportToken(c context.Context, req *types.QueryGetExportTokenRequest) (*types.QueryGetExportTokenResponse, error) {
//	return k.exportTokenKeeper.ExportToken(c, req)
//}
//
//// --- Mapping ---
//// Queries an Export Token by its MRN/Or in the future its process ID.
//func (k Keeper) ExportMapping(c context.Context, req *types.QueryGetExportMappingRequest) (*types.QueryGetExportMappingResponse, error) {
//	return k.exportTokenKeeper.ExportMapping(c, req)
//}
//
//// --- Event History ---
//// ExportEventHistory return the event history of a token
//func (k Keeper) ExportEventHistory(c context.Context, req *types.QueryGetEventHistoryRequest) (*types.QueryGetEventHistoryResponse, error) {
//	return k.exportTokenKeeper.ExportEventHistory(c, req)
//}

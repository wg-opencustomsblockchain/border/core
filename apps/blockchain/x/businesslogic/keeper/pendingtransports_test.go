// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
package keeper

import (
	exportTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/exporttoken/types"
	"testing"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	tokenwalletTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
)

func Test_PendingTransport(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	creatorA := "cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462y"

	goCtx := sdk.WrapSDKContext(ctx)

	k := msgServer{
		Keeper: *keeper,
	}
	_, errCreateExportWallet := k.walletKeeper.CreateWalletWithId(sdk.WrapSDKContext(ctx),
		tokenwalletTypes.NewMsgCreateWalletWithId(creatorA, ExportTokenWalletId, ExportTokenWalletId))
	if errCreateExportWallet != nil {
		t.Errorf("msgServer.CreateWalletWithId() error = %v", errCreateExportWallet)
	}

	_, errCreateImportWallet := k.walletKeeper.CreateWalletWithId(sdk.WrapSDKContext(ctx),
		tokenwalletTypes.NewMsgCreateWalletWithId(creatorA, ImportTokenWalletId, ImportTokenWalletId))
	if errCreateImportWallet != nil {
		t.Errorf("msgServer.CreateWalletWithId() error = %v", errCreateImportWallet)
	}

	token, errToken := k.CreateExportToken(goCtx, &types.MsgCreateExportToken{
		Creator:       creatorA,
		TokenWalletId: ExportTokenWalletId,
		ExportId:      ExportId,
		Consignee: &exportTypes.Company{
			Address: &exportTypes.Address{
				StreetAndNumber: "",
				Postcode:        "",
				City:            "",
				CountryCode:     CompanyCountryCode,
			},
			CustomsId:        "",
			Name:             CompanyName,
			SubsidiaryNumber: "",
		},
	})
	if errToken != nil {
		t.Errorf("msgServer.CreateExportToken() error = %v", errToken)
		return
	}

	notices := k.GetTransportNotices(ctx)

	if len(notices) > 0 {
		t.Errorf("Notices not empty (%d present at start)", len(notices))
		return
	}

	// Ready for pickup
	_, errUpdateStatus2 := k.UpdateExportToken(goCtx, &types.MsgUpdateExportToken{
		Creator:  creatorA,
		Id:       token.ExportToken.Id,
		ExportId: ExportId,
		Status:   "2",
	})
	if errUpdateStatus2 != nil {
		t.Errorf("msgServer.UpdateExportToken() error = %v", errUpdateStatus2)
	}

	should := types.TransportNotice{
		ExportTokenId: token.ExportToken.Id,
		CarrierUser:   "",
	}
	notices = k.GetTransportNotices(ctx)
	if len(notices) != 1 {
		t.Errorf("Wrong count of notices (%d present, should be 1)", len(notices))
		return
	}

	if *notices[0] != should {
		t.Errorf("Wrong notice (%v present, should be %v)", notices[0], should)
		return
	}

	// Pickup complete
	_, errUpdateStatus3 := k.UpdateExportToken(goCtx, &types.MsgUpdateExportToken{
		Creator:  creatorA,
		Id:       token.ExportToken.Id,
		ExportId: ExportId,
		Status:   "3",
	})
	if errUpdateStatus3 != nil {
		t.Errorf("msgServer.UpdateExportToken() error = %v", errUpdateStatus3)
	}

	should.CarrierUser = creatorA
	notices = k.GetTransportNotices(ctx)
	if len(notices) != 1 {
		t.Errorf("Wrong count of notices (%d present, should be 1)", len(notices))
		return
	}

	if *notices[0] != should {
		t.Errorf("Wrong notice (%v present, should be %v)", notices[0], should)
		return
	}

	// Transport complete
	_, errUpdateStatus4 := k.UpdateExportToken(goCtx, &types.MsgUpdateExportToken{
		Creator:  creatorA,
		Id:       token.ExportToken.Id,
		ExportId: ExportId,
		Status:   "4",
	})
	if errUpdateStatus4 != nil {
		t.Errorf("msgServer.UpdateExportToken() error = %v", errUpdateStatus4)
	}

	notices = k.GetTransportNotices(ctx)
	if len(notices) != 0 {
		t.Errorf("Notice should have been deleted")
		return
	}
}

// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
package keeper

//func (k msgServer) FetchTokensBySegmentId(goCtx context.Context, msg *types.MsgFetchTokensBySegmentId) (*types.QueryAllTokenResponse, error) {
//	ctx := sdk.UnwrapSDKContext(goCtx)
//
//	// Permission check
//	authorizationKeeper := k.authorizationKeeper
//	check, err := authorizationKeeper.HasRole(ctx, msg.Creator, auhtorizationTypes.BusinessLogicGetTokensBySegmentId)
//	if err != nil || !check {
//		return &types.QueryAllTokenResponse{}, sdkerrors.Wrap(auhtorizationTypes.ErrNoPermission, auhtorizationTypes.BusinessLogicGetTokensBySegmentId)
//	}
//
//	res, error := k.TokensBySegmentId(goCtx, &types.QueryAllTokenByIdRequest{
//		Id: msg.Id,
//	})
//	if error != nil {
//		return &types.QueryAllTokenResponse{}, error
//	}
//
//	return &types.QueryAllTokenResponse{Token: res.Token, HashTokens: res.HashTokens}, nil
//}

//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"
	"encoding/json"
	"fmt"
	tokenTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/token/types"
	"strconv"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	exportTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/exporttoken/types"
	walletTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
	"github.com/google/uuid"
	"github.com/jinzhu/copier"
)

func (k msgServer) checkWallet(goCtx context.Context, creator string, walletId string) (*walletTypes.Wallet, error) {
	// check if wallet exists
	walletResponse, errWallet := k.walletKeeper.FetchWallet(goCtx, walletTypes.NewMsgFetchGetWallet(creator, walletId))
	if errWallet != nil || walletResponse.Wallet.Id != walletId {
		return nil, types.ErrWalletNotFound
	}

	accounts := walletResponse.Wallet.WalletAccounts
	found := false
	for _, acc := range accounts {
		if acc.Address == creator {
			found = true
		}
	}
	if !found {
		return nil, types.ErrWalletAccessDenied
	}
	return walletResponse.Wallet, nil
}

func (k msgServer) checkToken(goCtx context.Context, creator string, tokenId string) (*exportTypes.MsgFetchExportTokenResponse, *walletTypes.Wallet, error) {

	tokenResponse, errTokenReponse := k.exportTokenKeeper.FetchExportToken(goCtx, &exportTypes.MsgFetchExportToken{
		Creator: creator,
		Id:      tokenId,
	})
	if errTokenReponse != nil {
		return nil, nil, errTokenReponse
	}
	// Todo: Fix in Basis Component @ Uwe - Chris
	token := tokenResponse.ExportToken
	wallet, errWallet := k.checkWallet(goCtx, creator, token.Exporter.CustomsId)
	if errWallet != nil {
		return nil, nil, errWallet
	}

	return tokenResponse, wallet, nil

}

func (k msgServer) CreateExportToken(goCtx context.Context, msg *types.MsgCreateExportToken) (*types.MsgCreateExportTokenResponse, error) {
	// check if wallet exists
	wallet, errWallet := k.checkWallet(goCtx, msg.Creator, msg.Exporter.CustomsId)
	if errWallet != nil {
		return nil, errWallet
	}

	check, err := k.checkPermissionExport(goCtx, msg.Creator, exportTypes.ExportTokenRoleCreateToken)
	if err != nil || !check {
		return nil, err
	}

	tmp := ConvertToExportMsgCreateExportToken(*msg)
	out, err := k.exportTokenKeeper.CreateExportToken(goCtx, &tmp)
	if err != nil {
		return nil, err
	}

	k.walletKeeper.CreateTokenRef(goCtx, &walletTypes.MsgCreateTokenRef{
		Creator:   msg.Creator,
		Id:        out.ExportToken.Id,
		ModuleRef: "ExportToken",
		SegmentId: GetIncomingSegmentName(wallet.Id),
	})

	tmp2 := types.MsgCreateExportTokenResponse(*out)
	return &tmp2, nil
}

func (k msgServer) ForwardExportToken(goCtx context.Context, msg *types.MsgForwardExportToken) (*types.MsgForwardExportTokenResponse, error) {

	check, err := k.checkPermissionExport(goCtx, msg.Creator, exportTypes.ExportTokenEventRoleForward)
	if err != nil || !check {
		return nil, err
	}

	tokenResponse, wallet, errToken := k.checkToken(goCtx, msg.Creator, msg.Id)
	if errToken != nil {
		return nil, errToken
	}

	info, errInfo := json.Marshal(types.ExportSegmentInfo{
		ExportId: tokenResponse.ExportToken.ExportId,
		LRN:      tokenResponse.ExportToken.LocalReferenceNumber,
	})
	if errInfo != nil {
		return nil, errInfo
	}

	segment, errCreateSegment := k.walletKeeper.CreateSegmentWithId(goCtx, &walletTypes.MsgCreateSegmentWithId{
		Creator:  msg.Creator,
		Id:       uuid.New().String(),
		Name:     tokenResponse.ExportToken.ExportId,
		Info:     string(info),
		WalletId: wallet.Id,
	})
	if errCreateSegment != nil {
		return nil, errCreateSegment
	}

	k.walletKeeper.MoveTokenToSegment(goCtx, walletTypes.NewMsgMoveTokenToSegment(msg.Creator, tokenResponse.ExportToken.Id,
		GetIncomingSegmentName(tokenResponse.ExportToken.Exporter.CustomsId), segment.Id))

	status := tokenResponse.Events[len(tokenResponse.Events)-1].Status
	if status != "0" {
		return nil, sdkerrors.Wrap(types.ErrTokenNotNew, fmt.Sprint("Token can't be forwarded, current status is '%s'", status))
	}

	k.exportTokenKeeper.CreateExportEvent(goCtx, &exportTypes.MsgCreateEvent{
		Creator:   msg.Creator,
		Index:     msg.Id,
		Status:    "1",
		Message:   "Forwarded",
		EventType: "EXPORT_CHECK",
	})

	out, err := k.exportTokenKeeper.FetchExportToken(goCtx, &exportTypes.MsgFetchExportToken{
		Creator: msg.Creator,
		Id:      msg.Id,
	})

	// Create an Export Notice in the Key Value store.
	// Set the EORI as a key. If not create a key from CountryCode and Name.
	var noticeKey string
	if out.ExportToken.Consignee.CustomsId == "" {
		noticeKey = BuildKey(out.ExportToken.Consignee.Name, out.ExportToken.Consignee.Address.CountryCode)
	} else {
		noticeKey = out.ExportToken.Consignee.CustomsId
	}
	ctx := sdk.UnwrapSDKContext(goCtx)
	k.AddExportNotice(ctx, out.ExportToken.Id, noticeKey)

	return &types.MsgForwardExportTokenResponse{
		ExportToken: out.ExportToken,
		Events:      out.Events,
	}, err
}

func (k msgServer) UpdateExportToken(goCtx context.Context, msg *types.MsgUpdateExportToken) (*types.MsgUpdateExportTokenResponse, error) {

	tmp := exportTypes.MsgUpdateExportToken(*msg)
	out, err := k.exportTokenKeeper.UpdateExportToken(goCtx, &tmp)

	ctx := sdk.UnwrapSDKContext(goCtx)
	switch msg.Status {
	case "2":
		k.AddTransportNotice(ctx, msg.Id)
	case "3":
		k.UpdateTransportNotice(ctx, msg.Id, msg.Creator)
	case "4":
		k.RemoveTransportNotice(ctx, msg.Id)
	}

	tmp2 := types.MsgUpdateExportTokenResponse(*out)
	return &tmp2, err
}

// ----- Status Transactions -----

func (k msgServer) handleEventUpdateExport(goCtx context.Context, index string, creator string, status string, message string, eventType string) (*types.MsgEventStatusResponseExport, error) {

	ctx := sdk.UnwrapSDKContext(goCtx)
	tokenResponse, errTokenReponse := k.exportTokenKeeper.FetchExportToken(goCtx, &exportTypes.MsgFetchExportToken{
		Creator: creator,
		Id:      index,
	})

	if errTokenReponse != nil {
		return &types.MsgEventStatusResponseExport{}, errTokenReponse
	}
	if status != "" {
		// We expect the status to move up if its set.
		currStat, _ := strconv.Atoi((tokenResponse.Events[len(tokenResponse.Events)-1].Status))
		nextStat, _ := strconv.Atoi(status)
		if (currStat + 1) != nextStat {
			newError := sdkerrors.Wrapf(sdkerrors.ErrInvalidRequest, "%s", fmt.Errorf("invalid status update operation"))
			return &types.MsgEventStatusResponseExport{}, newError
		}
	} else {
		// Otherwise set the status to the current status in case we have a non status changing transaction.
		status = tokenResponse.Events[len(tokenResponse.Events)-1].Status
	}

	k.exportTokenKeeper.CreateExportEvent(goCtx, &exportTypes.MsgCreateEvent{
		Creator:   creator,
		Index:     index,
		Status:    status,
		Message:   message,
		EventType: eventType,
	})

	tokenResponse, errTokenReponse = k.exportTokenKeeper.FetchExportToken(goCtx, &exportTypes.MsgFetchExportToken{
		Creator: creator,
		Id:      index,
	})

	switch status {
	case "2":
		k.AddTransportNotice(ctx, index)
	case "3":
		k.UpdateTransportNotice(ctx, index, creator)
	case "4":
		k.RemoveTransportNotice(ctx, index)
	}

	result := types.MsgEventStatusResponseExport(*tokenResponse)

	return &result, errTokenReponse
}

func (k msgServer) EventPresentQrCode(goCtx context.Context, msg *types.MsgEventPresentQrCode) (*types.MsgEventStatusResponseExport, error) {

	check, err := k.checkPermissionExport(goCtx, msg.Creator, exportTypes.ExportTokenRoleCreateToken)
	if err != nil || !check {
		return nil, err
	}
	return k.handleEventUpdateExport(goCtx, msg.Id, msg.Creator, "2", "Show Qr Code", "EXPORT_READY")
}

func (k msgServer) EventAcceptGoodsForTransport(goCtx context.Context, msg *types.MsgEventAcceptGoodsForTransport) (*types.MsgEventStatusResponseExport, error) {
	check, err := k.checkPermissionExport(goCtx, msg.Creator, exportTypes.ExportTokenRoleEventPickup)
	if err != nil || !check {
		return nil, err
	}
	return k.handleEventUpdateExport(goCtx, msg.Id, msg.Creator, "3", "Pickup Goods", "EXPORT_PICKUP")
}

func (k msgServer) EventPresentGoods(goCtx context.Context, msg *types.MsgEventPresentGoods) (*types.MsgEventStatusResponseExport, error) {
	check, err := k.checkPermissionExport(goCtx, msg.Creator, exportTypes.ExportTokenRoleEventPresent)
	if err != nil || !check {
		return nil, err
	}
	return k.handleEventUpdateExport(goCtx, msg.Id, msg.Creator, "4", "Presented the Export", "EXPORT_PRESENTED")
}

func (k msgServer) EventCertifyExitOfGoods(goCtx context.Context, msg *types.MsgEventCertifyExitOfGoods) (*types.MsgEventStatusResponseExport, error) {
	check, err := k.checkPermissionExport(goCtx, msg.Creator, exportTypes.ExportTokenRoleEventExit)
	if err != nil || !check {
		return nil, err
	}
	return k.handleEventUpdateExport(goCtx, msg.Id, msg.Creator, "5", "Certified Goods Exit", "EXPORT_EXIT")
}

func (k msgServer) EventMoveToArchive(goCtx context.Context, msg *types.MsgEventMoveToArchive) (*types.MsgEventStatusResponseExport, error) {

	check, err := k.checkPermissionExport(goCtx, msg.Creator, exportTypes.ExportTokenRoleEventArchive)
	if err != nil || !check {
		return nil, err
	}
	return k.handleEventUpdateExport(goCtx, msg.Id, msg.Creator, "6", "Moved Export Token to Archive", "EXPORT_ARCHIVED")
}

func (k msgServer) EventExportCheck(goCtx context.Context, msg *types.MsgEventExportCheck) (*types.MsgEventStatusResponseExport, error) {
	check, err := k.checkPermissionExport(goCtx, msg.Creator, exportTypes.ExportTokenRoleMobileCheck)
	if err != nil || !check {
		return nil, err
	}
	return k.handleEventUpdateExport(goCtx, msg.Id, msg.Creator, "", "Export mobile check", "EXPORT_MOBILECHECK")
}

func (k msgServer) EventExportRefusal(goCtx context.Context, msg *types.MsgEventExportRefusal) (*types.MsgEventStatusResponseExport, error) {
	check, err := k.checkPermissionExport(goCtx, msg.Creator, exportTypes.ExportTokenRoleExportRefusal)
	if err != nil || !check {
		return nil, err
	}
	return k.handleEventUpdateExport(goCtx, msg.Id, msg.Creator, "", "Export refused", "EXPORT_REFUSAL")
}

func (k msgServer) EventAcceptExport(goCtx context.Context, msg *types.MsgEventAcceptExport) (*types.MsgEventStatusResponseExport, error) {
	check, err := k.checkPermissionExport(goCtx, msg.Creator, exportTypes.ExportTokenRoleExportAccepted)
	if err != nil || !check {
		return nil, err
	}
	return k.handleEventUpdateExport(goCtx, msg.Id, msg.Creator, "", "Export Accepted", "EXPORT_ACCEPTED")
}

func (k msgServer) CreateExportEvent(goCtx context.Context, msg *types.MsgCreateExportEvent) (*types.MsgCreateExportEventResponse, error) {

	tmp := exportTypes.MsgCreateEvent(*msg)
	out, err := k.exportTokenKeeper.CreateExportEvent(goCtx, &tmp)

	tmp2 := types.MsgCreateExportEventResponse(*out)
	return &tmp2, err
}

func ConvertToExportMsgCreateExportToken(token types.MsgCreateExportToken) exportTypes.MsgCreateExportToken {
	out := exportTypes.MsgCreateExportToken{}
	copier.Copy(&out, &token)
	return out
}

func (k msgServer) handleExportTokenQuery(goCtx context.Context, index string, creator string) (*types.MsgFetchExportTokenResponse, error) {

	tokenResponse, _, errToken := k.checkToken(goCtx, creator, index)
	if errToken != nil {
		return nil, errToken
	}

	return &types.MsgFetchExportTokenResponse{
		ExportToken: tokenResponse.ExportToken,
		Events:      tokenResponse.Events,
	}, nil
}

func (k msgServer) FetchExportTokenExporter(goCtx context.Context, msg *types.MsgFetchExportTokenExporter) (*types.MsgFetchExportTokenExporterResponse, error) {

	check, err := k.checkPermissionExport(goCtx, msg.Creator, exportTypes.ExportTokenRoleReadToken)
	if err != nil || !check {
		return nil, err
	}
	query, err := k.handleExportTokenQuery(goCtx, msg.Id, msg.Creator)
	if err != nil {
		return nil, err
	}

	return convertToExporterResponse(query)
}

func (k msgServer) FetchExportTokenDriver(goCtx context.Context, msg *types.MsgFetchExportTokenDriver) (*types.MsgFetchExportTokenDriverResponse, error) {

	check, err := k.checkPermissionExport(goCtx, msg.Creator, exportTypes.ExportTokenRoleReadToken)
	if err != nil || !check {
		return nil, err
	}

	query, err := k.handleExportTokenQuery(goCtx, msg.Id, msg.Creator)
	if err != nil {
		return nil, err
	}

	return convertToDriverResponse(query)
}

func (k msgServer) FetchExportTokenImporter(goCtx context.Context, msg *types.MsgFetchExportTokenImporter) (*types.MsgFetchExportTokenImporterResponse, error) {

	check, err := k.checkPermissionExport(goCtx, msg.Creator, exportTypes.ExportTokenRoleReadToken)
	if err != nil || !check {
		return nil, err
	}
	query, err := k.handleExportTokenQuery(goCtx, msg.Id, msg.Creator)
	if err != nil {
		return nil, err
	}

	return convertToImporterResponse(query)
}

func (k msgServer) FetchExportTokenAllDriver(goCtx context.Context, msg *types.MsgFetchExportTokenAllDriver) (*types.MsgFetchExportTokenAllDriverResponse, error) {
	check, err := k.checkPermissionExport(goCtx, msg.Creator, exportTypes.ExportTokenRoleReadToken)
	if err != nil || !check {
		return nil, err
	}

	ctx := sdk.UnwrapSDKContext(goCtx)
	transportNotices := k.GetTransportNotices(ctx)

	tokens := []*types.MsgFetchExportTokenDriverResponse{}
	for _, notice := range transportNotices {
		if notice.CarrierUser == "" || notice.CarrierUser == msg.Creator {
			tokenResponse, err := k.exportTokenKeeper.FetchExportToken(goCtx, &exportTypes.MsgFetchExportToken{
				Id: notice.ExportTokenId,
			})
			if err == nil {
				driverResponse, _ := convertToDriverResponse(&types.MsgFetchExportTokenResponse{
					ExportToken: tokenResponse.ExportToken,
					Events:      tokenResponse.Events,
				})

				tokens = append(tokens, driverResponse)
			}
		}
	}

	return &types.MsgFetchExportTokenAllDriverResponse{
		Tokens: tokens,
	}, nil
}

func (k msgServer) FetchExportTokenAllExporter(goCtx context.Context, msg *types.MsgFetchExportTokenAllExporter) (*types.MsgFetchExportTokenAllExporterResponse, error) {

	check, err := k.checkPermissionExport(goCtx, msg.Creator, exportTypes.ExportTokenRoleReadToken)
	if err != nil || !check {
		return nil, err
	}

	_, errWallet := k.checkWallet(goCtx, msg.Creator, msg.TokenWalletId)
	if errWallet != nil {
		return nil, errWallet
	}

	segResponse, segError := k.walletKeeper.FetchSegmentAll(goCtx, &walletTypes.MsgFetchAllSegment{
		Creator:    msg.Creator,
		Pagination: nil,
	})

	if segError != nil {
		return &types.MsgFetchExportTokenAllExporterResponse{}, segError
	}

	tokens := []*types.MsgFetchExportTokenExporterResponse{}
	for _, seg := range segResponse.Segment {
		if seg.WalletId == msg.TokenWalletId {
			for _, tokenRef := range seg.TokenRefs {
				if tokenRef.ModuleRef == "ExportToken" {
					tokenResponse, errTokenReponse := k.exportTokenKeeper.FetchExportToken(goCtx, &exportTypes.MsgFetchExportToken{
						Id: tokenRef.Id,
					})
					if errTokenReponse != nil {
						return &types.MsgFetchExportTokenAllExporterResponse{}, errTokenReponse
					}
					exporterResponse, _ := convertToExporterResponse(&types.MsgFetchExportTokenResponse{
						ExportToken: tokenResponse.ExportToken,
						Events:      tokenResponse.Events,
					})

					tokens = append(tokens, exporterResponse)
				}
			}
		}
	}
	return &types.MsgFetchExportTokenAllExporterResponse{
		Tokens: tokens,
	}, nil
}

func (k msgServer) FetchExportTokenAllImporter(goCtx context.Context, msg *types.MsgFetchExportTokenAllImporter) (*types.MsgFetchExportTokenAllImporterResponse, error) {

	check, err := k.checkPermissionExport(goCtx, msg.Creator, exportTypes.ExportTokenRoleReadToken)
	if err != nil || !check {
		return nil, err
	}

	// Get All Export Notices by their Company Name and Identification Number
	ctx := sdk.UnwrapSDKContext(goCtx)
	key := BuildKey(msg.CompanyName, msg.CompanyCountry)
	tokenIdsByCompany := k.GetExportNotices(ctx, key)
	tokenIdsByIdNr := k.GetExportNotices(ctx, msg.IdentificationNumber)

	tokenIds := append(tokenIdsByCompany, tokenIdsByIdNr...)

	tokens := []*types.MsgFetchExportTokenImporterResponse{}
	for _, id := range tokenIds {
		tokenResponse, errToken := k.exportTokenKeeper.FetchExportToken(goCtx, &exportTypes.MsgFetchExportToken{
			Id: id,
		})
		if errToken != nil {
			return nil, errToken
		}
		importerResponse, _ := convertToImporterResponse(&types.MsgFetchExportTokenResponse{
			ExportToken: tokenResponse.ExportToken,
			Events:      tokenResponse.Events,
		})

		tokens = append(tokens, importerResponse)
	}

	return &types.MsgFetchExportTokenAllImporterResponse{
		Tokens: tokens,
	}, nil
}

// @Deprecated
func (k msgServer) FetchExportTokenExporterByExportId(goCtx context.Context, msg *types.MsgFetchExportTokenExporterByExportId) (*types.MsgFetchExportTokenResponse, error) {

	check, err := k.checkPermissionExport(goCtx, msg.Creator, exportTypes.ExportTokenRoleReadToken)
	if err != nil || !check {
		return nil, err
	}

	tokenResponse, errTokenReponse := k.exportTokenKeeper.FetchExportMapping(goCtx, &exportTypes.MsgFetchExportMapping{
		Creator: msg.Creator,
		Id:      msg.ExportId,
	})
	if errTokenReponse != nil {
		return &types.MsgFetchExportTokenResponse{}, errTokenReponse
	}

	return &types.MsgFetchExportTokenResponse{
		ExportToken: tokenResponse.ExportToken,
		Events:      tokenResponse.Events,
	}, nil
}

// @Deprecated
func (k msgServer) FetchExportTokenDriverByExportId(goCtx context.Context, msg *types.MsgFetchExportTokenDriverByExportId) (*types.MsgFetchExportTokenResponse, error) {

	check, err := k.checkPermissionExport(goCtx, msg.Creator, exportTypes.ExportTokenRoleReadToken)
	if err != nil || !check {
		return nil, err
	}

	tokenResponse, errTokenReponse := k.exportTokenKeeper.FetchExportMapping(goCtx, &exportTypes.MsgFetchExportMapping{
		Creator: msg.Creator,
		Id:      msg.ExportId,
	})
	if errTokenReponse != nil {
		return &types.MsgFetchExportTokenResponse{}, errTokenReponse
	}

	return &types.MsgFetchExportTokenResponse{
		ExportToken: tokenResponse.ExportToken,
		Events:      tokenResponse.Events,
	}, nil
}

// @Deprecated
func (k msgServer) FetchExportTokenImporterByExportId(goCtx context.Context, msg *types.MsgFetchExportTokenImporterByExportId) (*types.MsgFetchExportTokenResponse, error) {
	check, err := k.checkPermissionExport(goCtx, msg.Creator, exportTypes.ExportTokenRoleReadToken)
	if err != nil || !check {
		return nil, err
	}

	tokenResponse, errTokenReponse := k.exportTokenKeeper.FetchExportMapping(goCtx, &exportTypes.MsgFetchExportMapping{
		Creator: msg.Creator,
		Id:      msg.ExportId,
	})
	if errTokenReponse != nil {
		return &types.MsgFetchExportTokenResponse{}, errTokenReponse
	}

	return &types.MsgFetchExportTokenResponse{
		ExportToken: tokenResponse.ExportToken,
		Events:      tokenResponse.Events,
	}, nil
}

func (k msgServer) FetchTokenHistory(goCtx context.Context, msg *types.MsgFetchTokenHistory) (*types.MsgFetchTokenHistoryResponse, error) {
	res, _ := k.tokenKeeper.FetchTokenHistory(goCtx, &tokenTypes.MsgFetchTokenHistory{Id: msg.Id})
	return &types.MsgFetchTokenHistoryResponse{TokenHistory: res.TokenHistory}, nil
}

func (k msgServer) checkPermissionExport(goCtx context.Context, creator string, role string) (bool, error) {
	// Permission check
	check, err := k.authorizationKeeper.HasRole(sdk.UnwrapSDKContext(goCtx), creator, role)
	if err != nil || !check {
		return false, sdkerrors.Wrapf(sdkerrors.ErrUnauthorized, "%s", fmt.Errorf("unauthorized transaction: missing role"))
	}
	return true, nil
}

func convertToExporterResponse(in *types.MsgFetchExportTokenResponse) (*types.MsgFetchExportTokenExporterResponse, error) {
	out := types.MsgFetchExportTokenExporterResponse{
		ExportToken: &types.ExportTokenExporter{},
		Events:      in.Events,
	}
	copier.Copy(out.ExportToken, in.ExportToken)
	return &out, nil
}

func convertToDriverResponse(in *types.MsgFetchExportTokenResponse) (*types.MsgFetchExportTokenDriverResponse, error) {
	out := types.MsgFetchExportTokenDriverResponse{
		ExportToken: &types.ExportTokenDriver{},
		Events:      in.Events,
	}
	copier.Copy(out.ExportToken, in.ExportToken)
	return &out, nil
}

func convertToImporterResponse(in *types.MsgFetchExportTokenResponse) (*types.MsgFetchExportTokenImporterResponse, error) {
	events := []*exportTypes.ExportEvent{}

	for _, e := range in.Events {
		if e.Status == "1" || e.Status == "3" || e.Status == "4" || e.Status == "5" {
			events = append(events, e)
		}
	}

	out := types.MsgFetchExportTokenImporterResponse{
		ExportToken: &types.ExportTokenImporter{},
		Events:      events,
	}
	copier.Copy(out.ExportToken, in.ExportToken)
	return &out, nil
}

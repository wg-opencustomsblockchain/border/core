//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	authorizationTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
	walletTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
)

func (k msgServer) CreateSegment(goCtx context.Context, msg *types.MsgCreateSegment) (*walletTypes.MsgIdResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Permission check
	check, err := k.authorizationKeeper.HasRole(ctx, msg.Creator, authorizationTypes.WalletCreateSegment)
	if err != nil || !check {
		return &walletTypes.MsgIdResponse{}, sdkerrors.Wrap(authorizationTypes.ErrNoPermission, authorizationTypes.WalletCreateSegment)
	}

	res, error := k.walletKeeper.CreateSegment(goCtx, &walletTypes.MsgCreateSegment{
		Creator:  msg.Creator,
		Name:     msg.Name,
		Info:     msg.Info,
		WalletId: msg.WalletId,
	})

	if error != nil {
		return &walletTypes.MsgIdResponse{}, error
	}

	return res, nil
}

// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
package keeper

import (
	"testing"
	"time"

	"github.com/cosmos/cosmos-sdk/codec"
	codectypes "github.com/cosmos/cosmos-sdk/codec/types"
	"github.com/cosmos/cosmos-sdk/store"
	storetypes "github.com/cosmos/cosmos-sdk/store/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"github.com/tendermint/tendermint/libs/log"
	tmproto "github.com/tendermint/tendermint/proto/tendermint/types"
	tmdb "github.com/tendermint/tm-db"

	authorizationTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
	businesslogicTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	ExporttokenTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/exporttoken/types"
	ImporttokenTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/importtoken/types"
	tokenwallettypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"

	AuthorizationKeeper "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/keeper"
	ExporttokenKeeper "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/exporttoken/keeper"
	ImporttokenKeeper "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/importtoken/keeper"
	Walletkeeper "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/keeper"
)

func setupKeeper(t testing.TB) (*Keeper, sdk.Context) {
	storeKey := sdk.NewKVStoreKey(businesslogicTypes.StoreKey)
	memStoreKey := storetypes.NewMemoryStoreKey(businesslogicTypes.MemStoreKey)
	walletStoreKey := sdk.NewKVStoreKey(tokenwallettypes.StoreKey)
	authorizationKey := sdk.NewKVStoreKey(authorizationTypes.StoreKey)
	exporttokenKey := sdk.NewKVStoreKey(ExporttokenTypes.StoreKey)
	importtokenKey := sdk.NewKVStoreKey(ImporttokenTypes.StoreKey)

	db := tmdb.NewMemDB()
	stateStore := store.NewCommitMultiStore(db)
	stateStore.MountStoreWithDB(storeKey, sdk.StoreTypeIAVL, db)
	stateStore.MountStoreWithDB(walletStoreKey, sdk.StoreTypeIAVL, db)
	stateStore.MountStoreWithDB(authorizationKey, sdk.StoreTypeIAVL, db)
	stateStore.MountStoreWithDB(memStoreKey, sdk.StoreTypeMemory, nil)
	stateStore.MountStoreWithDB(exporttokenKey, sdk.StoreTypeIAVL, db)
	stateStore.MountStoreWithDB(importtokenKey, sdk.StoreTypeIAVL, db)
	require.NoError(t, stateStore.LoadLatestVersion())

	registry := codectypes.NewInterfaceRegistry()

	walletKeeper := *Walletkeeper.NewKeeper(
		codec.NewProtoCodec(registry),
		walletStoreKey,
		memStoreKey,
	)

	authorizationKeeper := *AuthorizationKeeper.NewKeeper(
		codec.NewProtoCodec(registry),
		authorizationKey,
		memStoreKey,
	)
	exporttokenKeeper := *ExporttokenKeeper.NewKeeper(
		codec.NewProtoCodec(registry),
		authorizationKey,
		memStoreKey,
	)

	importtokenKeeper := *ImporttokenKeeper.NewKeeper(
		codec.NewProtoCodec(registry),
		authorizationKey,
		memStoreKey,
	)

	keeper := NewKeeper(
		codec.NewProtoCodec(registry),
		storeKey,
		walletKeeper,
		authorizationKeeper,
		exporttokenKeeper,
		importtokenKeeper,
		memStoreKey,
	)

	roleIds := []string{
		authorizationTypes.BusinessLogicActivateToken,
		authorizationTypes.BusinessLogicCloneToken,
		authorizationTypes.BusinessLogicCreateToken,
		authorizationTypes.BusinessLogicDeactivateToken,
		authorizationTypes.BusinessLogicGetDocumentHash,
		authorizationTypes.BusinessLogicGetTokensBySegmentId,
		authorizationTypes.BusinessLogicGetTokensByWalletId,
		authorizationTypes.BusinessLogicMoveTokenToWallet,
		authorizationTypes.BusinessLogicSetTokenValidStatus,
		authorizationTypes.BusinessLogicStoreDocumentHash,
		authorizationTypes.BusinessLogicUpdateToken,
		authorizationTypes.BusinessLogicGetTokensBySegmentId,
		authorizationTypes.HashTokenActivateToken,
		authorizationTypes.HashTokenCreateToken,
		authorizationTypes.HashTokenDeactivateToken,
		authorizationTypes.HashTokenUpdateToken,
		authorizationTypes.HashTokenUpdateTokenInformation,
	}
	ctx := sdk.NewContext(stateStore, tmproto.Header{}, false, log.NewNopLogger())
	for _, v := range roleIds {
		authorizationKeeper.AppendApplicationRole(ctx, "", v, "")
	}
	authorizationKeeper.SetBlockchainAccount(ctx, authorizationTypes.BlockchainAccount{
		Creator: creatorA,
		Id:      creatorA,
		BlockchainAccountStates: []*authorizationTypes.BlockchainAccountState{{
			Creator:            creatorA,
			Id:                 uuid.New().String(),
			AccountID:          creatorA,
			TimeStamp:          time.Now().UTC().Format(time.RFC3339),
			ApplicationRoleIDs: roleIds,
			Valid:              true,
		}},
	})
	authorizationKeeper.SetConfiguration(ctx, authorizationTypes.Configuration{Creator: "", Id: 0, PermissionCheck: true})

	return keeper, ctx
}

const (
	creatorA = "cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462y"
	token    = "token"
)

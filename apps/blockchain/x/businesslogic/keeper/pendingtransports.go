//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"
)

var storeKey = []byte("transports")

func (k Keeper) AddTransportNotice(ctx sdk.Context, exportTokenId string) error {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.TransportNoticesKey))
	var notices types.TransportNotices
	k.cdc.MustUnmarshal(store.Get(storeKey), &notices)
	notices.Notices = append(notices.Notices, &types.TransportNotice{
		ExportTokenId: exportTokenId,
		CarrierUser:   "",
	})
	b, err := k.cdc.Marshal(&notices)
	if err != nil {
		return err
	}
	store.Set(storeKey, b)
	return nil
}

func (k Keeper) RemoveTransportNotice(ctx sdk.Context, exportTokenId string) error {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.TransportNoticesKey))
	var notices types.TransportNotices
	k.cdc.MustUnmarshal(store.Get(storeKey), &notices)
	oldNotices := notices.Notices
	for i, notice := range oldNotices {
		if notice.ExportTokenId == exportTokenId {
			newNotices := append(oldNotices[:i], oldNotices[i+1:]...)
			notices.Notices = newNotices
			b, err := k.cdc.Marshal(&notices)
			if err != nil {
				return err
			}
			store.Set(storeKey, b)
			return nil
		}
	}
	return types.ErrNoNotice
}

func (k Keeper) UpdateTransportNotice(ctx sdk.Context, exportTokenId string, carrier string) error {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.TransportNoticesKey))
	var notices types.TransportNotices
	k.cdc.MustUnmarshal(store.Get(storeKey), &notices)
	for _, notice := range notices.Notices {
		if notice.ExportTokenId == exportTokenId {
			notice.CarrierUser = carrier
			b, err := k.cdc.Marshal(&notices)
			if err != nil {
				return err
			}
			store.Set(storeKey, b)
			return nil
		}
	}
	return types.ErrNoNotice
}

func (k Keeper) GetTransportNotices(ctx sdk.Context) []*types.TransportNotice {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.TransportNoticesKey))
	var notices types.TransportNotices
	k.cdc.MustUnmarshal(store.Get(storeKey), &notices)
	return notices.Notices
}

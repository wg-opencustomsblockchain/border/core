//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package keeper

// --- Token ---
// Queries all existing Import Tokens
//func (k Keeper) ImportTokenAll(c context.Context, req *types.QueryAllImportTokenRequest) (*types.QueryAllImportTokenResponse, error) {
//	return k.importTokenKeeper.ImportTokenAll(c, req)
//}
//
//// Queries an existing Import Token by its Id
//func (k Keeper) ImportToken(c context.Context, req *types.QueryGetImportTokenRequest) (*types.QueryGetImportTokenResponse, error) {
//	return k.importTokenKeeper.ImportToken(c, req)
//}
//
//// --- Mapping ---
//// Queries an Import Token by its MRN/Or in the future its process ID.
//func (k Keeper) ImportMapping(c context.Context, req *types.QueryGetImportMappingRequest) (*types.QueryGetImportMappingResponse, error) {
//	return k.importTokenKeeper.ImportMapping(c, req)
//}
//
//// --- Event History ---
//// ImportEventHistory return the event history of a token
//func (k Keeper) ImportEventHistory(c context.Context, req *types.QueryGetEventHistoryRequest) (*types.QueryGetEventHistoryResponse, error) {
//	return k.importTokenKeeper.ImportEventHistory(c, req)
//}

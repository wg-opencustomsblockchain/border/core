// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
package keeper

import (
	"context"
	exportTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/exporttoken/types"
	"strings"
	"testing"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	tokenwalletTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
)

const (
	ExportTokenWalletId  = "TokenWalletId"
	ExportId             = "MRN123"
	LRN                  = "LRN123"
	ConsigneeCompanyName = "Ina Limited Ltd."
	ConsigneeCountry     = "GB"
)

func Test_msgServer_CreateExportToken(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	creatorA := "cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462y"

	goCtx := sdk.WrapSDKContext(ctx)
	type fields struct {
		Keeper Keeper
	}
	type args struct {
		goCtx context.Context
		msg   *types.MsgCreateExportToken
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *types.MsgIdResponse
		wantErr bool
	}{
		{
			name:   "Successfull Tx",
			fields: fields{Keeper: *keeper},
			args: args{goCtx: goCtx, msg: &types.MsgCreateExportToken{
				Creator:              creatorA,
				TokenWalletId:        ExportTokenWalletId,
				ExportId:             ExportId,
				LocalReferenceNumber: LRN,
				Consignee: &exportTypes.Company{
					Address: &exportTypes.Address{
						CountryCode: ConsigneeCountry,
					},
					Name: ConsigneeCompanyName,
				},
			}},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			k := msgServer{
				Keeper: tt.fields.Keeper,
			}
			_, errCreateExportWallet := k.walletKeeper.CreateWalletWithId(sdk.WrapSDKContext(ctx), tokenwalletTypes.NewMsgCreateWalletWithId(creatorA, ExportTokenWalletId, ExportTokenWalletId))
			if errCreateExportWallet != nil {
				t.Errorf("msgServer.CreateWalletWithId() error = %v", errCreateExportWallet)
			}

			oldWallet, errOldWallet := k.walletKeeper.FetchWallet(sdk.WrapSDKContext(ctx), &tokenwalletTypes.MsgFetchGetWallet{Creator: creatorA, Id: ExportTokenWalletId})
			if errOldWallet != nil {
				t.Errorf("msgServer.FetchWallet() error = %v", errOldWallet)
			}

			got, err := k.CreateExportToken(tt.args.goCtx, tt.args.msg)
			if (err != nil) != tt.wantErr {
				t.Errorf("msgServer.CreateWallet() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got == nil {
				t.Errorf("msgServer.CreateWallet() error = %v, wantErr %v", got, "id not nil")
				return
			}

			newWallet, errNewWallet := k.walletKeeper.FetchWallet(sdk.WrapSDKContext(ctx), &tokenwalletTypes.MsgFetchGetWallet{Creator: creatorA, Id: ExportTokenWalletId})
			if errNewWallet != nil {
				t.Errorf("msgServer.FetchWallet() error = %v", errNewWallet)
			}

			if len(oldWallet.Wallet.SegmentIds) == len(newWallet.Wallet.SegmentIds) {
				t.Error("Number of Segments has not changed, Segment has not been created?")
			}

			segments, errSegments := k.walletKeeper.FetchSegmentAll(sdk.WrapSDKContext(ctx), &tokenwalletTypes.MsgFetchAllSegment{Creator: creatorA})
			if errSegments != nil {
				t.Errorf("msgServer.FetchSegment() error = %v", errSegments)
			}
			for _, segment := range segments.Segment {
				if segment.WalletId == ExportTokenWalletId {
					for _, ref := range segment.TokenRefs {
						if ref.Id == got.ExportToken.Id {
							if !strings.Contains(segment.Info, got.ExportToken.ExportId) {
								t.Error("Segment Info does not contain ExportId")
								return
							}
							if !strings.Contains(segment.Info, got.ExportToken.LocalReferenceNumber) {
								t.Error("Segment Info does not contain LRN")
								return
							}
						}
					}
				}
			}
		})
	}
}

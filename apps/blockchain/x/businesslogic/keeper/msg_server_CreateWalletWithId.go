//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"
	"fmt"

	authorizationTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
	walletTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
)

const IncomingSegmentPrefix = "-IncomingExportToken"

func GetIncomingSegmentName(wallet string) string {
	return fmt.Sprintf("%s%s", wallet, IncomingSegmentPrefix)
}

func (k msgServer) CreateWalletWithId(goCtx context.Context, msg *types.MsgCreateWalletWithId) (*walletTypes.MsgIdResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Permission check
	check, err := k.authorizationKeeper.HasRole(ctx, msg.Creator, authorizationTypes.WalletCreateWallet)
	if err != nil || !check {
		return &walletTypes.MsgIdResponse{}, sdkerrors.Wrap(authorizationTypes.ErrNoPermission, authorizationTypes.WalletCreateWallet)
	}

	if k.walletKeeper.HasWallet(ctx, msg.Id) {
		return nil, sdkerrors.Wrap(types.ErrWalletAlreadyExists, fmt.Sprintf("wallet with id %s already exist", msg.Id))
	}

	resWallet, errWallet := k.walletKeeper.CreateWalletWithId(goCtx, &walletTypes.MsgCreateWalletWithId{
		Creator: msg.Creator,
		Name:    msg.Name,
		Id:      msg.Id,
	})
	if errWallet != nil {
		return &walletTypes.MsgIdResponse{}, errWallet
	}

	_, errAssign := k.walletKeeper.AssignCosmosAddressToWallet(goCtx, &walletTypes.MsgAssignCosmosAddressToWallet{
		Creator:       msg.Creator,
		CosmosAddress: msg.Creator,
		WalletId:      msg.Id,
	})
	if errAssign != nil {
		return &walletTypes.MsgIdResponse{}, errAssign
	}

	_, errSegment := k.walletKeeper.CreateSegmentWithId(goCtx, &walletTypes.MsgCreateSegmentWithId{
		Creator:  msg.Creator,
		Id:       GetIncomingSegmentName(msg.Id),
		Name:     "Incoming",
		Info:     "Incoming segment for new ExportToken",
		WalletId: msg.Id,
	})
	if errSegment != nil {
		return &walletTypes.MsgIdResponse{}, errSegment
	}

	return resWallet, nil
}

// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
package keeper

import (
	exportTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/exporttoken/types"
	"testing"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	tokenwalletTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
)

const (
	CompanyCountryCode      = "GB"
	CompanyName             = "Ina Import Ltd."
	WrongCompanyCountryCode = "US"
)

func Test_PendingExport(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	creatorA := "cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462y"

	goCtx := sdk.WrapSDKContext(ctx)

	k := msgServer{
		Keeper: *keeper,
	}
	_, errCreateWallet := k.walletKeeper.CreateWalletWithId(sdk.WrapSDKContext(ctx),
		tokenwalletTypes.NewMsgCreateWalletWithId(creatorA, ExportTokenWalletId, ExportTokenWalletId))
	if errCreateWallet != nil {
		t.Errorf("msgServer.CreateWalletWithId() error = %v", errCreateWallet)
	}

	token, errToken := k.CreateExportToken(goCtx, &types.MsgCreateExportToken{
		Creator:       creatorA,
		TokenWalletId: ExportTokenWalletId,
		ExportId:      ExportId,
		Consignee: &exportTypes.Company{
			Address: &exportTypes.Address{
				StreetAndNumber: "",
				Postcode:        "",
				City:            "",
				CountryCode:     CompanyCountryCode,
			},
			CustomsId:        "",
			Name:             CompanyName,
			SubsidiaryNumber: "",
		},
	})
	if errToken != nil {
		t.Errorf("msgServer.CreateExportToken() error = %v", errToken)
		return
	}

	tokenImports, errTokenImports := k.FetchExportTokenAllImporter(goCtx, &types.MsgFetchExportTokenAllImporter{
		Creator:        creatorA,
		Timestamp:      "123",
		CompanyName:    CompanyName,
		CompanyCountry: CompanyCountryCode,
	})
	if errTokenImports != nil {
		t.Errorf("msgServer.FetchExportTokenAllImporter() error = %v", errTokenImports)
		return
	}
	found := false
	for _, t := range tokenImports.Tokens {
		if t.ExportToken.Id == token.ExportToken.Id {
			found = true
		}
	}
	if !found {
		t.Error("Notice has not been found")
	}

	tokenImports2, errTokenImports2 := k.FetchExportTokenAllImporter(goCtx, &types.MsgFetchExportTokenAllImporter{
		Creator:        creatorA,
		Timestamp:      "123",
		CompanyName:    CompanyName,
		CompanyCountry: WrongCompanyCountryCode,
	})
	if errTokenImports2 != nil {
		t.Errorf("msgServer.FetchExportTokenAllImporter() error = %v", errTokenImports2)
		return
	}
	found = false
	for _, t := range tokenImports2.Tokens {
		if t.ExportToken.Id == token.ExportToken.Id {
			found = true
		}
	}
	if found {
		t.Error("Notice has been found for different company")
	}

	_, errCreateImportWallet := k.walletKeeper.CreateWalletWithId(sdk.WrapSDKContext(ctx),
		tokenwalletTypes.NewMsgCreateWalletWithId(creatorA, ImportTokenWalletId, ImportTokenWalletId))
	if errCreateImportWallet != nil {
		t.Errorf("msgServer.CreateWalletWithId() error = %v", errCreateWallet)
	}

	k.CreateImportToken(goCtx, &types.MsgCreateImportToken{
		Creator:            creatorA,
		RelatedExportToken: token.ExportToken.Id,
		TokenWalletId:      ImportTokenWalletId,
	})
	tokenImports3, errTokenImports3 := k.FetchExportTokenAllImporter(goCtx, &types.MsgFetchExportTokenAllImporter{
		Creator:        creatorA,
		Timestamp:      "123",
		CompanyName:    CompanyName,
		CompanyCountry: WrongCompanyCountryCode,
	})
	if errTokenImports3 != nil {
		t.Errorf("msgServer.FetchExportTokenAllImporter() error = %v", errTokenImports3)
		return
	}
	found = false
	for _, t := range tokenImports3.Tokens {
		if t.ExportToken.Id == token.ExportToken.Id {
			found = true
		}
	}
	if found {
		t.Error("Notice has been found, but should have already been deleted")
	}
}

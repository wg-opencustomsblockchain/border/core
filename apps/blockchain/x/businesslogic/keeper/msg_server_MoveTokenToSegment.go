//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	authorizationTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
	walletTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
)

func (k msgServer) MoveTokenToSegment(goCtx context.Context, msg *types.MsgMoveTokenToSegment) (*walletTypes.MsgEmptyResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Permission check
	check, err := k.authorizationKeeper.HasRole(ctx, msg.Creator, authorizationTypes.WalletMoveTokenToSegment)
	if err != nil || !check {
		return &walletTypes.MsgEmptyResponse{}, sdkerrors.Wrap(authorizationTypes.ErrNoPermission, authorizationTypes.WalletMoveTokenToSegment)
	}
	res, error := k.walletKeeper.MoveTokenToSegment(goCtx, &walletTypes.MsgMoveTokenToSegment{
		Creator:         msg.Creator,
		TokenRefId:      msg.TokenRefId,
		SourceSegmentId: msg.SourceSegmentId,
		TargetSegmentId: msg.TargetSegmentId,
	})

	if error != nil {
		return &walletTypes.MsgEmptyResponse{}, error
	}

	return res, nil
}

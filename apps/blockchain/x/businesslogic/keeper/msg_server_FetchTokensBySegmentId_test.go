// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
package keeper

//func Test_msgServer_FetchTokensBySegmentId(t *testing.T) {
//	keeper, ctx := setupKeeper(t)
//	creatorA := "cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462y"
//	creatorB := "cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462x"
//	goCtx := sdk.WrapSDKContext(ctx)
//	type fields struct {
//		Keeper Keeper
//	}
//	type args struct {
//		goCtx context.Context
//		msg   *types.MsgFetchTokensBySegmentId
//	}
//	tests := []struct {
//		name    string
//		fields  fields
//		args    args
//		want    *types.QueryAllTokenResponse
//		wantErr bool
//	}{
//		{
//			name:    "Tx Key not found",
//			fields:  fields{Keeper: *keeper},
//			args:    args{goCtx: goCtx, msg: &types.MsgFetchTokensBySegmentId{Creator: creatorA}},
//			want:    &types.QueryAllTokenResponse{},
//			wantErr: true,
//		},
//		{
//			name:    "Tx Permission denied",
//			fields:  fields{Keeper: *keeper},
//			args:    args{goCtx: goCtx, msg: &types.MsgFetchTokensBySegmentId{Creator: creatorB}},
//			want:    &types.QueryAllTokenResponse{},
//			wantErr: true,
//		},
//	}
//	for _, tt := range tests {
//		t.Run(tt.name, func(t *testing.T) {
//			k := msgServer{
//				Keeper: tt.fields.Keeper,
//			}
//			authorizationKeeper := k.authorizationKeeper
//			authorizationKeeper.SetApplicationRole(ctx, auhtorizationTypes.ApplicationRole{
//				Creator: creatorA,
//				Id:      auhtorizationTypes.BusinessLogicGetTokensBySegmentId,
//				ApplicationRoleStates: []*auhtorizationTypes.ApplicationRoleState{
//					{
//						Creator:           creatorA,
//						Id:                auhtorizationTypes.BusinessLogicGetTokensBySegmentId,
//						ApplicationRoleID: auhtorizationTypes.BusinessLogicGetTokensBySegmentId,
//						Description:       "",
//						Valid:             true,
//						TimeStamp:         "",
//					},
//				},
//			})
//			authorizationKeeper.SetBlockchainAccount(ctx, auhtorizationTypes.BlockchainAccount{
//				Creator: creatorA,
//				Id:      creatorA,
//				BlockchainAccountStates: []*auhtorizationTypes.BlockchainAccountState{
//					{
//						Creator:            creatorA,
//						Id:                 creatorA,
//						AccountID:          creatorA,
//						TimeStamp:          "",
//						Valid:              true,
//						ApplicationRoleIDs: []string{auhtorizationTypes.BusinessLogicGetTokensBySegmentId},
//					},
//				},
//			},
//			)
//			_, err := k.FetchTokensBySegmentId(tt.args.goCtx, tt.args.msg)
//			if (err != nil) != tt.wantErr {
//				t.Errorf("msgServer.FetchTokensBySegmentId() error = %v, wantErr %v", err, tt.wantErr)
//				return
//			}
//		})
//	}
//}

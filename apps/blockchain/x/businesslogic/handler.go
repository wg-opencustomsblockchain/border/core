// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
package TokenManager

import (
	"fmt"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/keeper"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
)

// NewHandler ...
func NewHandler(k keeper.Keeper) sdk.Handler {
	// this line is used by starport scaffolding # handler/msgServer
	msgServer := keeper.NewMsgServerImpl(k)

	return func(ctx sdk.Context, msg sdk.Msg) (*sdk.Result, error) {
		ctx = ctx.WithEventManager(sdk.NewEventManager())

		switch msg := msg.(type) {
		// this line is used by starport scaffolding # 1

		case *types.MsgFetchGetWallet:
			res, err := msgServer.FetchGetWallet(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

		case *types.MsgFetchGetSegment:
			res, err := msgServer.FetchGetSegment(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

		case *types.MsgFetchAllSegment:
			res, err := msgServer.FetchAllSegment(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

		case *types.MsgUpdateWallet:
			res, err := msgServer.UpdateWallet(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

		case *types.MsgCreateWallet:
			res, err := msgServer.CreateWallet(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

		case *types.MsgUpdateSegment:
			res, err := msgServer.UpdateSegment(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

		case *types.MsgFetchGetWalletHistory:
			res, err := msgServer.FetchGetWalletHistory(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

		case *types.MsgCreateSegment:
			res, err := msgServer.CreateSegment(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

		case *types.MsgCreateWalletWithId:
			res, err := msgServer.CreateWalletWithId(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

		case *types.MsgFetchAllWallet:
			res, err := msgServer.FetchAllWallet(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

		case *types.MsgFetchSegmentHistory:
			res, err := msgServer.FetchSegmentHistory(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

		case *types.MsgCreateSegmentWithId:
			res, err := msgServer.CreateSegmentWithId(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

		// ExportToken
		case *types.MsgCreateExportToken:
			res, err := msgServer.CreateExportToken(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

		case *types.MsgCreateExportEvent:
			res, err := msgServer.CreateExportEvent(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

		case *types.MsgUpdateExportToken:
			res, err := msgServer.UpdateExportToken(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

			// STATUS EVENT MSG START
			// Events with Status changes  numbered from 1-6
		case *types.MsgEventPresentQrCode:
			res, err := msgServer.EventPresentQrCode(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

		case *types.MsgEventAcceptGoodsForTransport:
			res, err := msgServer.EventAcceptGoodsForTransport(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

		case *types.MsgEventPresentGoods:
			res, err := msgServer.EventPresentGoods(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

		case *types.MsgEventCertifyExitOfGoods:
			res, err := msgServer.EventCertifyExitOfGoods(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

		case *types.MsgEventMoveToArchive:
			res, err := msgServer.EventMoveToArchive(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

			// Events without status changes
		case *types.MsgEventExportCheck:
			res, err := msgServer.EventExportCheck(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

		case *types.MsgEventAcceptExport:
			res, err := msgServer.EventAcceptExport(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

		case *types.MsgEventExportRefusal:
			res, err := msgServer.EventExportRefusal(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

			// STATUS EVENT MSG END

		case *types.MsgFetchExportTokenExporter:
			res, err := msgServer.FetchExportTokenExporter(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

		case *types.MsgFetchExportTokenAllExporter:
			res, err := msgServer.FetchExportTokenAllExporter(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

		case *types.MsgFetchExportTokenDriver:
			res, err := msgServer.FetchExportTokenDriver(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

		case *types.MsgFetchExportTokenAllDriver:
			res, err := msgServer.FetchExportTokenAllDriver(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

		case *types.MsgFetchExportTokenImporter:
			res, err := msgServer.FetchExportTokenImporter(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

		case *types.MsgFetchExportTokenAllImporter:
			res, err := msgServer.FetchExportTokenAllImporter(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

			// ImportToken

			// STATUS EVENT MSG START
			// Events with Status changes  numbered from 1-6
		case *types.MsgCreateImportEvent:
			res, err := msgServer.CreateImportEvent(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

		case *types.MsgEventAcceptImport:
			res, err := msgServer.EventAcceptImport(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

		case *types.MsgEventCompleteImport:
			res, err := msgServer.EventCompleteImport(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

		case *types.MsgEventImportProposal:
			res, err := msgServer.EventImportProposal(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

		case *types.MsgEventImportRefusal:
			res, err := msgServer.EventImportRefusal(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

		case *types.MsgEventImportSupplement:
			res, err := msgServer.EventImportSupplement(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

			// SOON TO BE IMPLEMENTED
		case *types.MsgEventImportExport:
			res, err := msgServer.EventImportExport(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

			// SOON TO BE IMPLEMENTED
		case *types.MsgEventImportSubmit:
			res, err := msgServer.EventImportSubmit(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

			// SOON TO BE IMPLEMENTED
		case *types.MsgEventImportRelease:
			res, err := msgServer.EventImportRelease(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

			// STATUS EVENT MSG END

		case *types.MsgCreateImportToken:
			res, err := msgServer.CreateImportToken(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

		case *types.MsgUpdateImportToken:
			res, err := msgServer.UpdateImportToken(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

		case *types.MsgFetchImportToken:
			res, err := msgServer.FetchImportToken(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

		case *types.MsgFetchImportTokenAll:
			res, err := msgServer.FetchImportTokenAll(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

		case *types.MsgFetchExportTokenExporterByExportId:
			res, err := msgServer.FetchExportTokenExporterByExportId(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

		case *types.MsgFetchExportTokenDriverByExportId:
			res, err := msgServer.FetchExportTokenDriverByExportId(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

		case *types.MsgFetchExportTokenImporterByExportId:
			res, err := msgServer.FetchExportTokenImporterByExportId(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

		case *types.MsgFetchImportTokenByImportId:
			res, err := msgServer.FetchImportTokenByImportId(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

		case *types.MsgForwardExportToken:
			res, err := msgServer.ForwardExportToken(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

		case *types.MsgAssignCosmosAddressToWallet:
			res, err := msgServer.AssignCosmosAddressToWallet(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

		case *types.MsgFetchTokenHistory:
			res, err := msgServer.FetchTokenHistory(sdk.WrapSDKContext(ctx), msg)
			return sdk.WrapServiceResult(ctx, res, err)

		default:
			errMsg := fmt.Sprintf("unrecognized %s message type: %T", types.ModuleName, msg)
			return nil, sdkerrors.Wrap(sdkerrors.ErrUnknownRequest, errMsg)
		}
	}
}

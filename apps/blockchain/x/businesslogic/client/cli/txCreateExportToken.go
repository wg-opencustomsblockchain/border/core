// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
package cli

import (
	"encoding/xml"
	"io/ioutil"
	"os"
	"strings"
	"time"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	exportTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/exporttoken/types"
	"github.com/cosmos/cosmos-sdk/client"
	"github.com/cosmos/cosmos-sdk/client/flags"
	"github.com/cosmos/cosmos-sdk/client/tx"
	"github.com/spf13/cobra"
)

type TransportMeans struct {
	Mode        int64  `xml:"Mode"`
	Type        int64  `xml:"Type"`
	Nationality string `xml:"Nationality"`
}

type Itinerary struct {
	Country string `xml:"Country"`
}

type Company struct {
	IdentificationReferenceNumber  string `xml:"Identification>ReferenceNumber"`
	IdentificationSubsidiaryNumber string `xml:"Identification>SubsidiaryNumber"`
	Name                           string `xml:"Name"`
	AddressLine                    string `xml:"Address>Line"`
	AddressCity                    string `xml:"Address>City"`
	AddressPostcode                string `xml:"Address>Postcode"`
	AddressCountry                 string `xml:"Address>Country"`
}

type Document struct {
	XMLName         xml.Name `xml:"Document"`
	Type            string   `xml:"Type"`
	ReferenceNumber string   `xml:"ReferenceNumber"`
	Complement      string   `xml:"Complement"`
	Detail          string   `xml:"Detail"`
}

type GoodsItem struct {
	XMLName   xml.Name   `xml:"GoodsItem"`
	Documents []Document `xml:"Document"`

	SequenceNumber           int64  `xml:"SequenceNumber"`
	DescriptionOfGoods       string `xml:"GoodsDescription"`
	CountryOfOrigin          string `xml:"OriginFederalState"`
	GrossMass                string `xml:"GrossMass"`
	NetMass                  string `xml:"NetMass"`
	CombinedNomenclatureCode string `xml:"CommodityCode>CombinedNomenclatureCode"`

	ValueAtBorderExport float64 `xml:"ForeignTradeStatistics>StatisticalValue"`
	TypeOfPackages      string  `xml:"Package>Kind"`
	NumberOfPackages    int64   `xml:"Package>Quantity"`
	MarksNumbers        string  `xml:"Package>MarksNumbers"`
	ContainerId         string  `xml:"Container>IdentificationNumber"`
	DangerousGoodsCode  int64   `xml:"DangerousGoodsCode"`
}

type DEXPRE struct {
	XMLName                 xml.Name `xml:"DEXPRE"`
	MovementReferenceNumber string   `xml:"Header>MovementReferenceNumber"`
	LocalReferenceNumber    string   `xml:"Header>LocalReferenceNumber"`
	ExportCountry           string   `xml:"Header>ExportCountry"`
	DestinationCountry      string   `xml:"Header>DestinationCountry"`
	IssuingDateTime         string   `xml:"Header>IssuingDateTime"`
	TotalGrossMassMeasure   string   `xml:"Header>TotalGrossMassMeasure"`
	GoodsItemQuantity       int64    `xml:"Header>GoodsItemQuantity"`
	TotalPackageQuantity    int64    `xml:"Header>TotalPackageQuantity"`

	InlandTransportMeans TransportMeans `xml:"Header>InlandTransportMeans"`
	BorderTransportMeans TransportMeans `xml:"Header>BorderTransportMeans"`

	NatureOfTransaction int64   `xml:"Transaction>Type"`
	TotalAmountInvoiced float64 `xml:"Transaction>InvoiceAmount"`
	TransactionCurrency string  `xml:"Transaction>Currency"`

	Itinerary                                []string `xml:"Itinerary>Country"`
	DeliveryTermsIncotermCode                string   `xml:"DeliveryTerms>IncotermCode"`
	DeliveryTermsLocation                    string   `xml:"DeliveryTerms>Location"`
	ExportCustomsOfficeReferenceNumber       string   `xml:"ExportCustomsOffice>ReferenceNumber"`
	IntendedExitCustomsOfficeReferenceNumber string   `xml:"IntendedExitCustomsOffice>ReferenceNumber"`
	Exporter                                 Company
	Declarant                                Company
	Consignee                                Company
	GoodsItems                               []GoodsItem `xml:"GoodsItem"`
}

func CmdCreateExportToken() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "create-export-token [file]",
		Short: "Creates an export token from xml",
		Args:  cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			clientCtx, ctxErr := client.GetClientTxContext(cmd)
			if ctxErr != nil {
				return ctxErr
			}

			xmlFile, xmlErr := os.Open(args[0])
			if xmlErr != nil {
				return xmlErr
			}
			defer xmlFile.Close()

			byteValue, readErr := ioutil.ReadAll(xmlFile)
			if readErr != nil {
				return readErr
			}
			var dexpre DEXPRE

			errXML := xml.Unmarshal(byteValue, &dexpre)
			if errXML != nil {
				return errXML
			}

			var goodsItems []*exportTypes.GoodsItem

			for _, item := range dexpre.GoodsItems {
				var documents []*exportTypes.Document
				for _, doc := range item.Documents {
					documents = append(documents, &exportTypes.Document{
						Type:            doc.Type,
						ReferenceNumber: doc.ReferenceNumber,
						Complement:      doc.Complement,
						Detail:          doc.Detail,
					})
				}

				gi := exportTypes.GoodsItem{
					Documents:                      documents,
					CountryOfOrigin:                item.CountryOfOrigin,
					SequenceNumber:                 item.SequenceNumber,
					DescriptionOfGoods:             item.DescriptionOfGoods,
					HarmonizedSystemSubheadingCode: item.CombinedNomenclatureCode[0:6],
					LocalClassificationCode:        item.CombinedNomenclatureCode[6:],
					GrossMass:                      item.GrossMass,
					NetMass:                        item.NetMass,
					NumberOfPackages:               item.NumberOfPackages,
					TypeOfPackages:                 item.TypeOfPackages,
					MarksNumbers:                   item.MarksNumbers,
					ContainerId:                    item.ContainerId,
					ValueAtBorderExport:            int64(item.ValueAtBorderExport * 100),
					ValueAtBorderExportCurrency:    dexpre.TransactionCurrency,
					DangerousGoodsCode:             item.DangerousGoodsCode,
					//ConsigneeOrderNumber:           item.ConsigneeOrderNumber,
					//AmountInvoiced:                 item.AmountInvoiced,
					//AmountInvoicedCurrency:         item.AmountInvoicedCurrency,
				}
				goodsItems = append(goodsItems, &gi)
			}

			itinerary := strings.Join(dexpre.Itinerary, ", ")

			msg := types.MsgCreateExportToken{
				Creator: clientCtx.GetFromAddress().String(),
				Id:      "",
				Exporter: &exportTypes.Company{
					Address: &exportTypes.Address{
						StreetAndNumber: dexpre.Exporter.AddressLine,
						Postcode:        dexpre.Exporter.AddressPostcode,
						City:            dexpre.Exporter.AddressCity,
						CountryCode:     dexpre.Exporter.AddressCountry,
					},
					CustomsId:        dexpre.Exporter.IdentificationReferenceNumber,
					Name:             dexpre.Exporter.Name,
					SubsidiaryNumber: dexpre.Exporter.IdentificationSubsidiaryNumber,
				},
				Declarant: &exportTypes.Company{
					Address: &exportTypes.Address{
						StreetAndNumber: dexpre.Declarant.AddressLine,
						Postcode:        dexpre.Declarant.AddressPostcode,
						City:            dexpre.Declarant.AddressCity,
						CountryCode:     dexpre.Declarant.AddressCountry,
					},
					CustomsId:        dexpre.Declarant.IdentificationReferenceNumber,
					Name:             dexpre.Declarant.Name,
					SubsidiaryNumber: dexpre.Declarant.IdentificationSubsidiaryNumber,
				},
				Representative: &exportTypes.Company{},
				Consignee: &exportTypes.Company{
					Address: &exportTypes.Address{
						StreetAndNumber: dexpre.Consignee.AddressLine,
						Postcode:        dexpre.Consignee.AddressPostcode,
						City:            dexpre.Consignee.AddressCity,
						CountryCode:     dexpre.Consignee.AddressCountry,
					},
					CustomsId:        dexpre.Consignee.IdentificationReferenceNumber,
					Name:             dexpre.Consignee.Name,
					SubsidiaryNumber: dexpre.Consignee.IdentificationSubsidiaryNumber,
				},
				CustomsOfficeOfExport: &exportTypes.CustomsOffice{
					Address:           nil,
					CustomsOfficeCode: dexpre.ExportCustomsOfficeReferenceNumber,
					CustomsOfficeName: "",
				},
				CustomOfficeOfExit: &exportTypes.CustomsOffice{
					Address:           nil,
					CustomsOfficeCode: dexpre.IntendedExitCustomsOfficeReferenceNumber,
					CustomsOfficeName: "",
				},
				GoodsItems: goodsItems,
				TransportToBorder: &exportTypes.Transport{
					ModeOfTransport:       dexpre.InlandTransportMeans.Mode,
					TypeOfIdentification:  dexpre.InlandTransportMeans.Type,
					Identity:              "",
					Nationality:           dexpre.InlandTransportMeans.Nationality,
					TransportCost:         0,
					TransportCostCurrency: "",
					TransportOrderNumber:  "",
				},
				TransportAtBorder: &exportTypes.Transport{
					ModeOfTransport:       dexpre.BorderTransportMeans.Mode,
					TypeOfIdentification:  dexpre.BorderTransportMeans.Type,
					Identity:              "",
					Nationality:           dexpre.BorderTransportMeans.Nationality,
					TransportCost:         0,
					TransportCostCurrency: "",
					TransportOrderNumber:  "",
				},
				ExportId:              dexpre.MovementReferenceNumber,
				LocalReferenceNumber:  dexpre.LocalReferenceNumber,
				DestinationCountry:    dexpre.DestinationCountry,
				ExportCountry:         dexpre.ExportCountry,
				Itinerary:             itinerary,
				ReleaseDateAndTime:    dexpre.IssuingDateTime,
				IncotermCode:          dexpre.DeliveryTermsIncotermCode,
				IncotermLocation:      dexpre.DeliveryTermsLocation,
				TotalGrossMass:        dexpre.TotalGrossMassMeasure,
				GoodsItemQuantity:     dexpre.GoodsItemQuantity,
				TotalPackagesQuantity: dexpre.TotalPackageQuantity,
				NatureOfTransaction:   dexpre.NatureOfTransaction,
				TotalAmountInvoiced:   int64(dexpre.TotalAmountInvoiced * 100),
				InvoiceCurrency:       dexpre.TransactionCurrency,
				User:                  clientCtx.GetFromAddress().String(),
				Timestamp:             GetTimestamp(),

				//UniqueConsignmentReference: "",
			}

			if err := msg.ValidateBasic(); err != nil {
				return err
			}

			return tx.GenerateOrBroadcastTxCLI(clientCtx, cmd.Flags(), &msg)
		},
	}
	flags.AddTxFlagsToCmd(cmd)
	return cmd
}

func GetTimestamp() string {
	return time.Now().UTC().Format(time.RFC3339)
}

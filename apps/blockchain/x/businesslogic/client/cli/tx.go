// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
package cli

import (
	"fmt"

	"github.com/spf13/cobra"

	"github.com/cosmos/cosmos-sdk/client"
	// "github.com/cosmos/cosmos-sdk/client/flags"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	"github.com/cosmos/cosmos-sdk/client/flags"
	"github.com/cosmos/cosmos-sdk/client/tx"
)

var (
	tokenValid bool
)

// GetTxCmd returns the transaction commands for this module
func GetTxCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:                        types.ModuleName,
		Short:                      fmt.Sprintf("%s transactions subcommands", types.ModuleName),
		DisableFlagParsing:         true,
		SuggestionsMinimumDistance: 2,
		RunE:                       client.ValidateCmd,
	}

	// this line is used by starport scaffolding # 1

	//cmd.AddCommand(CmdCreateToken())
	//cmd.AddCommand(CmdActivateToken())
	//cmd.AddCommand(CmdDeactivateToken())
	//cmd.AddCommand(CmdMoveTokenToWallet())
	//cmd.AddCommand(CmdCreateHashToken())
	//cmd.AddCommand(CmdStoreDocumentHash())
	//cmd.AddCommand(CmdCloneToken())
	//cmd.AddCommand(CmdUpdateToken())
	//cmd.AddCommand(CmdFetchDocumentHash())
	//cmd.AddCommand(CmdFetchTokensBySegmentId())
	//cmd.AddCommand(CmdFetchTokensByWalletId())
	//cmd.AddCommand(CmdRevertToGenesis())

	cmd.AddCommand(CmdCreateWalletWithId())
	cmd.AddCommand(CmdCreateExportToken())
	cmd.AddCommand(CmdAssignCosmosAddressToWallet())

	return cmd
}

func CmdCreateToken() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "create-token [tokenType] [changeMessage] [segmentId] [moduleRef]",
		Short: "Creates a new token",
		Args:  cobra.ExactArgs(4),
		RunE: func(cmd *cobra.Command, args []string) error {

			clientCtx, err := client.GetClientTxContext(cmd)
			if err != nil {
				return err
			}

			msg := types.NewMsgCreateToken(clientCtx.GetFromAddress().String(), args[0], args[1], args[2], args[3])
			if err := msg.ValidateBasic(); err != nil {
				return err
			}
			return tx.GenerateOrBroadcastTxCLI(clientCtx, cmd.Flags(), msg)
		},
	}

	flags.AddTxFlagsToCmd(cmd)

	return cmd
}

func CmdActivateToken() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "activate-token [tokenId] [segmentId] [moduleRef]",
		Short: "Activates a token",
		Args:  cobra.ExactArgs(3),
		RunE: func(cmd *cobra.Command, args []string) error {

			clientCtx, err := client.GetClientTxContext(cmd)
			if err != nil {
				return err
			}

			msg := types.NewMsgActivateToken(clientCtx.GetFromAddress().String(), args[0], args[1], args[2])
			if err := msg.ValidateBasic(); err != nil {
				return err
			}
			return tx.GenerateOrBroadcastTxCLI(clientCtx, cmd.Flags(), msg)
		},
	}

	flags.AddTxFlagsToCmd(cmd)

	return cmd
}

func CmdDeactivateToken() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "deactivate-token [tokenId]",
		Short: "Deactivates a token",
		Args:  cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {

			clientCtx, err := client.GetClientTxContext(cmd)
			if err != nil {
				return err
			}

			msg := types.NewMsgDeactivateToken(clientCtx.GetFromAddress().String(), args[0])
			if err := msg.ValidateBasic(); err != nil {
				return err
			}
			return tx.GenerateOrBroadcastTxCLI(clientCtx, cmd.Flags(), msg)
		},
	}

	flags.AddTxFlagsToCmd(cmd)

	return cmd
}

func CmdMoveTokenToWallet() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "move-token-to-wallet [tokenRefId] [sourceSegmentId] [targetWalletId]",
		Short: "Deactivates a token",
		Args:  cobra.ExactArgs(3),
		RunE: func(cmd *cobra.Command, args []string) error {

			clientCtx, err := client.GetClientTxContext(cmd)
			if err != nil {
				return err
			}
			msg := types.NewMsgMoveTokenToWallet(clientCtx.GetFromAddress().String(), args[0], args[1], args[2])
			if err := msg.ValidateBasic(); err != nil {
				return err
			}
			return tx.GenerateOrBroadcastTxCLI(clientCtx, cmd.Flags(), msg)
		},
	}

	flags.AddTxFlagsToCmd(cmd)

	return cmd
}

func CmdCreateHashToken() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "create-hash-token [changeMessage] [segmentId] [document] [hash] [hashFunction] [metadata]",
		Short: "Creates a new hash token",
		Args:  cobra.ExactArgs(6),
		RunE: func(cmd *cobra.Command, args []string) error {

			clientCtx, err := client.GetClientTxContext(cmd)
			if err != nil {
				return err
			}

			msg := types.NewMsgCreateHashToken(clientCtx.GetFromAddress().String(), args[0], args[1], args[2], args[3], args[4], args[5])
			if err := msg.ValidateBasic(); err != nil {
				return err
			}
			return tx.GenerateOrBroadcastTxCLI(clientCtx, cmd.Flags(), msg)
		},
	}

	flags.AddTxFlagsToCmd(cmd)

	return cmd
}

func CmdStoreDocumentHash() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "store-document-hash [changeMessage] [segmentId] [document] [hash] [hashFunction] [metadata]",
		Short: "Creates a new hash token",
		Args:  cobra.ExactArgs(6),
		RunE: func(cmd *cobra.Command, args []string) error {

			clientCtx, err := client.GetClientTxContext(cmd)
			if err != nil {
				return err
			}

			msg := types.NewMsgCreateHashToken(clientCtx.GetFromAddress().String(), args[0], args[1], args[2], args[3], args[4], args[5])
			if err := msg.ValidateBasic(); err != nil {
				return err
			}
			return tx.GenerateOrBroadcastTxCLI(clientCtx, cmd.Flags(), msg)
		},
	}

	flags.AddTxFlagsToCmd(cmd)

	return cmd
}

func CmdCloneToken() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "clone-token [tokenId] [walletId] [moduleRef]",
		Short: "Clones a token to another tokenwallet",
		Args:  cobra.ExactArgs(3),
		RunE: func(cmd *cobra.Command, args []string) error {

			clientCtx, err := client.GetClientTxContext(cmd)
			if err != nil {
				return err
			}

			msg := types.NewMsgCloneToken(clientCtx.GetFromAddress().String(), args[0], args[1], args[2])
			if err := msg.ValidateBasic(); err != nil {
				return err
			}
			return tx.GenerateOrBroadcastTxCLI(clientCtx, cmd.Flags(), msg)
		},
	}

	flags.AddTxFlagsToCmd(cmd)

	return cmd
}

func CmdUpdateToken() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "update-token [tokenRefId] [tokenType] [changeMessage] [segmentId] [moduleRef]",
		Short: "Update a token",
		Args:  cobra.ExactArgs(5),
		RunE: func(cmd *cobra.Command, args []string) error {

			clientCtx, err := client.GetClientTxContext(cmd)
			if err != nil {
				return err
			}

			msg := types.NewMsgUpdateToken(clientCtx.GetFromAddress().String(), args[0], args[1], args[2], args[3], args[4])
			if err := msg.ValidateBasic(); err != nil {
				return err
			}
			return tx.GenerateOrBroadcastTxCLI(clientCtx, cmd.Flags(), msg)
		},
	}

	flags.AddTxFlagsToCmd(cmd)

	return cmd
}

func CmdRevertToGenesis() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "revert-to-genesis",
		Short: "Reverts the blockchain to its genesis state",
		Args:  cobra.ExactArgs(0),
		RunE: func(cmd *cobra.Command, args []string) error {
			clientCtx, err := client.GetClientTxContext(cmd)
			if err != nil {
				return err
			}

			msg := types.NewMsgRevertToGenesis(clientCtx.GetFromAddress().String())
			if err := msg.ValidateBasic(); err != nil {
				return err
			}
			return tx.GenerateOrBroadcastTxCLI(clientCtx, cmd.Flags(), msg)
		},
	}

	flags.AddTxFlagsToCmd(cmd)

	return cmd
}

func CmdCreateWalletWithId() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "create-wallet-with-id [id] [name]",
		Short: "Creates a wallet with the given id and name",
		Args:  cobra.ExactArgs(2),
		RunE: func(cmd *cobra.Command, args []string) error {
			clientCtx, err := client.GetClientTxContext(cmd)
			if err != nil {
				return err
			}

			msg := types.NewMsgCreateWalletWithId(clientCtx.GetFromAddress().String(), args[0], args[1])
			if err := msg.ValidateBasic(); err != nil {
				return err
			}
			return tx.GenerateOrBroadcastTxCLI(clientCtx, cmd.Flags(), msg)
		},
	}

	flags.AddTxFlagsToCmd(cmd)

	return cmd
}

func CmdAssignCosmosAddressToWallet() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "assign-address-to-wallet [address] [wallet]",
		Short: "Assign a cosmos address to a wallet",
		Args:  cobra.ExactArgs(2),
		RunE: func(cmd *cobra.Command, args []string) error {
			clientCtx, err := client.GetClientTxContext(cmd)
			if err != nil {
				return err
			}

			msg := types.NewMsgAssignCosmosAddressToWallet(clientCtx.GetFromAddress().String(), args[0], args[1])
			if err := msg.ValidateBasic(); err != nil {
				return err
			}
			return tx.GenerateOrBroadcastTxCLI(clientCtx, cmd.Flags(), msg)
		},
	}

	flags.AddTxFlagsToCmd(cmd)

	return cmd
}

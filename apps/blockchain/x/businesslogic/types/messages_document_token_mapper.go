// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
)

var _ sdk.Msg = &MsgCreateDocumentTokenMapper{}

func NewMsgCreateDocumentTokenMapper(creator string, documentId string, tokenId string) *MsgCreateDocumentTokenMapper {
	return &MsgCreateDocumentTokenMapper{
		Creator:    creator,
		DocumentId: documentId,
		TokenId:    tokenId,
	}
}

func (msg *MsgCreateDocumentTokenMapper) Route() string {
	return RouterKey
}

func (msg *MsgCreateDocumentTokenMapper) Type() string {
	return "CreateDocumentTokenMapper"
}

func (msg *MsgCreateDocumentTokenMapper) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgCreateDocumentTokenMapper) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgCreateDocumentTokenMapper) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgUpdateDocumentTokenMapper{}

func NewMsgUpdateDocumentTokenMapper(creator string, documentId string, tokenId string) *MsgUpdateDocumentTokenMapper {
	return &MsgUpdateDocumentTokenMapper{
		Creator:    creator,
		DocumentId: documentId,
		TokenId:    tokenId,
	}
}

func (msg *MsgUpdateDocumentTokenMapper) Route() string {
	return RouterKey
}

func (msg *MsgUpdateDocumentTokenMapper) Type() string {
	return "UpdateDocumentTokenMapper"
}

func (msg *MsgUpdateDocumentTokenMapper) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgUpdateDocumentTokenMapper) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgUpdateDocumentTokenMapper) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgDeleteDocumentTokenMapper{}

func NewMsgDeleteDocumentTokenMapper(creator string, index string) *MsgDeleteDocumentTokenMapper {
	return &MsgDeleteDocumentTokenMapper{
		Creator: creator,
		Index:   index,
	}
}
func (msg *MsgDeleteDocumentTokenMapper) Route() string {
	return RouterKey
}

func (msg *MsgDeleteDocumentTokenMapper) Type() string {
	return "DeleteDocumentTokenMapper"
}

func (msg *MsgDeleteDocumentTokenMapper) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgDeleteDocumentTokenMapper) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgDeleteDocumentTokenMapper) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

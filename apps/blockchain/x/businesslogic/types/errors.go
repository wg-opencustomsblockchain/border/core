// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
package types

// DONTCOVER

import (
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
)

// x/tokenManager module sentinel errors
var (
	ErrNoMapping           = sdkerrors.Register(ModuleName, 1, "document token mapping does not exist")
	ErrSegmentNotFound     = sdkerrors.Register(ModuleName, 2, "segment not found")
	ErrWalletAlreadyExists = sdkerrors.Register(ModuleName, 3, "Wallet already exists")
	ErrNoNotice            = sdkerrors.Register(ModuleName, 4, "Notice not found")
	ErrWalletAccessDenied  = sdkerrors.Register(ModuleName, 5, "Wallet access denied")
	ErrWalletNotFound      = sdkerrors.Register(ModuleName, 6, "Wallet not found")
	ErrTokenNotNew         = sdkerrors.Register(ModuleName, 7, "Only token with status 'new' can be forwarded")
	ErrTokenWrongStatus    = sdkerrors.Register(ModuleName, 8, "Token in wrong Status")
)

// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
package types

import (
	exportTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/exporttoken/types"
	importTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/importtoken/types"
	"github.com/cosmos/cosmos-sdk/codec"
	cdctypes "github.com/cosmos/cosmos-sdk/codec/types"
	sdk "github.com/cosmos/cosmos-sdk/types"

	// this line is used by starport scaffolding # 1
	"github.com/cosmos/cosmos-sdk/types/msgservice"
)

func RegisterCodec(cdc *codec.LegacyAmino) {
	// this line is used by starport scaffolding # 2
	cdc.RegisterConcrete(&MsgFetchAllSegmentHistory{}, "businesslogic/FetchAllSegmentHistory", nil)
	cdc.RegisterConcrete(&MsgFetchGetWallet{}, "businesslogic/FetchGetWallet", nil)
	cdc.RegisterConcrete(&MsgFetchGetSegment{}, "businesslogic/FetchGetSegment", nil)
	cdc.RegisterConcrete(&MsgFetchAllSegment{}, "businesslogic/FetchAllSegment", nil)
	cdc.RegisterConcrete(&MsgUpdateWallet{}, "businesslogic/UpdateWallet", nil)
	cdc.RegisterConcrete(&MsgCreateWallet{}, "businesslogic/CreateWallet", nil)
	cdc.RegisterConcrete(&MsgUpdateSegment{}, "businesslogic/UpdateSegment", nil)
	cdc.RegisterConcrete(&MsgFetchGetWalletHistory{}, "businesslogic/FetchGetWalletHistory", nil)
	cdc.RegisterConcrete(&MsgCreateSegment{}, "businesslogic/CreateSegment", nil)
	cdc.RegisterConcrete(&MsgFetchAllTokenHistoryGlobal{}, "businesslogic/FetchAllTokenHistoryGlobal", nil)
	cdc.RegisterConcrete(&MsgFetchGetTokenHistoryGlobal{}, "businesslogic/FetchGetTokenHistoryGlobal", nil)
	cdc.RegisterConcrete(&MsgCreateWalletWithId{}, "businesslogic/CreateWalletWithId", nil)
	cdc.RegisterConcrete(&MsgMoveTokenToSegment{}, "businesslogic/MoveTokenToSegment", nil)
	cdc.RegisterConcrete(&MsgFetchAllWalletHistory{}, "businesslogic/FetchAllWalletHistory", nil)
	cdc.RegisterConcrete(&MsgFetchAllWallet{}, "businesslogic/FetchAllWallet", nil)
	cdc.RegisterConcrete(&MsgFetchSegmentHistory{}, "businesslogic/FetchSegmentHistory", nil)
	cdc.RegisterConcrete(&MsgCreateSegmentWithId{}, "businesslogic/CreateSegmentWithId", nil)
	cdc.RegisterConcrete(&MsgCreateTokenCopies{}, "businesslogic/CreateTokenCopies", nil)
	cdc.RegisterConcrete(&MsgUpdateTokenCopies{}, "businesslogic/UpdateTokenCopies", nil)
	cdc.RegisterConcrete(&MsgDeleteTokenCopies{}, "businesslogic/DeleteTokenCopies", nil)
	cdc.RegisterConcrete(&MsgCreateDocumentTokenMapper{}, "businesslogic/CreateDocumentTokenMapper", nil)
	cdc.RegisterConcrete(&MsgUpdateDocumentTokenMapper{}, "businesslogic/UpdateDocumentTokenMapper", nil)
	cdc.RegisterConcrete(&MsgDeleteDocumentTokenMapper{}, "businesslogic/DeleteDocumentTokenMapper", nil)
	cdc.RegisterConcrete(&MsgCreateToken{}, "TokenManager/CreateToken", nil)
	cdc.RegisterConcrete(&MsgUpdateToken{}, "TokenManager/UpdateToken", nil)
	cdc.RegisterConcrete(&MsgActivateToken{}, "TokenManager/ActivateToken", nil)
	cdc.RegisterConcrete(&MsgDeactivateToken{}, "TokenManager/DeactivateToken", nil)
	cdc.RegisterConcrete(&MsgMoveTokenToWallet{}, "TokenManager/MoveTokenToWallet", nil)
	cdc.RegisterConcrete(&MsgCreateHashToken{}, "TokenManager/CreateHashToken", nil)
	cdc.RegisterConcrete(&MsgCloneToken{}, "TokenManager/CloneToken", nil)
	cdc.RegisterConcrete(&MsgFetchDocumentHash{}, "businesslogic/FetchDocumentHash", nil)
	cdc.RegisterConcrete(&MsgFetchTokensBySegmentId{}, "businesslogic/FetchTokensBySegmentId", nil)
	cdc.RegisterConcrete(&MsgFetchTokensByWalletId{}, "businesslogic/FetchTokensByWalletId", nil)
	cdc.RegisterConcrete(&MsgRevertToGenesis{}, "businesslogic/RevertToGenesis", nil)

	cdc.RegisterConcrete(&exportTypes.MsgCreateExportToken{}, "exporttoken/CreateExportToken", nil)
	cdc.RegisterConcrete(&exportTypes.MsgUpdateExportToken{}, "exporttoken/UpdateExportToken", nil)

	cdc.RegisterConcrete(&exportTypes.MsgCreateEvent{}, "exporttoken/CreateEvent", nil)

	cdc.RegisterConcrete(&importTypes.MsgCreateImportToken{}, "exporttoken/CreateImportToken", nil)
	cdc.RegisterConcrete(&importTypes.MsgUpdateImportToken{}, "exporttoken/UpdateImportToken", nil)
	cdc.RegisterConcrete(&importTypes.MsgDeleteImportToken{}, "exporttoken/DeleteImportToken", nil)

	cdc.RegisterConcrete(&MsgFetchTokenHistory{}, "exporttoken/FetchTokenHistory", nil)

	cdc.RegisterConcrete(&MsgFetchExportTokenExporter{}, "businesslogic/FetchExportTokenExporter", nil)
	cdc.RegisterConcrete(&MsgFetchExportTokenAllExporter{}, "businesslogic/FetchExportTokenAllExporter", nil)
	cdc.RegisterConcrete(&MsgFetchExportTokenDriver{}, "businesslogic/FetchExportTokenDriver", nil)
	cdc.RegisterConcrete(&MsgFetchExportTokenAllDriver{}, "businesslogic/FetchExportTokenAllDriver", nil)
	cdc.RegisterConcrete(&MsgFetchExportTokenImporter{}, "businesslogic/FetchExportTokenImporter", nil)
	cdc.RegisterConcrete(&MsgFetchExportTokenAllImporter{}, "businesslogic/FetchExportTokenAllImporter", nil)
	cdc.RegisterConcrete(&MsgFetchImportToken{}, "businesslogic/FetchImportToken", nil)
	cdc.RegisterConcrete(&MsgFetchImportTokenAll{}, "businesslogic/FetchImportTokenAll", nil)

	cdc.RegisterConcrete(&MsgFetchExportTokenExporterByExportId{}, "businesslogic/FetchExportTokenExporterByExportId", nil)
	cdc.RegisterConcrete(&MsgFetchExportTokenDriverByExportId{}, "businesslogic/FetchExportTokenDriverByExportId", nil)
	cdc.RegisterConcrete(&MsgFetchExportTokenImporterByExportId{}, "businesslogic/FetchExportTokenImporterByExportId", nil)
	cdc.RegisterConcrete(&MsgFetchImportTokenByImportId{}, "businesslogic/FetchImportTokenByImportId", nil)
	cdc.RegisterConcrete(&MsgForwardExportToken{}, "businesslogic/ForwardExportToken", nil)
	cdc.RegisterConcrete(&MsgAssignCosmosAddressToWallet{}, "businesslogic/AssignCosmosAddressToWallet", nil)

	cdc.RegisterConcrete(&MsgEventPresentQrCode{}, "businesslogic/EventPresentQrCode", nil)
	cdc.RegisterConcrete(&MsgEventAcceptGoodsForTransport{}, "businesslogic/EventAcceptGoodsForTransport", nil)
	cdc.RegisterConcrete(&MsgEventPresentGoods{}, "businesslogic/EventPresentGoods", nil)
	cdc.RegisterConcrete(&MsgEventCertifyExitOfGoods{}, "businesslogic/EventCertifyExitOfGoods", nil)
	cdc.RegisterConcrete(&MsgEventMoveToArchive{}, "businesslogic/EventMoveToArchive", nil)

	cdc.RegisterConcrete(&MsgEventExportCheck{}, "businesslogic/EventExportCheck", nil)
	cdc.RegisterConcrete(&MsgEventAcceptExport{}, "businesslogic/EventAcceptExport", nil)
	cdc.RegisterConcrete(&MsgEventExportRefusal{}, "businesslogic/EventExportRefusal", nil)

	cdc.RegisterConcrete(&MsgEventAcceptImport{}, "businesslogic/EventAcceptImport", nil)
	cdc.RegisterConcrete(&MsgEventCompleteImport{}, "businesslogic/EventCompleteImport", nil)
	cdc.RegisterConcrete(&MsgEventImportProposal{}, "businesslogic/EventImportProposal", nil)
	cdc.RegisterConcrete(&MsgEventImportRefusal{}, "businesslogic/EventImportRefusal", nil)
	cdc.RegisterConcrete(&MsgEventImportSupplement{}, "businesslogic/EventImportSupplement", nil)
	cdc.RegisterConcrete(&MsgEventImportExport{}, "businesslogic/EventImportExport", nil)
	cdc.RegisterConcrete(&MsgEventImportSubmit{}, "businesslogic/EventImportSubmit", nil)
	cdc.RegisterConcrete(&MsgEventImportRelease{}, "businesslogic/EventImportRelease", nil)

}

func RegisterInterfaces(registry cdctypes.InterfaceRegistry) {
	// this line is used by starport scaffolding # 3
	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgCreateTokenCopies{},
		&MsgUpdateTokenCopies{},
		&MsgDeleteTokenCopies{},
		&MsgCreateDocumentTokenMapper{},
		&MsgUpdateDocumentTokenMapper{},
		&MsgDeleteDocumentTokenMapper{},
		&MsgCreateToken{},
		&MsgUpdateToken{},
		&MsgActivateToken{},
		&MsgDeactivateToken{},
		&MsgMoveTokenToWallet{},
		&MsgCreateHashToken{},
		&MsgCloneToken{},
		&MsgFetchDocumentHash{},
		&MsgFetchTokensBySegmentId{},
		&MsgFetchTokensByWalletId{},
		&MsgRevertToGenesis{},
	)

	registry.RegisterImplementations((*sdk.Msg)(nil),
		&exportTypes.MsgCreateExportToken{},
		&exportTypes.MsgUpdateExportToken{},
		&exportTypes.MsgCreateEvent{},

		&MsgFetchExportTokenExporter{},
		&MsgFetchExportTokenAllExporter{},
		&MsgFetchExportTokenDriver{},
		&MsgFetchExportTokenAllDriver{},
		&MsgFetchExportTokenImporter{},
		&MsgFetchExportTokenAllImporter{},
		&MsgFetchTokenHistory{},
	)

	registry.RegisterImplementations((*sdk.Msg)(nil),
		&importTypes.MsgCreateImportToken{},
		&importTypes.MsgUpdateImportToken{},
		&importTypes.MsgDeleteImportToken{},

		&MsgFetchImportToken{},
		&MsgFetchImportTokenAll{},

		&MsgFetchExportTokenExporterByExportId{},
		&MsgFetchExportTokenDriverByExportId{},
		&MsgFetchExportTokenImporterByExportId{},
		&MsgFetchImportTokenByImportId{},
		&MsgForwardExportToken{},
		&MsgAssignCosmosAddressToWallet{},
		&MsgEventPresentQrCode{},
	)

	msgservice.RegisterMsgServiceDesc(registry, &_Msg_serviceDesc)
}

var (
	amino     = codec.NewLegacyAmino()
	ModuleCdc = codec.NewAminoCodec(amino)
)

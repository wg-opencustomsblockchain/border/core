//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
)

var _ sdk.Msg = &MsgCreateImportToken{}

func (msg *MsgCreateImportToken) Route() string {
	return RouterKey
}

func (msg *MsgCreateImportToken) Type() string {
	return "CreateImportToken"
}

func (msg *MsgCreateImportToken) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgCreateImportToken) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgCreateImportToken) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgUpdateImportToken{}

func (msg *MsgUpdateImportToken) Route() string {
	return RouterKey
}

func (msg *MsgUpdateImportToken) Type() string {
	return "UpdateImportToken"
}

func (msg *MsgUpdateImportToken) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgUpdateImportToken) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgUpdateImportToken) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgDeleteImportToken{}

func NewMsgDeleteImportToken(creator string, id string) *MsgDeleteImportToken {
	return &MsgDeleteImportToken{
		Id:      id,
		Creator: creator,
	}
}
func (msg *MsgDeleteImportToken) Route() string {
	return RouterKey
}

func (msg *MsgDeleteImportToken) Type() string {
	return "DeleteImportToken"
}

func (msg *MsgDeleteImportToken) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgDeleteImportToken) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgDeleteImportToken) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgFetchImportToken{}

func NewMsgFetchImportToken(creator string, id string) *MsgFetchImportToken {
	return &MsgFetchImportToken{
		Creator: creator,
	}
}
func (msg *MsgFetchImportToken) Route() string {
	return RouterKey
}

func (msg *MsgFetchImportToken) Type() string {
	return "FetchImportToken"
}

func (msg *MsgFetchImportToken) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgFetchImportToken) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgFetchImportToken) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgFetchImportTokenAll{}

func NewMsgFetchImportTokenAll(creator string, id string) *MsgFetchImportTokenAll {
	return &MsgFetchImportTokenAll{
		Creator: creator,
	}
}
func (msg *MsgFetchImportTokenAll) Route() string {
	return RouterKey
}

func (msg *MsgFetchImportTokenAll) Type() string {
	return "FetchImportTokenAll"
}

func (msg *MsgFetchImportTokenAll) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgFetchImportTokenAll) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgFetchImportTokenAll) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

// Accept Import

// --- 1 ---
var _ sdk.Msg = &MsgEventAcceptImport{}

func NewMsgEventAcceptImport(creator string, index string, message string) *MsgEventAcceptImport {
	return &MsgEventAcceptImport{
		Creator: creator,
		Id:      index,
		Message: message,
	}
}

func (msg *MsgEventAcceptImport) Route() string {
	return RouterKey
}

func (msg *MsgEventAcceptImport) Type() string {
	return "EventAcceptImport"
}

func (msg *MsgEventAcceptImport) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgEventAcceptImport) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgEventAcceptImport) ValidateBasic() error {

	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

// Complete Import
// --- 1 ---
var _ sdk.Msg = &MsgEventCompleteImport{}

func NewMsgEventCompleteImport(creator string, index string, message string) *MsgEventCompleteImport {
	return &MsgEventCompleteImport{
		Creator: creator,
		Id:      index,
		Message: message,
	}
}

func (msg *MsgEventCompleteImport) Route() string {
	return RouterKey
}

func (msg *MsgEventCompleteImport) Type() string {
	return "EventCompleteImport"
}

func (msg *MsgEventCompleteImport) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgEventCompleteImport) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgEventCompleteImport) ValidateBasic() error {

	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

// Import Proposal

// --- 1 ---
var _ sdk.Msg = &MsgEventImportProposal{}

func NewMsgEventImportProposal(creator string, index string, message string) *MsgEventImportProposal {
	return &MsgEventImportProposal{
		Creator: creator,
		Id:      index,
		Message: message,
	}
}

func (msg *MsgEventImportProposal) Route() string {
	return RouterKey
}

func (msg *MsgEventImportProposal) Type() string {
	return "EventImportProposal"
}

func (msg *MsgEventImportProposal) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgEventImportProposal) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgEventImportProposal) ValidateBasic() error {

	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

// Import Refusal

// --- 1 ---
var _ sdk.Msg = &MsgEventImportRefusal{}

func NewMsgEventImportRefusal(creator string, index string, message string) *MsgEventImportRefusal {
	return &MsgEventImportRefusal{
		Creator: creator,
		Id:      index,
		Message: message,
	}
}

func (msg *MsgEventImportRefusal) Route() string {
	return RouterKey
}

func (msg *MsgEventImportRefusal) Type() string {
	return "EventImportRefusal"
}

func (msg *MsgEventImportRefusal) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgEventImportRefusal) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgEventImportRefusal) ValidateBasic() error {

	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

// Import Supplement

// --- 1 ---
var _ sdk.Msg = &MsgEventImportSupplement{}

func NewMsgEventImportSupplement(creator string, index string, message string) *MsgEventImportSupplement {
	return &MsgEventImportSupplement{
		Creator: creator,
		Id:      index,
		Message: message,
	}
}

func (msg *MsgEventImportSupplement) Route() string {
	return RouterKey
}

func (msg *MsgEventImportSupplement) Type() string {
	return "EventImportSupplement"
}

func (msg *MsgEventImportSupplement) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgEventImportSupplement) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgEventImportSupplement) ValidateBasic() error {

	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

// Import Export

// --- 1 ---
var _ sdk.Msg = &MsgEventImportExport{}

func NewMsgEventImportExport(creator string, index string, message string) *MsgEventImportExport {
	return &MsgEventImportExport{
		Creator: creator,
		Id:      index,
		Message: message,
	}
}

func (msg *MsgEventImportExport) Route() string {
	return RouterKey
}

func (msg *MsgEventImportExport) Type() string {
	return "EventImportExport"
}

func (msg *MsgEventImportExport) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgEventImportExport) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgEventImportExport) ValidateBasic() error {

	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

// Import Submit

// --- 1 ---
var _ sdk.Msg = &MsgEventImportSubmit{}

func NewMsgEventImportSubmit(creator string, index string, message string) *MsgEventImportSubmit {
	return &MsgEventImportSubmit{
		Creator: creator,
		Id:      index,
		Message: message,
	}
}

func (msg *MsgEventImportSubmit) Route() string {
	return RouterKey
}

func (msg *MsgEventImportSubmit) Type() string {
	return "EventImportSubmit"
}

func (msg *MsgEventImportSubmit) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgEventImportSubmit) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgEventImportSubmit) ValidateBasic() error {

	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

// Import Release

// --- 1 ---
var _ sdk.Msg = &MsgEventImportRelease{}

func NewMsgEventImportRelease(creator string, index string, message string) *MsgEventImportRelease {
	return &MsgEventImportRelease{
		Creator: creator,
		Id:      index,
		Message: message,
	}
}

func (msg *MsgEventImportRelease) Route() string {
	return RouterKey
}

func (msg *MsgEventImportRelease) Type() string {
	return "EventImportRelease"
}

func (msg *MsgEventImportRelease) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgEventImportRelease) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgEventImportRelease) ValidateBasic() error {

	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

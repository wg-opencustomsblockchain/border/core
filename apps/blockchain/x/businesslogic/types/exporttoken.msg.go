//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package types

// TODO: Check if truly deprecated and maybe get rid of it? #CodeReview

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
)

var _ sdk.Msg = &MsgCreateExportToken{}

func (msg *MsgCreateExportToken) Route() string {
	return RouterKey
}

func (msg *MsgCreateExportToken) Type() string {
	return "CreateExportToken"
}

func (msg *MsgCreateExportToken) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgCreateExportToken) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgCreateExportToken) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgUpdateExportToken{}

func (msg *MsgUpdateExportToken) Route() string {
	return RouterKey
}

func (msg *MsgUpdateExportToken) Type() string {
	return "UpdateExportToken"
}

func (msg *MsgUpdateExportToken) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgUpdateExportToken) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgUpdateExportToken) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgFetchExportTokenExporter{}

func NewMsgFetchExportTokenExporter(creator string, id string) *MsgFetchExportTokenExporter {
	return &MsgFetchExportTokenExporter{
		Creator: creator,
	}
}
func (msg *MsgFetchExportTokenExporter) Route() string {
	return RouterKey
}

func (msg *MsgFetchExportTokenExporter) Type() string {
	return "FetchExportTokenExporter"
}

func (msg *MsgFetchExportTokenExporter) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgFetchExportTokenExporter) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgFetchExportTokenExporter) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgFetchExportTokenAllExporter{}

func NewMsgFetchExportTokenAllExporter(creator string, id string) *MsgFetchExportTokenAllExporter {
	return &MsgFetchExportTokenAllExporter{
		Creator: creator,
	}
}
func (msg *MsgFetchExportTokenAllExporter) Route() string {
	return RouterKey
}

func (msg *MsgFetchExportTokenAllExporter) Type() string {
	return "FetchExportTokenAllExporter"
}

func (msg *MsgFetchExportTokenAllExporter) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgFetchExportTokenAllExporter) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgFetchExportTokenAllExporter) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgFetchExportTokenDriver{}

func NewMsgFetchExportTokenDriver(creator string, id string) *MsgFetchExportTokenDriver {
	return &MsgFetchExportTokenDriver{
		Creator: creator,
	}
}
func (msg *MsgFetchExportTokenDriver) Route() string {
	return RouterKey
}

func (msg *MsgFetchExportTokenDriver) Type() string {
	return "FetchExportTokenDriver"
}

func (msg *MsgFetchExportTokenDriver) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgFetchExportTokenDriver) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgFetchExportTokenDriver) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgFetchExportTokenAllDriver{}

func NewMsgFetchExportTokenAllDriver(creator string, id string) *MsgFetchExportTokenAllDriver {
	return &MsgFetchExportTokenAllDriver{
		Creator: creator,
	}
}
func (msg *MsgFetchExportTokenAllDriver) Route() string {
	return RouterKey
}

func (msg *MsgFetchExportTokenAllDriver) Type() string {
	return "FetchExportTokenAllDriver"
}

func (msg *MsgFetchExportTokenAllDriver) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgFetchExportTokenAllDriver) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgFetchExportTokenAllDriver) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgFetchExportTokenImporter{}

func NewMsgFetchExportTokenImporter(creator string, id string) *MsgFetchExportTokenImporter {
	return &MsgFetchExportTokenImporter{
		Creator: creator,
	}
}
func (msg *MsgFetchExportTokenImporter) Route() string {
	return RouterKey
}

func (msg *MsgFetchExportTokenImporter) Type() string {
	return "FetchExportTokenImporter"
}

func (msg *MsgFetchExportTokenImporter) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgFetchExportTokenImporter) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgFetchExportTokenImporter) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgFetchExportTokenAllImporter{}

func NewMsgFetchExportTokenAllImporter(creator string, id string) *MsgFetchExportTokenAllImporter {
	return &MsgFetchExportTokenAllImporter{
		Creator: creator,
	}
}
func (msg *MsgFetchExportTokenAllImporter) Route() string {
	return RouterKey
}

func (msg *MsgFetchExportTokenAllImporter) Type() string {
	return "FetchExportTokenAllImporter"
}

func (msg *MsgFetchExportTokenAllImporter) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgFetchExportTokenAllImporter) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgFetchExportTokenAllImporter) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgForwardExportToken{}

func NewMsgForwardExportToken(creator string, id string) *MsgForwardExportToken {
	return &MsgForwardExportToken{
		Creator: creator,
		Id:      id,
	}
}

func (msg *MsgForwardExportToken) Route() string {
	return RouterKey
}

func (msg *MsgForwardExportToken) Type() string {
	return "ForwardExportToken"
}

func (msg *MsgForwardExportToken) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgForwardExportToken) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgForwardExportToken) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

// --- Event MSG ---
// --- 1 ---
var _ sdk.Msg = &MsgEventPresentQrCode{}

func NewMsgEventPresentQrCode(creator string, id string, message string) *MsgEventPresentQrCode {
	return &MsgEventPresentQrCode{
		Creator: creator,
		Id:      id,
		Message: message,
	}
}

func (msg *MsgEventPresentQrCode) Route() string {
	return RouterKey
}

func (msg *MsgEventPresentQrCode) Type() string {
	return "EventPresentQrCode"
}

func (msg *MsgEventPresentQrCode) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgEventPresentQrCode) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgEventPresentQrCode) ValidateBasic() error {

	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

// --- 2 ---
var _ sdk.Msg = &MsgEventAcceptGoodsForTransport{}

func NewMsgEventAcceptGoodsForTransport(creator string, id string, message string) *MsgEventAcceptGoodsForTransport {
	return &MsgEventAcceptGoodsForTransport{
		Creator: creator,
		Id:      id,
		Message: message,
	}
}

func (msg *MsgEventAcceptGoodsForTransport) Route() string {
	return RouterKey
}

func (msg *MsgEventAcceptGoodsForTransport) Type() string {
	return "EventAcceptGoodsForTransport"
}

func (msg *MsgEventAcceptGoodsForTransport) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgEventAcceptGoodsForTransport) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgEventAcceptGoodsForTransport) ValidateBasic() error {

	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

// --- 1 ---
var _ sdk.Msg = &MsgEventPresentGoods{}

func NewMsgEventPresentGoods(creator string, id string, message string) *MsgEventPresentGoods {
	return &MsgEventPresentGoods{
		Creator: creator,
		Id:      id,
		Message: message,
	}
}

func (msg *MsgEventPresentGoods) Route() string {
	return RouterKey
}

func (msg *MsgEventPresentGoods) Type() string {
	return "EventPresentGoods"
}

func (msg *MsgEventPresentGoods) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgEventPresentGoods) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgEventPresentGoods) ValidateBasic() error {

	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

// --- 3 ---
var _ sdk.Msg = &MsgEventCertifyExitOfGoods{}

func NewMsgEventCertifyExitOfGoods(creator string, id string, message string) *MsgEventCertifyExitOfGoods {
	return &MsgEventCertifyExitOfGoods{
		Creator: creator,
		Id:      id,
		Message: message,
	}
}

func (msg *MsgEventCertifyExitOfGoods) Route() string {
	return RouterKey
}

func (msg *MsgEventCertifyExitOfGoods) Type() string {
	return "EventCertifyExitOfGoods"
}

func (msg *MsgEventCertifyExitOfGoods) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgEventCertifyExitOfGoods) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgEventCertifyExitOfGoods) ValidateBasic() error {

	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

// --- 4 ---
var _ sdk.Msg = &MsgEventMoveToArchive{}

func NewMsgEventMoveToArchive(creator string, id string, message string) *MsgEventMoveToArchive {
	return &MsgEventMoveToArchive{
		Creator: creator,
		Id:      id,
		Message: message,
	}
}

func (msg *MsgEventMoveToArchive) Route() string {
	return RouterKey
}

func (msg *MsgEventMoveToArchive) Type() string {
	return "EventMoveToArchive"
}

func (msg *MsgEventMoveToArchive) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgEventMoveToArchive) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgEventMoveToArchive) ValidateBasic() error {

	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

// ExportCheck
// --- 1 ---
var _ sdk.Msg = &MsgEventExportCheck{}

func NewMsgEventExportCheck(creator string, index string, message string) *MsgEventExportCheck {
	return &MsgEventExportCheck{
		Creator: creator,
		Id:      index,
		Message: message,
	}
}

func (msg *MsgEventExportCheck) Route() string {
	return RouterKey
}

func (msg *MsgEventExportCheck) Type() string {
	return "EventExportCheck"
}

func (msg *MsgEventExportCheck) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgEventExportCheck) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgEventExportCheck) ValidateBasic() error {

	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

// EventAcceptExport

// --- 1 ---
var _ sdk.Msg = &MsgEventAcceptExport{}

func NewMsgEventAcceptExport(creator string, index string, message string) *MsgEventAcceptExport {
	return &MsgEventAcceptExport{
		Creator: creator,
		Id:      index,
		Message: message,
	}
}

func (msg *MsgEventAcceptExport) Route() string {
	return RouterKey
}

func (msg *MsgEventAcceptExport) Type() string {
	return "EventAcceptExport"
}

func (msg *MsgEventAcceptExport) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgEventAcceptExport) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgEventAcceptExport) ValidateBasic() error {

	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

// --- 1 ---
var _ sdk.Msg = &MsgEventExportRefusal{}

func NewMsgEventExportRefusal(creator string, index string, message string) *MsgEventExportRefusal {
	return &MsgEventExportRefusal{
		Creator: creator,
		Id:      index,
		Message: message,
	}
}

func (msg *MsgEventExportRefusal) Route() string {
	return RouterKey
}

func (msg *MsgEventExportRefusal) Type() string {
	return "EventExportRefusal"
}

func (msg *MsgEventExportRefusal) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgEventExportRefusal) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgEventExportRefusal) ValidateBasic() error {

	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

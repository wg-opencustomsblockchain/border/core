// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
package types

import (
	"context"
	tokenTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/token/types"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
	exportTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/exporttoken/types"
	importTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/importtoken/types"
	walletTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
)

// interface to import the wallet keeper
type WalletKeeper interface {
	FetchTokenHistoryGlobal(goCtx context.Context, msg *walletTypes.MsgFetchGetTokenHistoryGlobal) (*walletTypes.MsgFetchGetTokenHistoryGlobalResponse, error)
	FetchTokenHistoryGlobalAll(goCtx context.Context, msg *walletTypes.MsgFetchAllTokenHistoryGlobal) (*walletTypes.MsgFetchAllTokenHistoryGlobalResponse, error)
	FetchSegmentHistory(goCtx context.Context, msg *walletTypes.MsgFetchGetSegmentHistory) (*walletTypes.MsgFetchGetSegmentHistoryResponse, error)
	FetchSegmentHistoryAll(goCtx context.Context, msg *walletTypes.MsgFetchAllSegmentHistory) (*walletTypes.MsgFetchAllSegmentHistoryResponse, error)
	FetchSegment(goCtx context.Context, msg *walletTypes.MsgFetchGetSegment) (*walletTypes.MsgFetchGetSegmentResponse, error)
	FetchSegmentAll(goCtx context.Context, msg *walletTypes.MsgFetchAllSegment) (*walletTypes.MsgFetchAllSegmentResponse, error)
	FetchWalletHistory(goCtx context.Context, msg *walletTypes.MsgFetchGetWalletHistory) (*walletTypes.MsgFetchGetWalletHistoryResponse, error)
	FetchWalletHistoryAll(goCtx context.Context, msg *walletTypes.MsgFetchAllWalletHistory) (*walletTypes.MsgFetchAllWalletHistoryResponse, error)
	FetchWallet(goCtx context.Context, msg *walletTypes.MsgFetchGetWallet) (*walletTypes.MsgFetchGetWalletResponse, error)
	FetchWalletAll(goCtx context.Context, msg *walletTypes.MsgFetchAllWallet) (*walletTypes.MsgFetchAllWalletResponse, error)
	MoveTokenToSegment(goCtx context.Context, msg *walletTypes.MsgMoveTokenToSegment) (*walletTypes.MsgEmptyResponse, error)
	MoveTokenToWallet(goCtx context.Context, msg *walletTypes.MsgMoveTokenToWallet) (*walletTypes.MsgEmptyResponse, error)
	RemoveTokenRefFromSegment(goCtx context.Context, msg *walletTypes.MsgRemoveTokenRefFromSegment) (*walletTypes.MsgEmptyResponse, error)
	CreateSegment(goCtx context.Context, msg *walletTypes.MsgCreateSegment) (*walletTypes.MsgIdResponse, error)
	CreateSegmentWithId(goCtx context.Context, msg *walletTypes.MsgCreateSegmentWithId) (*walletTypes.MsgIdResponse, error)
	UpdateSegment(goCtx context.Context, msg *walletTypes.MsgUpdateSegment) (*walletTypes.MsgEmptyResponse, error)
	CreateTokenRef(goCtx context.Context, msg *walletTypes.MsgCreateTokenRef) (*walletTypes.MsgIdResponse, error)
	CreateWallet(goCtx context.Context, msg *walletTypes.MsgCreateWallet) (*walletTypes.MsgIdResponse, error)
	CreateWalletWithId(goCtx context.Context, msg *walletTypes.MsgCreateWalletWithId) (*walletTypes.MsgIdResponse, error)
	UpdateWallet(goCtx context.Context, msg *walletTypes.MsgUpdateWallet) (*walletTypes.MsgEmptyResponse, error)
	AssignCosmosAddressToWallet(goCtx context.Context, msg *walletTypes.MsgAssignCosmosAddressToWallet) (*walletTypes.MsgEmptyResponse, error)
	HasWallet(ctx sdk.Context, id string) bool
	SetSegment(ctx sdk.Context, segment walletTypes.Segment)
	Segment(c context.Context, req *walletTypes.QueryGetSegmentRequest) (*walletTypes.QueryGetSegmentResponse, error)
	Wallet(c context.Context, req *walletTypes.QueryGetWalletRequest) (*walletTypes.QueryGetWalletResponse, error)
	RevertToGenesis(ctx sdk.Context)
}

// interface to import the authorization keeper
type AuthorizationKeeper interface {
	FetchAllApplicationRole(goCtx context.Context, msg *types.MsgFetchAllApplicationRole) (*types.MsgFetchAllApplicationRoleResponse, error)
	FetchAllBlockchainAccount(goCtx context.Context, msg *types.MsgFetchAllBlockchainAccount) (*types.MsgFetchAllBlockchainAccountResponse, error)
	FetchApplicationRole(goCtx context.Context, msg *types.MsgFetchApplicationRole) (*types.MsgFetchApplicationRoleResponse, error)
	FetchBlockchainAccount(goCtx context.Context, msg *types.MsgFetchBlockchainAccount) (*types.MsgFetchBlockchainAccountResponse, error)
	CreateApplicationRole(goCtx context.Context, msg *types.MsgCreateApplicationRole) (*types.MsgCreateApplicationRoleResponse, error)
	UpdateApplicationRole(goCtx context.Context, msg *types.MsgUpdateApplicationRole) (*types.MsgUpdateApplicationRoleResponse, error)
	DeactivateApplicationRole(goCtx context.Context, msg *types.MsgDeactivateApplicationRole) (*types.MsgDeactivateApplicationRoleResponse, error)
	CreateBlockchainAccount(goCtx context.Context, msg *types.MsgCreateBlockchainAccount) (*types.MsgCreateBlockchainAccountResponse, error)
	GrantAppRoleToBlockchainAccount(goCtx context.Context, msg *types.MsgGrantAppRoleToBlockchainAccount) (*types.MsgGrantAppRoleToBlockchainAccountResponse, error)
	RevokeAppRoleFromBlockchainAccount(goCtx context.Context, msg *types.MsgRevokeAppRoleFromBlockchainAccount) (*types.MsgRevokeAppRoleFromBlockchainAccountResponse, error)
	DeactivateBlockchainAccount(goCtx context.Context, msg *types.MsgDeactivateBlockchainAccount) (*types.MsgDeactivateBlockchainAccountResponse, error)
	HasRole(ctx sdk.Context, key string, applicationRole string) (bool, error)
	SetApplicationRole(ctx sdk.Context, applicationRole types.ApplicationRole)
	SetBlockchainAccount(ctx sdk.Context, blockchainAccount types.BlockchainAccount)
	RevertToGenesis(ctx sdk.Context)
}

// interface to import the ExportToken keeper
type ExportTokenKeeper interface {
	//ExportEventHistory(c context.Context, req *exportTypes.QueryGetEventHistoryRequest) (*exportTypes.QueryGetEventHistoryResponse, error)
	//ExportTokenAll(c context.Context, req *exportTypes.QueryAllExportTokenRequest) (*exportTypes.QueryAllExportTokenResponse, error)
	//ExportToken(c context.Context, req *exportTypes.QueryGetExportTokenRequest) (*exportTypes.QueryGetExportTokenResponse, error)
	//ExportMapping(c context.Context, req *exportTypes.QueryGetExportMappingRequest) (*exportTypes.QueryGetExportMappingResponse, error)

	CreateExportEvent(goCtx context.Context, msg *exportTypes.MsgCreateEvent) (*exportTypes.MsgCreateEventResponse, error)
	CreateExportToken(goCtx context.Context, msg *exportTypes.MsgCreateExportToken) (*exportTypes.MsgCreateExportTokenResponse, error)
	UpdateExportToken(goCtx context.Context, msg *exportTypes.MsgUpdateExportToken) (*exportTypes.MsgUpdateExportTokenResponse, error)

	FetchExportToken(goCtx context.Context, msg *exportTypes.MsgFetchExportToken) (*exportTypes.MsgFetchExportTokenResponse, error)
	FetchExportMapping(goCtx context.Context, msg *exportTypes.MsgFetchExportMapping) (*exportTypes.MsgFetchExportMappingResponse, error)
}

// interface to import the ImportToken keeper
type ImportTokenKeeper interface {
	//ImportEventHistory(c context.Context, req *importTypes.QueryGetEventHistoryRequest) (*importTypes.QueryGetEventHistoryResponse, error)
	//ImportTokenAll(c context.Context, req *importTypes.QueryAllImportTokenRequest) (*importTypes.QueryAllImportTokenResponse, error)
	//ImportToken(c context.Context, req *importTypes.QueryGetImportTokenRequest) (*importTypes.QueryGetImportTokenResponse, error)
	//ImportMapping(c context.Context, req *importTypes.QueryGetImportMappingRequest) (*importTypes.QueryGetImportMappingResponse, error)

	CreateImportEvent(goCtx context.Context, msg *importTypes.MsgCreateEvent) (*importTypes.MsgCreateEventResponse, error)
	CreateImportToken(goCtx context.Context, msg *importTypes.MsgCreateImportToken) (*importTypes.MsgCreateImportTokenResponse, error)
	UpdateImportToken(goCtx context.Context, msg *importTypes.MsgUpdateImportToken) (*importTypes.MsgUpdateImportTokenResponse, error)

	FetchImportToken(goCtx context.Context, msg *importTypes.MsgFetchImportToken) (*importTypes.MsgFetchImportTokenResponse, error)
	FetchImportMapping(goCtx context.Context, msg *importTypes.MsgFetchImportMapping) (*importTypes.MsgFetchImportMappingResponse, error)
}

type TokenKeeper interface {
	FetchTokenHistory(goCtx context.Context, msg *tokenTypes.MsgFetchTokenHistory) (*tokenTypes.MsgFetchTokenHistoryResponse, error)
}

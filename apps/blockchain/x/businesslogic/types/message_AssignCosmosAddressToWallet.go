//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
)

var _ sdk.Msg = &MsgAssignCosmosAddressToWallet{}

func NewMsgAssignCosmosAddressToWallet(creator string, address string, walletId string) *MsgAssignCosmosAddressToWallet {
	return &MsgAssignCosmosAddressToWallet{
		Creator:       creator,
		CosmosAddress: address,
		WalletId:      walletId,
	}
}

func (msg *MsgAssignCosmosAddressToWallet) Route() string {
	return RouterKey
}

func (msg *MsgAssignCosmosAddressToWallet) Type() string {
	return "AssignCosmosAddressToWallet"
}

func (msg *MsgAssignCosmosAddressToWallet) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgAssignCosmosAddressToWallet) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgAssignCosmosAddressToWallet) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

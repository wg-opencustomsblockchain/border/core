//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package types

import (
	"fmt"
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
	"time"
)

var _ sdk.Msg = &MsgFetchImportTokenByImportId{}

func NewMsgFetchImportTokenByImportId(creator string, exportId string, walletId string) *MsgFetchImportTokenByImportId {
	return &MsgFetchImportTokenByImportId{
		Creator:       creator,
		ImportId:      exportId,
		Timestamp:     fmt.Sprint(time.Now().UnixNano()),
		TokenWalletId: walletId,
	}
}

func (msg *MsgFetchImportTokenByImportId) Route() string {
	return RouterKey
}

func (msg *MsgFetchImportTokenByImportId) Type() string {
	return "FetchImportTokenByImportId"
}

func (msg *MsgFetchImportTokenByImportId) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgFetchImportTokenByImportId) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgFetchImportTokenByImportId) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package types

import (
	"fmt"
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
	"time"
)

var _ sdk.Msg = &MsgFetchExportTokenExporterByExportId{}

func NewMsgFetchExportTokenExporterByExportId(creator string, exportId string, walletId string) *MsgFetchExportTokenExporterByExportId {
	return &MsgFetchExportTokenExporterByExportId{
		Creator:       creator,
		ExportId:      exportId,
		Timestamp:     fmt.Sprint(time.Now().UnixNano()),
		TokenWalletId: walletId,
	}
}

func (msg *MsgFetchExportTokenExporterByExportId) Route() string {
	return RouterKey
}

func (msg *MsgFetchExportTokenExporterByExportId) Type() string {
	return "FetchExportTokenExporterByExportId"
}

func (msg *MsgFetchExportTokenExporterByExportId) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgFetchExportTokenExporterByExportId) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgFetchExportTokenExporterByExportId) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgFetchExportTokenDriverByExportId{}

func NewMsgFetchExportTokenDriverByExportId(creator string, exportId string, walletId string) *MsgFetchExportTokenDriverByExportId {
	return &MsgFetchExportTokenDriverByExportId{
		Creator:   creator,
		ExportId:  exportId,
		Timestamp: fmt.Sprint(time.Now().UnixNano()),
	}
}

func (msg *MsgFetchExportTokenDriverByExportId) Route() string {
	return RouterKey
}

func (msg *MsgFetchExportTokenDriverByExportId) Type() string {
	return "FetchExportTokenDriverByExportId"
}

func (msg *MsgFetchExportTokenDriverByExportId) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgFetchExportTokenDriverByExportId) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgFetchExportTokenDriverByExportId) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgFetchExportTokenImporterByExportId{}

func NewMsgFetchExportTokenImporterByExportId(creator string, exportId string, walletId string) *MsgFetchExportTokenImporterByExportId {
	return &MsgFetchExportTokenImporterByExportId{
		Creator:   creator,
		ExportId:  exportId,
		Timestamp: fmt.Sprint(time.Now().UnixNano()),
	}
}

func (msg *MsgFetchExportTokenImporterByExportId) Route() string {
	return RouterKey
}

func (msg *MsgFetchExportTokenImporterByExportId) Type() string {
	return "FetchExportTokenImporterByExportId"
}

func (msg *MsgFetchExportTokenImporterByExportId) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgFetchExportTokenImporterByExportId) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgFetchExportTokenImporterByExportId) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

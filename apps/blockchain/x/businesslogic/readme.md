# BusinessLogic

**BusinessLogic** is a blockchain module built using Cosmos SDK and Tendermint and created with [Starport](https://github.com/tendermint/starport).

## Cloning and updating of the repository
The Wallet repository is a blockchain module used by the [TokenManger](https://gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/token-manager/token-management)

The Token Manager uses the modules [Wallet](https://gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/token-manager/e-wallet) and [Token](https://gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/token-manager/e-document), which are developed in their corresponding repositories. Both modules are currently already added in this repository, 
so everything is ready to go. To get the latest version from either of these modules, the module has to be copied from its repository from the "x/" folder into the "x/" folder of this repository.

## Get started
In order to start the blockchain, it is needed to navigate to the location of the TokenManager and run

```
starport chain serve
```

`serve` command installs dependencies, builds, initializes and starts your blockchain in development.

## Configure

Your blockchain in development can be configured with `config.yml`. To learn more see the [reference](https://github.com/tendermint/starport#documentation).

## Launch

To launch your blockchain live on mutliple nodes use `starport network` commands. Learn more about [Starport Network](https://github.com/tendermint/spn).

## CLI commands
The following commands are supported by the token manager module

### Transactions
After launching the blockchain a folder named ./TokenMangerd will be generated in the home folder and a binary named TokenManagerd in the Go Path.

It is possible to change the default folder and binary name with the help of the config.yml file.

To run CLI commands it is possible to simply run

```
TokenManagerd 
```

Afterwards more information about the cli commands will be printed.


## Learn more

- [Starport](https://github.com/tendermint/starport)
- [Cosmos SDK documentation](https://docs.cosmos.network)
- [Cosmos SDK Tutorials](https://tutorials.cosmos.network)
- [Discord](https://discord.gg/W8trcGV)

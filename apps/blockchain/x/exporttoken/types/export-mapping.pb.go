// Code generated by protoc-gen-gogo. DO NOT EDIT.
// source: exporttoken/export-mapping.proto

package types

import (
	fmt "fmt"
	_ "github.com/gogo/protobuf/gogoproto"
	proto "github.com/gogo/protobuf/proto"
	io "io"
	math "math"
	math_bits "math/bits"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.GoGoProtoPackageIsVersion3 // please upgrade the proto package

// Creator: Cosmos Address of who created the initial Token
// Index : This is the Export ID. Currently we are using the MRN.
// tokenId: The Corresponding Token which holds the entire EAD.
type ExportMapping struct {
	Creator string `protobuf:"bytes,1,opt,name=creator,proto3" json:"creator,omitempty"`
	Index   string `protobuf:"bytes,2,opt,name=index,proto3" json:"index,omitempty"`
	Tokenid string `protobuf:"bytes,3,opt,name=tokenid,proto3" json:"tokenid,omitempty"`
}

func (m *ExportMapping) Reset()         { *m = ExportMapping{} }
func (m *ExportMapping) String() string { return proto.CompactTextString(m) }
func (*ExportMapping) ProtoMessage()    {}
func (*ExportMapping) Descriptor() ([]byte, []int) {
	return fileDescriptor_9475cec1c3d765c2, []int{0}
}
func (m *ExportMapping) XXX_Unmarshal(b []byte) error {
	return m.Unmarshal(b)
}
func (m *ExportMapping) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	if deterministic {
		return xxx_messageInfo_ExportMapping.Marshal(b, m, deterministic)
	} else {
		b = b[:cap(b)]
		n, err := m.MarshalToSizedBuffer(b)
		if err != nil {
			return nil, err
		}
		return b[:n], nil
	}
}
func (m *ExportMapping) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ExportMapping.Merge(m, src)
}
func (m *ExportMapping) XXX_Size() int {
	return m.Size()
}
func (m *ExportMapping) XXX_DiscardUnknown() {
	xxx_messageInfo_ExportMapping.DiscardUnknown(m)
}

var xxx_messageInfo_ExportMapping proto.InternalMessageInfo

func (m *ExportMapping) GetCreator() string {
	if m != nil {
		return m.Creator
	}
	return ""
}

func (m *ExportMapping) GetIndex() string {
	if m != nil {
		return m.Index
	}
	return ""
}

func (m *ExportMapping) GetTokenid() string {
	if m != nil {
		return m.Tokenid
	}
	return ""
}

func init() {
	proto.RegisterType((*ExportMapping)(nil), "org.borderblockchain.exporttoken.ExportMapping")
}

func init() { proto.RegisterFile("exporttoken/export-mapping.proto", fileDescriptor_9475cec1c3d765c2) }

var fileDescriptor_9475cec1c3d765c2 = []byte{
	// 263 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x4c, 0x90, 0xb1, 0x4e, 0xc4, 0x30,
	0x10, 0x44, 0x63, 0x10, 0x20, 0x22, 0xd1, 0x44, 0x57, 0x44, 0x14, 0x56, 0x44, 0x45, 0x93, 0xb8,
	0xe0, 0x0f, 0x90, 0x28, 0xaf, 0x41, 0x34, 0xd0, 0x39, 0xf6, 0x9e, 0x59, 0x25, 0xe7, 0xb5, 0x36,
	0x3e, 0x29, 0xf7, 0x15, 0xf0, 0x59, 0x94, 0x57, 0x52, 0xa2, 0xe4, 0x47, 0x50, 0x12, 0xd0, 0x5d,
	0x37, 0x63, 0xbd, 0x19, 0x6b, 0x36, 0x2d, 0xa0, 0x0f, 0xc4, 0x31, 0x52, 0x03, 0x5e, 0x2d, 0xba,
	0xdc, 0xea, 0x10, 0xd0, 0xbb, 0x2a, 0x30, 0x45, 0xca, 0x0a, 0x62, 0x57, 0xd5, 0xc4, 0x16, 0xb8,
	0x6e, 0xc9, 0x34, 0xe6, 0x5d, 0xa3, 0xaf, 0x4e, 0x62, 0xb7, 0x2b, 0x47, 0x8e, 0x66, 0x58, 0x4d,
	0x6a, 0xc9, 0xdd, 0xbd, 0xa6, 0x37, 0x4f, 0x33, 0xb4, 0x5e, 0xea, 0xb2, 0x3c, 0xbd, 0x32, 0x0c,
	0x3a, 0x12, 0xe7, 0xa2, 0x10, 0xf7, 0xd7, 0xcf, 0xff, 0x36, 0x5b, 0xa5, 0x17, 0xe8, 0x2d, 0xf4,
	0xf9, 0xd9, 0xfc, 0xbe, 0x98, 0x89, 0x9f, 0xfb, 0xd1, 0xe6, 0xe7, 0x0b, 0xff, 0x67, 0x1f, 0x3f,
	0xc4, 0xd7, 0x20, 0xc5, 0x61, 0x90, 0xe2, 0x67, 0x90, 0xe2, 0x73, 0x94, 0xc9, 0x61, 0x94, 0xc9,
	0xf7, 0x28, 0x93, 0xb7, 0x9d, 0xc3, 0x58, 0x51, 0x00, 0xdf, 0x92, 0xc3, 0x2e, 0xa2, 0xe9, 0x36,
	0xb4, 0xf3, 0x56, 0x47, 0x24, 0x5f, 0x11, 0x3b, 0xd5, 0x61, 0x8b, 0x86, 0x7c, 0x09, 0x86, 0x3c,
	0x6d, 0xf7, 0xaa, 0xd6, 0x1d, 0xa8, 0xe3, 0xb0, 0x9a, 0xa9, 0x01, 0x56, 0x16, 0x1d, 0x46, 0xdd,
	0x96, 0x1b, 0x6a, 0x2d, 0xb0, 0x7a, 0x99, 0x3e, 0x5e, 0x6b, 0xaf, 0x1d, 0xb0, 0xea, 0xd5, 0xe9,
	0xcd, 0xe2, 0x3e, 0x40, 0x57, 0x5f, 0xce, 0x9b, 0x1f, 0x7e, 0x03, 0x00, 0x00, 0xff, 0xff, 0x21,
	0xc6, 0x32, 0x35, 0x4f, 0x01, 0x00, 0x00,
}

func (m *ExportMapping) Marshal() (dAtA []byte, err error) {
	size := m.Size()
	dAtA = make([]byte, size)
	n, err := m.MarshalToSizedBuffer(dAtA[:size])
	if err != nil {
		return nil, err
	}
	return dAtA[:n], nil
}

func (m *ExportMapping) MarshalTo(dAtA []byte) (int, error) {
	size := m.Size()
	return m.MarshalToSizedBuffer(dAtA[:size])
}

func (m *ExportMapping) MarshalToSizedBuffer(dAtA []byte) (int, error) {
	i := len(dAtA)
	_ = i
	var l int
	_ = l
	if len(m.Tokenid) > 0 {
		i -= len(m.Tokenid)
		copy(dAtA[i:], m.Tokenid)
		i = encodeVarintExportMapping(dAtA, i, uint64(len(m.Tokenid)))
		i--
		dAtA[i] = 0x1a
	}
	if len(m.Index) > 0 {
		i -= len(m.Index)
		copy(dAtA[i:], m.Index)
		i = encodeVarintExportMapping(dAtA, i, uint64(len(m.Index)))
		i--
		dAtA[i] = 0x12
	}
	if len(m.Creator) > 0 {
		i -= len(m.Creator)
		copy(dAtA[i:], m.Creator)
		i = encodeVarintExportMapping(dAtA, i, uint64(len(m.Creator)))
		i--
		dAtA[i] = 0xa
	}
	return len(dAtA) - i, nil
}

func encodeVarintExportMapping(dAtA []byte, offset int, v uint64) int {
	offset -= sovExportMapping(v)
	base := offset
	for v >= 1<<7 {
		dAtA[offset] = uint8(v&0x7f | 0x80)
		v >>= 7
		offset++
	}
	dAtA[offset] = uint8(v)
	return base
}
func (m *ExportMapping) Size() (n int) {
	if m == nil {
		return 0
	}
	var l int
	_ = l
	l = len(m.Creator)
	if l > 0 {
		n += 1 + l + sovExportMapping(uint64(l))
	}
	l = len(m.Index)
	if l > 0 {
		n += 1 + l + sovExportMapping(uint64(l))
	}
	l = len(m.Tokenid)
	if l > 0 {
		n += 1 + l + sovExportMapping(uint64(l))
	}
	return n
}

func sovExportMapping(x uint64) (n int) {
	return (math_bits.Len64(x|1) + 6) / 7
}
func sozExportMapping(x uint64) (n int) {
	return sovExportMapping(uint64((x << 1) ^ uint64((int64(x) >> 63))))
}
func (m *ExportMapping) Unmarshal(dAtA []byte) error {
	l := len(dAtA)
	iNdEx := 0
	for iNdEx < l {
		preIndex := iNdEx
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return ErrIntOverflowExportMapping
			}
			if iNdEx >= l {
				return io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= uint64(b&0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		fieldNum := int32(wire >> 3)
		wireType := int(wire & 0x7)
		if wireType == 4 {
			return fmt.Errorf("proto: ExportMapping: wiretype end group for non-group")
		}
		if fieldNum <= 0 {
			return fmt.Errorf("proto: ExportMapping: illegal tag %d (wire type %d)", fieldNum, wire)
		}
		switch fieldNum {
		case 1:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field Creator", wireType)
			}
			var stringLen uint64
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowExportMapping
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				stringLen |= uint64(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			intStringLen := int(stringLen)
			if intStringLen < 0 {
				return ErrInvalidLengthExportMapping
			}
			postIndex := iNdEx + intStringLen
			if postIndex < 0 {
				return ErrInvalidLengthExportMapping
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.Creator = string(dAtA[iNdEx:postIndex])
			iNdEx = postIndex
		case 2:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field Index", wireType)
			}
			var stringLen uint64
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowExportMapping
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				stringLen |= uint64(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			intStringLen := int(stringLen)
			if intStringLen < 0 {
				return ErrInvalidLengthExportMapping
			}
			postIndex := iNdEx + intStringLen
			if postIndex < 0 {
				return ErrInvalidLengthExportMapping
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.Index = string(dAtA[iNdEx:postIndex])
			iNdEx = postIndex
		case 3:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field Tokenid", wireType)
			}
			var stringLen uint64
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowExportMapping
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				stringLen |= uint64(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			intStringLen := int(stringLen)
			if intStringLen < 0 {
				return ErrInvalidLengthExportMapping
			}
			postIndex := iNdEx + intStringLen
			if postIndex < 0 {
				return ErrInvalidLengthExportMapping
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.Tokenid = string(dAtA[iNdEx:postIndex])
			iNdEx = postIndex
		default:
			iNdEx = preIndex
			skippy, err := skipExportMapping(dAtA[iNdEx:])
			if err != nil {
				return err
			}
			if (skippy < 0) || (iNdEx+skippy) < 0 {
				return ErrInvalidLengthExportMapping
			}
			if (iNdEx + skippy) > l {
				return io.ErrUnexpectedEOF
			}
			iNdEx += skippy
		}
	}

	if iNdEx > l {
		return io.ErrUnexpectedEOF
	}
	return nil
}
func skipExportMapping(dAtA []byte) (n int, err error) {
	l := len(dAtA)
	iNdEx := 0
	depth := 0
	for iNdEx < l {
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return 0, ErrIntOverflowExportMapping
			}
			if iNdEx >= l {
				return 0, io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= (uint64(b) & 0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		wireType := int(wire & 0x7)
		switch wireType {
		case 0:
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return 0, ErrIntOverflowExportMapping
				}
				if iNdEx >= l {
					return 0, io.ErrUnexpectedEOF
				}
				iNdEx++
				if dAtA[iNdEx-1] < 0x80 {
					break
				}
			}
		case 1:
			iNdEx += 8
		case 2:
			var length int
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return 0, ErrIntOverflowExportMapping
				}
				if iNdEx >= l {
					return 0, io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				length |= (int(b) & 0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			if length < 0 {
				return 0, ErrInvalidLengthExportMapping
			}
			iNdEx += length
		case 3:
			depth++
		case 4:
			if depth == 0 {
				return 0, ErrUnexpectedEndOfGroupExportMapping
			}
			depth--
		case 5:
			iNdEx += 4
		default:
			return 0, fmt.Errorf("proto: illegal wireType %d", wireType)
		}
		if iNdEx < 0 {
			return 0, ErrInvalidLengthExportMapping
		}
		if depth == 0 {
			return iNdEx, nil
		}
	}
	return 0, io.ErrUnexpectedEOF
}

var (
	ErrInvalidLengthExportMapping        = fmt.Errorf("proto: negative length found during unmarshaling")
	ErrIntOverflowExportMapping          = fmt.Errorf("proto: integer overflow")
	ErrUnexpectedEndOfGroupExportMapping = fmt.Errorf("proto: unexpected end of group")
)

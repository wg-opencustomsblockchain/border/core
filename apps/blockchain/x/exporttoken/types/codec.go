//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package types

import (
	"github.com/cosmos/cosmos-sdk/codec"
	cdctypes "github.com/cosmos/cosmos-sdk/codec/types"
	"github.com/cosmos/cosmos-sdk/types/msgservice"
)

// RegisterCodec defines the Messages this module accepts
func RegisterCodec(cdc *codec.LegacyAmino) {
	// this line is used by starport scaffolding # 2
	//cdc.RegisterConcrete(&MsgCreateExportToken{}, "exporttoken/CreateExportToken", nil)
	//cdc.RegisterConcrete(&MsgUpdateExportToken{}, "exporttoken/UpdateExportToken", nil)
	//cdc.RegisterConcrete(&MsgCreateEvent{}, "exporttoken/CreateEvent", nil)
}

// RegisterInterfaces defines the implementations this module accepts
func RegisterInterfaces(registry cdctypes.InterfaceRegistry) {
	// this line is used by starport scaffolding # 3
	//registry.RegisterImplementations((*sdk.Msg)(nil),
	//	&MsgCreateExportToken{},
	//	&MsgUpdateExportToken{},
	//	&MsgCreateEvent{},
	//)

	msgservice.RegisterMsgServiceDesc(registry, &_Msg_serviceDesc)
}

var (
	amino     = codec.NewLegacyAmino()
	ModuleCdc = codec.NewAminoCodec(amino)
)

// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
package types

const (

	// Role for creating a new Role
	ExportTokenRoleReadToken = "EXPORT-TOKEN-MODULE_READ-TOKEN"

	// Role for creating a new Role
	ExportTokenRoleCreateToken = "EXPORT-TOKEN-MODULE_CREATE-TOKEN"
	// Role for initial writing - Status 1
	ExportTokenEventRoleForward = "EXPORT-TOKEN-MODULE_EXPORT-RECORDED"
	// Role for preparing the Export - Status 2
	ExportTokenRoleEventReady = "EXPORT-TOKEN-MODULE_EXPORT-READY"
	// Role for Pickup procedure - Status 3
	ExportTokenRoleEventPickup = "EXPORT-TOKEN-MODULE_EXPORT-PICKUP"
	// Role for presenting at office - Status 4
	ExportTokenRoleEventPresent = "EXPORT-TOKEN-MODULE_EXPORT-PRESENTED"
	// Role for confirming the exit of goods - Status 5
	ExportTokenRoleEventExit = "EXPORT-TOKEN-MODULE_EXPORT-EXIT"
	// Role for Archiving the Token - Status 6
	ExportTokenRoleEventArchive = "EXPORT-TOKEN-MODULE_EXPORT-ARCHIVED"

	// -- No Status Changes --
	// Event for presenting for a mobile customs check
	ExportTokenRoleExportCheck = "EXPORT-TOKEN-MODULE_EXPORT_CHECK"
	// Event for presenting for a mobile customs check
	ExportTokenRoleMobileCheck = "EXPORT-TOKEN-MODULE_EXPORT_MOBILECHECK"
	// Event in case the Export is refused by an importer
	ExportTokenRoleExportRefusal = "EXPORT-TOKEN-MODULE_EXPORT_REFUSAL"
	// Event in case the Export is refused by an importer
	ExportTokenRoleExportAccepted = "EXPORT-TOKEN-MODULE_EXPORT_ACCEPTED"
	// The Event after the broker added a new EAD
	ExportTokenRoleExportNew = "EXPORT_NEW"
)

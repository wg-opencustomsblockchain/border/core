//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package types

import (
	"fmt"
)

// DefaultIndex is the default capability global index
const DefaultIndex uint64 = 1

// DefaultGenesis returns the default Capability genesis state
func DefaultGenesis() *GenesisState {
	return &GenesisState{
		// this line is used by starport scaffolding # genesis/types/default
		EventHistoryList:  []*EventHistory{},
		ExportMappingList: []*ExportMapping{},
		ExportTokenList:   []*ExportToken{},
	}
}

// Validate performs basic genesis state validation returning an error upon any
// failure.
func (gs GenesisState) Validate() error {
	// this line is used by starport scaffolding # genesis/types/validate
	// Check for duplicated index in eventHistory
	eventHistoryIndexMap := make(map[string]bool)

	for _, elem := range gs.EventHistoryList {
		if _, ok := eventHistoryIndexMap[elem.Index]; ok {
			return fmt.Errorf("duplicated index for eventHistory")
		}
		eventHistoryIndexMap[elem.Index] = true
	}
	// Check for duplicated index in exportMapping
	exportMappingIndexMap := make(map[string]bool)

	for _, elem := range gs.ExportMappingList {
		if _, ok := exportMappingIndexMap[elem.Index]; ok {
			return fmt.Errorf("duplicated index for exportMapping")
		}
		exportMappingIndexMap[elem.Index] = true
	}
	// Check for duplicated ID in exportToken
	exportTokenIdMap := make(map[string]bool)

	for _, elem := range gs.ExportTokenList {
		if _, ok := exportTokenIdMap[elem.Id]; ok {
			return fmt.Errorf("duplicated id for exportToken")
		}
		exportTokenIdMap[elem.Id] = true
	}

	return nil
}

//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/exporttoken/types"
	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"
)

// SetExportMapping set a specific ExportMapping in the store from its index
func (k Keeper) SetExportMapping(ctx sdk.Context, exportMapping types.ExportMapping) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.ExportMappingKey))
	b := k.cdc.MustMarshal(&exportMapping)
	store.Set(types.KeyPrefix(exportMapping.Index), b)
}

// GetExportMapping returns a ExportMapping from its index
func (k Keeper) GetExportMapping(ctx sdk.Context, index string) (val types.ExportMapping, found bool) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.ExportMappingKey))

	b := store.Get(types.KeyPrefix(index))
	if b == nil {
		return val, false
	}

	k.cdc.MustUnmarshal(b, &val)
	return val, true
}

// GetAllExportMapping returns all ExportMappings
func (k Keeper) GetAllExportMapping(ctx sdk.Context) (list []types.ExportMapping) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.ExportMappingKey))
	iterator := sdk.KVStorePrefixIterator(store, []byte{})

	defer iterator.Close()

	for ; iterator.Valid(); iterator.Next() {
		var val types.ExportMapping
		k.cdc.MustUnmarshal(iterator.Value(), &val)
		list = append(list, val)
	}

	return
}

// RemoveExportMapping removes an ExportMapping from the store
func (k Keeper) RemoveExportMapping(ctx sdk.Context, index string) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.ExportMappingKey))
	store.Delete(types.KeyPrefix(index))
}

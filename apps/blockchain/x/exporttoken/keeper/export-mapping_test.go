//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"fmt"
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/stretchr/testify/assert"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/exporttoken/types"
)

func createNExportMapping(keeper *Keeper, ctx sdk.Context, n int) []types.ExportMapping {
	items := make([]types.ExportMapping, n)
	for i := range items {
		items[i].Creator = "any"
		items[i].Index = fmt.Sprintf("%d", i)
		keeper.SetExportMapping(ctx, items[i])
	}
	return items
}

func TestExportMappingGet(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	items := createNExportMapping(keeper, ctx, 10)
	for _, item := range items {
		rst, found := keeper.GetExportMapping(ctx, item.Index)
		assert.True(t, found)
		assert.Equal(t, item, rst)
	}
}
func TestExportMappingRemove(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	items := createNExportMapping(keeper, ctx, 10)
	for _, item := range items {
		keeper.RemoveExportMapping(ctx, item.Index)
		_, found := keeper.GetExportMapping(ctx, item.Index)
		assert.False(t, found)
	}
}

func TestExportMappingGetAll(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	items := createNExportMapping(keeper, ctx, 10)
	assert.Equal(t, items, keeper.GetAllExportMapping(ctx))
}

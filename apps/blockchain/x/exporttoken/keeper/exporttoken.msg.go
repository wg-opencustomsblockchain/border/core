//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"
	"fmt"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/exporttoken/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
	"github.com/google/uuid"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// CreateExportToken creates a new ExportToken filled with data from the msg parameter.
// It returns the newly created token including the token's history.
// If the token includes a MRN, a ExportMapping is also being created.
func (k Keeper) CreateExportToken(goCtx context.Context, msg *types.MsgCreateExportToken) (*types.MsgCreateExportTokenResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	var exportToken = ParseCreateMsg(msg)

	if exportToken.ExportId == "" {
		return nil, status.Error(codes.InvalidArgument, "Missing ExportID on Export Token.")
	}

	k.CreateNewToken(goCtx, exportToken)

	k.SetExportMapping(ctx, types.ExportMapping{
		Creator: msg.Creator,
		Index:   msg.ExportId,
		Tokenid: exportToken.Id,
	})

	k.CreateExportEvent(goCtx, &types.MsgCreateEvent{
		Creator:   msg.Creator,
		Index:     exportToken.Id,
		Status:    "0",
		Message:   "Created",
		EventType: "EXPORT_NEW",
	})

	history, _ := k.GetEventHistory(ctx, exportToken.Id)

	return &types.MsgCreateExportTokenResponse{
		ExportToken: &exportToken,
		Events:      history.Events,
	}, nil

}

// UpdateExportToken adds a new event to the Token History.
// It returns the token including the new token's history.
func (k Keeper) UpdateExportToken(goCtx context.Context, msg *types.MsgUpdateExportToken) (*types.MsgUpdateExportTokenResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	var exportToken = parseUpdateMsg(msg)

	// Checks that the element exists
	if !k.HasExportToken(goCtx, msg.Id) {
		return nil, sdkerrors.Wrap(sdkerrors.ErrKeyNotFound, fmt.Sprintf("key %s doesn't exist", msg.Id))
	}

	storedToken := k.GetExportToken(goCtx, msg.Id)
	if storedToken.ExportId != msg.ExportId {
		return nil, status.Error(codes.InvalidArgument, "MRN mismatch.")
	}

	k.SetExportToken(goCtx, exportToken)

	k.CreateExportEvent(goCtx, &types.MsgCreateEvent{
		Creator:   msg.Creator,
		Index:     msg.Id,
		Status:    msg.Status,
		Message:   "Updated",
		EventType: "EXPORT_TOKEN_UPDATE",
	})

	history, _ := k.GetEventHistory(ctx, msg.Id)

	return &types.MsgUpdateExportTokenResponse{
		ExportToken: &exportToken,
		Events:      history.Events,
	}, nil
}

func ParseCreateMsg(msg *types.MsgCreateExportToken) types.ExportToken {

	var exportToken = types.ExportToken{
		Creator:               msg.Creator,
		Id:                    uuid.New().String(),
		Exporter:              msg.Exporter,
		Declarant:             msg.Declarant,
		Representative:        msg.Representative,
		Consignee:             msg.Consignee,
		CustomsOfficeOfExport: msg.CustomsOfficeOfExport,
		CustomOfficeOfExit:    msg.CustomOfficeOfExit,
		GoodsItems:            msg.GoodsItems,
		TransportToBorder:     msg.TransportToBorder,
		TransportAtBorder:     msg.TransportAtBorder,

		ExportId:                   msg.ExportId,
		UniqueConsignmentReference: msg.UniqueConsignmentReference,
		LocalReferenceNumber:       msg.LocalReferenceNumber,
		DestinationCountry:         msg.DestinationCountry,
		ExportCountry:              msg.ExportCountry,
		Itinerary:                  msg.Itinerary,
		ReleaseDateAndTime:         msg.ReleaseDateAndTime,
		IncotermCode:               msg.IncotermCode,
		IncotermLocation:           msg.IncotermLocation,
		TotalGrossMass:             msg.TotalGrossMass,
		GoodsItemQuantity:          msg.GoodsItemQuantity,
		TotalPackagesQuantity:      msg.TotalPackagesQuantity,
		NatureOfTransaction:        msg.NatureOfTransaction,
		TotalAmountInvoiced:        msg.TotalAmountInvoiced,
		InvoiceCurrency:            msg.InvoiceCurrency,
	}

	return exportToken
}

func (k Keeper) FetchExportToken(goCtx context.Context, msg *types.MsgFetchExportToken) (*types.MsgFetchExportTokenResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)
	token := k.GetExportToken(goCtx, msg.Id)
	history, historyFound := k.GetEventHistory(ctx, msg.Id)
	if !historyFound {
		history = types.EventHistory{}
	}

	return &types.MsgFetchExportTokenResponse{
		ExportToken: &token,
		Events:      history.Events,
	}, nil
}

func (k Keeper) FetchExportMapping(goCtx context.Context, msg *types.MsgFetchExportMapping) (*types.MsgFetchExportMappingResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)
	mapping, found := k.GetExportMapping(ctx, msg.Id)
	if !found {
		return nil, status.Error(codes.InvalidArgument, "not found")
	}

	token := k.GetExportToken(goCtx, mapping.Tokenid)
	history, historyFound := k.GetEventHistory(ctx, mapping.Tokenid)
	if !historyFound {
		history = types.EventHistory{}
	}

	return &types.MsgFetchExportMappingResponse{
		ExportToken: &token,
		Events:      history.Events,
	}, nil
}

func parseUpdateMsg(msg *types.MsgUpdateExportToken) types.ExportToken {
	var exportToken = types.ExportToken{
		Creator:        msg.Creator,
		Id:             msg.Id,
		Exporter:       msg.Exporter,
		Declarant:      msg.Declarant,
		Representative: msg.Representative,
		Consignee:      msg.Consignee,

		CustomsOfficeOfExport: msg.CustomsOfficeOfExport,
		CustomOfficeOfExit:    msg.CustomOfficeOfExit,
		GoodsItems:            msg.GoodsItems,
		TransportToBorder:     msg.TransportToBorder,
		TransportAtBorder:     msg.TransportAtBorder,

		ExportId:                   msg.ExportId,
		UniqueConsignmentReference: msg.UniqueConsignmentReference,
		LocalReferenceNumber:       msg.LocalReferenceNumber,
		DestinationCountry:         msg.DestinationCountry,
		ExportCountry:              msg.ExportCountry,
		Itinerary:                  msg.Itinerary,
		ReleaseDateAndTime:         msg.ReleaseDateAndTime,
		IncotermCode:               msg.IncotermCode,
		IncotermLocation:           msg.IncotermLocation,
		TotalGrossMass:             msg.TotalGrossMass,
		GoodsItemQuantity:          msg.GoodsItemQuantity,
		TotalPackagesQuantity:      msg.TotalPackagesQuantity,
		NatureOfTransaction:        msg.NatureOfTransaction,
		TotalAmountInvoiced:        msg.TotalAmountInvoiced,
		InvoiceCurrency:            msg.InvoiceCurrency,
	}

	return exportToken
}

//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"fmt"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/exporttoken/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/google/uuid"
)

func createNExportToken(keeper *Keeper, ctx sdk.Context, n int) []types.ExportToken {
	items := make([]types.ExportToken, n)
	for i := range items {
		items[i].Creator = "any"
		items[i].ExportId = fmt.Sprintf("%d", i)
		items[i].Id = uuid.New().String()

		keeper.SetExportToken(ctx, items[i])
		keeper.SetExportMapping(ctx, types.ExportMapping{
			Index:   items[i].ExportId,
			Tokenid: items[i].Id,
		})
	}
	return items
}

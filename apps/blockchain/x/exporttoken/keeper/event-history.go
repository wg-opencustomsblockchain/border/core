//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/exporttoken/types"
	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"
)

// SetEventHistory set a specific eventHistory in the store from its index
func (k Keeper) SetEventHistory(ctx sdk.Context, eventHistory types.EventHistory) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.EventHistoryKey))
	b := k.cdc.MustMarshal(&eventHistory)
	store.Set(types.KeyPrefix(eventHistory.Index), b)
}

// GetEventHistory returns a eventHistory from its index
func (k Keeper) GetEventHistory(ctx sdk.Context, index string) (val types.EventHistory, found bool) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.EventHistoryKey))

	b := store.Get(types.KeyPrefix(index))
	if b == nil {
		return val, false
	}

	k.cdc.MustUnmarshal(b, &val)
	return val, true
}

// GetAllEventHistory returns all eventHistory. Required for Genesis creation.
func (k Keeper) GetAllEventHistory(ctx sdk.Context) (list []types.EventHistory) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.EventHistoryKey))
	iterator := sdk.KVStorePrefixIterator(store, []byte{})

	defer iterator.Close()

	for ; iterator.Valid(); iterator.Next() {
		var val types.EventHistory
		k.cdc.MustUnmarshal(iterator.Value(), &val)
		list = append(list, val)
	}

	return
}

// DeleteEventHistory removes a eventHistory from the store
func (k Keeper) RemoveEventHistory(ctx sdk.Context, index string) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.EventHistoryKey))
	store.Delete(types.KeyPrefix(index))
}

//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"fmt"
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/stretchr/testify/assert"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/exporttoken/types"
)

func createEventHistory(keeper *Keeper, ctx sdk.Context, n int) []types.EventHistory {
	items := make([]types.EventHistory, n)
	for i := range items {
		items[i].Creator = "any"
		items[i].Index = fmt.Sprintf("%d", i)
		keeper.SetEventHistory(ctx, items[i])
	}
	return items
}

func TestEventHistoryGet(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	items := createEventHistory(keeper, ctx, 10)
	for _, item := range items {
		rst, found := keeper.GetEventHistory(ctx, item.Index)
		assert.True(t, found)
		assert.Equal(t, item, rst)
	}
}
func TestEventHistoryGetAll(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	items := createEventHistory(keeper, ctx, 10)
	assert.Equal(t, items, keeper.GetAllEventHistory(ctx))
}

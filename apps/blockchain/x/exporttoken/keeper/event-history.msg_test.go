//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"fmt"
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/exporttoken/types"
)

func TestCreateExportEvent(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	wctx := sdk.WrapSDKContext(ctx)
	tokens := createNExportToken(keeper, ctx, 1)
	creator := "A"
	for i := 0; i < 5; i++ {
		expected := &types.MsgCreateEvent{
			Creator:   creator,
			Index:     tokens[0].Id,
			Status:    fmt.Sprintf("Status %d", i),
			Message:   fmt.Sprintf("Message %d", i),
			EventType: fmt.Sprintf("EventType %d", i),
		}
		_, err := keeper.CreateExportEvent(wctx, expected)
		require.NoError(t, err)
		rst, found := keeper.GetEventHistory(ctx, expected.Index)
		require.True(t, found)
		assert.Equal(t, expected.Creator, rst.Creator)
	}
}

//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"
	"encoding/json"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/exporttoken/types"
	TokenTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/token/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
)

const (
	tokenType = "ExportToken"
)

// CreateNewToken creates a new ExportToken in the store.
func (k Keeper) CreateNewToken(goCtx context.Context, exportToken types.ExportToken) {
	k.tokenKeeper.CreateToken(goCtx, &TokenTypes.MsgCreateToken{
		Creator:       exportToken.Creator,
		Id:            exportToken.Id,
		Timestamp:     GetTimestamp(),
		TokenType:     tokenType,
		ChangeMessage: "CreateToken",
		SegmentId:     exportToken.ExportId,
	})

	js, err := json.Marshal(exportToken)
	if err != nil {
		k.Logger(sdk.UnwrapSDKContext(goCtx)).Error("Error marshaling to json", err)
	}

	k.tokenKeeper.UpdateTokenInformation(goCtx, &TokenTypes.MsgUpdateTokenInformation{
		Creator: exportToken.Creator,
		TokenId: exportToken.Id,
		Data:    string(js),
	})
}

func (k Keeper) SetExportToken(goCtx context.Context, exportToken types.ExportToken) {
	js, err := json.Marshal(exportToken)
	if err != nil {
		k.Logger(sdk.UnwrapSDKContext(goCtx)).Error("Error marshaling to json", err)
	}

	k.tokenKeeper.UpdateTokenInformation(goCtx, &TokenTypes.MsgUpdateTokenInformation{
		Creator: exportToken.Creator,
		TokenId: exportToken.Id,
		Data:    string(js),
	})
}

// GetExportToken returns a exportToken from its id
func (k Keeper) GetExportToken(goCtx context.Context, id string) types.ExportToken {
	//store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.ExportTokenKey))
	//var exportToken types.ExportToken
	//k.cdc.MustUnmarshal(store.Get(GetExportTokenIDBytes(id)), &exportToken)

	resp, errFetch := k.tokenKeeper.FetchToken(goCtx, &TokenTypes.MsgFetchToken{
		Creator: "",
		Id:      id,
	})
	if errFetch != nil {
		return types.ExportToken{}
	}

	var exportToken types.ExportToken
	errJson := json.Unmarshal([]byte(resp.Token.Info.Data), &exportToken)
	if errJson != nil {
		return types.ExportToken{}
	}
	return exportToken
}

// HasExportToken checks if the exportToken exists in the store
func (k Keeper) HasExportToken(goCtx context.Context, id string) bool {
	//store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.ExportTokenKey))
	//return store.Has(GetExportTokenIDBytes(id))

	token := k.GetExportToken(goCtx, id)
	return token.Id != ""
}

// GetAllExportToken returns all exportToken
func (k Keeper) GetAllExportToken(goCtx context.Context) (list []types.ExportToken) {
	//store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.ExportTokenKey))
	//iterator := sdk.KVStorePrefixIterator(store, []byte{})
	//
	//defer iterator.Close()
	//
	//for ; iterator.Valid(); iterator.Next() {
	//	var val types.ExportToken
	//	k.cdc.MustUnmarshal(iterator.Value(), &val)
	//	list = append(list, val)
	//}
	//
	//return

	resp, err := k.tokenKeeper.FetchAllToken(goCtx, &TokenTypes.MsgFetchAllToken{
		Creator: "",
	})
	if err != nil {
		return nil
	}
	for _, t := range resp.Token {
		if t.TokenType != tokenType {
			continue
		}
		var exportToken types.ExportToken
		errJson := json.Unmarshal([]byte(t.Info.Data), exportToken)
		if errJson == nil {
			list = append(list, exportToken)
		}
	}
	return
}

// GetExportTokenIDBytes returns the byte representation of the ID
func GetExportTokenIDBytes(id string) []byte {
	return []byte(id)
}

// GetExportTokenIDFromBytes returns ID in uint64 format from a byte array
func GetExportTokenIDFromBytes(bz []byte) string {
	return string(bz)
}

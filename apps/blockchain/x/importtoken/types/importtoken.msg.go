//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
)

var _ sdk.Msg = &MsgCreateImportToken{}

/* deprecated
func NewMsgCreateImportToken(
	creator string,
	mrn string,

	consignor *Company,
	exporter *Company,
	consignee *Company,
	customOfficeOfExit *CustomsOffice,
	customOfficeOfEntry *CustomsOffice,
	goodsItems *[]GoodsItem,
	transportAtBorder *Transport,
	transportFromBorder *Transport,

	importId string,
	localReferenceNumber string,
	destinationCountry string,
	exportCountry string,
	incotermCode string,
	incotermLocation string,
	totalGrossMass float64,
	goodsItemQuantity string,
	totalPackagesQuantity string,
	releaseDateAndTime string,
	natureOfTransaction string,
	totalAmountInvoiced int64,
	invoiceCurrency string,
	status string,
	user string,
	timestamp string) *MsgCreateImportToken {
	return &MsgCreateImportToken{
		Creator: creator,
	}
}
*/

func (msg *MsgCreateImportToken) Route() string {
	return RouterKey
}

func (msg *MsgCreateImportToken) Type() string {
	return "CreateImportToken"
}

func (msg *MsgCreateImportToken) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgCreateImportToken) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgCreateImportToken) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgUpdateImportToken{}

// deprecated
/*
func NewMsgUpdateImportToken(
	creator string,
	id uint64,
	mrn string,

	consignor *Company,
	exporter *Company,
	consignee *Company,
	customOfficeOfExit *CustomsOffice,
	customOfficeOfEntry *CustomsOffice,
	goodsItems *[]GoodsItem,
	transportAtBorder *Transport,
	transportFromBorder *Transport,

	importId string,
	localReferenceNumber string,
	destinationCountry string,
	exportCountry string,
	incotermCode string,
	incotermLocation string,
	totalGrossMass string,
	goodsItemQuantity string,
	totalPackagesQuantity string,
	releaseDateAndTime string,
	natureOfTransaction string,
	totalAmountInvoiced string,
	invoiceCurrency string,
	status string,
	user string,
	timestamp string) *MsgUpdateImportToken {
	return &MsgUpdateImportToken{
		Creator: creator,
	}
}
*/

func (msg *MsgUpdateImportToken) Route() string {
	return RouterKey
}

func (msg *MsgUpdateImportToken) Type() string {
	return "UpdateImportToken"
}

func (msg *MsgUpdateImportToken) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgUpdateImportToken) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgUpdateImportToken) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgCreateImportToken{}

func NewMsgDeleteImportToken(creator string, id string) *MsgDeleteImportToken {
	return &MsgDeleteImportToken{
		Id:      id,
		Creator: creator,
	}
}
func (msg *MsgDeleteImportToken) Route() string {
	return RouterKey
}

func (msg *MsgDeleteImportToken) Type() string {
	return "DeleteImportToken"
}

func (msg *MsgDeleteImportToken) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgDeleteImportToken) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgDeleteImportToken) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgFetchImportToken{}

func (msg *MsgFetchImportToken) Route() string {
	return RouterKey
}

func (msg *MsgFetchImportToken) Type() string {
	return "FetchImportToken"
}

func (msg *MsgFetchImportToken) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgFetchImportToken) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgFetchImportToken) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgFetchImportMapping{}

func (msg *MsgFetchImportMapping) Route() string {
	return RouterKey
}

func (msg *MsgFetchImportMapping) Type() string {
	return "FetchImportMapping"
}

func (msg *MsgFetchImportMapping) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgFetchImportMapping) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgFetchImportMapping) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

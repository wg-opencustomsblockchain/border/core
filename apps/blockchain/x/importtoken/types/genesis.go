//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package types

import (
	"fmt"
)

// DefaultIndex is the default capability global index
const DefaultIndex uint64 = 1

// DefaultGenesis returns the default Capability genesis state
func DefaultGenesis() *GenesisState {
	return &GenesisState{
		// this line is used by starport scaffolding # genesis/types/default
		EventHistoryList:  []*EventHistory{},
		ImportMappingList: []*ImportMapping{},
		ImportTokenList:   []*ImportToken{},
	}
}

// Validate performs basic genesis state validation returning an error upon any
// failure.
func (gs GenesisState) Validate() error {
	// this line is used by starport scaffolding # genesis/types/validate
	// Check for duplicated index in eventHistory
	eventHistoryIndexMap := make(map[string]bool)

	for _, elem := range gs.EventHistoryList {
		if _, ok := eventHistoryIndexMap[elem.Index]; ok {
			return fmt.Errorf("duplicated index for eventHistory")
		}
		eventHistoryIndexMap[elem.Index] = true
	}
	// Check for duplicated index in importMapping
	importMappingIndexMap := make(map[string]bool)

	for _, elem := range gs.ImportMappingList {
		if _, ok := importMappingIndexMap[elem.Index]; ok {
			return fmt.Errorf("duplicated index for importMapping")
		}
		importMappingIndexMap[elem.Index] = true
	}
	// Check for duplicated ID in importToken
	importTokenIdMap := make(map[string]bool)

	for _, elem := range gs.ImportTokenList {
		if _, ok := importTokenIdMap[elem.Id]; ok {
			return fmt.Errorf("duplicated id for importToken")
		}
		importTokenIdMap[elem.Id] = true
	}

	return nil
}

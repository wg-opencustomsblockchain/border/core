//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package types

const (

	// Role for reading Token
	ImportTokenRoleReadToken = "EXPORT-TOKEN-MODULE_READ-TOKEN"

	// Role for creating a Token
	ImportTokenRoleCreate = "IMPORT-TOKEN-MODULE_CREATE-TOKEN"

	// Role for updating a Token
	ImportTokenRoleUpdate = "IMPORT-TOKEN-MODULE_UPDATE-TOKEN"

	// Role for an export being accewpted as an import - Status 1
	ImportTokenRoleAccept = "IMPORT-TOKEN-MODULE_IMPORT-ACCEPTED"
	// Role for an import Process being completed - Status 2
	ImportTokenRoleComplete = "IMPORT-TOKEN-MODULE_IMPORT-COMPLETED"

	// -- No Status Changes --
	// Role for updating the token by the import
	ImportTokenRolePropose = "IMPORT-TOKEN-MODULE_IMPORT-PROPOSAL"
	// Role for Refusing the token from Export
	ImportTokenRoleRefuse = "IMPORT-TOKEN-MODULE_IMPORT-REFUSED"
	// Role for updating the token by the import
	ImportTokenRoleSupplement = "IMPORT-TOKEN-MODULE_IMPORT-SUPPLEMENT"

	// NOT IMPLEMENTED YET
	ImportTokenRoleExport = "IMPORT-TOKEN-MODULE_IMPORT-EXPORTED"
	// NOT IMPLEMENTED YET
	ImportTokenRoleSubmit = "IMPORT-TOKEN-MODULE_IMPORT-SUBMITTED"
	// NOT IMPLEMENTED YET
	ImportTokenRoleRelease = "IMPORT-TOKEN-MODULE_IMPORT-RELEASE"
)

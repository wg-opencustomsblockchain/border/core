// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
package types

import (
	"context"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/token/types"
)

// interface to import the token keeper
type TokenKeeper interface {
	CreateToken(goCtx context.Context, msg *types.MsgCreateToken) (*types.MsgIdResponse, error)
	UpdateToken(goCtx context.Context, msg *types.MsgUpdateToken) (*types.MsgEmptyResponse, error)
	UpdateTokenInformation(goCtx context.Context, msg *types.MsgUpdateTokenInformation) (*types.MsgEmptyResponse, error)

	FetchAllToken(goCtx context.Context, msg *types.MsgFetchAllToken) (*types.MsgFetchAllTokenResponse, error)
	FetchAllTokenHistory(goCtx context.Context, msg *types.MsgFetchAllTokenHistory) (*types.MsgFetchAllTokenHistoryResponse, error)
	FetchTokenHistory(goCtx context.Context, msg *types.MsgFetchTokenHistory) (*types.MsgFetchTokenHistoryResponse, error)
	FetchToken(goCtx context.Context, msg *types.MsgFetchToken) (*types.MsgFetchTokenResponse, error)
}

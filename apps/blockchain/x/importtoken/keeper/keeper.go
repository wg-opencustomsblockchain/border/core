//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

// Package keeper is being used for saving, reading and manipulating import tokens
package keeper

import (
	"fmt"
	"time"

	"github.com/tendermint/tendermint/libs/log"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/importtoken/types"
	"github.com/cosmos/cosmos-sdk/codec"
	sdk "github.com/cosmos/cosmos-sdk/types"
)

type (
	Keeper struct {
		cdc         codec.Codec
		storeKey    sdk.StoreKey
		memKey      sdk.StoreKey
		tokenKeeper types.TokenKeeper
	}
)

// NewKeeper creates a new keeper instance
func NewKeeper(cdc codec.Codec, storeKey sdk.StoreKey, tokenKeeper types.TokenKeeper, memKey sdk.StoreKey) *Keeper {
	return &Keeper{
		cdc:         cdc,
		storeKey:    storeKey,
		tokenKeeper: tokenKeeper,
		memKey:      memKey,
	}
}

// Logger returns the logger for the keeper instance
func (k Keeper) Logger(ctx sdk.Context) log.Logger {
	return ctx.Logger().With("module", fmt.Sprintf("x/%s", types.ModuleName))
}

// GetTimestamp is a helper method to return the current time in the correct format.
func GetTimestamp() string {
	return time.Now().UTC().Format(time.RFC3339)
}

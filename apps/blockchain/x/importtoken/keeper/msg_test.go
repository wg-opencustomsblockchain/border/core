//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"
)

//func setupMsgServer(t testing.TB) (types.MsgServer, context.Context) {
// keeper, ctx := setupKeeper(t)
// return NewMsgServerImpl(*keeper), sdk.WrapSDKContext(ctx)
//}

func setupMsgServer(t testing.TB) (Keeper, context.Context) {
	k, ctx := setupKeeper(t)
	return *k, sdk.WrapSDKContext(ctx)
}

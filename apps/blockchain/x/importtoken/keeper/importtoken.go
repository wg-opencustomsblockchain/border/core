//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"
	"encoding/json"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/importtoken/types"
	TokenTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/token/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
)

const (
	tokenType = "ImportToken"
)

// CreateNewToken creates a new ImportToken in the store.
func (k Keeper) CreateNewToken(goCtx context.Context, importToken types.ImportToken) {
	k.tokenKeeper.CreateToken(goCtx, &TokenTypes.MsgCreateToken{
		Creator:       importToken.Creator,
		Id:            importToken.Id,
		Timestamp:     GetTimestamp(),
		TokenType:     tokenType,
		ChangeMessage: "CreateToken",
		SegmentId:     importToken.ImportId,
	})

	js, err := json.Marshal(importToken)
	if err != nil {
		k.Logger(sdk.UnwrapSDKContext(goCtx)).Error("Error marshaling to json", err)
	}

	k.tokenKeeper.UpdateTokenInformation(goCtx, &TokenTypes.MsgUpdateTokenInformation{
		Creator: importToken.Creator,
		TokenId: importToken.Id,
		Data:    string(js),
	})
}

// SetImportToken updates a specific ImportToken in the store. If the token does not exist, it will be created.
func (k Keeper) SetImportToken(goCtx context.Context, importToken types.ImportToken) {
	js, err := json.Marshal(importToken)
	if err != nil {
		k.Logger(sdk.UnwrapSDKContext(goCtx)).Error("Error marshaling to json", err)
	}

	k.tokenKeeper.UpdateTokenInformation(goCtx, &TokenTypes.MsgUpdateTokenInformation{
		Creator: importToken.Creator,
		TokenId: importToken.Id,
		Data:    string(js),
	})
}

// GetImportToken returns a importToken from its id
func (k Keeper) GetImportToken(goCtx context.Context, id string) types.ImportToken {
	resp, errFetch := k.tokenKeeper.FetchToken(goCtx, &TokenTypes.MsgFetchToken{
		Creator: "",
		Id:      id,
	})
	if errFetch != nil {
		return types.ImportToken{}
	}

	var importToken types.ImportToken
	errJson := json.Unmarshal([]byte(resp.Token.Info.Data), &importToken)
	if errJson != nil {
		return types.ImportToken{}
	}
	return importToken
}

// HasImportToken checks if the importToken exists in the store
func (k Keeper) HasImportToken(goCtx context.Context, id string) bool {
	token := k.GetImportToken(goCtx, id)
	return token.Id != ""
}

// GetAllImportToken returns all importToken
func (k Keeper) GetAllImportToken(goCtx context.Context) (list []types.ImportToken) {
	resp, err := k.tokenKeeper.FetchAllToken(goCtx, &TokenTypes.MsgFetchAllToken{
		Creator: "",
	})
	if err != nil {
		return nil
	}
	for _, t := range resp.Token {
		if t.TokenType != tokenType {
			continue
		}
		var importToken types.ImportToken
		errJson := json.Unmarshal([]byte(t.Info.Data), importToken)
		if errJson == nil {
			list = append(list, importToken)
		}
	}
	return
}

// GetImportTokenIDBytes returns the byte representation of the ID
func GetImportTokenIDBytes(id string) []byte {
	return []byte(id)
}

// GetImportTokenIDFromBytes returns ID in uint64 format from a byte array
func GetImportTokenIDFromBytes(bz []byte) string {
	return string(bz)
}

//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/importtoken/types"
	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/cosmos/cosmos-sdk/types/query"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// ImportEventHistoryAll provides all persisted events for the Genesis creation/Export.
func (k Keeper) ImportEventHistoryAll(c context.Context, req *types.QueryAllEventHistoryRequest) (*types.QueryAllEventHistoryResponse, error) {
	if req == nil {
		return nil, status.Error(codes.InvalidArgument, "invalid request")
	}

	var eventHistorys []*types.EventHistory
	ctx := sdk.UnwrapSDKContext(c)

	store := ctx.KVStore(k.storeKey)
	eventHistoryStore := prefix.NewStore(store, types.KeyPrefix(types.EventHistoryKey))

	pageRes, err := query.Paginate(eventHistoryStore, req.Pagination, func(key []byte, value []byte) error {
		var eventHistory types.EventHistory
		if err := k.cdc.Unmarshal(value, &eventHistory); err != nil {
			return err
		}

		eventHistorys = append(eventHistorys, &eventHistory)
		return nil
	})

	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &types.QueryAllEventHistoryResponse{EventHistory: eventHistorys, Pagination: pageRes}, nil
}

// ImportEventHistory return the event history of a token
func (k Keeper) ImportEventHistory(c context.Context, req *types.QueryGetEventHistoryRequest) (*types.QueryGetEventHistoryResponse, error) {
	if req == nil {
		return nil, status.Error(codes.InvalidArgument, "invalid request")
	}
	ctx := sdk.UnwrapSDKContext(c)

	val, found := k.GetEventHistory(ctx, req.Index)
	if !found {
		return nil, status.Error(codes.InvalidArgument, "not found")
	}

	return &types.QueryGetEventHistoryResponse{EventHistory: &val}, nil
}

//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"fmt"
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/stretchr/testify/assert"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/importtoken/types"
)

func createNImportMapping(keeper *Keeper, ctx sdk.Context, n int) []types.ImportMapping {
	items := make([]types.ImportMapping, n)
	for i := range items {
		items[i].Creator = "any"
		items[i].Index = fmt.Sprintf("%d", i)
		keeper.SetImportMapping(ctx, items[i])
	}
	return items
}

func TestImportMappingGet(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	items := createNImportMapping(keeper, ctx, 10)
	for _, item := range items {
		rst, found := keeper.GetImportMapping(ctx, item.Index)
		assert.True(t, found)
		assert.Equal(t, item, rst)
	}
}
func TestImportMappingRemove(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	items := createNImportMapping(keeper, ctx, 10)
	for _, item := range items {
		keeper.RemoveImportMapping(ctx, item.Index)
		_, found := keeper.GetImportMapping(ctx, item.Index)
		assert.False(t, found)
	}
}

func TestImportMappingGetAll(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	items := createNImportMapping(keeper, ctx, 10)
	assert.Equal(t, items, keeper.GetAllImportMapping(ctx))
}

//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/importtoken/types"
	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"
)

// SetImportMapping set a specific ImportMapping in the store from its index
func (k Keeper) SetImportMapping(ctx sdk.Context, importMapping types.ImportMapping) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.ImportMappingKey))
	b := k.cdc.MustMarshal(&importMapping)
	store.Set(types.KeyPrefix(importMapping.Index), b)
}

// GetImportMapping returns a ImportMapping from its index
func (k Keeper) GetImportMapping(ctx sdk.Context, index string) (val types.ImportMapping, found bool) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.ImportMappingKey))

	b := store.Get(types.KeyPrefix(index))
	if b == nil {
		return val, false
	}

	k.cdc.MustUnmarshal(b, &val)
	return val, true
}

// RemoveImportMapping removes a ImportMapping from the store
func (k Keeper) RemoveImportMapping(ctx sdk.Context, index string) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.ImportMappingKey))
	store.Delete(types.KeyPrefix(index))
}

// GetAllImportMapping returns all ImportMappings
func (k Keeper) GetAllImportMapping(ctx sdk.Context) (list []types.ImportMapping) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.ImportMappingKey))
	iterator := sdk.KVStorePrefixIterator(store, []byte{})

	defer iterator.Close()

	for ; iterator.Valid(); iterator.Next() {
		var val types.ImportMapping
		k.cdc.MustUnmarshal(iterator.Value(), &val)
		list = append(list, val)
	}

	return
}

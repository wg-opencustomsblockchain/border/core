//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/importtoken/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
)

// CreateEvent adds an event to a token's history
func (k Keeper) CreateImportEvent(goCtx context.Context, msg *types.MsgCreateEvent) (*types.MsgCreateEventResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Check if the value already exists
	eventHistory, historyExists := k.GetEventHistory(ctx, msg.Index)
	if !historyExists {
		eventHistory = types.EventHistory{
			Creator: msg.Creator,
			Index:   msg.Index,
			Events:  []*types.ImportEvent{},
		}
	}

	eventHistory.Events = append(eventHistory.Events, &types.ImportEvent{
		Creator:   msg.Creator,
		Status:    msg.Status,
		Message:   msg.Message,
		EventType: msg.EventType,
		Timestamp: GetTimestamp(),
	})

	k.SetEventHistory(
		ctx,
		eventHistory,
	)
	return &types.MsgCreateEventResponse{}, nil
}

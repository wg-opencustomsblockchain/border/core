//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"testing"

	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/importtoken/types"
)

func TestImportTokenMsgServerCreate(t *testing.T) {
	srv, ctx := setupMsgServer(t)
	creator := "A"
	for i := 0; i < 5; i++ {
		resp, err := srv.CreateImportToken(ctx, &types.MsgCreateImportToken{
			Creator:             creator,
			Id:                  "1",
			Consignor:           &types.Company{},
			Exporter:            &types.Company{},
			Consignee:           &types.Company{},
			CustomOfficeOfExit:  &types.CustomsOffice{},
			CustomOfficeOfEntry: &types.CustomsOffice{},
			TransportAtBorder:   &types.Transport{},
			TransportFromBorder: &types.Transport{},
		})
		require.NoError(t, err)
		assert.NotNil(t, resp.ImportToken.Id)
	}
}

func TestImportTokenMsgServerUpdate(t *testing.T) {
	creator := "A"

	for _, tc := range []struct {
		desc    string
		request *types.MsgUpdateImportToken
		err     error
	}{
		{
			desc:    "Completed",
			request: &types.MsgUpdateImportToken{Creator: creator},
		},
		{
			desc:    "KeyNotFound",
			request: &types.MsgUpdateImportToken{Creator: creator, Id: "10"},
			err:     sdkerrors.ErrKeyNotFound,
		},
	} {
		tc := tc
		t.Run(tc.desc, func(t *testing.T) {
			srv, ctx := setupMsgServer(t)

			response, err := srv.CreateImportToken(ctx, &types.MsgCreateImportToken{
				Creator:             creator,
				Id:                  "1",
				Consignor:           &types.Company{},
				Exporter:            &types.Company{},
				Consignee:           &types.Company{},
				CustomOfficeOfExit:  &types.CustomsOffice{},
				CustomOfficeOfEntry: &types.CustomsOffice{},
				TransportAtBorder:   &types.Transport{},
				TransportFromBorder: &types.Transport{},
			})

			require.NoError(t, err)

			if tc.request.Id == "" {
				tc.request.Id = response.ImportToken.Id
			}

			_, err = srv.UpdateImportToken(ctx, tc.request)
			if tc.err != nil {
				require.ErrorIs(t, err, tc.err)
			} else {
				require.NoError(t, err)
			}
		})
	}
}

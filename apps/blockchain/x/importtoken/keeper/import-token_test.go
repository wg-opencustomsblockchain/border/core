//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"fmt"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/importtoken/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/google/uuid"
)

func createNImportToken(keeper *Keeper, ctx sdk.Context, n int) []types.ImportToken {
	items := make([]types.ImportToken, n)
	for i := range items {
		items[i].Creator = "any"
		items[i].ImportId = fmt.Sprintf("%d", i)
		items[i].Id = uuid.New().String()

		keeper.SetImportToken(ctx, items[i])
		keeper.SetImportMapping(ctx, types.ImportMapping{
			Index:   items[i].ImportId,
			Tokenid: items[i].Id,
		})
	}
	return items
}

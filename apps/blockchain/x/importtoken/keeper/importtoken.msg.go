//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"
	"fmt"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/importtoken/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
	"github.com/google/uuid"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// CreateImportToken creates a new ImportToken filled with data from the msg parameter. It returns the newly created token including the token's history.
// If the token includes a ImportId, a ImportMapping is also being created.
func (k Keeper) CreateImportToken(goCtx context.Context, msg *types.MsgCreateImportToken) (*types.MsgCreateImportTokenResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	var importToken = parseCreateMsg(msg)

	if importToken.Id == "" {
		return nil, status.Error(codes.InvalidArgument, "Please provide an import with a MRN.")
	}

	k.CreateNewToken(goCtx, importToken)

	if msg.ImportId != "" {
		k.SetImportMapping(ctx, types.ImportMapping{
			Creator: msg.Creator,
			Index:   msg.ImportId,
			Tokenid: importToken.Id,
		})
	}

	k.CreateImportEvent(goCtx, &types.MsgCreateEvent{
		Creator:   msg.Creator,
		Index:     importToken.Id,
		Status:    "1",
		Message:   "Created",
		EventType: "IMPORT_ACCEPTED",
	})

	history, _ := k.GetEventHistory(ctx, importToken.Id)

	return &types.MsgCreateImportTokenResponse{
		ImportToken: &importToken,
		Events:      history.Events,
	}, nil
}

// UpdateImportToken updates an existing ImportToken with data from the msg parameter. It returns the updated token including the new token's history.
// If the token's ImportId has been changed, ImportMappings are being updated as well.
func (k Keeper) UpdateImportToken(goCtx context.Context, msg *types.MsgUpdateImportToken) (*types.MsgUpdateImportTokenResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	var importToken = parseUpdateMsg(msg)

	// Checks that the element exists
	if !k.HasImportToken(goCtx, msg.Id) {
		return nil, sdkerrors.Wrap(sdkerrors.ErrKeyNotFound, fmt.Sprintf("key %s doesn't exist", msg.Id))
	}

	oldToken := k.GetImportToken(goCtx, msg.Id)

	k.SetImportToken(goCtx, importToken)

	if oldToken.ImportId != importToken.ImportId {
		k.RemoveImportMapping(ctx, oldToken.ImportId)

		if importToken.ImportId != "" {
			k.SetImportMapping(ctx, types.ImportMapping{
				Creator: msg.Creator,
				Index:   msg.ImportId,
				Tokenid: importToken.Id,
			})
		}
	}

	k.CreateImportEvent(goCtx, &types.MsgCreateEvent{
		Creator:   msg.Creator,
		Index:     importToken.Id,
		Status:    msg.Status,
		Message:   "Updated",
		EventType: "IMPORT_SUPPLEMENT",
	})

	history, _ := k.GetEventHistory(ctx, importToken.Id)

	return &types.MsgUpdateImportTokenResponse{
		ImportToken: &importToken,
		Events:      history.Events,
	}, nil
}

func parseCreateMsg(msg *types.MsgCreateImportToken) types.ImportToken {

	var importToken = types.ImportToken{
		Creator:   msg.Creator,
		Id:        uuid.New().String(),
		Consignor: msg.Consignor,
		Exporter:  msg.Exporter,
		Consignee: msg.Consignee,
		Declarant: msg.Declarant,

		CustomOfficeOfExit:   msg.CustomOfficeOfExit,
		CustomOfficeOfEntry:  msg.CustomOfficeOfEntry,
		CustomOfficeOfImport: msg.CustomOfficeOfImport,
		GoodsItems:           msg.GoodsItems,
		TransportAtBorder:    msg.TransportAtBorder,
		TransportFromBorder:  msg.TransportFromBorder,

		ImportId:                   msg.ImportId,
		UniqueConsignmentReference: msg.UniqueConsignmentReference,
		LocalReferenceNumber:       msg.LocalReferenceNumber,
		DestinationCountry:         msg.DestinationCountry,
		ExportCountry:              msg.ExportCountry,
		Itinerary:                  msg.Itinerary,
		IncotermCode:               msg.IncotermCode,
		IncotermLocation:           msg.IncotermLocation,
		TotalGrossMass:             msg.TotalGrossMass,
		GoodsItemQuantity:          msg.GoodsItemQuantity,
		TotalPackagesQuantity:      msg.TotalPackagesQuantity,
		ReleaseDateAndTime:         msg.ReleaseDateAndTime,
		NatureOfTransaction:        msg.NatureOfTransaction,
		TotalAmountInvoiced:        msg.TotalAmountInvoiced,
		InvoiceCurrency:            msg.InvoiceCurrency,

		RelatedExportToken: msg.RelatedExportToken,
	}

	return importToken
}

func parseUpdateMsg(msg *types.MsgUpdateImportToken) types.ImportToken {

	var importToken = types.ImportToken{
		Creator:   msg.Creator,
		Id:        msg.Id,
		Consignor: msg.Consignor,
		Exporter:  msg.Exporter,
		Consignee: msg.Consignee,
		Declarant: msg.Declarant,

		CustomOfficeOfExit:   msg.CustomOfficeOfExit,
		CustomOfficeOfEntry:  msg.CustomOfficeOfEntry,
		CustomOfficeOfImport: msg.CustomOfficeOfImport,
		GoodsItems:           msg.GoodsItems,
		TransportAtBorder:    msg.TransportAtBorder,
		TransportFromBorder:  msg.TransportFromBorder,

		ImportId:                   msg.ImportId,
		UniqueConsignmentReference: msg.UniqueConsignmentReference,
		LocalReferenceNumber:       msg.LocalReferenceNumber,
		DestinationCountry:         msg.DestinationCountry,
		ExportCountry:              msg.ExportCountry,
		Itinerary:                  msg.Itinerary,
		IncotermCode:               msg.IncotermCode,
		IncotermLocation:           msg.IncotermLocation,
		TotalGrossMass:             msg.TotalGrossMass,
		GoodsItemQuantity:          msg.GoodsItemQuantity,
		TotalPackagesQuantity:      msg.TotalPackagesQuantity,
		ReleaseDateAndTime:         msg.ReleaseDateAndTime,
		NatureOfTransaction:        msg.NatureOfTransaction,
		TotalAmountInvoiced:        msg.TotalAmountInvoiced,
		InvoiceCurrency:            msg.InvoiceCurrency,

		RelatedExportToken: msg.RelatedExportToken,
	}

	return importToken
}

func (k Keeper) FetchImportToken(goCtx context.Context, msg *types.MsgFetchImportToken) (*types.MsgFetchImportTokenResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)
	token := k.GetImportToken(goCtx, msg.Id)
	history, historyFound := k.GetEventHistory(ctx, msg.Id)
	if !historyFound {
		history = types.EventHistory{}
	}

	return &types.MsgFetchImportTokenResponse{
		ImportToken: &token,
		Events:      history.Events,
	}, nil
}

func (k Keeper) FetchImportMapping(goCtx context.Context, msg *types.MsgFetchImportMapping) (*types.MsgFetchImportMappingResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)
	mapping, found := k.GetImportMapping(ctx, msg.Id)
	if !found {
		return nil, status.Error(codes.InvalidArgument, "not found")
	}

	token := k.GetImportToken(goCtx, mapping.Tokenid)
	history, historyFound := k.GetEventHistory(ctx, mapping.Tokenid)
	if !historyFound {
		history = types.EventHistory{}
	}

	return &types.MsgFetchImportMappingResponse{
		ImportToken: &token,
		Events:      history.Events,
	}, nil
}

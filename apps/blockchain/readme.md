# Token Manager

**Token Manager** is a blockchain built using Cosmos SDK and Tendermint and created with [Starport](https://github.com/tendermint/starport).

## Cloning and updating of the repository

The Token Manager uses the modules [Wallet](https://gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/token-manager/token-wallet), [Token](https://gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/token-manager/token), [BusinessLogic](https://gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/token-manager/business-logic) and [HashToken](https://gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/token-manager/hash-token), which are included as submodules. To clone all repositories at once, you can use the command `git clone --recursive https://gitlab.cc-asp.fraunhofer.de/silicon-economy/base/blockchainbroker/token-manager/token-management.git`. To update all repositories at one, you can use `git pull --recurse-submodules`, to update the submodules use `git submodule update --init --recursive`.

## Get started

```
starport chain serve
```

`serve` command installs dependencies, builds, initializes and starts your blockchain in development.

## Configure

Your blockchain in development can be configured with `config.yml`. To learn more see the [reference](https://github.com/tendermint/starport#documentation).

## Launch

To launch your blockchain live on mutliple nodes use `starport network` commands. Learn more about [Starport Network](https://github.com/tendermint/spn).

## CLI commands
The following commands are supported by the token manager module

### Transactions
All business logic module transaction commands need a preceding `TokenManagerd tx BusinessLogic `

* `create-token [tokenType] [changeMessage] [segmentId]` to create a token and assign it to a segment
* `activate-token [tokenId] [segmentId]`  to activate a token and assign it to a segment
* `deactivate-token [tokenId]` to deactivate a token. This will also remove it from the containing segment
* `set-token-valid-status [tokenId]` to set a token valid
* `move-token-to-wallet [tokenRefId] [sourceSegmentId] [targetWalletId]` to move a token to another wallet

All token wallet module transaction commands need a preceding `TokenManagerd tx Wallet`

* `create-wallet [name]` to create a new wallet
* `update-wallet [id] [name]` to change a wallet's name
* `create-walletAccount [cosmosAddress] [walletId]` to assign a cosmos-account to a wallet
* `create-segment [name] [info] [walletId]` to create a new segment in a wallet
* `update-segment [id] [name] [info]` to change a segment's name and/or info
* `create-tokenRef [id] [moduleRef] [segmentId]` to create a new reference for a token in a segment
* `move-token-to-segment [tokenRefId] [sourceSegmentId] [targetSegmentId]` to move a token to another segment

All token module transaction commands need a preceding `TokenManagerd tx Token`

* `create-token [id] [tokenType] [changeMessage]` to create a new token
* `update-token [id] [tokenType] [changeMessage]` to change a token type, including a change message
* `activate-token [id]` to set a token's valid flag to active
* `deactivate-token [id]` to set a token's valid flag to inactive
* `update-token-information [tokenId] [data]` to update a token's data

### Queries
All business logic module queries commands need a preceding `TokenManagerd query BusinessLogic`
* `list-token-by-segmentId [segmentId]` to list all tokens from a specified segment
* `list-token-by-walletId [walletId]` to list all tokens from a specified wallet

All token wallet module queries commands need a preceding `TokenManagerd query Wallet`
* `list-wallet` to list all wallets
* `show-wallet [id]` to view details of a wallet
* `list-walletHistory` to view all wallets' histories
* `show-walletHistory [id]` to view the history of a specific wallet
* `list-segment` to list all segments
* `show-segment [id]` to view details of a segment
* `list-segmentHistory` to view all segments' histories
* `show-segmentHistory [id]` to view a specific segment's history
* `list-tokenHistoryGlobal` to lookup all tokens and a list of wallets they have been contained by
* `show-tokenHistoryGlobal [id]` to lookup all wallets a specific token has been contained by

All token module queries commands need a preceding `TokenManagerd query Token`
* `list-token` to list all tokens
* `show-token [id]` to view a specific token
* `list-tokenHistory` to view all token's histories
* `show-tokenHistory [id]` to view a specific token's history

## Learn more

- [Starport](https://github.com/tendermint/starport)
- [Cosmos SDK documentation](https://docs.cosmos.network)
- [Cosmos SDK Tutorials](https://tutorials.cosmos.network)
- [Discord](https://discord.gg/W8trcGV)

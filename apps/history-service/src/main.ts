/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Logger, LogLevel } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';

import { AppModule } from './app/app.module';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { AmqpClientEnum } from '@border/api-interfaces';

async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(
    AppModule,
    {
      transport: Transport.RMQ,
      options: {
        urls: [process.env.MSG_QUEUE_URL],
        queue: AmqpClientEnum.AMQP_QUEUE_HISTORY,
        queueOptions: {
          durable: false,
        },
      },
      logger:
        process.env.LOG_LEVEL === 'dev'
          ? (process.env.LOG_SETTINGS_DEV.split(',') as LogLevel[])
          : (process.env.LOG_SETTINGS_PROD.split(',') as LogLevel[]),
    }
  );

  const logger = new Logger('HISTORY-SERVICE');
  app
    .listen()
    .then(() =>
      logger.log(
        `🚀 - History Service started with RMQ: ${process.env.MSG_QUEUE_URL}`
      )
    );
}

bootstrap();

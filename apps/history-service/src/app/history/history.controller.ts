/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Controller, Logger } from '@nestjs/common';
import { HistoryService } from './history.service';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { HistoryMqPattern } from '@border/api-interfaces/lib/patterns/history.mqpattern';

import { FieldInformationDto } from '@border/api-interfaces/lib/entities/history/history.dto';
import { User } from '@border/api-interfaces';
import { ExportTokenDTO } from '@border/api-interfaces/lib/dto/export.dto';
import { ImportToken } from '@border/api-interfaces/lib/client/org.borderblockchain.businesslogic/types/importtoken/importtoken';

@Controller()
export class HistoryController {
  private logger: Logger = new Logger(HistoryController.name);

  constructor(private readonly historyService: HistoryService) {}

  @MessagePattern(HistoryMqPattern.PREFIX + HistoryMqPattern.GENERATE_HISTORY)
  public async generateHistory(
    @Payload() payload: { token: ExportTokenDTO }
  ): Promise<void> {
    await this.historyService.generateHistory(payload.token);
  }

  @MessagePattern(
    HistoryMqPattern.PREFIX + HistoryMqPattern.GENERATE_HISTORY_IMPORT
  )
  public async generateHistoryImport(
    @Payload() payload: { token: ImportToken }
  ): Promise<void> {
    await this.historyService.generateHistory<ImportToken>(payload.token);
  }

  @MessagePattern(
    HistoryMqPattern.PREFIX + HistoryMqPattern.CREATE_HISTORY_IMPORT
  )
  public async createHistoryImport(
    @Payload() payload: { token: ImportToken }
  ): Promise<void> {
    await this.historyService.createHistoryImport(payload.token);
  }

  @MessagePattern(HistoryMqPattern.PREFIX + HistoryMqPattern.GET_HISTORY)
  public getHistory(
    @Payload() payload: { id: string; user: User }
  ): Promise<FieldInformationDto[]> {
    return this.historyService.getTokenHistory(payload.id, payload.user);
  }
}

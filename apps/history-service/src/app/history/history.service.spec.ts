/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { User } from '@border/api-interfaces';
import { MQBroker } from '@border/broker';
import { DynamicModule } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { HistoryService } from './history.service';
import { PrismaService } from './prisma.service';
import { prismaMock } from './test/singleton';

describe('HistoryService', () => {
  let service: HistoryService;
  const mqBroker: DynamicModule = new MQBroker().getMsgBroker();

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [mqBroker],
      providers: [HistoryService, PrismaService],
    }).compile();

    service = module.get<HistoryService>(HistoryService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should return the correct string representation of an object', () => {
    const obj = {
      name: 'test',
      obj1: {
        name: 'test1',
        objInObj: {
          name: 'test objInObj',
        },
      },
      array1: [
        {
          name: 'array test 1',
        },
      ],
    };

    expect(service.generateTokenStringRepresentation(obj)).toEqual([
      'name',
      'obj1.name',
      'obj1.objInObj.name',
      'array1.0.name',
    ]);
  });
});

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Inject, Injectable, Logger } from '@nestjs/common';

import {
  AmqpClientEnum,
  Company,
  CompanyAMQPatterns,
  User,
  UserMsgPatterns,
} from '@border/api-interfaces';
import { MsgFetchTokenHistoryResponse } from '@border/api-interfaces/lib/client/org.borderblockchain.businesslogic/types/businesslogic/tx';
import { ImportToken } from '@border/api-interfaces/lib/client/org.borderblockchain.businesslogic/types/importtoken/importtoken';
import { FieldInformationDto } from '@border/api-interfaces/lib/entities/history/history.dto';
import { DirectSecp256k1HdWallet } from '@cosmjs/proto-signing';
import { ClientProxy } from '@nestjs/microservices';
import { firstValueFrom } from 'rxjs';
import { GetTokenHistoryDto } from './models/dto';
import { PrismaService } from './prisma.service';
import { FieldInformation, History, PCompany, Prisma } from '@prisma/client';

@Injectable()
export class HistoryService {
  private logger: Logger = new Logger(HistoryService.name);
  private loadingList: Map<string, boolean> = new Map<string, boolean>();

  constructor(
    @Inject(AmqpClientEnum.AMQP_CLIENT_BLOCKCHAIN)
    private blockchainService: ClientProxy,
    @Inject(AmqpClientEnum.AMQP_CLIENT_AUTH) private authService: ClientProxy,
    @Inject(AmqpClientEnum.AMQP_CLIENT_COMPANY)
    private companyService: ClientProxy,
    private prisma: PrismaService
  ) {}

  /**
   * Returns the history of all fields from a token
   * @param id Token ID
   * @param user Executing user
   */
  async getTokenHistory(
    id: string,
    user: User
  ): Promise<FieldInformationDto[]> {
    if (this.loadingList.has(id) && this.loadingList.get(id)) {
      await new Promise((resolve) => setTimeout(resolve, 2000));
      return this.getTokenHistory(id, user);
    }

    const history: History & {
      fieldInformation: (FieldInformation & {
        editorCompany: PCompany;
      })[];
    } = await this.prisma.history.findFirst({
      where: {
        tokenId: id,
      },
      include: {
        fieldInformation: {
          include: {
            editorCompany: true,
          },
        },
      },
    });

    if (!history) return [];

    const userCosmosAddress = await this.getCosmosAddress(user.mnemonic);
    const userCompany = await this.getCompanyFromAddress(userCosmosAddress);

    const fieldInformationDto: FieldInformationDto[] = [];

    history.fieldInformation.forEach((element) => {
      const info: FieldInformationDto = {
        fieldName: element.fieldName,
        isEdited: element.isEdited,
        lastEditor:
          userCompany.identificationNumber === element.editorCompany.customsId
            ? element.editorName
            : element.editorCompany.name,
        level:
          element.creator === element.editor
            ? 'secure'
            : userCompany.identificationNumber ===
              element.editorCompany.customsId
            ? 'company'
            : 'external',
      };
      fieldInformationDto.push(info);
    });

    return fieldInformationDto;
  }

  /**
   * Create the first history for a token
   * @param token The token
   */
  async createHistoryImport(token: ImportToken) {
    this.loadingList.set(token.id, true);
    const exportHistory = await this.prisma.history.findFirst({
      where: {
        tokenId: token.relatedExportToken,
      },
      include: {
        fieldInformation: {
          include: {
            editorCompany: true,
          },
        },
      },
    });

    const history = await this.createHistory({
      tokenId: token.id,
    });

    const stringRepresentation = this.generateTokenStringRepresentation(token);

    const user = await this.getUserFromAddress(token.creator);
    const company = await this.getCompanyFromAddress(token.creator);

    if (!exportHistory)
      for (const s of stringRepresentation) {
        await this.prisma.fieldInformation.create({
          data: {
            creator: token.creator,
            fieldName: s,
            editorCompany: {
              connectOrCreate: {
                create: {
                  name: company.name,
                  customsId: company.identificationNumber,
                },
                where: {
                  customsId: company.identificationNumber,
                },
              },
            },
            editor: token.creator,
            editorName: `${user.firstName} ${user.lastName}`,
            isEdited: false,
            history: {
              connect: {
                id: history.id,
              },
            },
          },
        });
      }
    else
      for (const s of stringRepresentation) {
        const info = exportHistory.fieldInformation.find(
          (fi) => fi.fieldName === s
        );

        info
          ? await this.prisma.fieldInformation.create({
              data: {
                creator: info.creator,
                fieldName: s,
                isEdited: info.isEdited,
                editor: info.editor,
                editorName: info.editorName,
                editorCompany: {
                  connectOrCreate: {
                    where: {
                      customsId: info.editorCompany.customsId,
                    },
                    create: {
                      customsId: info.editorCompany.customsId,
                      name: info.editorCompany.name,
                    },
                  },
                },
                history: {
                  connect: {
                    id: history.id,
                  },
                },
              },
            })
          : await this.prisma.fieldInformation.create({
              data: {
                creator: token.creator,
                fieldName: s,
                editorCompany: {
                  connectOrCreate: {
                    where: {
                      customsId: company.identificationNumber,
                    },
                    create: {
                      customsId: company.identificationNumber,
                      name: company.name,
                    },
                  },
                },
                editor: token.creator,
                editorName: `${user.firstName} ${user.lastName}`,
                isEdited: false,
                history: {
                  connect: {
                    id: history.id,
                  },
                },
              },
            });
      }

    this.loadingList.set(token.id, false);
  }

  /**
   * Generate the history for a token
   * @param token
   */
  async generateHistory<T>(token: T): Promise<void> {
    this.logger.log('Generating history for token: ', token['id']);
    this.loadingList.set(token['id'], true);
    let history = await this.prisma.history.findFirst({
      where: {
        tokenId: token['id'],
      },
      include: {
        fieldInformation: {
          include: {
            editorCompany: true,
          },
        },
      },
    });

    const dto: GetTokenHistoryDto = {
      id: token['id'],
      mnemonic: process.env.WALLET_ADDRESS_MNEMONIC,
    };
    if (!history)
      history = await this.createHistory({
        tokenId: token['id'],
      });

    const tokenHistory: MsgFetchTokenHistoryResponse = await firstValueFrom(
      this.blockchainService.send('v2/blockchain/history', dto)
    );

    this.logger.log(tokenHistory.TokenHistory.history[1]);

    if (
      !tokenHistory.TokenHistory ||
      tokenHistory.TokenHistory.history.length <= 1
    ) {
      this.loadingList.set(token['id'], false);
      return;
    }
    const originalToken: T = JSON.parse(
      tokenHistory.TokenHistory.history[1].info.data
    );

    const stringRepresentation =
      this.generateTokenStringRepresentation(originalToken);

    const lastEditorUser = await this.getUserFromAddress(token['creator']);
    const lastEditorCompany = await this.getCompanyFromAddress(
      token['creator']
    );
    const originalUser = await this.getUserFromAddress(
      originalToken['creator']
    );
    const originalCompany = await this.getCompanyFromAddress(
      originalToken['creator']
    );

    this.compareTokens<T>(
      stringRepresentation,
      originalToken,
      token,
      history,
      lastEditorUser,
      originalUser,
      lastEditorCompany,
      originalCompany
    );

    this.loadingList.set(token['id'], false);
  }

  /**
   * Create a DB history object
   * @param history
   */
  async createHistory(history: Prisma.HistoryCreateInput): Promise<
    History & {
      fieldInformation: (FieldInformation & { editorCompany: PCompany })[];
    }
  > {
    const model = await this.prisma.history.create({
      data: {
        ...history,
      },
      include: {
        fieldInformation: {
          include: {
            editorCompany: true,
          },
        },
      },
    });
    return model;
  }

  /**
   * Compare the tokens
   * @param stringRepresentation
   * @param originalToken
   * @param token
   * @param history
   * @param lastEditorUser
   * @param originalUser
   * @param lastEditorCompany
   * @param originalCompany
   * @private
   */
  private async compareTokens<T>(
    stringRepresentation: string[],
    originalToken: T,
    token: T,
    history: History & {
      fieldInformation: (FieldInformation & { editorCompany: PCompany })[];
    },
    lastEditorUser: User,
    originalUser: User,
    lastEditorCompany: Company,
    originalCompany: Company
  ) {
    for (const o of stringRepresentation) {
      //this.logger.debug(o);
      const splitted = o.split('.');

      let fieldValueOriginal = originalToken;
      let fieldValueEdited = token;

      splitted.forEach((f) => {
        if (fieldValueEdited && fieldValueOriginal) {
          fieldValueOriginal = fieldValueOriginal[f]
            ? fieldValueOriginal[f]
            : null;
          fieldValueEdited = fieldValueEdited[f] ? fieldValueEdited[f] : null;
        }
      });

      const currentHistory = history.fieldInformation.findIndex(
        (fi) => fi.fieldName === o
      );

      if (currentHistory === -1)
        await this.prisma.fieldInformation.create({
          data: {
            fieldName: o,
            isEdited: fieldValueOriginal !== fieldValueEdited,
            creator: originalToken['creator'],
            editor:
              fieldValueOriginal === fieldValueEdited
                ? originalToken['creator']
                : token['creator'],
            editorCompany: {
              connectOrCreate: {
                where: {
                  customsId:
                    fieldValueOriginal === fieldValueEdited
                      ? originalCompany.identificationNumber
                      : lastEditorCompany.identificationNumber,
                },
                create: {
                  customsId:
                    fieldValueOriginal === fieldValueEdited
                      ? originalCompany.identificationNumber
                      : lastEditorCompany.identificationNumber,
                  name:
                    fieldValueOriginal === fieldValueEdited
                      ? originalCompany.name
                      : lastEditorCompany.name,
                },
              },
            },
            editorName:
              fieldValueOriginal === fieldValueEdited
                ? `${originalUser.firstName} ${originalUser.lastName}`
                : `${lastEditorUser.firstName} ${lastEditorUser.lastName}`,
            history: {
              connect: {
                id: history.id,
              },
            },
          },
        });
      else if (fieldValueOriginal !== fieldValueEdited) {
        await this.prisma.fieldInformation.update({
          where: {
            id: history.fieldInformation[currentHistory].id,
          },
          data: {
            isEdited: true,
            editor: token['creator'],
            editorCompany: {
              connectOrCreate: {
                where: {
                  customsId: lastEditorCompany.identificationNumber,
                },
                create: {
                  customsId: lastEditorCompany.identificationNumber,
                  name: lastEditorCompany.name,
                },
              },
            },
            editorName: `${lastEditorUser.firstName} ${lastEditorUser.lastName}`,
          },
        });
      }
    }
  }

  /**
   * Generate a string representation for all token fields (eg. exporter.address.city)
   * @param token
   * @private
   */
  public generateTokenStringRepresentation<T>(token: T) {
    const fields = Object.keys(token);
    let fieldArray: string[] = [];
    for (const field of fields) {
      fieldArray = fieldArray.concat(
        this.getStringRepresentationOfObject(token, field)
      );
    }
    return fieldArray;
  }

  private getStringRepresentationOfObject<Type>(
    object: Type,
    currentFieldName: string
  ) {
    const array = [];
    if (object[currentFieldName] instanceof Object) {
      const fields = Object.keys(object[currentFieldName]);
      for (const field of fields) {
        const temp = this.getStringRepresentationOfObject(
          object[currentFieldName],
          field
        );
        temp.forEach((e) => array.push(`${currentFieldName}.${e}`));
      }
      return array;
    }
    return [currentFieldName];
  }

  private getUserFromAddress(address: string): Promise<User> {
    return firstValueFrom(
      this.authService.send(
        UserMsgPatterns.API_TAG +
          UserMsgPatterns.API_VERSION +
          UserMsgPatterns.GET_ONE_BY_COSMOS,
        address
      )
    );
  }

  private async getCompanyFromAddress(address: string): Promise<Company> {
    return JSON.parse(
      await firstValueFrom(
        this.companyService.send(
          CompanyAMQPatterns.API_TAG +
            CompanyAMQPatterns.API_VERSION +
            CompanyAMQPatterns.GET_ONE_BY_COSMOS,
          address
        )
      )
    );
  }

  private async getCosmosAddress(mnemonic: string): Promise<string> {
    const [creatorAddress] = await (
      await DirectSecp256k1HdWallet.fromMnemonic(mnemonic)
    ).getAccounts();

    return creatorAddress.address;
  }
}

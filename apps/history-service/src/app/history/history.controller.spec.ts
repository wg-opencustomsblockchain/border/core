/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { User } from '@border/api-interfaces';
import { ExportTokenDTO } from '@border/api-interfaces/lib/dto/export.dto';
import { FieldInformationDto } from '@border/api-interfaces/lib/entities/history/history.dto';
import { Test, TestingModule } from '@nestjs/testing';
import { HistoryController } from './history.controller';
import { HistoryService } from './history.service';

describe('HistoryController', () => {
  let controller: HistoryController;
  let historyService: HistoryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [HistoryController],
      providers: [
        {
          provide: HistoryService,
          useValue: {
            generateHistory(token: ExportTokenDTO): Promise<void> {
              return Promise.resolve();
            },
            getTokenHistory(
              id: string,
              user: User
            ): Promise<FieldInformationDto[]> {
              return Promise.resolve([]);
            },
          },
        },
      ],
    }).compile();

    controller = module.get<HistoryController>(HistoryController);
    historyService = module.get<HistoryService>(HistoryService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should generate a history', () => {
    const spy = jest.spyOn(historyService, 'generateHistory');
    controller.generateHistory({ token: {} as ExportTokenDTO });
    expect(spy).toBeCalled();
  });

  it('should return the history', async () => {
    expect(await controller.getHistory({ id: '', user: {} })).toEqual([]);
  });
});

/**
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */

(function (window) {
  window['env'] = window['env'] || {};

  // Log Settings

  window['env']['LOG_LEVEL'] = 'dev';
  //window["env"]["LOG_LEVEL"] = "prod";
  window['env']['LOG_SETTINGS_DEV'] = 'log,error,warn,debug,verbose';
  window['env']['LOG_SETTINGS_PROD'] = 'error,warn';

  // Environment Variables
  window['env']['USER_API_URL'] = 'http://localhost:8082/';
  window['env']['USER_API_AUTH_ENDPOINT'] = 'auth/';
  window['env']['USER_API_USER_ENDPOINT'] = 'users/';
  window['env']['EAD_API_URL'] = 'http://localhost:8081/';
  window['env']['EAD_API_ENDPOINT'] = 'border/v1';
  window['env']['blockchainTypeUrl'] = '/org.borderblockchain.businesslogic';
  window['env']['TENDERMINT_CHAIN_ENDPOINT_RPC'] = 'http://localhost:26657/';
  window['env']['TENDERMINT_CHAIN_ENDPOINT_NODE'] = 'http://localhost:1317/';
  window['env']['KEYCLOAK_URL'] = 'https://keycloak.public.apps.blockchain-europe.iml.fraunhofer.de';
  window['env']['KEYCLOAK_REALM'] = 'BORDER';
  window['env']['KEYCLOAK_CLIENT_ID_FRONTEND'] = 'frontend';
})(this);

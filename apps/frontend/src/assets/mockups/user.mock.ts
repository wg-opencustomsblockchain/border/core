/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { User } from '@border/api-interfaces';

export const MOCK_USER: User[] = [
  {
    attributes: {
      mnemonic: [
        'cabbage dynamic purse danger stairs bike solid bridge mushroom face winter fantasy virtual before recycle weasel emotion royal equip story undo poet tonight hub',
      ],
      pubkey: [''],
      wallet: ['123'],
      tokenWalletId: [''],
      company: [
        {
          name: 'Erik Export GmbH',
          address: {
            streetAndNumber: '',
            postcode: '',
            countryCode: '',
            city: '',
          },
          subsidiaryNumber: 123,
        },
      ],
      role: ['1'],
    },
    id: '1',
    username: 'Freddy',
  },
  {
    attributes: {
      mnemonic: [''],
      pubkey: [''],
      wallet: ['234'],
      tokenWalletId: [''],
      company: [
        {
          name: 'Erik Export GmbH',
          address: {
            streetAndNumber: '',
            postcode: '',
            countryCode: '',
            city: '',
          },
          subsidiaryNumber: 123,
        },
      ],
      role: ['2'],
    },
    id: '0',
    username: 'Erik',
  },
  {
    attributes: {
      mnemonic: [''],
      pubkey: [''],
      wallet: ['345'],
      tokenWalletId: [''],
      company: [
        {
          name: 'Erik Export GmbH',
          address: {
            streetAndNumber: '',
            postcode: '',
            countryCode: '',
            city: '',
          },
          subsidiaryNumber: 123,
        },
      ],
      role: ['3'],
    },
    id: '2',
    username: 'Ina',
  },
];

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ExportStatus } from '@base/src/app/core/_enums/status.enum';
import { TransportEvent } from '@base/src/app/core/_entities/event.entity';
import { MOCK_USER } from './user.mock';

export const MOCK_HISTORY: TransportEvent[] = [
  {
    creator: 'WALLET-0',
    status: ExportStatus.RECORDED,
    message: 'Created',
    eventType: 'IMPORT_ACCEPTED',
    timestamp: new Date(),
    user: MOCK_USER[0],
    creatorName: 'WALLET-0',
  },
  {
    creator: 'WALLET-1',
    status: ExportStatus.RECORDED,
    message: 'Created',
    eventType: 'IMPORT_ACCEPTED',
    timestamp: new Date(),
    user: MOCK_USER[1],
    creatorName: 'WALLET-1',
  },
  {
    creator: 'WALLET-2',
    status: ExportStatus.RECORDED,
    message: 'Created',
    eventType: 'IMPORT_ACCEPTED',
    timestamp: new Date(),
    user: MOCK_USER[2],
    creatorName: 'WALLET-2',
  },
];

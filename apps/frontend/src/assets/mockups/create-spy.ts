/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export function createSpyObj<T>(baseName: string | (keyof T)[], methodNames?: (keyof T)[]): jest.Mocked<T> {
  if (!methodNames) {
    methodNames = Array.isArray(baseName) ? baseName : [];
  }

  const obj: any = {};

  for (let i = 0; i < methodNames.length; i++) {
    obj[methodNames[i]] = jest.fn();
  }

  return obj;
}

export const createSpy = (baseName?) => {
  return jest.fn();
};

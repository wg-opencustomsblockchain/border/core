/**
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */

(function (window) {
  window['env'] = window['env'] || {};

  // Log Levels

  window['env']['LOG_LEVEL'] = '${LOG_LEVEL}';
  window['env']['LOG_SETTINGS_DEV'] = '${LOG_SETTINGS_DEV}';
  window['env']['LOG_SETTINGS_PROD'] = '${LOG_SETTINGS_PROD}';

  // Environment Variables
  window['env']['USER_API_URL'] = '${USER_API_URL}';
  window['env']['USER_API_AUTH_ENDPOINT'] = '${USER_API_AUTH_ENDPOINT}';
  window['env']['USER_API_USER_ENDPOINT'] = '${USER_API_USER_ENDPOINT}';
  window['env']['EAD_API_URL'] = '${EAD_API_URL}';
  window['env']['EAD_API_ENDPOINT'] = '${EAD_API_ENDPOINT}';
  window['env']['blockchainTypeUrl'] = '${blockchainTypeUrl}';
  window['env']['TENDERMINT_CHAIN_ENDPOINT_RPC'] = '${TENDERMINT_CHAIN_ENDPOINT_RPC}';
  window['env']['TENDERMINT_CHAIN_ENDPOINT_NODE'] = '${TENDERMINT_CHAIN_ENDPOINT_NODE}';
  window['env']['KEYCLOAK_URL'] = '${KEYCLOAK_URL}';
  window['env']['KEYCLOAK_REALM'] = '${KEYCLOAK_REALM}';
  window['env']['KEYCLOAK_CLIENT_ID_FRONTEND'] = '${KEYCLOAK_CLIENT_ID_FRONTEND}';
})(this);

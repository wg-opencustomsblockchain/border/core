/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import '@angular/localize/init';
import 'jest-preset-angular/setup-jest';
import * as process from 'process';
window['process'] = process;

// global is undefined
(window as any)['global'] = window;

// eslint-disable-next-line @typescript-eslint/no-var-requires
(window as any).global.Buffer = (window as any).global.Buffer || require('buffer').Buffer;

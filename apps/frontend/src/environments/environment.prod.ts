/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

declare const window: any;

/** Environments for production deployments. Includes all APIs addresses. */
export const environment = {
  //LOG_LEVEL: 'dev',
  LOG_LEVEL: 'prod',
  version: '0.3.8',
  production: true,
  USER_API_URL: window.env.USER_API_URL || 'http://localhost:8082/',
  USER_API_AUTH_ENDPOINT: window.env.USER_API_AUTH_ENDPOINT || 'auth/',
  USER_API_USER_ENDPOINT: window.env.USER_API_USER_ENDPOINT || 'users/',
  EAD_API_URL: window.env.EAD_API_URL || 'http://localhost:8081/',
  EAD_API_ENDPOINT: window.env.EAD_API_ENDPOINT || 'border/v1',
  blockchainTypeUrl: window.env.blockchainTypeUrl || '/org.borderblockchain.businesslogic',

  TENDERMINT_CHAIN_ENDPOINT_RPC: window.env.TENDERMINT_CHAIN_ENDPOINT_RPC || 'http://localhost:26657',
  TENDERMINT_CHAIN_ENDPOINT_NODE: window.env.TENDERMINT_CHAIN_ENDPOINT_NODE || 'http://localhost:1317',
  BLOCKCHAIN_EXPORT_TYPE_URL: '/org.borderblockchain.exporttoken',
  BLOCKCHAIN_IMPORT_TYPE_URL: '/org.borderblockchain.importtoken',
  BLOCKCHAIN_BUSINESS_TYPE_URL: '/org.borderblockchain.businesslogic',
  TENDERMINT_MAX_RETRIES: 50,
  TENDERMINT_MAX_GAS: '1800000000',
  TOKENWALLET_ID_CONNECTOR: 'DE537400371045831',
  BORDERBLOCKCHAIN_URL: 'org/borderblockchain',

  KEYCLOAK_URL: 'https://keycloak.public.apps.blockchain-europe.iml.fraunhofer.de',
  KEYCLOAK_REALM: 'BORDER',
  KEYCLOAK_CLIENT_ID_FRONTEND: 'frontend',
};

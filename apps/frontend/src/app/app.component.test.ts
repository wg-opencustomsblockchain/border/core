/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * @jest-environment jsdom
 */

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthenticationService } from '@core/services/authentication.service';
import { Company } from '@core/_entities';
import { UserRole } from '@core/_enums/user-role.enum';
import { User } from '@core/_interfaces/user.interface';
import { TranslateModule } from '@ngx-translate/core';
import { KeycloakAngularModule } from 'keycloak-angular';
import { of } from 'rxjs';
import { AppComponent } from './app.component';
import { LocalizationComponent } from './core/_components/localization/localization.component';
import { ToolbarComponent } from './core/_components/toolbar/toolbar.component';
import { SharedModule } from './shared/shared.module';

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;

  const user: User = {
    id: 0,
    company: new Company(),
    mnemonic: 'mnemonic',
    pubkey: 'pubkey',
    role: UserRole.DRIVER,
    username: 'test user',
    wallet: 'walletaddress',
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        TranslateModule.forRoot(),
        KeycloakAngularModule,
        SharedModule,
        NoopAnimationsModule,
      ],
      declarations: [AppComponent, ToolbarComponent, LocalizationComponent],
      providers: [
        {
          provide: AuthenticationService,
          useValue: {
            currentUser: of(user),
          },
        },
      ],
    }).compileComponents();
  });

  beforeAll(() => {
    Object.defineProperty(window, 'matchMedia', {
      writable: true,
      value: jest.fn().mockImplementation((query) => ({
        matches: false,
        media: query,
        onchange: null,
        addListener: jest.fn(), // Deprecated
        removeListener: jest.fn(), // Deprecated
        addEventListener: jest.fn(),
        removeEventListener: jest.fn(),
        dispatchEvent: jest.fn(),
      })),
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
  });

  it('should create the app', () => {
    expect(fixture).toBeTruthy();
  });
});

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { UserRole } from '../core/_enums/user-role.enum';

export const NextActions = [
  {
    icon: 'thumb_up',
    iconColor: 'primary',
    heading: 'next-actions.ACCEPT.heading',
    subheading: 'next-actions.ACCEPT.subheading',
    action: 'accept',
    displayWithStatus: 0,
    view: 'EXPORT',
    showWithRole: UserRole.EXPORTER,
  },
  {
    icon: 'thumb_down',
    iconColor: 'danger',
    heading: 'next-actions.DECLINE.heading',
    subheading: 'next-actions.DECLINE.subheading',
    action: 'decline',
    displayWithStatus: 0,
    view: 'EXPORT',
    showWithRole: UserRole.EXPORTER,
  },
  {
    icon: 'thumb_up',
    iconColor: 'primary',
    heading: 'next-actions.ACCEPT_IMPORT.heading',
    subheading: 'next-actions.ACCEPT_IMPORT.subheading',
    action: 'importAccept',
    displayWithStatus: 1,
    view: 'IMPORT',
    showWithRole: UserRole.IMPORTER,
  },
  {
    icon: 'thumb_down',
    iconColor: 'danger',
    heading: 'next-actions.DECLINE_IMPORT.heading',
    subheading: 'next-actions.DECLINE_IMPORT.subheading',
    action: 'importDecline',
    displayWithStatus: 1,
    view: 'IMPORT',
    showWithRole: UserRole.IMPORTER,
  },
  {
    icon: 'qr_code',
    iconColor: 'primary',
    heading: 'next-actions.QR.heading',
    subheading: 'next-actions.QR.subheading',
    action: 'generateQRCode',
    displayWithStatus: 1,
    view: 'EXPORT',
    showWithRole: UserRole.EXPORTER,
  },
  {
    icon: 'exit_to_app',
    iconColor: 'primary',
    heading: 'next-actions.EXIT.heading',
    subheading: 'next-actions.EXIT.subheading',
    action: 'exit',
    displayWithStatus: 4,
    view: 'EXPORT',
    showWithRole: UserRole.EXPORTER,
  },
  {
    icon: 'archive',
    iconColor: 'primary',
    heading: 'next-actions.ARCHIVE.heading',
    subheading: 'next-actions.ARCHIVE.subheading',
    action: 'archive',
    displayWithStatus: 5,
    view: 'EXPORT',
    showWithRole: UserRole.EXPORTER,
  },
  {
    icon: 'assignment_turned_in',
    iconColor: 'primary',
    heading: 'next-actions.FINALIZE.heading',
    subheading: 'next-actions.FINALIZE.subheading',
    action: 'finalizeImport',
    displayWithStatus: 1,
    view: 'IMPORT',
    showWithRole: UserRole.IMPORTER,
  },
];

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input, OnChanges } from '@angular/core';
import { fadeInItems } from '@angular/material/menu';
import { EADDto, ImportEntity } from '@base/src/app/core/_entities';
import { Document } from '@base/src/app/core/_entities/document.entity';

@Component({
  selector: 'app-files',
  templateUrl: './files.component.html',
  styleUrls: ['./files.component.scss'],
})
export class FilesComponent implements OnChanges {
  @Input() exportToken: EADDto | null = null;
  @Input() importToken: ImportEntity | null = null;
  files: Document[] = [];

  ngOnChanges(): void {
    if (this.exportToken) {
      this.exportToken.goodsItems.forEach((item) => {
        this.files = this.files.concat(item.documents);
      });
    }
    if (this.importToken) {
      this.importToken.goodsItems.forEach((item) => {
        this.files = this.files.concat(item.documents);
      });
    }
  }
}

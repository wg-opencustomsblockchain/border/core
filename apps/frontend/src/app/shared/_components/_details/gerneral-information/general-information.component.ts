/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input } from '@angular/core';
import { EADDto, ImportEntity } from '@base/src/app/core/_entities';
import { ImportService } from '@base/src/app/features/import/import.service';
import { TranslateService } from '@ngx-translate/core';
import { ExportService } from '../../../services/export.service';
import { CurrencyUtility } from '../../../utils/utils.currency';

@Component({
  selector: 'app-general-information',
  templateUrl: './general-information.component.html',
  styleUrls: ['./general-information.component.scss'],
})
export class GeneralInformationComponent {
  @Input() exportToken: EADDto | null = null;
  @Input() importToken: ImportEntity | null = null;
  currUtil = new CurrencyUtility();
  currentLang: string;

  constructor(
    private readonly translateService: TranslateService,
    private readonly exportService: ExportService,
    private importService: ImportService
  ) {
    this.currentLang = translateService.currentLang;
  }

  getIcon(status: number, tokenStatus: number) {
    if (tokenStatus === undefined) return 'sync';
    if (tokenStatus + 1 === status) return 'update';
    if (tokenStatus + 1 > status) return 'done_all';

    return 'schedule';
  }

  editToken() {
    if (this.exportToken) this.exportService.editExport(this.exportToken);
    if (this.importToken) this.importService.editImport(this.importToken);
  }
}

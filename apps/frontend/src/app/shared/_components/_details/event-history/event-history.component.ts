/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { EADDto, ImportEntity } from '@base/src/app/core/_entities';
import { User } from '@border/api-interfaces';
import { KeycloakService } from 'keycloak-angular';
import { TransportEvent } from '../../../../core/_entities/event.entity';

@Component({
  selector: 'app-event-history',
  templateUrl: './event-history.component.html',
  styleUrls: ['./event-history.component.scss'],
})
export class EventHistoryComponent implements OnInit, OnChanges {
  @Input() exportToken: EADDto | null = null;
  @Input() importToken: ImportEntity | null = null;
  currentUser: User;

  eventReverse: TransportEvent[] = [];

  constructor(private readonly authService: KeycloakService) {}

  async ngOnInit(): Promise<void> {
    this.currentUser = await this.authService.loadUserProfile();
  }

  ngOnChanges(): void {
    if (this.exportToken) {
      this.eventReverse = this.exportToken.events.slice().reverse();
    }
    if (this.importToken) {
      this.eventReverse = this.importToken.events.reverse();
    }
  }
}

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { EADDto, ImportEntity } from '@base/src/app/core/_entities';
import { User } from '@border/api-interfaces';
import { KeycloakService } from 'keycloak-angular';

@Component({
  selector: 'app-editors',
  templateUrl: './editors.component.html',
  styleUrls: ['./editors.component.scss'],
})
export class EditorsComponent implements OnChanges, OnInit {
  @Input() exportToken: EADDto | null = null;
  @Input() importToken: ImportEntity | null = null;
  editorMap: Map<string, string> = new Map<string, string>();

  currentUser: User;

  constructor(private readonly authService: KeycloakService) {}

  async ngOnInit(): Promise<void> {
    this.currentUser = await this.authService.loadUserProfile();
  }

  ngOnChanges(): void {
    if (this.exportToken)
      this.exportToken.events.forEach((event) => {
        if (!this.editorMap.has(event.creator)) this.editorMap.set(event.creator, event.creatorName);
      });
    if (this.importToken)
      this.importToken.events.forEach((event) => {
        if (!this.editorMap.has(event.creator)) this.editorMap.set(event.creator, event.creatorName);
      });
  }
}

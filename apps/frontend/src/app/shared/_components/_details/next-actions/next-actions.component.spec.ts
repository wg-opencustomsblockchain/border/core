/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule } from '@angular/material/dialog';
import {
  BrowserAnimationsModule,
  NoopAnimationsModule,
} from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule, TranslatePipe } from '@ngx-translate/core';
import { KeycloakService } from 'keycloak-angular';
import { ToastrModule } from 'ngx-toastr';
import { mockEAD } from '../../../../../assets/mockups/EADFile.mock';
import {
  ExportStatus,
  ImportStatus,
} from '../../../../core/_enums/status.enum';
import { ExportService } from '../../../services/export.service';
import { TranslatePipeMock } from '../../details-list/details.component.test';

import { NextActionsComponent } from './next-actions.component';

import { LoggerTestingModule } from 'ngx-logger/testing';
import { SharedModule } from '@shared/shared.module';
import { mockImport } from '@base/src/assets/mockups/Import.mock';
import { ImportService } from '@base/src/app/features/import/import.service';
import { of } from 'rxjs';
import { ImportEventTypes } from '@base/src/app/core/_enums/event-types.enum';
import { Router } from '@angular/router';

describe('NextActionsComponent', () => {
  let component: NextActionsComponent;
  let fixture: ComponentFixture<NextActionsComponent>;
  let importService: ImportService;
  let exportService: ExportService;
  let router: Router;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        TranslateModule.forRoot(),
        MatDialogModule,
        RouterTestingModule,
        ToastrModule.forRoot(),
        BrowserAnimationsModule,
        LoggerTestingModule,
        SharedModule,
        NoopAnimationsModule,
      ],
      declarations: [NextActionsComponent],
      providers: [
        {
          provide: KeycloakService,
          useService: KeycloakService,
        },
        {
          provide: TranslatePipe,
          useService: TranslatePipeMock,
        },
        { provide: ExportService, useService: ExportService },
        { provide: ImportService, useService: ImportService },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(NextActionsComponent);
    component = fixture.componentInstance;
    importService = TestBed.inject(ImportService);
    exportService = TestBed.inject(ExportService);
    router = TestBed.inject(Router);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('callFunction states', () => {
    it('should call accept', () => {
      const accept = jest.spyOn(NextActionsComponent.prototype, 'accept');

      component.exportToken = mockEAD.ead;
      component.exportToken.status = ExportStatus.NEW;
      component.callFunction('accept');
      expect(accept).toBeCalled();
    });
    it('should call decline', () => {
      const decline = jest.spyOn(NextActionsComponent.prototype, 'decline');

      component.exportToken = mockEAD.ead;
      component.exportToken.status = ExportStatus.NEW;
      component.callFunction('decline');
      expect(decline).toBeCalled();
    });
    it('should call generateQRCode', () => {
      const generateQRCode = jest.spyOn(
        NextActionsComponent.prototype,
        'generateQRCode'
      );

      component.exportToken = mockEAD.ead;
      component.exportToken.status = ExportStatus.RECORDED;
      component.callFunction('generateQRCode');
      expect(generateQRCode).toBeCalled();
    });
    it('should call archive', () => {
      const archive = jest.spyOn(NextActionsComponent.prototype, 'archive');

      component.exportToken = mockEAD.ead;
      component.exportToken.status = ExportStatus.EXIT;
      component.callFunction('archive');
      expect(archive).toBeCalled();
    });
    it('should call exit', () => {
      const exit = jest.spyOn(NextActionsComponent.prototype, 'exit');

      component.exportToken = mockEAD.ead;
      component.exportToken.status = ExportStatus.PRESENTED;
      component.callFunction('exit');
      expect(exit).toBeCalled();
    });

    it('should throw error', () => {
      expect(() => {
        component.callFunction('default');
      }).toThrow('No function defined');
    });

    it('should call finalizeImport', () => {
      const final = jest.spyOn(
        NextActionsComponent.prototype,
        'finalizeImport'
      );

      component.importToken = mockImport;
      component.callFunction('finalizeImport');
      expect(final).toBeCalled();
    });

    it('should change status after accept', () => {
      component.exportToken = mockEAD.ead;
      component.exportToken.status = ExportStatus.EXIT;
      expect(component.exportToken.status).not.toBe(ExportStatus.RECORDED);
      jest.spyOn(exportService, 'acceptExportToken').mockReturnValue(of(true));
      component.accept();
      expect(component.exportToken.status).toBe(ExportStatus.RECORDED);
    });

    it('should change status after decline', () => {
      component.exportToken = mockEAD.ead;
      expect(component.exportToken.status).not.toBe(ExportStatus.NEW);
      jest.spyOn(exportService, 'setStatus').mockReturnValue(of(true));
      component.decline();
      expect(component.exportToken.status).toBe(ExportStatus.NEW);
    });

    it('should navigate after import accept', () => {
      component.exportToken = mockEAD.ead;
      const routerSpy = jest.spyOn(router, 'navigateByUrl');
      jest.spyOn(exportService, 'acceptImport').mockReturnValue(of(true));
      component.importAccept();
      expect(routerSpy).toHaveBeenCalledWith('/imports/new');
    });

    it('should navigate after import decline', () => {
      component.exportToken = mockEAD.ead;
      const routerSpy = jest.spyOn(router, 'navigateByUrl');
      jest.spyOn(exportService, 'declineImport').mockReturnValue(of(true));
      component.importDecline();
      expect(routerSpy).toHaveBeenCalledWith('/imports/new');
    });
  });

  describe('filter next actions array', () => {
    it('should filter for finalize', () => {
      component.importToken = mockImport;
      component.importToken.status = '1';
      component.ngOnChanges();
      expect(component.nextActions[0].action).toEqual('finalizeImport');
    });

    it('should filter for accept and decline import', () => {
      component.exportToken = mockEAD.ead;
      component.exportToken.status = '1';
      component.ngOnChanges();
      expect(component.nextActions[0].action).toEqual('importAccept');
      expect(component.nextActions[1].action).toEqual('importDecline');
    });
  });
});

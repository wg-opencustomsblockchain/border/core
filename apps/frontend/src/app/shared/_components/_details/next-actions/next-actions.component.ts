/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { EADDto, ImportEntity } from '@base/src/app/core/_entities';
import { ExportStatus } from '@base/src/app/core/_enums/status.enum';
import { ExportService } from '@shared/services/export.service';
import { NextActions } from '@shared/constants';
import {
  ExportEventTypes,
  ImportEventTypes,
} from '../../../../core/_enums/event-types.enum';
import { Router } from '@angular/router';
import { UserRole } from '@base/src/app/core/_enums/user-role.enum';
import { ImportService } from '@base/src/app/features/import/import.service';

@Component({
  selector: 'app-next-actions',
  templateUrl: './next-actions.component.html',
  styleUrls: ['./next-actions.component.scss'],
})
export class NextActionsComponent implements OnChanges {
  @Input() exportToken: EADDto | null = null;
  @Input() importToken: ImportEntity | null = null;
  @Input() isExporter: boolean;

  nextActions: any[];

  constructor(
    private readonly exportService: ExportService,
    private readonly importService: ImportService,
    private router: Router
  ) {}

  ngOnChanges(): void {
    if (this.exportToken) {
      if (this.isExporter)
        this.nextActions = NextActions.filter(
          (action) =>
            action.displayWithStatus === +this.exportToken.status &&
            action.view === 'EXPORT' &&
            action.showWithRole === UserRole.EXPORTER &&
            action.action !== 'decline'
        );
      if (!this.isExporter)
        this.nextActions = NextActions.filter(
          (action) =>
            action.displayWithStatus === +this.exportToken.status &&
            action.view === 'IMPORT' &&
            action.showWithRole === UserRole.IMPORTER &&
            action.action !== 'finalizeImport'
        );
    }
    if (this.importToken) {
      this.nextActions = NextActions.filter(
        (action) =>
          action.displayWithStatus === +this.importToken.status &&
          action.view === 'IMPORT' &&
          action.showWithRole === UserRole.IMPORTER &&
          action.action !== 'importDecline' &&
          action.action !== 'importAccept'
      );
    }
  }

  callFunction(name: string): void {
    switch (name) {
      case 'accept':
        this.accept();
        break;
      case 'decline':
        this.decline();
        break;
      case 'generateQRCode':
        this.generateQRCode();
        break;
      case 'archive':
        this.archive();
        break;
      case 'exit':
        this.exit();
        break;
      case 'importAccept':
        this.importAccept();
        break;
      case 'importDecline':
        this.importDecline();
        break;
      case 'finalizeImport':
        this.finalizeImport();
        break;
      default:
        throw Error('No function defined');
    }
  }

  accept(): void {
    this.exportService
      .acceptExportToken(this.exportToken)
      .subscribe((result: boolean) => {
        if (result) this.exportToken.status = ExportStatus.RECORDED;
      });
  }

  decline(): void {
    this.exportService
      .setStatus({
        eadDto: this.exportToken,
        status: ExportEventTypes.EXPORT_NEW,
      })
      .subscribe((result: boolean) => {
        if (result) this.exportToken.status = ExportStatus.NEW;
      });
  }

  generateQRCode(): void {
    this.exportService.showQRCode(this.exportToken);
  }

  archive(): void {
    this.exportService
      .setStatus({
        eadDto: this.exportToken,
        status: ExportEventTypes.EXPORT_ARCHIVED,
      })
      .subscribe((result: boolean) => {
        if (result) this.exportToken.status = ExportStatus.ARCHIVED;
      });
  }

  exit(): void {
    this.exportService
      .setStatus({
        eadDto: this.exportToken,
        status: ExportEventTypes.EXPORT_EXIT,
      })
      .subscribe((result: boolean) => {
        if (result) this.exportToken.status = ExportStatus.EXIT;
      });
  }

  importAccept(): void {
    this.exportService
      .acceptImport(this.exportToken)
      .subscribe(() => this.router.navigateByUrl('/imports/new'));
  }

  importDecline(): void {
    this.exportService
      .declineImport(this.exportToken)
      .subscribe(() => this.router.navigateByUrl('/imports/new'));
  }

  finalizeImport(): void {
    this.importService
      .setStatus({
        importToken: this.importToken,
        status: ImportEventTypes.IMPORT_COMPLETED,
      })
      .subscribe();
  }
}

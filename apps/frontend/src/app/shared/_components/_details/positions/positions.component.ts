/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input } from '@angular/core';
import { EADDto, ImportEntity } from '@base/src/app/core/_entities';
import { TranslateService } from '@ngx-translate/core';
import { CurrencyUtility } from '../../../utils/utils.currency';

@Component({
  selector: 'app-positions',
  templateUrl: './positions.component.html',
  styleUrls: ['./positions.component.scss'],
})
export class PositionsComponent {
  @Input() exportToken: EADDto | null = null;
  @Input() importToken: ImportEntity | null = null;
  currUtil = new CurrencyUtility();
  currentLang: string;

  constructor(private readonly translateService: TranslateService) {
    this.currentLang = translateService.currentLang;
  }
}

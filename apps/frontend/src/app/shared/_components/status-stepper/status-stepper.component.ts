/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-status-stepper',
  templateUrl: './status-stepper.component.html',
  styleUrls: ['./status-stepper.component.scss'],
})
export class StatusStepperComponent {
  @Input() status: number;
  @Input() isExport = true;

  getIcon(status: number, tokenStatus: number): string {
    if (tokenStatus === undefined) return 'sync';
    if (tokenStatus + 1 === status) return 'update';
    if (tokenStatus + 1 > status) return 'done_all';

    return 'schedule';
  }
}

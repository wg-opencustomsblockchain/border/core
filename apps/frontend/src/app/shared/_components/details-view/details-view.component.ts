/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { animate, state, style, transition, trigger } from '@angular/animations';
import { SelectionModel } from '@angular/cdk/collections';
import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { CurrencyUtility, SUPPORTED_CURRENCIES, SUPPORTED_LOCALES } from '@base/src/app/shared/utils/utils.currency';
import { UserRole, User } from '@border/api-interfaces';
import { TranslateService } from '@ngx-translate/core';
import { EADDto, GoodsItem, ImportEntity } from '../../../core/_entities';
import { ExportStatus } from '../../../core/_enums/status.enum';
import { ExportEventTypes } from '../../../core/_enums/event-types.enum';

/**
 * A desktop component for showing all details of a import or export token (table view)
 */
@Component({
  selector: 'app-details-view',
  templateUrl: './details-view.component.html',
  styleUrls: ['./details-view.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class DetailsViewComponent implements OnInit, AfterViewInit {
  // Inputs
  /**
   * The data that is displayed
   */
  @Input() dataSource: MatTableDataSource<EADDto | ImportEntity> = new MatTableDataSource();

  /**
   * The columns which are displayed
   */
  @Input() displayedColumns: string[] = [
    'mrn',
    'destinationCountry',
    'consignee',
    'customOfficeOfExit',
    'incotermCode',
    'status',
    'actions',
    'editedAt',
  ];
  /**
   * Should the table be editable
   */
  @Input() isEditable = false;
  /**
   * The current user object
   */
  @Input() user: User | null = null;

  @Input() userRole: UserRole;
  UserRole = UserRole;

  /**
   * Should the import status be shown
   */
  @Input() showImportStatus = false;

  /**
   * Should the export status be shown
   */
  @Input() showExportStatus = true;

  // Outputs
  /**
   * A function for barcode click
   */
  @Output() barcodeAction = new EventEmitter<EADDto>();

  /**
   * A function for export token view
   */
  @Output() editExportAction = new EventEmitter<EADDto>();

  /**
   * A function for upload click
   */
  @Output() uploadAction = new EventEmitter<EADDto>();
  /**
   * A function for accept click
   */
  @Output() acceptImportAction = new EventEmitter<EADDto>();

  /**
   * A function for accept click
   */
  @Output() refuseImportAction = new EventEmitter<EADDto>();
  /**
   * A function for edit import click
   */
  @Output() editImportAction = new EventEmitter<ImportEntity>();
  /**
   * A function for archive export click
   */
  @Output() setStatus = new EventEmitter<{ eadDto: EADDto; status: ExportEventTypes }>();

  /**
   * A function for archive export click
   */
  @Output() showTransportHistory = new EventEmitter<string>();

  /**
   * The material table pagination handler;
   */
  private paginator!: MatPaginator;
  /**
   * The material table sort handler
   */
  private sort!: MatSort;

  /**
   * ExportStatus fot html
   */
  ExportStatus = ExportStatus;

  /**
   * ExportEvents fot html
   */
  ExportEventTypes = ExportEventTypes;

  /**
   * Border Currency Utility to handle conversion back/from currency formats and display them.
   */
  currUtil = new CurrencyUtility();

  /**
   * Get enum of supported currencies to call from HTML context.
   */
  currencies = SUPPORTED_CURRENCIES;

  /**
   * Get enum of supported locales to call from HTML context.
   */
  locales = SUPPORTED_LOCALES;

  /**
   * Get the material sort view child an set it as the handler
   */
  @ViewChild(MatSort) set matSort(ms: MatSort) {
    this.sort = ms;
    this.setDataSourceAttributes();
  }

  /**
   * Get the material pagination view child an set it as the handler
   */
  @ViewChild(MatPaginator) set matPaginator(mp: MatPaginator) {
    this.paginator = mp;
    this.setDataSourceAttributes();
  }

  // Table Properties
  /**
   * Selected table cells (false is for single select)
   */
  selection = new SelectionModel<EADDto>(false, []);
  /**
   * The element that is expanded
   */
  expandedElement: EADDto | null = null;

  /**
   * A array of the columns that can be expanded
   */
  expandedColumns: string[] = [];

  /**
   * The Current language to format currencies
   */
  currentLang: string;

  /**
   * Initialize the view. Set the filter and sorting algorithms for the data source. Set all displayed columns to be expandable.
   */
  ngOnInit(): void {
    this.dataSource.filterPredicate = this.filterPredicate;
    this.dataSource.sortingDataAccessor = this.pathDataAccessor;
    this.displayedColumns.forEach((column: string) => this.expandedColumns.push(column + '_expanded'));
  }

  constructor(translateService: TranslateService) {
    this.currentLang = translateService.currentLang;
  }
  /**
   * Set the pagination and sorting of the data source object to the values initialized above.
   */
  setDataSourceAttributes(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  /**
   * The algorithm for sorting the table. It uses the defined mat-sort-header to get deeper into object in order
   * to sort for nested properties.
   * @param item - The html item
   * @param path - The material sort header
   * @returns The objects in the sorted way
   */
  pathDataAccessor(item: any, path: string): any {
    return path.split('.').reduce((accumulator: any, key: string) => {
      return accumulator ? accumulator[key] : undefined;
    }, item);
  }

  /**
   * Filters all attributes, if the filter string can be found
   * @param ead the ead
   * @param filter the string to filter for
   * @returns found or not found
   */
  filterPredicate(ead: EADDto | ImportEntity, filter: string): boolean {
    const transformedFilter = filter.trim().toLowerCase();

    const listAsFlatString = (obj: any): string => {
      let returnVal = '';
      Object.values(obj).forEach((val) => {
        if (typeof val !== 'object') {
          returnVal = returnVal + ' ' + val;
        } else if (val !== null) {
          returnVal = returnVal + ' ' + listAsFlatString(val);
        }
      });
      return returnVal.trim().toLowerCase();
    };

    return listAsFlatString(ead).includes(transformedFilter);
  }

  /**
   * Set pagination and sort to the data source after initialization
   */
  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  /**
   * Sums up all statistical values of the GoodItems in the array
   * @param goodsItems Good Item Array
   * @returns Sum of all good item values
   */
  getSumOfValueImport(goodsItems: GoodsItem[]): number {
    const currencyImport = goodsItems[0].valueAtBorderCurrency;
    // Check if all the Currencies are coherent and match up. Return 0 if not.
    for (const item of goodsItems) {
      if (item.valueAtBorderCurrency != currencyImport) return 0;
    }

    return goodsItems.reduce((a, b) => a + +b.valueAtBorder, 0);
  }

  /**
   * Sums up all statistical values of the GoodItems in the array
   * @param goodsItems Good Item Array
   * @returns Sum of all good item values
   */
  getSumOfValueExport(goodsItems: GoodsItem[]): number {
    const currencyExport = goodsItems[0].valueAtBorderExportCurrency;
    // Check if all the Currencies are coherent and match up. Return 0 if not.
    for (const item of goodsItems) {
      if (item.valueAtBorderExportCurrency != currencyExport) return 0;
    }
    return goodsItems.reduce((a, b) => a + +b.valueAtBorderExport, 0);
  }

  /**
   * Sums up the number of packages
   * @param goodsItems
   * @returns 
   */
  getSumOfPackages(goodsItems: GoodsItem[]): number {
    return goodsItems.reduce((a, b) => a + +b.numberOfPackages, 0);
  }

  /**
   * Sums up the mass of packages
   * @param goodsItems 
   * @returns 
   */
  getSumOfMass(goodsItems: GoodsItem[]): number {
    return goodsItems.reduce((a, b) => a + +b.netMass, 0);
  }

  /**
   * Apply the filter to the list
   * @param value the filter string
   */
  @Input() applyFilter(value: string): void {
    // const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = value.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}

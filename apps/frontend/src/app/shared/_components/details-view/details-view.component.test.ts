/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from '@base/src/app/shared/shared.module';
import { mockEAD } from '@base/src/assets/mockups/EADFile.mock';
import { TranslateModule } from '@ngx-translate/core';

import { DetailsViewComponent } from './details-view.component';

describe('DetailsViewComponent', () => {
  let component: DetailsViewComponent;
  let fixture: ComponentFixture<DetailsViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot(), SharedModule, NoopAnimationsModule],

      declarations: [DetailsViewComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsViewComponent);
    component = fixture.componentInstance;
    component.showImportStatus = false;
    component.showExportStatus = false;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should correctly calculate the sum of the goods', () => {
    component.showExportStatus = true;
    expect(component.getSumOfValueExport(mockEAD.ead.goodsItems)).toEqual(270);
  });

  it('should correctly calculate the sum of the goods', () => {
    component.showImportStatus = true;
    expect(component.getSumOfValueImport(mockEAD.ead.goodsItems)).toEqual(450);
  });

  it('should correctly calculate the sum of the packages', () => {
    expect(component.getSumOfPackages(mockEAD.ead.goodsItems)).toEqual(300);
  });

  it('should correctly calculate the sum of mass', () => {
    expect(component.getSumOfMass(mockEAD.ead.goodsItems)).toEqual(6000);
  });

  it('should filter correctly', () => {
    expect(component.filterPredicate(mockEAD.ead, 'Erik')).toEqual(true);
    expect(component.filterPredicate(mockEAD.ead, 'abcde')).toEqual(false);
  });

  it('should access the path correctly', () => {
    expect(component.pathDataAccessor(mockEAD.ead, 'exportId')).toEqual('20DE295638991213E1');
  });
});

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-security-status',
  templateUrl: './security-status.component.html',
  styleUrls: ['./security-status.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class SecurityStatusComponent {
  @Input() level: 'secure' | 'company' | 'external' | 'loading' = 'loading';

  @Input() lastEditedBy = 'Extern';
}

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { UserRole, User } from '@border/api-interfaces';
import { EADDto } from '@core/_entities/ead.entity';
import { ImportEntity } from '@core/_entities/import.entity';
import { KeycloakService } from 'keycloak-angular';

import { ExportStatus } from '../../../../core/_enums/status.enum';
import { tap } from 'rxjs/operators';
import { EditExportDialogComponent } from '../../../../core/_components/edit-export-dialog/edit-export-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { LoadingComponent } from '../../../../core/_components/loading/loading.component';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { AuthenticationService } from '../../../../core/services/authentication.service';
import { TransportEvent } from '../../../../core/_entities/event.entity';
import { EventHistoryType } from '../../../../core/_components/event-history/event-history.enum';
import { BehaviorSubject } from 'rxjs';
import { EventHistoryComponent } from '../../../../core/_components/event-history/event-history.component';
import { BorderApiService } from '../../../../core/services/import.service';
import { TokenService } from '@base/src/app/shared/services/token.service';
import { SpinnerComponent } from '../../../../core/_components/spinner/spinner.component';

/**
 * The archive page
 */
@Component({
  selector: 'app-archive',
  templateUrl: './archive.component.html',
  styleUrls: ['./archive.component.scss'],
})
export class ArchiveComponent implements OnInit {
  /**
   * The current user
   */
  user: User | null = null;

  /**
   * The current users roles
   */
  userRoles: string[];

  /**
   * A reference to the dialog
   */
  dialogRef: any;

  /**
   * An array with the columns that should be displayed
   */
  displayedColumns: string[] = [
    'mrn',
    'destinationCountry',
    'consignee',
    'customOfficeOfExit',
    'incotermCode',
    'status',
    'actions',
    'editedAt',
  ];
  /**
   * The actual data to display
   */
  dataSource: MatTableDataSource<EADDto | ImportEntity> = new MatTableDataSource();
  /**
   * Creates an instance of archive page component
   * @param eadService inject ead service
   * @param keycloak injects keycloak service
   * @param dialog injects dialog service
   * @param translateService injects translate service
   * @param authService injects auth service
   * @param toastr injects toastr service
   * @param importService injects import service
   */
  constructor(
    protected readonly eadService: TokenService,
    private readonly keycloak: KeycloakService,
    private readonly dialog: MatDialog,
    private translateService: TranslateService,
    private readonly authService: AuthenticationService,
    private toastr: ToastrService,
    private readonly importService: BorderApiService
  ) {}

  /**
   * Load data source on init
   */
  async ngOnInit(): Promise<void> {
    this.populateDatasources();
    this.userRoles = this.keycloak.getUserRoles();
    this.user = await this.keycloak.loadUserProfile();
  }

  /**
   * Load the archived eads from the service
   */
  populateDatasources(): void {
    this.dialog.open(SpinnerComponent, { panelClass: 'loading-container' });

    this.dataSource = new MatTableDataSource();
    this.userRoles = [UserRole.EXPORTER];

    this.eadService
      .getWrittenEAD()
      .pipe(
        tap((res: EADDto[]) => {
          this.dialog.closeAll();
          res
            .filter((ead: EADDto) => ead.status === ExportStatus.ARCHIVED)
            .forEach((ead: EADDto) => {
              this.setEventUser(ead);
              ead.exportStatus = +ead.status;

              this.dataSource.data.push(ead);
            });
          this.dataSource._updateChangeSubscription();
        })
      )
      .subscribe();
  }

  /**
   * Sets the User Objects for all Events as the Token does not include personal data.
   * @param token
   */
  private async setEventUser(token: EADDto | ImportEntity) {
    const userMap = new Map<string, User>();
    for (const event of token.events) {
      if (userMap.get(event.creator) === undefined) {
        this.authService.getUserByCosmosAddress(event.creator).subscribe((user: User) => {
          userMap.set(event.creator, user);
          event.user = user;
        });
      } else {
        event.user = userMap.get(event.creator);
      }
    }
  }

  /**
   * Takes a role string and parses it as corresponding UserRole type
   * @param role string which describes a role
   * @returns string role as UserRole
   */
  getUserRole(role: string): UserRole {
    return role as UserRole;
  }

  /**
   * Shows the loading popup
   */
  openLoadingDialog(): void {
    this.dialog.open(LoadingComponent, {
      data: {
        heading: this.translateService.instant('loading.component.heading'),
        text: this.translateService.instant('loading.component.heading'),
      },
      width: '80vw',
      disableClose: true,
    });
  }

  /**
   * Currently views export data in the details view.
   * @param ead export token
   */
  editExport(ead: EADDto): void {
    const editExportDialog = this.dialog.open(EditExportDialogComponent, {
      data: {
        export: ead,
      },
      width: '85vw',
      height: '85vh',
    });

    editExportDialog.afterClosed().subscribe((closeData: EADDto) => {
      if (!closeData) return;
      this.openLoadingDialog();
      this.eadService
        .updateExportToken([closeData])
        .then(() => {
          this.toastr.success(this.translateService.instant('toaster.update.success'));
          this.populateDatasources();
          this.dialog.closeAll();
        })
        .catch(() => {
          this.toastr.error(this.translateService.instant('toaster.update.failure'));
        });
    });
  }

  /**
   * Update the status of a given ead
   * @param ead the ead
   */
  async handleEADUpdate(ead: EADDto): Promise<void> {
    this.eadService
      .updateExportTokenEventLocal(ead)
      .then(() => this.toastr.success(this.translateService.instant('toaster.update.success')))
      .catch((result: Error) =>
        this.toastr.error(this.translateService.instant(result.message), '', {
          disableTimeOut: true,
        })
      )
      .finally(() => {
        this.dialog.closeAll();
        this.ngOnInit();
      });
  }

  /**
   * Combine, if necessary, Import Events and Export Events into a cohesive timeline and provide it for the
   * TransportHistory Component to consume.
   * @param text The text which should be displayed
   * @param eads the eads that should be displayed
   */
  async showTransportHistory(transportId: string): Promise<void> {
    let events: TransportEvent[] = [];
    const viewType = EventHistoryType.EXPORT_VIEW;
    const currentUser = this.user;
    const eventObservable = new BehaviorSubject<TransportEvent[]>([]);

    // At this point we're temporarily parsing through all our Token for demonstration purposes.
    // The intended transaction to receive a proper Event History is currently being worked on by a colleague.
    // Get all Import Events

    this.eadService.getWrittenEAD().subscribe((exTokenList) => {
      exTokenList.forEach(async (exToken) => {
        if (exToken.id == transportId) {
          await this.setEventUser(exToken);
          events = events.concat(exToken.events);
        }
        // Sort the Elements in ascending Order
        events = events.sort((objA, objB) => new Date(objB.timestamp).getTime() - new Date(objA.timestamp).getTime());
        eventObservable.next(events);
      });
    });

    this.dialogRef = this.dialog.open(EventHistoryComponent, {
      data: {
        eventObservable,
        viewType,
        currentUser,
      },
      width: '60vw',
    });
  }
}

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
  MatDialog,
  MatDialogModule,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthenticationService } from '@base/src/app/core/services/authentication.service';
import { SharedModule } from '@base/src/app/shared/shared.module';
import { mockEAD } from '@base/src/assets/mockups/EADFile.mock';
import {
  TranslateLoader,
  TranslateModule,
  TranslatePipe,
} from '@ngx-translate/core';
import { KeycloakService } from 'keycloak-angular';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { of } from 'rxjs';
import { user } from '@core/services/authentication.service.test';
import { DetailsViewComponent } from '../../details-view/details-view.component';
import { LoadingComponent } from '../../../../core/_components/loading/loading.component';
import { ExportStatus } from '../../../../core/_enums/status.enum';

import { ArchiveComponent } from './archive.component';
import { ActivatedRoute } from '@angular/router';
import { MOCK_USER } from '../../../../../assets/mockups/user.mock';
import { DialogConfirmWriteComponent } from '../../../../core/_pages/release/release.component';
import { HttpLoaderFactory } from '../../../../app.module';
import { Pipe, PipeTransform } from '@angular/core';
import { LightClientService } from '../../../../core/services/light-client.service';
import { WebsocketService } from '../../../../core/services/websocket.service';
import { EADFile } from '../../../../core/_entities';
import { TokenService } from '@base/src/app/shared/services/token.service';
import { CustomsOfficeService } from '../../../../core/services/customs-office.service';
import { LoggerTestingModule } from 'ngx-logger/testing';

describe('ArchiveComponent', () => {
  let component: ArchiveComponent;
  let fixture: ComponentFixture<ArchiveComponent>;
  let eadService: TokenService;
  let toastr: ToastrService;
  let dialog: MatDialog;
  let httpBool: true;
  let dialogComponent: DialogConfirmWriteComponent;
  let dialogFixture: ComponentFixture<DialogConfirmWriteComponent>;
  let authService: AuthenticationService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        SharedModule,
        MatDialogModule,
        NoopAnimationsModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [httpBool],
          },
        }),
        ToastrModule.forRoot(),
        LoggerTestingModule,
      ],
      declarations: [ArchiveComponent, DetailsViewComponent, TranslatePipeMock],
      providers: [
        { provide: TokenService, useClass: TokenService },
        { provide: MAT_DIALOG_DATA, useValue: {} },
        { provide: WebsocketService, useClass: WebsocketService },
        { provide: TranslatePipe, useClass: TranslatePipeMock },
        { provide: httpBool, useValue: true },
        { provide: AuthenticationService, useClass: AuthenticationService },
        { provide: LightClientService, useClass: LightClientService },
        { provide: CustomsOfficeService, useClass: CustomsOfficeService },
        {
          provide: KeycloakService,
          useValue: {
            async loadUserProfile() {
              return Promise.resolve(MOCK_USER[1]);
            },
            getUserRoles() {
              return ['EXPORTER'];
            },
          },
        },
        {
          provide: ActivatedRoute,
          useValue: {
            queryParams: of({
              view: 'active',
            }),
          },
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArchiveComponent);
    dialogFixture = TestBed.createComponent(DialogConfirmWriteComponent);
    dialog = fixture.debugElement.injector.get(MatDialog);
    dialogComponent = dialogFixture.componentInstance;
    component = fixture.componentInstance;
    fixture.detectChanges();
    toastr = TestBed.inject(ToastrService);
    eadService = TestBed.inject(TokenService);
    authService = TestBed.inject(AuthenticationService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('when the component initializes', () => {
    it('the list of eads should be fetched', () => {
      // jest.spyOn(eadService, 'getMissingEAD').mockReturnValue(of([]));
      jest.spyOn(eadService, 'getWrittenEAD').mockReturnValue(of([]));
      component.ngOnInit();
      expect(component.dataSource.data).toEqual([]);
    });
  });

  it('properly populate datasources', () => {
    const mock: EADFile = mockEAD;
    mock.ead.status = ExportStatus.ARCHIVED;

    // jest.spyOn(eadService, 'getMissingEAD').mockReturnValue(of([mock]));
    jest.spyOn(eadService, 'getWrittenEAD').mockReturnValue(of([mock.ead]));

    const authSpy = jest
      .spyOn(authService, 'getUserByCosmosAddress')
      .mockReturnValue(of(user));

    component.user = user;

    // Testing for positive behaviour
    component.populateDatasources();

    expect(component.dataSource).toBeDefined();
    expect(component.dataSource.data[0]).toBeDefined();
    expect(component.dataSource.data[1]).toBeUndefined();
    expect(component.dataSource.data[0].events[0].user).toEqual(user);
    expect(authSpy).toHaveBeenCalled();
  });

  it('getImportData() should do nothing if no user is set', () => {
    const eadSpy = jest
      .spyOn(eadService, 'getEadsReadyForImport')
      .mockReturnValue(of([mockEAD.ead]));

    component.user = null;

    component.populateDatasources();

    expect(component.dataSource).toBeDefined();
    expect(component.dataSource.data).toEqual([]);
    expect(eadSpy).toHaveBeenCalledTimes(0);
  });

  it('handeEADUpdate should call the correct functions with the correct ead', async () => {
    const eadServiceSpy = jest
      .spyOn(eadService, 'updateExportTokenEventLocal')
      .mockReturnValue(Promise.resolve(mockEAD.ead));
    const toastrSpySuccess = jest.spyOn(toastr, 'success');
    const toastrSpyWarning = jest.spyOn(toastr, 'warning');
    await component.handleEADUpdate(mockEAD.ead).then();

    expect(eadServiceSpy).toHaveBeenCalledWith(mockEAD.ead);
    expect(toastrSpySuccess).toHaveBeenCalledTimes(1);

    eadServiceSpy.mockReturnValue(Promise.reject(mockEAD.ead));
    component.handleEADUpdate(mockEAD.ead);
    expect(eadServiceSpy).toHaveBeenCalledWith(mockEAD.ead);
    expect(toastrSpyWarning).toHaveBeenCalledTimes(0);
  });

  it('should open the loading dialog', () => {
    const dialogSpy = jest.spyOn(dialog, 'open');
    component.openLoadingDialog();
    expect(dialogSpy).toHaveBeenCalledWith(LoadingComponent, {
      data: {
        heading: 'loading.component.heading',
        text: 'loading.component.heading',
      },
      width: '80vw',
      disableClose: true,
    });
  });

  it('should open the dialog and process the return data', () => {
    const exportToken = mockEAD.ead;
    const dialogSpy = jest
      .spyOn(dialog, 'open')
      .mockReturnValue({ afterClosed: () => of(true) } as MatDialogRef<
        typeof component
      >);
    component.editExport(exportToken);
    expect(dialogSpy).toHaveBeenCalled();
  });

  it('should open the dialog and process the event data', () => {
    const exportToken = mockEAD.ead;
    jest.spyOn(eadService, 'getWrittenEAD').mockReturnValue(of([mockEAD.ead]));
    const dialogSpy = jest
      .spyOn(dialog, 'open')
      .mockReturnValue({ afterClosed: () => of(true) } as MatDialogRef<
        typeof component
      >);
    component.showTransportHistory(exportToken.exportId);
    expect(dialogSpy).toHaveBeenCalled();
  });
});

@Pipe({
  name: 'translate',
})
export class TranslatePipeMock implements PipeTransform {
  public name = 'translate';

  public transform(query: string, ...args: any[]): any {
    return query;
  }
}

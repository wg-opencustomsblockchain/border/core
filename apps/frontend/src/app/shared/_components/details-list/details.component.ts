/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import {
  Component,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrencyUtility } from '@base/src/app/shared/utils/utils.currency';
import { ExportEventPipe } from '@base/src/app/shared/_pipes/statusLabel.pipe';
import { User } from '@border/api-interfaces';
import { BorderApiService } from '@core/services/import.service';
import { WebsocketService } from '@core/services/websocket.service';
import { EditImportDialogComponent } from '@core/_components/edit-import-dialog/edit-import-dialog.component';
import { LoadingComponent } from '@core/_components/loading/loading.component';
import { EADDto } from '@core/_entities/ead.entity';
import { ImportEntity } from '@core/_entities/import.entity';
import { ExportStatus } from '@core/_enums/status.enum';
import { TranslateService } from '@ngx-translate/core';
import { KeycloakService } from 'keycloak-angular';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { filter, map, switchMap, tap } from 'rxjs/operators';

import { TokenService } from '@base/src/app/shared/services/token.service';
import { AuthenticationService } from '../../../core/services/authentication.service';
import { CustomsOfficeService } from '../../../core/services/customs-office.service';
import { EditExportDialogComponent } from '../../../core/_components/edit-export-dialog/edit-export-dialog.component';
import { EventHistoryComponent } from '../../../core/_components/event-history/event-history.component';
import { EventHistoryType } from '../../../core/_components/event-history/event-history.enum';
import { SpinnerComponent } from '../../../core/_components/spinner/spinner.component';
import { Transport } from '../../../core/_entities';
import { TransportEvent } from '../../../core/_entities/event.entity';
import {
  ExportEventTypes,
  ImportEventTypes,
} from '../../../core/_enums/event-types.enum';
import { UserRole } from '../../../core/_enums/user-role.enum';
import { DialogConfirmWriteComponent } from '../../../core/_pages/release/release.component';

/**
 * The page showing the details of eads/imports
 */
@Component({
  selector: 'app-details-export',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      ),
    ]),
  ],
})
export class DetailsComponent implements OnInit, OnDestroy, OnChanges {
  @Input() eads: EADDto[] = [];
  @Input() imports: ImportEntity[] = [];

  /**
   * The current user
   */
  user: User | null = null;

  /**
   * The current users roles
   */
  userRoles: string[];

  UserRole = UserRole;

  /**
   * An array with the columns that should be displayed
   */
  displayedColumns: string[] = [
    'mrn',
    'destinationCountry',
    'consignee',
    'customOfficeOfExit',
    'incotermCode',
    'status',
    'actions',
    'editedAt',
  ];
  /**
   * The actual data to display
   */
  dataSource: MatTableDataSource<EADDto | ImportEntity> =
    new MatTableDataSource();
  /**
   * A reference to the dialog
   */
  dialogRef: any;
  /**
   * Should the import status be shown
   */
  showImportStatus = false;

  breadcrumb = 'breadcrumb.details';

  /**
   * Get the view property from the route query params
   */
  view$ = this.route.queryParams.pipe(map((p) => p['view']));

  /**
   * Subscription for the websocket
   */
  websocketSubscription: Subscription = new Subscription();

  /**
   * Subscription for the query
   */
  querySubscription: Subscription = new Subscription();

  /**
   * Border Currency utility to transform monetary values.
   */
  currUtil: CurrencyUtility = new CurrencyUtility();

  exportEventPipe = new ExportEventPipe();

  eventObservable = new Observable<Transport>();

  @Input() importer = true;

  /**
   * Creates an instance of details page component
   * @param eadService inject ead service
   * @param importService injects import service
   * @param lightClient injects light client service
   * @param dialog injects dialog service
   * @param router injects router
   * @param websocketService injects websocket service
   * @param toastr injects toastr service
   * @param translateService injects translate service
   * @param authService injects auth serviec
   * @param customsOfficeService injects customs office service
   * @param route injects current route
   */
  constructor(
    private readonly eadService: TokenService,
    private readonly importService: BorderApiService,
    private readonly dialog: MatDialog,
    private router: Router,
    private websocketService: WebsocketService,
    private toastr: ToastrService,
    private translateService: TranslateService,
    private readonly authService: AuthenticationService,
    private readonly customsOfficeService: CustomsOfficeService,
    private route: ActivatedRoute,
    private keycloak: KeycloakService
  ) {}

  /**
   * Initializes the current user, loads the correct breadcrumbs and requests the data to display.
   */
  async ngOnInit(): Promise<void> {
    // Get the current user
    this.userRoles = this.keycloak.getUserRoles();
    this.user = await this.keycloak.loadUserProfile();

    if (this.importer)
      this.dialog.open(SpinnerComponent, { panelClass: 'loading-container' });

    this.populateDatasources();
  }

  /**
   * Unsubscribes from the websocket on destroy
   */
  ngOnDestroy(): void {
    this.websocketSubscription.unsubscribe();
  }

  /**
   * If eads change based on the search filter, component needs to rerender list
   * @param changes
   */
  ngOnChanges(changes: SimpleChanges): void {
    if (changes['eads']) {
      this.dataSource = new MatTableDataSource();
      this.eads.forEach((element) => {
        this.dataSource.data.push(element);
      });
    }
  }

  /**
   * Requests the data from the service depending on the active view
   */
  initializeImport(): void {
    this.userRoles = ['IMPORTER'];
    this.querySubscription.unsubscribe();
    this.querySubscription = this.view$.subscribe((view: string) => {
      if (view === 'active') {
        this.breadcrumb = this.translateService.instant(
          'breadcrumb.import.active'
        );
        this.dataSource = new MatTableDataSource();
        this.getActiveImportData();
        this.showImportStatus = true;
        this.displayedColumns = [
          'id',
          'mrn-export',
          'exportCountry',
          'exporter',
          'incotermCode',
          'status',
          'actions',
          'editedAt',
        ];
      }
      if (view === 'new') {
        this.breadcrumb = this.translateService.instant(
          'breadcrumb.import.ready'
        );
        this.dataSource = new MatTableDataSource();
        this.getNewImportData();
        this.showImportStatus = false;
        this.displayedColumns = [
          'mrn',
          'exportCountry',
          'exporter',
          'incotermCode',
          'status',
          'actions',
          'editedAt',
        ];
      }
      if (!view || view === '')
        this.router.navigateByUrl('/details?view=active');
    });
  }

  /**
   * Get all export data
   */
  initializeExport(): void {
    this.dataSource = new MatTableDataSource();
    this.userRoles = ['EXPORTER'];

    this.eads.forEach((element) => {
      this.dataSource.data.push(element);
    });
  }

  /**
   * Get the data depending on the users role
   */
  populateDatasources(): void {
    // open loading dialog

    if (this.userRoles.includes('EXPORTER')) this.initializeExport();
    if (this.userRoles.includes('IMPORTER')) this.initializeImport();
  }

  /**
   * Request eads that are already imported to the blockchain and related to the user
   */
  getActiveImportData(): void {
    this.importService
      .getActiveImports()
      .pipe(
        tap((res: ImportEntity[]) => {
          res.forEach((im: ImportEntity) => {
            this.eadService
              .getEADFromBlockchain(im.relatedExportToken)
              .subscribe((ead: EADDto) => {
                if (ead) {
                  im.exportStatus = +ead.status - 1;
                }
                im.goodsItems.forEach((_value, index) => {
                  if (
                    im.goodsItems[index].sequenceNumber ==
                    ead.goodsItems[index].sequenceNumber
                  ) {
                    im.goodsItems[index].valueAtBorderExport =
                      ead.goodsItems[index].valueAtBorderExport;
                    im.goodsItems[index].valueAtBorderExportCurrency =
                      ead.goodsItems[index].valueAtBorderExportCurrency;
                  } else {
                    console.error('GoodsItems Sequence mismatch.');
                  }
                });
              });
            this.setCustomsFullname(im);
            im.importStatus = +im.status;
            this.dataSource.data.push(im);
            this.dataSource._updateChangeSubscription();
          });

          // close loading dialog
          this.dialog.closeAll();
        })
      )
      .subscribe();
  }

  /**
   * Get import data that has not been accepted
   * @returns void
   */
  getNewImportData(): void {
    // tslint:disable-next-line: curly
    if (!this.user) return;
    this.eadService
      .getEadsReadyForImport(this.user.username)
      .pipe(
        tap((res: EADDto[]) => {
          this.importService
            .getActiveImports()
            .subscribe((imports: Array<ImportEntity>) => {
              res.forEach((ead: EADDto) => {
                if (ead.status !== ExportStatus.NEW) {
                  let isAlreadyImported = false;
                  isAlreadyImported = imports.some(
                    (vendor) => vendor.relatedExportToken === ead.id
                  );
                  // tslint:disable-next-line: curly
                  if (isAlreadyImported) return;
                  this.setCustomsFullname(ead);
                  ead.importStatus = 0;
                  ead.exportStatus = +ead.status - 1;
                  this.dataSource.data.push(ead);
                  this.dataSource._updateChangeSubscription();
                }

                // close loading dialog
                this.dialog.closeAll();
              });
            });
        })
      )
      .subscribe();
  }

  /**
   * Loads the full name of the customs office
   * @param object export or import token
   */
  setCustomsFullname(object: EADDto | ImportEntity): void {
    object.customOfficeOfExit.customsOfficeName =
      this.customsOfficeService.getFullName(
        object.customOfficeOfExit.customsOfficeCode
      );
  }

  /**
   * Navigate back to selection menu
   */
  returnToSelection(): void {
    this.router.navigateByUrl('selection');
  }

  /**
   * Show the QRCode for the selected element after confirmation
   * @param element the selected element
   */
  showQRCode(element: EADDto): void {
    this.openConfirmDialog(
      this.translateService.instant('dialog.confirm.barcode.show'),
      [element]
    );

    this.dialogRef
      .afterClosed()
      .pipe(
        filter((result: boolean) => result),
        tap(() => {
          element.eventType = ExportEventTypes.EXPORT_READY;
          this.handleEADUpdate(element);
          this.router.navigate([`/barcode`], {
            queryParams: { id: element.id },
          });
        })
      )
      .subscribe();
  }

  /**
   * Changes the Status to the desired EventType
   * @param element the selected element
   */
  setStatus(event: { eadDto: EADDto; status: ExportEventTypes }): void {
    this.openConfirmDialog(
      this.translateService.instant('dialog.confirm.status'),
      [event.eadDto]
    );
    event.eadDto.eventType = event.status;

    this.dialogRef
      .afterClosed()
      .pipe(
        filter((result: boolean) => result),
        tap(() => {
          this.handleEADUpdate(event.eadDto);
        })
      )
      .subscribe();
  }

  /**
   * Update the status of a given ead
   * @param ead the ead
   */
  async handleEADUpdate(ead: EADDto): Promise<void> {
    this.eadService
      .updateExportTokenEventLocal(ead)
      .then(() =>
        this.toastr.success(
          this.translateService.instant('toaster.update.success')
        )
      )
      .catch((result: Error) =>
        this.toastr.error(this.translateService.instant(result.message), '', {
          disableTimeOut: true,
        })
      )
      .finally(() => {
        this.dialog.closeAll();
        this.ngOnInit();
      });
  }

  /**
   * Accepts an import and writes it to the blockchain
   * @param ead export token
   */
  acceptImport(ead: EADDto): void {
    this.openConfirmDialog(
      this.translateService.instant('dialog.confirm.write.text.import'),
      [ead]
    );

    this.dialogRef
      .afterClosed()
      .pipe(
        filter((result: boolean) => result),
        tap(() => this.openLoadingDialog()),
        switchMap(() => {
          return this.eadService.createImportToken([
            ImportEntity.parseExport(ead),
          ]);
        })
      )
      .subscribe(() => {
        this.populateDatasources();
        this.dialog.closeAll();
      });
  }

  /**
   * Refuses an import and writes an event to the blockchain
   * @param ead export token
   */
  refuseImport(ead: EADDto): void {
    this.openConfirmDialog(
      this.translateService.instant('dialog.confirm.refuse.text.import'),
      [ead]
    );

    this.dialogRef
      .afterClosed()
      .pipe(
        filter((result: boolean) => result),
        tap(() => {
          ead.eventType = ExportEventTypes.EXPORT_REFUSAL;
          return this.handleEADUpdate(ead);
        })
      )
      .subscribe(() => {
        this.populateDatasources();
        this.dialog.closeAll();
      });
  }

  /**
   * Upload the given ead to the blockchain after confirmation
   * @param element the ead
   */
  uploadToBlockchain(element: EADDto): void {
    this.openConfirmDialog(
      this.translateService.instant('dialog.confirm.write.text'),
      [element]
    );
    element.status = ExportStatus.RECORDED;
    element.eventType = ExportEventTypes.EXPORT_RECORDED;
    this.dialogRef
      .afterClosed()
      .pipe(
        filter((result: boolean) => result),
        tap(() => this.openLoadingDialog()),
        switchMap(() => this.eadService.createExportToken([element]))
      )
      .subscribe(() => {
        this.toastr.success(
          this.translateService.instant('toaster.update.success')
        );
        this.populateDatasources();
        this.dialog.closeAll();
      });
  }

  /**
   * Creates and opens the confirm dialog with the given values
   * @param text The text which should be displayed
   * @param eads the eads that should be displayed
   */
  openConfirmDialog(text: string, eads: EADDto[]): void {
    this.dialogRef = this.dialog.open(DialogConfirmWriteComponent, {
      data: {
        eads,
        text,
      },
      width: '80vw',
    });
  }

  /**
   * Shows the loading popup
   */
  openLoadingDialog(): void {
    this.dialog.open(LoadingComponent, {
      data: {
        heading: this.translateService.instant('loading.component.heading'),
        text: this.translateService.instant('loading.component.heading'),
      },
      width: '80vw',
      disableClose: true,
    });
  }

  /**
   * Edits an import and updates it on the blockchain
   * All input Data for Monetary amounts have to be increased by a factor, depending on the currency.
   * @param im import token
   */
  editImport(im: ImportEntity): void {
    const editImportDialog = this.dialog.open(EditImportDialogComponent, {
      data: {
        import: im,
      },
      width: '85vw',
      height: '85vh',
    });

    editImportDialog.afterClosed().subscribe((closeData: ImportEntity) => {
      // tslint:disable-next-line: curly
      if (!closeData) return;
      this.openLoadingDialog();
      closeData.eventType = ImportEventTypes.IMPORT_SUPPLEMENT;
      this.eadService
        .updateImportToken([closeData])
        .then(() => {
          this.toastr.success(
            this.translateService.instant('toaster.update.success')
          );
          this.populateDatasources();
          this.dialog.closeAll();
        })
        .catch(() => {
          this.toastr.error(
            this.translateService.instant('toaster.update.failure')
          );
        });
    });
  }

  /**
   * Currently views export data in the details view.
   * @param ead export token
   */
  editExport(ead: EADDto): void {
    const editExportDialog = this.dialog.open(EditExportDialogComponent, {
      data: {
        export: ead,
      },
      width: '85vw',
      height: '85vh',
    });

    editExportDialog.afterClosed().subscribe((closeData: EADDto) => {
      if (!closeData) return;
      this.openLoadingDialog();
      this.eadService
        .updateExportToken([closeData])
        .then(() => {
          this.toastr.success(
            this.translateService.instant('toaster.update.success')
          );
          this.populateDatasources();
          this.dialog.closeAll();
        })
        .catch(() => {
          this.toastr.error(
            this.translateService.instant('toaster.update.failure')
          );
        });
    });
  }

  /**
   * Combine, if necessary, Import Events and Export Events into a cohesive timeline and provide it for the
   * TransportHistory Component to consume.
   * @param text The text which should be displayed
   * @param eads the eads that should be displayed
   */
  async showTransportHistory(transportId: string): Promise<void> {
    let events: TransportEvent[] = [];
    const viewType = EventHistoryType.EXPORT_VIEW;
    const currentUser = this.user;
    const eventObservable = new BehaviorSubject<TransportEvent[]>([]);

    // At this point we're temporarily parsing through all our Token for demonstration purposes.
    // The intended transaction to receive a proper Event History is currently being worked on by a colleague.
    // Get all Import Events

    this.eadService
      .getEADFromBlockchain(transportId)
      .subscribe((exportToken) => {
        events = events.concat(exportToken.events);
        events = events.sort(
          (objA, objB) =>
            new Date(objB.timestamp).getTime() -
            new Date(objA.timestamp).getTime()
        );
        eventObservable.next(events);
      });

    this.importService.getActiveImports().subscribe((imTokenList) => {
      imTokenList.forEach(async (imToken) => {
        if (imToken.relatedExportToken == transportId) {
          events = events.concat(imToken.events);
        }
      });
      events = events.sort(
        (objA, objB) =>
          new Date(objB.timestamp).getTime() -
          new Date(objA.timestamp).getTime()
      );
      eventObservable.next(events);
    });

    this.dialogRef = this.dialog.open(EventHistoryComponent, {
      data: {
        eventObservable,
        viewType,
        currentUser,
      },
      width: '60vw',
    });
  }

  getUserRole(role: string): UserRole {
    return role as UserRole;
  }
}

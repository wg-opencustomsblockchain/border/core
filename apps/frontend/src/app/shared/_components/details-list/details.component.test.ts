/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Pipe, PipeTransform } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
  MatDialog,
  MatDialogModule,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import {
  BrowserAnimationsModule,
  NoopAnimationsModule,
} from '@angular/platform-browser/animations';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpLoaderFactory } from '@base/src/app/app.module';
import { mockEAD } from '@base/src/assets/mockups/EADFile.mock';
import { AuthenticationService } from '@core/services/authentication.service';
import { user } from '@core/services/authentication.service.test';
import { BorderApiService } from '@core/services/import.service';
import { LightClientService } from '@core/services/light-client.service';
import { WebsocketService } from '@core/services/websocket.service';
import { LoadingComponent } from '@core/_components/loading/loading.component';
import { ImportEntity } from '@core/_entities/import.entity';
import { ExportStatus } from '@core/_enums/status.enum';
import {
  TranslateLoader,
  TranslateModule,
  TranslatePipe,
} from '@ngx-translate/core';
import { SocketIoConfig, SocketIoModule } from 'ngx-socket-io';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { EMPTY, of } from 'rxjs';

import { BreadcrumbComponent } from '@base/src/app/shared/breadcrumb/breadcrumb.component';
import { TokenService } from '@base/src/app/shared/services/token.service';
import { SharedModule } from '@base/src/app/shared/shared.module';
import { MOCK_USER } from '@base/src/assets/mockups/user.mock';
import { KeycloakService } from 'keycloak-angular';
import { LoggerTestingModule } from 'ngx-logger/testing';
import { CustomsOfficeService } from '../../../core/services/customs-office.service';
import { EADDto } from '../../../core/_entities';
import { ExportEventTypes } from '../../../core/_enums/event-types.enum';
import { DialogConfirmWriteComponent } from '../../../core/_pages/release/release.component';
import { SelectionComponent } from '../../../core/_pages/selection/selection.component';
import { DetailsViewComponent } from '../../_components/details-view/details-view.component';
import { DetailsComponent } from './details.component';

const config: SocketIoConfig = { url: 'http://localhost:3005', options: {} };

describe('DetailsComponent', () => {
  let component: DetailsComponent;
  let dialogComponent: DialogConfirmWriteComponent;
  let fixture: ComponentFixture<DetailsComponent>;
  let dialogFixture: ComponentFixture<DialogConfirmWriteComponent>;
  let dialog: MatDialog;
  let http: true;
  let eadService: TokenService;
  let importService: BorderApiService;
  let router: Router;
  let toastr: ToastrService;
  let eads: EADDto[];
  let authService: AuthenticationService;
  let tokenService: TokenService;
  let element: any;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        DetailsComponent,
        TranslatePipeMock,
        BreadcrumbComponent,
        DetailsViewComponent,
      ],
      imports: [
        HttpClientTestingModule,
        MatDialogModule,
        RouterTestingModule.withRoutes([
          { path: 'selection', component: SelectionComponent },
        ]),
        BrowserAnimationsModule,
        SharedModule,
        ToastrModule.forRoot(),
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [http],
          },
        }),
        SocketIoModule.forRoot(config),
        SharedModule,
        NoopAnimationsModule,
        LoggerTestingModule,
      ],
      providers: [
        { provide: TokenService, useClass: TokenService },
        {
          provide: MAT_DIALOG_DATA,
          useValue: { data: { eads: [mockEAD.ead], text: 'text' } },
        },
        { provide: WebsocketService, useClass: WebsocketService },
        { provide: TranslatePipe, useClass: TranslatePipeMock },
        { provide: http, useValue: true },
        { provide: AuthenticationService, useClass: AuthenticationService },
        { provide: LightClientService, useClass: LightClientService },
        { provide: CustomsOfficeService, useClass: CustomsOfficeService },
        {
          provide: ActivatedRoute,
          useValue: {
            queryParams: of({
              view: 'active',
            }),
          },
        },
        {
          provide: KeycloakService,
          useValue: {
            async loadUserProfile() {
              return Promise.resolve(MOCK_USER[1]);
            },
            getUserRoles() {
              return ['EXPORTER'];
            },
          },
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsComponent);
    dialogFixture = TestBed.createComponent(DialogConfirmWriteComponent);
    component = fixture.componentInstance;
    element = fixture.nativeElement;
    dialog = fixture.debugElement.injector.get(MatDialog);
    dialogComponent = dialogFixture.componentInstance;
    fixture.detectChanges();
    eadService = TestBed.inject(TokenService);
    importService = TestBed.inject(BorderApiService);
    router = TestBed.inject(Router);
    toastr = TestBed.inject(ToastrService);
    authService = TestBed.inject(AuthenticationService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // it('should detect changes', () => {
  //   component.ngOnChanges({

  //   }
  //     new SimpleChanges(null, {mockEAD.ead}, true))
  //   fixture.detectChanges();
  // });

  describe('when the component initializes', () => {
    it('the list of eads should be fetched', () => {
      // jest.spyOn(eadService, 'getMissingEAD').mockReturnValue(of([]));
      jest.spyOn(eadService, 'getWrittenEAD').mockReturnValue(of([]));
      component.ngOnInit();
      expect(component.dataSource.data).toEqual([]);
    });

    it('the list of imports should be fetched', () => {
      jest.spyOn(importService, 'getActiveImports').mockReturnValue(of([]));
      jest.spyOn(eadService, 'getEadsReadyForImport').mockReturnValue(of([]));
      component.initializeImport();
      expect(component.dataSource.data).toEqual([]);
    });
  });

  describe('when the return to selection button is clicked', () => {
    it('then the selection route should be displayed', () => {
      const navigateSpy = jest.spyOn(router, 'navigateByUrl');
      component.returnToSelection();
      expect(navigateSpy).toHaveBeenCalledWith('selection');
    });
  });

  describe('when ', () => {
    it('then the selection route should be displayed', () => {
      const navigateSpy = jest.spyOn(router, 'navigateByUrl');
      component.returnToSelection();
      expect(navigateSpy).toHaveBeenCalledWith('selection');
    });
  });

  describe('when a new status is', () => {
    it('then the selection route should be displayed', () => {
      for (let i = 0; i < Object.keys(ExportStatus).length; i++) {
        expect(i).toBe(i);
      }
    });
  });

  describe('if an action from the html gets clicked', () => {
    it('the dataStream should get filled with the given information', () => {
      const openDialogSpy = jest.spyOn(dialog, 'open').mockReturnValue({
        afterClosed: () => EMPTY,
      } as any);
      component.showQRCode(mockEAD.ead as EADDto);
      expect(dialogComponent.dataStream).toBeTruthy();
      expect(openDialogSpy).toHaveBeenCalled();
    });

    it('the dataStream should get filled with the given information', () => {
      const openDialogSpy = jest.spyOn(dialog, 'open').mockReturnValue({
        afterClosed: () => EMPTY,
      } as any);
      component.uploadToBlockchain(mockEAD.ead);
      expect(dialogComponent.dataStream).toBeTruthy();
      expect(openDialogSpy).toHaveBeenCalled();
    });
  });

  // Maybe update this Test with the Translation Component in Mind.
  it('should return the correct string for status enum', () => {
    expect(ExportStatus.ARCHIVED).toEqual('6');
    expect(ExportStatus.NEW).toEqual('0');
    expect(ExportStatus.EXIT).toEqual('5');
    expect(ExportStatus.PICKUP).toEqual('3');
    expect(ExportStatus.PRESENTED).toEqual('4');
    expect(ExportStatus.READY).toEqual('2');
    expect(ExportStatus.RECORDED).toEqual('1');
  });

  describe('When the view is initialized', () => {
    it('should properly populate the Data Tables for EXPORTER', () => {
      jest
        .spyOn(eadService, 'getWrittenEAD')
        .mockReturnValue(of([mockEAD.ead]));

      component.user = user;
      component.eads[0] = mockEAD.ead;

      // Testing for positive behaviour
      component.populateDatasources();

      expect(component.dataSource).toBeDefined();
      expect(component.dataSource.data[0]).toBeDefined();
      expect(component.dataSource.data[1]).toBeUndefined();
      expect(component.dataSource.data[0].events[0].user).toEqual(MOCK_USER[0]);
    });

    it('should properly populate the Data Tables for IMPORTER', () => {
      jest
        .spyOn(eadService, 'getEadsReadyForImport')
        .mockReturnValue(of([mockEAD.ead]));

      component.userRoles = ['IMPORTER'];

      component.view$ = of('active');

      // Testing for positive behaviour
      component.populateDatasources();

      expect(component.dataSource).toBeDefined();
      expect(component.dataSource.data[0]).toBeUndefined();
    });

    it('should properly populate the Data Tables for IMPORTER with new view', () => {
      jest
        .spyOn(eadService, 'getEadsReadyForImport')
        .mockReturnValue(of([mockEAD.ead]));

      component.userRoles = ['IMPORTER'];

      component.view$ = of('new');

      // Testing for positive behaviour
      component.populateDatasources();

      expect(component.dataSource).toBeDefined();
      expect(component.dataSource.data[0]).toBeUndefined();
    });

    it('getImportData() should do nothing if no user is set', () => {
      const eadSpy = jest
        .spyOn(eadService, 'getEadsReadyForImport')
        .mockReturnValue(of([mockEAD.ead]));

      component.user = null;

      component.populateDatasources();

      expect(component.dataSource).toBeDefined();
      expect(component.dataSource.data).toEqual([]);
      expect(eadSpy).toHaveBeenCalledTimes(0);
    });
  });

  it('getNewImportData should fill the stream with data', () => {
    const importSpy = jest
      .spyOn(importService, 'getActiveImports')
      .mockReturnValue(of([ImportEntity.parseExport(mockEAD.ead)]));
    jest
      .spyOn(eadService, 'getEadsReadyForImport')
      .mockReturnValue(of([mockEAD.ead]));
    component.getNewImportData();
    expect(component.dataSource.data.length).toEqual(0);

    component.getNewImportData();
    expect(component.dataSource.data.length).toEqual(0);

    importSpy.mockReturnValue(of([]));
    component.getNewImportData();
    expect(component.dataSource.data.length).toEqual(1);
    expect(component.dataSource.data[0].events[0].user).toEqual(MOCK_USER[0]);
  });

  it('acceptImport should fill the stream with data', async () => {
    const openDialogSpy = jest.spyOn(dialog, 'open').mockReturnValue({
      afterClosed: () => EMPTY,
    } as any);
    await component.acceptImport(mockEAD.ead);
    expect(dialogComponent.dataStream).toBeTruthy();
    expect(openDialogSpy).toHaveBeenCalled();
  });

  it('handeEADUpdate should call the correct functions with the correct ead', async () => {
    const eadServiceSpy = jest
      .spyOn(eadService, 'updateExportTokenEventLocal')
      .mockReturnValue(Promise.resolve(mockEAD.ead));
    const toastrSpySuccess = jest.spyOn(toastr, 'success');
    const toastrSpyWarning = jest.spyOn(toastr, 'warning');
    await component.handleEADUpdate(mockEAD.ead).then();

    expect(eadServiceSpy).toHaveBeenCalledWith(mockEAD.ead);
    expect(toastrSpySuccess).toHaveBeenCalledTimes(1);

    eadServiceSpy.mockReturnValue(Promise.reject(mockEAD.ead));
    component.handleEADUpdate(mockEAD.ead);
    expect(eadServiceSpy).toHaveBeenCalledWith(mockEAD.ead);
    expect(toastrSpyWarning).toHaveBeenCalledTimes(0);
  });

  it('setStatus should call the correct functions with the correct ead', async () => {
    const openDialogSpy = jest.spyOn(dialog, 'open').mockReturnValue({
      afterClosed: () => EMPTY,
    } as any);
    const eadServiceSpy = jest.spyOn(component, 'handleEADUpdate');
    component.setStatus({
      eadDto: mockEAD.ead,
      status: ExportEventTypes.EXPORT_PICKUP,
    });
    expect(openDialogSpy).toHaveBeenCalled();
  });

  it('should open the loading dialog', () => {
    const dialogSpy = jest.spyOn(dialog, 'open');
    component.openLoadingDialog();
    expect(dialogSpy).toHaveBeenCalledWith(LoadingComponent, {
      data: {
        heading: 'loading.component.heading',
        text: 'loading.component.heading',
      },
      width: '80vw',
      disableClose: true,
    });
  });

  it('should open the dialog and process the return data', () => {
    const importToken = ImportEntity.parseExport(mockEAD.ead);
    const dialogSpy = jest
      .spyOn(dialog, 'open')
      .mockReturnValue({ afterClosed: () => of(true) } as MatDialogRef<
        typeof component
      >);
    component.editImport(importToken);
    expect(dialogSpy).toHaveBeenCalled();
  });

  it('should open the dialog and process the event data', () => {
    const importToken = ImportEntity.parseExport(mockEAD.ead);
    const dialogSpy = jest
      .spyOn(dialog, 'open')
      .mockReturnValue({ afterClosed: () => of(true) } as MatDialogRef<
        typeof component
      >);
    component.showTransportHistory(importToken.relatedExportToken);
    expect(dialogSpy).toHaveBeenCalled();
  });
});

@Pipe({
  name: 'translate',
})
export class TranslatePipeMock implements PipeTransform {
  public name = 'translate';

  public transform(query: string, ...args: any[]): any {
    return query;
  }
}

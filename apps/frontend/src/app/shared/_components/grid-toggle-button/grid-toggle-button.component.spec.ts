/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatButtonToggleModule } from '@angular/material/button-toggle';

import { GridToggleButtonComponent } from './grid-toggle-button.component';
import { SharedModule } from '@shared/shared.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

describe('GridToggleButtonComponent', () => {
  let component: GridToggleButtonComponent;
  let fixture: ComponentFixture<GridToggleButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MatButtonToggleModule, SharedModule, NoopAnimationsModule],
      declarations: [GridToggleButtonComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(GridToggleButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    jest.spyOn(component.viewUpdate, 'emit');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.toggled).toStrictEqual('grid');
  });

  describe('methods', () => {
    it('should toggleView', () => {
      component.toggleView('grid');
      expect(component.toggled).toStrictEqual('grid');

      component.toggleView('list');
      expect(component.toggled).toStrictEqual('list');
    });

    it('should ', () => {
      // trigger the click
      const nativeElement = fixture.nativeElement;
      const button = nativeElement.querySelector('button');
      button.dispatchEvent(new Event('click'));

      fixture.detectChanges();

      expect(component.viewUpdate.emit).toHaveBeenCalled();
    });
  });
});

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatButtonToggleChange } from '@angular/material/button-toggle';

@Component({
  selector: 'app-grid-toggle-button',
  templateUrl: './grid-toggle-button.component.html',
  styleUrls: ['./grid-toggle-button.component.scss'],
})
export class GridToggleButtonComponent implements OnInit {
  // string which is either 'grid' or 'list'
  toggled: string;
  @Input() disabled: boolean;

  @Output() viewUpdate = new EventEmitter<string>();

  ngOnInit(): void {
    this.toggled = 'grid';
  }

  toggleView(value: string): void {
    value == 'grid' ? (this.toggled = 'grid') : (this.toggled = 'list');
  }

  onValChange(change: MatButtonToggleChange): void {
    this.toggleView(change.value);
    this.viewUpdate.emit(this.toggled);
  }
}

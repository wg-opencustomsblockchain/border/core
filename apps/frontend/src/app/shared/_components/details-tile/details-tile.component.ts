/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { EADDto } from '@base/src/app/core/_entities';
import {
  ExportEventTypes,
  ImportEventTypes,
} from '../../../core/_enums/event-types.enum';
import { ExportStatus, ImportStatus } from '../../../core/_enums/status.enum';
import { ExportService } from '../../services/export.service';
import { ImportEntity } from '@base/src/app/core/_entities';
import { ImportService } from '@base/src/app/features/import/import.service';

@Component({
  selector: 'app-details-tile',
  templateUrl: './details-tile.component.html',
  styleUrls: ['./details-tile.component.scss'],
})
export class DetailsTileComponent {
  @Input() ead?: EADDto;
  @Input() importEntity?: ImportEntity;
  @Input() isExporter: boolean;

  @Output() refreshEadList = new EventEmitter();
  @Output() eadUpdateEvent = new EventEmitter();

  toggle = false;
  exportStatusLength = Object.keys(ExportStatus).length;
  importStatusLength = Object.keys(ImportStatus).length;

  constructor(
    private readonly exportService: ExportService,
    private importService: ImportService
  ) {}

  async onNextAction(action: string): Promise<void> {
    switch (action) {
      case 'accept':
        this.exportService
          .acceptExportToken(this.ead)
          .subscribe((result: boolean) => {
            if (result) this.ead.status = ExportStatus.RECORDED;
          });
        break;
      case 'decline':
        this.exportService
          .setStatus({
            eadDto: this.ead,
            status: ExportEventTypes.EXPORT_REFUSAL,
          })
          .subscribe((result: boolean) => {
            if (result) this.ead.status = ExportStatus.DECLINED;
          });
        break;
      case 'generateQR':
        this.exportService.showQRCode(this.ead);
        break;
      case 'exit':
        this.exportService
          .setStatus({ eadDto: this.ead, status: ExportEventTypes.EXPORT_EXIT })
          .subscribe((result: boolean) => {
            if (result) this.ead.status = ExportStatus.EXIT;
          });
        break;
      case 'archive':
        this.exportService
          .setStatus({
            eadDto: this.ead,
            status: ExportEventTypes.EXPORT_ARCHIVED,
          })
          .subscribe((result: boolean) => {
            if (result) this.ead.status = ExportStatus.ARCHIVED;
          });
        break;
      case 'importAccept':
        this.exportService.acceptImport(this.ead).subscribe((res) => {
          if (res) {
            this.refreshEadList.emit();
          }
        });
        break;
      case 'importDecline':
        this.exportService
          .declineImport(this.ead as EADDto)
          .subscribe((res) => {
            if (res) {
              this.refreshEadList.emit();
            }
          });
        break;
      case 'finalizeImport':
        this.importService
          .setStatus({
            importToken: this.importEntity,
            status: ImportEventTypes.IMPORT_COMPLETED,
          })
          .subscribe((res: boolean) => {
            if (res) this.importEntity.status = ImportStatus.COMPLETED;
          });
        break;
      default:
        throw Error('No function defined');
    }
  }
}

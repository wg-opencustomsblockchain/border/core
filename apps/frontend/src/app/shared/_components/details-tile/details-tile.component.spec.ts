/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import {
  TranslateFakeLoader,
  TranslateLoader,
  TranslateModule,
  TranslateService,
} from '@ngx-translate/core';
import { mockEAD } from '../../../../assets/mockups/EADFile.mock';
import { MOCK_USER } from '../../../../assets/mockups/user.mock';
import { KeycloakService } from 'keycloak-angular';

import { DetailsTileComponent } from './details-tile.component';
import { MatDialogModule } from '@angular/material/dialog';
import { ToastrModule } from 'ngx-toastr';
import { ExportService } from '../../services/export.service';
import { LoggerTestingModule } from 'ngx-logger/testing';
import { SharedModule } from '@shared/shared.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { DialogConfirmWriteComponent } from '@core/_pages/release/release.component';
import { ImportService } from '@base/src/app/features/import/import.service';
import { of } from 'rxjs';
import {
  ExportStatus,
  ImportStatus,
} from '@base/src/app/core/_enums/status.enum';
import { mockImport } from '@base/src/assets/mockups/Import.mock';

describe('DetailsTileComponent', () => {
  let component: DetailsTileComponent;
  let fixture: ComponentFixture<DetailsTileComponent>;
  let exportService: ExportService;
  let importService: ImportService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DetailsTileComponent, DialogConfirmWriteComponent],
      providers: [
        {
          provide: TranslateService,
          useClass: TranslateService,
        },
        {
          provide: KeycloakService,
          useValue: {
            async loadUserProfile() {
              return Promise.resolve(MOCK_USER[1]);
            },
            getUserRoles() {
              return ['EXPORTER'];
            },
          },
        },
      ],
      imports: [
        RouterTestingModule,
        HttpClientModule,
        MatDialogModule,
        LoggerTestingModule,
        ToastrModule.forRoot(),
        TranslateModule.forRoot(),
        SharedModule,
        NoopAnimationsModule,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(DetailsTileComponent);
    component = fixture.componentInstance;
    component.ead = mockEAD.ead;
    exportService = TestBed.inject<ExportService>(ExportService);
    importService = TestBed.inject<ImportService>(ImportService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call the correct next action', () => {
    const acceptSpy = jest.spyOn(exportService, 'acceptExportToken');
    const setStatusSpy = jest.spyOn(exportService, 'setStatus');
    const qrSpy = jest.spyOn(exportService, 'showQRCode');
    const importAcceptSpy = jest.spyOn(exportService, 'acceptImport');
    const importDeclineSpy = jest.spyOn(exportService, 'declineImport');
    const setStatusImportSpy = jest.spyOn(importService, 'setStatus');
    component.onNextAction('accept');
    expect(acceptSpy).toBeCalledTimes(1);
    component.onNextAction('decline');
    component.onNextAction('exit');
    component.onNextAction('archive');
    expect(setStatusSpy).toBeCalledTimes(3);
    component.onNextAction('generateQR');
    expect(qrSpy).toBeCalled();
    component.onNextAction('importAccept');
    expect(importAcceptSpy).toBeCalledTimes(1);
    component.onNextAction('importDecline');
    expect(importDeclineSpy).toBeCalledTimes(1);
    component.onNextAction('finalizeImport');
    expect(setStatusImportSpy).toHaveBeenCalledTimes(1);
  });

  it('should Change status to COMPLETED', () => {
    component.importEntity = mockImport;
    const setStatusImportSpy = jest
      .spyOn(importService, 'setStatus')
      .mockReturnValue(of(true));
    component.onNextAction('finalizeImport');
    expect(component.importEntity.status).toBe(ImportStatus.COMPLETED);
  });

  it('should Change status to EXIT', () => {
    component.ead = mockEAD.ead;
    const setStatusSpy = jest
      .spyOn(exportService, 'setStatus')
      .mockReturnValue(of(true));
    component.onNextAction('exit');
    expect(component.ead.status).toBe(ExportStatus.EXIT);
  });

  it('should Change status to EXIT', () => {
    component.ead = mockEAD.ead;
    const setStatusSpy = jest
      .spyOn(exportService, 'setStatus')
      .mockReturnValue(of(true));
    component.onNextAction('archive');
    expect(component.ead.status).toBe(ExportStatus.ARCHIVED);
  });

  it('should Change status to DECLINED', () => {
    component.ead = mockEAD.ead;
    const setStatusSpy = jest
      .spyOn(exportService, 'setStatus')
      .mockReturnValue(of(true));
    component.onNextAction('decline');
    expect(component.ead.status).toBe(ExportStatus.DECLINED);
  });

  it('should Change status to RECORDED', () => {
    component.ead = mockEAD.ead;
    const setStatusSpy = jest
      .spyOn(exportService, 'acceptExportToken')
      .mockReturnValue(of(true));
    component.onNextAction('accept');
    expect(component.ead.status).toBe(ExportStatus.RECORDED);
  });
});

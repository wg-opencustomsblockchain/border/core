/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { CountryCodePipe } from './country.pipe';

describe('countryPipe', () => {
  let component: CountryCodePipe;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CountryCodePipe],
      imports: [TranslateModule.forRoot()],
      providers: [CountryCodePipe],
    });
    component = TestBed.inject(CountryCodePipe);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

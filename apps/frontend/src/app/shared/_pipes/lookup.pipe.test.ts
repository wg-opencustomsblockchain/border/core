/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { CustomsOfficeService } from '../../core/services/customs-office.service';

import { LookupPipe } from './lookup.pipe';
import { LoggerTestingModule } from 'ngx-logger/testing';

describe('lookupPipe', () => {
  let component: LookupPipe;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LookupPipe],
      imports: [HttpClientTestingModule, TranslateModule.forRoot(), LoggerTestingModule],
      providers: [
        LookupPipe,
        {
          provide: CustomsOfficeService,
          useClass: CustomsOfficeService,
        },
      ],
    }).compileComponents();
    component = TestBed.inject(LookupPipe);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should correclty deliver a translation', () => {
    // This line breaks because of a commit that changed in
    // border.frontend/src/app/core/services/light-client.service.ts line 45
    //expect(component.transform('DE001006')).toEqual('Hamburg');
    expect(component.transform('Cats')).toEqual('Cats');
  });
});

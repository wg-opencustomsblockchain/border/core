/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Pipe, PipeTransform } from '@angular/core';

/** Angular pipe class to parse customs code into human readble names. */
@Pipe({ name: 'countryCode' })
export class CountryCodePipe implements PipeTransform {
  regionNamesEnglish: Intl.DisplayNames = new Intl.DisplayNames(['en'], { type: 'region' });

  /**
   * the main function of the pipe
   * converts an input value to the output value
   * In this case: gets a county code and return the correct country name
   * @param value input value
   * @returns output value
   */
  transform(value: string | undefined): string {
    if (!value) return 'Country Code not provided';
    return this.regionNamesEnglish.of(value);
  }
}

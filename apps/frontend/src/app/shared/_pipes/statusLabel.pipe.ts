/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ExportEventTypes, ImportEventTypes } from '../../core/_enums/event-types.enum';
import { ExportStatus, ImportStatus } from '../../core/_enums/status.enum';

/**
 * Utility Pipe to converts a status to its event equivalent.
 * The status are a subset of Events.
 */
@Pipe({
  name: 'exportEvent',
})
export class ExportEventPipe implements PipeTransform {
  transform(value: string): string {
    switch (value) {
      case ExportStatus.NEW:
        return ExportEventTypes.EXPORT_RECORDED;
      case ExportStatus.PICKUP:
        return ExportEventTypes.EXPORT_PICKUP;
      case ExportStatus.READY:
        return ExportEventTypes.EXPORT_READY;
      case ExportStatus.RECORDED:
        return ExportEventTypes.EXPORT_RECORDED;
      case ExportStatus.ARCHIVED:
        return ExportEventTypes.EXPORT_ARCHIVED;
      case ExportStatus.EXIT:
        return ExportEventTypes.EXPORT_EXIT;
      case ExportStatus.PRESENTED:
        return ExportEventTypes.EXPORT_PRESENTED;
      case ExportEventTypes.EXPORT_REFUSAL:
        return ExportEventTypes.EXPORT_REFUSAL;
      default:
        return 'Unknown Event';
    }
  }
}

/**
 * Translates an event code to a human readable string.
 */
@Pipe({
  name: 'exportEventTranslation',
})
export class ExportEventTranslation implements PipeTransform {
  constructor(private readonly translateService: TranslateService) {}
  transform(value: string): string {
    switch (value) {
      case ExportEventTypes.EXPORT_NEW:
        return this.translateService.instant('event.export.new');
      case ExportEventTypes.EXPORT_RECORDED:
        return this.translateService.instant('event.export.recorded');
      case ExportEventTypes.EXPORT_READY:
        return this.translateService.instant('event.export.ready');
      case ExportEventTypes.EXPORT_PICKUP:
        return this.translateService.instant('event.export.pickup');
      case ExportEventTypes.EXPORT_PRESENTED:
        return this.translateService.instant('event.export.presented');
      case ExportEventTypes.EXPORT_EXIT:
        return this.translateService.instant('event.export.exit');
      case ExportEventTypes.EXPORT_ARCHIVED:
        return this.translateService.instant('event.export.archived');
      case ExportEventTypes.EXPORT_CHECK:
        return this.translateService.instant('event.export.check');
      case ExportEventTypes.EXPORT_MOBILECHECK:
        return this.translateService.instant('event.export.mobilecheck');
      case ExportEventTypes.EXPORT_REFUSAL:
        return this.translateService.instant('event.export.refused');
      default:
        return 'Unknown Eventtype';
    }
  }
}

/**
 * Utility Pipe to converts a status to its event equivalent.
 * The status are a subset of Events.
 */
@Pipe({
  name: 'importEvent',
})
export class ImportStatusPipe implements PipeTransform {
  transform(value: string): string {
    switch (value) {
      case ImportStatus.ACCEPTED:
        return ImportEventTypes.IMPORT_ACCEPTED;
      case ImportStatus.COMPLETED:
        return ImportEventTypes.IMPORT_COMPLETED;
      case ImportStatus.EXPORTED:
        return ImportEventTypes.IMPORT_EXPORTED;
      case ImportStatus.PROPOSAL:
        return ImportEventTypes.IMPORT_PROPOSAL;
      case ImportStatus.RELEASE:
        return ImportEventTypes.IMPORT_RELEASE;
      case ImportStatus.SUBMITTED:
        return ImportEventTypes.IMPORT_SUBMITTED;

      default:
        return 'Unknown Status';
    }
  }
}

/**
 * Translates an event code to a human readable string.
 */
@Pipe({
  name: 'importEventTranslation',
})
export class ImportEventTranslation implements PipeTransform {
  constructor(private readonly translateService: TranslateService) {}
  transform(value: string): string {
    switch (value) {
      case ImportEventTypes.IMPORT_ACCEPTED:
        return this.translateService.instant('event.import.accepted');
      case ImportEventTypes.IMPORT_COMPLETED:
        return this.translateService.instant('event.import.completed');
      case ImportEventTypes.IMPORT_EXPORTED:
        return this.translateService.instant('event.import.exported');
      case ImportEventTypes.IMPORT_PROPOSAL:
        return this.translateService.instant('event.import.proposed');
      case ImportEventTypes.IMPORT_RELEASE:
        return this.translateService.instant('event.import.released');
      case ImportEventTypes.IMPORT_SUBMITTED:
        return this.translateService.instant('event.import.review');
      case ImportEventTypes.IMPORT_SUPPLEMENT:
        return this.translateService.instant('event.import.supplement');
      case ImportEventTypes.IMPORT_REFUSED:
        return this.translateService.instant('event.import.refused');
      default:
        return 'Unknown EventType';
    }
  }
}

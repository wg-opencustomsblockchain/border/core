/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Pipe, PipeTransform } from '@angular/core';
import { CustomsOfficeService } from '@core/services/customs-office.service';

/** Angular pipe class to parse customs code into human readble names. */
@Pipe({ name: 'customsLookup' })
export class LookupPipe implements PipeTransform {
  /** Injects a service which implements the customs resolution. */
  constructor(private readonly customsOfficeService: CustomsOfficeService) {}

  /**
   * the main function of the pipe
   * converts an input value to the output value
   * In this case: gets a customs code and return the correct full name
   * @param value input value
   * @returns output value
   */
  transform(value: string | undefined): string {
    if (!value) return 'Office not provided';
    return this.customsOfficeService.getFullName(value);
  }
}

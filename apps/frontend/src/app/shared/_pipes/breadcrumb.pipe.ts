/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
/** Angular pipe class for translating breadcrumps. */
@Pipe({ name: 'translateBreadcrumb' })
export class BreadcrumbPipe implements PipeTransform {
  /** Inject the translateService to resolve a breadcrump to the desired language. */
  constructor(private readonly translateService: TranslateService) {}

  /**
   * the main function of the pipe
   * converts an input value to the output value
   * In this case: gets a customs code and return the correct full name
   * @param value input value
   * @returns output value
   */
  transform(value: string): string {
    return this.translateService.instant(value);
  }
}

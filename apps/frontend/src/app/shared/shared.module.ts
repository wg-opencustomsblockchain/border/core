/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { A11yModule } from '@angular/cdk/a11y';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSortModule } from '@angular/material/sort';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { MzdTimelineModule } from 'ngx-mzd-timeline';

import { DetailsTileComponent } from './_components/details-tile/details-tile.component';
import { FilterComponent } from './_components/filter/filter.component';
import { SearchInputComponent } from './_components/search-input/search-input.component';
import { LookupPipe } from './_pipes/lookup.pipe';
import {
  ExportEventPipe,
  ExportEventTranslation,
  ImportEventTranslation,
  ImportStatusPipe,
} from './_pipes/statusLabel.pipe';

import { GeneralInformationComponent } from './_components/_details/gerneral-information/general-information.component';
import { NgxShimmerLoadingModule } from 'ngx-shimmer-loading';
import { CountryCodePipe } from './_pipes/country.pipe';
import { NextActionsComponent } from './_components/_details/next-actions/next-actions.component';
import { EventHistoryComponent } from './_components/_details/event-history/event-history.component';
import { AvatarModule } from 'ngx-avatars';
import { EditorsComponent } from './_components/_details/editors/editors.component';
import { PositionsComponent } from './_components/_details/positions/positions.component';
import { FilesComponent } from './_components/_details/files/files.component';
import { GridToggleButtonComponent } from './_components/grid-toggle-button/grid-toggle-button.component';
import { DetailsViewComponent } from './_components/details-view/details-view.component';
import { DetailsComponent } from './_components/details-list/details.component';
import { ArchiveComponent } from './_components/details-list/archive/archive.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { StatusStepperComponent } from './_components/status-stepper/status-stepper.component';
import { SecurityStatusComponent } from '@shared/_components/security-status/security-status.component';
import { MatSliderModule } from '@angular/material/slider';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
/** List of Material Modules imported from Angular Material  */
const MATERIAL_MODULES = [
  MatIconModule,
  MatCardModule,
  MatToolbarModule,
  MatMenuModule,
  MatButtonModule,
  MatStepperModule,
  MatInputModule,
  MatCheckboxModule,
  MatAutocompleteModule,
  MatTableModule,
  MzdTimelineModule,
  MatPaginatorModule,
  MatSortModule,
  MatDialogModule,
  MatTooltipModule,
  A11yModule,
  MatExpansionModule,
  MatSlideToggleModule,
  MatTabsModule,
  MatDividerModule,
  MatProgressBarModule,
  MatSelectModule,
  MatButtonToggleModule,
  MatStepperModule,
  MatSliderModule,
  MatProgressSpinnerModule,
];

/** List miscellaneous modules to be provided for all components. */
const ANGULAR_SHARED_MODULES = [
  CommonModule,
  ReactiveFormsModule,
  HttpClientModule,
  TranslateModule,
  FormsModule,
  NgxShimmerLoadingModule,
  AvatarModule,
];

const SHARED_COMPONENTS = [
  DetailsComponent,
  ArchiveComponent,
  DetailsViewComponent,
  DetailsTileComponent,
  SearchInputComponent,
  FilterComponent,
  GeneralInformationComponent,
  NextActionsComponent,
  EventHistoryComponent,
  EditorsComponent,
  PositionsComponent,
  FilesComponent,
  GridToggleButtonComponent,
  StatusStepperComponent,
  SecurityStatusComponent,
];

@NgModule({
  declarations: [
    LookupPipe,
    CountryCodePipe,
    ImportStatusPipe,
    ExportEventPipe,
    ImportEventTranslation,
    ExportEventTranslation,
    BreadcrumbComponent,
    ...SHARED_COMPONENTS,
  ],
  imports: [...ANGULAR_SHARED_MODULES, ...MATERIAL_MODULES, RouterModule],
  exports: [
    ...ANGULAR_SHARED_MODULES,
    ...MATERIAL_MODULES,
    LookupPipe,
    CountryCodePipe,
    ImportStatusPipe,
    ExportEventPipe,
    ImportEventTranslation,
    ExportEventTranslation,
    BreadcrumbComponent,
    ...SHARED_COMPONENTS,
  ],
  providers: [
    LookupPipe,
    CountryCodePipe,
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: { displayDefaultIndicatorType: false },
    },
  ],
})

/** Class to bundle Module declaration for components. */
export class SharedModule {}

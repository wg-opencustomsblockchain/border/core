/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { mockEAD } from '@base/src/assets/mockups/EADFile.mock';
import { environment } from '@environments/environment';

import { LightClientService } from '@core/services/light-client.service';
import { WebsocketService } from '@core/services/websocket.service';
import { EADDto, ImportEntity } from '@core/_entities';
import { TokenService } from './token.service';

import { MOCK_USER } from '@base/src/assets/mockups/user.mock';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';

import {
  ExportEventTypes,
  ImportEventTypes,
} from '@core/_enums/event-types.enum';
import {
  TranslateModule,
  TranslatePipe,
  TranslateService,
} from '@ngx-translate/core';
import { LoggerTestingModule } from 'ngx-logger/testing';
import { BorderApi } from '@border/api-interfaces';

describe('EadService', () => {
  let service: TokenService;
  let httpMock: HttpTestingController;
  let lightClient: LightClientService;
  let authService: KeycloakService;

  const borderPrefix = BorderApi.GLOBAL_PREFIX;
  const apiVersionExport = BorderApi.EXPORT_API_VERSION;
  const apiVersionImport = BorderApi.IMPORT_API_VERSION;
  const endPointExport = BorderApi.EXPORT_CONTROLLER;
  const endPointImport = BorderApi.IMPORT_CONTROLLER;
  const exportApi = `${environment.EAD_API_URL}/${borderPrefix}/${apiVersionExport}/${endPointExport}`;
  const importApi = `${environment.EAD_API_URL}/${borderPrefix}/${apiVersionImport}/${endPointImport}`;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        KeycloakAngularModule,
        TranslateModule.forRoot(),
        LoggerTestingModule,
      ],
      providers: [
        {
          translationService: TranslateService,
          translate: TranslatePipe,
          provide: KeycloakService,
          useValue: {
            async loadUserProfile() {
              return Promise.resolve(MOCK_USER[1]);
            },
          },
        },
        { provide: WebsocketService, useClass: WebsocketService },
        {
          provide: LightClientService,
          useClass: LightClientService,
        },
      ],
    });
    service = TestBed.inject(TokenService);
    httpMock = TestBed.inject(HttpTestingController);
    lightClient = TestBed.inject(LightClientService);
    authService = TestBed.inject(KeycloakService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('Backend Microservice', () => {
    it('createExportToken() should call the correct endpoint', () => {
      const result = service.createExportToken([mockEAD.ead]).then((data) => {
        expect(data).toBe(mockEAD.ead);
      });

      httpMock.verify();
    });

    it('getWrittenEAD() should call the correct endpoint and return an array', () => {
      service.getWrittenEAD().subscribe((emp) => {
        expect(emp).toEqual([]);
      });

      const req = httpMock.expectOne(exportApi);
      expect(req.request.method).toEqual('GET');

      req.flush([]);
      httpMock.verify();
    });

    it('getEADFromBlockchain() should call the correct endpoint & return an EAD', () => {
      const mockResponse = mockEAD.ead;
      service.getEADFromBlockchain(mockEAD.ead.exportId).subscribe((emp) => {
        expect(emp).toEqual(mockEAD.ead);
      });
      const req = httpMock.expectOne(exportApi + '/' + mockEAD.ead.exportId);
      expect(req.request.method).toEqual('GET');

      req.flush(mockResponse);
      httpMock.verify();
    });

    it('updateExportToken() should update an Export Token and return it', () => {
      service.createExportToken([mockEAD.ead]).then((data) => {
        expect(data).toBe(mockEAD.ead);
      });
    });

    it('acceptExportToken should accept a new export token and forward it to a new address', () => {
      service.acceptExportToken([mockEAD.ead]).then((data) => {
        expect(data).toBe(mockEAD.ead);
      });
    });

    it('prepareUpdate should prepare an updateMessage', () => {
      service.prepareUpdate([mockEAD.ead]).then((data) => {
        expect(data).toBe(mockEAD.ead);
      });
    });

    it('getEadsOfUser() should call the correct endpoint & return a list of EADs', () => {
      const mockResponse = [mockEAD.ead];
      const user = 'Freddy';
      service.getEadsOfUser(user).subscribe((emp) => {
        expect(emp).toEqual(mockResponse);
      });

      const req = httpMock.expectOne(`${exportApi}/user/${user}`);
      expect(req.request.method).toEqual('GET');

      req.flush(mockResponse);
      httpMock.verify();
    });

    it('getEadsReadyForImport() should call the correct endpoint & return a list of EADs', () => {
      const mockResponse = [mockEAD.ead];
      const user = 'ina';
      service.getEadsReadyForImport(user).subscribe((emp) => {
        expect(emp).toEqual(mockResponse);
      });

      const req = httpMock.expectOne(`${exportApi}/import/${user}`);
      expect(req.request.method).toEqual('GET');

      req.flush(mockResponse);
      httpMock.verify();
    });
  });

  describe('Light Client', () => {
    it('createImportToken() should send a request via light client to create a new import token', async () => {
      jest
        .spyOn(lightClient, 'buildSignAndBroadcast')
        .mockReturnValue(Promise.resolve({} as any));

      service
        .createImportToken([ImportEntity.parseExport(mockEAD.ead)])
        .then((data) => {
          expect(data).toBeDefined();
        });
    });

    it('createExportTokenLocal() should send a request via light client to create a new export token', () => {
      jest
        .spyOn(lightClient, 'buildSignAndBroadcast')
        .mockReturnValue(Promise.resolve({} as any));

      service.createExportTokenLocal([mockEAD.ead]).then((data) => {
        expect(data).toBeDefined();
      });
    });

    it('updateImportTokenLocal() should send a request via light client to update an existing import token', () => {
      jest
        .spyOn(lightClient, 'buildSignAndBroadcast')
        .mockReturnValue(Promise.resolve({} as any));

      service
        .updateImportTokenLocal([ImportEntity.parseExport(mockEAD.ead)])
        .then((data) => {
          expect(data).toBeDefined();
        });
    });

    it('updateExportTokenLocal() should send a request via light client to update an existing export token', () => {
      jest
        .spyOn(lightClient, 'buildSignAndBroadcast')
        .mockReturnValue(Promise.resolve({} as any));

      service
        .updateImportTokenLocal([ImportEntity.parseExport(mockEAD.ead)])
        .then((data) => {
          expect(data).toBeDefined();
        });
    });

    it('updateExportTokenEventLocal() should pick the proper event and send the corresponding event MSG to the Blockchain', async () => {
      jest
        .spyOn(lightClient, 'buildSignAndBroadcast')
        .mockReturnValue(Promise.resolve({} as any));

      const exp: EADDto = JSON.parse(JSON.stringify(mockEAD.ead));

      exp.eventType = ExportEventTypes.EXPORT_READY;
      await service.updateExportTokenEventLocal(exp).then((data) => {
        expect(data).toBeDefined();
      });

      exp.eventType = ExportEventTypes.EXPORT_ACCEPTED;
      await service.updateExportTokenEventLocal(exp).then((data) => {
        expect(data).toBeDefined();
      });

      exp.eventType = ExportEventTypes.EXPORT_ARCHIVED;
      await service.updateExportTokenEventLocal(exp).then((data) => {
        expect(data).toBeDefined();
      });

      exp.eventType = ExportEventTypes.EXPORT_CHECK;
      await service.updateExportTokenEventLocal(exp).then((data) => {
        expect(data).toBeDefined();
      });

      exp.eventType = ExportEventTypes.EXPORT_EXIT;
      await service.updateExportTokenEventLocal(exp).then((data) => {
        expect(data).toBeDefined();
      });

      exp.eventType = ExportEventTypes.EXPORT_PICKUP;
      await service.updateExportTokenEventLocal(exp).then((data) => {
        expect(data).toBeDefined();
      });

      exp.eventType = ExportEventTypes.EXPORT_PRESENTED;
      await service.updateExportTokenEventLocal(exp).then((data) => {
        expect(data).toBeDefined();
      });

      exp.eventType = ExportEventTypes.EXPORT_REFUSAL;
      await service.updateExportTokenEventLocal(exp).then((data) => {
        expect(data).toBeDefined();
      });
    });

    it('updateImportTokenEventLocal() should pick the proper event and send the corresponding event MSG to the Blockchain', async () => {
      jest
        .spyOn(lightClient, 'buildSignAndBroadcast')
        .mockReturnValue(Promise.resolve({} as any));
      const im: ImportEntity = ImportEntity.parseExport(mockEAD.ead);

      im.eventType = ImportEventTypes.IMPORT_ACCEPTED;
      await service.updateImportTokenEventLocal(im).then((data) => {
        expect(data).toBeDefined();
      });

      im.eventType = ImportEventTypes.IMPORT_COMPLETED;
      await service.updateImportTokenEventLocal(im).then((data) => {
        expect(data).toBeDefined();
      });
      im.eventType = ImportEventTypes.IMPORT_EXPORTED;
      await service.updateImportTokenEventLocal(im).then((data) => {
        expect(data).toBeDefined();
      });
      im.eventType = ImportEventTypes.IMPORT_PROPOSAL;
      await service.updateImportTokenEventLocal(im).then((data) => {
        expect(data).toBeDefined();
      });
      im.eventType = ImportEventTypes.IMPORT_REFUSED;
      await service.updateImportTokenEventLocal(im).then((data) => {
        expect(data).toBeDefined();
      });
      im.eventType = ImportEventTypes.IMPORT_RELEASE;
      await service.updateImportTokenEventLocal(im).then((data) => {
        expect(data).toBeDefined();
      });
      im.eventType = ImportEventTypes.IMPORT_SUBMITTED;
      await service.updateImportTokenEventLocal(im).then((data) => {
        expect(data).toBeDefined();
      });
      im.eventType = ImportEventTypes.IMPORT_SUPPLEMENT;
      await service.updateImportTokenEventLocal(im).then((data) => {
        expect(data).toBeDefined();
      });
    });
  });
});

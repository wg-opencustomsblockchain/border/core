/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BorderApi, User } from '@border/api-interfaces';
import { environment } from '@environments/environment';

import { KeycloakService } from 'keycloak-angular';
import { firstValueFrom, Observable } from 'rxjs';

import {
  MsgCreateExportEvent,
  MsgCreateExportToken,
  MsgCreateImportToken,
  MsgEventAcceptExport,
  MsgEventAcceptGoodsForTransport,
  MsgEventAcceptImport,
  MsgEventCertifyExitOfGoods,
  MsgEventCompleteImport,
  MsgEventExportCheck,
  MsgEventExportRefusal,
  MsgEventImportExport,
  MsgEventImportProposal,
  MsgEventImportRefusal,
  MsgEventImportRelease,
  MsgEventImportSubmit,
  MsgEventImportSupplement,
  MsgEventMoveToArchive,
  MsgEventPresentGoods,
  MsgEventPresentQrCode,
  MsgForwardExportToken,
  MsgUpdateImportToken,
} from '@border/api-interfaces/lib/client/org.borderblockchain.businesslogic/module';
import { TranslateService } from '@ngx-translate/core';
import { LightClientService } from '../../core/services/light-client.service';
import { EADDto, ImportEntity } from '../../core/_entities';
import {
  ExportEventTypes,
  ImportEventTypes,
} from '../../core/_enums/event-types.enum';
import { TxMessages } from '../../core/_enums/tx-messages';

/**
 * A service for handling export and import token requests and write operations
 */
@Injectable({
  providedIn: 'root',
})
export class TokenService {
  updateMessage = 'MESSAGE PLACEHOLDER';
  eventType = 'TOKEN_UPDATE';
  userWallet: string;
  userMnemonic: string;

  borderPrefix = BorderApi.GLOBAL_PREFIX;

  apiVersionExport = BorderApi.EXPORT_API_VERSION;
  apiVersionImport = BorderApi.IMPORT_API_VERSION;
  endPointExport = BorderApi.EXPORT_CONTROLLER;
  endPointImport = BorderApi.IMPORT_CONTROLLER;
  exportApi = `${environment.EAD_API_URL}/${this.borderPrefix}/${this.apiVersionExport}/${this.endPointExport}`;
  importApi = `${environment.EAD_API_URL}/${this.borderPrefix}/${this.apiVersionImport}/${this.endPointImport}`;

  /**
   * Creates an instance if ead service
   * @param http injects http client
   * @param authService injects auth service
   * @param lightClient injects light client
   */
  constructor(
    private readonly http: HttpClient,
    private readonly authService: KeycloakService,
    private readonly lightClient: LightClientService,
    private readonly translateService: TranslateService
  ) {}

  /**
   * Load written eads from the blockchain
   * @returns observable list of ead files
   */
  getWrittenEAD(): Observable<EADDto[]> {
    return this.http.get<Array<EADDto>>(this.exportApi);
  }

  /**
   * Write an ead to the blockchain via the Blockchain Connector.
   * @returns observable list of ead files
   */
  async createExportToken(ead: EADDto[]): Promise<any> {
    const user: User = await this.authService.loadUserProfile();
    ead.forEach((element) => {
      element.creator = user.attributes.wallet[0];
      element.user = user.username;
    });
    return firstValueFrom(this.http.post(`${this.exportApi}`, ead));
  }

  /**
   * Get an ead with the specified mrn from the blockchain
   * @param id uuid of the token
   * @returns observable list of ead files
   */
  getEADFromBlockchain(id: string): Observable<EADDto> {
    return this.http.get<EADDto>(`${this.exportApi}/${id}`);
  }

  /**
   * Loads a list of eads associated with the given user from the blockchain
   * @param user username
   * @returns observable list of ead files
   */
  getEadsOfUser(user: string): Observable<EADDto[]> {
    return this.http.get<EADDto[]>(
      `${this.exportApi}/${BorderApi.EXPORT_GET_USER}/${user}`
    );
  }

  /**
   * Loads the eads, that are ready for import with the specified user from the blockchain
   * @param user username
   * @returns observable list of ead files
   */
  getEadsReadyForImport(user: string): Observable<EADDto[]> {
    return this.http.get<EADDto[]>(
      `${this.exportApi}/${BorderApi.EXPORT_GET_IMPORT_READY}/${user}`
    );
  }

  /**
   * Loads all eads with the status archived from the blockchain
   * @returns observable list of ead files
   */
  getArchivedEads(): Observable<EADDto[]> {
    return this.http.get<EADDto[]>(
      `${this.exportApi}/${BorderApi.EXPORT_GET_ARCHIVED}`
    );
  }

  /**
   * Write an ead to the blockchain via the Blockchain Connector.
   * @returns observable list of ead files
   */
  async createImportToken(im: ImportEntity[]): Promise<any> {
    const user: User = await this.authService.loadUserProfile();
    im.forEach((element) => {
      element.creator = user.attributes.wallet[0];
      element.user = user.username;
    });

    return firstValueFrom(this.http.post(`${this.importApi}`, im));
  }

  /**
   * Update an ead by adding a new event via the Blockchain Connector
   * @returns observable list of ead files
   */
  async updateImportToken(im: ImportEntity[]): Promise<any> {
    const user: User = await this.authService.loadUserProfile();
    im.forEach((element) => {
      element.creator = user.attributes.wallet[0];
      element.user = user.username;
    });
    return firstValueFrom(
      this.http.put(`${this.importApi}/${BorderApi.IMPORT_UPDATE}`, im)
    );
  }

  /**
   * Creates an export token on the blockchain via the local Light Client.
   * @param ex The export token to be saved.
   * @returns A parsed version of the export token as an EADDto.
   */
  async createExportTokenLocal(ex: EADDto[]): Promise<EADDto> {
    for (const token of ex) {
      token.eventType = ExportEventTypes.EXPORT_RECORDED;
    }
    const user: User = await this.authService.loadUserProfile();

    const msgArray: MsgCreateExportToken[] = await this.prepareExportToken(ex);
    return this.lightClient.buildSignAndBroadcast(
      user.attributes.mnemonic[0],
      msgArray,
      'MsgCreateExportToken'
    );
  }

  /**
   * Update an ead by adding a new event via the Blockchain Connector
   * @returns observable list of ead files
   */
  async updateExportToken(ex: EADDto[]): Promise<any> {
    const user: User = (await this.authService.loadUserProfile()) as User;
    ex.forEach((element) => {
      element.creator = user.attributes.wallet[0];
      element.user = user.username;
    });
    return firstValueFrom(
      this.http.put(`${this.exportApi}/${BorderApi.EXPORT_UPDATE}`, ex)
    );
  }

  /**
   * Adds the necessary additional info required to persist a list of EAD to the blockchain.
   * @param token a list of EAD to be tokenized.
   * @returns A list of messages which can be consumed by the Cosmos/Tendermint API.
   */
  private async prepareExportToken(token: any[]): Promise<any[]> {
    const msgArray: any = [];
    const user: User = await this.authService.loadUserProfile();
    for (const content of token) {
      content.creator = user.attributes.wallet[0];

      content.tokenWalletId = user.attributes.tokenWalletId[0];
      const input: MsgCreateExportToken = content;
      msgArray.push(input);
    }
    return msgArray;
  }

  /**
   * Writes a new import token to the blockchain.
   * @param im Importtoken to be updated.
   * @returns The blockchain response to the create Message. String representation of the newly updated import token.
   */
  async updateImportTokenLocal(im: ImportEntity[]): Promise<string> {
    const msgUpdateImportToken: MsgUpdateImportToken[] =
      await this.prepareImportToken(im);

    const user: User = await this.authService.loadUserProfile();

    return this.lightClient.buildSignAndBroadcast(
      user.attributes.mnemonic[0],
      msgUpdateImportToken,
      'MsgUpdateImportToken'
    );
  }

  /**
   * Appends an update to the export token on the blockchain.
   * @param ex The export token to be updated.
   * @returns A parsed version of the newly updated export token as an EADDto.
   * MsgEventPresentQrCode
   */
  async updateExportTokenEventLocal(ex: EADDto): Promise<EADDto> {
    const user: User = await this.authService.loadUserProfile();

    const tmpArr: any = [
      { creator: user.attributes.wallet[0], id: ex.id, message: 'Message' },
    ];
    let msgArray;
    let msgEvent;

    switch (ex.eventType) {
      case ExportEventTypes.EXPORT_READY: {
        msgArray = tmpArr as MsgEventPresentQrCode;
        msgEvent = TxMessages.MsgEventPresentQrCode as string;
        break;
      }
      case ExportEventTypes.EXPORT_PICKUP: {
        msgArray = tmpArr as MsgEventAcceptGoodsForTransport[];
        msgEvent = TxMessages.MsgEventAcceptGoodsForTransport as string;
        break;
      }
      case ExportEventTypes.EXPORT_PRESENTED: {
        msgArray = tmpArr as MsgEventPresentGoods[];
        msgEvent = TxMessages.MsgEventPresentGoods as string;
        break;
      }
      case ExportEventTypes.EXPORT_EXIT: {
        msgArray = tmpArr as MsgEventCertifyExitOfGoods[];
        msgEvent = TxMessages.MsgEventCertifyExitOfGoods as string;
        break;
      }
      case ExportEventTypes.EXPORT_ARCHIVED: {
        msgArray = tmpArr as MsgEventMoveToArchive[];
        msgEvent = TxMessages.MsgEventMoveToArchive as string;
        break;
      }
      case ExportEventTypes.EXPORT_MOBILECHECK:
      case ExportEventTypes.EXPORT_CHECK: {
        msgArray = tmpArr as MsgEventExportCheck[];
        msgEvent = TxMessages.MsgEventExportCheck as string;
        break;
      }

      case ExportEventTypes.EXPORT_REFUSAL: {
        msgArray = tmpArr as MsgEventExportRefusal[];
        msgEvent = TxMessages.MsgEventExportRefusal as string;
        break;
      }

      case ExportEventTypes.EXPORT_ACCEPTED: {
        msgArray = tmpArr as MsgEventAcceptExport[];
        msgEvent = TxMessages.MsgEventAcceptExport as string;
        break;
      }
      default: {
        throw new Error(this.translateService.instant('blockchain.error.19'));
      }
    }

    return this.lightClient.buildSignAndBroadcast(
      user.attributes.mnemonic[0],
      msgArray,
      msgEvent
    );
  }

  /**
   * Appends an update to the export token on the blockchain.
   * @param ex The export token to be updated.
   * @returns A parsed version of the newly updated export token as an EADDto.
   * MsgEventPresentQrCode
   */
  async updateImportTokenEventLocal(im: ImportEntity): Promise<ImportEntity> {
    const user: User = await this.authService.loadUserProfile();

    const tmpArr: any = [
      { creator: user.attributes.wallet[0], id: im.id, message: 'Message' },
    ];
    let msgArray;
    let msgEvent;

    switch (im.eventType) {
      case ImportEventTypes.IMPORT_ACCEPTED: {
        msgArray = tmpArr as MsgEventAcceptImport[];
        msgEvent = TxMessages.MsgEventAcceptImport as string;
        break;
      }
      case ImportEventTypes.IMPORT_COMPLETED: {
        msgArray = tmpArr as MsgEventCompleteImport[];
        msgEvent = TxMessages.MsgEventCompleteImport as string;
        break;
      }
      case ImportEventTypes.IMPORT_PROPOSAL: {
        msgArray = tmpArr as MsgEventImportProposal[];
        msgEvent = TxMessages.MsgEventImportProposal as string;
        break;
      }
      case ImportEventTypes.IMPORT_REFUSED: {
        msgArray = tmpArr as MsgEventImportRefusal[];
        msgEvent = TxMessages.MsgEventImportRefusal as string;
        break;
      }
      case ImportEventTypes.IMPORT_SUPPLEMENT: {
        msgArray = tmpArr as MsgEventImportSupplement[];
        msgEvent = TxMessages.MsgEventImportSupplement as string;
        break;
      }
      case ImportEventTypes.IMPORT_EXPORTED: {
        msgArray = tmpArr as MsgEventImportExport[];
        msgEvent = TxMessages.MsgEventImportExport as string;
        break;
      }
      case ImportEventTypes.IMPORT_SUBMITTED: {
        msgArray = tmpArr as MsgEventImportSubmit[];
        msgEvent = TxMessages.MsgEventImportSubmit as string;
        break;
      }
      case ImportEventTypes.IMPORT_RELEASE: {
        msgArray = tmpArr as MsgEventImportRelease[];
        msgEvent = TxMessages.MsgEventImportRelease as string;
        break;
      }
      default: {
        throw new Error(this.translateService.instant('blockchain.error.19'));
      }
    }

    return this.lightClient.buildSignAndBroadcast(
      user.attributes.mnemonic[0],
      msgArray,
      msgEvent
    );
  }

  /**
   * Sets the status from NEW to CHECKED. Used when a new EAD is being imported and accepted into an Export officials wallet.
   * @param ex the EAD to be accepted
   * @returns
   */
  async acceptExportToken(ex: EADDto[]): Promise<EADDto> {
    const msgArray: MsgForwardExportToken[] = await this.prepareUpdate(ex);

    const user: User = await this.authService.loadUserProfile();
    return this.lightClient.buildSignAndBroadcast(
      user.attributes.mnemonic[0],
      msgArray,
      'MsgForwardExportToken'
    );
  }

  /**
   * Adds the necessary additional info required to persist a list of imports to the blockchain.
   * @param token a list of imports to be tokenized.
   * @returns A list of messages which can be consumed by the Cosmos/Tendermint API.
   */
  private async prepareImportToken(token: any[]): Promise<any[]> {
    const msgArray: any = [];
    const user: User = await this.authService.loadUserProfile();
    for (const content of token) {
      content.creator = user.attributes.wallet[0];
      content.tokenWalletId = user.attributes.tokenWalletId[0];
      const input: MsgCreateImportToken = content;
      msgArray.push(input);
    }
    return msgArray;
  }

  /**
   * Adds the necessary additional info required to append events to their corresponding export token.
   * At this point limited for export events.
   * @param token a list events to be tokenized.
   * @returns A list of messages which can be consumed by the Cosmos/Tendermint API.
   */
  private async prepareEvent(token: any[]): Promise<any[]> {
    const eventList: MsgCreateExportEvent[] = [];
    const user: User = await this.authService.loadUserProfile();
    for (const content of token) {
      const msgCreateExportEvent: MsgCreateExportEvent = {
        creator: user.attributes.wallet[0],
        message: this.updateMessage,
        eventType: content.eventType,
        index: content.id,
        status: content.status,
      };
      eventList.push(msgCreateExportEvent);
    }
    return eventList;
  }

  async prepareUpdate(token: EADDto[]): Promise<MsgForwardExportToken[]> {
    const eventList: MsgForwardExportToken[] = [];
    const user: User = await this.authService.loadUserProfile();
    for (const content of token) {
      const msgForwardExportEvent: MsgForwardExportToken = {
        creator: user.attributes.wallet[0],
        id: content.id,
      };
      eventList.push(msgForwardExportEvent);
    }
    return eventList;
  }
}

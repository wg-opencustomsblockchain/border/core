/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { mockEAD } from '@base/src/assets/mockups/EADFile.mock';

import { LightClientService } from '@core/services/light-client.service';
import { WebsocketService } from '@core/services/websocket.service';

import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import { MOCK_USER } from '@base/src/assets/mockups/user.mock';
import { TranslateModule, TranslatePipe, TranslateService } from '@ngx-translate/core';
import { ExportService } from './export.service';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { TokenService } from './token.service';
import { EMPTY, of } from 'rxjs';
import { ExportEventTypes } from '../../core/_enums/event-types.enum';
import { LoggerTestingModule } from 'ngx-logger/testing';

describe('ExportService', () => {
  let service: ExportService;
  let tokenService: TokenService;
  let dialog: MatDialog;
  let toastr: ToastrService;
  let translateService: TranslateService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        KeycloakAngularModule,
        TranslateModule.forRoot(),
        MatDialogModule,
        ToastrModule.forRoot(),
        LoggerTestingModule,
      ],
      providers: [
        {
          translationService: TranslateService,
          translate: TranslatePipe,
          provide: KeycloakService,
          useValue: {
            async loadUserProfile() {
              return Promise.resolve(MOCK_USER[1]);
            },
          },
        },
        TokenService,
        { provide: ToastrService, useClass: ToastrService },
        { provide: WebsocketService, useClass: WebsocketService },
        {
          provide: LightClientService,
          useClass: LightClientService,
        },
      ],
    });
    service = TestBed.inject(ExportService);
    tokenService = TestBed.inject(TokenService);
    dialog = TestBed.inject(MatDialog);
    toastr = TestBed.inject(ToastrService);
    translateService = TestBed.inject(TranslateService);
  });

  describe('methods', () => {
    it('should be created', () => {
      expect(service).toBeTruthy();
    });

    it('should getEadById', () => {
      const testId = mockEAD.ead.id;

      jest.spyOn(tokenService, 'getEADFromBlockchain').mockReturnValue(of(mockEAD.ead));

      service.getEadById(testId);

      expect(service.exportToken.getValue()).toBe(mockEAD.ead);
    });

    it('the dataStream should get filled with the given information', () => {
      const openDialogSpy = jest.spyOn(dialog, 'open').mockReturnValue({
        afterClosed: () => EMPTY,
      } as any);
      service.showQRCode(mockEAD.ead);
      expect(openDialogSpy).toHaveBeenCalled();
    });

    it('handeEADUpdate should call the correct functions with the correct ead', async () => {
      const eadServiceSpy = jest
        .spyOn(tokenService, 'updateExportTokenEventLocal')
        .mockReturnValue(Promise.resolve(mockEAD.ead));
      const toastrSpySuccess = jest.spyOn(toastr, 'success');
      const toastrSpyWarning = jest.spyOn(toastr, 'warning');
      await service.handleEADUpdate(mockEAD.ead).then();

      expect(eadServiceSpy).toHaveBeenCalledWith(mockEAD.ead);
      expect(toastrSpySuccess).toHaveBeenCalledTimes(1);

      eadServiceSpy.mockReturnValue(Promise.reject(mockEAD.ead));
      service.handleEADUpdate(mockEAD.ead);
      expect(eadServiceSpy).toHaveBeenCalledWith(mockEAD.ead);
      expect(toastrSpyWarning).toHaveBeenCalledTimes(0);
    });

    it('setStatus should call the correct functions with the correct ead', async () => {
      const openDialogSpy = jest.spyOn(dialog, 'open').mockReturnValue({
        afterClosed: () => EMPTY,
      } as any);
      service.setStatus({ eadDto: mockEAD.ead, status: ExportEventTypes.EXPORT_PICKUP });
      expect(openDialogSpy).toHaveBeenCalled();
    });

    it('editExport() should call toastr with transalate service', () => {
      mockEAD.ead.status = '5';
      const translateSpy = jest.spyOn(translateService, 'instant');
      const toastrSpy = jest.spyOn(toastr, 'error');
      jest.spyOn(dialog, 'open').mockReturnValue({
        afterClosed: () => of(mockEAD.ead),
      } as any);
      service.editExport(mockEAD.ead);
      expect(toastrSpy).toHaveBeenCalled();
      expect(translateSpy).toHaveBeenCalledWith('dialog.confirm.edit.error');
    });
  });
});

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

import { Router } from '@angular/router';
import { EditExportDialogComponent } from '@core/_components/edit-export-dialog/edit-export-dialog.component';
import { LoadingComponent } from '@core/_components/loading/loading.component';
import { EADDto, ImportEntity } from '@core/_entities';
import { ExportEventTypes } from '@core/_enums/event-types.enum';
import { DialogConfirmWriteComponent } from '@core/_pages/release/release.component';
import { TranslateService } from '@ngx-translate/core';
import { TokenService } from '@shared/services/token.service';
import { ToastrService } from 'ngx-toastr';
import {
  BehaviorSubject,
  filter,
  finalize,
  map,
  Observable,
  switchMap,
  tap,
} from 'rxjs';
import { CustomsOfficeService } from '../../core/services/customs-office.service';
import { ExportStatus } from '../../core/_enums/status.enum';

@Injectable({
  providedIn: 'root',
})
export class ExportService {
  dialogRef: MatDialogRef<DialogConfirmWriteComponent> | null = null;
  public exportToken: BehaviorSubject<EADDto> = new BehaviorSubject(null);

  constructor(
    private readonly tokenService: TokenService,
    private readonly translateService: TranslateService,
    private readonly dialog: MatDialog,
    private readonly router: Router,
    private readonly toastr: ToastrService,
    private readonly officeService: CustomsOfficeService
  ) {}

  getEadById(id: string): void {
    this.exportToken.next(null);
    this.tokenService.getEADFromBlockchain(id).subscribe((res) => {
      const { name, city, countryCode, postalCode, streetAndNumber } =
        this.officeService.getFullInformation(
          res.customOfficeOfExit.customsOfficeCode
        );
      res.customOfficeOfExit.address = {
        streetAndNumber,
        city,
        countryCode,
        postcode: postalCode,
      };
      res.customOfficeOfExit.customsOfficeName = name;
      this.exportToken.next(res);
    });
  }

  /**
   * Show the QRCode for the selected element after confirmation
   * @param element the selected element
   */
  showQRCode(element: EADDto): void {
    this.openConfirmDialog(
      this.translateService.instant('dialog.confirm.barcode.show'),
      [element]
    );

    this.dialogRef
      .afterClosed()
      .pipe(
        filter((result: boolean) => result),
        tap(() => {
          element.eventType = ExportEventTypes.EXPORT_READY;
          this.handleEADUpdate(element);
          this.router.navigate([`/barcode`], {
            queryParams: { uuid: element.id },
          });
        })
      )
      .subscribe();
  }

  /**
   * Update the status of a given ead
   * @param ead the ead
   */
  async handleEADUpdate(ead: EADDto): Promise<void> {
    this.tokenService
      .updateExportTokenEventLocal(ead)
      .then(() =>
        this.toastr.success(
          this.translateService.instant('toaster.update.success')
        )
      )
      .catch((result: Error) =>
        this.toastr.error(this.translateService.instant(result.message), '', {
          disableTimeOut: true,
        })
      )
      .finally(() => {
        this.dialog.closeAll();
      });
  }

  acceptExportToken(ead: EADDto): Observable<boolean> {
    const dialogRef = this.dialog.open(DialogConfirmWriteComponent, {
      data: {
        eads: [ead],
        text: this.translateService.instant('dialog.confirm.write.text'),
      },
      width: '80vw',
    });

    return dialogRef.afterClosed().pipe(
      map((result) => {
        if (result) {
          this.dialog.open(LoadingComponent, {
            data: {
              heading: this.translateService.instant(
                'loading.component.heading'
              ),
              text: this.translateService.instant('loading.component.text'),
            },
            width: '80vw',
            disableClose: true,
          });

          this.tokenService
            .acceptExportToken([ead])
            .then(() => {
              this.toastr.success(
                `1 ${this.translateService.instant('release.write.multiple')}`
              );
              this.dialog.closeAll();
              this.getEadById(ead.id);
            })
            .catch(() => {
              this.dialog.closeAll();
              this.toastr.error(
                this.translateService.instant('error.write-failed')
              );
            });
          return true;
        }
        return false;
      })
    );
  }

  /**
   * Moves the selected element to the archive after user confirmation
   * @param element the selected element
   */
  setStatus(event: {
    eadDto: EADDto;
    status: ExportEventTypes;
  }): Observable<boolean> {
    this.openConfirmDialog(
      this.translateService.instant('dialog.confirm.status'),
      [event.eadDto]
    );
    event.eadDto.eventType = event.status;

    return this.dialogRef.afterClosed().pipe(
      map((result: boolean) => {
        if (result) {
          this.handleEADUpdate(event.eadDto);
          return true;
        }
        return false;
      })
    );
  }

  /**
   * Currently views export data in the details view.
   * @param ead export token
   */
  editExport(ead: EADDto): void {
    const editExportDialog = this.dialog.open(EditExportDialogComponent, {
      data: {
        export: ead,
      },
      width: '85vw',
      height: '85vh',
    });

    editExportDialog.afterClosed().subscribe((closeData: EADDto) => {
      if (!closeData) return;
      if (closeData.status !== '0') {
        this.toastr.error(
          this.translateService.instant('dialog.confirm.edit.error')
        );
        return;
      }
      this.openConfirmDialog(
        this.translateService.instant('dialog.confirm.edit.text'),
        [ead]
      );

      this.dialogRef.afterClosed().subscribe((result) => {
        if (!result) {
          this.dialog.closeAll();
          this.getEadById(ead.id);
          return;
        }
        this.openLoadingDialog();
        this.tokenService
          .updateExportToken([closeData])
          .then(() => {
            this.toastr.success(
              this.translateService.instant('toaster.update.success')
            );
            this.dialog.closeAll();
          })
          .catch(() => {
            this.toastr.error(
              this.translateService.instant('toaster.update.failure')
            );
            this.dialog.closeAll();
          });
      });
    });
  }

  acceptImport(ead: EADDto) {
    this.openConfirmDialog(
      this.translateService.instant('dialog.confirm.accept.text.import'),
      [ead]
    );

    return this.dialogRef.afterClosed().pipe(
      filter((result: boolean) => result),
      tap(() => this.openLoadingDialog()),
      switchMap(() => {
        return this.tokenService.createImportToken([
          ImportEntity.parseExport(ead),
        ]);
      }),
      finalize(() => this.dialog.closeAll())
    );
  }

  declineImport(ead: EADDto) {
    this.openConfirmDialog(
      this.translateService.instant('dialog.confirm.refuse.text.import'),
      [ead]
    );

    return this.dialogRef.afterClosed().pipe(
      filter((result: boolean) => result),
      tap(() => {
        ead.eventType = ExportEventTypes.EXPORT_REFUSAL;
        ead.status = ExportStatus.DECLINED;
        return this.handleEADUpdate(ead);
      }),
      finalize(() => this.dialog.closeAll())
    );
  }

  /**
   * Shows the loading popup
   */
  private openLoadingDialog(): void {
    this.dialog.open(LoadingComponent, {
      data: {
        heading: this.translateService.instant('loading.component.heading'),
        text: this.translateService.instant('loading.component.heading'),
      },
      width: '80vw',
      disableClose: true,
    });
  }

  /**
   * Creates and opens the confirm dialog with the given values
   * @param text The text which should be displayed
   * @param eads the eads that should be displayed
   */
  private openConfirmDialog(text: string, eads: EADDto[]): void {
    this.dialogRef = this.dialog.open(DialogConfirmWriteComponent, {
      data: {
        eads,
        text,
      },
      width: '80vw',
    });
  }
}

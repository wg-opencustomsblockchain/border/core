/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * Utility class to format currencies.
 * Depends on the internationally used currency library INTL.
 */
export class CurrencyUtility {
  /**
   * Turns a number into a currency format for display.
   * We Expect all data handled inside the application to be on its lowest denominator.
   * Performs no operation if no currency type is provided.
   * @param locale The locale to work in e.g. "en-US/en-GB/en-IN/de-DE/ja-JP"
   * @param currency The currency to display, e.g. "EUR/USD/JPY/GBP". Based on ISO norms.
   * @param amount The amount as an integer with the lowest possible denomination. Cents for EUR/GBP/USD, or Yen as is.
   */
  public getCurrencyFormatString(locale: string, currency: string, amount: number): string {
    // Grab the case where we try to format without a currency or non supported currencies.
    if (currency in SUPPORTED_CURRENCIES) {
      const formatter = new Intl.NumberFormat(locale, {
        style: 'currency',
        currency: currency,
      });

      amount = this.toMajorDenom(currency, amount);
      return formatter.format(amount);
    } else {
      return this.toMajorDenom(currency, amount).toString();
    }
  }

  /**
   * Number handling for MajorUnit declarations of currencies.
   * Turns a Cent value ('1286'Cents) into a Major version for display and Export (e.g '12.86'Euro).
   * @param currency The currency to do the calculation with.
   * @param amount The amount to be parsed into the currency's major units.
   */
  public toMajorDenom(currency: string, amount: number): number {
    switch (currency) {
      case SUPPORTED_CURRENCIES.EUR:
      case SUPPORTED_CURRENCIES.USD:
      case SUPPORTED_CURRENCIES.GBP:
        amount = amount / 100;
        break;
      // YEN and other currencies without a major denom may stay as is.
      default:
    }
    // Basic rounding function to remove all decimals after 2.
    return Math.round((amount + Number.EPSILON) * 100) / 100;
  }

  /**
   * Number handling for MajorUnit declarations of currencies.
   * Turns a Euro value (e.g '12.86'Euro) into a cent only version for internal handling ('1286'Cents).
   * @param currency The currency to do the calculation with.
   * @param amount The amount to be parsed into the currency's major units.
   */
  public toMinorDenom(currency: string, amount: number): number {
    switch (currency) {
      case SUPPORTED_CURRENCIES.EUR:
      case SUPPORTED_CURRENCIES.USD:
      case SUPPORTED_CURRENCIES.GBP:
        amount = amount * 100;
        break;
      // YEN and other currencies without a major denom may stay as is.
      default:
    }
    // Basic rounding function to remove all decimals after 2.
    return Math.round((+amount + Number.EPSILON) * 100) / 100;
  }
}

/**
 * A list of a select few supported currencies.
 * There is a list of currencies readily available which is also used by international Frameworks.
 * If requried we should make use of it.
 * https://www.six-group.com/dam/download/financial-information/data-center/iso-currrency/lists/list_one.xml
 */
export enum SUPPORTED_CURRENCIES {
  EUR = 'EUR',
  USD = 'USD',
  GBP = 'GBP',
  JPY = 'JPY',
}

export enum SUPPORTED_LOCALES {
  DEFAULT = 'en-IN',
  GERMANY = 'de-DE',
  USA = 'en-US',
  UK = 'en-GB',
  JAPAN = 'jp-JP',
}

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { CurrencyUtility, SUPPORTED_CURRENCIES, SUPPORTED_LOCALES } from './utils.currency';

describe('CurrencyUtility', () => {
  const currUtil = new CurrencyUtility();

  const currValueMajorDef = 15000.21;
  const currValueMajorYEN = 1500021;
  const currValueMinor = 1500021;

  // Compare to a string and make sure it includes a non breaking space.

  const poundString = '£15,000.21';
  const usdString = '$15,000.21';
  const defaultString = '1500021';

  it('should be created', () => {
    expect(currUtil).toBeTruthy();
  });

  it('should transform an amount into a correct currency string, which conforms to the locale they are from.', () => {
    /* expect(
      currUtil.getCurrencyFormatString(SUPPORTED_LOCALES.GERMANY, SUPPORTED_CURRENCIES.EUR, currValueMinor)
    ).toEqual(euroString);*/

    expect(currUtil.getCurrencyFormatString(SUPPORTED_LOCALES.UK, SUPPORTED_CURRENCIES.GBP, currValueMinor)).toEqual(
      poundString
    );

    expect(currUtil.getCurrencyFormatString(SUPPORTED_LOCALES.USA, SUPPORTED_CURRENCIES.USD, currValueMinor)).toEqual(
      usdString
    );

    /* expect(currUtil.getCurrencyFormatString(SUPPORTED_LOCALES.JAPAN, SUPPORTED_CURRENCIES.JPY, currValueMinor)).toEqual(
      yenString
    ); */

    expect(currUtil.getCurrencyFormatString(SUPPORTED_LOCALES.DEFAULT, 'NOT SUPPORTED', currValueMinor)).toEqual(
      defaultString
    );
  });

  it('Should properly transform back from minor denomination into a major', () => {
    expect(currUtil.toMajorDenom(SUPPORTED_CURRENCIES.EUR, currValueMinor)).toEqual(currValueMajorDef);
    expect(currUtil.toMajorDenom(SUPPORTED_CURRENCIES.USD, currValueMinor)).toEqual(currValueMajorDef);
    expect(currUtil.toMajorDenom(SUPPORTED_CURRENCIES.GBP, currValueMinor)).toEqual(currValueMajorDef);
    expect(currUtil.toMajorDenom(SUPPORTED_CURRENCIES.JPY, currValueMinor)).toEqual(currValueMajorYEN);

    // A not supported Currency remains as it is for now.
    expect(currUtil.toMajorDenom('NOT SUPPORTED CURRENCY', currValueMinor)).toEqual(currValueMajorYEN);
  });

  it('Should properly transform back from major denomination into a minor', () => {
    expect(currUtil.toMinorDenom(SUPPORTED_CURRENCIES.EUR, currValueMajorDef)).toEqual(currValueMinor);
    expect(currUtil.toMinorDenom(SUPPORTED_CURRENCIES.USD, currValueMajorDef)).toEqual(currValueMinor);
    expect(currUtil.toMinorDenom(SUPPORTED_CURRENCIES.GBP, currValueMajorDef)).toEqual(currValueMinor);
    expect(currUtil.toMinorDenom(SUPPORTED_CURRENCIES.JPY, currValueMajorYEN)).toEqual(currValueMinor);

    // A not supported Currency remains as it is for now.
    expect(currUtil.toMinorDenom('NOT SUPPORTED CURRENCY', currValueMajorDef)).toEqual(currValueMajorDef);
  });
});

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { UserRole } from '@border/api-interfaces';

export const NavigationItems = [
  {
    title: 'toolbar.HOME',
    icon: 'home',
    link: '/selection',
    roles: [UserRole.EXPORTER, UserRole.IMPORTER, UserRole.DRIVER],
  },
  {
    title: 'toolbar.RELEASE',
    icon: 'verified',
    link: '/release',
    roles: [UserRole.EXPORTER],
  },
  {
    title: 'toolbar.DETAILS',
    icon: 'grid_view',
    link: '/exports/details',
    roles: [UserRole.EXPORTER],
  },
  {
    title: 'toolbar.BARCODE',
    icon: 'qr_code',
    link: '/barcode/generate',
    roles: [UserRole.EXPORTER],
  },
  {
    title: 'toolbar.ARCHIVE',
    icon: 'archive',
    link: '/archive',
    roles: [UserRole.EXPORTER],
  },
  {
    title: 'toolbar.NEW_IMPORT',
    icon: 'archive',
    link: '/imports/new',
    roles: [UserRole.IMPORTER],
  },
  {
    title: 'toolbar.ACTIVE_IMPORT',
    icon: 'archive',
    link: '/imports/active',
    roles: [UserRole.IMPORTER],
  },
  {
    title: 'home.barcode.scan',
    icon: 'qr_code_scanner',
    link: '/camera',
    roles: [UserRole.DRIVER],
  },
  {
    title: 'home.button.mrn.show',
    icon: 'preview',
    link: '/details-mobile',
    roles: [UserRole.DRIVER],
  },
  {
    title: 'home.code.input',
    icon: 'input',
    link: '/code-input',
    roles: [UserRole.DRIVER],
  },
];

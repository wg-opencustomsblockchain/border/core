/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { EADDto } from './ead.entity';

/** EADFile entity class to handle EADFile info. */
export class EADFile {
  filename!: string;
  ead!: EADDto;
}

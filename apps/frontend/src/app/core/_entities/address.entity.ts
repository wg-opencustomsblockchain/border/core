/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/** Address entity class to handle address info. */
export class Address {
  streetAndNumber!: string;
  postcode!: string;
  city!: string;
  countryCode!: string;

  /** Basic constructor */
  constructor() {
    this.streetAndNumber = '';
    this.postcode = '';
    this.city = '';
    this.countryCode = '';
  }
}

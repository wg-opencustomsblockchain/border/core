/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/** CustomsOfficeTranslation entity class to handle CustomsOfficeTranslation info. */
export class CustomsOfficeTranslation {
  code!: string;
  description!: string;

  /** Basic Constructor */
  constructor(code: string, description: string) {
    this.code = code;
    this.description = description;
  }
}

export interface CustomsOfficeInformations {
  referenceNumber: string;
  postalCode: string;
  countryCode: string;
  name: string;
  streetAndNumber: string;
  city: string;
}

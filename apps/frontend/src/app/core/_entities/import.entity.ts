/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { EADDto } from './ead.entity';
import { Company } from './company.entity';
import { CustomsOffice } from './customs-coffice.entity';
import { GoodsItem } from './goods-item.entity';
import { Transport } from './transport.entity';
import { TransportEvent } from './event.entity';

/** ImportEntity entity class to handle ImportEntity info. */
export class ImportEntity {
  creator: string;
  id: string;
  consignor: Company | undefined;
  exporter: Company | undefined;
  consignee: Company | undefined;
  declarant: Company | undefined;
  customOfficeOfExit: CustomsOffice | undefined;
  customOfficeOfEntry: CustomsOffice | undefined;
  customOfficeOfImport: CustomsOffice | undefined;
  goodsItems: GoodsItem[];
  transportAtBorder: Transport | undefined;
  transportFromBorder: Transport | undefined;
  importId: string;
  uniqueConsignmentReference: string;
  localReferenceNumber: string;
  destinationCountry: string;
  exportCountry: string;
  itinerary: string;
  incotermCode: string;
  incotermLocation: string;
  totalGrossMass: string;
  goodsItemQuantity: number;
  totalPackagesQuantity: number;
  releaseDateAndTime: string;
  natureOfTransaction: number;
  totalAmountInvoiced: number;
  invoiceCurrency: string;
  relatedExportToken: string;

  status!: string;
  user!: string;
  timestamp!: string;

  importStatus!: number;
  exportStatus!: number;
  idShortened!: string;
  events: TransportEvent[];

  eventType: string;

  /** Basic Constructor */
  constructor(
    consignor: Company,
    exporter: Company,
    consignee: Company,
    declarant: Company,
    customOfficeOfExit: CustomsOffice,
    customOfficeOfEntry: CustomsOffice,
    customOfficeOfImport: CustomsOffice,
    goodsItems: GoodsItem[],
    transportAtBorder: Transport,
    transportFromBorder: Transport,
    importId: string,
    uniqueConsignmentReference: string,
    localReferenceNumber: string,
    destinationCountry: string,
    exportCounty: string,
    itinerary: string,
    incotermCode: string,
    incotermLocation: string,
    totalGrossMass: string,
    goodsItemQuantity: number,
    totalPackagesQuantity: number,
    natureOfTransaction: number,
    totalAmountInvoiced: number,
    invoiceCurrency: string,
    releaseDateAndTime: string,
    status: string,
    relatedExportToken: string,
    events: TransportEvent[]
  ) {
    this.consignor = consignor;
    this.exporter = exporter;
    this.consignee = consignee;
    this.declarant = declarant;
    this.customOfficeOfExit = customOfficeOfExit;
    this.customOfficeOfEntry = customOfficeOfEntry;
    this.customOfficeOfImport = customOfficeOfImport;
    this.goodsItems = goodsItems;
    this.transportAtBorder = transportAtBorder;
    this.transportFromBorder = transportFromBorder;
    this.importId = importId;
    this.uniqueConsignmentReference = uniqueConsignmentReference;
    this.localReferenceNumber = localReferenceNumber;
    this.destinationCountry = destinationCountry;
    this.exportCountry = exportCounty;
    this.itinerary = itinerary;
    this.incotermCode = incotermCode;
    this.incotermLocation = incotermLocation;
    this.totalGrossMass = totalGrossMass;
    this.goodsItemQuantity = goodsItemQuantity;
    this.totalPackagesQuantity = totalPackagesQuantity;
    this.natureOfTransaction = natureOfTransaction;
    this.totalAmountInvoiced = totalAmountInvoiced;
    this.invoiceCurrency = invoiceCurrency;
    this.releaseDateAndTime = releaseDateAndTime;
    this.status = status;
    this.relatedExportToken = relatedExportToken;
    this.events = events;
  }

  /** Helper function to parse an export into an import entity. */
  static parseExport(ead: EADDto): ImportEntity {
    return new ImportEntity(
      new Company(), // ? Abklärung mit Roman/Lorenz
      ead.exporter,
      ead.consignee,
      new Company(),
      ead.customOfficeOfExit,
      new CustomsOffice(),
      new CustomsOffice(),
      ead.goodsItems,
      ead.transportAtBorder,
      new Transport(),
      '',
      ead.uniqueConsignmentReference,
      '',
      ead.destinationCountry,
      ead.exportCountry,
      ead.itinerary,
      ead.incotermCode,
      ead.incotermLocation,
      ead.totalGrossMass,
      ead.goodsItemQuantity,
      ead.totalPackagesQuantity,
      ead.natureOfTransaction,
      ead.totalAmountInvoiced,
      ead.invoiceCurrency,
      ead.releaseDateAndTime,
      '1',
      ead.id,
      ead.events
    );
  }
}

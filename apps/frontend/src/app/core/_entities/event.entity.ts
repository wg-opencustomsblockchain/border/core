/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { User } from '@border/api-interfaces';
import { ExportStatus, ImportStatus } from '../_enums/status.enum';

export class TransportEvent {
  creator: string;
  user: User;
  eventType: string;
  message: string;
  status: ImportStatus | ExportStatus;
  timestamp: Date;
  creatorName?: string;
}

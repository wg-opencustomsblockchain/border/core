/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Company } from './company.entity';
import { CustomsOffice } from './customs-coffice.entity';
import { TransportEvent } from './event.entity';
import { GoodsItem } from './goods-item.entity';
import { Transport } from './transport.entity';

/** EADDto entity class to handle all EAD info. */
export class EADDto {
  creator: string;
  id: string;
  exporter: Company | undefined;
  declarant: Company | undefined;
  representative: Company | undefined;
  consignee: Company | undefined;
  customsOfficeOfExport: CustomsOffice | undefined;
  customOfficeOfExit: CustomsOffice | undefined;
  customOfficeOfImport: CustomsOffice | undefined;
  customOfficeOfEntry: CustomsOffice | undefined;
  goodsItems: GoodsItem[];
  transportToBorder: Transport | undefined;
  transportAtBorder: Transport | undefined;
  exportId: string;
  uniqueConsignmentReference: string;
  localReferenceNumber: string;
  destinationCountry: string;
  exportCountry: string;
  itinerary: string;
  releaseDateAndTime: string;
  incotermCode: string;
  incotermLocation: string;
  totalGrossMass: string;
  goodsItemQuantity: number;
  totalPackagesQuantity: number;
  natureOfTransaction: number;
  totalAmountInvoiced: number;
  invoiceCurrency: string;
  events: TransportEvent[];

  user: string;
  status: string;

  importStatus!: number;
  exportStatus!: number;

  eventType: string;
  timestamp: string | number | Date;
}

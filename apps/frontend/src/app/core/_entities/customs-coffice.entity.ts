/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Address } from './address.entity';

/** CustomsOffice entity class to handle CustomsOffice info. */
export class CustomsOffice {
  address!: Address | undefined;
  customsOfficeCode!: string;
  customsOfficeName!: string;

  /** Basic Constructor */
  constructor() {
    this.address = new Address();
    this.customsOfficeCode = '';
    this.customsOfficeName = '';
  }
}

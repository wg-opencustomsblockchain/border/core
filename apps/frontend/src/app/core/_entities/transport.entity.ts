/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/** Transport entity class to handle Transport info. */
export class Transport {
  modeOfTransport: number;
  typeOfIdentification: number;
  identity: string;
  nationality: string;
  transportCost: number;
  transportCostCurrency: string;
  transportOrderNumber: string;

  constructor() {
    this.modeOfTransport = -1;
    this.typeOfIdentification = -1;
    this.identity = '';
    this.nationality = '';
    this.transportCost = -1;
    this.transportCostCurrency = '';
    this.transportOrderNumber = '';
  }
}

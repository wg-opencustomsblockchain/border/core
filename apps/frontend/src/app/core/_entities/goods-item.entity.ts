/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Document } from './document.entity';

/** GoodItem entity class to handle GoodItem info. */
export class GoodsItem {
  documents: Document[];
  countryOfOrigin: string;
  sequenceNumber: number;
  descriptionOfGoods: string;
  harmonizedSystemSubheadingCode: number;
  localClassificationCode: number;
  grossMass: string;
  netMass: string;
  numberOfPackages: number;
  typeOfPackages: string;
  marksNumbers: string;
  containerId: string;
  consigneeOrderNumber: string;
  valueAtBorderExport: number | undefined;
  valueAtBorderExportCurrency: string | undefined;
  valueAtBorder: number | undefined;
  valueAtBorderCurrency: string | undefined;
  amountInvoiced: number;
  amountInvoicedCurrency: string;
  dangerousGoodsCode: number;
}

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export enum ExportStatus {
  // Status before recording on the Chain
  NEW = '0',
  // Recorded on blockchain
  RECORDED = '1',
  // Ready for pickup
  READY = '2',
  // Pickup complete
  PICKUP = '3',
  // Presentation of goods complete
  PRESENTED = '4',
  // Exit of goods certified
  EXIT = '5',
  // Archived
  ARCHIVED = '6',
  // DECLINED
  DECLINED = '10',
}

export enum ImportStatus {
  // Status before recording on the Chain
  PROPOSAL = '0',
  // Recorded on blockchain
  ACCEPTED = '1',
  // Ready for pickup
  COMPLETED = '2',
  // Pickup complete
  EXPORTED = '3',
  // Presentation of goods complete
  SUBMITTED = '4',
  // Exit of goods certified
  RELEASE = '5',
  // If import is declined
  DECLINED = '10'
}

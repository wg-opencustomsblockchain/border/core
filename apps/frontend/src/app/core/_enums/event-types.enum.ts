/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export enum ExportEventTypes {
  // Event for initial writing - Status 1
  EXPORT_RECORDED = 'EXPORT_RECORDED',
  // Event for preparing the Export - Status 2
  EXPORT_READY = 'EXPORT_READY',
  // Event for Pickup procedure - Status 3
  EXPORT_PICKUP = 'EXPORT_PICKUP',
  // Event for presenting at office - Status 4
  EXPORT_PRESENTED = 'EXPORT_PRESENTED',
  // Event for confirming the exit of goods - status 5
  EXPORT_EXIT = 'EXPORT_EXIT',
  // Event for Archiving the Token - Status 6
  EXPORT_ARCHIVED = 'EXPORT_ARCHIVED',

  // -- No Status Changes --
  // Event for presenting for a mobile customs check
  EXPORT_CHECK = 'EXPORT_CHECK',
  // Event for presenting for a mobile customs check
  EXPORT_MOBILECHECK = 'EXPORT_MOBILECHECK',
  // Event in case the Export is refused by an importer
  EXPORT_REFUSAL = 'EXPORT_REFUSAL',
  // Event in case the Export is refused by an importer
  EXPORT_ACCEPTED = 'EXPORT_ACCEPTED',
  // The Event after the broker added a new EAD
  EXPORT_NEW = 'EXPORT_NEW',
}

export enum ImportEventTypes {
  // Event for an export being accewpted as an import - Status 1
  IMPORT_ACCEPTED = 'IMPORT_ACCEPTED',
  // Event for an import Process being completed - Status 2
  IMPORT_COMPLETED = 'IMPORT_COMPLETED',

  // -- No Status Changes --
  // Event for updating the token by the import
  IMPORT_PROPOSAL = 'IMPORT_PROPOSAL',
  // Event for Refusing the token from Export
  IMPORT_REFUSED = 'IMPORT_REFUSED',
  // Event for updating the token by the import
  IMPORT_SUPPLEMENT = 'IMPORT_SUPPLEMENT',
  // NOT IMPLEMENTED YET
  IMPORT_EXPORTED = 'IMPORT_EXPORTED',
  // NOT IMPLEMENTED YET
  IMPORT_SUBMITTED = 'IMPORT_SUBMITTED',
  // NOT IMPLEMENTED YET
  IMPORT_RELEASE = 'IMPORT_RELEASE',
}

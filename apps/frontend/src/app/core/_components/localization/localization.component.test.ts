/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';

import { LocalizationComponent } from './localization.component';

describe('LocalizationComponent', () => {
  let component: LocalizationComponent;
  let fixture: ComponentFixture<LocalizationComponent>;
  let translateService: TranslateService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule, TranslateModule.forRoot()],
      declarations: [LocalizationComponent],
      providers: [
        {
          provide: TranslateService,
          useClass: TranslateService,
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LocalizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    translateService = TestBed.inject(TranslateService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('When the program is initialized', () => {
    it('should initialize with a selected Locale', () => {
      component.initLocale('de-DE');
      expect(translateService.currentLang).toEqual('de-DE');
      expect(translateService.defaultLang).toEqual('de-DE');
    });
  });

  describe('When a locale is swapped', () => {
    it('should change the selected locale to reflect that', () => {
      component.changeLocale('en-GB');
      expect(translateService.currentLang).toEqual('en-GB');
      component.changeLocale('de-DE');
      expect(translateService.currentLang).toEqual('de-DE');
    });
  });

  describe('When a preload is called', () => {
    it('should show all available languages', () => {
      component.preload();
      expect(translateService.getLangs.length).toBeGreaterThanOrEqual(0);
    });
  });
});

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

/**
 * A component for changing the language of the ui
 */
@Component({
  selector: 'app-localization',
  templateUrl: './localization.component.html',
  styleUrls: ['./localization.component.scss'],
})
export class LocalizationComponent {
  /**
   * Creates an instance of the localization component
   * @param translate injects the translate service
   * @param router injects the router
   */
  constructor(public translate: TranslateService, private router: Router) {
    // Check if a locale has been set before. If not initialize in German.
    const locale = localStorage.getItem('locale');

    !locale ? this.initLocale('de-DE') : this.initLocale(locale);
  }

  /**
   * The current locale
   */
  locale = 'de-DE';

  /**
   * Force A pre-Reload to serve languages immediately.
   * This fixes a bug in some Angular versions.
   */
  preload(): void {
    this.translate.reloadLang('en-GB');
    this.translate.reloadLang('de-DE');
  }

  /**
   * Called when initializing the locale.
   * @param locale The local string
   */
  initLocale(locale: string): void {
    this.preload();
    this.translate.setDefaultLang(locale);
    this.translate.use(locale);
    this.locale = locale;
  }

  /**
   * Change to the requested locale if available and update the Translation Service as well as the View.
   * @param locale the local string
   */
  changeLocale(locale: string): void {
    this.translate.use(locale);
    localStorage.setItem('locale', this.translate.currentLang);
    this.locale = this.translate.currentLang;
  }
}

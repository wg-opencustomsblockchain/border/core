/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { User } from '@border/api-interfaces';
import { BehaviorSubject, Subscription } from 'rxjs';
import { TransportEvent } from '../../_entities/event.entity';
import { EventHistoryType } from './event-history.enum';

/**
 * The main menu component
 */
@Component({
  selector: 'app-event-history',
  templateUrl: './event-history.component.html',
  styleUrls: ['./event-history.component.scss'],
})
export class EventHistoryComponent implements OnInit, OnDestroy {
  /**
   * Behaviour Subject which delivers new Events on hot Load
   */
  eventSubject: BehaviorSubject<TransportEvent[]>;
  /**
   * Subsription for the Behaviour Subject.
   * Unsubscribe on Destruction.
   */
  eventSubscription: Subscription;

  /**
   * Show different content depending on Role
   */
  viewType: string;

  /**
   * The Currently logged in User.
   */
  currentUser: User;

  currentUserRoles: string[];

  /**
   * The list of Events to show.
   */
  events: TransportEvent[] = [];

  /**
   * All Users involved in the Event History.
   */
  eventUsers: User[] = [];

  /**
   * A list of Wallet Addresses of involved Users for quicker lookup.
   */
  walletList: string[] = [];

  /**
   * Construct the view with Event Data.
   * @param data including events as Behaviour Subject, what kind of view is requested and the current user logged in.
   * @param userService to dynamically map other Users to their wallet addresses.
   */
  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: { eventObservable: BehaviorSubject<TransportEvent[]>; viewType: EventHistoryType; currentUser: User }
  ) {
    this.viewType = data.viewType;
    this.currentUser = data.currentUser;
    this.eventSubject = data.eventObservable;
  }
  /**
   * On Init subscribe to the behaviour subject and dynamically fill the Event History.
   */
  ngOnInit(): void {
    this.eventSubscription = this.eventSubject.subscribe(async (element) => {
      this.events = element;
    });
  }

  /**
   * Destroy the subscription on closing.
   */
  ngOnDestroy(): void {
    this.eventSubscription.unsubscribe();
  }
}

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpLoaderFactory } from '@base/src/app/app.module';
import { LightClientService } from '@core/services/light-client.service';
import { TranslateLoader, TranslateModule, TranslatePipe, TranslateService } from '@ngx-translate/core';
import { TokenService } from '@shared/services/token.service';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { BehaviorSubject } from 'rxjs';

import { TransportEvent } from '../../_entities/event.entity';
import { EventHistoryComponent } from './event-history.component';
import { SharedModule } from '@shared/shared.module';

describe('EventHistory', () => {
  let component: EventHistoryComponent;
  let fixture: ComponentFixture<EventHistoryComponent>;
  const http = true;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EventHistoryComponent],
      imports: [
        HttpClientTestingModule,
        BrowserAnimationsModule,
        MatDialogModule,
        RouterTestingModule,
        SharedModule,
        NoopAnimationsModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [http],
          },
        }),

        BrowserAnimationsModule,

        ToastrModule.forRoot(),
      ],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: {}, translationService: TranslateService, translate: TranslatePipe },
        {
          provide: TokenService,
          useClass: TokenService,
        },
        { provide: ToastrService, useClass: ToastrService },
        { provide: http, useValue: true },
        { provide: LightClientService, useClass: LightClientService },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    const eventObservable = new BehaviorSubject<TransportEvent[]>([]);
    fixture = TestBed.createComponent(EventHistoryComponent);
    component = fixture.componentInstance;
    component.eventSubject = eventObservable;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

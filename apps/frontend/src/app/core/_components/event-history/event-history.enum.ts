/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * Available views for the Event History.
 */
export enum EventHistoryType {
  EXPORT_VIEW = 'EXPORT_VIEW',
  IMPORT_VIEW = 'IMPORT_VIEW',
}

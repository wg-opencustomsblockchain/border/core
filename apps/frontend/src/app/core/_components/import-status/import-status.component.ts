/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input } from '@angular/core';
import { ImportEventTypes } from '../../_enums/event-types.enum';
import { ImportStatus } from '../../_enums/status.enum';

@Component({
  selector: 'app-import-status[status]',
  templateUrl: './import-status.component.html',
  styleUrls: ['./import-status.component.scss'],
})
/**
 * Makes Transport Status human readble and adds corresponding icons.
 */
export class ImportStatusComponent {
  importEnum: typeof ImportStatus = ImportStatus;
  importEvenType: typeof ImportEventTypes = ImportEventTypes;
  @Input() eventType!: string;
  @Input() status!: string;
  @Input() showIcon = true;
}

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Pipe, PipeTransform } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpLoaderFactory } from '@base/src/app/app.module';
import { ExportEventTranslation, ImportEventTranslation } from '@base/src/app/shared/_pipes/statusLabel.pipe';

import { LightClientService } from '@core/services/light-client.service';
import { TokenService } from '@shared/services/token.service';
import { TranslateLoader, TranslateModule, TranslatePipe, TranslateService } from '@ngx-translate/core';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { AuthenticationService } from '../../services/authentication.service';
import { ImportStatusComponent } from './import-status.component';

describe('EventHistory', () => {
  let component: ImportStatusComponent;
  let fixture: ComponentFixture<ImportStatusComponent>;
  const http = true;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ImportStatusComponent, ExportEventTranslation, ImportEventTranslation],
      imports: [
        HttpClientTestingModule,
        BrowserAnimationsModule,
        MatDialogModule,
        RouterTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [http],
          },
        }),
        BrowserAnimationsModule,
        ToastrModule.forRoot(),
      ],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: {}, translationService: TranslateService, translate: TranslatePipe },
        { provide: TokenService, useClass: TokenService },
        { provide: TranslatePipe, useClass: TranslatePipeMock },
        { provide: ExportEventTranslation, useClass: ExportEventTranslationMock },
        { provide: ImportEventTranslation, useClass: ImportEventTranslationMock },
        { provide: ToastrService, useClass: ToastrService },
        { provide: http, useValue: true },
        { provide: LightClientService, useClass: LightClientService },
        { provide: AuthenticationService, useClass: AuthenticationService },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

@Pipe({
  name: 'translate',
})
export class TranslatePipeMock implements PipeTransform {
  public name = 'translate';

  // Required for proper Pipe Mocking.
  // eslint-disable-next-line
  public transform(query: string, ...args: any[]): any {
    return query;
  }

  public get(query: string) {
    return query;
  }
}

@Pipe({
  name: 'exportEventTranslation',
})
export class ExportEventTranslationMock implements PipeTransform {
  public name = 'exportEventTranslation';

  // Required for proper Pipe Mocking.
  // eslint-disable-next-line
  public transform(query: string, ...args: any[]): any {
    return query;
  }

  public get(query: string) {
    return query;
  }
}

@Pipe({
  name: 'importEventTranslation',
})
export class ImportEventTranslationMock implements PipeTransform {
  public name = 'importEventTranslation';
  // Required for proper Pipe Mocking.
  // eslint-disable-next-line
  public transform(query: string, ...args: any[]): any {
    return query;
  }

  public get(query: string) {
    return query;
  }
}

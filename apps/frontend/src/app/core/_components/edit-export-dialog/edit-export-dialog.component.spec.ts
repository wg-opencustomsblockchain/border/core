/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
  MatDialogModule,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { RouterTestingModule } from '@angular/router/testing';
import { mockEAD } from '@base/src/assets/mockups/EADFile.mock';
import {
  TranslateModule,
  TranslatePipe,
  TranslateService,
} from '@ngx-translate/core';
import { Observable, of } from 'rxjs';

import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from '@base/src/app/shared/shared.module';
import { EADDto, Transport } from '@core/_entities/';
import { TokenService } from '@shared/services/token.service';
import { EditExportDialogComponent } from './edit-export-dialog.component';
import { EditFileComponent } from '@core/_components/edit-import-dialog/edit-file/edit-file.component';
import { LocalizationComponent } from '@core/_components/localization/localization.component';

describe('EditImportDialogComponent', () => {
  let component: EditExportDialogComponent;
  let fixture: ComponentFixture<EditExportDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        MatDialogModule,
        RouterTestingModule,
        TranslateModule.forRoot(),
        SharedModule,
        NoopAnimationsModule,
      ],
      declarations: [
        EditExportDialogComponent,
        EditFileComponent,
        LocalizationComponent,
      ],
      providers: [
        {
          provide: MAT_DIALOG_DATA,
          useValue: {},
          translationService: TranslateService,
          translate: TranslatePipe,
        },
        {
          provide: MatDialogRef,
          useValue: {
            close(value = ''): void {
              return;
            },
          },
        },
        {
          provide: TokenService,
          useValue: {
            getEADFromBlockchain(_: string): Observable<HttpResponse<EADDto>> {
              return of(new HttpResponse({ body: mockEAD.ead }));
            },
          },
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditExportDialogComponent);
    component = fixture.componentInstance;
    component.data = { export: mockEAD.ead };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit should load the correct export token and set the import token', () => {
    expect(component.exportToken).toEqual(mockEAD.ead);
  });

  describe('toggleTransport should ', () => {
    it('set transportToBorder equal to transportAtBorder if checked', () => {
      component.toggleTransport({ checked: true } as MatSlideToggleChange);
      expect(component.foreignEqualsDomestic).toBe(true);
      expect(component.exportToken.transportToBorder).toEqual(
        component.exportToken.transportAtBorder
      );
    });

    it('set transportToBorder to an empty transport object if unchecked', () => {
      component.toggleTransport({ checked: false } as MatSlideToggleChange);
      expect(component.foreignEqualsDomestic).toBe(false);
      expect(component.exportToken.transportToBorder).toEqual(new Transport());
    });
  });

  it('get sum should calculate the correct sum', () => {
    component.exportToken = component.data.export;
    component.goodsItems = component.data.export.goodsItems;
    component.selection = [true, true, false];
    component.getSum();
    expect(component.statisticalValueSum).toEqual(180);
    expect(component.packageCount).toEqual(200);
    expect(component.invoiceValueSum).toEqual(200);
    expect(component.grossMassSum).toEqual(4200);
    expect(component.netMassSum).toEqual(4000);
    expect(component.positionCount).toEqual(2);
  });

  describe('closeDialog should', () => {
    it('just close the dialog without data', () => {
      const importCopy = component.exportToken;
      component.closeDialog(false);
      expect(component.exportToken).toEqual(importCopy);
    });

    it('should set the values needed and return the import copy object', () => {
      component.selection = [true, true, false];
      component.closeDialog(true);
      expect(component.exportToken.id).toEqual(component.data.export.id);
      expect(component.exportToken.creator).toEqual(
        component.data.export.creator
      );
      expect(component.exportToken.status).toEqual(
        component.data.export.status
      );
      expect(component.exportToken.id).toEqual(component.data.export.id);
      expect(component.exportToken.totalPackagesQuantity).toEqual(
        component.packageCount
      );
      expect(component.exportToken.totalGrossMass).toEqual(
        component.grossMassSum.toString()
      );
      expect(component.exportToken.totalAmountInvoiced).toEqual(
        component.invoiceValueSum
      );
      expect(component.exportToken.goodsItemQuantity).toEqual(
        component.positionCount
      );
      expect(component.exportToken.goodsItems.length).toEqual(2);
    });
  });

  describe('getGoodsItem should', () => {
    it('return null if sequence number is not defined', () => {
      expect(component.getGoodsItem(undefined)).toEqual(null);
    });

    it('return null if sequence number is defined and export token is not defined', () => {
      component.exportToken = undefined;
      expect(component.getGoodsItem(1)).toEqual(null);
    });

    it('should return the good item with the sequence number if sequence number is not undefined', () => {
      const goodItemExpectedValue = {
        amountInvoiced: 100,
        amountInvoicedCurrency: 'EUR',
        consigneeOrderNumber: '12',
        containerId: '12',
        countryOfOrigin: 'DE',
        documents: [],
        localClassificationCode: 0,
        marksNumbers: '0',
        typeOfPackages: 'RTL',
        valueAtBorder: 20000,
        valueAtBorderCurrency: 'EUR',
        valueAtBorderExport: 0.9,
        valueAtBorderExportCurrency: 'EUR',
        descriptionOfGoods: 'Biegeleiste',
        grossMass: '2100',
        netMass: '2000',
        numberOfPackages: 100,
        harmonizedSystemSubheadingCode: 0,
        dangerousGoodsCode: 0,
        sequenceNumber: 1,
      };
      expect(component.getGoodsItem(1)).toEqual(goodItemExpectedValue);
    });
  });

  describe('getSecureLevel should ', () => {
    it(' retun loading if fieldInformation is undefined', () => {
      component.fieldInformation = undefined;
      expect(component.getSecureLevel('Test')).toEqual('loading');
    });
    it(' retun loading if fieldInformation is an empty Array', () => {
      component.fieldInformation = [];
      expect(component.getSecureLevel('Test')).toEqual('loading');
    });
    it(' retun secure if fieldInformation has an unedited field', () => {
      component.fieldInformation = [
        { fieldName: 'Test', isEdited: false, lastEditor: 'Border Broker' },
      ];
      expect(component.getSecureLevel('Test')).toEqual('secure');
    });
    it(' retun company if fieldInformation has an edited field', () => {
      component.fieldInformation = [
        { fieldName: 'Test', isEdited: true, lastEditor: 'Border Broker' },
      ];
      expect(component.getSecureLevel('Test')).toEqual('company');
    });
  });

  describe('getLastEditor should ', () => {
    it(' retun history.loading if fieldInformation is undefined', () => {
      component.fieldInformation = undefined;
      expect(component.getLastEditor('Test')).toEqual('history.loading');
    });
    it(' retun history.unavailable if fieldInformation is an empty Array', () => {
      component.fieldInformation = [];
      expect(component.getLastEditor('Test')).toEqual('history.unavailable');
    });
    it(' retun secure if fieldInformation has an unedited field', () => {
      component.fieldInformation = [
        { fieldName: 'Test', isEdited: false, lastEditor: 'Border Broker' },
      ];
      expect(component.getLastEditor('Test')).toEqual('Border Broker');
    });
  });
});

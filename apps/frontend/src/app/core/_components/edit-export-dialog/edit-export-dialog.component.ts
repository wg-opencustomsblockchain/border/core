/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';

import { Company, EADDto, GoodsItem, Transport } from '../../_entities';
import {
  CurrencyUtility,
  SUPPORTED_CURRENCIES,
  SUPPORTED_LOCALES,
} from '@base/src/app/shared/utils/utils.currency';
import { TranslateService } from '@ngx-translate/core';
import { HistoryService } from '@core/services/history.service';
import { FieldInformationDto } from '@border/api-interfaces/lib/entities/history/history.dto';
import { Address } from '@border/api-interfaces';
/*
 * A desktop component for viewing/editing export tokens
 */

@Component({
  selector: 'app-edit-export-dialog',
  templateUrl: './edit-export-dialog.component.html',
  styleUrls: ['./edit-export-dialog.component.scss'],
})
export class EditExportDialogComponent implements OnInit {
  /**
   * Is the consignee equals to consignor
   */
  consigneeEqualsConsignor = false;
  /**
   * Is the declarant equals to exporter
   */
  declarantEqualsExporter = false;
  /**
   * Are there additional transport costs
   */
  foreignTransportCosts = false;
  /**
   * Is the foreign transport the same as the domestic transport
   */
  foreignEqualsDomestic = false;

  // Sum Calculations
  /**
   * The statistical value of all goods
   */
  statisticalValueSum = 0;
  /**
   * The package count of all goods
   */
  packageCount = 0;
  /**
   * The net mass of all goods
   */
  netMassSum = 0;
  /**
   * The gross mass of all goods
   */
  grossMassSum = 0;
  /**
   * The invoice value of all goods
   */
  invoiceValueSum = 0;
  /**
   * The count of all positions selected for acceptance
   */
  positionCount = 0;

  /**
   * The Positions for the Import
   * Deep Copied so it can be edited without messing up the Import Token.
   */
  goodsItems: GoodsItem[] = [];

  /**
   * The export token which is referenced by the import process
   */
  exportToken: EADDto;

  /**
   * An array to save, which goods positions are selected
   */
  selection: boolean[] = [];

  /**
   * Border Currency utility to transform monetary values.
   */
  currUtil: CurrencyUtility = new CurrencyUtility();

  /**
   * Get enum of supported currencies to call from HTML context.
   */
  currencies = SUPPORTED_CURRENCIES;

  /**
   * Get enum of supported locales to call from HTML context.
   */
  locales = SUPPORTED_LOCALES;

  /**
   * The locale in which parts of the document should be formatted.
   */
  currentLocale: string;

  /**
   * Is editing of the ead allowed?
   */
  locked = true;

  /**
   * Temporary variable to set to, for checking if the field is edited.
   */
  fieldInformation: FieldInformationDto[];

  /**
   * Creates a edit export dialog popup component
   * @param data The export data object
   * @param dialogRef Injects the material dialog service
   * @param translateService
   * @param historyService
   */
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { export: EADDto },
    public dialogRef: MatDialogRef<EditExportDialogComponent>,
    private translateService: TranslateService,
    private readonly historyService: HistoryService
  ) {
    this.currentLocale = translateService.currentLang;
  }

  /**
   * Initialize the export, set the selection array, calculate the sum afterwards
   */
  ngOnInit(): void {
    this.exportToken = this.data.export;
    console.log(this.exportToken);
    if (!this.exportToken.representative.address)
      this.exportToken.representative.address = {} as Address;

    // Deep Copy the GoodsItems so the positions can be edited without messing up the Import Token
    const goodsItemsCopy = this.exportToken.goodsItems.map((x) =>
      Object.assign({}, x)
    );
    goodsItemsCopy.forEach((element) => {
      element.valueAtBorderExport = this.currUtil.toMajorDenom(
        element.valueAtBorderExportCurrency,
        element.valueAtBorderExport
      );
      element.amountInvoiced = this.currUtil.toMajorDenom(
        this.exportToken.invoiceCurrency,
        element.amountInvoiced
      );
    });
    this.goodsItems = goodsItemsCopy;
    this.exportToken.goodsItems.forEach(() => this.selection.push(true));
    this.getSum();
    console.log(this.goodsItems);
    this.historyService
      .getHistoryOfToken(this.exportToken.id)
      .subscribe((res) => {
        this.fieldInformation = res;
      });
  }

  /**
   * Toggles if the consignor equals the consignee
   * @param event - material slide toggle on change event
   */
  toggleDeclarant(event: MatSlideToggleChange): void {
    this.declarantEqualsExporter = event.checked;

    if (event.checked) {
      this.exportToken.declarant = this.exportToken.exporter;
    } else {
      this.exportToken.declarant = new Company();
    }
  }

  /**
   * Toggles if the transport to border equals the transport at border
   * @param event - material slide toggle on change event
   */
  toggleTransport(event: MatSlideToggleChange): void {
    this.foreignEqualsDomestic = event.checked;

    if (event.checked) {
      this.exportToken.transportToBorder = this.exportToken.transportAtBorder;
    } else {
      this.exportToken.transportToBorder = new Transport();
    }
  }

  /**
   * Calculates all sums related to the goods items, takes the selected array into account for calculation
   */
  getSum(): void {
    this.resetValuesForSum();
    this.goodsItems.forEach((i: GoodsItem, index) => {
      if (this.selection[index]) {
        if (i.valueAtBorderExport) {
          this.statisticalValueSum += +i.valueAtBorderExport;
        }
        if (i.numberOfPackages) {
          this.packageCount += +i.numberOfPackages;
        }
        if (i.amountInvoiced) {
          this.invoiceValueSum += +i.amountInvoiced;
        }
        if (i.grossMass) {
          this.grossMassSum += +i.grossMass;
        }
        if (i.netMass) {
          this.netMassSum += +i.netMass;
        }
      }
    });
    console.log(this.invoiceValueSum);
    this.positionCount = this.selection.filter(Boolean).length;
  }

  /**
   * Searches for a good item in export token good items array and return the item if one is found
   * @param sequenceNumber the sequence number to search for
   * @returns the good item with the given sequence number if found, otherwise undefined
   */
  getGoodsItem(sequenceNumber: number): GoodsItem {
    if (!this.exportToken || !sequenceNumber) {
      return null;
    }

    return this.exportToken.goodsItems.find(
      (goodsItem) => goodsItem.sequenceNumber === sequenceNumber
    );
  }

  /**
   * Reset all calculated values to 0
   */
  private resetValuesForSum(): void {
    this.statisticalValueSum = 0;
    this.packageCount = 0;
    this.invoiceValueSum = 0;
    this.grossMassSum = 0;
    this.netMassSum = 0;
    this.positionCount = 0;
  }

  /**
   * If the edit was aborted, nothing will happen and the dialog gets closed.
   * If the edit was saved:
   * - Copy the not editable data (ex creator, id, status) to the copy object
   * - filter the goods items for non-selected and delete them
   * - close the dialog and return the new export data
   * @param withData should the changed be written
   * @returns void
   */
  closeDialog(withData: boolean): void {
    if (!withData) {
      this.dialogRef.close();
      return;
    }
    console.log(this.invoiceValueSum);
    this.goodsItems = this.formatGoodItemsCurrencies();
    this.exportToken.totalPackagesQuantity = this.packageCount;
    this.exportToken.totalGrossMass = this.grossMassSum.toString();
    this.exportToken.totalAmountInvoiced = this.invoiceValueSum;
    this.exportToken.goodsItemQuantity = this.positionCount;
    this.exportToken.goodsItems = this.goodsItems;
    console.log(this.invoiceValueSum);

    this.dialogRef.close(this.exportToken);
  }

  /**
   * Get information on secure level of the field
   * @param fieldName 
   * @returns Secure level
   */
  getSecureLevel(fieldName: string): 'secure' | 'company' | 'loading' {
    if (!this.fieldInformation || this.fieldInformation.length === 0)
      return 'loading';
    const field = this.fieldInformation.find(
      (fi) => fi.fieldName === fieldName
    );
    if (!field) return 'loading';
    return field.isEdited ? 'company' : 'secure';
  }

  /**
   * Get information on the last editor of the field
   * @param fieldName 
   * @returns Last editor
   */
  getLastEditor(fieldName: string): string {
    if (!this.fieldInformation)
      return this.translateService.instant('history.loading');
    const field = this.fieldInformation.find(
      (fi) => fi.fieldName === fieldName
    );
    if (!field) return this.translateService.instant('history.unavailable');
    return field.lastEditor;
  }

  /**
   * getter for translateservice
   * @returns currentLang of translateService
   */
  getLocale() {
    return this.translateService.currentLang;
  }

  /**
   * replace comma decimal separator with dot
   * @param value number containing comma
   * @returns converted value
   */
  formatDecimalSeparatorToDot(value: number): number {
    return +value.toString().replace(',', '.');
  }

  /**
   * format all currency inputs to cent and replaced comma sparator with dot
   * @returns formated goods item array
   */
  private formatGoodItemsCurrencies(): GoodsItem[] {
    this.invoiceValueSum = this.currUtil.toMinorDenom(
      this.exportToken.invoiceCurrency,
      this.invoiceValueSum
    );
    const goodItems = [];
    this.selection.forEach((item, index) => {
      if (item) {
        const gooditem = this.goodsItems[index];
        gooditem.valueAtBorderExport = this.formatDecimalSeparatorToDot(
          gooditem.valueAtBorderExport
        );
        gooditem.valueAtBorder = this.currUtil.toMinorDenom(
          gooditem.valueAtBorderCurrency,
          gooditem.valueAtBorder
        );
        gooditem.amountInvoiced = this.formatDecimalSeparatorToDot(
          gooditem.amountInvoiced
        );
        gooditem.amountInvoiced = this.currUtil.toMinorDenom(
          this.exportToken.invoiceCurrency,
          gooditem.amountInvoiced
        );
        goodItems.push(gooditem);
      }
    });
    return goodItems;
  }
}

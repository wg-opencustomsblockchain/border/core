/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { NavigationItems } from '@base/src/app/shared/navigation';
import { UserRole, User } from '@border/api-interfaces';
import { KeycloakService } from 'keycloak-angular';
import { Subscription } from 'rxjs';

import { VERSION } from '../../../../environments/version';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit, OnDestroy {
  @Input() extended = true;

  navigationItems: any[];
  /**
   * The current user object (nullable)
   */
  currentUser!: User | null;
  roles: string[] = [];

  /**
   * The user subscription
   */
  private userSubscription: Subscription = new Subscription();

  /**
   * Creates an instance of the toolbar component
   * @param authService inject auth service
   */
  constructor(private readonly keycloak: KeycloakService) {}

  /**
   * If the page gets closed, unsubscribe from user subscription
   */
  ngOnDestroy(): void {
    // unsubscribe from user subscription
    this.userSubscription.unsubscribe();
  }

  /**
   * On initialization, get the current user
   */
  async ngOnInit() {
    this.currentUser = await this.keycloak.loadUserProfile();
    this.roles = this.keycloak.getUserRoles();
    this.navigationItems = NavigationItems.filter((item) => this.hasRoles(item.roles));
  }

  /**
   * Log out the user
   */
  logout(): void {
    this.keycloak.logout(window.location.origin);
  }

  /**
   * Returns the user role as string
   * @param role UserRole as number
   * @returns The user role as string
   */
  getRoleName(): string {
    const rolesArray = [UserRole.IMPORTER, UserRole.EXPORTER, UserRole.DRIVER];
    let returnValue = '';
    rolesArray.forEach((role: string) => {
      if (this.roles.includes(role)) returnValue = role;
    });
    return returnValue;
  }

  /**
   * Checks if roles are included
   * @param roles 
   * @returns 
   */
  hasRoles(roles: string[]) {
    let hasRole = false;
    roles.forEach((role) => {
      if (this.roles.includes(role)) hasRole = true;
    });
    return hasRole;
  }

  get version(): string {
    return VERSION.raw;
  }
}

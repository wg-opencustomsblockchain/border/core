/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input } from '@angular/core';
import { ExportEventTypes } from '../../_enums/event-types.enum';
import { ExportStatus } from '../../_enums/status.enum';

@Component({
  selector: 'app-export-status[status]',
  templateUrl: './export-status.component.html',
  styleUrls: ['./export-status.component.scss'],
})
export class ExportStatusComponent {
  /**
   * Makes Transport Status human readble and adds corresponding icons.
   */
  exportEnum: typeof ExportStatus = ExportStatus;
  exportEventType: typeof ExportEventTypes = ExportEventTypes;
  @Input() eventType!: string;
  @Input() status!: string;
  @Input() showIcon = true;
}

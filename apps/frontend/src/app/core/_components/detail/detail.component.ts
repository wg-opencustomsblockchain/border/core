/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input } from '@angular/core';
import { EADDto } from '../../_entities/ead.entity';

/**
 * The mobile component for showing some information of the scanned qr code
 */
@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
})
export class DetailComponent {
  /**
   * Input for the selected elements that should be displayed in confirmation
   */
  @Input() selection: EADDto = new EADDto();
}

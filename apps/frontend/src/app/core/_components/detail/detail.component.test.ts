/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Pipe, PipeTransform } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule, TranslatePipe, TranslateService } from '@ngx-translate/core';

import { CustomsOfficeService } from '@core/services/customs-office.service';
import { TokenService } from '@shared/services/token.service';
import { LookupPipe } from '@shared/_pipes/lookup.pipe';
import { DetailComponent } from './detail.component';

describe('DetailComponent', () => {
  let component: DetailComponent;
  let fixture: ComponentFixture<DetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, MatDialogModule, RouterTestingModule, TranslateModule.forRoot()],
      declarations: [DetailComponent, CustomsLookupPipeMock],
      providers: [
        {
          provide: TokenService,
          useClass: TokenService,
          translateService: TranslateService,
          translate: TranslatePipe,
        },
        { provide: CustomsOfficeService, useClass: CustomsOfficeService },
        { provide: LookupPipe, useClass: CustomsLookupPipeMock },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

@Pipe({
  name: 'customsLookup',
})
export class CustomsLookupPipeMock implements PipeTransform {
  public name = 'customsLookup';

  public transform(query: string, ...args: any[]): any {
    return query;
  }
}

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

/**
 * A loading component with a nice animation
 */
@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss'],
})
export class LoadingComponent {
  /**
   * Creates an Loading popup with the given text ({ text: '' })
   * @param data MAT_DIALOG_DATA an object with text property which should be displayed
   */
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {}
}

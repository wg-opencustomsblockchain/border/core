/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from '@base/src/app/shared/shared.module';
import { MOCK_USER } from '@base/src/assets/mockups/user.mock';
import { TranslateModule } from '@ngx-translate/core';
import { KeycloakService } from 'keycloak-angular';
import { UserRole } from '@border/api-interfaces';
import { LocalizationComponent } from '../localization/localization.component';

import { ToolbarComponent } from './toolbar.component';

describe('ToolbarComponent', () => {
  let component: ToolbarComponent;
  let fixture: ComponentFixture<ToolbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        SharedModule,
        NoopAnimationsModule,
        TranslateModule.forRoot(),
      ],
      declarations: [ToolbarComponent, LocalizationComponent],
      providers: [
        {
          provide: KeycloakService,
          useValue: {
            async loadUserProfile() {
              return Promise.resolve(MOCK_USER[1]);
            },
            getUserRoles() {
              return [UserRole.EXPORTER];
            },
          },
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    component.roles = [UserRole.EXPORTER];
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('how the toolbar handles roles', () => {
    it('should get the role name', () => {
      const role = component.getRoleName();
      expect(role).toBe(UserRole.EXPORTER);
    });

    it('check if has roles', () => {
      const bool = component.hasRoles([UserRole.EXPORTER]);

      expect(bool).toBe(true);
    });
  });
});

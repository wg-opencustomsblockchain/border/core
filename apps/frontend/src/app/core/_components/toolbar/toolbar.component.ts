/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { UserRole, User } from '@border/api-interfaces';
import { KeycloakService } from 'keycloak-angular';

/**
 * The toolbar component
 */
@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent implements OnInit {
  /**
   * The current user object (nullable)
   */
  currentUser!: User | null;
  roles: string[] = [];

  @Output() menuClick = new EventEmitter<void>();

  /**
   * Creates an instance of the toolbar component
   * @param authService inject auth service
   */
  constructor(private readonly keycloak: KeycloakService) {}

  /**
   * On initialization, get the current user
   */
  async ngOnInit() {
    this.currentUser = await this.keycloak.loadUserProfile();
    this.currentUser.attributes.company[0] = JSON.parse(this.currentUser.attributes.company[0].toString());
    this.roles = this.keycloak.getUserRoles();
  }

  /**
   * Log out the user
   */
  logout(): void {
    this.keycloak.logout(window.location.origin);
  }

  /**
   * Returns the user role as string
   * @param role UserRole as number
   * @returns The user role as string
   */
  getRoleName(): string {
    const rolesArray = [UserRole.IMPORTER, UserRole.EXPORTER, UserRole.DRIVER];
    let returnValue = '';
    rolesArray.forEach((role: string) => {
      if (this.roles.includes(role)) returnValue = role;
    });
    return returnValue;
  }

  /**
   * Returns a corresponding boolean if, given a certain set of roles, the
   * users role is included in this set to true, and else to false.
   * @param roles an array of roles
   * @returns true if user has a role in the array of roles
   */
  hasRoles(roles: string[]) {
    let hasRole = false;
    roles.forEach((role) => {
      if (this.roles.includes(role)) hasRole = true;
    });
    return hasRole;
  }
}

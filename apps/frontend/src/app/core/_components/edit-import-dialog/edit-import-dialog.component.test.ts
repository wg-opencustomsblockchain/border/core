/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
  MatDialogModule,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { RouterTestingModule } from '@angular/router/testing';
import { mockEAD } from '@base/src/assets/mockups/EADFile.mock';
import {
  TranslateModule,
  TranslatePipe,
  TranslateService,
} from '@ngx-translate/core';
import { Observable, of } from 'rxjs';

import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { mockImport } from '@base/src/assets/mockups/Import.mock';
import { Company, EADDto, Transport } from '@core/_entities/';
import { TokenService } from '@shared/services/token.service';
import { SharedModule } from '@shared/shared.module';
import { EditFileComponent } from './edit-file/edit-file.component';
import { EditImportDialogComponent } from './edit-import-dialog.component';

describe('EditImportDialogComponent', () => {
  let component: EditImportDialogComponent;
  let fixture: ComponentFixture<EditImportDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        MatDialogModule,
        RouterTestingModule,
        TranslateModule.forRoot(),
        SharedModule,
        NoopAnimationsModule,
      ],
      declarations: [EditImportDialogComponent, EditFileComponent],
      providers: [
        {
          provide: MAT_DIALOG_DATA,
          useValue: {},
          translationService: TranslateService,
          translate: TranslatePipe,
        },
        {
          provide: MatDialogRef,
          useValue: {
            close(value = ''): void {
              return;
            },
          },
        },
        {
          provide: TokenService,
          useValue: {
            getEADFromBlockchain(_: string): Observable<EADDto> {
              return of(mockEAD.ead);
            },
          },
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditImportDialogComponent);
    component = fixture.componentInstance;
    component.data = { import: mockImport };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit should load the correct export token and set the import token', () => {
    expect(component.importToken).toEqual(mockImport);
    expect(component.exportToken).toEqual(mockEAD.ead);
    expect(component.selection.length).toEqual(
      component.importToken.goodsItems.length
    );
  });

  describe('toggleConsignor should ', () => {
    it('set consignor equal to consignee if checked', () => {
      component.toggleConsignor({ checked: true } as MatSlideToggleChange);
      expect(component.consigneeEqualsConsignor).toBe(true);
      expect(component.importToken.consignor).toEqual(
        component.importToken.consignee
      );
    });

    it('set consignor to an empty company object if unchecked', () => {
      component.toggleConsignor({ checked: false } as MatSlideToggleChange);
      expect(component.consigneeEqualsConsignor).toBe(false);
      expect(component.importToken.consignor).toEqual(new Company());
    });
  });

  describe('toggleTransport should ', () => {
    it('set transportFromBorder equal to transportAtBorder if checked', () => {
      component.toggleTransport({ checked: true } as MatSlideToggleChange);
      expect(component.foreignEqualsDomestic).toBe(true);
      expect(component.importToken.transportFromBorder).toEqual(
        component.importToken.transportAtBorder
      );
    });

    it('set transportFromBorder to an empty transport object if unchecked', () => {
      component.toggleTransport({ checked: false } as MatSlideToggleChange);
      expect(component.foreignEqualsDomestic).toBe(false);
      expect(component.importToken.transportFromBorder).toEqual(
        new Transport()
      );
    });
  });

  it('get sum should calculate the correct sum', () => {
    component.importToken = component.data.import;
    component.goodsItems = component.importToken.goodsItems;
    component.selection = [true, true, false];
    component.getSum();
    expect(component.statisticalValueSum).toEqual(300);
    expect(component.packageCount).toEqual(200);
    expect(component.invoiceValueSum).toEqual(200);
    expect(component.grossMassSum).toEqual(4200);
    expect(component.netMassSum).toEqual(4000);
    expect(component.positionCount).toEqual(2);
  });

  describe('closeDialog should', () => {
    it('just close the dialog without data', () => {
      const importCopy = component.importToken;
      component.closeDialog(false);
      expect(component.importToken).toEqual(importCopy);
    });

    it('should set the values needed and return the import copy object', () => {
      component.selection = [true, true, false];
      component.closeDialog(true);
      expect(component.importToken.id).toEqual(component.data.import.id);
      expect(component.importToken.creator).toEqual(
        component.data.import.creator
      );
      expect(component.importToken.status).toEqual(
        component.data.import.status
      );
      expect(component.importToken.id).toEqual(component.data.import.id);
      expect(component.importToken.totalPackagesQuantity).toEqual(
        component.packageCount
      );
      expect(component.importToken.totalGrossMass).toEqual(
        component.grossMassSum.toString()
      );
      expect(component.importToken.totalAmountInvoiced).toEqual(
        component.invoiceValueSum
      );
      expect(component.importToken.goodsItemQuantity).toEqual(
        component.positionCount
      );
      expect(component.importToken.goodsItems.length).toEqual(2);
    });
  });

  describe('getGoodsItem should', () => {
    it('return null if sequence number is not defined', () => {
      expect(component.getGoodsItem(undefined)).toEqual(null);
    });

    it('return null if sequence number is defined and export token is not defined', () => {
      component.exportToken = undefined;
      expect(component.getGoodsItem(1)).toEqual(null);
    });

    it('should return the good item with the sequence number if sequence number is not undefined', () => {
      const goodItemExpectedValue = {
        amountInvoiced: 100,
        amountInvoicedCurrency: 'EUR',
        consigneeOrderNumber: '12',
        containerId: '12',
        countryOfOrigin: 'DE',
        documents: [],
        localClassificationCode: 0,
        marksNumbers: '0',
        typeOfPackages: 'RTL',
        valueAtBorder: 200,
        valueAtBorderCurrency: 'EUR',
        valueAtBorderExport: 90,
        valueAtBorderExportCurrency: 'EUR',
        descriptionOfGoods: 'Biegeleiste',
        grossMass: '2100',
        netMass: '2000',
        numberOfPackages: 100,
        harmonizedSystemSubheadingCode: 0,
        dangerousGoodsCode: 0,
        sequenceNumber: 1,
      };
      expect(component.getGoodsItem(1)).toEqual(goodItemExpectedValue);
    });
  });
});

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';

import {
  Company,
  EADDto,
  ImportEntity,
  Transport,
  GoodsItem,
} from '../../_entities';
import {
  CurrencyUtility,
  SUPPORTED_CURRENCIES,
  SUPPORTED_LOCALES,
} from '@base/src/app/shared/utils/utils.currency';
import { TranslateService } from '@ngx-translate/core';
import { TokenService } from '@base/src/app/shared/services/token.service';
import { HistoryService } from '@core/services/history.service';
import { FieldInformationDto } from '@border/api-interfaces/lib/entities/history/history.dto';
/*
 * A desktop component for editing import tokens
 */
@Component({
  selector: 'app-edit-import-dialog',
  templateUrl: './edit-import-dialog.component.html',
  styleUrls: ['./edit-import-dialog.component.scss'],
})
export class EditImportDialogComponent implements OnInit {
  /**
   * Is the consignee equals to consignor
   */
  consigneeEqualsConsignor = false;
  /**
   * Is the declarant equals to exporter
   */
  declarantEqualsExporter = false;
  /**
   * Are there additional transport costs
   */
  foreignTransportCosts = false;
  /**
   * Is the foreign transport the same as the domestic transport
   */
  foreignEqualsDomestic = false;

  // Sum Calculations
  /**
   * The statistical value of all goods
   */
  statisticalValueSum = 0;
  /**
   * The package count of all goods
   */
  packageCount = 0;
  /**
   * The net mass of all goods
   */
  netMassSum = 0;
  /**
   * The gross mass of all goods
   */
  grossMassSum = 0;
  /**
   * The invoice value of all goods
   */
  invoiceValueSum = 0;
  /**
   * The count of all positions selected for acceptance
   */
  positionCount = 0;

  /**
   * The Positions for the Import
   * Deep Copied so it can be edited without messing up the Import Token.
   */
  goodsItems: GoodsItem[] = [];
  /**
   * The import token which has been provided by the view
   */
  importToken: ImportEntity;

  /**
   * The export token which is referenced by the import process
   */
  exportToken: EADDto;

  /**
   * An array to save, which goods positions are selected
   */
  selection: boolean[] = [];

  /**
   * Border Currency utility to transform monetary values.
   */
  currUtil: CurrencyUtility = new CurrencyUtility();

  /**
   * Get enum of supported currencies to call from HTML context.
   */
  currencies = SUPPORTED_CURRENCIES;

  /**
   * Get enum of supported locales to call from HTML context.
   */
  locales = SUPPORTED_LOCALES;

  /**
   * The locale in which parts of the document should be formatted.
   */
  currentLocale: string;

  /**
   * Is editing of the ead allowed?
   */
  locked = true;

  /**
   * Field information for both tokens
   */
  exportTokenFieldInformation: FieldInformationDto[];
  importTokenFieldInformation: FieldInformationDto[];

  /**
   * Creates a edit import dialog popup component with the import as selected data
   * @param data The import data object
   * @param dialogRef Injects the material dialog service
   */
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { import: ImportEntity },
    public dialogRef: MatDialogRef<EditImportDialogComponent>,
    private readonly exportService: TokenService,
    readonly translateService: TranslateService,
    private historyService: HistoryService
  ) {
    this.currentLocale = translateService.currentLang;
  }

  /**
   * Initialize the import, get the referenced export token and set the selection array, calculate the sum afterwards
   */
  ngOnInit(): void {
    this.importToken = this.data.import;
    this.exportService
      .getEADFromBlockchain(this.importToken.relatedExportToken)
      .subscribe((res: EADDto) => {
        this.exportToken = res;
      });

    // Deep Copy the GoodsItems so the positions can be edited without messing up the Import Token
    const goodsItemsCopy = this.importToken.goodsItems.map((x) =>
      Object.assign({}, x)
    );
    goodsItemsCopy.forEach((element) => {
      element.valueAtBorder = this.currUtil.toMajorDenom(
        element.valueAtBorderCurrency,
        element.valueAtBorder
      );
      element.amountInvoiced = this.currUtil.toMajorDenom(
        this.importToken.invoiceCurrency,
        element.amountInvoiced
      );
    });
    this.goodsItems = goodsItemsCopy;
    this.importToken.goodsItems.forEach(() => this.selection.push(true));
    this.getSum();

    this.historyService
      .getHistoryOfToken(this.importToken.relatedExportToken)
      .subscribe((res) => {
        this.exportTokenFieldInformation = res;
      });

    this.historyService
      .getHistoryOfToken(this.importToken.id)
      .subscribe((res) => (this.importTokenFieldInformation = res));
  }

  /**
   * Toggles if the consignor equals the consignee
   * @param event - material slide toggle on change event
   */
  toggleConsignor(event: MatSlideToggleChange): void {
    this.consigneeEqualsConsignor = event.checked;

    if (event.checked) {
      this.importToken.consignor = this.importToken.consignee;
    } else {
      this.importToken.consignor = new Company();
    }
  }

  /**
   * Toggles if the consignor equals the consignee
   * @param event - material slide toggle on change event
   */
  toggleDeclarant(event: MatSlideToggleChange): void {
    this.declarantEqualsExporter = event.checked;

    if (event.checked) {
      this.importToken.declarant = this.importToken.exporter;
    } else {
      this.importToken.declarant = new Company();
    }
  }

  /**
   * Toggles if the transport from border equals the transport at border
   * @param event - material slide toggle on change event
   */
  toggleTransport(event: MatSlideToggleChange): void {
    this.foreignEqualsDomestic = event.checked;

    if (event.checked) {
      this.importToken.transportFromBorder = this.importToken.transportAtBorder;
    } else {
      this.importToken.transportFromBorder = new Transport();
    }
  }

  /**
   * Calculates all sums related to the goods items, takes the selected array into account for calculation
   */
  getSum(): void {
    this.resetValuesForSum();
    this.goodsItems.forEach((i: GoodsItem, index) => {
      if (this.selection[index]) {
        if (i.valueAtBorder) {
          this.statisticalValueSum += +i.valueAtBorder;
        }
        if (i.numberOfPackages) {
          this.packageCount += +i.numberOfPackages;
        }
        if (i.amountInvoiced) {
          this.invoiceValueSum += +i.amountInvoiced;
        }
        if (i.grossMass) {
          this.grossMassSum += +i.grossMass;
        }
        if (i.netMass) {
          this.netMassSum += +i.netMass;
        }
      }
    });
    this.positionCount = this.selection.filter(Boolean).length;
  }

  /**
   * Searches for a good item in export token good items array and return the item if one is found
   * @param sequenceNumber the sequence number to search for
   * @returns the good item with the given sequence number if found, otherwise undefined
   */
  getGoodsItem(sequenceNumber: number): GoodsItem {
    if (!this.exportToken || !sequenceNumber) {
      return null;
    }

    return this.exportToken.goodsItems.find(
      (goodsItem) => goodsItem.sequenceNumber === sequenceNumber
    );
  }

  /**
   * Reset all calculated values to 0
   */
  private resetValuesForSum(): void {
    this.statisticalValueSum = 0;
    this.packageCount = 0;
    this.invoiceValueSum = 0;
    this.grossMassSum = 0;
    this.netMassSum = 0;
    this.positionCount = 0;
  }

  /**
   * If the edit was aborted, nothing will happen and the dialog gets closed.
   * If the edit was saved:
   * - Copy the not editable data (ex creator, id, status) to the copy object
   * - filter the goods items for non selected and delete them
   * - close the dialog and return the new import data
   * @param withData should the changed be written
   * @returns void
   */
  closeDialog(withData: boolean): void {
    if (!withData) {
      this.dialogRef.close();
      return;
    }
    this.goodsItems = this.formatGoodItemsCurrencies();
    this.importToken.totalPackagesQuantity = this.packageCount;
    this.importToken.totalGrossMass = this.grossMassSum.toString();
    this.importToken.totalAmountInvoiced = this.invoiceValueSum;
    this.importToken.goodsItemQuantity = this.positionCount;
    this.importToken.goodsItems = this.goodsItems;

    this.dialogRef.close(this.importToken);
  }

  /**
   * Check if value is empty
   * @param value 
   * @returns The value or an empty sign
   */
  checkEmpty(value: any): any {
    if (value == undefined) return '∅';
    return value;
  }

  /**
   * Get information on secure level of the field, for the importer
   * @param fieldName 
   * @returns Secure level
   */
  getSecureLevelImport(
    fieldName: string
  ): 'secure' | 'company' | 'external' | 'loading' {
    if (
      !this.importTokenFieldInformation ||
      this.importTokenFieldInformation.length === 0
    )
      return 'loading';
    const field = this.importTokenFieldInformation.find(
      (fi) => fi.fieldName === fieldName
    );
    if (!field) return 'loading';
    return field.level;
  }

  /**
   * Get information on the last editor of the field, for the importer
   * @param fieldName 
   * @returns Last editor
   */
  getLastEditorImport(fieldName: string): string {
    if (!this.importTokenFieldInformation)
      return this.translateService.instant('history.loading');
    const field = this.importTokenFieldInformation.find(
      (fi) => fi.fieldName === fieldName
    );
    if (!field) return this.translateService.instant('history.unavailable');
    return field.lastEditor;
  }

  /**
   * Get information on secure level of the field, for the exporter
   * @param fieldName 
   * @returns Secure level
   */
  getSecureLevelExport(
    fieldName: string
  ): 'secure' | 'company' | 'external' | 'loading' {
    if (
      !this.exportTokenFieldInformation ||
      this.exportTokenFieldInformation.length === 0
    )
      return 'loading';
    const field = this.exportTokenFieldInformation.find(
      (fi) => fi.fieldName === fieldName
    );
    if (!field) return 'loading';
    return field.isEdited ? 'company' : 'secure';
  }

  /**
   * Get information on the last editor of the field, for the exporter
   * @param fieldName 
   * @returns Last editor
   */
  getLastEditorExport(fieldName: string): string {
    if (!this.exportTokenFieldInformation)
      return this.translateService.instant('history.loading');
    const field = this.exportTokenFieldInformation.find(
      (fi) => fi.fieldName === fieldName
    );
    if (!field) return this.translateService.instant('history.unavailable');
    return field.lastEditor;
  }

  /**
   * replace comma decimal separator with dot
   * @param value number containing comma
   * @returns converted value
   */
  formatDecimalSeparatorToDot(value: number): number {
    return +value.toString().replace(',', '.');
  }

  /**
   * format all currency inputs to cent and replaced comma sparator with dot
   * @returns formated goods item array
   */
  private formatGoodItemsCurrencies(): GoodsItem[] {
    this.invoiceValueSum = this.currUtil.toMinorDenom(
      this.importToken.invoiceCurrency,
      this.invoiceValueSum
    );
    const goodItems = [];
    this.selection.forEach((item, index) => {
      if (item) {
        const gooditem = this.goodsItems[index];
        gooditem.valueAtBorder = this.formatDecimalSeparatorToDot(
          gooditem.valueAtBorder
        );

        gooditem.valueAtBorder = this.currUtil.toMinorDenom(
          gooditem.valueAtBorderCurrency,
          gooditem.valueAtBorder
        );
        gooditem.amountInvoiced = this.formatDecimalSeparatorToDot(
          gooditem.amountInvoiced
        );
        gooditem.amountInvoiced = this.currUtil.toMinorDenom(
          this.importToken.invoiceCurrency,
          gooditem.amountInvoiced
        );

        goodItems.push(gooditem);
      }
    });
    return goodItems;
  }
}

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

/**
 * The popup component for confirming write and update operations to the blockchain
 */
@Component({
  selector: 'app-confirmation-popup',
  templateUrl: './confirmation-popup.component.html',
  styleUrls: ['./confirmation-popup.component.scss'],
})
export class ConfirmationPopupComponent {
  /**
   * Creates a confirmation popup with given text in data
   * @param data MAT_DIALOG_DATA object with text element which should be displayed
   */
  constructor(@Inject(MAT_DIALOG_DATA) public data: { heading: string; text: string }) {}
}

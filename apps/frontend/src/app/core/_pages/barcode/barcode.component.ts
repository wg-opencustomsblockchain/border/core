/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, HostListener, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';

/**
 * A component for showing the barcode
 */
@Component({
  selector: 'app-barcode',
  templateUrl: './barcode.component.html',
  styleUrls: ['./barcode.component.scss'],
})
export class BarcodeComponent implements OnInit {
  /**
   * Width of the QR code
   */
  width = window.innerWidth;

  /**
   * The current mrn selected from the query parameters
   */
  mrn$ = this.route.queryParams.pipe(map((p) => p['mrn']));

  /**
   * The current mrn selected from the query parameters
   */
  uuid = this.route.queryParams.pipe(map((p) => p['uuid']));

  /**
   * Creates an instance of barcode component
   * @param route injects the actual route
   */
  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    this.setQRCodeWidth();
  }

  private setQRCodeWidth() {
    this.width = window.innerWidth * 0.4;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.width = window.innerWidth * 0.4;
  }
}

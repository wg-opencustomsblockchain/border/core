/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Pipe, PipeTransform } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogModule,
} from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import {
  BrowserAnimationsModule,
  NoopAnimationsModule,
} from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import {
  TranslateLoader,
  TranslateModule,
  TranslatePipe,
} from '@ngx-translate/core';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { HttpLoaderFactory } from '@base/src/app/app.module';
import { SharedModule } from '@base/src/app/shared/shared.module';
import { mockEAD } from '@base/src/assets/mockups/EADFile.mock';
import { EADDto } from '@core/_entities/ead.entity';
import { LightClientService } from '@core/services/light-client.service';
import { TokenService } from '@shared/services/token.service';
import {
  DialogConfirmWriteComponent,
  ReleaseComponent,
} from './release.component';
import { KeycloakService } from 'keycloak-angular';
import { LoggerTestingModule } from 'ngx-logger/testing';
import { EMPTY } from 'rxjs';

import { ImportEntity } from '../../_entities';

describe('ReleaseComponent', () => {
  let component: ReleaseComponent;
  let dialogComponent: DialogConfirmWriteComponent;
  let fixture: ComponentFixture<ReleaseComponent>;
  let dialogFixture: ComponentFixture<DialogConfirmWriteComponent>;
  let http: true;
  let dialog: MatDialog;
  let toastr: ToastrService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        MatDialogModule,
        BrowserAnimationsModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [http],
          },
        }),
        ToastrModule.forRoot(),
        SharedModule,
        NoopAnimationsModule,
        LoggerTestingModule,
      ],
      declarations: [ReleaseComponent, TranslatePipeMock],
      providers: [
        { provide: TokenService, useClass: TokenService },
        {
          provide: MAT_DIALOG_DATA,
          useValue: { importToken: {} as ImportEntity },
        },
        { provide: TranslatePipe, useClass: TranslatePipeMock },
        { provide: http, useValue: true },
        { provide: LightClientService, useClass: LightClientService },
        { provide: KeycloakService, useValue: {} },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReleaseComponent);
    dialogFixture = TestBed.createComponent(DialogConfirmWriteComponent);
    component = fixture.componentInstance;
    dialogComponent = dialogFixture.componentInstance;
    fixture.detectChanges();
    dialogFixture.detectChanges();
    dialog = fixture.debugElement.injector.get(MatDialog);
    toastr = TestBed.inject(ToastrService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create Dialog', () => {
    expect(dialogComponent).toBeTruthy();
  });

  describe('if the dialog get called', () => {
    it('the dataStream should get filled with the given information', async () => {
      //   jest.spyOn(eadService, 'getMissingEAD').mockReturnValue(of([]));
      const openDialogSpy = jest.spyOn(dialog, 'open').mockReturnValue({
        afterClosed: () => EMPTY,
      } as any);
      const toastrSpy = jest.spyOn(toastr, 'success');
      await component.openConfirmDialog();
      expect(dialogComponent.dataStream).toBeTruthy();
      expect(openDialogSpy).toHaveBeenCalled();
      expect(toastrSpy).toHaveBeenCalledTimes(0);
      expect(component.selection.selected).toEqual([]);
    });

    it('should call mat table data source with ImportEntity and set isImport to true', async () => {
      const openDialogSpy = jest.spyOn(dialog, 'open').mockReturnValue({
        afterClosed: () => EMPTY,
      } as any);
      const toastrSpy = jest.spyOn(toastr, 'success');
      await component.openConfirmDialog();
      expect(dialogComponent.dataStream).toBeTruthy();
      expect(dialogComponent.isImport).toBe(true);
    });
  });

  describe('isAllSelected()', () => {
    it('should return true if all is selected', () => {
      expect(component.isAllSelected()).toBe(true);
    });
  });

  describe('masterToggle() should', () => {
    it('select all rows if they are not selected', () => {
      jest.spyOn(component, 'isAllSelected').mockReturnValue(false);
      component.dataSource = new MatTableDataSource<EADDto>();
      component.masterToggle();
      expect(component.dataSource.data.length).toEqual(
        component.selection.selected.length
      );
    });
    it('clear selection otherwise', () => {
      jest.spyOn(component, 'isAllSelected').mockReturnValue(true);
      component.dataSource = new MatTableDataSource<EADDto>();
      component.masterToggle();
      expect(component.dataSource.data.length).toEqual(0);
    });
  });

  describe('checkboxLabel() should return', () => {
    it('"select all" if no row is defined and all checkboxes are selected', () => {
      jest.spyOn(component, 'isAllSelected').mockReturnValue(true);
      expect(component.checkboxLabel()).toEqual('select all');
    });
    it('"deselect all" if no row is defined and not all checkboxes are selected', () => {
      jest.spyOn(component, 'isAllSelected').mockReturnValue(false);
      expect(component.checkboxLabel()).toEqual('deselect all');
    });
    it('"select row" if a row is defined and not all checkboxes are selected', () => {
      jest.spyOn(component, 'isAllSelected').mockReturnValue(false);
      expect(component.checkboxLabel(mockEAD.ead)).toEqual(
        `select row ${mockEAD.ead.exportId + 1}`
      );
    });
    it('"select row" if a row is defined and all checkboxes are selected', () => {
      jest.spyOn(component, 'isAllSelected').mockReturnValue(true);
      expect(component.checkboxLabel(mockEAD.ead)).toEqual(
        `select row ${mockEAD.ead.exportId + 1}`
      );
    });
  });

  describe('when the component initializes', () => {
    it('the list of eads should be fetched', () => {
      //jest.spyOn(eadService, 'getMissingEAD').mockReturnValue(of([]));
      component.ngOnInit();
      expect(component.dataSource).toBeTruthy();
    });
  });
});

@Pipe({
  name: 'translate',
})
export class TranslatePipeMock implements PipeTransform {
  public name = 'translate';

  public transform(query: string, ...args: any[]): any {
    return query;
  }
}

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { SelectionModel } from '@angular/cdk/collections';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { TokenService } from '@base/src/app/shared/services/token.service';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { LoadingComponent } from '../../_components/loading/loading.component';
import { SpinnerComponent } from '../../_components/spinner/spinner.component';
import { ImportEntity } from '../../_entities';
import { EADFile } from '../../_entities/ead-file.entity';
import { EADDto } from '../../_entities/ead.entity';
import { ExportStatus } from '../../_enums/status.enum';

/**
 * The release component
 */
@Component({
  selector: 'app-release',
  templateUrl: './release.component.html',
  styleUrls: ['./release.component.scss'],
})
export class ReleaseComponent implements OnInit {
  /**
   * Displayed columns
   */
  displayedColumns: string[] = [
    'select',
    'mrn',
    'destinationCountry',
    'consignee',
    'customOfficeOfExit',
    'incotermCode',
  ];
  /**
   * Data source to display
   */
  dataSource: MatTableDataSource<EADDto> = new MatTableDataSource();
  /**
   * The selection
   */
  selection = new SelectionModel<EADDto>(true, []);

  /**
   * Creates an instance of release page component
   * @param eadService injects ead service
   * @param dialog injects material dialog service
   * @param toastr injects toastr service
   * @param translateService injects translate service
   */
  constructor(
    private readonly eadService: TokenService,
    private readonly dialog: MatDialog,
    private toastr: ToastrService,
    private translateService: TranslateService
  ) {}

  /**
   * Loads the missing eads
   */
  ngOnInit(): void {
    this.updateMissingEADs();
  }

  /**
   * Get all eads that are not already written to the blockchain
   */
  updateMissingEADs(): void {
    // loading dialog
    this.dialog.open(SpinnerComponent, { panelClass: 'loading-container' });

    this.dataSource = new MatTableDataSource();
    this.eadService.getWrittenEAD().subscribe((res) => {
      res.forEach((ead: EADDto) => {
        if (ead.status === ExportStatus.NEW) this.dataSource.data.push(ead);
        this.dataSource._updateChangeSubscription();
      });
      this.dialog.closeAll();
    });
  }

  /**
   * Whether the number of selected elements matches the total number of rows.
   */
  isAllSelected(): boolean {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource?.data.length;
    return numSelected === numRows;
  }

  /**
   * Selects all rows if they are not all selected; otherwise clear selection.
   */
  masterToggle(): void {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach((row: EADDto) =>
          this.selection.select(row)
        );
  }

  /**
   * The label for the checkbox on the passed row
   */
  checkboxLabel(row?: EADDto): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
      row.exportId + 1
    }`;
  }

  /**
   * Opens the confirmation dialog and writes the ead with status READY to the blockchain if confirmation is successful
   */
  openConfirmDialog(): void {
    const selectedEads: EADDto[] = [];
    this.selection.selected.forEach((row) => {
      selectedEads.push(row);
    });
    const dialogRef = this.dialog.open(DialogConfirmWriteComponent, {
      data: {
        eads: selectedEads,
        text: this.translateService.instant('dialog.confirm.write.text'),
      },
      width: '80vw',
    });

    dialogRef.afterClosed().subscribe(async (result) => {
      if (result) {
        this.dialog.open(LoadingComponent, {
          data: {
            heading: this.translateService.instant('loading.component.heading'),
            text: this.translateService.instant('loading.component.text'),
          },
          width: '80vw',
          disableClose: true,
        });

        await this.eadService
          .acceptExportToken(this.selection.selected)
          .catch(() => {
            this.selection.clear();
            this.dialog.closeAll();
            this.toastr.error(
              this.translateService.instant('error.write-failed')
            );
            return;
          });

        this.toastr.success(
          `${this.selection.selected.length} ${this.translateService.instant(
            'release.write.multiple'
          )}`
        );
        this.dialog.closeAll();
        this.updateMissingEADs();
        this.selection.clear();
        return;
      }
    });
  }
}

/**
 * A confirm popup component
 */
@Component({
  selector: 'app-dialog-confirm-write',
  templateUrl: './dialog-confirm-write.html',
  styleUrls: ['./release.component.scss'],
})
export class DialogConfirmWriteComponent {
  isImport: boolean;

  /**
   * Displayed columns for Import Token
   */
  displayedColumnsImport: string[] = [
    'id',
    'destinationCountry',
    'exporter',
    'customOfficeOfExit',
    'incotermCode',
  ];

  /**
   * Displayed columns for Export Token
   */
  displayedColumns: string[] = [
    'mrn',
    'destinationCountry',
    'consignee',
    'customOfficeOfExit',
    'incotermCode',
  ];
  /**
   * Displayed data
   */
  dataStream: MatTableDataSource<EADFile | ImportEntity>;
  /**
   * Text to show
   */
  text: string;

  /**
   * Creates an instance of the confirm write popup component
   * @param data data object
   */
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
    if (data.importToken) {
      this.isImport = true;
      this.dataStream = new MatTableDataSource<ImportEntity>(data.importToken);
    } else {
      this.dataStream = new MatTableDataSource<EADFile>(data.eads);
    }
    this.text = data.text;
  }
}

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { routes } from '@base/src/app/app-routing.module';
import { HttpLoaderFactory } from '@base/src/app/app.module';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { LoggerTestingModule } from 'ngx-logger/testing';

import { CodeInputComponent } from './code-input.component';

describe('CodeInputComponent', () => {
  let component: CodeInputComponent;
  let fixture: ComponentFixture<CodeInputComponent>;
  let router: Router;
  const http = true;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CodeInputComponent],
      imports: [
        LoggerTestingModule,
        RouterTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [http],
          },
        }),
      ],
      providers: [{ provide: http, useValue: true }],
    }).compileComponents();

    fixture = TestBed.createComponent(CodeInputComponent);
    router = TestBed.get(Router);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call navigate if error is false', () => {
    const code = '1234';
    component.code = code;
    const naviagationSpy = jest.spyOn(router, 'navigateByUrl');
    component.enterMRN();
    expect(naviagationSpy).toHaveBeenCalledWith(`/check-transport/${code}`);
  });

  it('should set error to true if code is undefined', () => {
    component.code = undefined;
    component.enterMRN();
    expect(component.error).toBeTruthy();
  });
  it('should set error to true if code is empty String', () => {
    component.code = '';
    component.enterMRN();
    expect(component.error).toBeTruthy();
  });
  it('should set error to true if code contains space', () => {
    component.code = ' ';
    component.enterMRN();
    expect(component.error).toBeTruthy();
  });
});

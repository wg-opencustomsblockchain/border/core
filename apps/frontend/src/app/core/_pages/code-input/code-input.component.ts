/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-code-input',
  templateUrl: './code-input.component.html',
  styleUrls: ['./code-input.component.scss'],
})
export class CodeInputComponent {
  code: string;
  error = false;

  constructor(private router: Router) {}

  /**
   * Navigates application via the mrn, which has to be input
   */
  public enterMRN(): void {
    if (!this.code || this.code === '' || this.code.includes(' ')) {
      this.error = true;
    } else {
      this.router.navigateByUrl(`/check-transport/${this.code}`);
    }
  }
}

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { LookupPipe } from '@base/src/app/shared/_pipes/lookup.pipe';
import { TranslateModule } from '@ngx-translate/core';
import { KeycloakService } from 'keycloak-angular';
import { AvatarModule } from 'ngx-avatars';
import { CustomsLookupPipeMock } from '../../_components/detail/detail.component.test';
import { ReleaseComponent } from '../release/release.component';
import { TranslatePipeMock } from '../release/release.component.test';
import { OverlayModule } from '@angular/cdk/overlay';
import { LoggerTestingModule } from 'ngx-logger/testing';

import { SelectionComponent } from './selection.component';

describe('SelectionComponent', () => {
  let component: SelectionComponent;
  let fixture: ComponentFixture<SelectionComponent>;
  let router: Router;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([
          {
            path: 'release',
            component: ReleaseComponent,
          },
        ]),
        HttpClientTestingModule,
        TranslateModule.forRoot(),
        AvatarModule,
        MatDialogModule,
        LoggerTestingModule,
      ],
      declarations: [SelectionComponent, CustomsLookupPipeMock],
      providers: [
        { provide: KeycloakService, useClass: KeycloakService },
        { provide: LookupPipe, useClass: CustomsLookupPipeMock },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectionComponent);
    component = fixture.componentInstance;
    router = TestBed.inject<Router>(Router);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('when navigateTo() is called', () => {
    it('then the correct route should be displayed', () => {
      const navigateSpy = jest.spyOn(router, 'navigateByUrl');
      component.navigateTo('/release');
      expect(navigateSpy).toHaveBeenCalledWith('/release');
    });
  });
});

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { TokenService } from '@base/src/app/shared/services/token.service';
import { UserRole, User } from '@border/api-interfaces';
import { KeycloakService } from 'keycloak-angular';
import { BorderApiService } from '../../services/import.service';
import { SpinnerComponent } from '../../_components/spinner/spinner.component';
import { EADDto, ImportEntity } from '../../_entities';

@Component({
  selector: 'app-selection',
  templateUrl: './selection.component.html',
  styleUrls: ['./selection.component.scss'],
})
export class SelectionComponent implements OnInit {
  /**
   * The current user
   */
  currentUser!: User | null;

  userRoles: string[] = [];
  UserRole = UserRole;

  missingCount: number;
  writtenCount: number;
  lastUpdatedToken: EADDto | ImportEntity;

  /**
   * Creates an instance of a selection page component
   * @param authService injects auth service
   * @param router injects router
   */
  constructor(
    private readonly authService: KeycloakService,
    private readonly dialog: MatDialog,
    private readonly router: Router,
    private tokenService: TokenService,
    private importService: BorderApiService
  ) {}
  /**
   * Load the current user
   */
  async ngOnInit(): Promise<void> {
    // open loading dialog
    this.dialog.open(SpinnerComponent, { panelClass: 'loading-container' });

    // Get the current user
    this.currentUser = await this.authService.loadUserProfile();
    this.userRoles = this.authService.getUserRoles();

    if (this.hasRole(UserRole.EXPORTER)) {
      this.tokenService.getWrittenEAD().subscribe((res) => {
        this.writtenCount = res.length;
        if (this.writtenCount === 0) return;
        this.lastUpdatedToken = res.sort(
          (a, b) => new Date(b.timestamp).getTime() - new Date(a.timestamp).getTime()
        )[0];

        // close loading dialog
        this.dialog.closeAll();
      });
    }

    if (this.hasRole('IMPORTER')) {
      this.importService.getActiveImports().subscribe((res) => {
        this.writtenCount = res.length;
        if (this.writtenCount === 0) {
          // close loading dialog
          this.dialog.closeAll();
          return;
        }
        this.lastUpdatedToken = res.sort(
          (a, b) => new Date(b.timestamp).getTime() - new Date(a.timestamp).getTime()
        )[0];
        this.tokenService.getEadsReadyForImport(this.currentUser.username).subscribe((exports) => {
          const alreadyImported = exports.filter((ead) => res.some((vendor) => vendor.relatedExportToken === ead.id));
          this.missingCount = exports.length - alreadyImported.length;

          // close loading dialog
          this.dialog.closeAll();
        });
      });
    }

    if (this.hasRole(UserRole.DRIVER)) {
      this.dialog.closeAll();
    }
  }

  /**
   * Navigates to the given route
   * @param route route string
   */
  navigateTo(route: string): void {
    this.router.navigateByUrl(route);
  }

  getRoleName(): string {
    const rolesArray = ['IMPORTER', UserRole.EXPORTER, UserRole.DRIVER];
    let returnValue = '';
    rolesArray.forEach((role: string) => {
      if (this.userRoles.includes(role)) returnValue = role;
    });
    return returnValue;
  }

  hasRole(role: string) {
    return this.userRoles.includes(role);
  }
}

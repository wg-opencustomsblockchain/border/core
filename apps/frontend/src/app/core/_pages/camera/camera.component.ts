/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { BarcodeFormat } from '@zxing/library';
import { BehaviorSubject } from 'rxjs';

import { SpinnerComponent } from '../../_components/spinner/spinner.component';
import { EADDto } from '../../_entities/ead.entity';

/**
 * A component for scanning the qr code
 */
@Component({
  selector: 'app-camera',
  templateUrl: './camera.component.html',
  styleUrls: ['./camera.component.scss'],
})
export class CameraComponent implements OnInit {
  /**
   * Was an error detected
   */
  hasShownError = false;
  /**
   * The result of the scan
   */
  qrResultString = '';
  /**
   * The ead which had the mrn of the result
   */
  selection: EADDto = new EADDto();

  /**
   * The available camera devices
   */
  availableDevices: MediaDeviceInfo[] = [];
  /**
   * The activated current camera
   */
  currentDevice: any = null;

  /**
   * The formats which can be scanned
   */
  formatsEnabled: BarcodeFormat[] = [
    BarcodeFormat.CODE_128,
    BarcodeFormat.DATA_MATRIX,
    BarcodeFormat.EAN_13,
    BarcodeFormat.QR_CODE,
  ];

  /**
   * Has the device a camera
   */
  hasDevices = false;
  /**
   * Do we have permission to access the camera
   */
  hasPermission = false;

  /**
   * Is the torch enabled
   */
  torchEnabled = false;
  /**
   * Subscribe to the torch subject
   */
  torchAvailable$ = new BehaviorSubject<boolean>(false);
  /**
   * Should we try harder to read the barcode
   */
  tryHarder = false;

  /**
   * Are we scanning right now
   */
  scanning = true;
  /**
   * Has the position been accepted by the user
   */
  positionAccepted = false;

  /**
   * Creates an instance of barcode component
   * @param dialog injects material dialog service
   * @param router injects router
   * @param toastr injects toastr service
   * @param eadService injects ead service
   * @param translateService injects translate service
   */
  constructor(private dialog: MatDialog, private router: Router) {}

  ngOnInit() {
    this.dialog.open(SpinnerComponent, { panelClass: 'loading-container' });
  }

  /**
   * Set available camera devices
   * @param devices the device list
   */
  onCamerasFound(devices: MediaDeviceInfo[]): void {
    this.availableDevices = devices;
    this.hasDevices = Boolean(devices && devices.length);

    this.dialog.closeAll();
  }

  /**
   * If and code is scanned
   * @param result result of the scanned code
   */
  onCodeResult(result: string): void {
    this.router.navigateByUrl(`check-transport/${result}`);
    this.finishScanProcess();
  }

  /**
   * If the selected camera device is changed
   * @param selected selected device
   */
  onDeviceSelectChange(selected: string): void {
    const device = this.availableDevices.find((x) => x.deviceId === selected);
    this.currentDevice = device || null;
  }

  /**
   * If permission is granted to use the device
   * @param has permission granted?
   */
  onHasPermission(has: boolean): void {
    this.hasPermission = has;
  }

  /**
   * Enable/Disable torch
   */
  toggleTorch(): void {
    this.torchEnabled = !this.torchEnabled;
  }

  /**
   * Enable tryHarder
   */
  toggleTryHarder(): void {
    this.tryHarder = true;
  }

  /**
   * Reset this view
   */
  finishScanProcess(): void {
    this.scanning = true;
    this.positionAccepted = false;
    this.tryHarder = false;
    this.selection = new EADDto();
  }
}

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpLoaderFactory } from '@base/src/app/app.module';
import { EADDto } from '@core/_entities/ead.entity';
import { LightClientService } from '@core/services/light-client.service';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TokenService } from '@shared/services/token.service';

import { CheckTransportComponent } from '../check-transport/check-transport.component';
import { SelectionComponent } from '../selection/selection.component';
import { CameraComponent } from './camera.component';
import { KeycloakService } from 'keycloak-angular';
import { BreadcrumbComponent } from '@base/src/app/shared/breadcrumb/breadcrumb.component';
import { ZXingScannerModule } from '@zxing/ngx-scanner';
import { SharedModule } from '@shared/shared.module';
import {ToastrModule} from "ngx-toastr";

describe('CameraComponent', () => {
  let component: CameraComponent;
  let fixture: ComponentFixture<CameraComponent>;
  let router: Router;
  const http = true;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CameraComponent, BreadcrumbComponent],
      imports: [
        HttpClientTestingModule,
        MatDialogModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [http],
          },
        }),
        RouterTestingModule.withRoutes([{ path: 'selection', component: SelectionComponent }]),

        ToastrModule.forRoot(),
        RouterTestingModule.withRoutes([
          { path: 'selection', component: SelectionComponent },
          { path: 'check-transport:id', component: CheckTransportComponent },
        ]),
        ZXingScannerModule,
        SharedModule,
        NoopAnimationsModule,
      ],
      providers: [
        {
          provide: TokenService,
          useClass: TokenService,
        },
        { provide: http, useValue: true },
        { provide: LightClientService, useClass: LightClientService },
        { provide: KeycloakService, useValue: {} },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CameraComponent);
    component = fixture.componentInstance;
    router = TestBed.get(Router);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should correctly set availableDevices and hasDevices', () => {
    const devices: MediaDeviceInfo[] = [
      {
        deviceId: '',
        groupId: '',
        label: '',
      } as MediaDeviceInfo,
    ];
    component.onCamerasFound(devices);
    expect(component.availableDevices.length).toEqual(1);
    expect(component.hasDevices).toEqual(true);
  });
  it('should correctly set change devices', () => {
    const devices: MediaDeviceInfo[] = [
      {
        deviceId: '1',
        groupId: '1',
        label: '1',
      } as MediaDeviceInfo,
      {
        deviceId: '2',
        groupId: '2',
        label: '2',
      } as MediaDeviceInfo,
    ];
    component.onCamerasFound(devices);
    expect(component.hasDevices).toEqual(true);
    component.onDeviceSelectChange('2');
    expect(component.currentDevice.deviceId).toEqual('2');

    component.onDeviceSelectChange('');
    expect(component.currentDevice).toEqual(null);
  });

  it('should succeed onCodeResult()', () => {
    const result = '1234';
    const handleResultSpy = jest.spyOn(router, 'navigateByUrl');
    component.onCodeResult(result);
    expect(handleResultSpy).toHaveBeenCalledWith(`check-transport/${result}`);
  });

  it('should set permission', () => {
    component.onHasPermission(true);
    expect(component.hasPermission).toEqual(true);
  });

  it('should toggle torch', () => {
    const torchState = component.torchEnabled;
    component.toggleTorch();
    expect(component.torchEnabled).toEqual(!torchState);
  });

  it('should toggle try harder', () => {
    component.toggleTryHarder();
    expect(component.tryHarder).toEqual(true);
  });

  it('should finish the scan process correctly', () => {
    component.finishScanProcess();
    expect(component.scanning).toEqual(true);
    expect(component.positionAccepted).toEqual(false);
    expect(component.tryHarder).toEqual(false);
    expect(component.selection).toEqual(new EADDto());
  });
});

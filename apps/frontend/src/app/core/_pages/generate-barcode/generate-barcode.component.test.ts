/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Pipe, PipeTransform } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MAT_DIALOG_DATA, MatDialogModule } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import {
  BrowserAnimationsModule,
  NoopAnimationsModule,
} from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import {
  TranslateLoader,
  TranslateModule,
  TranslatePipe,
} from '@ngx-translate/core';
import { ToastrModule } from 'ngx-toastr';
import { of } from 'rxjs';
import { HttpLoaderFactory } from '@base/src/app/app.module';
import { mockEAD } from '@base/src/assets/mockups/EADFile.mock';

import { EADDto } from '@core/_entities/ead.entity';
import { LightClientService } from '@core/services/light-client.service';
import { TokenService } from '@shared/services/token.service';
import { DialogConfirmWriteComponent } from '../release/release.component';
import { GenerateBarcodeComponent } from './generate-barcode.component';
import { KeycloakService } from 'keycloak-angular';
import { SharedModule } from '@base/src/app/shared/shared.module';
import { BreadcrumbComponent } from '@base/src/app/shared/breadcrumb/breadcrumb.component';
import { LoggerTestingModule } from 'ngx-logger/testing';

describe('Generate Barcode Component', () => {
  let component: GenerateBarcodeComponent;
  let dialogComponent: DialogConfirmWriteComponent;
  let fixture: ComponentFixture<GenerateBarcodeComponent>;
  let dialogFixture: ComponentFixture<DialogConfirmWriteComponent>;
  let http: true;
  let eadService: TokenService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        MatDialogModule,
        BrowserAnimationsModule,
        RouterTestingModule,
        ToastrModule.forRoot(),
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [http],
          },
        }),
        SharedModule,
        NoopAnimationsModule,
        LoggerTestingModule,
      ],
      declarations: [
        GenerateBarcodeComponent,
        TranslatePipeMock,
        BreadcrumbComponent,
      ],
      providers: [
        { provide: TokenService, useClass: TokenService },
        { provide: TranslatePipe, useClass: TranslatePipeMock },
        { provide: MAT_DIALOG_DATA, useValue: {} },
        { provide: http, useValue: true },
        { provide: LightClientService, useClass: LightClientService },
        { provide: KeycloakService, useValue: {} },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateBarcodeComponent);
    dialogFixture = TestBed.createComponent(DialogConfirmWriteComponent);
    component = fixture.componentInstance;
    dialogComponent = dialogFixture.componentInstance;
    fixture.detectChanges();
    dialogFixture.detectChanges();
    eadService = TestBed.inject(TokenService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create Dialog', () => {
    expect(dialogComponent).toBeTruthy();
  });

  describe('if the dialog get called', () => {
    it('the dataStream should get filled with the given information', () => {
      //  jest.spyOn(eadService, 'getMissingEAD').mockReturnValue(of([]));
      component.openConfirmDialog();
      expect(dialogComponent.dataStream).toBeTruthy();
    });
  });

  it('should return true if all is selected', () => {
    expect(component.isAllSelected()).toBe(true);
  });

  describe('masterToggle() should', () => {
    it('select all rows if they are not selected', () => {
      jest.spyOn(component, 'isAllSelected').mockReturnValue(false);
      component.dataSource = new MatTableDataSource<EADDto>();
      component.masterToggle();
      expect(component.dataSource.data.length).toEqual(
        component.selection.selected.length
      );
    });
    it('clear selection otherwise', () => {
      jest.spyOn(component, 'isAllSelected').mockReturnValue(true);
      component.dataSource = new MatTableDataSource<EADDto>();
      component.masterToggle();
      expect(component.dataSource.data.length).toEqual(0);
    });
  });

  describe('checkboxLabel() should return', () => {
    it('"select all" if no row is defined and all checkboxes are selected', () => {
      jest.spyOn(component, 'isAllSelected').mockReturnValue(true);
      expect(component.checkboxLabel()).toEqual('select all');
    });
    it('"deselect all" if no row is defined and not all checkboxes are selected', () => {
      jest.spyOn(component, 'isAllSelected').mockReturnValue(false);
      expect(component.checkboxLabel()).toEqual('deselect all');
    });
    it('"select row" if a row is defined and not all checkboxes are selected', () => {
      jest.spyOn(component, 'isAllSelected').mockReturnValue(false);
      expect(component.checkboxLabel(mockEAD.ead)).toEqual(
        `select row ${mockEAD.ead.exportId + 1}`
      );
    });
    it('"select row" if a row is defined and all checkboxes are selected', () => {
      jest.spyOn(component, 'isAllSelected').mockReturnValue(true);
      expect(component.checkboxLabel(mockEAD.ead)).toEqual(
        `select row ${mockEAD.ead.exportId + 1}`
      );
    });
  });

  describe('when the component initializes', () => {
    it('the list of eads should be fetched', () => {
      //jest.spyOn(eadService, 'getMissingEAD').mockReturnValue(of([]));
      component.ngOnInit();
      expect(component.dataSource).toBeTruthy();
    });
  });

  // Testing for positive behaviour
  it('should handle EAD update properly', () => {
    // Testing for Positive behaviour
    component.handleEADUpdate(mockEAD.ead);
  });

  // Testing for negative behaviour
  it('should handle EAD update failures properly', () => {
    const falseEAD: EADDto = JSON.parse(JSON.stringify(mockEAD.ead));
    falseEAD.exportId = 'empty';
    component.handleEADUpdate(falseEAD);
  });
});

@Pipe({
  name: 'translate',
})
export class TranslatePipeMock implements PipeTransform {
  public name = 'translate';

  public transform(query: string, ...args: any[]): any {
    return query;
  }

  public get(query: string) {
    return query;
  }
}

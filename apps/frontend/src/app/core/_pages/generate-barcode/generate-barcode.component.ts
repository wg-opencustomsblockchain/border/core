/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { animate, state, style, transition, trigger } from '@angular/animations';
import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';

import { EADDto } from '../../_entities/ead.entity';
import { DialogConfirmWriteComponent } from '../release/release.component';
import { ExportEventTypes } from '../../_enums/event-types.enum';
import { TokenService } from '@base/src/app/shared/services/token.service';
import { SpinnerComponent } from '../../_components/spinner/spinner.component';

/**
 * A component for selecting, generating and displaying a barcode
 */
@Component({
  selector: 'app-generate-barcode',
  templateUrl: '../release/release.component.html',
  styleUrls: ['../release/release.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class GenerateBarcodeComponent implements OnInit {
  /**
   * The table columns to display
   */
  displayedColumns: string[] = [
    'select',
    'mrn',
    'destinationCountry',
    'consignee',
    'customOfficeOfExit',
    'incotermCode',
    'editedAt',
  ];
  /**
   * The data source
   */
  dataSource: MatTableDataSource<EADDto> = new MatTableDataSource();
  /**
   * The selected export token
   */
  selection = new SelectionModel<EADDto>(false, []);

  /**
   *
   * @param eadService injects ead service
   * @param dialog injects material dialog service
   * @param router injects router
   * @param toastr injects toastr service
   * @param translateService injects translate service
   */
  constructor(
    private readonly eadService: TokenService,
    private readonly dialog: MatDialog,
    private readonly router: Router,
    private readonly toastr: ToastrService,
    private translateService: TranslateService
  ) {}

  /**
   * Load the written export tokens from the blockchain
   */
  ngOnInit(): void {
    // loading dialog
    this.dialog.open(SpinnerComponent, { panelClass: 'loading-container' });

    this.eadService.getWrittenEAD().subscribe((res: EADDto[]) => {
      res = res.filter((ead) => [1].includes(Number.parseInt(ead.status)));
      this.dataSource = new MatTableDataSource<EADDto>(res);

      // close loading dialog
      this.dialog.closeAll();
    });
  }

  /**
   * Whether the number of selected elements matches the total number of rows.
   */
  isAllSelected(): boolean {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource?.data.length;
    return numSelected === numRows;
  }

  /**
   * Selects all rows if they are not all selected; otherwise clear selection.
   */
  masterToggle(): void {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach((row: EADDto) => this.selection.select(row));
  }

  /**
   * The label for the checkbox on the passed row
   */
  checkboxLabel(row?: EADDto): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.exportId + 1}`;
  }

  /**
   * Updates the status of the ead
   * @param ead the ead
   */
  handleEADUpdate(ead: EADDto): void {
    ead.eventType = ExportEventTypes.EXPORT_READY;
    this.eadService
      .updateExportTokenEventLocal(ead)
      .then(() => this.toastr.success(this.translateService.instant('toaster.update.success')))
      .catch((result: Error) =>
        this.toastr.error(this.translateService.instant(result.message), '', {
          disableTimeOut: true,
        })
      )
      .finally(() => this.dialog.closeAll());
  }

  /**
   * Opens a confirm dialog
   */
  openConfirmDialog(): void {
    const dialogRef = this.dialog.open(DialogConfirmWriteComponent, {
      data: {
        eads: this.selection.selected,
        text: this.translateService.instant('dialog.confirm.barcode.show'),
      },
      width: '80vw',
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.selection.selected.forEach((item: EADDto) => {
          this.handleEADUpdate(item);
          this.router.navigate([`/barcode`], {
            queryParams: { uuid: item.id },
          });
        });
      }
    });
  }
}

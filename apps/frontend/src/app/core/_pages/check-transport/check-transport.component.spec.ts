/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpLoaderFactory } from '@base/src/app/app.module';
import { TokenService } from '@base/src/app/shared/services/token.service';
import { mockEAD } from '@base/src/assets/mockups/EADFile.mock';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { KeycloakService } from 'keycloak-angular';
import { LoggerTestingModule } from 'ngx-logger/testing';
import { ActiveToast, ToastrModule, ToastrService } from 'ngx-toastr';
import { EMPTY, firstValueFrom, of, throwError } from 'rxjs';

import { EADDto } from '../../_entities';
import { LightClientService } from '../../services/light-client.service';
import { SelectionComponent } from '../selection/selection.component';
import { CheckTransportComponent } from './check-transport.component';

describe('CheckTransportComponent', () => {
  let component: CheckTransportComponent;
  let fixture: ComponentFixture<CheckTransportComponent>;
  let eadService: TokenService;
  let toastrService: ToastrService;
  let dialog: MatDialog;
  const http = true;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CheckTransportComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { snapshot: { params: { id: mockEAD.ead.exportId } } },
        },
        { provide: KeycloakService, useValue: {} },
        { provide: LightClientService, useClass: LightClientService },
        {
          provide: TokenService,
          useClass: TokenService,
        },
        { provide: http, useValue: true },
      ],
      imports: [
        LoggerTestingModule,
        HttpClientTestingModule,
        BrowserAnimationsModule,
        MatDialogModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [http],
          },
        }),
        RouterTestingModule.withRoutes([
          { path: 'selection', component: SelectionComponent },
          { path: 'check-transport:id', component: CheckTransportComponent },
        ]),
        ToastrModule.forRoot(),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckTransportComponent);
    eadService = TestBed.inject(TokenService);
    toastrService = TestBed.inject(ToastrService);
    dialog = fixture.debugElement.injector.get(MatDialog);

    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // Testing for positive behaviour
  it('should handle EAD request properly', () => {
    const mockResponse = mockEAD.ead;
    jest.spyOn(eadService, 'getEADFromBlockchain').mockReturnValue(of(mockResponse));

    component.ngOnInit();
    expect(component.selection).toEqual(mockEAD.ead);
    expect(component.selection.exportId).toEqual(mockEAD.ead.exportId);
  });

  // Testing for negative behaviour
  it('should handle EAD request failures properly', () => {
    const toastrSpy = jest.spyOn(toastrService, 'warning');
    jest
      .spyOn(eadService, 'getEADFromBlockchain')
      .mockReturnValue(throwError(new HttpResponse({ body: {} as EADDto, status: 404 })));
    component.ngOnInit();
    expect(toastrSpy).toHaveBeenCalled();
  });

  // Testing for Positive behaviour
  it('should handle EAD update properly', async () => {
    const mockResponse = new HttpResponse<EADDto>({ body: mockEAD.ead });
    jest.spyOn(eadService, 'updateExportToken').mockReturnValue(firstValueFrom(of(mockResponse)));
    jest.spyOn(eadService, 'updateExportTokenEventLocal').mockResolvedValue(mockEAD.ead);
    component.handleEADUpdate(mockEAD.ead);
    const toastrSpy = jest.spyOn(toastrService, 'success').mockReturnValue({} as ActiveToast<string>);
    await eadService.updateExportTokenEventLocal({} as EADDto);
    expect(toastrSpy).toHaveBeenCalled();
  });

  // Testing for negative behaviour
  it('should handle EAD update failures properly', () => {
    const falseEAD: EADDto = JSON.parse(JSON.stringify(mockEAD.ead));
    falseEAD.exportId = 'empty';
    component.handleEADUpdate(falseEAD);
  });

  it('should properly confirm the scanning dialogue and set the EAD for pickup', () => {
    const mockResponse = new HttpResponse<EADDto>({ body: mockEAD.ead });
    jest.spyOn(eadService, 'updateExportToken').mockReturnValue(firstValueFrom(of(mockResponse)));
    component.selection = mockEAD.ead;
    component.confirm();
  });

  it('should open confirm dialog', () => {
    component.selection = mockEAD.ead;
    const successSpy = jest.spyOn(dialog, 'open').mockReturnValue({
      afterClosed: () => of(EMPTY),
    } as any);
    const confirmSpy = jest.spyOn(component, 'confirm').mockReturnValue();
    component.openConfirmDialog();
    expect(successSpy).toHaveBeenCalled();
    expect(confirmSpy).toHaveBeenCalled();
  });
});

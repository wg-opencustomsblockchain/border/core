/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { TokenService } from '@base/src/app/shared/services/token.service';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationPopupComponent } from '../../_components/confirmation-popup/confirmation-popup.component';
import { LoadingComponent } from '../../_components/loading/loading.component';
import { EADDto } from '../../_entities';
import { ExportEventTypes } from '../../_enums/event-types.enum';

@Component({
  selector: 'app-check-transport',
  templateUrl: './check-transport.component.html',
  styleUrls: ['./check-transport.component.scss'],
})
export class CheckTransportComponent implements OnInit {
  selection: EADDto;

  constructor(
    private route: ActivatedRoute,
    private readonly tokenService: TokenService,
    private translateService: TranslateService,
    private toastr: ToastrService,
    private dialog: MatDialog,
    private router: Router,
    private location: Location
  ) {}

  ngOnInit(): void {
    const id: string = this.route.snapshot.params['id'];
    if (id) {
      this.tokenService.getEADFromBlockchain(id).subscribe({
        next: (response: EADDto) => {
          // If the response does not have a body, do nothing
          // tslint:disable-next-line: curly
          if (!response) return;
          this.toastr.success(this.translateService.instant('camera.scan.mrn') + id);
          this.selection = { ...response };
        },

        error: (e) => {
          this.toastr.warning(this.translateService.instant('camera.scan.fetch.fail'));
          this.location.back();
        },
      });
    }
  }

  /**
   * Open the confirm dialog
   */
  openConfirmDialog(): void {
    const dialogRef = this.dialog.open(ConfirmationPopupComponent, {
      data: {
        heading: this.translateService.instant('mobile.customs.confirmation.text.heading'),
        text: `<p>${this.translateService.instant('mobile.customs.confirmation.text.scan')}</p><br><p>[${
          this.selection.exportId
        }]</p><br>
        <p>${this.translateService.instant('mobile.customs.confirmation.text.bottom-action')}</p>`,
      },
      width: '80vw',
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.confirm();
      }
    });
  }

  /**
   * If the mrn is confirmed, update the ead on the blockchain and finish the scan process.
   * Notify the user about the finished process.
   * Navigate back to selection page
   */
  confirm(): void {
    this.handleEADUpdate(this.selection);
    this.toastr.success(this.translateService.instant('camera.scan.reason'));
    this.router.navigateByUrl('selection');
  }

  /**
   * Update the given ead and write the changes to the blockchain
   * Display a Loading Dialog until update is finished.
   * @param ead the ead
   */
  handleEADUpdate(ead: EADDto): void {
    // Display Loading Popup
    this.dialog.open(LoadingComponent, {
      data: {
        heading: this.translateService.instant('camera.loading.assignment.heading'),
        text: this.translateService.instant('camera.loading.assignment.text'),
      },
      width: '80vw',
      disableClose: true,
    });
    ead.eventType = ExportEventTypes.EXPORT_PICKUP;
    this.tokenService
      .updateExportTokenEventLocal(ead)
      .then(() => this.toastr.success(this.translateService.instant('toaster.update.success')))
      .catch((result: Error) =>
        this.toastr.error(this.translateService.instant(result.message), '', {
          disableTimeOut: true,
        })
      )
      .finally(() => this.dialog.closeAll());
  }
}

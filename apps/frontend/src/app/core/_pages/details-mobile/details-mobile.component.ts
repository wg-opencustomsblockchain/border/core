/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, HostListener, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationPopupComponent } from '../../_components/confirmation-popup/confirmation-popup.component';
import { LoadingComponent } from '../../_components/loading/loading.component';
import { ExportStatus } from '../../_enums/status.enum';
import { EADDto } from '../../_entities/ead.entity';
import { TranslateService } from '@ngx-translate/core';
import { ExportEventTypes } from '../../_enums/event-types.enum';
import { User } from '@border/api-interfaces';
import { KeycloakService } from 'keycloak-angular';
import { TokenService } from '@base/src/app/shared/services/token.service';
import { WebsocketService } from '../../services/websocket.service';
import { SpinnerComponent } from '../../_components/spinner/spinner.component';

/**
 * The mobile details page component
 */
@Component({
  selector: 'app-details-mobile',
  templateUrl: './details-mobile.component.html',
  styleUrls: ['./details-mobile.component.scss'],
})
export class DetailsMobileComponent implements OnInit {
  /**
   * The ead to display
   */
  ead?: EADDto;
  /**
   * Is a reason set?
   */
  isReason = false;
  /**
   * Which reason was selected
   */
  reason?: number = undefined;
  /**
   * The username of the current user
   */
  username?: string;
  user: User | null;
  /**
   * The wallet address of the current user
   */
  wallet?: string;

  screenHeight: number;
  screenWidth: number;

  /**
   * Creates an instance of the details mobile page component
   * @param dialog inject material dialog service
   * @param toastr inject toastr service
   * @param eadService inject ead service
   * @param authService inject auth service
   * @param router inject router
   * @param translateService inject translate service
   */
  constructor(
    private readonly dialog: MatDialog,
    private toastr: ToastrService,
    private eadService: TokenService,
    private router: Router,
    private translateService: TranslateService,
    private keycloak: KeycloakService,
    private readonly socketService: WebsocketService
  ) {
    this.getScreenSize();
  }

  /**
   * Gets the screen size of the device
   * @param event Screen resize event
   */
  @HostListener('window:resize', ['$event'])
  getScreenSize(event?): void {
    this.screenHeight = window.innerHeight;
    this.screenWidth = window.innerWidth;
  }

  /**
   * Initializes the username and wallet and load the current ead of the user to display
   */
  async ngOnInit(): Promise<void> {
    this.dialog.open(SpinnerComponent, { panelClass: 'loading-container' });

    this.user = await this.keycloak.loadUserProfile();
    this.username = this.user?.username;
    this.wallet = this.user?.attributes.wallet[0];
    if (this.wallet) {
      this.eadService.getEadsOfUser(this.wallet).subscribe((ead) => {
        if (ead.length === 0) {
          /* this.toastr.warning(this.translateService.instant('toaster.active.warning'));
          this.router.navigateByUrl('selection'); */
          this.ead = null;
          return;
        }
        this.ead = ead[0];
        this.dialog.closeAll();
      });
    } else {
      this.toastr.warning(this.translateService.instant('toaster.user.error'));
    }
  }

  /**
   * Close the screen
   */
  closeScreen(): void {
    this.isReason = true;
  }

  /**
   * Set a reason why the barcode was shown
   */
  setReason(id: number): void {
    this.reason = id;
    const content =
      this.reason == 1
        ? this.translateService.instant('mobile.customs.confirmation.text.presentation')
        : this.translateService.instant('mobile.customs.confirmation.text.check');
    const dialogRef = this.dialog.open(ConfirmationPopupComponent, {
      data: {
        heading: this.translateService.instant('mobile.customs.confirmation.text.heading'),
        text: `<p>${this.translateService.instant(
          'mobile.customs.confirmation.text.top'
        )}</p><br><p>[${content}]</p><br>
        <p>${this.translateService.instant('mobile.customs.confirmation.text.bottom')}</p>`,
      },
      width: '80vw',
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.sendReasonToBlockchain();
        this.socketService.closeGate(this.ead.exportId);
      }
    });
  }

  /**
   * Write the selected reason to the blockchain
   */
  sendReasonToBlockchain(): void {
    if (this.reason !== undefined) {
      this.dialog.open(LoadingComponent, {
        data: {
          heading: this.translateService.instant('mobile.customs.send.heading'),
          text: this.translateService.instant('mobile.customs.send.text'),
        },
        width: '80vw',
        disableClose: true,
      });
      if (this.reason === 1 && this.ead) {
        this.ead.eventType = ExportEventTypes.EXPORT_PRESENTED;
        this.eadService
          .updateExportTokenEventLocal(this.ead)
          .then(() => this.toastr.success(this.translateService.instant('toaster.update.success')))
          .catch((result: Error) =>
            this.toastr.error(this.translateService.instant(result.message), '', {
              disableTimeOut: true,
            })
          )
          .finally(() => this.dialog.closeAll());
        this.dialog.closeAll();
      } else if (this.reason === 0 && this.ead) {
        this.ead.eventType = ExportEventTypes.EXPORT_CHECK;
        this.eadService
          .updateExportTokenEventLocal(this.ead)
          .then(() => this.toastr.success(this.translateService.instant('toaster.update.success')))
          .catch((result: Error) =>
            this.toastr.error(this.translateService.instant(result.message), '', {
              disableTimeOut: true,
            })
          )
          .finally(() => this.dialog.closeAll());
      } else {
        this.toastr.error(this.translateService.instant('toaster.assignment.error'));
      }
      this.router.navigateByUrl('selection').catch((e) => {
        throw new Error(e);
      });
    } else {
      this.toastr.error(this.translateService.instant('toaster.choice.error'));
    }
  }
}

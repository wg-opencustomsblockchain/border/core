/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, fakeAsync, flush, TestBed, tick } from '@angular/core/testing';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpLoaderFactory } from '@base/src/app/app.module';
import { mockEAD } from '@base/src/assets/mockups/EADFile.mock';
import { MOCK_USER } from '@base/src/assets/mockups/user.mock';
import { UserRole, User } from '@border/api-interfaces';
import { AuthenticationService } from '@core/services/authentication.service';
import { ExportStatus } from '@core/_enums/status.enum';
import { TranslateLoader, TranslateModule, TranslatePipe } from '@ngx-translate/core';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { EMPTY, of } from 'rxjs';
import { TokenService } from '@shared/services/token.service';

import { BreadcrumbComponent } from '@base/src/app/shared/breadcrumb/breadcrumb.component';
import { KeycloakService } from 'keycloak-angular';
import { SocketIoConfig, SocketIoModule } from 'ngx-socket-io';
import { TranslatePipeMock } from '@shared/_components/details-list/details.component.test';
import { SelectionComponent } from '../selection/selection.component';
import { DetailsMobileComponent } from './details-mobile.component';
import { LoggerTestingModule } from 'ngx-logger/testing';
import { SharedModule } from '@shared/shared.module';

const config: SocketIoConfig = { url: 'http://localhost:3005', options: {} };

describe('DetailsMobileComponent', () => {
  let component: DetailsMobileComponent;
  let fixture: ComponentFixture<DetailsMobileComponent>;
  let http: true;
  let keycloak: KeycloakService;
  let eadService: TokenService;
  let toastrService: ToastrService;
  let router: Router;
  let dialog: MatDialog;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule.withRoutes([{ path: 'selection', component: SelectionComponent }]),
        MatDialogModule,
        SharedModule,
        NoopAnimationsModule,
        ToastrModule.forRoot(),
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [http],
          },
        }),
        SocketIoModule.forRoot(config),
        LoggerTestingModule,
      ],
      providers: [
        { provide: TokenService, useClass: TokenService },
        { provide: http, useValue: true },
        { provide: TranslatePipe, useClass: TranslatePipeMock },
        { provide: AuthenticationService, useClass: AuthenticationService },
        {
          provide: KeycloakService,
          useValue: {
            async loadUserProfile() {
              return Promise.resolve(MOCK_USER[1]);
            },
            getUserRoles() {
              return [UserRole.EXPORTER];
            },
          },
        },
      ],
      declarations: [DetailsMobileComponent, BreadcrumbComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    keycloak = TestBed.inject(KeycloakService);
    eadService = TestBed.inject(TokenService);
    toastrService = TestBed.inject(ToastrService);
    router = TestBed.inject(Router);
    dialog = fixture.debugElement.injector.get(MatDialog);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should close screen', () => {
    component.closeScreen();
    expect(component.isReason).toBe(true);
  });

  const user: User = MOCK_USER[1];

  describe('if onInit is called', () => {
    it('if the username can be fetched, the eads should load properly', fakeAsync(() => {
      jest.spyOn(keycloak, 'loadUserProfile').mockReturnValue(Promise.resolve(user));
      jest.spyOn(eadService, 'getEadsOfUser').mockReturnValue(of([mockEAD.ead]));
      component.ngOnInit();
      tick();
      expect(component.wallet).toEqual(user.attributes.wallet[0]);
      expect(component.ead).toEqual(mockEAD.ead);
      flush();
    }));

    it('if no username can be fetched, the execution should be ignored & an error message should be displayed', fakeAsync(() => {
      const toastrSpy = jest.spyOn(toastrService, 'warning');
      jest.spyOn(keycloak, 'loadUserProfile').mockReturnValue(Promise.resolve(null));
      component.ngOnInit();
      tick();
      expect(component.username).toBeUndefined();
      expect(toastrSpy).toHaveBeenCalled();
      flush();
    }));

    it('if a username can be fetched but the user does not have assigned eads, the execution should be ignored & an error message should be displayed', fakeAsync(() => {
      jest.spyOn(keycloak, 'loadUserProfile').mockReturnValue(Promise.resolve(user));
      jest.spyOn(eadService, 'getEadsOfUser').mockReturnValue(of([]));
      component.ngOnInit();
      tick();
      expect(component.username).toEqual(user.username);
      expect(component.ead).toBeNull();
      flush();
    }));
  });

  describe('sendReasonToBlockchain should', () => {
    it('display info and success if reason is mobile check and ead is present', () => {
      const successSpy = jest.spyOn(dialog, 'open');
      component.reason = 0;
      component.ead = mockEAD.ead;
      component.sendReasonToBlockchain();
      expect(successSpy).toHaveBeenCalled();
    });

    it('display info and success if reason is mobile check and ead is present and call ead update', () => {
      const infoSpy = jest.spyOn(dialog, 'open');

      component.reason = 1;
      component.ead = mockEAD.ead;
      component.sendReasonToBlockchain();
      expect(component.ead.status).toEqual(ExportStatus.RECORDED);
      expect(infoSpy).toHaveBeenCalled();
    });

    it('display error message if no ead is present', () => {
      const errorSpy = jest.spyOn(toastrService, 'error');
      component.reason = 0;
      component.ead = undefined;
      component.sendReasonToBlockchain();
      expect(errorSpy).toHaveBeenCalled();

      component.reason = 1;
      component.sendReasonToBlockchain();
      expect(errorSpy).toHaveBeenCalled();
    });

    it('display error if no reason set but sendReason attempted', () => {
      const errorSpy = jest.spyOn(toastrService, 'error');
      component.reason = undefined;
      component.sendReasonToBlockchain();
      expect(errorSpy).toHaveBeenCalled();
    });
  });

  it('set reason should set the correct reason', () => {
    const openDialogSpy = jest.spyOn(dialog, 'open').mockReturnValue({
      afterClosed: () => EMPTY,
    } as any);
    component.setReason(1);
    expect(component.reason).toEqual(1);
    expect(openDialogSpy).toHaveBeenCalled();
  });
});

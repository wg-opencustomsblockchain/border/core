/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { ZXingScannerModule } from '@zxing/ngx-scanner';
import { QRCodeModule } from 'angularx-qrcode';
import { AvatarModule } from 'ngx-avatars';
import { NgxBarcode6Module } from 'ngx-barcode6';
import { ToastrModule } from 'ngx-toastr';
import { SharedModule } from '@shared/shared.module';
import { AuthenticationService } from './services/authentication.service';
import { ConfirmationPopupComponent } from './_components/confirmation-popup/confirmation-popup.component';
import { DetailComponent } from './_components/detail/detail.component';
import { EditFileComponent } from './_components/edit-import-dialog/edit-file/edit-file.component';
import { EditImportDialogComponent } from './_components/edit-import-dialog/edit-import-dialog.component';
import { EventHistoryComponent } from './_components/event-history/event-history.component';
import { ExportStatusComponent } from './_components/export-status/export-status.component';
import { ImportStatusComponent } from './_components/import-status/import-status.component';
import { LoadingComponent } from './_components/loading/loading.component';
import { LocalizationComponent } from './_components/localization/localization.component';
import { ToolbarComponent } from './_components/toolbar/toolbar.component';
import { BarcodeComponent } from './_pages/barcode/barcode.component';
import { CameraComponent } from './_pages/camera/camera.component';
import { DetailsMobileComponent } from './_pages/details-mobile/details-mobile.component';
import { GenerateBarcodeComponent } from './_pages/generate-barcode/generate-barcode.component';
import { ImprintComponent } from './_pages/imprint/imprint.component';
import { PrivacyPolicyComponent } from './_pages/privacy-policy/privacy-policy.component';
import { DialogConfirmWriteComponent, ReleaseComponent } from './_pages/release/release.component';
import { SelectionComponent } from './_pages/selection/selection.component';
import { HomeComponent } from './_pages/home/home.component';
import { NgxEchartsModule } from 'ngx-echarts';
import { UnauthorizedComponent } from './_pages/error/unauthorized/unauthorized.component';
import { EditExportDialogComponent } from './_components/edit-export-dialog/edit-export-dialog.component';
import { SidebarComponent } from './_components/sidebar/sidebar.component';
import { CustomsOfficeService } from './services/customs-office.service';
import { SpinnerComponent } from './_components/spinner/spinner.component';
import { HistoryService } from '@core/services/history.service';
import { MatInputModule } from '@angular/material/input';
import { CodeInputComponent } from './_pages/code-input/code-input.component';
import { CheckTransportComponent } from './_pages/check-transport/check-transport.component';

@NgModule({
  declarations: [
    ToolbarComponent,
    SelectionComponent,
    ReleaseComponent,
    GenerateBarcodeComponent,
    DialogConfirmWriteComponent,
    BarcodeComponent,
    CameraComponent,
    DetailComponent,
    DetailsMobileComponent,
    ConfirmationPopupComponent,
    LoadingComponent,
    SpinnerComponent,
    LocalizationComponent,
    EditImportDialogComponent,
    EditExportDialogComponent,
    ImprintComponent,
    PrivacyPolicyComponent,
    EditFileComponent,
    EventHistoryComponent,
    ExportStatusComponent,
    ImportStatusComponent,
    HomeComponent,
    UnauthorizedComponent,
    SidebarComponent,
    CodeInputComponent,
    CheckTransportComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    HttpClientModule,
    FormsModule,
    RouterModule,
    QRCodeModule,
    BrowserAnimationsModule,
    ZXingScannerModule,
    ToastrModule,
    NgxBarcode6Module,
    AvatarModule,
    MatInputModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts'),
    }),
  ],
  exports: [ToolbarComponent, SidebarComponent],
  providers: [AuthenticationService, CustomsOfficeService, HistoryService],
})
export class CoreModule {}

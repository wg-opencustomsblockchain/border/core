/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FieldInformationDto } from '@border/api-interfaces/lib/entities/history/history.dto';
import { environment } from '@environments/environment';
import { Injectable } from '@angular/core';
import { BorderApi } from '@border/api-interfaces';

@Injectable({
  providedIn: 'root',
})
export class HistoryService {
  constructor(private readonly httpClient: HttpClient) {}

  public getHistoryOfToken(tokenId: string): Observable<FieldInformationDto[]> {
    return this.httpClient.get<FieldInformationDto[]>(
      `${environment.EAD_API_URL}${environment.EAD_API_ENDPOINT}/${BorderApi.EXPORT_CONTROLLER}/${tokenId}/history`
    );
  }
}

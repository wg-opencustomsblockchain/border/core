/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { LoggerTestingModule } from 'ngx-logger/testing';
import { of } from 'rxjs';

import { CustomsOfficeService } from './customs-office.service';

describe('CustomsOfficeService', () => {
  let service: CustomsOfficeService;
  let httpClient: HttpClient;

  const mockXML =
    '<?xml version="1.0" encoding="UTF-8"?><Codelist Id="C0141" Name="Customs Office" Release="3.0" System="EX" PublicationDate="2021-07-19T09:56:42"><Entry><StartDate>2002-03-22T00:00:00</StartDate><EndDate>2006-05-29T23:59:59</EndDate><Code>DE001006</Code><Description>Hamburg</Description></Entry></Codelist>';

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, LoggerTestingModule],
    });
    service = TestBed.inject(CustomsOfficeService);
    httpClient = TestBed.inject(HttpClient);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('translations', () => {
    it('should correctly deliver a translation', () => {
      service.parseCustomsOffices(mockXML.toString());
      expect(service.getFullName('DE001006')).toEqual('Hamburg');
      expect(service.getFullName('Cats')).toEqual('Cats');
    });
  });

  describe('should load the translations from', () => {
    it('localstorage if they are present', () => {
      window.localStorage.setItem('customsOffices', JSON.stringify(mockXML));
      const httpSpy = jest.spyOn(httpClient, 'get');
      service.initializeCustomsService();
      expect(httpSpy).toHaveBeenCalledTimes(0);
    });

    it('http if they are not present', () => {
      window.localStorage.removeItem('customsOffices');
      const httpSpy = jest.spyOn(httpClient, 'get').mockReturnValue(of(mockXML));
      service.initializeCustomsService();
      expect(httpSpy).toHaveBeenCalledTimes(1);
      expect(JSON.parse(localStorage.getItem('customsOffices'))).toEqual(
        JSON.parse('[{ "code": "DE001006", "description": "Hamburg" }]')
      );
    });
  });
});

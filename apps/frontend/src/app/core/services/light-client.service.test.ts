/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { LoggerTestingModule } from 'ngx-logger/testing';

import { AuthenticationService } from './authentication.service';
import { LightClientService } from './light-client.service';

describe('LightClientService', () => {
  let service: LightClientService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, LoggerTestingModule, RouterTestingModule, TranslateModule.forRoot()],
      providers: [
        {
          provide: AuthenticationService,
          useClass: AuthenticationService,
          translationService: TranslateService,
        },
      ],
    });
    service = TestBed.inject(LightClientService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

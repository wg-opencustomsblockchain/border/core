/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '@border/api-interfaces';
import { environment } from '@environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';

/**
 * A service for handling authentication
 */
@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  /**
   * Creates an instance of authentication service
   * @param http injects http client
   * @param router injects router
   */
  constructor(private readonly http: HttpClient) {}

  /**
   * Looks up a User according to their Cosmos Address.
   * @param address The Cosmos address to look up.
   * @returns The User identified by the cosmos blockchain address.
   */
  public getUserByCosmosAddress(address: string): Observable<User> {
    return this.http.get<User>(`${environment.USER_API_URL}${environment.USER_API_USER_ENDPOINT}${address}`);
  }
}

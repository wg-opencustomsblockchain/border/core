/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as parser from 'fast-xml-parser';
import { CustomsOfficeInformations, CustomsOfficeTranslation } from '@core/_entities/customs-office-translation.entity';
import { NGXLogger } from 'ngx-logger';

/** Service to resolve a customs ID to a human readable city format. */
@Injectable({
  providedIn: 'root',
})
export class CustomsOfficeService {
  private customsOffices: CustomsOfficeTranslation[] = [];
  private customsInformation: CustomsOfficeInformations[] = [];

  /** Grab a customs list via the injected http client. */
  constructor(private http: HttpClient, private logger: NGXLogger) {}

  /**
   * The main function of the pipe
   * converts an input value to the output value
   * In this case: gets a customs code and return the correct full name
   * @param value input value
   * @returns output value
   */
  getFullName(value: string): string {
    // Remove Whitespaces since the datasource encodes them without.
    value = value.replace(' ', '');
    let result = '';
    const office = this.customsOffices.find((element) => element.code === value);
    if (office) {
      result = office.description;
    } else {
      result = value;
    }
    return result;
  }

  /**
   * Attempts to parse element which corresponds to a customs code.
   * @param customCode The code to look up.
   * @returns The info of the desired Customs Office.
   */
  getFullInformation(customCode: string) {
    return (
      this.customsInformation.find((element) => element.referenceNumber === customCode) || {
        name: '',
        city: `No information found for customs code ${customCode}`,
        countryCode: null,
        postalCode: null,
        referenceNumber: customCode,
        streetAndNumber: null,
      }
    );
  }

  /**
   * Parses the customs office array to a useable data format
   * @param xml the customs office xml file
   * @returns an custom Office array
   */
  parseCustomsOffices(xml: string): CustomsOfficeTranslation[] {
    this.logger.log('Parsing new Customs Offices Datasource');

    const customsOffices: CustomsOfficeTranslation[] = [];

    const options = this.setOptions();
    // If Required the parser can Validate the XML Schema.
    if (parser.validate(xml)) {
      this.logger.log('Valid XML Source');
    }

    const tObj = parser.getTraversalObj(xml, options);
    const jsonObj = parser.convertToJson(tObj, options);

    // Iterate over the returned object and remove duplicates.
    // We save ourselves the parsing and extract the values by JSON notation.
    for (const office of jsonObj.Codelist[0].Entry) {
      if (!customsOffices.find((element) => element.code == office.Code)) {
        customsOffices.push(new CustomsOfficeTranslation(office.Code, office.Description));
      }
    }

    // Use Local Storage for now to save the small(ish) result of the initial big XML Parse On Startup.
    // Local Storage supports about 5mb of storage space and the parsed list of **all** international Customs Offices totals at about 0.5MB
    // If required we should think about a different way to Cache this.
    localStorage.setItem('customsOffices', JSON.stringify(customsOffices));
    this.customsOffices = customsOffices;
    return customsOffices;
  }

  /**
   * Options set as per specification:
   * Array Mode was modified to "true" since that was required to parse this particular XML that the Atlas provides.
   * https://www.npmjs.com/package/fast-xml-parser
   * @returns An XML Parser configuration.
   */
  setOptions(): any {
    return {
      attributeNamePrefix: '@_',
      attrNodeName: 'attr', // default is 'false'
      textNodeName: '#text',
      ignoreAttributes: false,
      ignoreNameSpace: false,
      allowBooleanAttributes: false,
      parseNodeValue: true,
      parseAttributeValue: true,
      trimValues: true,
      cdataTagName: '__cdata', // default is 'false'
      cdataPositionChar: '\\c',
      parseTrueNumberOnly: false,
      arrayMode: true, // "strict"
    };
  }

  /**
   * Initializes the Customs Service by doing a query on the browser local storage.
   */
  initializeCustomsService(): void {
    const customOfficeJson = localStorage.getItem('customsOffices');

    if (customOfficeJson) {
      this.customsOffices = JSON.parse(customOfficeJson);
    } else {
      this.loadXML();
    }
  }

  /**
   * Specify the Datasource for this Pipe.
   * In the Future we might want to make a Service out of this.
   * But as it stands its only required exactly once per Program Startup at this one spot in the application.
   */
  loadXML(): void {
    this.http
      .get('/assets/data/customsoffices.xml', {
        headers: new HttpHeaders()
          .set('Content-Type', 'text/xml')
          .append('Access-Control-Allow-Methods', 'GET')
          // .append('Access-Control-Allow-Origin', '*')
          .append(
            'Access-Control-Allow-Headers',
            'Access-Control-Allow-Headers, Access-Control-Allow-Origin, Access-Control-Request-Method'
          ),
        responseType: 'text',
      })
      .subscribe((data) => {
        localStorage.setItem('customsOffices', JSON.stringify(this.parseCustomsOffices(data)));
      });
  }
}

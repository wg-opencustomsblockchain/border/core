/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { mockEAD } from '@base/src/assets/mockups/EADFile.mock';
import { environment } from '@environments/environment';
import { BorderApi } from '@border/api-interfaces';
import { ImportEntity } from '../_entities/import.entity';

import { BorderApiService } from './import.service';

describe('ImportService', () => {
  let service: BorderApiService;
  let httpMock: HttpTestingController;

  const borderPrefix = BorderApi.GLOBAL_PREFIX;
  const apiVersionImport = BorderApi.IMPORT_API_VERSION;
  const endPointImport = BorderApi.IMPORT_CONTROLLER;
  const importApi = `${environment.EAD_API_URL}/${borderPrefix}/${apiVersionImport}/${endPointImport}`;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(BorderApiService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('getActiveImports() should call the correct endpoint & return a empty array', () => {
    service.getActiveImports().subscribe((emp) => {
      expect(emp).toEqual([]);
    });

    const req = httpMock.expectOne(`${importApi}`);
    expect(req.request.method).toEqual('GET');

    req.flush([]);
    httpMock.verify();
  });

  it('acceptImport() should call the correct endpoint & return a empty array', () => {
    service
      .acceptImport(ImportEntity.parseExport(mockEAD.ead))
      .subscribe((emp) => {
        expect(emp).toEqual([]);
      });

    const req = httpMock.expectOne(`${importApi}`);
    expect(req.request.method).toEqual('POST');

    req.flush([]);
    httpMock.verify();
  });
});

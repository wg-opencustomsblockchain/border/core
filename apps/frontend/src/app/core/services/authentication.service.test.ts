/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MOCK_USER } from '@base/src/assets/mockups/user.mock';
import { User } from '@border/api-interfaces';
import { environment } from '@environments/environment';

import { AuthenticationService } from './authentication.service';

export const user: User = MOCK_USER[1];

describe('AuthenticationService', () => {
  let service: AuthenticationService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule],
    });
    httpMock = TestBed.inject(HttpTestingController);
    service = TestBed.inject(AuthenticationService);

    localStorage.setItem('currentUser', JSON.stringify(user));
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('getUserByCosmosAddress() should call http GET method', () => {
    const cosmosAddress = 'cosmos123123123123';
    service.getUserByCosmosAddress(cosmosAddress).subscribe((emp) => {
      expect(emp).toEqual(user);
    });

    const req = httpMock.expectOne(`${environment.USER_API_URL}${environment.USER_API_USER_ENDPOINT}${cosmosAddress}`);

    expect(req.request.method).toEqual('GET');
    req.flush(user);

    httpMock.verify();
  });
});

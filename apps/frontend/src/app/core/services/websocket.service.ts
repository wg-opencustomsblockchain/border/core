/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { Observable } from 'rxjs';

/** Basic websocket service, currently implemented as a simple preview. */
@Injectable({
  providedIn: 'root',
})
export class WebsocketService {
  /** Inject a basic websocket */
  constructor(private socket: Socket) {}

  /** Receives an update from the websocket, providing info on an EAD update from another party. */
  receiveEADUpdate(): Observable<any> {
    return this.socket.fromEvent('ead-update');
  }

  closeGate(mrn: string): void {
    this.socket.emit('gate', mrn);
  }
}

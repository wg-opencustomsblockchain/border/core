/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { Coin } from '@cosmjs/launchpad';
import * as proto from '@cosmjs/proto-signing';
import { DirectSecp256k1HdWallet } from '@cosmjs/proto-signing';
import { EncodeObject } from '@cosmjs/proto-signing/build/registry';
import * as stargate from '@cosmjs/stargate';
import { environment } from '@environments/environment';
import { TranslateService } from '@ngx-translate/core';

import { Client } from '@border/api-interfaces/lib/client';
import { TxBody } from '@border/api-interfaces/lib/client/cosmos.tx.v1beta1';
import { NGXLogger } from 'ngx-logger';
import {
  MsgCreateExportEventResponse,
  MsgFetchExportTokenAllResponse,
  MsgFetchExportTokenResponse,
  MsgFetchImportTokenAllResponse,
  MsgFetchImportTokenResponse,
} from '@border/api-interfaces/lib/client/org.borderblockchain.businesslogic/types/businesslogic/tx';
import {
  MsgCreateExportTokenResponse,
  MsgUpdateExportTokenResponse,
} from '@border/api-interfaces/lib/client/org.borderblockchain.exporttoken';
import {
  MsgCreateImportTokenResponse,
  MsgUpdateImportTokenResponse,
} from '@border/api-interfaces/lib/client/org.borderblockchain.importtoken';

/**
 * Light client implementation for Tendermint/Cosmos chains.
 * Workhorse class for all Chain interactions of this application.
 * Handles both the details of writing and reading to the chain.
 */
@Injectable({
  providedIn: 'root',
})
export class LightClientService {
  constructor(
    private translateService: TranslateService,
    private logger: NGXLogger
  ) {}
  /** A standard coin object - Can be set to 0 as we do not use any monetary token systems.  */
  stdCoins: Coin[] = [{ denom: 'token', amount: '0' }];

  /** How much gas a transaction may use - Set to big enough value to accept all transactions.  */
  stdGas = '18000000000';

  /** Memo that is attached to a transaction  */
  stdMemo: 'Sent From Border Frontend';

  /** Registry to decode Blockchain responses. */
  decodeRegistry: any;

  /** PART OF THE HOTFIX FOR VERSION 0.24 */
  private maxTimeout = 10;
  private timeoutMs = 1000;

  types = [
    // All Messages and Responses related to Export
    [
      `/org.borderblockchain.businesslogic.MsgCreateEvent`,
      MsgCreateExportEventResponse,
    ],
    [
      `/org.borderblockchain.businesslogic.MsgCreateExportEvent`,
      MsgCreateExportEventResponse,
    ],
    [
      `/org.borderblockchain.businesslogic.MsgCreateExportToken`,
      MsgCreateExportTokenResponse,
    ],
    [
      `/org.borderblockchain.businesslogic.MsgUpdateExportToken`,
      MsgUpdateExportTokenResponse,
    ],
    [
      `/org.borderblockchain.businesslogic.MsgCreateImportToken`,
      MsgCreateImportTokenResponse,
    ],
    [
      `/org.borderblockchain.businesslogic.MsgUpdateImportToken`,
      MsgUpdateImportTokenResponse,
    ],

    [
      `/org.borderblockchain.businesslogic.MsgFetchExportTokenAllExporter`,
      MsgFetchExportTokenAllResponse,
    ],
    [
      `/org.borderblockchain.businesslogic.MsgFetchExportTokenAllDriver`,
      MsgFetchExportTokenAllResponse,
    ],
    [
      `/org.borderblockchain.businesslogic.MsgFetchExportTokenAllImporter`,
      MsgFetchExportTokenAllResponse,
    ],
    [
      `/org.borderblockchain.businesslogic.MsgFetchExportTokenExporterByExportId`,
      MsgFetchExportTokenResponse,
    ],
    [
      `/org.borderblockchain.businesslogic.MsgFetchExportTokenDriverByExportId`,
      MsgFetchExportTokenResponse,
    ],
    [
      `/org.borderblockchain.businesslogic.MsgFetchImportTokenAll`,
      MsgFetchImportTokenAllResponse,
    ],
    [
      `/org.borderblockchain.businesslogic.MsgFetchImportTokenByImportId`,
      MsgFetchImportTokenResponse,
    ],

    [
      `/org.borderblockchain.businesslogic.MsgFetchImportToken`,
      MsgFetchImportTokenResponse,
    ],

    [
      `/org.borderblockchain.businesslogic.MsgFetchImportTokenAll`,
      MsgFetchImportTokenAllResponse,
    ],
    [
      `/org.borderblockchain.businesslogic.MsgFetchExportTokenImporterByExportId`,
      MsgFetchExportTokenResponse,
    ],
    [
      `/org.borderblockchain.businesslogic.MsgFetchExportTokenAllExporter`,
      MsgFetchExportTokenAllResponse,
    ],
  ];

  registry: proto.Registry = new proto.Registry(this.types as any);

  /**
   * Prepare an incoming transaction into a Cosmos/Tendermint message format.
   * @param wallet The Wallet address used for signing.
   * @param mnemonic The private Key used to sign with.
   * @param txInput The transaction input to sign.
   * @param type The type of a transaction Msg. For example: "MsgCreateExportToken"
   * @param typeUrl The URL under which the API is served. For example: "/org.borderblockchain.importtoken"
   * @returns
   */
  async buildSignAndBroadcast(
    mnemonic: string,
    txInput: any[],
    type: string
  ): Promise<any> {
    const blockchainRPCEndpoint = `${environment.TENDERMINT_CHAIN_ENDPOINT_RPC}`;
    const blockchainNodeEndpoint = `${environment.TENDERMINT_CHAIN_ENDPOINT_NODE}`;

    let response: stargate.DeliverTxResponse;
    const messages: EncodeObject[] = [];
    const walletAddress = await this.getCosmosAddress(mnemonic);

    const wallet = await DirectSecp256k1HdWallet.fromMnemonic(mnemonic);
    const client = new Client(
      {
        apiURL: blockchainNodeEndpoint,
        rpcURL: blockchainRPCEndpoint,
        prefix: 'cosmos',
      },
      wallet
    );

    for (const tx of txInput) {
      tx.creator = walletAddress;
      const encodeObject: EncodeObject = {
        typeUrl: '/org.borderblockchain.businesslogic.' + type,
        value: tx,
      };
      messages.push(encodeObject);
    }

    // Retry sending the message and handle errors.
    for (let i = 0; i < this.maxTimeout; i++) {
      try {
        response = await client.signAndBroadcast(
          messages,
          { amount: this.stdCoins, gas: this.stdGas },
          this.stdMemo
        );

        if (await this.checkTxResult(response.code)) {
          const value = await client.CosmosTxV1Beta1.query.serviceGetTx(
            response.transactionHash
          );
          const hexString = value.data.tx_response.data;
          const arrayBuffer = Uint8Array.from(
            hexString.match(/.{1,2}/g).map((byte) => parseInt(byte, 16))
          );

          try {
            const decodedResult: TxBody =
              this.decodeRegistry.decodeTxBody(arrayBuffer);
            return decodedResult.messages[0];
          } catch (e) {
            this.logger.info(e);
            this.logger.info('Transaction OK, But nothing to decode.');

            return null;
          }
        }
      } catch (e) {
        this.logger.error(e);
        await this.checkTxResult(e.code);
      }
    }
    throw new Error('Timeout');
  }

  /**
   * Rudimentary Error Handling method based on Chain Responses.
   * Includes a Bandaid fix in case we send two messages from the same wallet at the same time.
   * @param code: The Error code
   * @returns
   */
  public async checkTxResult(code: number): Promise<boolean> {
    switch (code) {
      case 0: {
        return true;
      }
      case 32: {
        this.logger.info(
          'Sequence missmatch: Retry in ' + this.timeoutMs + ' Seconds'
        );
        await this.delay(this.timeoutMs);
        return false;
      }
      case -32603: {
        this.logger.info(
          'Tx already in Cache: Retry in ' + this.timeoutMs + ' Seconds'
        );
        await this.delay(this.timeoutMs);
        return false;
      }
      // Authorization Errors
      case 4: {
        this.logger.error('Authorization Error.');
        throw new Error('Auth Error');
      }
      // Illegal Status Transaction
      case 18: {
        this.logger.error('Attempted Status change outside the chain.');
        throw new Error('Invalid Transaction');
      }
      case 7: {
        this.logger.error('Invalid Address.');
        throw new Error('Invalid Address');
      }
    }

    return true;
  }

  /**
   * Derives the blockchain address from a given mnemonic.
   * @param mnemonic A cosmos mnemonic.
   * @returns The corresponding cosmos address.
   */
  private async getCosmosAddress(mnemonic: string): Promise<string> {
    const [creatorAddress] = await (
      await DirectSecp256k1HdWallet.fromMnemonic(mnemonic, undefined)
    ).getAccounts();

    return creatorAddress.address;
  }

  public decodeResult(result: any) {
    return result;
  }
  /**
   * This is the last step in the writing process onto the tendermint/cosmos instance.
   * It signs a pre-packaged Array of Encode Objects and broadcasts it to chain peers.
   * Use buildSignAndBroadcast if you need to build your Encode objects first.
   * @param wallet The Wallet address used for signing.
   * @param mnemonic The private Key used to sign with.
   * @param messages The array of messages of Encode Objects.
   * @returns
   */

  //** PART OF THE HOTFIX FOR VERSION 0.24 */
  async delay(ms: number) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }
}

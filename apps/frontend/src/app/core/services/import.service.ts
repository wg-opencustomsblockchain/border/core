/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { BorderApi } from '@border/api-interfaces';

import { ImportEntity } from '../_entities';

/**
 * A service for handling non written data
 */
@Injectable({
  providedIn: 'root',
})
export class BorderApiService {
  constructor(private readonly http: HttpClient) {}

  borderPrefix = BorderApi.GLOBAL_PREFIX;
  apiVersionImport = BorderApi.IMPORT_API_VERSION;
  endPointImport = BorderApi.IMPORT_CONTROLLER;
  importApi = `${environment.EAD_API_URL}/${this.borderPrefix}/${this.apiVersionImport}/${this.endPointImport}`;

  /**
   * Queries the microservice to pull import data from the chain.
   * @returns A list of imports.
   */
  getActiveImports(): Observable<Array<ImportEntity>> {
    return this.http.get<Array<ImportEntity>>(`${this.importApi}`);
  }

  /**
   * Utilizes the microservice to accept a new import. Sends a creation request for a new import token.
   * @returns The newly accepted EAD as an import token.
   */

  acceptImport(imp: ImportEntity): Observable<any> {
    return this.http.post(`${this.importApi}`, imp);
  }
}

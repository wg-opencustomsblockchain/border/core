/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Injector } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { ToastrModule } from 'ngx-toastr';

import { AuthenticationService } from '../services/authentication.service';
import { GlobalHttpInterceptor } from './global-http.interceptor';

describe('GlobalHttpInterceptor', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ToastrModule.forRoot(), HttpClientTestingModule, RouterTestingModule],
      providers: [GlobalHttpInterceptor],
    });
  });

  it('should be created', () => {
    const interceptor: GlobalHttpInterceptor = TestBed.inject(GlobalHttpInterceptor);
    expect(interceptor).toBeTruthy();
  });
});

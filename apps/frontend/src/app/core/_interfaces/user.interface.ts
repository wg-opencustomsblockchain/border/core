/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { UserRole } from '../_enums/user-role.enum';
import { Company } from '../_entities/company.entity';
/** Basic interface for user interactions.  */
export interface User {
  /** User ID provided by the user microservice. */
  id: number;
  /** Username provided by the user microservice. */
  username: string;
  /** Role provided by the user microservice. */
  role: UserRole;
  /** Company associated with the user identity. Required for matching imports to users. */
  company: Company;
  /** Blockchain mnemonic provided by the user microservice. Used for signing transactions. */
  mnemonic: string;
  /** Blockchain wallet provided by the user microservice. Used for signing transactions. */
  wallet: string;
  /** Blockchain public Key provided by the user microservice. Used for signing transactions. */
  pubkey: string;
}

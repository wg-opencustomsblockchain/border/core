/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EADDto } from '@base/src/app/core/_entities';
import { ExportService } from '@base/src/app/shared/services/export.service';
import { KeycloakService } from 'keycloak-angular';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
})
export class EDetailsComponent implements OnInit {
  exportToken: EADDto | null = null;
  isExporter: boolean;

  constructor(
    private readonly route: ActivatedRoute,
    private location: Location,
    private readonly exportService: ExportService,
    private authService: KeycloakService
  ) {}

  ngOnInit(): void {
    this.exportService.getEadById(this.route.snapshot.params['id']);
    this.exportService.exportToken.asObservable().subscribe((res) => {
      this.exportToken = res;
    });
    this.isExporter = this.authService.getUserRoles().includes('EXPORTER');
  }

  back(): void {
    this.location.back();
  }
}

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { KeycloakService } from 'keycloak-angular';
import { TranslateModule, TranslateService } from '@ngx-translate/core';

import { EDetailsComponent } from './details.component';
import { RouterTestingModule } from '@angular/router/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { ToastrModule } from 'ngx-toastr';
import { LoggerTestingModule } from 'ngx-logger/testing';
import { SharedModule } from '@shared/shared.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

describe('DetailsComponent', () => {
  let component: EDetailsComponent;
  let fixture: ComponentFixture<EDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        TranslateModule.forRoot(),
        RouterTestingModule,
        MatDialogModule,
        ToastrModule.forRoot(),
        LoggerTestingModule,
        SharedModule,
        NoopAnimationsModule,
      ],
      declarations: [EDetailsComponent],
      providers: [
        {
          provide: KeycloakService,
          useValue: {
            getUserRoles() {
              return ['EXPORTER'];
            },
          },
        },
        {
          provide: TranslateService,
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(EDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

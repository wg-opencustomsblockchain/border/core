/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { KeycloakService } from 'keycloak-angular';

import { DetailsOverviewComponent } from './details-overview.component';

import { mockEAD } from '../../../../../assets/mockups/EADFile.mock';
import { CustomsOfficeService } from '../../../../core/services/customs-office.service';
import { TokenService } from '../../../../shared/services/token.service';
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { LoggerTestingModule } from 'ngx-logger/testing';
import { SharedModule } from '@shared/shared.module';

describe('DetailsOverviewComponent', () => {
  let component: DetailsOverviewComponent;
  let fixture: ComponentFixture<DetailsOverviewComponent>;
  let customsOfficeService: CustomsOfficeService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        TranslateModule.forRoot(),
        MatDialogModule,
        BrowserAnimationsModule,
        ToastrModule.forRoot(),
        LoggerTestingModule,
        SharedModule,
        NoopAnimationsModule,
      ],
      declarations: [DetailsOverviewComponent],
      providers: [
        {
          provide: KeycloakService,
          useService: KeycloakService,
        },
        { provide: TokenService, useService: TokenService },
        { provide: ToastrService, useClass: ToastrService },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(DetailsOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    customsOfficeService = TestBed.inject(CustomsOfficeService);
    component.exportTokens = [mockEAD.ead];
    component.originalTokens = [mockEAD.ead];
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('component methods', () => {
    it('should set customsFullname', () => {
      const token = component.exportTokens[0];
      component.setCustomsFullname(component.exportTokens[0]);
      expect(token.customOfficeOfExit.customsOfficeName).toBe(
        customsOfficeService.getFullName(token.customOfficeOfExit.customsOfficeCode)
      );
    });

    it('should filter tokens after a search term', () => {
      const testString = 'test';
      component.search(testString);
      expect(component.exportTokens).toStrictEqual(
        component.originalTokens.filter((ex) => component.filterPredicate(ex, testString))
      );
    });

    it('should sort by a sort parameter', () => {
      component.sortBy('ID');
      component.sortBy('TIMESTAMP');
      component.sortBy('CONSIGNEE');
      component.sortBy('EXPORT_COUNTRY');
      component.sortBy('default');
    });
  });
});

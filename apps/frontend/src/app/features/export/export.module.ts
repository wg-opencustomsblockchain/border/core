/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/shared.module';
import { DetailsOverviewComponent } from './_pages/details-overview/details-overview.component';
import { ExportRoutingModule } from './export-routing.module';
import { TranslateModule } from '@ngx-translate/core';
import { EDetailsComponent } from './_pages/details/details.component';

@NgModule({
  declarations: [DetailsOverviewComponent, EDetailsComponent],
  imports: [CommonModule, ExportRoutingModule, SharedModule, TranslateModule.forChild({ extend: true })],
  providers: [],
})
export class ExportModule {}

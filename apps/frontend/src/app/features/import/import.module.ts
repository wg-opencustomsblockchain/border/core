/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/shared.module';
import { ImportRoutingModule } from './import-routing.module';
import { TranslateModule } from '@ngx-translate/core';
import { NewImportsComponent } from './new-imports/new-imports.component';
import { ActiveImportsComponent } from './active-imports/active-imports.component';
import { ImportDetailsComponent } from './import-details/import-details.component';
import { ImportService } from './import.service';

@NgModule({
  declarations: [NewImportsComponent, ActiveImportsComponent, ImportDetailsComponent],
  imports: [CommonModule, ImportRoutingModule, SharedModule, TranslateModule.forChild({ extend: true })],
  providers: [ImportService],
})
export class ImportModule {}

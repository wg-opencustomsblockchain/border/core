/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ActiveImportsComponent } from './active-imports/active-imports.component';
import { ImportDetailsComponent } from './import-details/import-details.component';
import { NewImportsComponent } from './new-imports/new-imports.component';

const routes: Routes = [
  { path: '', redirectTo: 'new', pathMatch: 'full' },
  { path: 'new', component: NewImportsComponent },
  { path: 'active', component: ActiveImportsComponent },
  { path: ':id', component: ImportDetailsComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ImportRoutingModule {}

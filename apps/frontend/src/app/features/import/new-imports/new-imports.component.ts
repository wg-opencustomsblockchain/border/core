/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AuthenticationService } from '@base/src/app/core/services/authentication.service';
import { BorderApiService } from '@base/src/app/core/services/import.service';
import { User } from '@border/api-interfaces';
import { ImportToken } from '@border/api-interfaces/lib/client/org.borderblockchain.importtoken';
import { CustomsOfficeService } from '@core/services/customs-office.service';
import { EADDto } from '@core/_entities';
import { TokenService } from '@shared/services/token.service';
import { KeycloakService } from 'keycloak-angular';

@Component({
  selector: 'app-new-imports',
  templateUrl: './new-imports.component.html',
  styleUrls: ['./new-imports.component.scss'],
})
export class NewImportsComponent implements OnInit {
  exportTokens: EADDto[] | null;
  originalTokens: EADDto[];
  listViewSelected = false;
  currentUser: User | null;
  isExporter: boolean;

  // attribute for determining which view (grid or list) to use
  toggledView = 'grid';
  disabled = true;

  constructor(
    private tokenService: TokenService,
    private customsOfficeService: CustomsOfficeService,
    private authService: KeycloakService,
    private dialog: MatDialog
  ) {}

  async ngOnInit(): Promise<void> {
    /* this.tokenService.getWrittenEAD().subscribe((res: EADDto[]) => {
      this.exportTokens = res;
      this.exportTokens.filter((ex) => +ex.status === 0);
      this.exportTokens.map((et) => {
        et.customOfficeOfExit.customsOfficeName = this.customsOfficeService.getFullName(
          et.customOfficeOfExit.customsOfficeCode
        );
      });
      this.originalTokens = [...this.exportTokens];
      this.disabled = false;
    }); */
    this.currentUser = await this.authService.loadUserProfile();
    this.isExporter = this.authService.getUserRoles().includes('EXPORTER');
    this.loadExportToken();

    
  }

  /**
   * Loads export tokens
   */
  async loadExportToken() {
    this.exportTokens = null;
    this.tokenService.getEadsReadyForImport(this.currentUser.username).subscribe((res: EADDto[]) => {
      this.exportTokens = res;
    });
  }

  /**
   * Loads the full name of the customs office
   * @param object export or import token
   */
  setCustomsFullname(object: EADDto): void {
    object.customOfficeOfExit.customsOfficeName = this.customsOfficeService.getFullName(
      object.customOfficeOfExit.customsOfficeCode
    );
  }

  /**
   * Search through all events of the original token list and find related events.
   * @param event the event to filter for
   */
  search(event: string) {
    this.exportTokens = this.originalTokens.filter((ex) => this.filterPredicate(ex, event));
  }

  filterPredicate(ead: EADDto, filter: string): boolean {
    const transformedFilter = filter.trim().toLowerCase();

    const listAsFlatString = (obj: unknown): string => {
      let returnVal = '';
      Object.values(obj).forEach((val) => {
        if (typeof val !== 'object') {
          returnVal = returnVal + ' ' + val;
        } else if (val !== null) {
          returnVal = returnVal + ' ' + listAsFlatString(val);
        }
      });
      return returnVal.trim().toLowerCase();
    };

    return listAsFlatString(ead).includes(transformedFilter);
  }

  /**
   * Sets the parameter, to which the tokens are sorted
   * @param sortParam 
   */
  sortBy(sortParam: string) {
    switch (sortParam) {
      case 'ID':
        this.exportTokens.sort((a, b) => (a.exportId > b.exportId ? 1 : -1));
        break;
      case 'TIMESTAMP':
        this.exportTokens.sort((a, b) => (a.timestamp < b.timestamp ? 1 : -1));
        break;
      case 'CONSIGNEE':
        this.exportTokens.sort((a, b) => (a.consignee.name > b.consignee.name ? 1 : -1));
        break;
      case 'EXPORT_COUNTRY':
        this.exportTokens.sort((a, b) => (a.consignee.address.countryCode > b.consignee.address.countryCode ? 1 : -1));
        break;
      default:
        this.exportTokens = [...this.originalTokens];
    }
  }

  /**
   * Toggles if the grid or list view should be shown
   * @param event 
   */
  toggleGridListView(event: string) {
    this.toggledView = event;
  }
}

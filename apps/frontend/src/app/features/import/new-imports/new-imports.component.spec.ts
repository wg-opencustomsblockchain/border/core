/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { KeycloakService } from 'keycloak-angular';

import { NewImportsComponent } from './new-imports.component';

import { MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { TokenService } from '@base/src/app/shared/services/token.service';
import { CustomsOfficeService } from '@base/src/app/core/services/customs-office.service';
import { mockEAD } from '@base/src/assets/mockups/EADFile.mock';
import { LoggerTestingModule } from 'ngx-logger/testing';
import { SharedModule } from '@shared/shared.module';

describe('DetailsOverviewComponent', () => {
  let component: NewImportsComponent;
  let fixture: ComponentFixture<NewImportsComponent>;
  let customsOfficeService: CustomsOfficeService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        TranslateModule.forRoot(),
        MatDialogModule,
        BrowserAnimationsModule,
        LoggerTestingModule,
        SharedModule,
        NoopAnimationsModule,
      ],
      declarations: [NewImportsComponent],
      providers: [
        {
          provide: KeycloakService,
          useService: KeycloakService,
        },
        { provide: TokenService, useService: TokenService },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(NewImportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    customsOfficeService = TestBed.inject(CustomsOfficeService);
    component.exportTokens = [mockEAD.ead];
    component.originalTokens = [mockEAD.ead];
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('component methods', () => {
    it('should set customsFullname', () => {
      const token = component.exportTokens[0];
      component.setCustomsFullname(component.exportTokens[0]);
      expect(token.customOfficeOfExit.customsOfficeName).toBe(
        customsOfficeService.getFullName(token.customOfficeOfExit.customsOfficeCode)
      );
    });

    it('should filter tokens after a search term', () => {
      const testString = 'test';
      component.search(testString);
      expect(component.exportTokens).toStrictEqual(
        component.originalTokens.filter((ex) => component.filterPredicate(ex, testString))
      );
    });

    it('should sort by a sort parameter', () => {
      component.sortBy('ID');
      component.sortBy('TIMESTAMP');
      component.sortBy('CONSIGNEE');
      component.sortBy('EXPORT_COUNTRY');
      component.sortBy('default');
    });
  });
});

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, OnInit } from '@angular/core';
import { User } from '@border/api-interfaces';
import { ImportEntity } from '@core/_entities';
import { CustomsOfficeService } from '@core/services/customs-office.service';
import { KeycloakService } from 'keycloak-angular';

import { ImportService } from '../import.service';

@Component({
  selector: 'app-active-imports',
  templateUrl: './active-imports.component.html',
  styleUrls: ['./active-imports.component.scss'],
})
export class ActiveImportsComponent implements OnInit {
  importTokens: ImportEntity[] | null;
  originalTokens: ImportEntity[];
  listViewSelected = false;
  currentUser: User | null;
  isExporter: boolean;

  // attribute for determining which view (grid or list) to use
  toggledView = 'grid';
  disabled = true;

  constructor(
    private customsOfficeService: CustomsOfficeService,
    private authService: KeycloakService,
    private importService: ImportService
  ) {}

  async ngOnInit(): Promise<void> {
    this.currentUser = await this.authService.loadUserProfile();
    this.isExporter = this.authService.getUserRoles().includes('EXPORTER');
    this.loadImportToken();
  }

  /**
   * Loads import tokens
   */
  async loadImportToken() {
    this.importTokens = null;
    this.importService.getTokenSubject().subscribe((res) => {
      if (res) {
        this.originalTokens = [...res];
        this.importTokens = res;
      }
    });
    this.importService.fetchImports();
  }

  /**
   * Loads the full name of the customs office
   * @param object export or import token
   */
  setCustomsFullname(object: ImportEntity): void {
    object.customOfficeOfExit.customsOfficeName =
      this.customsOfficeService.getFullName(
        object.customOfficeOfExit.customsOfficeCode
      );
  }

  /**
   * Search through all events of the original token list and find related events.
   * @param event the event to filter for
   */
  search(event: string) {
    this.importTokens = this.originalTokens.filter((ex) =>
      this.filterPredicate(ex, event)
    );
  }

  filterPredicate(ead: ImportEntity, filter: string): boolean {
    const transformedFilter = filter.trim().toLowerCase();

    const listAsFlatString = (obj: unknown): string => {
      let returnVal = '';
      Object.values(obj).forEach((val) => {
        if (typeof val !== 'object') {
          returnVal = returnVal + ' ' + val;
        } else if (val !== null) {
          returnVal = returnVal + ' ' + listAsFlatString(val);
        }
      });
      return returnVal.trim().toLowerCase();
    };

    return listAsFlatString(ead).includes(transformedFilter);
  }

  /**
   * Sets the parameter, to which the tokens are sorted
   * @param sortParam 
   */
  sortBy(sortParam: string) {
    switch (sortParam) {
      case 'ID':
        this.importTokens.sort((a, b) => (a.importId > b.importId ? 1 : -1));
        break;
      case 'TIMESTAMP':
        this.importTokens.sort((a, b) => (a.timestamp < b.timestamp ? 1 : -1));
        break;
      case 'CONSIGNEE':
        this.importTokens.sort((a, b) =>
          a.consignee.name > b.consignee.name ? 1 : -1
        );
        break;
      case 'EXPORT_COUNTRY':
        this.importTokens.sort((a, b) =>
          a.consignee.address.countryCode > b.consignee.address.countryCode
            ? 1
            : -1
        );
        break;
      default:
        this.importTokens = [...this.originalTokens];
    }
  }

  /**
   * Toggles if the grid or list view should be shown
   * @param event 
   */
  toggleGridListView(event: string) {
    this.toggledView = event;
  }
}

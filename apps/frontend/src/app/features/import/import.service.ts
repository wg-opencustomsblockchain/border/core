/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

import { Router } from '@angular/router';
import { LoadingComponent } from '@core/_components/loading/loading.component';
import { ImportEntity } from '@core/_entities';
import { ImportEventTypes } from '@core/_enums/event-types.enum';
import { DialogConfirmWriteComponent } from '@core/_pages/release/release.component';
import { TranslateService } from '@ngx-translate/core';
import { TokenService } from '@shared/services/token.service';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject, map, Observable, tap } from 'rxjs';
import { CustomsOfficeService } from '../../core/services/customs-office.service';
import { BorderApiService } from '../../core/services/import.service';
import { EditImportDialogComponent } from '../../core/_components/edit-import-dialog/edit-import-dialog.component';

@Injectable({
  providedIn: 'root',
})
export class ImportService {
  dialogRef: MatDialogRef<DialogConfirmWriteComponent> | null = null;
  editImportDialog: MatDialogRef<EditImportDialogComponent> | null = null;
  private importTokens: BehaviorSubject<ImportEntity[] | null> =
    new BehaviorSubject(null);

  constructor(
    private readonly borderApiService: BorderApiService,
    private readonly translateService: TranslateService,
    private readonly dialog: MatDialog,
    private readonly toastr: ToastrService,
    private readonly tokenService: TokenService
  ) {}

  /**
   * set importTokens to null to enable shimmer loading, then fetch all import tokens
   */
  fetchImports(): void {
    this.importTokens.next(null);
    this.borderApiService
      .getActiveImports()
      .pipe(tap((res) => this.importTokens.next(res)))
      .subscribe();
  }

  /**
   * display editing dialog and update token after confirmation
   * @param importToken import token
   */
  editImport(importToken: ImportEntity): void {
    this.openEditImportDialog(importToken);
    this.editImportDialog.afterClosed().subscribe((closeData: ImportEntity) => {
      if (!closeData) return;
      this.openLoadingDialog();
      this.tokenService
        .updateImportToken([closeData])
        .then(() => {
          this.toastr.success(
            this.translateService.instant('toaster.update.success')
          );
          this.dialog.closeAll();
        })
        .catch(() => {
          this.toastr.error(
            this.translateService.instant('toaster.update.failure')
          );
        });
    });
  }

  /**
   * change status of import Token and update token
   * @param event containing import token and new status
   * @returns true if user confirmed to change status, else returns false
   */
  setStatus(event: {
    importToken: ImportEntity;
    status: ImportEventTypes;
  }): Observable<boolean> {
    this.openConfirmDialog(
      this.translateService.instant('dialog.confirm.import.status'),
      [event.importToken]
    );
    event.importToken.eventType = event.status;
    return this.dialogRef.afterClosed().pipe(
      map((result: boolean) => {
        if (result) {
          this.openLoadingDialog();
          this.handleImportTokenUpdate(event.importToken);
          return true;
        }
        return false;
      })
    );
  }

  /**
   * updates status  of given import token
   * @param importToken token to update
   */
  async handleImportTokenUpdate(importToken: ImportEntity): Promise<void> {
    this.tokenService
      .updateImportTokenEventLocal(importToken)
      .then(() => {
        this.toastr.success(
          this.translateService.instant('toaster.update.success')
        );
        this.fetchImports();
      })
      .catch((result: Error) => {
        this.toastr.error(this.translateService.instant(result.message), '', {
          disableTimeOut: true,
        });
      })
      .finally(() => {
        this.dialog.closeAll();
      });
  }

  getTokenSubject(): BehaviorSubject<ImportEntity[]> {
    return this.importTokens;
  }

  /**
   * Shows the loading popup
   */
  private openLoadingDialog(): void {
    this.dialog.open(LoadingComponent, {
      data: {
        heading: this.translateService.instant('loading.component.heading'),
        text: this.translateService.instant('loading.component.heading'),
      },
      width: '80vw',
      disableClose: true,
    });
  }

  /**
   * Creates and opens the confirm dialog with the given values
   * @param text The text which should be displayed
   * @param importToken the import Token that should be displayed
   */
  private openConfirmDialog(text: string, importToken: ImportEntity[]): void {
    this.dialogRef = this.dialog.open(DialogConfirmWriteComponent, {
      data: {
        importToken,
        text,
      },
      width: '80vw',
    });
  }

  /**
   * Creates and opens dialog to edit/display the given import token
   * @param importToken token which should be displayed and edited
   */
  private openEditImportDialog(importToken: ImportEntity): void {
    this.editImportDialog = this.dialog.open(EditImportDialogComponent, {
      data: {
        import: importToken,
      },
      width: '85vw',
      height: '85vh',
    });
  }
}

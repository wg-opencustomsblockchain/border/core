/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Location } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CustomsOfficeService } from '@base/src/app/core/services/customs-office.service';
import { ImportEntity } from '@base/src/app/core/_entities';
import { TokenService } from '@base/src/app/shared/services/token.service';
import { Subscription } from 'rxjs';
import { ImportService } from '../import.service';

@Component({
  selector: 'app-import-details',
  templateUrl: './import-details.component.html',
  styleUrls: ['./import-details.component.scss'],
})
export class ImportDetailsComponent implements OnInit, OnDestroy {
  importToken: ImportEntity | null = null;
  subscription: Subscription | null = null;

  constructor(
    private location: Location,
    private importService: ImportService,
    private route: ActivatedRoute,
    private officeService: CustomsOfficeService,
    private tokenService: TokenService
  ) {}

  ngOnInit(): void {
    const id = this.route.snapshot.params['id'];
    this.subscription = this.importService
      .getTokenSubject()
      .subscribe((res) => {
        if (!res) {
          this.importToken = null;
          return;
        }
        const token: ImportEntity = res.find((token) => token.id === id);

        const { name, city, countryCode, postalCode, streetAndNumber } =
          this.officeService.getFullInformation(
            token.customOfficeOfExit.customsOfficeCode
          );

        token.customOfficeOfExit.address = {
          streetAndNumber,
          city,
          countryCode,
          postcode: postalCode,
        };
        token.customOfficeOfExit.customsOfficeName = name;
        this.tokenService
          .getEADFromBlockchain(token.relatedExportToken)
          .subscribe((res) => {
            token.exportStatus = +res.status;
            token.events = [...token.events, ...res.events];
            token.events.sort(
              (a, b) =>
                new Date(a.timestamp).getTime() -
                new Date(b.timestamp).getTime()
            );
            this.importToken = token;
          });
      });
    this.importService.fetchImports();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  back(): void {
    this.location.back();
  }
}

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { KeycloakService } from 'keycloak-angular';
import { TranslateModule, TranslateService } from '@ngx-translate/core';

import { ImportDetailsComponent } from './import-details.component';
import { RouterTestingModule } from '@angular/router/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { ToastrModule } from 'ngx-toastr';
import { LoggerTestingModule } from 'ngx-logger/testing';
import { SharedModule } from '@shared/shared.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ImportService } from '../import.service';
import { BehaviorSubject } from 'rxjs';
import { mockImport } from '@base/src/assets/mockups/Import.mock';

describe('DetailsComponent', () => {
  let component: ImportDetailsComponent;
  let fixture: ComponentFixture<ImportDetailsComponent>;
  let service: ImportService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        TranslateModule.forRoot(),
        RouterTestingModule,
        MatDialogModule,
        ToastrModule.forRoot(),
        LoggerTestingModule,
        SharedModule,
        NoopAnimationsModule,
      ],
      declarations: [ImportDetailsComponent],
      providers: [
        {
          provide: KeycloakService,
          useService: KeycloakService,
        },
        {
          provide: TranslateService,
        },
        { provide: ImportService },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(ImportDetailsComponent);
    component = fixture.componentInstance;
    service = TestBed.inject(ImportService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set Token corectly', () => {
    component.importToken = mockImport;
    const spy = jest
      .spyOn(service, 'getTokenSubject')
      .mockReturnValue(new BehaviorSubject(null));
    component.ngOnInit();
    expect(spy).toHaveBeenCalled();
    expect(component.importToken).toBe(null);
  });
});

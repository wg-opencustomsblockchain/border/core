/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { RouterTestingModule } from '@angular/router/testing';
import { mockImport } from '@base/src/assets/mockups/Import.mock';
import { MOCK_USER } from '@base/src/assets/mockups/user.mock';
import { LightClientService } from '@core/services/light-client.service';
import { WebsocketService } from '@core/services/websocket.service';
import {
  TranslateModule,
  TranslatePipe,
  TranslateService,
} from '@ngx-translate/core';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import { LoggerTestingModule } from 'ngx-logger/testing';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { EMPTY, of } from 'rxjs';
import { ImportEntity } from '../../core/_entities';
import { BorderApiService } from '../../core/services/import.service';
import { ImportEventTypes } from '../../core/_enums/event-types.enum';
import { TokenService } from '../../shared/services/token.service';
import { ImportService } from './import.service';

describe('ImportService', () => {
  let service: ImportService;
  let tokenService: TokenService;
  let dialog: MatDialog;
  let toastr: ToastrService;
  let translateService: TranslateService;
  let apiService: BorderApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        KeycloakAngularModule,
        TranslateModule.forRoot(),
        MatDialogModule,
        ToastrModule.forRoot(),
        LoggerTestingModule,
      ],
      providers: [
        {
          translationService: TranslateService,
          translate: TranslatePipe,
          provide: KeycloakService,
          useValue: {
            async loadUserProfile() {
              return Promise.resolve(MOCK_USER[1]);
            },
          },
        },
        TokenService,
        ImportService,
        { provide: ToastrService, useClass: ToastrService },
        { provide: WebsocketService, useClass: WebsocketService },
        {
          provide: LightClientService,
          useClass: LightClientService,
        },
      ],
    });
    service = TestBed.inject(ImportService);
    tokenService = TestBed.inject(TokenService);
    dialog = TestBed.inject(MatDialog);
    toastr = TestBed.inject(ToastrService);
    translateService = TestBed.inject(TranslateService);
    apiService = TestBed.inject(BorderApiService);
  });

  describe('methods', () => {
    it('should be created', () => {
      expect(service).toBeTruthy();
    });

    it('should fetch imports', () => {
      const apiServiceSpy = jest
        .spyOn(apiService, 'getActiveImports')
        .mockReturnValue(of([{} as ImportEntity]));
      apiService.getActiveImports().subscribe((res) => {
        expect(service.getTokenSubject[0].getValue()).toBe(res);
      });
    });

    it('setStatus should call the correct functions with the correct import Token', async () => {
      const openDialogSpy = jest.spyOn(dialog, 'open').mockReturnValue({
        afterClosed: () => EMPTY,
      } as any);
      jest.spyOn(service, 'handleImportTokenUpdate');
      service.setStatus({
        importToken: mockImport,
        status: ImportEventTypes.IMPORT_SUBMITTED,
      });
      expect(openDialogSpy).toHaveBeenCalled();
    });

    it('setStatus should return true', async () => {
      jest.spyOn(dialog, 'open').mockReturnValue({
        afterClosed: () => {
          return of(true);
        },
      } as any);
      service
        .setStatus({
          importToken: mockImport,
          status: ImportEventTypes.IMPORT_SUBMITTED,
        })
        .subscribe((result) => {
          expect(result).toBe(true);
        });
    });

    it('setStatus should return false', async () => {
      jest.spyOn(dialog, 'open').mockReturnValue({
        afterClosed: () => {
          return of(false);
        },
      } as any);
      service
        .setStatus({
          importToken: mockImport,
          status: ImportEventTypes.IMPORT_SUBMITTED,
        })
        .subscribe((result) => {
          expect(result).toBe(false);
        });
    });

    it('editIMport shouls open dialog', () => {
      const dialodSpy = jest
        .spyOn(dialog, 'open')
        .mockReturnValue({ afterClosed: () => of(true) } as any);
      const tokenServiceSpy = jest.spyOn(tokenService, 'updateImportToken');
      service.editImport(mockImport);
      expect(dialodSpy).toHaveBeenCalled();
      expect(tokenServiceSpy).toHaveBeenCalled();
    });

    it('handle ImportTokenUpdate should call correct functions and toastr success', async () => {
      const tokenServiceSpy = jest
        .spyOn(tokenService, 'updateImportTokenEventLocal')
        .mockReturnValue(Promise.resolve(mockImport));
      const toastrSpySuccess = jest.spyOn(toastr, 'success');
      await service.handleImportTokenUpdate(mockImport).then();

      expect(toastrSpySuccess).toHaveBeenCalled();
      expect(tokenServiceSpy).toHaveBeenCalledWith(mockImport);
    });

    it('handle ImportTokenUpdate should call correct functions and toastr error', async () => {
      const tokenServiceSpy = jest
        .spyOn(tokenService, 'updateImportTokenEventLocal')
        .mockReturnValue(Promise.reject(new Error('error')));
      const toastrSpyError = jest.spyOn(toastr, 'error');
      service.handleImportTokenUpdate(mockImport);

      expect(tokenServiceSpy).rejects.toThrowError();
      expect(tokenServiceSpy).toHaveBeenCalledWith(mockImport);
    });
  });
});

/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { inject, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';

import { AppRoutingModule } from './app-routing.module';
import { HttpLoaderFactory } from './app.module';

describe('AppRoutingModule', () => {
  let http: true;
  class DummyComponent {}

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{ provide: http, useValue: true }],
      imports: [
        RouterTestingModule.withRoutes([{ path: 'home', component: DummyComponent }]),
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [http],
          },
        }),
      ],
    }).compileComponents();
  });

  describe('When the program is initialized', () => {
    it('should initialize a Router with its respective settings.', inject(
      [TranslateService, Router],
      (service: TranslateService, router: Router) => {
        const appRouter: AppRoutingModule = new AppRoutingModule(service, router);
        expect(appRouter).toBeTruthy();
      }
    ));
  });
});

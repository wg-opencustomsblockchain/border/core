/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { APP_INITIALIZER, LOCALE_ID, NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ZXingScannerModule } from '@zxing/ngx-scanner';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import { MzdTimelineModule } from 'ngx-mzd-timeline';
import { SocketIoConfig, SocketIoModule } from 'ngx-socket-io';
import { ToastrModule } from 'ngx-toastr';

import { environment } from '@environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from '@core/core.module';
import { CustomsOfficeService } from '@core/services/customs-office.service';
import { GlobalHttpInterceptor } from '@core/_interceptors/global-http.interceptor';
import { ExportModule } from './features/export/export.module';
import { initializeKeycloak } from './init/keycloak-init.factory';

import { LoggerModule, NgxLoggerLevel } from 'ngx-logger';

/** IO Configuration for Websocket implementation. */
const config: SocketIoConfig = {
  url: environment.EAD_API_URL,
};

/**
 * AoT requires an exported function for factories.
 * @param http
 * @returns the current translation table.
 */
export function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
    }),
    CoreModule,
    ExportModule,
    ZXingScannerModule,
    ToastrModule.forRoot(), // ToastrModule added
    SocketIoModule.forRoot(config),

    TranslateModule.forRoot({
      defaultLanguage: 'de-DE',
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    MzdTimelineModule,
    MatIconModule,
    KeycloakAngularModule,
    LoggerModule.forRoot({
      serverLoggingUrl: '/api/logs',
      level: `${environment.LOG_LEVEL}` === 'dev' ? NgxLoggerLevel.DEBUG : NgxLoggerLevel.ERROR,

      serverLogLevel: NgxLoggerLevel.ERROR,
    }),
  ],
  providers: [
    CustomsOfficeService,
    { provide: LOCALE_ID, useValue: 'de-DE' },
    { provide: LOCALE_ID, useValue: 'en-GB' },
    { provide: APP_INITIALIZER, useFactory: initializeKeycloak, multi: true, deps: [KeycloakService] },
    {
      provide: APP_INITIALIZER,
      useFactory: (ds: CustomsOfficeService) => () => ds.initializeCustomsService(),
      deps: [CustomsOfficeService],
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: GlobalHttpInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})

/** Basic Angular AppModule class. Defines all modules used in the application. */
export class AppModule {}

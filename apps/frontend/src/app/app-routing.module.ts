/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { Router, RouterModule, Routes } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ImprintComponent } from './core/_pages/imprint/imprint.component';
import { BarcodeComponent } from './core/_pages/barcode/barcode.component';
import { CameraComponent } from './core/_pages/camera/camera.component';
import { DetailsMobileComponent } from './core/_pages/details-mobile/details-mobile.component';
import { ArchiveComponent } from './shared/_components/details-list/archive/archive.component';
import { DetailsComponent } from './shared/_components/details-list/details.component';
import { GenerateBarcodeComponent } from './core/_pages/generate-barcode/generate-barcode.component';
import { ReleaseComponent } from './core/_pages/release/release.component';
import { SelectionComponent } from './core/_pages/selection/selection.component';
import { PrivacyPolicyComponent } from './core/_pages/privacy-policy/privacy-policy.component';
import { AuthGuard } from './core/_guards/auth.guard';
import { HomeComponent } from './core/_pages/home/home.component';
import { UnauthorizedComponent } from './core/_pages/error/unauthorized/unauthorized.component';
import { CodeInputComponent } from './core/_pages/code-input/code-input.component';
import { CheckTransportComponent } from './core/_pages/check-transport/check-transport.component';

export const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'selection',
    component: SelectionComponent,
    data: { breadcrumb: 'breadcrumb.functionalities' },
    canActivate: [AuthGuard],
  },
  {
    path: 'release',
    component: ReleaseComponent,
    data: { breadcrumb: 'breadcrumb.release' },
    canActivate: [AuthGuard],
  },
  {
    path: 'details',
    component: DetailsComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'barcode/generate',
    component: GenerateBarcodeComponent,
    data: { breadcrumb: 'breadcrumb.barcode' },
    canActivate: [AuthGuard],
  },
  {
    path: 'barcode',
    component: BarcodeComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'camera',
    component: CameraComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'check-transport/:id',
    component: CheckTransportComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'code-input',
    component: CodeInputComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'details-mobile',
    component: DetailsMobileComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'import',
    component: DetailsComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'archive',
    component: ArchiveComponent,
    data: { breadcrumb: 'breadcrumb.archive' },
    canActivate: [AuthGuard],
  },
  {
    path: 'imprint',
    component: ImprintComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'privacy-policy',
    component: PrivacyPolicyComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'error/unauthorized',
    component: UnauthorizedComponent,
  },
  {
    path: 'exports',
    loadChildren: () => import('./features/export/export.module').then((m) => m.ExportModule),
  },
  {
    path: 'imports',
    loadChildren: () => import('./features/import/import.module').then((m) => m.ImportModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {})],
  exports: [RouterModule],
})
export class AppRoutingModule {
  constructor(translateService: TranslateService, private router: Router) {
    // Configure Router to reload the same location to refresh transactions.
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';

    // Translations for Breadcrumbs for translation parsing
    translateService.instant('breadcrumb.functionalities');
    translateService.instant('breadcrumb.release');
    translateService.instant('breadcrumb.barcode');

    translateService.instant('breadcrumb.archive');
    translateService.instant('breadcrumb.details');

    translateService.instant('breadcrumb.import.active');
    translateService.instant('breadcrumb.import.ready');
  }
}

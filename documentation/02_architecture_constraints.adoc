== Chapter 2: Architecture Constraints

=== Introduction
The following technical constraints apply:

[options="header"]
[cols="~h,~,~"]
|=======================
|Id|Constraint|Value/Description
|BAP1|Basic Architecture Patterns|Microservices
|BAP2|Basic Architecture Patterns|Every Microservice is executed in an application container
|BAP3|Basic Architecture Patterns|Loose coupling
|BAP4|Basic Architecture Patterns|Event-based communication (when appropriate)
|TE1 |Target environment         |OKD/Kubernetes (basic stack of a Silicon Economy Plattform)
|BSD1|Basic Stack for Development: Application Container|Docker
|BSD2|Basic Stack for Development: Interfaces/Communication|HTTP/REST, AMQP, WebSockets
|BSD3|Basic Stack for Development: programming langues|Programming Languages: Typescript
|BSD4|Basic Stack for Development: frontend	|	Frontend Development: Typescript (Angular)
|BSD5|Basic Stack for Development: blockchain|Blockchain Development: Golang
|BSD6|Basic Stack for Development: other|All other technologies must be compatible with OKD/Kubernetes
|BC1|Continuous Integration Pipeline constraints |Integrated with the Gitlab Actions
|BC2|Project License|Developed under the Open Logistics License 1.0
|BC3|Requirements at libraries |Only libraries under a Copy-Left free Open Source License can be used. Accepted open source licenses are, among others, Apache License 2.0, MIT License, BSD License
|BC4|Libraries constraints|Frameworks & libraries under an open-source license (such as GPL, LGPL) are restricted by the use of the Open Logistics License 1.0
|=======================

=== Organizational Constraints
The following organizational constraints apply:

[options="header"]
[cols="~h,~h,~"]
|=======================
|Id|Constraint|Background and / or Motivation
|O1|Schedule|
Milestones:

04/2021 start of development

08/2021 completion of version 0.1

08/2021 start of development for version 0.2

12/2021 start of preperations for open source release

01/2022 first preview version for 0.2

02/2022 ongoing open source preparations

03/2022 improvements for cooperation with external developers 

04/2022 integration of idoc documentation format

06/2022 finalize BORDER 0.2 with full export functionalities 

06/2022 start of development for version 0.3

07/2022 process transparency upgrades via history and company components

07/2022 organization layer finalization / patent check completion

10/2022 finalize physical BORDER demonstrator 

12/2022 full open source release v1.0.0

Next Milestone:

04/2023 finalizing of BORDER for the first project lifetime

04/2023 first integration of an external third party adapter 

|O2|Parts|
04/2021 start of development usermanagement microservice

04/2021 start of development blockchain connector microservice

04/2021 start of integration blockchain ewallet

04/2021 start of development frontend

07/2021 start of integration blockchain token-manager

09/2021 start of integration light client frontend

04/2022 start of cooperation for DOKLASS integration


|O3|Process model|
Development, agile/Scrum. To describe the architecture arc42 is used.
An architecture documentation structured according to this template is a key project result.
|O4|Development tools|
Design with Draw.io, in addition Magic Draw or Enterprise Architect.
Work results for architecture documentation collected in Confluence Wiki.

Golang source code created with Visual Studio Code / GoLand, Angular source code created with Visual Studio Code.
|O5|Configuration management|Git at GitLab hosted by Fraunhofer: https://git.openlogisticsfoundation.org/silicon-economy/base/iotbroker
|O6|Test tools and test processes|
Unit testing as part of a CI/CD Pipeline (stage test)

Typescript based components: Jasmine and karma with headless chrome
|O7|Other tools|
npm 8 for Build Management

Angular 11 and Typescript as UI Framework

NestJs 7 as Application Framework (Typescript)

Kubernetes (OKD version 4.5) based Silicon Economy Plattform

Cosmos SDK v0.42.6 & Starport 16.01 for Blockchain Development
|=======================

=== Political Constraints
The following political constraints apply:

[options="header"]
[cols="~h,~,~"]
|=======================
|Id|Constraint|Background and / or Motivation
|P1|Release as Open Source|The source code of the solution is made available as open source. License: Open Logistics License 1.0, Hosted by Blockchain Europe.
|P2|All dependencies are Open Source|A complete list of (transitive) dependencies and their Open Source licences needs to be provided
|=======================

=== Conventions
All conventions are listed in the Silicon Economy Development Guidelines, e.g.
[options="header"]
[cols="~h,~,~"]
|=======================
|Id|Constraint|Background and / or Motivation
|C1|Architecture documentation|Terminology and structure according to arc42, version 6.0
|C2|Coding guidelines for Java|https://google.github.io/styleguide/javaguide.html[Google Java coding] conventions, checked using SonarQube
|C3|Coding guidelines for TypeScript|https://angular.io/guide/styleguide[Angular coding style] / https://github.com/google/gts[Google TypeScript Style] conventions, checked using SonarQube
|C4|Language|Documentation in English, naming of things (components, interfaces, variables) in diagrams and source code in English
|C5|Versioning|Semantic Versioning 2.0.0 (https://semver.org/lang/de/) in GitLab
|C6|Specific file formats|JSON (preferred), YAML, XML
|=======================
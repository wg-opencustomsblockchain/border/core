== Chapter 12: Tutorial

For an easy start, all components can be deployed in one step using docker-compose. To use the compose file the repositories have to be cloned, after that using docker-compose, the images can be build and be run as docker containers. The compose file contains all neccessary configurations for the respective containers. For the communication in the backend, an  https://www.rabbitmq.com/[RabbitMQ].AMQP-broker needs to be deployed. See https://www.rabbitmq.com/download.html for more information on how to download and install an RabbitMQ Broker.

A detailed starting guide can also be found in the readme.md at the root of the BORDER repository with additional info on how to start developing. It also includes additional details about the recommended development environment.

[source, javascript]
----
git clone https://gitlab.cc-asp.fraunhofer.de/silicon-economy/services/border/border.git --recursive
----

[source, javascript]
----
version: "3.9"
services:
    blockchain-connector:
        depends_on:
            - mosquitto
            - blockchain
        build: ./border.backend.data.blockchain.connector/
        environment:
            - TENDERMINT_CHAIN_ENDPOINT_NODE=http://blockchain:1317
            - TENDERMINT_CHAIN_ENDPOINT_RPC=http://blockchain:26657
            - MSG_QUEUE_URL=http://mosquitto:1883

    mosquitto:
        image: eclipse-mosquitto:1.6.14
        hostname: mosquitto
        container_name: mosquitto
        expose:
            - 1883
            - 9001
        ports:
            - 1883:1883

    api-controller:
        build: ./border.backend.data.controller/
        depends_on:
            - mosquitto
        ports:
            - 8081:3000
        environment:
            - MSG_QUEUE_URL=http://mosquitto:1883
            - JWT_SECRET=bOrDeRsEcReT

    usermanagement-controller:
        build:
            context: ./border.backend.usermanagement/
            dockerfile: Dockerfile.controller
        ports:
            - 8082:8082
        environment:
            - MSG_QUEUE_URL=http://mosquitto:1883

    usermanagement-service:
        build:
            context: ./border.backend.usermanagement/
            dockerfile: Dockerfile.service
        environment:
            - MSG_QUEUE_URL=http://mosquitto:1883
            - JWT_SECRET=bOrDeRsEcReT
            - JWT_EXPIRATION_TIME=1h

    frontend:
      build:
        context: ./border.frontend/
        dockerfile: ./Dockerfile-Docker
      depends_on:
        - api-controller
        - usermanagement-controller
        - blockchain
      ports:
        - 8080:8080
      environment:
        - USER_API_URL=http://usermanagement-controller:3000/
        - EAD_API_URL=http://api-controller:3000/
        - TENDERMINT_CHAIN_ENDPOINT_RPC=http://blockchain:26657/
        - TENDERMINT_CHAIN_ENDPOINT_RPC=http://blockchain:1317/

    blockchain:
        build: ./border.backend.blockchain/
        ports:
            - 26657:26657
            - 1317:1317
            - 12344:12345
----

[source, javascript]
----
docker-compose build
docker-compose up
----

The app's frontend is now avaiable at https://border.public.apps.blockchain-europe.iml.fraunhofer.de/login

=== Use-Case-related assumptions
Due to the given complexity of customs processing, a few assumptions were made in order to reduce the complexity of this first use case.
These assumptions only reduced the use case to a level that was manageable within the scope of the research project. The characteristics and process steps of customs processing that [underline]#are# contained within this application case, were not abstracted or simplified, to ensure sufficient practical relevance.

==== Assumptions made
1. Location: [bold]#EU-based customs process# for the exporting user (Persona: Erik) and UK-based import (Persona: Ina)
2. General type of trade: [bold]#External Trade# (with third country)
3. Type of customs process: [bold]#Export# from an EU Country to a non-EU third country, [bold]#Simplified Decleration Procedure# with formal authorisation ("Vereinfachte Zollanmeldung mit förmlicher Bewilligung")
4. Exporting Country: [bold]#EU country, customs union member state#; if spefication required: [bold]#Germany#
5. No use of representatives/brokers: [bold]#The Exporter is also the export declarant/consignor in this case); the Importer is also the import declarant/consginee in this case#
6. The users of the application are known (login credendtials and Blockchain addresses) and no new users are created at runtime of the blockchain
7. The roles and rights of the participating companies and their employees are statically defined from the beginning and are not changed during the runtime


==== Resulting restrictions
1. Involved parties currently covered by software application: [bold]#Exporter (also the export declarant/consignor in this case); Forwarder; Importer (also the import declarant/consginee in this case)#
2. Customs & logistical process steps currently covered by software application: [bold]#Generation and transfer of accompanying documents# (after export declaration; before import declaration;); Transport; Presentation at customs office of exit;
3. Document covered: [bold]#Export accompanying document (EAD)#
4. Data covered: Process-relevant data fields of the EAD

==== What can the software do?
Upon receiving the export clearance from the customs authorities, an [bold]#exporter# can use the BORDER software to extract the information contained within the EAD XML-fie and write it as a dataset on the blockchain. From this point onward, the exporter can transmit this information to the importer/consignee in a third country via blockchain. Even before the shipment has left the exporter's premises and without any print-out of the EAD.
Once a shipment is ready for pickup, the exporter can generate a QR code that links to the corresponding shipment information on the blockchain.

The [bold]#importer# receives customs-relevant information early on in the process and can amend or enhance this dataset for the import declaration. Any changes or data entries that the importer adds, are stored on the blockchain as well.

By logging in to the system via smartphone and scanning the [underline]#QR Code#, the [bold]#forwarder# can receive the relevant information from the EAD and confirm acceptance of the shipment on the blockchain. At customs control points or for the final presenation of goods at the customs office of exit, the forwarder can produce a [underline]#barcode in the format required by customs# and present it on the smartphone. No paper print-out of the EAD must accompany the shipment anymore.

=== Deployment using helm files
The BORDER repositories contain the needed helm configuration for deployment on a Kubernetes cluster.

For deployment on kubernetes the user has to build the docker image and push it to a docker repository. Afterwards, the helm configuration has to be configured and deployed onto the kubernetes cluster.

To configure the helm configuration, you need to edit the values.yaml entering the correct information for the intended deployment. After that, the deployment can be pushed onto the kubernetes cluster.

For the OpenShift distribution of Kubernetes use the oc command like displayed in the okd_deploy.sh files, e.g. for the api controller:

[source, javascript]
----
#!/bin/bash
# Use Namespace already provided or the default given here
NAMESPACE="${NAMESPACE:=border}"

# Switch to project if it exists or create a new one
oc project "$NAMESPACE"
# Upgrade or install
helm upgrade --namespace "$NAMESPACE" -i border-backend-api-controller .
# Ensure image stream picks up the new docker image right away
oc import-image border-backend-api-controller
----

==== Deployment Development
The development branch of the BORDER repository is automatically deployed to the BE Kubernetes Cluster via the Gitlab Pipeline. (https://border-dev.apps.blockchain-europe.iml.fraunhofer.de)

==== Deployment Staging
The main branch of the BORDER repository is automatically deployed to the BE Kubernetes Cluster via the Gitlab Pipeline. (https://border.apps.blockchain-europe.iml.fraunhofer.de) This version is stable and will not get updated during development process.

==== Deployment Backup
Due to possible problems with the BE Cluster, the Backup Deployment is published to (https://border.apps.sele.iml.fraunhofer.de). It has the same state as the Staging Deployment and is used as fail save, if the BE Cluster is not reachable. The deploy is automated via the Gitlab Pipeline.

=== Running a blockchain network

Please refer to https://git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/documentation/-/blob/main/chapter12.adoc[chapter 12] of the documentation of the digital folder for all information on how to set up a blockchain network, especially https://git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/documentation#user-content-12-5-running-a-blockchain-network[section 12.5 "Running a blockchain network"].

=== Use Case
The use case described below can be structured on the basis of the personas considered and the process statuses. First, the statuses in the import and export process are presented. Based on these basic logistical and customs status chains, the functionalities of the three personas are described below.

Personas are described below ("Roles").

==== Status Overview:
The process status chains can be subdivided into export and import status. These status chains can also be run through in parallel and largely independently of each other. The statuses with a grey background have not yet been integrated into the solution.
The export status covers the process from the release and the associated possibility of the initial recording on the blockchain to the successful conclusion of the customs procedure with the exit and the associated systemic archiving:

[cols=",,,",options="header",]
|===
|Export Status |Description |Trigger |Triggered by
|New |New export processes are imported into the solution (not in the blockchain) |New EAD found |System / API / Erik

|Recorded on Blockchain |Export information from the EAD is written on the blockchain |Writing command confirmed |Erik

|Ready for pickup |Export goods are released for pickup at the exporters location |QR-Code generated |Erik

|Pickup complete |Goods and export information are handed over to the logistics service provider (LSP) | QR-Code Scan and Pick-Up Confirmation
successful |Freddy

|Presentation of goods complete |LSP presented the export goods at the customs office of exit and showed the MRN barcode |Presentation of the MRN-Barcode confirmed |Freddy

|Exit of goods certified |The exit is certified by the customs office of export and the export process is completed. |Exit confirmation selected (tbd) |Erik
|===

The import process begins with the provision of the relevant data for the import declaration and ends with the  completion of the import procedure through the release for free circulation in the destination
country:

[cols=",,,",options="header",]
|===
|Import Status |Description |Trigger |Triggered by
|Import proposal |An import process addressed to the user is written in the blockchain and presented to the user |New export with the matching criteria is written on the blockchain |System / Erik

|Proposal accepted |The proposal of a new import process is accepted by the import clerk and added to the list of recent imports |Acceptance confirmed |Ina

|Import data added |The import data is updated based on the data from the export token |Finalisation of the data updated with the matching command (tbd) |Ina

|Import data set forwarded |The import data set is exported from the blockchain into certified customs IT |export functionality is selected (tbd) |Ina

|Import declaration lodged |The import declaration based on the presented information is successfully lodged at the customs authorities |tbd / out of scope |tbd / out of scope

|Goods released |The imported goods are released in the protected market |tbd / out of scope |tbd / out of scope
|===

==== Usage of the BORDER Blockchain Solution:

Login

.`_Login page_`
image::images/chapter_12/01_Login.png[Border Summary, 600]

On the first page of the progressive web app, you can perform a log in with your credentials.
Based on your role and the underlying rights and functionalities, the following pages will differ from each other.

To open the related page and perform tasks, log in with username and password.

==== Roles

The possible role assigned to you is one of the following:

[cols=",,",options="header",]
|===
|Role |Log-In in Demonstrator phase |Functionalities of the role

|Customs Broker |Border Broker a|
*	Write new Export Tokens on the blockchain based on EAD data from the customs authorities


|Exporter / Export Clerk |Erik a|
*	Check and accept export data on the blockchain and forward it to the importing supply chain partner
*	Generate QR-Codes for the Hand-Over of Export Tokens to Logistics Service Providers
*	View the detail information of Export Processes (data and process statuses)
*	Add further information to export tokens
*	Archive of finished export processes
*	View and set the import status


|Importer / Import Clerk |Ina a|
*	Accept proposed imports
*	Check the proposed data from the connected export process
*	Edit the transferred data from the export process
*	View the export status
*	View and set the import status



|Logistikcs Service Provider / Driver |Freddy a|
*	Takeover of export processes through QR Scan
*	Handling of one export data set
*	Presentation of exports at the customs office of exit by showing the MRN barcode



|===

*Selected User: Exporter*

===== Home / Functionality Overview

After logging in using the role of an export clerk you have access to the following pages:

*	Write / Release new export data sets on the Blockchain (and share them with other stakeholders)
*	View the detailed information on Export Processes
*	Generate QR-Codes for the Hand-Over of Export Tokens to the driver
*	Archive of finished export processes

.`_Exporter - Homescreen_`
image::images/chapter_12/02_Erik_Hompage.jpg[Border Summary,600]


===== Release Page

.`_Exporter - Release confirmation page_`
image::images/chapter_12/03_Exporter Release_ Confirm_selection.jpg[Border Summary, 600]

On the Release page an overview of all new export data sets (based on released export accompanying documents) is shown. These are written on the blockchain by the export clerk or customs brokers with direct API-access to the blockchain and the blockchain wallets.
Here export token can be selected. By releasing the export data sets, the data is shared with the respective importer (based on the data itself). Only export processes with the statuses 0 and 1 are displayed here.

The matching details are as followed:

* Master reference number (MRN)
* Destination country
* Consignee
* Customs office of exit
* Incoterm


===== Detail View Page - Overview

.`_Exporter - Detail View page_`
image::images/chapter_12/04_Exporter_Detailview.jpg[Border Summary, 600]

On the Detail View page additional information for every single export token are shown in an overview and in detail. For a first overview, key data from the overall data set is presented, including:

* Actual status
* Next action (status-dependent button)
* Last update (updates on the blockchain)
* Consignee
* Customs Office of Exit
* Destination Country

By using the filter, individual EADs can be selected.

===== Detail View Page - Token View

.`_Exporter - View selected Token_` 
image::images/chapter_12/05_Exporter_Detailview_Overview.jpg[Border Summary, 600]

By selecting a single export process, more details can be viewed. This additional information focus on detailed data fields, current editors of the process status or data fields, an event history, mentioned files from the export declaration and information on the different positions from the export.
The next possible action can be triggered in this view, too.

The color coding shows which agent has made the last change to an entry. By selecting the icon, the agent is displayed. In Figure 6, it is "Border Brooker" who initially changed the entry.

The whole export token can be viewed from this window, too:

.`_Import Token - General Information_` 
image::images/chapter_12/06_Exporter_Edit_Export_Entry_General.jpg[Border Summary,600]
.`_Import Token - Company Information_`  
image::images/chapter_12/07_Exporter_Edit_Export_Company.jpg[Border Summary, 600]
.`_Import Token - Logitics Information_`
image::images/chapter_12/08_Exporter_Export_Logistics.jpg[Border Summary, 600]
.`_Import Token - Positions_`
image::images/chapter_12/09_Exporter_Export_Positions.jpg[Border Summary, 600]

As soon as the EAD is captured on the blockchain the QR-Code can be generated.
To do so proceed to the page QR-Code Generation or choose the functionality directly in the "Next action" column.

===== QR Code Creation

.`_Exporter - Generating QR Code_` 
image::images/chapter_12/10_Exporter_Export_Generate_QR_Code.jpg[Border Summary, 600]

On this page the user can generate the QR-Code for any Export Token that is already written on the blockchain ("released"), by selecting the entry.
For every possible export token the listed properties are:

* MRN (Master reference number)
* Destination country
* Consignee
* Customs office of exit
* Incoterm


.`_Loading Animation - Writing Token to Blockchain_` 
image::images/chapter_12/11_Exporter_Export_Writing_to_Blockchain.jpg[Border Summary, 600]

Before the QR-Code will be available for the logistics service provider
the user is asked to verify the chosen action. +
After the QR-Code is available the status of the export token is updated
to status 2 ("Ready for Pickup").


===== QR Code View

.`_Exporter - View QR Code_`
image::images/chapter_12/12_Exporter_Export_Display_QR_Code.jpg[Border Summary, 600]

The presented QR Code for one export token contains a unique link to an export token on the blockchain. By scanning this QR-Code, a person equipped with the necessary rights, can gain access to the necessary segment in the blockchain wallet, select the token on the blockchain accept it/ the corresponding shipment.

===== Archive
.`_Exproter - Archiv page_`
image::images/chapter_12/13_Exporter_Export_Archive.jpg[Border Summary, 600]

Export tokens with a status "Exit of goods certified" are completed from an export perspective.

On the Archive page every archived export token with the status Exit of goods certified can be accessed. For this purpose, a filter is displayed.


==== Logistics service provider

.`_Logistics Service Porvider - Home Screen_` 
image::images/chapter_12/14 Logistics homescreen.jpg[Border Summary, 200]

In the role of a logistics service provider, you have the options of: 

* scanning a QR-Code for accessing an export process and displaying the current active process after pick up
* or to enter the MRN manually 

In the current demonstration the truck driver can accept, transport, and pass on just one single export token with one single ID and MRN for presentation at the customs office of exit.


===== QR-Code Scanner

.`_Logistics Service Porvider - QR Code Scanning_`
image::images/chapter_12/15_Logistics_QR-Code_Scanner.png[Border Summary, 200]
.`_Logistics Service Porvider - QR Code Scanned_`
image::images/chapter_12/15_1_Logistics_QR_Confirmation.png[Border Summary, 200]
.`_Logistics Service Porvider - QR Code confirmation_`
image::images/chapter_12/15_2_Logistics_QR_Scan_Confirmation.png[Border Summary, 200]
.`_Logistics Service Porvider - Updating the Blockchain_` 
image::images/chapter_12/15_3_Logistics_Assigned_the_EAD.png[Border Summary, 200]

By using the automatically opened camera function, QR codes with Token IDs can be scanned and blockchain entries can be accessed.
If a valid QR Code is scanned and the necessary authorization and rights are available, a detailed view of the export token is displayed in an overview screen.
If this information matches the order information, the export details and the status update can be confirmed. This will update the export status with reference to the user. With this confirmation, the Export Token is assigned to the driver and can be accessed again when the goods are presented.

===== View MRN

.`_View MRN_` 
image::images/chapter_12/15_4 Logistics_Display_MRN.png[Border Summary, 200]


When choosing this the display functionality, the MRN and the corresponding barcode of the currently assigned export token are displayed. The barcode is used for presentation at the customs offices of export in the European Union.

After displaying the current MRN you must give a reason why you have presented the MRN. The possible options are:

*	Mobile customs inspection
*	Presentation at customs office of exit

Before the chosen reason is written permanently to the blockchain, you must confirm the option. 
After a confirmed presentation at the customs office of exit, the export token is no longer matched to the logistics personnel.


==== Importer

.`_Importer - Homescreen_` 
image::images/chapter_12/16_Importer_Import_Home.jpg[Border Summary, 600]

In the Role of Importer the pages of _Active Imports_ and _New Imports_ are displayed.


===== New Imports
.`_Importer - New Imports Page_`
image::images/chapter_12/17_Importer_New_Imports.png[Border Summary, 600]
.`_Importer - Selected Import_`
image::images/chapter_12/17_1_Importer New_Imports.png[Border Summary, 600]

On the _New Imports_ page, the current import proposal can be analysed and accepted. 
If done so they will be transferred to the _Active Import_ section.

===== Active Import
.`_Importer - Active Imports page_`
image::images/chapter_12/18_Importer_actice_Imports.jpg[Border Summary, 600]
.`_Importer - Selected Import_` 
image::images/chapter_12/18_Importer_actice_Imports_2.jpg[Border Summary, 600]

The active Import page lists all Imports currently active by ID with following information display:

*	MRN
*	Country of Origin
*	Consignor
*	Incoterm
*	Status
**	Next Action
**	Last updated


By opening a specific import, you gain access to detailed information about the individual status:

* Description
* Mass [kg]
* Number of packages
* Statistical Value [€]
* Status:
** Import Status (Recorded on Blockchain, Ready for pickup, Pickup complete, Presentation of goods complete, Exit of goods certified)
** Export Status (Import proposal, Proposal accepted, Import data added, Import data set forwarded, Import declaration lodged, Goods released)


By opening the next action detailed information can be edited. The next step is to submit the data. To do so every entry can be edited individually as shown below or all entry’s may be transferred by selecting Transfer all.

The color coding shows which agent has made the last change to an entry. By selecting the icon, the agent is displayed.

The detailed Information includes:

.`_Import Token - Edit general information_` 
image::images/chapter_12/22_Importer_Edit_General.jpg[Border Summary, 600]

.General
* Import ID
* Nature of Transaction
* Total Amount invoiced
* Currency
* Unique Consignment Reference Number (UCR)
* Local reference number


.Customs office of entry
* Street and Number
* Postcode
* City
* Country code
* Customs office code


.Customs office of Import
* Street and Number
* Postcode
* City
* Country code
* Customs office code

.`_Import Token - Edit company information_`
image::images/chapter_12/24 Import Edit import entry company.jpg[Border Summary, 600]

Company related data


.Consignor/Exporter:
* Identification Number
* Name
* Street and Number
* Postcode
* City
* Country Code


.Declarant:
* Identification Number
* Subsidiary Number
* Name
* Street and Number
* Postcode
* City
* Country Code


.Consignee:
* Identification Number
* Subsidiary Number
* Name
* Street and Number
* Postcode
* City
* Country Code


In the case that the consignee is also the declarant of the customs declaration, the inputs can be copied by choosing the functionality "exporter = declarant".

.`_Import Token - Edit logistics informaiotn_`
image::images/chapter_12/24 Import Edit import entry logistics.jpg[Border Summary, 600]

Logistics

.General
* Export country
* Country of destination
* Province of destination
* Incoterm
* Incoterm (Location)


.Transport at the border
* Mode of Transport
* Type of identification
* Identity of transportation
* Nationality
* Costs
* Currency
* Order number of transport


.Transport from domestic Border
* Mode of Transport
* Type of Identification of Means of Transport
* Identity of means of transport
* Nationality
* Costs
* Currency
* Order number of Transport

.`_Import Token -  Edit Positions_`
image::images/chapter_12/24 Import edit import entry positions.jpg[Border Summary, 600]

[%hardbreaks] 

.Positions
* Description of goods
* Combined nomenclature code
* SUM
** Number of positions
** Number of packages
** Overall gross mass
** Overall net mass
** Total invoice amount
** Currency
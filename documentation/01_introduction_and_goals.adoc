== Chapter 1: Introduction and Goals

=== Introduction

==== Introduction to link:https://blockchain-europe.nrw/en/homepage-english/[Blockchain Europe]
In the »Blockchain Europe« project to establish the European Blockchain Institute in North-Rhine Westphalia (NRW), we will be driving blockchain technology forwards together with companies and other research institutions. This sees a unique European institute being created to advance digitization in science and practice. Particularly in logistics and supply chain management many different, economically independent partners have to work closely together even though they do not inevitably have complete trust in each other. The focus of our research is therefore on open and integrated solutions that can be used by all players on the market. This will make it possible to digitally connect entire logistics chains from end to end. In our research and demonstration center we will show the opportunities blockchain provides live and oriented to applications while offering specific examples for practical uses.

==== Silicon Economy - The big picture
In a Silicon Economy (SE), services are executed and used via platforms. A Silicon Economy Platform consists of five different kinds of major components, namely one or more SE Services, one or more IDS connectors, a Logistics Broker, an optional IoT Broker and an optional Blockchain Full-Node. This platform is the environment of SE Services, its location does not matter (e.g., IT environment of a company, a cloud environment, etc.).

SE Services are professional (logistics) services, consisting of IT and/or physical services. They are part of a functional (logistics) process and can be linked and subsequently orchestrated in the sense of that process.

image::images/chapter_1/1.1_Silicon-Economy-Overview.png[Overview silicon economy platform]

* IDS Connector (not integrated): Industrial Data Space Connector -Used for secure communication of various contents and management of access rights to various data.

* Logistics Broker (not integrated): Logistics brokers organize logistics services and their processing and connect the providers of logistics services with customers and users. This applies equally to internal and external logistics.

* SE Service (use case related): SE Services are professional (logistics) services, consisting of IT and/or physical services. They are part of a functional (logistics) process and can be linked and subsequently orchestrated in the sense of that process. Furthermore, SE Services can be integrated into existing structures, and must be able to be executed and monitored. SE Services are documented. SE Services must be able to be booked and accounted for and have a (technical) description that includes all information about the function of the service and the conditions of use. Finally, SE Services must be able to be supplied with data and must be able to be called via the IDS.

* IoT Broker (not integrated): IoT brokers and their components are essential data sources for the Silicon Economy. They connect the cyberphysical systems (CPS) such as smart containers and pallets, as well as smart machines with the Silicon Economy - e.g., with cloud services and platforms. IoT brokers trade near-real-time data and information over fixed and (especially) mobile networks such as 5G and low power wide area networks (LPWAN such as NB-IoT...). The development of open source devices and open source software for IoT devices and CPS are also located here

* Blockchain (integrated): The blockchain is a decentralized, distributed, cooperative data store. It enables the secure exchange of data in networks without recourse to an intermediary. The basis of blockchain technology is the so-called "distributed ledger", a distributed stored logbook, which contains entries with information. These entries are time-stamped and also contain a reference to the previous entry.

Furthermore, SE Services can be integrated into existing structures, and must be able to be executed and monitored. SE Services are documented. SE Services must be able to be booked and accounted for and have a (technical) description that includes all information about the function of the service and the conditions of use. Finally, SE Services must be able to be supplied with data and must be able to be called via the IDS.

==== BORDER - A Blockchain Europe development project

____
BORDER = Blockchain‑based organization of relevant documents in external trade with regulatory compliance
____

Customs clearance in foreign trade is a highly regulated area, shaped by national and EU regulations, as well as through various requirements of the respective third countries. The large number of involved international players often do not have a common level of knowledge about the progress of customs clearance and the logistics process.

Overall, customs clearance is (still) heavily paper-based. In the BORDER development project, research is being conducted into how and in which areas of the customs clearance process blockchain technology in combination with smart contracts can further automate existing processes and improve interaction between the players involved, including on an international level. The results will be transferred directly into practice through our partners.

image::images/chapter_1/1.2_Border-Project-Summary_1.PNG[Border Summary]
See also: https://blockchain-europe.nrw/en/customs/

*Challenges:*

The project is inspired by different current challenges in customs processing in foreign trade between the European Union and third countries:

Customs declarations are already digital but many paper documents come into play for the physical transport of goods
Involved parties have no shared truth on the process progress

image::images/chapter_1/1.3_Border-Challenges.PNG[Border Challenges]

*Benefits:*

All the current and future partners involved want to have all customs documents available in digital form and make the paper form obsolete. The blockchain technology used is intended to make it possible to securely store digital processes and documents for all parties involved and – linked to the physical flow of goods – to update them. This would make their information traceable at all times for exporters (consignors), importers (consignees), logistics partners, transport companies, customs brokers and, most importantly, customs and other government authorities.

The goal: End-to-end digitized and largely automated customs processing.

image::images/chapter_1/1.4_Border-Benefits.PNG[Border Benefits]

In the next diagram, the components and internal structure of the BORDER project can be seen:

image::images/chapter_1/1.5_Architecture.png[Border Infrastructure]
Compare: 5.3 Building Blocks - Level 2


=== Requirements Overview
From an end user's point of view, BORDER improves the transparency of customs processes between countries and companies. Furthermore the solution automates manual tasks and digitizes paper-based processes. The improvements are achieved for export clerks at the exporting company, for import clerks in the importing companies and for operational logistic service providers (LSP) at the current release status:

The export clerk gains insights in the logistic processes following the export declaration and has a manipulation safe documentation of the information under his responsibility. In addition environmental gains through reduction of paper based documentation can be achieved.
The import clerk gains process transparency in the exporting country at an early stage regarding customs and logistics process steps. In addition to process transparency, importers gain access to genuine data from the export declaration for reuse in following import declarations.
LSPs and their personell can
We summarize the requirements for BORDER, which are detailed in the Jira.

*What is the BORDER Blockchain solution*

A solution to digitize and standardize the information sharing between companies in customs processes

Avoiding paper use by digitising customs documents while complying with relevant regulations (non-printed EAD with digital MRN barcode)
Automatic allocation of information in the transnational supply chain
Transfer and further processing of former export data in the third country for import preparation
Digitalised information transfer between companies with independent IT solutions
A solution improve transparency in customs and logistic processes
Information sharing in early stages of the process to avoid waiting times
Access to process information of other actors controlled via an authorisation module
Tamper-Proof documentation of responsibilities and data provenance in international trade

*Essential Features*

. Bridging information asymmetries and improve data integrity between supply chain partners through a shared truth on the Blockchain

. Mapping of export and import information into tokens on the blockchain

. Compliance with the customs process framework conditions while leveraging already existing digitalisation potentials

*Functional requirements*

. Data Sovereignity - Data access to corporate data can be controlled and restricted so that the value of corporate data can be disseminated in a controlled manner.

. Irreversibility - Written information and transactions are stored unchangeably and can only be updated via updates to the existing information but not removed.

. Interoperability - Easy integration into existing IT ecosystems in different companies and countries.

. Process-fit - The BORDER service fits to existing basic logistical and customs processes. It does not require major process changes, to be usable.

=== Quality Goals
The following table describes the quality goals of Blockchain Europe classified byISO/IEC 9126-1 characteristics. The order of goals gives a rough idea of their importance.

[options="header"]
[frame="topbot",grid="none"]
|=======================
|Quality Goal|Motivation/Description
|Reliability (Robustness)       |Due to the decentralized nature of a blockchain, Reliability (Robustness) is inherent
|Efficiency (Scalability)       |Depending on the type of blockchain solution, additional nodes can join the blockchain network at will or after prior approval, e.g. by a committee.
|Usability (Simplicity)         |For tech-savvy individuals of a Silicon Economy, the use case-specific use of blockchain solutions and functionalities of the BORDER system must be as simple as possible. Additionally, as the BORDER system interacts with, and is partly composed of Blockchain Europe basic components, an integration with these, as well as third-party services must be possible.
|Usability (Attractiveness)     |The BORDER Modules are generally not used on their own, but in the course of a use case. In the course of this, the modules must appeal to the blockchain Administrators and other stakeholders especially in demonstrations.
|Functionality (Secure updates) |Minor updates for blockchain solutions and functionalities of the BORDER system must not lead to an error in the application. There might be some major releases that require an adaption of the application that uses the blockchain solutions and functionalities of the BORDER system.
|Functionality (Completeness)   |Every transaction must be broadcasted and processed correctly in the blockchain network.
|Usability / Maintainability    |The usage of any modules of the BORDER system must be clear and at the same time include all configuration options for a specific use case.
|=======================

=== Stakeholders
The following table lists the most important stakeholders of an XXX (person, roles and/or organizations) and their respective expectations, goal and intentions:

[options="header"]
[source, adoc]
|=====================
|Steakholder Group|Role|Description|Goal, Intention|Example
|Member of the Blockchain Network | Light Client User | Can be another operative role  | |
|Member of the Blockchain Network | Validation Node User | Can be another operative role  | |
|Member of the Blockchain Network | X Node User | Can be another operative role  | |
|Regulatory Authorities  | Customs Authorities in Exporting Countries |
a| Controlling the leaving goods from the starting point until the exit at the relevant border.

Checking the data and documents of the process for subsequent checks.

* Can perform real-time & retrospective checks, audits, controls and risk assessment on tamperproof data
* Verifiable integrity of customs documentation between direct contact points with authorities
* Reliable, truthful & up-to-date source of information on static and dynamic information
* Distributed data storage makes data exchange independent and transmissible between multiple political, judicial and economic areas |
|Regulatory Authorities  | Customs Authorities in Importing Countries |
a|
Controlling incoming goods from the point of first entry until the arrival at the point of destination.

Checking the data and documents of the process for subsequent checks

* Can perform real-time & retrospective checks, audits, controls and risk assessment on tamper‑proof data
* Verifiable integrity of customs documentation between direct contact points with authorities
* Reliable, truthful & up-to-date source of information on static and dynamic information
* Distributed data storage makes data exchange independent and transmissible between multiple political, judicial and economic areas
|
|Industry enterprises | Exporter / Consignor | The exporter is the party who transfers goods or has goods transferred to foreign economic territories.
a|
* Less time & effort for preparation and clearance of exports
* Simplified & consistent data exchange
* Fewer paper printouts and manual entries
* Tamper-Proof documentation of exporter's data
|
|Industry enterprises | Importers / Consignees | An importer is anyone who brings goods or has goods brought into the economic territory.
a|
* Less time & effort for preparation and clearance of imports
* Cross-border access to verified information early in the process
* Simplified & consistent data exchange
|
|Industry enterprises | Logistics Service Providers |	Logistics service providers take over logistical or administrative tasks from the customs process.
a|
* Automatic data provision and reduced effort
* Simplified, automated collaboration
* Less paper clutter
* Less time checking integrity of data
|
|Industry enterprises | IT Service Providers | Companies that develop or provide software that can be used for customs clearance and interactions with customs authorities.
a|
* Expansion of service portfolio
* Added blockchain functionality for more security and transparency in customs-related interactions
* Access to open-source software without own development effort
|
|Industry enterprises | Custom Brokers | Companies that provide customs-related services for third parties, e. g. customs clearance and interactions with customs authorities.
a|
* Less time checking integrity of data
* Verifiable compliance for customs audits
* Expansion of service portfolio
* Added blockchain functionality for more security and transparency in customs-related interactions
|
|=====================


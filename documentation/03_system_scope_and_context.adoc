== Chapter 3: System Scope and Context (PDO)

This section describes the environment and scope of the BORDER blockchain solution. Who are its users, and with which other systems does it interact. It thereby specifies the external interfaces (domain interfaces and technical interfaces to communication partners). BORDER is a black box here.

=== Business Context
Stakeholders of BORDER need to understand which data is exchanged with the environment. All communication partners (users, IT-systems, …) of BORDER are described below. It specifies (the interfaces of) all communication partners in business/domain view. The following table explains the domain-specific inputs and outputs or interfaces, which may include domain-specific formats or communication protocols.

[options="header"]
[cols="~,~,~,~,~,~"]
|=======================
|Communication partner|Kind|Domain Inputs|Domain Outputs|Protocol|Data format
|BORDER Admin (UI link)|User|Device configuration, Device management|BORDER status|HTTP (Web Frontend)|HTML, CSS, Javascript
|Customs or ERP Systems in exporting countries|IT System|Use case data|/||XML, PDF (planned), API/ other formats
|Customs or ERP Systems in importing countries|IT System|/|Use case data||tbd
|Export agent|User|Data and processes updates|Process and status information|HTTP (PWA Frontend)|HTML,CSS,Javascript
|Import agent|User|Data and processes updates|Process and status information|HTTP (PWA Frontend)|HTML,CSS,Javascript
|Truck Driver|User|Data and processes updates|Process and status information|HTTP (PWA Frontend)|HTML,CSS,Javascript
|=======================

=== Technical Context
In contrast to the more abstract business context, here the technical view is described, e.g. different technical channels, protocols or interfaces used to realize the communication with the objects from the business context. Actual instances of building blocks communicate in the technical context. For details see link:https://git.openlogisticsfoundation.org/silicon-economy/services/border/core/-/blob/main/documentation/07_deployment_view.adoc[Chapter 7: Deployment View]. 

The following technical interfaces link BORDER to its environment:

[options="header"]
[cols="~,~,~,~,~"]
|=======================
| Interface | Inputs | Outputs | Protocol | Data format   
| REST-API | DTO/Parameters | DTO | HTTP | JSON 
| Message Queues | DTO | DTO | AMQP (RabbitMQ) | JSON  
| Blockchain Light Client | TxMessages | TxResponses | JSON gRPC | JSON/Binary 

|=======================

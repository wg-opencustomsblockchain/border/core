#!/bin/bash

while getopts 'n:f:p:i:t:' flag
do
    case "${flag}" in
        n) NAMESPACE=${OPTARG};;
        f) FILE=${OPTARG};;
        p) HELM_PATH=${OPTARG};;
        i) APPLICATION=${OPTARG};;
        t) TAG=${OPTARG};;
    esac
done

echo "Deploying $APPLICATION:$TAG with config file $FILE and path $HELM_PATH to namespace $NAMESPACE"

# Switch to project if it exists or create a new one
oc project "$NAMESPACE"
# Upgrade or install
helm upgrade --namespace "$NAMESPACE" -f $FILE  -i $APPLICATION $HELM_PATH
# Ensure image stream picks up the new docker image right away
oc import-image $APPLICATION:$TAG